# VERSION 1.2.000
# DOCKER-VERSION 0.3.4
#
# 1. Installer docker (http://docker.io)
# 2. Checkout source: svn@https://campus.mines-nantes.fr/CAPE_PairForm/trunk/node_serveur
# 3. Build container: docker-compose up

FROM    node:0.10
MAINTAINER contact@pairform.fr

#Installation des dependances avant le ADD de tous les fichiers pour garder en cache
#en cas de changement de fichiers
ADD package.json /usr/src/package.json
# Install app dependencies
RUN cd /usr/src && npm install
# Installation de supervisor pour le dev
RUN cd /usr/src && npm install supervisor -g
# Installation de forever pour la prod
RUN cd /usr/src && npm install forever -g

# Configuration des fichiers statiques
ADD init/pairform_data.tar.gz /usr/pairform_data

# App
ADD . /usr/src
WORKDIR /usr/src

EXPOSE  3000

CMD npm start
