angular.module('ui.thumbnail', [])

.provider('ThumbnailService', function() {

  this.defaults = {
    width: 100,
    height: 100
  };

  this.$get = function($q) {
    var defaults = this.defaults;

    return {

      generate: function generate(src, opts) {
        var deferred = $q.defer();

        opts = opts || {};

        this.load(src, opts).loaded.then(
          function success(canvas) {
            if (opts.returnType === 'blob') {
              if (typeof canvas.toBlob !== 'function') {
                return deferred.reject('Your browser doesn\'t support canvas.toBlob yet. Please polyfill it.');
              }

              try {
                canvas.toBlob(function(blob) {
                  // one may use blob-util to get a URL
                  deferred.resolve(blob);
                }, opts.type, opts.encoderOptions);
              } catch (ex) {
                deferred.reject(ex);
              }

            } else {
              if (typeof canvas.toDataURL !== 'function') {
                return deferred.reject('Your browser doesn\'t support canvas.toDataURL yet. Please polyfill it.');
              }

              try {
                var base64 = canvas.toDataURL(opts.type, opts.encoderOptions);
                deferred.resolve(base64);
              } catch (ex) {
                deferred.reject(ex);
              }
            }
          }
        );

        return deferred.promise;
      },

      load: function load(src, opts) {
        var canvas = this.createCanvas(opts);

        return {
          // creation is done
          created: $q.when(canvas),
          // wait for it
          loaded: this.draw(canvas, src, opts)
        };
      },

      draw: function draw(canvas, src, opts) {
        var deferred = $q.defer();

        var ctx = canvas.getContext('2d');

        // it seems that we cannot reuse image instance for drawing
        var item;

        //Default is an image
        if (typeof opts === "undefined" || typeof opts.media_type === "undefined" || opts.media_type === "image") {
          item = new Image();
          item.onload = onMediaLoad;
          item.src = src;
        }
        else if(opts.media_type === "video"){
          item = document.createElement("video"); 
          item.src = src;
          item.addEventListener("loadeddata", onMediaLoad);
          item.currentTime = 4; // video offset
          item.load();
        }
        else {

          var data =  '<svg xmlns="http://www.w3.org/2000/svg" id="Document" width="178.34" height="210.22" viewBox="0 0 178.34 210.22">' +
                      '  <g id="Document" data-name="Document">' +
                      '    <path d="M559.24,292.12a10,10,0,0,1-10-10V129.36A42.5,42.5,0,0,1,591.71,86.9h120.9a10,10,0,0,1,10,10V282.14a10,10,0,0,1-10,10Z" transform="translate(-546.76 -84.4)" style="fill:#fff"/>' +
                      '    <path d="M712.61,89.4a7.49,7.49,0,0,1,7.48,7.48V282.14a7.49,7.49,0,0,1-7.48,7.48H559.24a7.49,7.49,0,0,1-7.48-7.48V129.36a40,40,0,0,1,40-40h120.9m0-5H591.71a45,45,0,0,0-45,45V282.14a12.48,12.48,0,0,0,12.48,12.48H712.61a12.48,12.48,0,0,0,12.48-12.48V96.88A12.48,12.48,0,0,0,712.61,84.4Z" transform="translate(-546.76 -84.4)" style="fill:#aaa"/>' +
                      '    <path id="Reflet" d="M551.76,252.88l-.17-123.39a40,40,0,0,1,40-40.09h121s-61.24,17.85-103.87,60.48-57,103-57,103" transform="translate(-546.76 -84.4)" style="fill:#f1f1f1"/>' +
                      '    <text transform="translate(89.13 120.74)" style="font-size:60px;fill:#818181;font-family:Helvetica, Helvetica;text-anchor:  middle;">.' + opts.media_extension +'</text>' +
                      '  </g>' +
                      '</svg>';

          item = new Image();
          item.crossOrigin = "Anonymous";

          item.onload = function() {
            canvas.width = 180;
            canvas.height = 212;
            ctx.drawImage(item, 0, 0);
            // loading is done
            deferred.resolve(canvas);
          };
          item.src = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(data);
        }

        function onMediaLoad() {
          var keep_ratio = defaults.keep_ratio;
          if (keep_ratio) {
            // Keep the original dimension, constrained to the wanted height or width
            if (keep_ratio == "height") {
              var width = (this.videoWidth || this.width) * canvas.height / (this.videoHeight || this.height),
                  x = 0;
              if (canvas.width > width) {
                x = (canvas.width - width) / 2;
              }
              else{
                canvas.width = width;
              }
              ctx.drawImage(this, x, 0, width, canvas.height);
            } else if (keep_ratio == "width") {

              var height = (this.videoHeight || this.height) * canvas.width / (this.videoWidth || this.width),
                  y = 0;
              if (canvas.height > height) {
                y = (canvas.height - height) / 2;
              }
              else{
                canvas.height = height;
              }
              ctx.drawImage(this, 0, y, canvas.width, height);
            }
          }
          else
            // designated canvas dimensions should have been set
            ctx.drawImage(this, 0, 0, canvas.width, canvas.height);

          // loading is done
          deferred.resolve(canvas);
        };


        return deferred.promise;
      },

      createCanvas: function createCanvas(opts) {
        var canvas = angular.element('<canvas></canvas>')[0];

        return this.updateCanvas(canvas, opts);
      },

      updateCanvas: function updateCanvas(canvas, opts) {
        opts = opts || {};
        var w = Number(opts.width) || defaults.width;
        var h = Number(opts.height) || defaults.height;

        canvas.width = w;
        canvas.height = h;

        return canvas;
      }

    };

  }; // $get

})

.directive('uiThumbnail', function(ThumbnailService) {

  return {

    restrict: 'E',

    scope: {
      src: '=',
      opts: '='
    },

    link: function link(scope, el, attrs) {
      var promises = ThumbnailService.load(scope.src, scope.opts);

      promises.created.then(
        function created(canvas) {
          // can be appended at this point
          el.append(canvas);
        }
      );

      promises.loaded.then(
        function loaded(canvas) {
          // create watches
          scope.$watch('src', function(newSrc) {
            ThumbnailService.draw(canvas, newSrc);
          });
          scope.$watchCollection('opts', function(newOpts) {
            ThumbnailService.updateCanvas(canvas, newOpts);
            // need to redraw
            ThumbnailService.draw(canvas, scope.src);
          });
        }
      );
    }

  };

})

;