"use strict";

/* Affichage d'une pop-up d'informations pour les navigateurs qui ne sont pas à jour */
$( document ).ready(function() {
	outdatedBrowser({
		bgColor: '#f25648',
		color: '#ffffff',
		lowerThan: 'transform',
		languagePath: 'public/js/outdated-browser-1.1.0/lang/en.html'
	});
});