/*
Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.md or http://ckeditor.com/license
*/
CKEDITOR.plugins.setLang( 'imagetobase64', 'fr', {
	alt: 'Image',
	btnUpload: 'Envoyer sur le serveur',
	captioned: 'Afficher une légende',
	captionPlaceholder: 'Légende',
	infoTab: 'Informations sur l\'image',
	lockRatio: 'Conserver les proportions',
	menu: 'Propriétés de l\'image',
	pathName: 'image',
	pathNameCaption: 'Text alternatif',
	resetSize: 'Réinitialiser la taille',
	resizer: 'Cliquer et glisser pour redimensionner',
	title: 'Propriétés de l\'image',
	uploadTab: 'Téléverser',
	urlMissing: 'Il manque l\'URL source de l\'image.',
	altMissing: 'Il manque la légende.'
} );

