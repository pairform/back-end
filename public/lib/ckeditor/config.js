/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */


/*
 * WARNING !!!
 *
 * En cas de changement de version de ckeditor, conserver :
 * 
 * - le fichier config.js
 * - le skin icy_orange (cf. fichier private/css/backoffice, id #ckeditor_capsule)
 */

CKEDITOR.editorConfig = function( config ) {
	config.language = 'fr';
	config.skin = 'icy_orange';

	config.uiColor = '#F0F0EF';								// couleur de la barre et du cadre de ckeditor
	config.autoGrow_bottomSpace = 50;						// espace en px laissé sous le contenue ckeditor, (évite que le texte soit collé en bas) 
	config.allowedContent = true;
	//config.extraAllowedContent = 'main;nav;*[id];section;blockquote;*(*);*{*};iframe(*);iframe{*}';	//
	config.fillEmptyBlocks = false;							//
	config.tabSpaces = 0;									//
	config.enterMode = CKEDITOR.ENTER_P;					//
	config.dialog_noConfirmCancel = true;					// ne pas ouvire de popup de confirmation lors d'un clic sur le bouton "annuler" de la fenêtre d'un plugin ckeditor

	// boutons de la barre d'outils
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	// imagetobase64 est un plugin PairForm, il n'est donc pas dans la bibliothèque de plugins en ligne de ckeditor
	config.extraPlugins = 'fixed,autogrow,imagetobase64,autolink,button,toolbar,notification,notificationaggregator,embedbase,embed,html5audio,iframe';
	config.removePlugins = 'resize,elementspath';

	// Pour le backoffice, les boutons affichés/masqués sont définis dans initialisationBackoffice.js
	// config.removeButtons = ...

	// utilisation de la clef iFramely de PairForm pour le plugin Embed, permettant d'incorporer des médias externes (youtube ou autre)
	config.embed_provider = '//iframe.ly/api/oembed?url={url}&callback={callback}&api_key=55fe4fae738498e9d224a1';

	// on masque certains onglets et certains champs des plugins : table, link
	CKEDITOR.on('dialogDefinition', function(ev) {
		var dialogDefinition = ev.data.definition;

		if (ev.data.name == 'table') {
			dialogDefinition.removeContents('advanced');
			
			var infoTab = dialogDefinition.getContents('info');
			/*
			infoTab.remove('txtBorder');
			infoTab.remove('cmbAlign');
			infoTab.remove('txtWidth');
			infoTab.remove('txtHeight');
			infoTab.remove('txtCellSpace');
			infoTab.remove('txtCellPad');*/
			infoTab.remove('txtSummary');
		} 
		else if (ev.data.name == 'link') {
			/*dialogDefinition.removeContents('target');*/
			dialogDefinition.removeContents('upload');
			dialogDefinition.removeContents('advanced');
			
			var infoTab = dialogDefinition.getContents('info');

			infoTab.remove('linkType');
		} 
		else if (ev.data.name == 'iframe') {
			dialogDefinition.removeContents('advanced');
		}
	});
};
