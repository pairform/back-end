$(document).ready(function() {
    var controller = $.superscrollorama({
      triggerAtCenter: true,
      playoutAnimations: true
    });

    $('.featurette').each(function () {
      controller.addTween($(this), TweenMax.from($(this), .5, {css:{opacity:0}}), 0, -200);
    });

    $('.anchorHeader').each(function () {
      controller.addTween($(this), TweenMax.fromTo( $(this), .5, {css:{opacity:0, 'letter-spacing':'60px'}, immediateRender:true, ease:Quad.easeInOut}, {css:{opacity:1, 'letter-spacing':'0px'}, ease:Quad.easeInOut}), 0, -100);
    });
    
    $('.row .col-lg-4').each(function () {
      controller.addTween($(this), TweenMax.from($(this), .5, {css:{opacity:0}}),0, -200);
    });
      
    
    $('.shelf').each(function () {
      controller.addTween($(this), TweenMax.from($(this), .5, {css:{position:'relative', left:'-1250px'}, ease:Quad.easeInOut}), 0, -300);
    });
  });