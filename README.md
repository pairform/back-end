
# Surcharge de la configuration

Créer un fichier de configuration local *local.yaml* dans le dossier *config*. Vous pouvez surcharger n'importe quelle propriété, et ce fichier ne sera jamais envoyé via git (il est dans le .gitignore)

## Exemple de fichier config/local.yaml :

    app:
      # port internet utilisé par node
      port: 3000
    
      bdd_mysql:
        database: "PairForm_v1_2_420"
        user: "root"
        password: "toor"
    
      urls:
        serveur: "http://172.21.6.224"
        serveur_node: "http://172.21.6.224:3000"
        alias_serveur: "http://172.21.6.224"
        prerender: "http://172.21.6.224:2999"
        repertoire_upload: "/Users/maen/Dev/PairForm/pairform_data"
        etherpad: "http://172.21.6.224:9001/p/"
        ws: "ws://172.21.6.224:3000"
    
      compte_admin:
        id_utilisateur: 1
        email: "mail@test.fr"
        password: "testosterone"


---

# Installation via Docker :
1. Installer docker (http://docker.io)
2. Checkout source: svn@https://campus.mines-nantes.fr/CAPE_PairForm/trunk/node_serveur
3. Modifier les variables d'environnement dans docker-compose.yml :
  - PF_ENV=development                              // Production | development
  - PF_HOST=http://192.168.99.***                   // $(docker-machine ip $(docker-machine active))
  - PF_ADMIN_EMAIL=votre@email.com                  // Email de l'admin
  - PF_ADMIN_PASSWORD=******                        // Mot de passe de l'admin
  - PF_PAIRFORM_DATA=/usr/pairform_data             // Localisation des données
4. Build des container: docker-compose up
5. Accéder à PairForm via l'IP de la machine docker : $(docker-machine ip $(docker-machine active))

## Notes 

L'initialisation de PF se fait lors de la première installation : 
  - init/Pairform_data.tar.gz est décompressé dans /etc/pairform_data via le DockerFile
  - La BDD specifiée dans docker-compose est créée par le container mysql
  - Initialisation de la BDD (dans init/initialisation.js via server/app.js@setup) : 
    - Le schema de la BDD (init/mysql/schema.sql)
    - Le contenu statique de la BDD (init/mysql/static_tables.sql)
    - Le compte admin (avec les infos passées dans docker-compose.yml)
    - Les entités (espace, ressources...) par défaut à créer (init/mysql/default_objects.json)

## Requis

L'admin à un id_utilisateur = 1


L'espace public : 
    "id_espace" : 1,
    "nom_court" : "Espace public",
    "nom_long" : "Espace public"


La capsule vitrine : 
    "clef_pairform" : "00000000",
    "id_capsule" : 1

Pour gérer la suppression automatique des messages, le Bac à sable doit avoir ces paramètres :

La ressource bac à sable : 
    "id_ressource" : 1

La capsule Bac à sable : 
    "clef_pairform" : "11111111",
    "id_capsule" : 2

# Liste des modifs à faire lors d'une install clean du serveur, après installation des modules via npm :

## Bug LTI - bad signature
@ Description : Mismatch de l'url dans les headers et de celle utilisé pour calculer la signature oauth.
@  Problème trop localisé pour faire une pull-request, et trop petit pour faire un fork public
@ Fix : remplacer ce code manuellement dans ims-lti 2.1.5

* node_modules/ims-lti/lib/hmac-sha1.js :

    HMAC_SHA1.prototype.build_signature = function(req, body, consumer_secret, token) {
      var encrypted, hapiRawReq, hitUrl, originalUrl, parsedUrl, protocol, host;
      hapiRawReq = req.raw && req.raw.req;
      if (hapiRawReq) {
        req = hapiRawReq;
      }
      originalUrl = req.originalUrl || req.url;
      protocol = req.protocol || req.headers["x_forwarded_proto"];
      if (body.tool_consumer_info_product_family_code === 'canvas') {
        originalUrl = url.parse(originalUrl).pathname;
      }
      if (protocol === void 0) {
        encrypted = req.connection.encrypted;
        protocol = (encrypted && 'https') || 'http';
      }
      parsedUrl = url.parse(originalUrl, true);
      host = req.headers.host.match(/^localhost/) ? req.headers["x-forwarded-host"] || req.headers["x-forwarded-server"] : req.headers.host;
      // hitUrl = protocol + '://' + host + parsedUrl.pathname;
      // HACK FIX TODO (Maen Juganaikloo CAPE) - 
      // Mismatch de l'url dans les headers et de celle utilisé pour calculer la signature oauth
      // a cause du reverse proxy d'apache, qui remplace headers.host par localhost:3030
      // Impossible de reconstruire l'url de façon clean, à cause du "node/" qu'on ne peut avoir que 
      // dans un fichier de config, et pas autre part. On peut récupérer le host forwardé par contre
      // Solution : coder en dur l'URL, ou la choper de la config :
      // hitUrl = "https://www.pairform.fr/node/lti";
      hitUrl = require("config").app.urls.serveur_node + parsedUrl.pathname;

      // console.log('hitUrl : ' + hitUrl);
      // console.log('req.headers["x_forwarded_proto"] : ' + req.headers["x_forwarded_proto"]);
      // console.log('req.headers["x-forwarded-host"] : ' + req.headers["x-forwarded-host"]);
      // console.log('req.headers["x-forwarded-server"] : ' + req.headers["x-forwarded-server"]);
      // console.log('originalUrl : ' + originalUrl);
      // console.log('parsedUrl.pathname : ' + parsedUrl);
      
      return this.build_signature_raw(hitUrl, parsedUrl, req.method, body, consumer_secret, token);
    };

