"use strict";

//Pour être sur et certain que le serveur node est bien dans le bon dossier 
if ("NODE_CWD" in process.env) {
	process.chdir(process.env["NODE_CWD"]);
}

var express = require("express"),
	express_session = require('express-session'),
	SessionStore = require('express-session-mysql'),
	compression = require("compression"),
	body_parser = require("body-parser"),
  cookie_parser = require("cookie-parser"),
		   cors = require("cors"),
method_override = require("method-override"),
 cookie_session = require("cookie-session"),
   errorhandler = require("errorhandler"),
less_middleware = require("less-middleware"),
	 		log = require("metalogger")(),
		 CONFIG = require("config"),
	   passport = require("passport"),
	    favicon = require('serve-favicon');

require("../../pairform_modules/authentification/passport")(passport);

//Initialisation
var pairform_init = require("../../init");

exports = module.exports;

exports.setup = function(app) {

	pairform_init.initialiser(function () {

		// var cors_options_reflect = {
		// 	origin: true,
		// 	credentials: true
		// };
		// Middleware pour Cross domain
		var root_dir = require("path").dirname(module.parent.filename);
		app.set("root_dir", root_dir);
		// Error.stackTraceLimit = Infinity;

		//NewRelic en production uniquement par défaut, ou si le flag PF_NEW_RELIC est là
		if(process.env.NODE_ENV == 'production' || process.env.PF_NEW_RELIC)
			require("newrelic");
		else
			//Mise en forme cool des erreur : couleurs et formatage
 			require('pretty-error').start();
		// Middleware pour Cross domain & cache
		app.use(function(req, res, next) {
			// res.setHeader('Access-Control-Allow-Credentials', 'true');		// permission de stocker et d'accèder aux infos (ex: cookies) du header des requetes 

			/*
				http://www.w3.org/TR/cors/ : 6.4 Implementation Considerations
				Resources that wish to enable themselves to be shared with multiple Origins but do not respond uniformly with "*" 
				must in practice generate the Access-Control-Allow-Origin header dynamically in response to every request they wish to allow. 
				As a consequence, authors of such resources should send a Vary: Origin HTTP header or provide other appropriate control directives 
				to prevent caching of such responses, which may be inaccurate if re-used across-origins.
			*/
			res.setHeader('Vary', 'Origin'); //the response was negotiated based on the requestors Origin header value.

			//J'enlève le X-Powered-By pour ne pas exposer qu'on utilise Express
	  		// res.removeHeader("X-Powered-By");

	  		//Dans le cas de Chrome :
	  		//https://code.google.com/p/chromium/issues/detail?id=453306
	  		//https://code.google.com/p/chromium/issues/detail?id=426089#makechanges
	  		//https://code.google.com/p/chromium/issues/detail?id=409090
			// Il ne faut pas cacher /app à cause des règles CORS qui restent en cache
			// Ni les jsons de langues et oudatedbrowser, qui ne passent pas par les méthodes angular de get
			// var ua = req.headers['user-agent'];

			// if (ua.match(/Chrome/i) && (req.originalUrl.match(/.*?(webServices)/) || req.originalUrl.match(/.*?(\/app)/) || req.originalUrl.match(/.*?(\/json\/)/) || req.originalUrl.match(/.*?(\/outdatedbrowser)/))) {
			// 	res.setHeader("ETag" , new Date().getTime());
			// }
			
			// par défaut, le réponse du serveur Node ne seront pas mises en cache sur les clients pour les webservices
			if(req.originalUrl.match(/.*?(webServices)/))
				res.setHeader('Cache-Control', 'no-store');					
			//no-store au lieu de no-cache : http://stackoverflow.com/a/24556761/1437016 ; http://securityevaluators.com/knowledge/case_studies/caching/
			
			//Interception des URLs malformées pour éviter d'envoyer une page d'erreur avec plein d'infos sur notre architecture dessus 
			//Essaie de décoder l'URL, et si ça marche pas, erreur 400 + page 400 pour l'utilisateur.
	    try {
	        decodeURIComponent(req.path)
	    }
	    catch(err) {
        if (err instanceof URIError || err instanceof TypeError) {
    			log.warning(err.message, req.path);
	        return res.status(400).render('400',{
						"url_serveur_node" : CONFIG.app.urls.serveur_node
					});
		    }
	    }
	    next();
		});

		// app.all("/injecteur*", cors(), function (req, res, next) {
		// 	console.log("Injecteur appellé : " + req.originalUrl);
		// 	next();
		// });

		// app.use(cors(cors_options_reflect));

		//Autorisation de toutes les requetes CORS
		// Resolution probleme CORS : wildcard par defaut pour tout, sauf pour les webServices. 
		// En cas de tentative d'utilisation des webservices sur un site 
		// dont le domaine n'est pas enregistre sur les serveurs PF 
		// (condition vraie quand le domaine heberge au moins une capsule), 
		// on affiche une erreur expliquant a l'utilisateur (internationalise).
		// app.use(cors({
		// 	methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS']
		// }));
		app.get("*", cors({
			methods: ['GET']
		}));


		app.use(compression());

		// Jade est le moteur de template des applications web PairForm
		app.set("view engine", "jade");

		//Prerender pour SEO
		app.use(require('prerender-node').set('prerenderServiceUrl', CONFIG.app.urls.prerender));

		// modification de la taille limite de certaines requetes
		app.use("/webServices/espace", body_parser({ limit: 1024 * 1024 }));						// route avec de l'upload d'image (logo)
		app.use("/webServices/ressource", body_parser({ limit: 1024 * 1024 }));						// route avec de l'upload d'image (logo)
		app.use("/webServices/upload/capsule", body_parser({ limit: 1024 * 1024 * 1024 }));			// route avec de l'upload de fichier (archives/fichiers des capsules)
		app.use("/webServices/upload/conversion", body_parser({ limit: 1024 * 1024 * 1024 }));		// route avec de l'upload de fichier (import de .docx, .odt, etc.)
		app.use("/webServices/groupe/listeMembres", body_parser({ limit: 1024 * 1024 * 1024 }));	// import d'une multitude d'e-mails

		app.use("/", body_parser());
		app.use(cookie_parser());
		app.use(method_override());
		app.use(express.query());


		// This is not needed if you handle static files with, say, Nginx (recommended in production!)
		// Additionally you should probably pre-compile your LESS stylesheets in production
		// Last, but not least: Express" default error handler is very useful in dev, but probably not in prod.
		// if (("NODE_SERVE_STATIC" in process.env) && process.env["NODE_SERVE_STATIC"] == 1) {
			var pub_dir = root_dir + CONFIG.app.pub_dir;

			app.use(less_middleware( pub_dir ));
			app.use("/public", express.static(pub_dir));
			app.use("/dist", express.static(root_dir+ "/dist"));
			app.use(errorhandler({ dumpExceptions: true, showStack: true }));
		// }

		// Redirection des clients pointant directement sur import.(min.)js vers app
		// pour templater le fichier avec underscore avant envoi au client
		app.get("/private/js/import*", function (req, res) {
			res.redirect(CONFIG.app.urls.serveur_node + "/app");
		});


		/**
		 * Redirection des URLs après supression /node
		 */

		app.get("/node*", function (req, res, next) {
			var _path = req.originalUrl.substr(5);
			return res.redirect(301, CONFIG.app.urls.serveur_node + _path);
		});

		/* Catch-all post, put, delete… pour qu'ils renvoient les données avec le code 307 */
		app.all("/node*", function (req, res, next) {
			var _path = req.originalUrl.substr(5);
			return res.redirect(307, CONFIG.app.urls.serveur_node + _path);
		});
		
		app.use("/home", express.static(root_dir + "/pairform_modules/home/private"));
		app.use("/private", express.static(root_dir + "/pairform_modules/backoffice/private"));
		app.use("/private", express.static(root_dir + "/pairform_modules/vitrine/private"));
		app.use("/private", express.static(root_dir + "/pairform_modules/app/private"));
		app.use("/app", express.static(root_dir + "/pairform_modules/app/private"));
		app.use("/private", express.static(root_dir + "/pairform_modules/injecteur/private"));
		app.use("/serveur", express.static(root_dir + "/pairform_modules/serveur/private"));
		app.use("/private", express.static(root_dir + "/pairform_modules/authentification/private"));
		app.use("/private", express.static(root_dir + "/pairform_modules/authentification/node_modules/lti/private"));
		app.use("/private", express.static(root_dir + "/pairform_modules/utils/compatibilite/private"));
		app.use("/private", express.static(root_dir + "/pairform_modules/utils/speed_test/private"));
		
		//Favicon
		app.use(favicon(root_dir + '/public/ico/favicon.png'));

		var SessionStore_options = CONFIG.app.bdd_mysql;

		// SessionStore_options.schema = {
		// 	tableName: "cape_sessions",
	 //        columnNames: {
	 //            session_id: 'id_session',
	 //            expires: 'date_expiration',
	 //            data: 'donnees_session'
	 //        }
		// };
		SessionStore_options.useConnectionPooling = true;
		SessionStore_options.expiration = 7 * 24 * 60 * 60 * 1000; //7 jours
		SessionStore_options.checkExpirationInterval = 60 * 1000; //Toutes les minutes

		var _sessionStore = new SessionStore(SessionStore_options);

		// on stock les sessions avec cookie_session et passport
		app.use(express_session({ 
			key: "session.pf",
			secret: CONFIG.app.sessions.secret,
			resave: true,
			saveUninitialized: true,
			store: _sessionStore
		}));

		app.use(passport.initialize());
		app.use(passport.session());

		// Variables pour pug
		app.use(function (req, res, next) {
		  res.locals.urls = CONFIG.app.urls;
		  res.locals.utilisateur_connecte = req.user;
		  next();
		});

		//Set du répertoire courant au niveau global pour éviter les chemins relatifs à rallonge
		global.root_dir = root_dir;
		//Set pairform_modules pour accession modules
		global.pairform_modules = root_dir + "/pairform_modules/";
		
		//---- Mounting application modules
		app.use(require("../../pairform_modules/vitrine"));
		app.use(require("../../pairform_modules/app"));
		app.use(require("../../pairform_modules/injecteur"));
		app.use(require("../../pairform_modules/utils/compatibilite"));
		require("../../pairform_modules/cron");
		require("../../pairform_modules/utils/speed_test")(app, passport);
		require("../../pairform_modules/authentification")(app, passport);
		require("../../pairform_modules/serveur")(app, passport);
		require("../../pairform_modules/backoffice")(app, passport);
		require("../../pairform_modules/webservices")(app, passport);
		require("../../pairform_modules/home")(app, passport);


		app.use(function notFoundHandler(req, res) {
			log.warning("404 - " + req.url +" - "+ req.headers.referer);
			
			//Si on demande une image
			if (req.url.match(/.png|.jpg|.gif/)) {
			    res.status(404);
				res.sendFile(root_dir + "/public/img/logo_defaut.png");
				return;
			}
			//Sinon, on fait par rapport au contenttype
			switch(req.accepts(["json", "html"])){
				case 'json':
			    	res.setHeader('Content-Type', 'application/json')
			    	res.status(404);
					res.send({ status:"ko", retour : "ko", message : '404' });
			      break
			    case 'html':
			    	res.setHeader('Content-Type', 'text/html')
					res.status(404);
					res.render('404',{
						"url_serveur_node" : CONFIG.app.urls.serveur_node
					});
			    	break
			    default:
			    	// the fallback is text/plain, so no need to specify it above
			    	res.setHeader('Content-Type', 'text/plain')
					res.status(404);
					// Au cas où, on renvoie du texte brut
					res.type('txt').send('404');
			    	break
			}
		});

		//--- End of Internal modules

		//-- ATTENTION: make sure errorHandler are the very last app.use() call
		//-- ATTENTION: and in the sequence they are in, or it won't work!!!
		
		// Catch-all error handler. Modify as you see fit, but don't overuse.
		// Throwing exceptions is not how we normally handle errors in Node.
		app.use(function catchAllErrorHandler(err, req, res, next) {
			// Emergency: means system is unusable
			log.emergency(err.stack, req.originalUrl);
			res.status(500).render('500',{
				"url_serveur_node" : CONFIG.app.urls.serveur_node
			});

			// We aren't in the business of hiding exceptions under the rug. It should
			// still crush the process. All we want is: to properly log the error before
			// that happens.
			//
			// Clustering code in the lib/clustering module will restart the crashed process.
			// Make sure to always run clustering in production!
			setTimeout(function() { // Give a chance for response to be sent, before killing the process
				process.exit(1);
			}, 10);
		});
	});
};
