
const url = require('url');
const WebSocket = require('ws');
const log = require('metalogger')();
var wss;
var cluster = require("cluster");
exports.setup = function (port_ws) {
  wss = new WebSocket.Server({ port: port_ws });
  log.debug("[WS] Cluster master :" ,cluster.isMaster )

  wss.on('connection', function connection(client, req) {
    const location = url.parse(req.url, true);
    client.isAlive = true;
    client.id_utilisateur = location.query.id_utilisateur;
    client.id_espace = location.query.id_espace;
    client.id_ressource = location.query.id_ressource;
    log.debug('[WS] Utilisateur connecté : ', client.id_utilisateur)
    // You might use location.query.access_token to authenticate or share sessions
    // or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)

    client.send('{"status":"connected"}', function ack(error) {
      // log.debug(error);
    });

    client.on('message', function incoming(body) {
      // Broadcast to everyone else.
      log.debug('[WS] Message reçu : ', body);
      body = JSON.parse(body);
      wss.clients.forEach(function each(other_client) {
        // log.debug('[WS] Client  : ', other_client.id_utilisateur);
        if (other_client.id_utilisateur !== client.id_utilisateur && client.readyState === WebSocket.OPEN) {
          if (body.id_message) {
            var message = body;
            if (other_client.id_ressource == message.id_ressource) {
              // log.debug('[WS] Client reçoit message : ', other_client.id_utilisateur);
              other_client.send(JSON.stringify(message));
            }
          }
          else if (body.id_utilisateur) {

            var utilisateur = body;
            // log.debug('[WS] Utilisateur connecté : ', utilisateur);
            if ((other_client.id_espace == utilisateur.id_espace) && (other_client.id_ressource == utilisateur.id_ressource)) {
              // log.debug('[WS] Client reçoit notification utilisateur : ', other_client.id_utilisateur);
              other_client.send(JSON.stringify(utilisateur));
            }
          }
        }
      });
    });
    client.on('error', function error (err) {
      if (err.code != 'ECONNRESET')
        log.error('Erreur : ', err);
    });

  });
  return wss;
}

exports.wss = wss;