// Define third-party libraries
var util = require('util'),
 cluster = require('cluster'),
 express = require('express'),
	 app = express(),
	http = require('http'),
 // https = require('https'),
	   _ = require('underscore'),
  CONFIG = require('config'),
	  fs = require('fs'),
	 log = require('metalogger')();

require('./server_modules/server').setup(app);


var isClusterMaster = (cluster.isMaster && (process.env.NODE_CLUSTERED == 1));
var is_http_thread = true;
var server, wss;

if (isClusterMaster ||
	( 'undefined' !== typeof process.env.NODE_ISNOT_HTTP_SERVER_THREAD &&
		process.env.NODE_ISNOT_HTTP_SERVER_THREAD != 'true')) {
	
	is_http_thread = false;
}

log.debug("is http thread? " + is_http_thread);

if (isClusterMaster) {
	require('./server_modules/clustering').setup();
}

// options SSL
// var options_https = {
// 	key: fs.readFileSync(CONFIG.app.options_https.key),			// clef privé
// 	cert: fs.readFileSync(CONFIG.app.options_https.cert)		// certificat
// };

// Création du serveur WS uniquement sur le cluster master
// Ou si on est pas sur un cluster
if (isClusterMaster || process.env.NODE_CLUSTERED == 0){
	wss = require('./server_modules/server/ws').setup(CONFIG.app.port_ws);
}
if (is_http_thread) {
	// lancement du server node en http
	server = http.createServer(app);
	//Lancement du serveur WS sur le cluster master, ou sur le seul cluster si on est pas en mode cluster
	server.listen(CONFIG.app.port);
	// lancement du server node en https
	// https.createServer(options_https, app).listen(CONFIG.app.port);
}

// If we are not running a cluster at all:
if (!isClusterMaster && cluster.isMaster) {
	log.notice("Express server instance listening on port " + CONFIG.app.port);
}

// Kill de node et ws
for (var sig of ['SIGINT', 'SIGTERM']) {
  process.on(sig, function () {

    // Stop accepting new connections.
    server && server.close(function () {
      if(wss){
        wss.close();
        // Close all clients gracefully.
        for (var ws of wss.clients) 
          ws.close();
      }
    });
  });
}