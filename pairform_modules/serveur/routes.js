var web_services = require("../webservices/lib/webServices"),
		  CONFIG = require("config"),
		  	  fs = require('fs'),
		  	  log = require('metalogger')(),
		  	path = require('path'),
      pf_crypto = require(global.pairform_modules + "utils/pf_crypto"),
		  	api_rs = require('../webservices/lib/applications/api_rs'),
		   async = require('async'),
	  constantes = require("../webservices/lib/constantes"),
	pairform_dao = require("../webservices/node_modules/pairform_dao/lib/pairformDAO");


module.exports = function(app, passport) {

	/*
	 * Traitement des erreurs (peut être appellé par Apache pour afficher des pages d'erreurs uniques)
	 */
	app.get("/404", function (req, res) {		
		res.render("404",{
			"url_serveur_node" : CONFIG.app.urls.serveur_node
		});
	});

	app.get("/500", function (req, res) {		
		res.render("500",{
			"url_serveur_node" : CONFIG.app.urls.serveur_node
		});
	});

	/* Suppression de toutes les notifications */
	app.get("/notifications/all/:hash", function (req, res) {		
		var hash = req.param("hash"),
				id_utilisateur = pf_crypto.decrypt(hash);
		web_services.utilisateur.changerToutesNotifications(
			{
				id_utilisateur: id_utilisateur,
				value: 0
			},
			function(retour) {
				log.debug(retour);
				res.render("remove_notifications",{
					"url_serveur_node" : CONFIG.app.urls.serveur_node,
					"success" : retour.status
				});
			}
		);
	});


	/*
	 * Gestion de CGU
	 */
	app.get("/cgu/mobile/:code_langue*?", function (req, res) {		
		// var code_langue = ["fr","en","es"].indexOf(req.params.code_langue) && req.params.code_langue ? req.params.code_langue : "fr",
		var code_langue = "fr",
			array_json = [];

		array_json.push("utilisateur_" + code_langue);
		array_json.push("reseau_social_" + code_langue);
		array_json.push("confidentialite_" + code_langue);

		envoyerCGU(array_json, res);
	});

	app.get("/cgu/auteur/:code_langue*?", function (req, res) {		
		// var code_langue = ["fr","en","es"].indexOf(req.params.code_langue) && req.params.code_langue ? req.params.code_langue : "fr",
		var code_langue = "fr",
			nom_json = "auteur_" + code_langue;
		
		envoyerCGU([nom_json], res);
	});

	app.get("/cgu/reseau_social/:code_langue*?", function (req, res) {		
		// var code_langue = ["fr","en","es"].indexOf(req.params.code_langue) && req.params.code_langue ? req.params.code_langue : "fr",
		var code_langue = "fr",
			nom_json_rs = "reseau_social_" + code_langue,
			nom_json_confidentialite = "confidentialite_" + code_langue;
		
		envoyerCGU([nom_json_rs, nom_json_confidentialite], res);
	});

	app.get("/cgu/utilisateur/:code_langue*?", function (req, res) {		
		// var code_langue = ["fr","en","es"].indexOf(req.params.code_langue) && req.params.code_langue ? req.params.code_langue : "fr",
		var code_langue = "fr",
			nom_json = "utilisateur_" + code_langue;
		
		envoyerCGU([nom_json], res);
	});


	/*
	 * distribution des fichiers (pages) des capsules
	 */
	app.get("/doc/:id_espace-:id_ressource-:id_capsule*", function (req, res) {
		var nouvelle_url = CONFIG.app.urls.serveur_node + 
			"/doc/" + req.params.id_espace + 
			"/" + req.params.id_ressource + 
			"/" + req.params.id_capsule + 
			req.param(0);

		res.redirect(nouvelle_url);
	});

	app.get("/doc/:id_espace", function (req, res) {		
		var id_espace = parseInt(req.params.id_espace),
			email_utilisateur = req.isAuthenticated() ? req.user.email : "",
			id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000,
			id_langue = req.params.id_langue || 3;

		//Protection anti-petit malin
		if (!id_espace){
			return res.render("404",{
				"url_serveur_node" : CONFIG.app.urls.serveur_node
			});
		}

		//On check l'état de la capsule
		pairform_dao.getConnexionBDD(function (_connexion_courante){
			async.parallel({
				espace : function (callback_parallel) {
					pairform_dao.espace_dao.selectEspace(id_espace, function callback_success (espace) {
						if(!espace)
							callback_parallel({"message" : "espace n°" + id_espace + " inexistant"});
						else
							callback_parallel(null, espace);
					}, callback_parallel);
				},
				ressources : function (callback_parallel) {
					var liste_espaces = [{id_espace : id_espace}];
					pairform_dao.ressource_dao.selectRessourcesWithVisiblityByEspaces(liste_espaces, email_utilisateur, id_langue, function callback_success (retour_sql) {
						var ressources = filtreRessources(retour_sql, req.user);
						callback_parallel(null, ressources);
					}, callback_parallel);
				},
				nombre_messages_ressources : function (callback_parallel) {
					//TODO : recuperer les nombres des ressources qui nous interessent seulement
					pairform_dao.message_dao.selectNombreAllRessources(null, [1,2,3], function callback_success (nombre_messages_ressources) {
						//Transformation du tableau des ressources en 
						var nombre_messages_objet = {};
						for (var i = 0; i < nombre_messages_ressources.length; i++) {
							nombre_messages_objet[nombre_messages_ressources[i]['id_ressource']] = nombre_messages_ressources[i]['count'];
						};
						callback_parallel(null, nombre_messages_objet);
					}, callback_parallel);
				}
			}, function callback_final (err, resultat) {
				if (err) {
					log.error(JSON.stringify(err, null, 2));
					res.render('404', {
						"url_serveur_node" : CONFIG.app.urls.serveur_node
					});
				}
				else{
					pairform_dao.libererConnexionBDD(_connexion_courante);

					res.render(__dirname + "/views/espace", {
						"url_serveur_node" : CONFIG.app.urls.serveur_node,
						"url_serveur_ws" : CONFIG.app.urls.ws,
						"espace" : resultat.espace,
						"ressources" : resultat.ressources,
						"nombre_messages_ressources" : resultat.nombre_messages_ressources
					});

					api_rs.updateLastTimeActive(id_utilisateur);
				}
			});
		});
	});
	
	app.get("/doc/:id_espace/:id_ressource", function (req, res) {
		var id_espace = parseInt(req.params.id_espace),
			id_ressource = parseInt(req.params.id_ressource),
			email_utilisateur = req.isAuthenticated() ? req.user.email : "",
			id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000,
			id_langue = req.params.id_langue || 3;
		
		//Protection anti-petit malin
		if (!id_espace || !id_ressource){
			return res.render("404",{
				"url_serveur_node" : CONFIG.app.urls.serveur_node
			});
		}
		//On check l'état de la capsule
		pairform_dao.getConnexionBDD(function (_connexion_courante){
			async.parallel({
				espace : function (callback_parallel) {
					pairform_dao.espace_dao.selectEspace(id_espace, function callback_success (espace) {
						if(!espace)
							callback_parallel({"message" : "espace n°" + id_espace + " inexistant"});
						else
							callback_parallel(null, espace);
					}, callback_parallel);
				},
				ressources : function (callback_parallel) {
					var liste_espaces = [{id_espace : id_espace}];

					pairform_dao.ressource_dao.selectRessourcesWithVisiblityByEspaces(liste_espaces, email_utilisateur, id_langue, function callback_success (retour_sql) {
						var ressources = filtreRessources(retour_sql, req.user);
						callback_parallel(null, ressources);
					}, callback_parallel);
				},
				nombre_messages_ressources : function (callback_parallel) {
					//TODO : recuperer les nombres des ressources qui nous interessent seulement
					pairform_dao.message_dao.selectNombreAllRessources(id_ressource, [1,2,3], function callback_success (nombre_messages_ressources) {
						//Transformation du tableau des ressources en 
						var nombre_messages_objet = {};
						for (var i = 0; i < nombre_messages_ressources.length; i++) {
							nombre_messages_objet[nombre_messages_ressources[i]['id_ressource']] = nombre_messages_ressources[i]['count'];
						};
						callback_parallel(null, nombre_messages_objet);
					}, callback_parallel);
				},
				ressource_selectionne : function (callback_parallel) {
					pairform_dao.ressource_dao.selectRessource(id_ressource, function callback_success (ressource) {

						if(!ressource)
							callback_parallel({"message" : "ressource n°" + id_ressource + " inexistant"});
						else
							callback_parallel(null, ressource);
					}, callback_parallel);
				},
				capsules : function (callback_parallel) {
					//Récupération des capsules en fontion de leur visibilité
					pairform_dao.capsule_dao.selectCapsulesByRessource(id_ressource, email_utilisateur, function callback_success (retour_sql) {
						callback_parallel(null, retour_sql);
					}, callback_parallel);
				},
				nombre_messages_capsules : function (callback_parallel) {
					//TODO : recuperer les nombres des capsules qui nous interessent seulement
					pairform_dao.message_dao.selectNombreAllRessourcesByCapsules(null, [1,2,3], function callback_success (nombre_messages_capsules) {
						//Transformation du tableau des ressources en 
						var nombre_messages_objet = {};
						for (var i = 0; i < nombre_messages_capsules.length; i++) {
							nombre_messages_objet[nombre_messages_capsules[i]['id_capsule']] = nombre_messages_capsules[i]['count'];
						};
						callback_parallel(null, nombre_messages_objet);
					}, callback_parallel);
				},
				messages_ressource : function (callback_parallel) {
					//TODO : recuperer les nombres des capsules qui nous interessent seulement
					pairform_dao.message_dao.selectAllMessagesOfRessource(id_utilisateur, id_ressource, function callback_success (messages) {
						//Transformation du tableau des ressources en 
						callback_parallel(null, messages);
					}, callback_parallel);
				},
				utilisateurs : function (callback_parallel) {
					//TODO : recuperer les nombres des capsules qui nous interessent seulement
					pairform_dao.utilisateur_dao.selectUtilisateursOfRessource(id_ressource, function callback_success (utilisateurs) {
						//Transformation du tableau des ressources en 
						callback_parallel(null, utilisateurs);
					}, callback_parallel);
				}
			}, function callback_final (err, resultat) {
				
				if (err) {
					log.error(JSON.stringify(err, null, 2));
					res.render('404', {
						"url_serveur_node" : CONFIG.app.urls.serveur_node
					});
				}
				else{
					pairform_dao.libererConnexionBDD(_connexion_courante);

					res.render(__dirname + "/views/espace", {
						"url_serveur_node" : CONFIG.app.urls.serveur_node,
						"url_serveur_ws" : CONFIG.app.urls.ws,
						"espace" : resultat.espace,
						"ressources" : resultat.ressources,
						"nombre_messages_ressources" : resultat.nombre_messages_ressources,
						"ressource_selectionne" : resultat.ressource_selectionne,
						"messages" : resultat.messages_ressource,
						"capsules" : resultat.capsules,
						"utilisateurs" : resultat.utilisateurs,
						"nombre_messages_capsules" : resultat.nombre_messages_capsules,
	    			types_mime : constantes.MESSAGE_PJ_TYPES_MIME
					});

					api_rs.updateLastTimeActive(id_utilisateur);
				}
			});
		});
	});

	//TODO: Gerer les noms de fichiers avec plusieurs points (ex : test.min.js) en fin d'URL
	// -> Node ne dessert pas la route dans ce cas, 404.
	// -> OK
	//Gestion des documents privés
	//Url de forme http://imedia.emn.fr:3000/doc/1-12-980/Graphe/web/co/Accueil.html, avec tout ce qui suit id_capsule optionnel
	//http://imedia.emn.fr:3000/doc/1/7/10/co/Accueil.html
	app.get("/doc/:id_espace/:id_ressource/:id_capsule*", function (req, res, next) {
		//Récuperation des paramètres contenus dans l'URL
		var url_of_ressource = req.param(0),
			url_to_follow = req.url,
			id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000,
			id_espace = parseInt(req.params.id_espace),
			id_capsule = parseInt(req.params.id_capsule),
			id_ressource = parseInt(req.params.id_ressource);

		//Protection anti-petit malin
		if (!id_espace || !id_ressource || !id_capsule){
			return res.render("404",{
				"url_serveur_node" : CONFIG.app.urls.serveur_node
			});
		}

		//S'il n'y a pas de paramètre du tout, on redirige vers la mêm e route, 
		// avec un slash en plus 
		// Attention : url_of_ressource est nul, tant qu'il n'y a pas 2 slashs
		// On teste donc qu'il n'y a pas de ressource, et que le dernier caractère
		// de l'url n'est pas un slash
		if (!url_of_ressource) {
			var path_to_index = (req.originalUrl.substr(-1) != "/" ? "/" : "") + "web/index.html" ;
			//S'il n'y a pas d'url de ressource, on met un / pour aller chercher le bon fichier
			return res.redirect(CONFIG.app.urls.serveur_node + req.originalUrl + path_to_index);
		} 
		// Protection au cas ou il n'y a pas le trailing slash
		else if (url_of_ressource == "/web") {
			var path_to_index = (req.originalUrl.substr(-1) != "/" ? "/" : "") + "index.html" ;
			//S'il n'y a pas d'url de ressource, on met un / pour aller chercher le bon fichier
			return res.redirect(CONFIG.app.urls.serveur_node + req.originalUrl + path_to_index);
		}

		//Refuser l'accès à ce fichier, qui est chargé par Scenari lorsque l'user agent
		//reflète un terminal mobile. Ce JS empêche les panneaux PF d'être scrollables sur mobiles
		// if (url_of_ressource.match(/iscroll.js/) || url_of_ressource.match(/dynSizes.js/)) {
		// if (url_of_ressource.match(/iscroll.js/)) {
		// 	return res.sendFile(path.resolve(__dirname + "/../../public/js/custom_iScroll.js"));
		// }
		if (url_of_ressource.match(/tplMgr.js/)) {
			return res.sendFile(path.resolve(__dirname + "/../../public/js/tplMgr_noTouchDetection.js"));
		}

		//On ne vérifie l'accessibilité que pour les ressources HTML
		if (!url_of_ressource.match(/\.html/)) {
			//Pour les autres, on les envoi sans contrôle
			return sendRequestedFile(res, id_capsule, url_of_ressource);
		}

		//On check l'état de la capsule
		pairform_dao.capsule_dao.selectCapsuleVisibiliteParNomGroupe(
			id_capsule,
			constantes.GROUPE_SPHERE_PUBLIQUE,
			function callback_success (retour_sql) {

				//Capsule inexistante ?
				if (!retour_sql.length) {
					//On envoie une 404
					return send_404(res);
				};

				//Redirection du document si l'URL appelée n'est pas la bonne
				//Dans le cas où les documents sont déplacés, les id_ressource & id_espace peuvent être faux
				//On récupère l'URL stockée en base de donnée pour comparer, et si c'est mauvais, on redirige
				var url_web = retour_sql[0]["url_web"]; // "http://localhost:3000/doc/2/2/1/web/"
				var full_url = CONFIG.app.urls.serveur_node + req.url; // "http://localhost:3000" + "/doc/1/1/1/web/co/BaS_1.html"
				var matches_url_bdd = full_url.match(url_web) // null ||  http://localhost:3000/doc/2/2/1/web/

				if(!matches_url_bdd){
					//On substitue la fin de l'URL pour la reconstruire proprement
					var new_url = url_web + req.param(0).replace('\/(web)\/|(\/mob)',''); 
					// Exemples :
					//  "http://localhost:3000/doc/2/2/1/web/" + ("/web/co/BaS_1.html" - "/web/")
					//  "http://localhost:3000/doc/2/2/1/mob" + ("/mob.zip" - "/mob")

					//https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP#Redirection
					//http://stackoverflow.com/questions/14144664/whats-the-deal-with-http-status-code-308
					//http://insanecoding.blogspot.fr/2014/02/http-308-incompetence-expected.html
					return res.status(301).redirect(new_url);
				}

				var id_visibilite = retour_sql[0]["id_visibilite"];
				//Si la capsule est visible
				if(id_visibilite == constantes.VISIBILITE_ACCESSIBLE){
					//Rock'n'roule
					return sendRequestedFile(res, id_capsule, url_of_ressource);
				}
				//Si on est là, on est déjà en train d'essayer à une ressource privée
				//Si la ressource demandée n'est pas privée, on a rien à faire ici, malgré le fait que ça va marcher.
				//Si l'utilisateur est déjà connecté, 
				//ou qu'il est en train de se connecter via LTI avec un token
				else if (req.isAuthenticated() || req.query.token) {
					//Donc, dans le cas d'une connexion LTI
					if (req.query.token) {
						//Patch pour bearerStrategy
						req.body.access_token = req.query.access_token || req.query.token;

						passport.authenticate("login_LTI_client", function(err, user, info) {
							if (user && !err && !info) {
								verifierAccessibiliteUtilisateurACapsule(user, id_capsule, function callback_success () {
									// Envoi du fichier demandé
									sendRequestedFile(res, id_capsule, url_of_ressource);
								}, function callback_fail(){
									//Sinon, on l'envoie vers la page de login (en français par défaut), en le notifiant qu'il n'est pas autorisé (dernier param)
									redirigerVersLogin(res, url_to_follow, id_ressource, id_capsule, 3, true);
								});
							}
							else
								redirigerVersLogin(res, url_to_follow, id_ressource, id_capsule, 3, true);
							
						})(req, res, next);
					}
					//Sinon, c'est qu'on est connecté
					else{
						verifierAccessibiliteUtilisateurACapsule(req.user, id_capsule, function callback_success () {
							// Envoi du fichier demandé
							sendRequestedFile(res, id_capsule, url_of_ressource);
						}, function callback_fail(unauthorized_explication){
							//Sinon, on l'envoie vers la page de login (en français par défaut), en le notifiant qu'il n'est pas autorisé (dernier param)
							redirigerVersLogin(res, url_to_follow, id_ressource, id_capsule, 3, true, unauthorized_explication);
						});
					}
				} else {
					//Sinon, on l'envoie vers la page de login (en français par défaut)
					redirigerVersLogin(res, url_to_follow, id_ressource, id_capsule, 3, false);
				}	

				api_rs.updateLastTimeActive(id_utilisateur);

			},
			function callback_error (error) {
				log.error(error);
				//On envoie une 404
				return send_404(res);
			}
		)
		
	});
	/*  */
	app.get("/serveur/partial/:nom_view", function (req, res) {
		res.render(__dirname + "/views/" + req.params.nom_view, {
			"url_serveur_node" : CONFIG.app.urls.serveur_node
		});
	});

	/*
	 * distribution des fichiers (images, PJ, logos, ...) éditablent par les utilisateurs  
	 */
	app.get("/res/pieces_jointes/:piece_jointe", function (req, res) {		
		envoyerFichierDansRepertoire(res, req.params.piece_jointe, constantes.REPERTOIRE_PIECES_JOINTES);
	});
	app.get("/res/avatars/:avatar", function (req, res) {		
		envoyerFichierDansRepertoire(res, req.params.avatar, constantes.REPERTOIRE_AVATARS);
	});
	app.get("/res/logos_ressources/:logo_ressource", function (req, res) {		
		envoyerFichierDansRepertoire(res, req.params.logo_ressource, constantes.REPERTOIRE_LOGOS_RESSOURCES);
	});
	app.get("/res/logos_espaces/:logo_espace", function (req, res) {		
		envoyerFichierDansRepertoire(res, req.params.logo_espace, constantes.REPERTOIRE_LOGOS_ESPACES);
	});
	app.get("/res/logos_open_badges/:logo_open_badge", function (req, res) {		
		envoyerFichierDansRepertoire(res, req.params.logo_open_badge, constantes.REPERTOIRE_LOGOS_OPEN_BADGES);
	});


	/*
	 * distribution des fichiers JSON associés aux open-badges
	 */
	app.get("/openBadges/:repertoire/:fichier_json", function (req, res) {		
		var root_dir = path.dirname(require.main.filename),
			nom_fichier_json = req.params.fichier_json + (req.params.fichier_json.match(/\.json$/i) ? "" : ".json"),
			url_fichier_json = root_dir +"/public/json/openBadges/"+ req.params.repertoire + nom_fichier_json ;

		if (fs.existsSync(url_fichier_json)) {
			fs.readFile(url_fichier_json, 'utf8', function (err, datas) {
				if (err)
					done(err);
				else
					res.json(JSON.parse(datas));
			});
		} else {
			send_404(res);
		}
	});


	function verifierAccessibiliteUtilisateurACapsule(utilisateur, id_capsule, callback_success, callback_fail) {
		//Si l'utilisateur n'a pas validé son adresse e-mail, il n'a de toute façon pas le droit
		if (!utilisateur.email_est_valide) {
			return callback_fail("email_non_valide");
		}
		//On vérifie son accès à la capsule demandée
		web_services.capsule.getAutorisationAcces(
			id_capsule,
			utilisateur.email,
			function(autorisation) {
				// S'il est autorisé (au moins une autorisation trouvée dans le retour SQL) || admin pairform
				if (autorisation.length || utilisateur.est_admin_pairform) {
					callback_success();

				} else {
					callback_fail();
				}
			}
		);

	}

	//Pour envoyer les fichiers uploadés par les utilisateurs
	function envoyerFichierDansRepertoire (res, ressource, repertoire) {
		var url_of_ressource = repertoire + ressource;

		if (fs.existsSync(url_of_ressource))
			res.sendFile(url_of_ressource);
		else{
			if (ressource.match(/blur/i))
				res.sendFile(repertoire + "defaut.png.blur.jpg");
			else
				res.sendFile(repertoire + "defaut.png");
		}
	}

	function sendRequestedFile (res, id_capsule, url_of_ressource) {
		// Exemple : /Volumes/iMedia_Data/ServiceData/Web/Documents/PairForm_data/capsules/ID_CAPSULE/mob || web/path/to/res 
		var path_to_res = constantes.REPERTOIRE_CAPSULE + "/" + id_capsule + url_of_ressource;


		// On essaie d'append index.html derriere s'il n'y avait pas de fichier spécifié (par exemple, nom_doc/web/)
		// Modification de la regex pour permettre les numéro dans le filetype du fichier, et les caractères spéciaux français dans le nom du fichier
		// On récupère le dernier path component en prenant l'index du dernier "/",
		// puis tout ce qui suit derrière "**.*", sans le slash (d'où l'index +1) 
		// Et on check si c'est un fichier : un point dans ce composant?
		// S'il y a quelque chose, on a donc un last path component
		// Sinon, on essaie de choper index.html dans le dosiser spécifié
		// if (path_to_res.substr(path_to_res.lastIndexOf("/") + 1).indexOf(".") === -1)
		// 	path_to_res += path_to_res.substr(-1) == "/" ? "index.html" : "/index.html";
		
		// log.debug(path_to_res);
		//On envoie la page demandée
		if (fs.existsSync(path_to_res))
			res.sendFile(path_to_res);
		else{
			send_404(res);
		}
	}

	function send_404 (res) {
		res.status(404).render("404", {
			"url_serveur_node" : CONFIG.app.urls.serveur_node
		});
	}

	function redirigerVersLogin (res, url_to_follow, id_ressource, id_capsule, id_langue, unauthorized, unauthorized_explication) {
		//On récupère quelques infos sur la ressource concernée
		pairform_dao.getConnexionBDD(function (_connexion_courante){
			pairform_dao.ressource_dao.selectRessourceForWeb(
				id_ressource,
				id_langue,
				function (ressource) {
					pairform_dao.libererConnexionBDD(_connexion_courante);
					//Si la ressource concernée n'existe pas
					if (!ressource) {
						//On envoie une 404
						return send_404(res);
					}
					//Sinon, on envoie la page jade login avec les infos de la ressource
					res.render(__dirname + "/../authentification/views/gate_keeper",{
						url_to_follow : url_to_follow,
						id_ressource : id_ressource,
						id_capsule : id_capsule,
						espace_nom_long : ressource["espace_nom_long"],
						url_logo : CONFIG.app.urls.serveur_node + "/" + ressource["url_logo"],
						url_logo_espace : ressource["url_logo_espace"] ? CONFIG.app.urls.serveur_node + "/" + ressource["url_logo_espace"] : CONFIG.app.urls.serveur_node + '/public/img/logo.png',
						unauthorized : unauthorized,
						unauthorized_explication : unauthorized_explication,
						url_serveur_node : CONFIG.app.urls.serveur_node
					});
				},
				//S'il y a une erreur pendant la requete
				function () {
					//On envoie une 404
					return send_404(res);
				}
			);
		});
	}

	//Fonction pour aller chercher le ou les JSON de traduction dans le bon dossier, et les envoyer
	//via res
	//@param noms_json : tableau de noms avec suffixe code de langue (ex : auteur_fr)
	function envoyerCGU (array_noms_json, res) {
		var array_cgus = [];

		async.each(array_noms_json, function readFile (nom_json, done) {

			var root_dir = path.dirname(require.main.filename),
				path_to_json = root_dir + "/public/json/cgu/" + nom_json + (nom_json.match(/\.json$/i) ? "" : ".json");

			fs.readFile(path_to_json, 'utf8', function (err, data) {
				if (err) {
					done(err);
				}
				else{
					var cgu = JSON.parse(data);
					array_cgus.push(cgu);
					done(null);
				}
			});

		}, function sendCGU (err) {
			if(err){
				log.error(err);
				res.render("404",{
					"url_serveur_node" : CONFIG.app.urls.serveur_node
				});
			}
			else{
				res.render("CGU_template",{
					"array_cgus" : array_cgus,
					"url_serveur_node" : CONFIG.app.urls.serveur_node
				});
			}
		});
	}

	function checkUrlMatchFromRequest (req, url_to_match) {
		//Parfois, l'url est undefined : à checker rapidement
		url_to_match = url_to_match || '';
		var dns = req.get('origin') || null;
		var referer = req.get('referer') ? req.get('referer').replace(/https?:\/\//,"") : "";
		var url = url_to_match.replace(/https?:\/\//,"");

		//Test si les noms de domaines correspondent
		if(dns && !url.match(new RegExp(dns, "i")))
			return false;
		//Test si l'URL déclaré en BDD se retrouve dans le referer
		if(!referer.match(new RegExp(url, "i")))
			return false;

		//Sinon, tutto bene
		return true;

	}

	function filtreRessources(ressources, utilisateur) {
		return ressources.filter(function (ressource) {
			// log.debug(ressource)
			//Si la capsule est visible
			if (ressource.visible){
				//Elle passe
				return true;
			}
			//Sinon, on passe une suite de tests
			else {
				//S'il y a pas d'utilisateur
				if (!utilisateur) {
					return false;
				}
				//Si on est administrateur 
				else if (utilisateur.est_admin_pairform) {
					return true;
				}
				//Si l'utilisateur a au moins un rôle d'administration sur cet espace
				else if (utilisateur.liste_roles && utilisateur.liste_roles[ressource.id_espace]){
					
					//Si l'utilisateur a la gestion de tous les droits
					if(utilisateur.liste_roles[ressource.id_espace].gerer_ressources_et_capsules){
						return true;
					}
					//Si l'utilisateur a un rôle d'administration sur cette ressource
					else if(utilisateur.liste_roles[ressource.id_espace][ressource.id_ressource]) {
						return true;
					}
				}
				//Si l'utilisateur fait parti d'une session lié à cette ressource
				if (utilisateur.sessions && utilisateur.sessions[ressource.id_ressource]) {
					return true;
				}
				else
					return false;
			}
		})
	}
}