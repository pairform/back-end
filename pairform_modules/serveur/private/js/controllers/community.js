pf_app.controller("community", ["$http", "$scope", "$translate", function ($http, $scope, $translate) {
  $scope.users = {
    administrateurs: [],
    experts: [],
    learners: []
  }

  $scope.determinerDerniereConnexion = function  (user) {
    var lb_st = user.labelStatus;
    var t_off = Math.floor((Math.floor(new Date().getTime()/1000) - user.offline) / 60); // en minute, le temps d'inactivité
    $translate(['WEB_LABEL_EN_LIGNE', 'WEB_LABEL_INACTIF_DEPUIS', 'WEB_LABEL_MINUTES', 'WEB_LABEL_HEURES', 'WEB_LABEL_JOURS', 'WEB_LABEL_MOIS']).then(function (translation_object) {
      lb_st = translation_object['WEB_LABEL_EN_LIGNE'];
      if (t_off != false && t_off > 5) {
        user.online = false;
        if (t_off < 60) {
          t_off = Math.round(t_off);
          lb_st = translation_object['WEB_LABEL_INACTIF_DEPUIS'] + ' ' + t_off + ' ' +translation_object['WEB_LABEL_MINUTES'];
        }
        else if((t_off > 60) && (t_off < 1440)){
          t_off = Math.round(t_off / 60);
          lb_st = translation_object['WEB_LABEL_INACTIF_DEPUIS'] + ' ' + t_off + ' ' +translation_object['WEB_LABEL_HEURES'];
        }
        else if((t_off > 1440) && (t_off < 43200)){
          t_off = Math.round(t_off / 1440);
          lb_st = translation_object['WEB_LABEL_INACTIF_DEPUIS'] + ' ' + t_off + ' ' +translation_object['WEB_LABEL_JOURS'];
        }
        else if((t_off > 43200) && (t_off < 20000000)){
          t_off = Math.round(t_off / 43200);
          lb_st = translation_object['WEB_LABEL_INACTIF_DEPUIS'] + ' ' + t_off + ' ' +translation_object['WEB_LABEL_MOIS'];
        }
        else if (t_off > 20000000) {
          lb_st = translation_object['WEB_LABEL_INACTIF'];
        }
      }
      else 
        user.online = true;
      
      user.time_offline = t_off;
      user.labelStatus = lb_st;
    });
  }

  for (var i = PF.utilisateurs.length - 1; i >= 0; i--) {
    var user = PF.utilisateurs[i];
    $scope.determinerDerniereConnexion(user);
    if (user.id_categorie >= 5)
      $scope.users.administrateurs.push(user);
    else if (user.id_categorie == 4)
      $scope.users.experts.push(user);
    else
      $scope.users.learners.push(user);
  };

  // $scope.users.experts = [
  //   {
  //     pseudo: "Michel",
  //     id_utilisateur: 3,
  //     avatar_url : "http://127.0.0.1:3000/res/avatars/3.png"
  //   },
  //   {
  //     pseudo: "Frank",
  //     id_utilisateur: 2,
  //     avatar_url : "http://127.0.0.1:3000/res/avatars/2.png"
  //   }
  // ]

  // $scope.users.learners = [
  //   {
  //     pseudo: "PairForm",
  //     id_utilisateur: 1,
  //     avatar_url : "http://127.0.0.1:3000/res/avatars/1.png"
  //   },
  //   {
  //     pseudo: "Chanter",
  //     id_utilisateur: 15,
  //     avatar_url : "http://127.0.0.1:3000/res/avatars/15.png"
  //   },
  //   {
  //     pseudo: "Porlar",
  //     id_utilisateur: 12,
  //     avatar_url : "http://127.0.0.1:3000/res/avatars/12.png"
  //   },
  //   {
  //     pseudo: "Vulve",
  //     id_utilisateur: 8,
  //     avatar_url : "http://127.0.0.1:3000/res/avatars/8.png"
  //   }
  // ]
  //$http.get();

}]);