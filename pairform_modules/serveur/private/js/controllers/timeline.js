pf_app.controller('timeline', ["$scope", "toaster", "$http", "$timeout", function ($scope, toaster, $http, $timeout) {
  var defaults = {
  };
  $scope.moment = moment;
  angular.extend($scope, defaults);

  $scope.init = function () {
    var _params = [
      $scope.ressource.id_ressource
    ]
    // récupération des timelines
    $http.get(PF.globals.url.root + "webServices/session/ressource/"+ _params.join("/"))
    .success(function(retour_json){
      if (retour_json.status === "ok") {
        $scope.sessions = retour_json.data.sessions;
      } else {
        // toaster.pop("error", "");
      }
    })
    .error(function(){
      console.log("erreur requete http get /webServices/session/ressource/:id_espace/:id_ressource");
      // toaster.pop("error", "");
    });
  }


  $scope.displayItem = function (item, $event) {
    if($scope.$parent.display_item == item){
      $scope.$parent.display_item = false;
    }
    else{
      var html_item;

      if ($event) {
        html_item = $event.target.parentElement;
      }
      else {
        //Récupération de l'index de l'item en cours de display, après tri du tableau d'items
        var index = $scope.session.items.sort(function (item, item2) {
          //Tri descendant
          return parseInt(item.date_diffusion) > parseInt(item2.date_diffusion)
        }).findIndex(function (_item) {
          return _item.id_item == item.id_item;
        })
        //récupération de l'item
        html_item = document.querySelectorAll(".timeline-item")[index];
      }

      //Récupération de la position de l'élément
      var rect = html_item.getBoundingClientRect(),
            html_item_more = html_item.querySelector(".timeline-more");
      //Positionnement à droite
      html_item_more.style.left = rect.x + rect.width + window.scrollX + "px";
      html_item_more.style.top = rect.y + window.scrollY + "px";
      //Affichage du panel
      $scope.$parent.display_item = item;
    }
  }
  
  $scope.init();
}]);