pf_app.controller('objectifs', ["$scope", "$http", "toaster", function ($scope, $http, toaster) {
  var defaults = {
    "objectifs_apprentissage" : {
      "liste_objectifs_apprentissage": [
      ],
      "nb_objectifs_valides": 0
    }
  };

  angular.extend($scope, defaults);


  $scope.init = function () {
    var _params = [
      $scope.ressource.id_ressource
    ]
    // récupération des objectifs
    $http.get(PF.globals.url.root + "webServices/objectif/"+ _params.join("/"))
    .success(function(retour_json){
      if (retour_json.status === "ok") {
        $scope.ressource.objectifs_apprentissage = retour_json;
      } else {
        // toaster.pop("error", "");
      }
    })
    .error(console.error);

    // récupération des motivations
    $http.get(PF.globals.url.root + "webServices/profil/"+ _params.join("/"))
    .success(function(retour_json){
      if (retour_json.status === "ok") {
        $scope.ressource.profil_apprentissage = retour_json;
      } else {
        // toaster.pop("error", "");
      }
    })
    .error(console.error);
  }
  $scope.init();
  
  //Update du nombre d'objectif d'apprentissage validés
  $scope.$watch('ressource.objectifs_apprentissage.liste_objectifs_apprentissage', function (liste_objectifs_apprentissage_updated, old_value) {
    if (typeof liste_objectifs_apprentissage_updated != "undefined" && liste_objectifs_apprentissage_updated != old_value) {
      var count = 0;
      for (var i = liste_objectifs_apprentissage_updated.length - 1; i >= 0; i--) {
        count += +liste_objectifs_apprentissage_updated[i].est_valide;
      }
      $scope.ressource.objectifs_apprentissage.nb_objectifs_valides = count;
    }
  },true);


  $scope.ajouterObjectif = function () {
    PF.put('objectif', {"id_ressource": $scope.ressource.id_ressource, "nom" : ""} , function(data, status, headers, config) {
      if (data['status'] == "ok") {
        $scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.push({"id_objectif" : data['id_objectif'], "nom" : "", "est_valide" : 0});
      }
      else{
        if (retour['message'] == "ws_utilisateur_invalide") {
          $scope.deconnecter("WS_UTILISATEUR_INVALIDE");
        }
        else
          toaster.pop("error", "WS_ERREUR_CONTACTER_PAIRFORM");
      }
      
    });
  }

  $scope.updateObjectif = function (objectif) {
    var id_objectif = objectif.id_objectif,
      nom = objectif.nom,
      est_valide = objectif.est_valide;

    PF.post('objectif', {"id_objectif": id_objectif, "nom" : nom, "est_valide" : est_valide} , function(data, status, headers, config) {
      if (data['status'] == "ok") {
        //Cast de est_valide en -1 / 1 por false / true
        // var new_count = -1 + 2* (+est_valide);
        //Changement du décompte d'objectifs d'apprentissage valides 
        // $scope.ressource.objectifs_apprentissage.nb_objectifs_valides += new_count;
      }
      else{
        if (retour['message'] == "ws_utilisateur_invalide") {
          $scope.deconnecter("WS_UTILISATEUR_INVALIDE");
        }
        else
          toaster.pop("error", "WS_ERREUR_CONTACTER_PAIRFORM");
      }
      
    });
  }

  $scope.enleverObjectif = function (objectif) {
    PF.delete('objectif', {"id_objectif" : objectif.id_objectif} , function(data, status, headers, config) {
      if (data['status'] == "ok") {
        var index_objectif = $scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.indexOf(objectif);
        $scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.splice(index_objectif,1);

        //Cast de est_valide en -1 / 1 por false / true
        // var new_count = -1 + 2* (+objectif.est_valide);
        //Changement du décompte d'objectifs d'apprentissage valides 
        // $scope.ressource.objectifs_apprentissage.nb_objectifs_valides -= new_count;
      }
      else{
        if (retour['message'] == "ws_utilisateur_invalide") {
          $scope.deconnecter("WS_UTILISATEUR_INVALIDE");
        }
        else
          toaster.pop("error", "WS_ERREUR_CONTACTER_PAIRFORM");
      }
      
    });
  }

  $scope.updateProfilQuestion = function (question) {
    var id_question = question.id_question,
      id_reponse_valide = question.id_reponse_valide,
      param = {"id_utilisateur" : $scope.utilisateur_local.id_utilisateur, 
        "liste_reponses_valides" : [
          {
            "id_question": id_question,
            "id_reponse_valide" : id_reponse_valide
          }
        ]
      };

    PF.post('profil', param , function(data, status, headers, config) {
      if (data['status'] == "ok") {
        //Rien a faire
      }
      else{
        if (retour['message'] == "ws_utilisateur_invalide") {
          $scope.deconnecter("WS_UTILISATEUR_INVALIDE");
        }
        else
          toaster.pop("error", "WS_ERREUR_CONTACTER_PAIRFORM");
      }
      
    });
  }
}]);