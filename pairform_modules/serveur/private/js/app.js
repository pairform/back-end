
/*
 *	Main Controller
 */
var pf_app = angular.module('pf_app',[
	"pascalprecht.translate"
	// ,"ngRaven"
	,"ngStorage"
  ,"ngSanitize"
	,"ngAnimate"
	,"toaster"
	,"ngCookies"
	,"angular-loading-bar"
	,"ngFileUpload"
  ,"ui.thumbnail"
]);