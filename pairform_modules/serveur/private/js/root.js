
pf_app.run(["$rootScope",  "$localStorage", "toaster", "$httpParamSerializer", function ($rootScope, $localStorage, toaster, $httpParamSerializer) {

  $rootScope.espace = PF.espace;
  $rootScope.ressource = PF.ressource;
  $rootScope.messages = PF.messages;


  //S'il n'y a pas d'utilisateur du tout, ou que l'utilisateur non connecté n'est pas encore venu sur cette capsule
  if (!$localStorage.utilisateur_local)
    //On regénère un utilisateur, pour qu'il ait le rang pour cette capsule
    $localStorage.utilisateur_local = new Utilisateur();
  

  $rootScope.utilisateur_local = $localStorage.utilisateur_local;
  PF.globals.utilisateur_local = $localStorage.utilisateur_local;
  $rootScope.codeFromId = PF.langue.codeFromId;
  $rootScope.idFromCode = PF.langue.idFromCode;
  $rootScope.res_url_private = PF.globals.url.private;
  $rootScope.res_url_public = PF.globals.url.public;
  $rootScope.res_url_root = PF.globals.url.root;

  //On récupère les messages en déplacement depuis le PF._localStorage dès qu'on peut
  $rootScope.$root.array_messages_deplacement = $localStorage.array_messages_deplacement;
  
  $rootScope.$storage = $localStorage;

  $rootScope.$storage.flag_special_event_pip = $rootScope.$storage.flag_special_event_pip || false;

  var host = window.document.location.host.replace(/:.*/, '');

  if ($localStorage.utilisateur_local.id_utilisateur > 0) {

    PF.wsReconnect = function(e){
      console.log("WebSocketClient: retry in 20000ms");
      setTimeout(function(){
        console.log("WebSocketClient: reconnecting...");
        PF.ws = new WebSocket(PF.globals.url.websockets);
      },20000);
    }

    var params = $httpParamSerializer({
      id_utilisateur : $localStorage.utilisateur_local.id_utilisateur,
      id_espace: $rootScope.espace.id_espace,
      id_ressource: $rootScope.ressource ? $rootScope.ressource.id_ressource : null
    })
    PF.ws = new WebSocket(PF.globals.url.websockets+ "?" +params);

    //Dès l'ouverture, on s'identifie
    PF.ws.onopen = function () {
      PF.ws.send(JSON.stringify({
        id_utilisateur : $localStorage.utilisateur_local.id_utilisateur,
        pseudo : $localStorage.utilisateur_local.pseudo,
        id_espace: $rootScope.espace.id_espace,
        id_ressource: $rootScope.ressource ? $rootScope.ressource.id_ressource : null
      }));
    };
    PF.ws.onmessage = function (event) {
      $rootScope.$apply(function () {
        var data = JSON.parse(event.data);
        console.log(data);
        //Si un utilisateur se connecte
        if (data.id_utilisateur) {
          toaster.pop("success", data.pseudo + " vient de se connecter à cette page");
        }
        else if (data.id_message) {
          $rootScope.messages.push(data);
          $rootScope.notificationMessage(data);
        }
      })
    };

    // Log errors
    PF.ws.onerror = function (e) {

      switch (e.code){
        case 'ECONNREFUSED':
          PF.ws.reconnect(e);
          break;
        default:
          console.error('WebSocket Error ' + e);
          break;
        }
    };

    PF.ws.onclose = function(e) {
        switch (e){
          case 1000:  // CLOSE_NORMAL
            console.log("WebSocket: closed");
            break;
          default:  // Abnormal closure
            PF.wsReconnect(e);
            break;
        }
    };

    $rootScope.$on("pf:message:envoi", function envoiWS (event, nouveau_message) {
      try{
        PF.ws.send(JSON.stringify(nouveau_message));
      }catch (e){
        PF.ws.emit('error',e);
      }
    });

    //récupération des notifications
    setTimeout(PF.Notification.setUpdateTimer, 3000)
  }

  $rootScope.$on("pf:utilisateur:login", function(){
    window.location.reload();
  });
  $rootScope.$on("pf:utilisateur:logout", function(){
    window.location.reload();
  });

  $rootScope.verifierRole = function (callback) {
    if (!$rootScope.utilisateur_local || $rootScope.utilisateur_local.id_utilisateur == 0 ||  !$rootScope.ressource) {
      return typeof callback == "function" && callback();
    }
    PF.post("utilisateur/categorie/verifier/ressource", {
        "id_ressource" : $rootScope.ressource.id_ressource
    }, function  (retour) {
      var status = retour["status"];
      //Conflit d'identité (un utilisateur est connecté sur le client et sur le serveur, mais pas les même)
      //Besoin de checker est_connecte, parce que id_utilisateur peut-être égal à 0 s'il n'y avait pas d'utilisateur connecté (la condition est vraie qd même) 
      if (status == "ko" && retour['message'] == "ws_utilisateur_invalide"
        || (status == "ok" && $localStorage.utilisateur_local.est_connecte && (retour["id_utilisateur"] != $localStorage.utilisateur_local.id_utilisateur))) {
        //On déconnecte
        $rootScope.deconnecter("WS_UTILISATEUR_INVALIDE");
      }
      //S'il y a mise à jour
      else if(status == "up"){
        //S'il faut mettre à jour la categorie
        $localStorage.utilisateur_local.rank_ressources[$rootScope.ressource.id_ressource] = {
          "id_ressource" : $rootScope.ressource.id_ressource,
          "score" : "0",
          "notification_mail" : "1",
          "watch_mail" : "0",
          "afficher_noms_reels" : "0",
          "nom_categorie" : "Lecteur",
          "id_categorie" : "0"
        }
      }
      //Si le serveur nous dit que c'est good, il peut y avoir des cas chelous
      else if(status == "ok"){
        //Pire cas, l'utilisateur n'a pas un seul rôle en fait
        if(typeof $localStorage.utilisateur_local.rank_ressources){
          $localStorage.utilisateur_local.rank_ressources = [];
        }
        //Mais qu'on a rien en local
        if(typeof $localStorage.utilisateur_local.rank_ressources[$rootScope.ressource.id_ressource] == "undefined"){
          //Cas spécial où l'utilisateur n'est pas encore rentré dans une seule capsule
          //Du coup, il a pas de paramètre pour des capsules
          //Et passport.js ne récupère les catégorie qu'en lien avec les capsules
          //Pour pas tout péter, on le rajoute juste là en loose, ça fait l'affaire
          $localStorage.utilisateur_local.rank_ressources[$rootScope.ressource.id_ressource] = retour.categorie_ressource;
        }
      }
      //S'il y a un callback
      $rootScope.utilisateur_local = $localStorage.utilisateur_local;
      
      //Reload de la page
      typeof callback == "function" && callback()
    });
  }
  // Voyons si le navigateur supporte les notifications
  if ("Notification" in window) {
    //Demander l'affichage de notifications
    Notification.requestPermission();
  }

  $rootScope.notificationMessage = function notificationMessage (message) {
    var title = message.pseudo_auteur + " dit :"
        text = message.contenu, 
        id = message.id_message,
        options = {
          icon: message.url_avatar_auteur
        };

    $rootScope.notification(title, text, id, options);
  }
  $rootScope.notification = function notification (title, text, id, options) {
    title = title || "Nouveau message";
    text = text || "nouveau message";
    options = options || {}
    //https://developer.mozilla.org/fr/docs/Web/API/notification
    var _options = {
      dir:"ltr",
      lang:"fr-FR",
      body: text,
      tag: id || 0,
      icon: PF.globals.url.root + "public/img/logo.png"
    }

    angular.extend(_options, options);

    // Voyons si le navigateur supporte les notifications
    if (!("Notification" in window)) {
      console.error("Ce navigateur ne supporte pas les notifications desktop");
    }

    // Voyons si l'utilisateur est OK pour recevoir des notifications
    else if (Notification.permission === "granted") {
      // Si c'est ok, créons une notification
      var notification = new Notification(title, _options);
    }

    // Sinon, nous avons besoin de la permission de l'utilisateur
    // Note : Chrome n'implémente pas la propriété statique permission
    // Donc, nous devons vérifier s'il n'y a pas 'denied' à la place de 'default'
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {

        // Quelque soit la réponse de l'utilisateur, nous nous assurons de stocker cette information
        if(!('permission' in Notification)) {
          Notification.permission = permission;
        }

        // Si l'utilisateur est OK, on crée une notification
        if (permission === "granted") {
          var notification = new Notification(title, _options);
        }
      });
    }
  }

  $rootScope.afficherConnexion = function (mode){
    var scope_connexion = PF.getScopeOfController('controller_connexion');
    if (scope_connexion){
      // scope_connexion.$parent.setVisible();
      // scope_connexion.$parent.popin_visible = true;
      scope_connexion.visible = true;
      // scope_connexion.popin.visible = true;
      scope_connexion.mode = mode || 'connexion';
    } 
  }

  $rootScope.setSession = function (){

    // var date = new Date();
    // date.setTime(date.getTime() + (24 * 60 * 1000));

    $rootScope.utilisateur_local = $localStorage.utilisateur_local;
    PF.globals.utilisateur_local = $rootScope.utilisateur_local;
    $rootScope.utilisateur_local.est_connecte = true;
    // $localStorage.session = {"elggperm" : $rootScope.utilisateur_local.elggperm, "expiration" : date};
  }

  $rootScope.removeSession = function (){
    $rootScope.utilisateur_local = $localStorage.utilisateur_local;
    PF.globals.utilisateur_local = $rootScope.utilisateur_local;
    $rootScope.utilisateur_local.est_connecte = false;
    $localStorage.session = {};
  }
  $rootScope.deconnecter = function (toast, callback){

    var _scope = $rootScope;
    PF.post('utilisateur/logout', {}, function callback_success (data, status, headers, config) {
      var retour = data;
      if (retour['status'] == 'ok')
      {
        //Reset de l'utilisateur
        $localStorage.utilisateur_local = new Utilisateur();
        //On enlève la session stockée
        _scope.removeSession();

        //On toast grave
        if (typeof(toast) != "undefined"){
          if(toast != ""){
            toaster.clear();
            toaster.pop("warning",toast);
          }
          if(typeof(callback) == "function")
            callback();
        }
        else{
          toaster.pop("success", "LABEL_DECONNEXION_REUSSI");
          if(typeof(callback) == "function")
            callback();
        }
        window.location.reload()
      }
      else
      {
        toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");
      }

    });
  }

  //Activation des websockets
  $rootScope.setUpdateTimers = function () {
    // body...
  }

  $rootScope.chargerProfilFocus = function (id_utilisateur) {
    var scope_profil = PF.getScopeOfController('controller_profil_focus');
    scope_profil.chargerProfil(id_utilisateur);
  }

  $rootScope.chargerEtAfficherProfilFocus = function ($event, id_utilisateur) {
    var scope_profil = PF.getScopeOfController('controller_profil_focus'),
         client_rect = $event.currentTarget.getClientRects()[0];
    scope_profil.chargerProfil(id_utilisateur, function () {
      scope_profil.afficherProfil(client_rect);
    });
  }
  $rootScope.cacherProfilFocus = function (id_utilisateur) {

    var scope_profil = PF.getScopeOfController('controller_profil_focus');
    scope_profil.cacherProfil();
  }
  $rootScope.cancelTimeoutFocus = function (id_utilisateur) {

    var scope_profil = PF.getScopeOfController('controller_profil_focus');
    scope_profil.cancelTimeout();
  }
  $rootScope.afficherProfilFocus = function (event) {
    var scope_profil = PF.getScopeOfController('controller_profil_focus'),
         client_rect = event.currentTarget.getClientRects()[0];
    scope_profil.afficherProfil(client_rect);
  }
}]);