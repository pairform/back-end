pf_app.directive('pfMessage', ["$timeout", "$localStorage", function ($timeout, $localStorage) {
	
	return {
		restrict: 'E',
		// templateUrl:  PF.globals.url.views + 'partial/message',
		link: function (scope, element, attr) {

			if (scope.$last === true) {
			    $timeout(function () {
			        scope.$emit("pf:message_rendered");
			    });
			}
			else if (scope.$first === true) {
			    $timeout(function () {
			        scope.$emit("pf:message_rendered_reverse");
			    });
			}

			scope.init = function (message_updated) {
				var msg = message_updated || scope.message;
				msg.pieces_jointes = msg.pieces_jointes == "[]" ? [] : msg.pieces_jointes;
				
				element.addClass('rank-' + msg.id_role_auteur);

				if(msg.supprime_par != '0'){
					element.addClass('supprime'); 
				}
				else
					element.removeClass('supprime'); 	

				if((msg.id_message_parent != '') && !scope.transversal)
					element.addClass('reponse'); 
				else
					element.removeClass('reponse');

				if(msg.est_defi >= 1)
					element.addClass('est_defi');
				
				//Récupération de l'état de lecture via le tableau de messages lus
				if($localStorage.array_messages_lus.indexOf(msg.id_message) !== -1)
					msg.est_lu = 1;

				if (msg.est_lu == 1)
					element.addClass('est_lu');
				
				
				element.data({
					"id_message" : msg.id_message,
					"id_auteur" : msg.id_auteur,
					"id_role_auteur" : msg.id_role_auteur,
					"supprime_par" : msg.supprime_par,
					"id_langue" : msg.id_langue,
					"prive" : msg.prive,
					"est_defi" : msg.est_defi >= 1 ? msg.est_defi : '',
					"defi_valide" : msg.defi_valide == true ? msg.defi_valide : '',
					"id_message_parent" : msg.id_message_parent != '' ? msg.id_message_parent : ''
				});
			}

			//Url pour images
			scope.res_url_root = PF.globals.url.root;
			scope.res_url_private = PF.globals.url.private;
			scope.res_url_public = PF.globals.url.public;

			scope.init();

 			scope.setMessageLuIfVisible = function () {
		    	//Si le message est déjà lu, on ne cherche pas à tester la suite (pour la fluidité du scroll)
				if (scope.message.est_lu == 1)
					return;
				//Si le message est actuellement visible
				if (scope.isVisible()) {
					//On met le défini comme lu
					scope.setMessageLu();
				}
	    }
	    scope.isVisible = function isScrolledIntoView(el) {
			    var rect = element[0].getBoundingClientRect();
			    var elemTop = rect.top;
			    var elemBottom = rect.bottom;

			    // Only completely visible elements return true:
			    var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
			    // Partially visible elements return true:
			    //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
			    return isVisible;
			}
	   //  scope.isVisible = function () {
		  //   	//Si la barre n'est pas visible, aucun intérêt
				// var com_bar = $_pf("pf-message-list");
				// if(com_bar.length){
				// 	var	com_bar_height = com_bar[0].getBoundingClientRect().height,
				// 		scroll_top = com_bar.scrollTop(),
				// 		offset_message = scroll_top - element[0].offsetTop;
					
				// 	if (offset_message < 0 && Math.abs(offset_message) < com_bar_height)
			 //            return true;
				// 	else
				// 		return false;
				// }
	   //  }

	    scope.setMessageLu = function () {
		    	//Vérification que le message ne vient pas d'être posté(il n'a pas encore
				//d'identifiant attribué par le serveur) et qu'il n'est pas déjà lu
				if (scope.message.id_message && scope.message.est_lu != 1 && scope.message.id_message != 0) {
					scope.message.est_lu = 1;
					//On previent angular que les valeurs ont changées, on actionne au prochain digest
					scope.$apply(function () {
						scope.init();
						scope.$root.$broadcast("pf_message_lu", scope.message.id_message);
					})
				}
	    }

	    //Messages lu si visible dès leur affichage avec un délai de 1 seconde,
	    //le temps que le panneau s'affiche
	    setTimeout(scope.setMessageLuIfVisible, 1000);
			
			//Et on test à chaque mouvement de scroll
			$_pf(window).on('scroll', scope.setMessageLuIfVisible);

			scope.$watch('message',function (message_updated) {
				scope.init(message_updated);
			}, true)
		}
	};
}]);