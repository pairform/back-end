

PF.globals.url.ws = PF.globals.url.root+"webServices/";
PF.globals.url.views = PF.globals.url.root + "app/";

PF.globals.url.private = PF.globals.url.root + "private/";
PF.globals.url.public = PF.globals.url.root + "public/";
PF.globals.url.res = PF.globals.url.root + "res/";
PF.globals.url.dist = PF.globals.url.root + "dist/";

PF.globals.os = "web";
PF._localStorage = localStorage;

//Hack au cas où piwik n'est pas chargé, ça ne fait pas planter le code
_paq = _paq || {};

pf_app.config(["$translateProvider", function ($translateProvider) {
  $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
  $translateProvider.useStaticFilesLoader({
    prefix: PF.globals.url.private + 'json/',
    suffix: '/strings.json'
  });
  //Anglais par défaut, si le navigateur n'a pas la propriété définie
  //userLanguage pour IE
  var code_langue = PF._localStorage["ngStorage-code_langue_app"] ? JSON.parse(PF._localStorage["ngStorage-code_langue_app"])[0] : (window.navigator.userLanguage || window.navigator.language || "en").substr(0, 2);
  
  //S'il n'y a pas de langue compatible
  if (!PF.langue.tableau_inverse[code_langue])
    code_langue = "en";

  $translateProvider.preferredLanguage(code_langue);
  PF._localStorage.setItem("ngStorage-code_langue_app", JSON.stringify([code_langue]));

}]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = true;
  cfpLoadingBarProvider.parentSelector = ".pf.user_menu";
   cfpLoadingBarProvider.spinnerTemplate = "<div class='loading-bar-spinner'><div class='spinner-icon'></div><div class='spinner-icon-reverse'></div></div>";
  cfpLoadingBarProvider.includeBar = false;
  cfpLoadingBarProvider.latencyThreshold = 300;

}]).config(['$httpProvider', function($httpProvider) {
  // $httpProvider.defaults.useXDomain = true;
  // $httpProvider.defaults.withCredentials = true;
  // $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]).config(['ThumbnailServiceProvider', function(ThumbnailServiceProvider) {
    ThumbnailServiceProvider.defaults.width = 200;
    ThumbnailServiceProvider.defaults.height = 200;
    ThumbnailServiceProvider.defaults.keep_ratio = "height";
}]).config( [
    '$compileProvider','$sceDelegateProvider',
    function( $compileProvider, $sceDelegateProvider )
    {   
        $sceDelegateProvider.resourceUrlWhitelist([
          "self",
          PF.globals.url.websockets + "**"
        ]);
    }
]);