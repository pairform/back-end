var express = require('express'),
	app = exports = module.exports = express();


app.get('/', function (req,res) {
	res.render(__dirname + '/views/index');
});
app.get('/vitrine', function (req,res) {
	res.render(__dirname + '/views/index');
});

app.get("/vitrine/:nom_view", function (req, res) {
	res.render(__dirname + "/views/pages/" + req.params.nom_view);
});