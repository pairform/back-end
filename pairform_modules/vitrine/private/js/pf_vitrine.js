PF.initVitrine = function(pf_app) {
	pf_app.config(["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {
		// $locationProvider.html5Mode(true);
		$routeProvider
		// tableau de bord
		.when("/accueil", {
			templateUrl: "vitrine/accueil"
		})
		.when("/kiosque", {
			templateUrl: "vitrine/kiosque",
			controller: "controller_kiosque"
		})
		.when("/conseil", {
			templateUrl: "vitrine/conseil"
		})
		.when("/sur_mesure", {
			templateUrl: "vitrine/sur_mesure"
		})
		.when("/application", {
			templateUrl: "vitrine/application"
		})
		.when("/equipe", {
			templateUrl: "vitrine/equipe"
		})
		.when("/livreblanc", {
			templateUrl: "vitrine/livreblanc"
		})
		.when("/formation", {
			templateUrl: "vitrine/formation"
		})
		.when("/mentions_legales", {
			templateUrl: "vitrine/mentions_legales"
		})
		.when("/politique_confidentialite", {
			templateUrl: "vitrine/politique_confidentialite"
		})
		.when("/cgu", {
			templateUrl: "vitrine/cgu"
		})
		// redirection des routes inconnues vers la demo
		.otherwise({
			redirectTo: "/accueil"
		});

		$locationProvider
			.html5Mode(false)
			.hashPrefix('!');
	}]).config(["$translateProvider", function ($translateProvider) {
		$translateProvider.useStaticFilesLoader({
			files:[{
				prefix: PF.globals.url.private + 'json/',
				suffix: '/strings.json'
			},
			{
				prefix: PF.globals.url.private + 'json/vitrineLangue_',
				suffix: '.json'
			}]
		});
		//On empêche l'override dans main_controller de cette définition 
		$translateProvider.useStaticFilesLoader = function () {
			// console.log("useStaticFilesLoader is read-only to avoid override.");
		};
	}]);

	pf_app.run(["$rootScope", "$location", "toaster", "$httpParamSerializerJQLike", "$http", function ($rootScope, $location, toaster, $httpParamSerializerJQLike, $http) {

		$rootScope.$on('$locationChangeSuccess', function(){
			//On envoie l'info a Google Analytics au passage
			// ga('send', 'pageview', {page: $location.url()});
			_paq.push(['trackPageView']);
			//Timeout pour concorder avec l'animation CSS d'entrée / sortie
			setTimeout(function scrollToTop (argument) {
				window.scrollTo(0,0);
			}, 500);
		});
		$rootScope.$on("pf:utilisateur:login", function () {
			window.document.location = "/home";
		});
		
	}]);

	/*pf_app.directive("twitterTimeline", function() {
		return {
			//C means class E means element A means attribute, this is where this directive should be found
			restrict: 'C',
			link: function(scope, element, attributes) {
				!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
			}
		};
	});*/

	//Don't forget the $location dependency, to retrieve the current path
	pf_app.directive('menuItem', ["$location", "$translate", function ($location, $translate) {
		return{
			restrict: 'A',
			link : function (scope, element, attr) {
				//Watch location change event
				scope.$on('$locationChangeSuccess', function(){
					//Get the current path
					var path = $location.path(),
					//Get the child link of the current element
						href = element.children("a").attr("href").slice(1),
						json_path = 'MENU.'+path.replace("/", "").toUpperCase();
					

					$translate(json_path).then(function (traduction) {
						document.title = traduction;
					})


					//If the paths matches
					if (href == path)
						//Add the class
						element.addClass("active");
					else
						//Remove the class
						element.removeClass("active");
				});

			}
		}
	}]);

	pf_app.controller('controller_kiosque', ["$scope", "$http", "toaster", function ($scope, $http, toaster) {
		var defaults = {
			"visible" : false,
			"loaded" : false,
			"sort_type" : 0,
			"sort_types" : [
					{index : 0, label : "KIOSQUE.ESPACE"}, 
					{index : 1, label : "KIOSQUE.THEME"}
				],
			"order_type" : "ressources.length",
			"order_types" : [
					{value : "ressources.length", label : "KIOSQUE.NOMBRE_DE_DOCUMENTS"}, 
					{value : "nom_court", label : "KIOSQUE.ORDRE_ALPHABETIQUE"},
					{value : "date_creation_espace", label : "KIOSQUE.ANCIENNETE"}
			]
		};
		angular.extend($scope, defaults);

		$scope.init = function () {
			if (!$scope.loaded) {
				var _scope = $scope;
				PF.get('ressource/liste', {}, function(data, status, headers, config) {
					var retour = data;
					// S'il y a des ressources
					if (retour['status'] == 'ok')
					{
						var ressources = retour['ressources'];
						//On va chercher le nombre de messages / On enlève le & du début de post_OS
						_scope.ressources = ressources;
						//Tri des ressources, on commence à afficher
						_scope.sortRessources();
						//Parallèllement, on chope le nombre de messages pour les afficher sur les ressources
						PF.get("message/nombre", {"langues_affichage" : [1,2,3]}, function(retour, status, headers, config){
						
							if (retour['status'] == "ok")
							{
								_scope.nombre_messages = retour['nombre_messages'];
								// var postLoad = {'messages' : messages, 'ressources' : ressources, 'langueApp': LanguageM.langueApp };
								
							}
						});
						var _container = document.querySelector('.tile-container');
						
						setTimeout(function() {
							$scope.msnry = new Masonry(_container, {
								// options...
								itemSelector: '.ressources',
								columnWidth: 120,
								gutter : 10,
								isFitWidth : true
							});
							// $scope.msnry.layout();
						}, 50);

						_scope.loaded = true;
					}
				});
			}
		}

		$scope.afficherEspace = function (espace) {
			window.location(PF.globals.url.root + "doc/" + espace.id_espace);
		}

		$scope.afficherRessource = function (ressource) {
			window.location(PF.globals.url.root + "doc/" + ressource.id_espace + "/" + ressource.id_ressource);
		}

		$scope.showRessources = function (espace) {
			$scope.$root.ressource_affiche = espace.nom_court;
			setTimeout(function() {
				$scope.msnry.layout();
			}, 50);
		}

		$scope.sortRessources = function () {
			var resParDomaine = {};
			var resParEspace = {};
			angular.forEach($scope.ressources,function (ressource) {
				ressource.url_logo = PF.globals.url.root + ressource.url_logo;
				ressource.url_logo_espace = PF.globals.url.root + ressource.url_logo_espace;
				ressource.url = PF.globals.url.root + "doc/" + ressource.id_espace + "/" + ressource.id_ressource;

				if(typeof(resParEspace[ressource.espace_nom_court]) == "undefined")
				{
					resParEspace[ressource.espace_nom_court] = {
						"id_espace" : ressource.id_espace,
						"nom_court" : ressource.espace_nom_court,
						"url_logo" : ressource.url_logo_espace,
						"url" : PF.globals.url.root + "doc/" + ressource.id_espace,
						"date_creation_espace" : ressource.date_creation_espace,
						"ressources" : []
					};
				}
				resParEspace[ressource.espace_nom_court].ressources.push(ressource);

				if(typeof(resParDomaine[ressource.theme]) == "undefined")
				{
					resParDomaine[ressource.theme] = {
						"nom_court" : ressource.theme,
						"date_creation_espace" : ressource.date_creation_espace,
						"ressources" : []
					}
				}
				resParDomaine[ressource.theme].ressources.push(ressource);
				
			});

			$scope.sort_type = 0;
			$scope.res_sorted = [];

			function object2Array(input) {
				var out = []; 
				for(i in input){
					out.push(input[i]);
				}
				return out;
			
			}
			$scope.res_sorted[0] = object2Array(resParEspace);
			$scope.res_sorted[1] = object2Array(resParDomaine);
		}
		
		$scope.$watchGroup(["order_type", "order_type_reverse", "sort_type"], function (new_values, old_values, scope) {
			if (new_values != old_values) {
				setTimeout(function() {
					$scope.msnry.reloadItems();
					$scope.msnry.layout();
				}, 50);
			;}
		});

		$scope.colorFromString = function(string) {
			var hash = 0;
			if (string.length == 0) return hash;
			for (var i = 0; i < string.length; i++) {
				hash = string.charCodeAt(i) + ((hash << 5) - hash);
				hash = hash & hash; // Convert to 32bit integer
			}
			var shortened = hash % 255;
			return "hsl(" + shortened + ",100%,50%)";
		}

		//Initialisation du module
		$scope.init();
	}]);


	pf_app.controller('mautic_form', ["$scope", "$httpParamSerializerJQLike", "$http", function($scope, $httpParamSerializerJQLike, $http){
		$scope.formulaire = {};
		$scope.envoyer = function ($event) {
			var contact = {
				'formId': $scope.formId,
				// 'formName': $scope.formName,
				'return': { ok: 1 },
				'submit' : '1'
			}
			//Copie des champs du formulaire dans le contact
			angular.extend(contact, $scope.formulaire);
			
			var config = {
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}

			var data = { mauticform : contact };

			$http.post('https://mautic.pairform.fr/form/submit', $httpParamSerializerJQLike(data), config)
			.success(function(response) {
				$scope.submitted = true;
			}).error(function(response) {
				$scope.submitted = true;
			});

			$event.preventDefault();

			return false;
		}
	}]);
};


window.onload = function () {
	document.querySelector(".spin-box").classList.add("disappear");

	var toggleBar = function () {
		var navbar = document.querySelector("#navigation-bar");
		navbar.classList.toggle("collapse");
	};
	document.querySelector(".navbar-toggle").onclick = toggleBar;
	document.querySelector("#navigation-bar").onclick = function (){
		var navbar = document.querySelector("#navigation-bar");
		navbar.classList.add("collapse");
	};
}
