

var pf_injecteur = angular.module('pf_injecteur',[
	"pascalprecht.translate"
	,"ngStorage"
	,"ngSanitize"
	,"ngAnimate"
	,"toaster"
	,"ngCookies"
	,"angular-loading-bar"
	,"ngRoute"
]);
pf_injecteur.config(["$translateProvider", function ($translateProvider) {
	$translateProvider.useSanitizeValueStrategy('sanitizeParameters');
	$translateProvider.useStaticFilesLoader({
		files:[{
			prefix: PF.globals.url.private + 'json/',
			suffix: '/strings.json'
		},
		{
			prefix: PF.globals.url.private + 'json/vitrineLangue_',
			suffix: '.json'
		},
		{
			prefix: PF.globals.url.private + 'json/backofficeLangue_',
			suffix: '.json'
		}]
	});
	//Anglais par défaut, si le navigateur n'a pas la propriété définie
	//userLanguage pour IE
	var code_langue = PF._localStorage["ngStorage-code_langue_app"] ? JSON.parse(PF._localStorage["ngStorage-code_langue_app"])[0] : (window.navigator.userLanguage || window.navigator.language || "en").substr(0, 2);
	
	//S'il n'y a pas de langue compatible
	if (!PF.langue.tableau_inverse[code_langue])
		code_langue = "en";

	$translateProvider.preferredLanguage(code_langue);
	PF._localStorage.setItem("ngStorage-code_langue_app", JSON.stringify([code_langue]));

}]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	cfpLoadingBarProvider.includeSpinner = false;
}]).config(['$httpProvider', function($httpProvider) {
	// $httpProvider.defaults.useXDomain = true;
	// $httpProvider.defaults.withCredentials = true;
	// $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
	}
])
.config( ['$provide', function ($provide){
    $provide.decorator('$browser', ['$delegate', function ($delegate) {
        $delegate.onUrlChange = function () {};
        $delegate.url = function () { return ""};
        return $delegate;
    }]);
}]);
