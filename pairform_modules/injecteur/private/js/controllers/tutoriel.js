
pf_injecteur.controller("controller_tutoriel", ["$scope", "$localStorage", "toaster", "$translate", function ($scope, $localStorage, toaster, $translate) {
	var defaults = {
		"visible" : $scope.$storage.pf_injecteur.premiere_visite
	};
	angular.extend($scope, defaults);

	$scope.commencerInjection = function () {
		$scope.$storage.pf_injecteur.premiere_visite = false;
		$scope.$storage.pf_injecteur.etape = 1;
		$scope.visible = false;
		$scope.afficherConnexion();
	}
}]);
