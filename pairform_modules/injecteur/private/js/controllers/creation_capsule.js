
pf_injecteur.controller("controller_creation_capsule", ["$scope", "$http", "$location", "$translate", "toaster", function($scope, $http, $location, $translate, toaster){
	var succes_enregistrement, erreur_requete;
	var parametres_requete;
	var archive_mobile, archive_web;
	$scope.formulaire_soumis = false;
	$scope.archive_mobile_selectionne = false;	// false tant qu'aucune archive mobile respectant les contraintes (format, etc.) n'a été associé à la capsule
	$scope.archive_web_selectionne = false;		// false tant qu'aucune archive web respectant les contraintes (format, etc.) n'a été associé à la capsule
	$scope.format_mobile_incorrect = false;		// false si le format de l'archive mobile de la ressource n'est pas le bon
	$scope.format_web_incorrect = false;		// false si le format de l'archive web de la ressource n'est pas le bon


	$scope.capsule = PF.globals.capsule.hasOwnProperty("id_capsule") ? PF.globals.capsule : {
		espace : undefined, //Undefined exprès pour mettre la valeur par défaut dès le chargement
		ressource : undefined, //Undefined exprès pour mettre la valeur par défaut dès le chargement
		nom_court : document.querySelector("title") ? document.querySelector("title").text : "",
		description : document.querySelector("meta[name=description]") ? document.querySelector("meta[name=description]").content : "",
		url_web : document.location.origin + "/",
		nom_long : "",
		url_mobile : "",
		poid : 0,
		licence : "Non renseigné",
		auteurs : "Non renseigné",
	};

	$scope.$watch('$storage.pf_injecteur.etape', function(etape_actuelle, etape_precedente) {
        if (etape_actuelle !== etape_precedente && etape_actuelle === 3) {
            $scope.init();
        }
    });


	$scope.init = function() {
		
		// on ajoute la capsule en BDD
		PF.get("infosTableauDeBord", null, function callback_success (retour_json){
			if (retour_json.status === "ok") {

				//Ajout du document "Nouveau document"
				var nouveau_document = {
					id_espace : null, 
					id_ressource : 0, 
					id_langue : 3, //Français
					id_theme : 0, //Autre
					id_usage : 2, //Autre
					nom_court: "Nouveau document",
					nom_long: "Nouveau document",
					description: document.querySelector("meta[name=description]") ? document.querySelector("meta[name=description]").content : ""
				}

				retour_json.liste_ressources.splice(0,0, nouveau_document);
				angular.extend($scope.utilisateur_local, retour_json);

				//Sélection de la première valeur par défaut
				$scope.capsule.espace = $scope.capsule.espace || $scope.utilisateur_local.liste_espaces[0];
				$scope.capsule.ressource = $scope.capsule.ressource || $scope.utilisateur_local.liste_ressources[0];

				toaster.pop("success", "Informations chargées");
			} else {
				if (retour_json.message === "ws_utilisateur_invalide") 
					$scope.deconnecter();
				toaster.pop("error", retour_json.message);
			}
		}, function callback_error (erreur_requete){
			toaster.pop("error", JSON.stringify(erreur_requete));
		});
	}

	//Si on ouvre l'injecteur sur ce panneau, il faut initialiser 
    if ($scope.$storage.pf_injecteur.etape === 3)
    	$scope.init();
    
	$scope.gererCapsule = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			$scope.formulaire_en_cours = true;
			parametres_requete = $scope.capsule;
			parametres_requete.id_format = 3; //Format web
			parametres_requete.ressource.id_espace = $scope.capsule.espace.id_espace;
			parametres_requete.id_ressource = $scope.capsule.ressource.id_ressource;
			parametres_requete.ressource_cree_par = $scope.utilisateur_local.id_utilisateur;
			parametres_requete.selecteurs = $scope.$storage.pf_injecteur.selecteurs.contenu;
			
			toaster.pop("info", "Enregistrement en cours…");
			// on ajoute la capsule en BDD
			PF.put("injecteur/capsule", parametres_requete, function callback_success (retour_json){
				$scope.formulaire_en_cours = false;
				if (retour_json.status === "ok") {
					toaster.pop("success", "LABEL.SUCCES.ENREGISTREMENT");
					$scope.$storage.pf_injecteur.hash_section = retour_json.clef_pairform;
					$scope.changerEtape(4);
				} else {
					// SI le nom_ourt est identique à celui d'une capsule existante
					if (retour_json.message === "ER_DUP_ENTRY") {
						// on indique la précesence du doublon
						$scope.capsule_doublon = true;
						// toaster.pop("error", "Le formulaire a des soucis");
					} else {
						toaster.pop("error", erreur_requete);
					}
				}
			}, function callback_error (erreur_requete){
				$scope.formulaire_en_cours = false;
				console.log("erreur requete http put /webServices/capsule");
				toaster.pop("error", erreur_requete);
			});
		}
	}
}]);