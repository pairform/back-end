
pf_injecteur.controller('controller_connexion', ["$scope", "$http", "$localStorage", "toaster", "$translate", function ($scope, $http, $localStorage, toaster, $translate) {
	var defaults = {
		"visible" : false,
		"mode" : "connexion",
		"tableau_langues_label" : PF.langue.tableau_labels,
		// "connexion" : {"username" : "Bigood", "password" : "azerty"},
		"mot_de_passe_oublie" : {},
		"enregistrement" : {
			"id_capsule" : undefined
		}
	};
	// $scope.$watch('visible', function (new_value, old_value) {
	// 	$scope.$parent.visible = new_value;
	// });

	angular.extend($scope, defaults);

	$scope.connecter = function (){
		
		var _scope = $scope;
		PF.post('utilisateur/login', this.connexion, function callback_success (data, status, headers, config) {
			var retour = data;
			if (retour['status'] == 'ok')
			{
				var user = new Utilisateur(retour);
				//On cale l'ID elgg et le nom dans le stockage local
				if($localStorage)
				{
					$localStorage.utilisateur_local = user;
				}
				
				
				PF.getScopeOfController("controller_injecteur").setSession();
				// _scope.verifierRole();
				// Notification.setUpdateTimer();

				// updatePostOs();

				toaster.pop("success","label_connexion_reussi");
				_scope.visible = false;
				//Mise à jour des messages de la page
				//IMPORTANT : on efface le cache des messages
				// Message.recupererMessages(true);
				ga('send', 'event', 'user', 'login_success', user.username);

			}
			else
			{
				PF.getScopeOfController('controller_connexion').connexion.password = "";
				// $_pf('#popin-connexion').effect("shake");
				
				
				if(retour['message'] == "ws_utilisateur_invalide")
					toaster.pop("error", "title_erreur_identifiants");
				else
					toaster.pop("error", retour['message']);

				ga('send', 'event', 'user', 'login_fail');
			}

		}, function callback_error (data, status, headers, config) {
			
		});
	}
	$scope.connecterGooglePlus = function (){
	}
	$scope.connecterFacebook = function (){
		var
		  _screenX   = typeof window.screenX      != 'undefined'
		    ? window.screenX
		    : window.screenLeft,
		  screenY    = typeof window.screenY      != 'undefined'
		    ? window.screenY
		    : window.screenTop,
		  outerWidth = typeof window.outerWidth   != 'undefined'
		    ? window.outerWidth
		    : document.documentElement.clientWidth,
		  outerHeight = typeof window.outerHeight != 'undefined'
		    ? window.outerHeight
		    : (document.documentElement.clientHeight - 22), 

		  
		  
		  width    = 475,
		  height   = 222,
		  screenX  = (_screenX < 0) ? window.screen.width + _screenX : _screenX,
		  left     = parseInt(screenX + ((outerWidth - width) / 2), 10),
		  top      = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
		  features = [];

		if (width !== null) {
		  features.push('width=' + width);
		}
		if (height !== null) {
		  features.push('height=' + height);
		}
		features.push('left=' + left);
		features.push('top=' + top);
		features.push('scrollbars=1');

		features = features.join(',');

		var windowObjectReference = window.open(
			// "https://www.facebook.com/dialog/oauth?client_id=730511317075431&redirect_uri=" + PF.globals.url.ws + "login/facebook&display=popup&response_type=token&scope=email",
			PF.globals.url.ws + "utilisateur/login/facebook",
			"FB_login", 
			features);

		window.addEventListener("message", function callback (event) {
			var retour = event.data;
			var user = new Utilisateur(retour);
			//On cale l'ID elgg et le nom dans le stockage local
			if($localStorage)
			{
				$localStorage.utilisateur_local = user;
			}
			
			
			PF.getScopeOfController("controller_injecteur").setSession();
			// $scope.verifierRole();
			// Notification.setUpdateTimer();

			// updatePostOs();

			toaster.pop("success","label_connexion_reussi");
			$scope.visible = false;
			//Mise à jour des messages de la page
			//IMPORTANT : on efface le cache des messages
			// Message.recupererMessages(true);
			ga('send', 'event', 'user', 'login_success', user.username);
		}, false);
	}
	
	$scope.checkForLTIToken = function () {
		//Récuperation des parametres passés en GET dans l'URL depuis la passerelle LTI
		var url_params = getSearchParameters();
		//On récupère le token == elggperm
		var token = url_params['access_token'] || url_params['token'];
		//Si on est bien dans le cas d'une connexion LTI (et qu'on a le PF._localStorage)
		if (typeof(token) != "undefined"){

			$localStorage.premiere_visite = true;

			//Important : prévenir le WS qu'on est dans le cas de LTI, en changeant la valeur de version
			var post_params = {
				"access_token" : token
			}
			var _scope = $scope;
			_scope.deconnecter('', function(){
				//Et log de l'utilisateur.
				PF.post('utilisateur/login/lti', post_params, function callback_success (data, status, headers, config) {
					var retour = data;
					if (retour['status'] == 'ok')
					{
						var user = new Utilisateur(retour);
						//On cale l'ID elgg et le nom dans le stockage local
						if($localStorage)
						{
							$localStorage.utilisateur_local = user;
						}
						
						
						PF.getScopeOfController("controller_injecteur").setSession();
						// _scope.verifierRole();
						// Notification.setUpdateTimer();

						// updatePostOs();

						toaster.pop("success","label_connexion_reussi");
						//Mise à jour des messages de la page
						//IMPORTANT : on efface le cache des messages
						// Message.recupererMessages(true);
						ga('send', 'event', 'user', 'login_success', user.username);

					}
					else
					{
						toaster.pop("error", retour['message']);
						ga('send', 'event', 'user', 'login_fail');
					}

				});
			});
		}
	}
	
	function getSearchParameters () {
		var prmstr = window.location.search.substr(1);
		return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
	}

	function transformToAssocArray ( prmstr ) {
		var params = {};
		var prmarr = prmstr.split("&");
		for ( var i = 0; i < prmarr.length; i++) {
			var tmparr = prmarr[i].split("=");
			params[tmparr[0]] = tmparr[1];
		}
		return params;
	}
	
	$scope.renvoyerMotDePasse = function (){
		
		var _scope = $scope;
		PF.post('utilisateur/reset', this.mot_de_passe_oublie, function callback_success (data, status, headers, config) {
			var retour = data;
			if (retour['status'] == 'ok') {
				_scope.mode = "enregistrement-1";
			}
			else {
				toaster.pop("error", retour['message']);
			}
		});
	}
	$scope.enregistrer = function (){
		//Attention : register_cgu à la place de register-cgu
		var _scope = $scope;
		PF.put('utilisateur/enregistrer', this.enregistrement, function callback_success (data, status, headers, config) {
			var retour = data;
			if (retour['status'] == 'ok') {
				_scope.mode = "enregistrement-1";
				ga('send', 'event', 'user', 'register', _scope.enregistrement.id_capsule);
			}
			else {
				toaster.pop("error", retour['message']);
			}
		});
	}

	//Vérification LTI
	$scope.checkForLTIToken();
}]);
