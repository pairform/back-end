
pf_injecteur.controller('controller_nav_bar', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
	var defaults = {
		"visible" : true,
		"sidebar_visible" : false,
		"messages_deplacement_visible" : false,
		"notifications" : {'us' : [], 'un' : [], 'count_us' : 0, 'count_un' : 0, 'visible' : ''}
	};
	angular.extend($scope, defaults);
	$scope.deconnecter = function (toast, callback){

		var _scope = $scope;
		PF.post('utilisateur/logout', {}, function callback_success (data, status, headers, config) {
			var retour = data;
			if (retour['status'] == 'ok')
			{
				//Reset de l'utilisateur
				$localStorage.utilisateur_local = new Utilisateur();
				//On enlève la session stockée
				_scope.removeSession();

				//On toast grave
				if (typeof(toast) != "undefined"){
					if(toast != ""){
						toaster.clear();
						toaster.pop("warning",toast);
					}
					if(typeof(callback) == "function")
						callback();
				}
				else{
					toaster.pop("success", "LABEL_DECONNEXION_REUSSI");
					if(typeof(callback) == "function")
						callback();
				}
			}
			else
			{
				toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");
			}

		}, function callback_error (data, status, headers, config) {
			toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");
		});
	}
}]);
