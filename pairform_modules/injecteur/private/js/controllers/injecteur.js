
pf_injecteur.controller("controller_injecteur", ["$scope", "$localStorage", "toaster", "$translate", function ($scope, $localStorage, toaster, $translate) {
	
	//S'il n'y a pas d'utilisateur du tout, ou que l'utilisateur non connecté n'est pas encore venu sur cette capsule
	if (!$localStorage.utilisateur_local 
		|| !$localStorage.utilisateur_local.est_connecte)
		//On regénère un utilisateur, pour qu'il ait le rang pour cette capsule
		$localStorage.utilisateur_local = new Utilisateur();
	else
		//Sinon, on le récupère
		$localStorage.utilisateur_local = $localStorage.utilisateur_local;


	$scope.utilisateur_local = $localStorage.utilisateur_local;

	PF.globals.utilisateur_local = $localStorage.utilisateur_local;


	$scope.$root.mode_specific = false;

	//Valeur par défaut pour l'étape, si l'utilisateur vient de commencer
	$scope.$storage = $localStorage.$default({
		pf_injecteur : {
			premiere_visite : true,
			etape : 1,
			selecteurs : {
				contenu : PF.globals.capsule.selecteurs ? PF.globals.capsule.selecteurs.split(",") : [],
			},
			hash_section: PF.globals.capsule ? PF.globals.capsule.clef_pairform : ""
		},
		url_app : PF.globals.url.views
	});

	if ($scope.$storage.pf_injecteur.etape == 2) {
		initEtape2();
	}

	function updateLayout () {
		//S'il y a déjà des sélecteurs
		if ($scope.$storage.pf_injecteur.selecteurs.contenu.length) {
			for (var i = 0; i < $scope.$storage.pf_injecteur.selecteurs.contenu.length; i++) {
				$_pf($scope.$storage.pf_injecteur.selecteurs.contenu[i]).addClass("injecteur-selection");
			};
		}
	}

	$scope.$watchCollection("$storage.pf_injecteur.selecteurs.contenu", function (nouveaux_selecteurs, anciens_selecteurs) {
		if (nouveaux_selecteurs != anciens_selecteurs) {
			var selecteurs_enleves = $_pf(anciens_selecteurs).not(nouveaux_selecteurs).get();

			for (var i = 0; i < selecteurs_enleves.length; i++) {
				$_pf(selecteurs_enleves[i]).removeClass("injecteur-selection");
			};

			var selecteurs_ajoutes = $_pf(nouveaux_selecteurs).not(anciens_selecteurs).get();

			for (var i = 0; i < selecteurs_ajoutes.length; i++) {
				$_pf(selecteurs_ajoutes[i]).addClass("injecteur-selection");
			};
		}
	})
	$scope.ajouterSelecteur = function (){
		$scope.$storage.pf_injecteur.selecteurs.contenu.push("");
	}
	$scope.changerEtape = function (etape){
		switch (etape){
			case 1 :
				$scope.$storage.pf_injecteur.etape = 1;
				break;
			case 2 :
				//Si l'utilisateur est connecté, on avance
				if ($scope.utilisateur_local.id_utilisateur)
					$scope.$storage.pf_injecteur.etape = 2;
				else
					toaster.pop("warning", "Vous devez créer un compte pour continuer");
				break;
			case 3 :
				//Si l'utilisateur est connecté, 
				//et qu'il a sélectionné un sélecteur de contenu au minimum,
				//on avance
				if ($scope.utilisateur_local.id_utilisateur 
					&& $scope.$storage.pf_injecteur.selecteurs.contenu.length)
					$scope.$storage.pf_injecteur.etape = 3;
				else
					toaster.pop("warning", "Vous devez sélectionner au moins une zone commentable pour continuer");
				break;
			case 4 :
				//Si l'utilisateur est connecté,
				//et qu'il a sélectionné un sélecteur de contenu au minimum,
				//et qu'il a déclaré sa ressource,
				//on avance
				// if ($scope.utilisateur_local.id_utilisateur
				// 	&& $scope.$storage.pf_injecteur.selecteurs.contenu.length
				// 	&& $scope.$storage.pf_injecteur.hash_section)
					$scope.$storage.pf_injecteur.etape = 4;
				// else
					// toaster.pop("warning", "Vous devez déclarer votre site web pour continuer");
				break;
			default :
				$scope.$storage.pf_injecteur.etape = 1;
				break;
		}
	}
	$scope.afficherConnexion = function (mode){
		var scope_connexion = PF.getScopeOfController('controller_connexion');
		if (scope_connexion){
			// scope_connexion.$parent.setVisible();
			// scope_connexion.$parent.popin_visible = true;
			scope_connexion.visible = true;
			// scope_connexion.popin.visible = true;
			scope_connexion.mode = mode || 'connexion';
		}	
	}
	$scope.setSession = function (){

		// var date = new Date();
		// date.setTime(date.getTime() + (24 * 60 * 1000));

		$scope.utilisateur_local = $localStorage.utilisateur_local;
		PF.globals.utilisateur_local = $scope.utilisateur_local;
		$scope.utilisateur_local.est_connecte = true;
		$scope.changerEtape(2);
		// $localStorage.session = {"elggperm" : $scope.utilisateur_local.elggperm, "expiration" : date};
	}

	$scope.removeSession = function (){
		$scope.utilisateur_local = $localStorage.utilisateur_local;
		PF.globals.utilisateur_local = $scope.utilisateur_local;
		$scope.utilisateur_local.est_connecte = false;
		$localStorage.session = {};
		$scope.changerEtape(1);
	}
	$scope.switchToApp = function (){
		window.location.reload();
		delete localStorage.flag_inj;
		// $scope.$destroy();
		/*
		 * Iterate through the child scopes and kill 'em
		 * all, because Angular 1.2 won't let us $destroy()
		 * the $rootScope
		 */
		// var scope = $scope.$root.$$childHead;
		// while (scope) {
		//     var nextScope = scope.$$nextSibling;
		//     scope.$destroy();
		//     scope = nextScope;
		// }

		/*
		 * Iterate the properties of the $rootScope and delete 
		 * any that possibly were set by us but leave the 
		 * Angular-internal properties and functions intact so we 
		 * can re-use the application.
		 */
		// for(var prop in $scope.$root){
		//     if (($scope.$root[prop]) 
		//         && (prop.indexOf('$$') != 0) 
		//         && (typeof($scope.$root[prop]) === 'object')
		//         ) {
		//         $scope.$root[prop] = null;
		//     }
		// }
		// $_pf("[ng-controller=controller_injecteur]").remove();

		// require(['main_controller']);
	}
	function initEtape2 () {
		var html = html || $_pf("body");
		$_pf(document).on("mouseenter.injecteur_pf", "body *:not([ng-controller])", function (e) {
			e.stopPropagation();
			if (isPartOfPF(e.target))
				return;
			// var path = getSpecificSelector(e.target);
			var path_general = $scope.mode_specific ? getSpecificSelector(e.target) : getGeneralSelector(e.target);
			// $_pf(path).addClass("injecteur-selection-temp");
			$_pf(path_general).filter(":not(.pf,input,#toast-container)").addClass("injecteur-selection-temp-similaires");
			console.log(path_general);
		});
		$_pf(document).on("mouseout.injecteur_pf", "body *:not([ng-controller])", function (e) {
			e.stopPropagation();
			if (isPartOfPF(e.target))
				return;
			// var path = getSpecificSelector(e.target);
			var path_general = $scope.mode_specific ? getSpecificSelector(e.target) : getGeneralSelector(e.target);
			// $_pf(path).removeClass("injecteur-selection-temp");
			$_pf(path_general).filter(":not(.pf,input,#toast-container)").removeClass("injecteur-selection-temp-similaires");
		});
		$_pf(document).on("click.injecteur_pf", "body *:not([ng-controller])", function (e) {
			e.stopPropagation();
			if (isPartOfPF(e.target))
				return;
			var path = getSpecificSelector(e.target);
			var path_general = $scope.mode_specific ? getSpecificSelector(e.target) : getGeneralSelector(e.target);
			//On l'enlève s'il y est déjà
			var index_path = $scope.$storage.pf_injecteur.selecteurs.contenu.indexOf(path_general);
			$scope.$apply(function () {
				if (index_path >= 0){
					// $_pf(path_general).removeClass("injecteur-selection");
					$scope.$storage.pf_injecteur.selecteurs.contenu.splice(index_path, 1);
				}
				//Sinon on ajoute la classe et le sélecteur
				else{
					// $_pf(path_general).addClass("injecteur-selection");
					$scope.$storage.pf_injecteur.selecteurs.contenu.push(path_general);
				}
			})

		});

		updateLayout();
	}
	function getSpecificSelector(el){
	  var names = [];
	  while (el.parentNode){
	    if (el.id && !el.id.match(/[0-9]/)){
	      names.unshift('#'+el.id);
	      break;
	    }else{
	      if (el==el.ownerDocument.documentElement) {
	      	names.unshift(el.tagName.toLowerCase());
	      }
	      else{
	        for (var c=1,e=el;e.previousElementSibling;e=e.previousElementSibling,c++);
	        names.unshift(el.tagName.toLowerCase()+":nth-child("+c+")");
	      }
	      el=el.parentNode;
	    }
	  }
	  return names.join(" > ");
	}
	function getGeneralSelector(el){
	  var names = [];
	  while (el.parentNode){
	    //Si on tombe direct sur un id, on ne le prend pas
	  	//On ne veut pas des IDs avec des chiffres dedans, surement des ids de référence de post
	  	// de blog ou autre...
	    if (el.id 
	    	&& !el.id.match(/[0-9]/) 
	    	&& names.length){
	      names.unshift('#'+el.id);
      	  break;
	    }else{
	      // if (el==el.ownerDocument.documentElement) 
	      	names.unshift(el.tagName.toLowerCase());
	      	
		  	//On ne veut pas non plus remonter jusqu'au body s'il n'y a pas d'id type "#content" : 
		  	// on peut s'arrêter à "article" si on en trouve un
	      	if (el.tagName == "ARTICLE" || el.tagName == "HTML" || el.parentElement.tagName == "BODY")
	      		break;

	      el=el.parentNode;
	    }
	  }
	  return names.join(" > ");
	}
	function isPartOfPF(el){
	  var names = [];
	  while (el.parentNode){
	    //Si on tombe direct sur un id, on ne le prend pas
	    if (el.classList.contains("pf") || el.id === "toast-container")
      	  return true;
	    else
	      el=el.parentNode;		    
	  }
	  return false;
	}
	$scope.$watch("$storage.pf_injecteur.etape", function (new_value, old_value) {
		if (new_value != old_value) {
			if (new_value == 2) {
				initEtape2();
			}
			else{
				$_pf(document).off(".injecteur_pf");
			}
		}
	})
	//Vérification que l'utilisateur a un rôle pour cette ressource
	$scope.verifierRole = function () {
		// PF.post("utilisateur/categorie/verifier", {
		// 		"id_ressource" : $scope.capsule.id_ressource,
		// 		"id_capsule" : $scope.capsule.id_capsule
		// }, function  (retour) {
		// 	var status = retour["status"];
		// 	//Conflit d'identité (un utilisateur est connecté sur le client et sur le serveur, mais pas les même)
		// 	//Besoin de checker est_connecte, parce que id_utilisateur peut-être égal à 0 s'il n'y avait pas d'utilisateur connecté (la condition est vraie qd même) 
		// 	if (status == "ko" && retour['message'] == "ws_utilisateur_invalide"
		// 		|| ($scope.utilisateur_local.est_connecte && (retour["id_utilisateur"] != $scope.utilisateur_local.id_utilisateur))) {
		// 		//On déconnecte
		// 		$scope.deconnecter("ws_utilisateur_invalide");
		// 	}
		// 	//S'il y a mise à jour
		// 	else if(status == "up"){
		// 		//S'il faut mettre à jour la categorie
		// 		if (typeof(retour["categorie_ressource"]) != "undefined") {
		// 			//S'il n'y avait rien, ni rôle ni parametres pour cette capsule
		// 			if (retour["categorie_ressource"] == false) {
		// 				//Mise à jour de la catégorie avec les parametres par défaut
		// 				$scope.utilisateur_local.rank[$scope.capsule.id_capsule] = {
		// 					"id_ressource" : $scope.capsule.id_ressource,
		// 					"score" : "0",
		// 					"notification_mail" : "1",
		// 					"afficher_noms_reels" : "0",
		// 					"nom_categorie" : "Lecteur",
		// 					"id_categorie" : "0"
		// 				}
		// 			}
		// 			//Sinon, l'utilisateur n'avait pas visité cette capsule, mais avait déjà un rôle dans cette ressource
		// 			else{
		// 				//Mise à jour de la catégorie avec les parametres revenant du serveur
		// 				$scope.utilisateur_local.rank[$scope.capsule.id_capsule] = {
		// 					"id_ressource" : $scope.capsule.id_ressource,
		// 					"score" : retour["categorie_ressource"]["score"],
		// 					"notification_mail" : "1",
		// 					"afficher_noms_reels" : "0",
		// 					"nom_categorie" : retour["categorie_ressource"]["nom_categorie"],
		// 					"id_categorie" : retour["categorie_ressource"]["id_categorie"]
		// 				}
		// 			}
		// 		}
		// 		//Sinon, il y avait déjà les rôles .
		// 		else if (typeof(retour["utilisateur_parametres"]) != "undefined") {
		// 			//Mise à jour des paramètres utilisateurs
		// 			$scope.utilisateur_local.rank[$scope.capsule.id_capsule].notification_mail = "1";
		// 			$scope.utilisateur_local.rank[$scope.capsule.id_capsule].afficher_noms_reels = "0";
		// 		}
		// 	}
		// });
	}
}]);