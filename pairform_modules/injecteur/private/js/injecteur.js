'use strict';

//Utilisation de notre propre instance de jQuery, sans polluer le DOM
$_pf = window.$_pf || jQuery.noConflict(true);

//Injection de tous les modules
$_pf.get(PF.globals.url.root + 'injecteur/layer', function(layer) {
	$_pf('body').prepend(layer);


	var pf_injecteur = angular.module('pf_injecteur',[
		"pascalprecht.translate"
		,"ngStorage"
		,"ngSanitize"
		,"ngAnimate"
		,"toaster"
		,"ngCookies"
		,"angular-loading-bar"
		,"ngRoute"
	]);
	pf_injecteur.config(["$translateProvider", function ($translateProvider) {
		$translateProvider.useSanitizeValueStrategy('sanitizeParameters');
		$translateProvider.useStaticFilesLoader({
			files:[{
				prefix: PF.globals.url.private + 'json/',
				suffix: '/strings.json'
			},
			{
				prefix: PF.globals.url.private + 'json/vitrineLangue_',
				suffix: '.json'
			},
			{
				prefix: PF.globals.url.private + 'json/backofficeLangue_',
				suffix: '.json'
			}]
		});
		//Anglais par défaut, si le navigateur n'a pas la propriété définie
		//userLanguage pour IE
		var code_langue = PF._localStorage["ngStorage-code_langue_app"] ? JSON.parse(PF._localStorage["ngStorage-code_langue_app"])[0] : (window.navigator.userLanguage || window.navigator.language || "en").substr(0, 2);
		
		//S'il n'y a pas de langue compatible
		if (!PF.langue.tableau_inverse[code_langue])
			code_langue = "en";

		$translateProvider.preferredLanguage(code_langue);
		PF._localStorage.setItem("ngStorage-code_langue_app", JSON.stringify([code_langue]));

	}]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
		cfpLoadingBarProvider.includeSpinner = false;
	}]).config(['$httpProvider', function($httpProvider) {
		// $httpProvider.defaults.useXDomain = true;
		// $httpProvider.defaults.withCredentials = true;
		// $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		}
	]);

	// filtre la liste des ressource pour ne conserver que les ressources d'un espace
	pf_injecteur.filter("filtreByIdEspace", function() {
		return function(liste_ressources, id_espace) {
			var liste_ressources_filtres = new Array();

			// si un espace a été sélectionné 
			if (id_espace) {
				for(var i in liste_ressources) {
					// si l'espace sélectionné contient la ressource
					// Ou qu'on est dans le cas d'un nouveau document, sans encore d'espace
					if (id_espace === liste_ressources[i].id_espace || liste_ressources[i].id_espace === null) {
						liste_ressources_filtres.push(liste_ressources[i]);
					}
				}
				return liste_ressources_filtres;

			} else {
				return liste_ressources;
			}
		};
	});
	pf_injecteur.controller("controller_injecteur", ["$scope", "$localStorage", "toaster", "$translate", function ($scope, $localStorage, toaster, $translate) {
		
		//S'il n'y a pas d'utilisateur du tout, ou que l'utilisateur non connecté n'est pas encore venu sur cette capsule
		if (!$localStorage.utilisateur_local 
			|| !$localStorage.utilisateur_local.est_connecte)
			//On regénère un utilisateur, pour qu'il ait le rang pour cette capsule
			$localStorage.utilisateur_local = new Utilisateur();
		else
			//Sinon, on le récupère
			$localStorage.utilisateur_local = $localStorage.utilisateur_local;


		$scope.utilisateur_local = $localStorage.utilisateur_local;

		PF.globals.utilisateur_local = $localStorage.utilisateur_local;


		//Valeur par défaut pour l'étape, si l'utilisateur vient de commencer
		$scope.$storage = $localStorage.$default({
			pf_injecteur : {
				premiere_visite : true,
				etape : 1,
				selecteurs : {
					contenu : PF.globals.capsule.selecteurs ? PF.globals.capsule.selecteurs.split(",") : [],
				},
				hash_section: PF.globals.capsule ? PF.globals.capsule.clef_pairform : ""
			},
			url_app : PF.globals.url.views
		});

		if ($scope.$storage.pf_injecteur.etape == 2) {
			initEtape2();
		}

		function updateLayout () {
			//S'il y a déjà des sélecteurs
			if ($scope.$storage.pf_injecteur.selecteurs.contenu.length) {
				for (var i = 0; i < $scope.$storage.pf_injecteur.selecteurs.contenu.length; i++) {
					$_pf($scope.$storage.pf_injecteur.selecteurs.contenu[i]).addClass("injecteur-selection");
				};
			}
		}

		$scope.$watchCollection("$storage.pf_injecteur.selecteurs.contenu", function (nouveaux_selecteurs, anciens_selecteurs) {
			if (nouveaux_selecteurs != anciens_selecteurs) {
				var selecteurs_enleves = $_pf(anciens_selecteurs).not(nouveaux_selecteurs).get();

				for (var i = 0; i < selecteurs_enleves.length; i++) {
					$_pf(selecteurs_enleves[i]).removeClass("injecteur-selection");
				};

				var selecteurs_ajoutes = $_pf(nouveaux_selecteurs).not(anciens_selecteurs).get();

				for (var i = 0; i < selecteurs_ajoutes.length; i++) {
					$_pf(selecteurs_ajoutes[i]).addClass("injecteur-selection");
				};
			}
		})
		$scope.changerEtape = function (etape){
			switch (etape){
				case 1 :
					$scope.$storage.pf_injecteur.etape = 1;
					break;
				case 2 :
					//Si l'utilisateur est connecté, on avance
					if ($scope.utilisateur_local.id_utilisateur)
						$scope.$storage.pf_injecteur.etape = 2;
					else
						toaster.pop("warning", "Vous devez créer un compte pour continuer");
					break;
				case 3 :
					//Si l'utilisateur est connecté, 
					//et qu'il a sélectionné un sélecteur de contenu au minimum,
					//on avance
					if ($scope.utilisateur_local.id_utilisateur 
						&& $scope.$storage.pf_injecteur.selecteurs.contenu.length)
						$scope.$storage.pf_injecteur.etape = 3;
					else
						toaster.pop("warning", "Vous devez sélectionner au moins une zone commentable pour continuer");
					break;
				case 4 :
					//Si l'utilisateur est connecté,
					//et qu'il a sélectionné un sélecteur de contenu au minimum,
					//et qu'il a déclaré sa ressource,
					//on avance
					// if ($scope.utilisateur_local.id_utilisateur
					// 	&& $scope.$storage.pf_injecteur.selecteurs.contenu.length
					// 	&& $scope.$storage.pf_injecteur.hash_section)
						$scope.$storage.pf_injecteur.etape = 4;
					// else
						// toaster.pop("warning", "Vous devez déclarer votre site web pour continuer");
					break;
				default :
					$scope.$storage.pf_injecteur.etape = 1;
					break;
			}
		}
		$scope.afficherConnexion = function (mode){
			var scope_connexion = PF.getScopeOfController('controller_connexion');
			if (scope_connexion){
				// scope_connexion.$parent.setVisible();
				// scope_connexion.$parent.popin_visible = true;
				scope_connexion.visible = true;
				// scope_connexion.popin.visible = true;
				scope_connexion.mode = mode || 'connexion';
			}	
		}
		$scope.setSession = function (){

			// var date = new Date();
			// date.setTime(date.getTime() + (24 * 60 * 1000));

			$scope.utilisateur_local = $localStorage.utilisateur_local;
			PF.globals.utilisateur_local = $scope.utilisateur_local;
			$scope.utilisateur_local.est_connecte = true;
			$scope.changerEtape(2);
			// $localStorage.session = {"elggperm" : $scope.utilisateur_local.elggperm, "expiration" : date};
		}

		$scope.removeSession = function (){
			$scope.utilisateur_local = $localStorage.utilisateur_local;
			PF.globals.utilisateur_local = $scope.utilisateur_local;
			$scope.utilisateur_local.est_connecte = false;
			$localStorage.session = {};
			$scope.changerEtape(1);
		}
		$scope.switchToApp = function (){
			window.location.reload();
			delete localStorage.flag_inj;
			// $scope.$destroy();
			/*
			 * Iterate through the child scopes and kill 'em
			 * all, because Angular 1.2 won't let us $destroy()
			 * the $rootScope
			 */
			// var scope = $scope.$root.$$childHead;
			// while (scope) {
			//     var nextScope = scope.$$nextSibling;
			//     scope.$destroy();
			//     scope = nextScope;
			// }

			/*
			 * Iterate the properties of the $rootScope and delete 
			 * any that possibly were set by us but leave the 
			 * Angular-internal properties and functions intact so we 
			 * can re-use the application.
			 */
			// for(var prop in $scope.$root){
			//     if (($scope.$root[prop]) 
			//         && (prop.indexOf('$$') != 0) 
			//         && (typeof($scope.$root[prop]) === 'object')
			//         ) {
			//         $scope.$root[prop] = null;
			//     }
			// }
			// $_pf("[ng-controller=controller_injecteur]").remove();

			// require(['main_controller']);
		}
		function initEtape2 () {
			$_pf(document).on("mouseenter.injecteur_pf", "body *:not([ng-controller])", function (e) {
				e.stopPropagation();
				if (isPartOfPF(e.target))
					return;
				// var path = getSpecificSelector(e.target);
				var path_general = getGeneralSelector(e.target);
				// $_pf(path).addClass("injecteur-selection-temp");
				$_pf(path_general).addClass("injecteur-selection-temp-similaires");
				console.log(path_general);
			});
			$_pf(document).on("mouseout.injecteur_pf", "body *:not([ng-controller])", function (e) {
				e.stopPropagation();
				if (isPartOfPF(e.target))
					return;
				// var path = getSpecificSelector(e.target);
				var path_general = getGeneralSelector(e.target);
				// $_pf(path).removeClass("injecteur-selection-temp");
				$_pf(path_general).removeClass("injecteur-selection-temp-similaires");
			});
			$_pf(document).on("click.injecteur_pf", "body *:not([ng-controller])", function (e) {
				e.stopPropagation();
				if (isPartOfPF(e.target))
					return;
				var path = getSpecificSelector(e.target);
				var path_general = getGeneralSelector(e.target);
				//On l'enlève s'il y est déjà
				var index_path = $scope.$storage.pf_injecteur.selecteurs.contenu.indexOf(path_general);
				$scope.$apply(function () {
					if (index_path >= 0){
						// $_pf(path_general).removeClass("injecteur-selection");
						$scope.$storage.pf_injecteur.selecteurs.contenu.splice(index_path, 1);
					}
					//Sinon on ajoute la classe et le sélecteur
					else{
						// $_pf(path_general).addClass("injecteur-selection");
						$scope.$storage.pf_injecteur.selecteurs.contenu.push(path_general);
					}
				})

			});

			updateLayout();
		}
		function getSpecificSelector(el){
		  var names = [];
		  while (el.parentNode){
		    if (el.id && !el.id.match(/[0-9]/)){
		      names.unshift('#'+el.id);
		      break;
		    }else{
		      if (el==el.ownerDocument.documentElement) {
		      	names.unshift(el.tagName);
		      }
		      else{
		        for (var c=1,e=el;e.previousElementSibling;e=e.previousElementSibling,c++);
		        names.unshift(el.tagName+":nth-child("+c+")");
		      }
		      el=el.parentNode;
		    }
		  }
		  return names.join(" > ");
		}
		function getGeneralSelector(el){
		  var names = [];
		  while (el.parentNode){
		    //Si on tombe direct sur un id, on ne le prend pas
		  	//On ne veut pas des IDs avec des chiffres dedans, surement des ids de référence de post
		  	// de blog ou autre...
		    if (el.id 
		    	&& !el.id.match(/[0-9]/) 
		    	&& names.length){
		      names.unshift('#'+el.id);
	      	  break;
		    }else{
		      // if (el==el.ownerDocument.documentElement) 
		      	names.unshift(el.tagName.toLowerCase());
		      	
			  	//On ne veut pas non plus remonter jusqu'au body s'il n'y a pas d'id type "#content" : 
			  	// on peut s'arrêter à "article" si on en trouve un
		      	if (el.tagName == "ARTICLE")
		      		break;
		      el=el.parentNode;
		    }
		  }
		  return names.join(" > ");
		}
		function isPartOfPF(el){
		  var names = [];
		  while (el.parentNode){
		    //Si on tombe direct sur un id, on ne le prend pas
		    if (el.classList.contains("pf") || el.id === "toast-container")
	      	  return true;
		    else
		      el=el.parentNode;		    
		  }
		  return false;
		}
		$scope.$watch("$storage.pf_injecteur.etape", function (new_value, old_value) {
			if (new_value != old_value) {
				if (new_value == 2) {
					initEtape2();
				}
				else{
					$_pf(document).off(".injecteur_pf");
				}
			}
		})
		//Vérification que l'utilisateur a un rôle pour cette ressource
		$scope.verifierRole = function () {
			// PF.post("utilisateur/categorie/verifier", {
			// 		"id_ressource" : $scope.capsule.id_ressource,
			// 		"id_capsule" : $scope.capsule.id_capsule
			// }, function  (retour) {
			// 	var status = retour["status"];
			// 	//Conflit d'identité (un utilisateur est connecté sur le client et sur le serveur, mais pas les même)
			// 	//Besoin de checker est_connecte, parce que id_utilisateur peut-être égal à 0 s'il n'y avait pas d'utilisateur connecté (la condition est vraie qd même) 
			// 	if (status == "ko" && retour['message'] == "ws_utilisateur_invalide"
			// 		|| ($scope.utilisateur_local.est_connecte && (retour["id_utilisateur"] != $scope.utilisateur_local.id_utilisateur))) {
			// 		//On déconnecte
			// 		$scope.deconnecter("ws_utilisateur_invalide");
			// 	}
			// 	//S'il y a mise à jour
			// 	else if(status == "up"){
			// 		//S'il faut mettre à jour la categorie
			// 		if (typeof(retour["categorie_ressource"]) != "undefined") {
			// 			//S'il n'y avait rien, ni rôle ni parametres pour cette capsule
			// 			if (retour["categorie_ressource"] == false) {
			// 				//Mise à jour de la catégorie avec les parametres par défaut
			// 				$scope.utilisateur_local.rank[$scope.capsule.id_capsule] = {
			// 					"id_ressource" : $scope.capsule.id_ressource,
			// 					"score" : "0",
			// 					"notification_mail" : "1",
			// 					"afficher_noms_reels" : "0",
			// 					"nom_categorie" : "Lecteur",
			// 					"id_categorie" : "0"
			// 				}
			// 			}
			// 			//Sinon, l'utilisateur n'avait pas visité cette capsule, mais avait déjà un rôle dans cette ressource
			// 			else{
			// 				//Mise à jour de la catégorie avec les parametres revenant du serveur
			// 				$scope.utilisateur_local.rank[$scope.capsule.id_capsule] = {
			// 					"id_ressource" : $scope.capsule.id_ressource,
			// 					"score" : retour["categorie_ressource"]["score"],
			// 					"notification_mail" : "1",
			// 					"afficher_noms_reels" : "0",
			// 					"nom_categorie" : retour["categorie_ressource"]["nom_categorie"],
			// 					"id_categorie" : retour["categorie_ressource"]["id_categorie"]
			// 				}
			// 			}
			// 		}
			// 		//Sinon, il y avait déjà les rôles .
			// 		else if (typeof(retour["utilisateur_parametres"]) != "undefined") {
			// 			//Mise à jour des paramètres utilisateurs
			// 			$scope.utilisateur_local.rank[$scope.capsule.id_capsule].notification_mail = "1";
			// 			$scope.utilisateur_local.rank[$scope.capsule.id_capsule].afficher_noms_reels = "0";
			// 		}
			// 	}
			// });
		}
	}]);
	pf_injecteur.controller("controller_tutoriel", ["$scope", "$localStorage", "toaster", "$translate", function ($scope, $localStorage, toaster, $translate) {
		var defaults = {
			"visible" : $scope.$storage.pf_injecteur.premiere_visite
		};
		angular.extend($scope, defaults);

		$scope.commencerInjection = function () {
			$scope.$storage.pf_injecteur.premiere_visite = false;
			$scope.$storage.pf_injecteur.etape = 1;
			$scope.visible = false;
			$scope.afficherConnexion();
		}
	}]);
	
	pf_injecteur.controller('controller_nav_bar', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
		var defaults = {
			"visible" : true,
			"sidebar_visible" : false,
			"messages_deplacement_visible" : false,
			"notifications" : {'us' : [], 'un' : [], 'count_us' : 0, 'count_un' : 0, 'visible' : ''}
		};
		angular.extend($scope, defaults);
		$scope.deconnecter = function (toast, callback){

			var _scope = $scope;
			PF.post('utilisateur/logout', {}, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok')
				{
					//Reset de l'utilisateur
					$localStorage.utilisateur_local = new Utilisateur();
					//On enlève la session stockée
					_scope.removeSession();

					//On toast grave
					if (typeof(toast) != "undefined"){
						if(toast != ""){
							toaster.clear();
							toaster.pop("warning", toast);
						}
						if(typeof(callback) == "function")
							callback();
					}
					else{
						toaster.pop("success", "LABEL_DECONNEXION_REUSSI");
						if(typeof(callback) == "function")
							callback();
					}
				}
				else
				{
					toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");
				}

			}, function callback_error (data, status, headers, config) {
				toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");
			});
		}
	}]);

	pf_injecteur.controller("controller_creation_capsule", ["$scope", "$http", "$location", "$translate", "toaster", function($scope, $http, $location, $translate, toaster){
		var succes_enregistrement, erreur_requete;
		var parametres_requete;
		var archive_mobile, archive_web;
		$scope.formulaire_soumis = false;
		$scope.archive_mobile_selectionne = false;	// false tant qu'aucune archive mobile respectant les contraintes (format, etc.) n'a été associé à la capsule
		$scope.archive_web_selectionne = false;		// false tant qu'aucune archive web respectant les contraintes (format, etc.) n'a été associé à la capsule
		$scope.format_mobile_incorrect = false;		// false si le format de l'archive mobile de la ressource n'est pas le bon
		$scope.format_web_incorrect = false;		// false si le format de l'archive web de la ressource n'est pas le bon


		$scope.capsule = PF.globals.capsule.hasOwnProperty("id_capsule") ? PF.globals.capsule : {
			espace : undefined, //Undefined exprès pour mettre la valeur par défaut dès le chargement
			ressource : undefined, //Undefined exprès pour mettre la valeur par défaut dès le chargement
			nom_court : document.querySelector("title") ? document.querySelector("title").text : "",
			description : document.querySelector("meta[name=description]") ? document.querySelector("meta[name=description]").content : "",
			url_web : document.location.origin + "/",
			nom_long : "",
			url_mobile : "",
			poid : 0,
			licence : "Non renseigné",
			auteurs : "Non renseigné",
		};

		$scope.$watch('$storage.pf_injecteur.etape', function(etape_actuelle, etape_precedente) {
	        if (etape_actuelle !== etape_precedente && etape_actuelle === 3) {
	            $scope.init();
	        }
	    });


		$scope.init = function() {
			
			// on ajoute la capsule en BDD
			PF.get("infosTableauDeBord", null, function callback_success (retour_json){
				if (retour_json.status === "ok") {

					//Ajout du document "Nouveau document"
					var nouveau_document = {
						id_espace : null, 
						id_ressource : 0, 
						id_langue : 3, //Français
						id_theme : 0, //Autre
						id_usage : 2, //Autre
						nom_court: "Nouveau document",
						nom_long: "Nouveau document",
						description: document.querySelector("meta[name=description]") ? document.querySelector("meta[name=description]").content : ""
					}

					retour_json.liste_ressources.splice(0,0, nouveau_document);
					angular.extend($scope.utilisateur_local, retour_json);

					//Sélection de la première valeur par défaut
					$scope.capsule.espace = $scope.capsule.espace || $scope.utilisateur_local.liste_espaces[0];
					$scope.capsule.ressource = $scope.capsule.ressource || $scope.utilisateur_local.liste_ressources[0];

					toaster.pop("success", "Informations chargées");
				} else {
					if (retour_json.message === "ws_utilisateur_invalide") 
						$scope.deconnecter();
					toaster.pop("error", retour_json.message);
				}
			}, function callback_error (erreur_requete){
				toaster.pop("error", JSON.stringify(erreur_requete));
			});
		}

		//Si on ouvre l'injecteur sur ce panneau, il faut initialiser 
	    if ($scope.$storage.pf_injecteur.etape === 3)
	    	$scope.init();
	    
		$scope.gererCapsule = function(formulaireValide) {
			$scope.formulaire_soumis = true;
			// si tout les champs du formulaire sont valides
			if(formulaireValide) {
				$scope.formulaire_en_cours = true;
				parametres_requete = $scope.capsule;
				parametres_requete.ressource.id_espace = $scope.capsule.espace.id_espace;
				parametres_requete.id_ressource = $scope.capsule.ressource.id_ressource;
				parametres_requete.ressource_cree_par = $scope.utilisateur_local.id_utilisateur;
				parametres_requete.selecteurs = $scope.$storage.pf_injecteur.selecteurs.contenu;
				
				toaster.pop("info", "Enregistrement en cours…");
				// on ajoute la capsule en BDD
				PF.put("injecteur/capsule", parametres_requete, function callback_success (retour_json){
					$scope.formulaire_en_cours = false;
					if (retour_json.status === "ok") {
						toaster.pop("success", "LABEL.SUCCES.ENREGISTREMENT");
						$scope.$storage.pf_injecteur.hash_section = retour_json.clef_pairform;
						$scope.changerEtape(4);
					} else {
						// SI le nom_ourt est identique à celui d'une capsule existante
						if (retour_json.message === "ER_DUP_ENTRY") {
							// on indique la précesence du doublon
							$scope.capsule_doublon = true;
							// toaster.pop("error", "Le formulaire a des soucis");
						} else {
							toaster.pop("error", erreur_requete);
						}
					}
				}, function callback_error (erreur_requete){
					$scope.formulaire_en_cours = false;
					console.log("erreur requete http put /webServices/capsule");
					toaster.pop("error", erreur_requete);
				});
			}
		}
	}]);

	pf_injecteur.controller('controller_connexion', ["$scope", "$http", "$localStorage", "toaster", "$translate", function ($scope, $http, $localStorage, toaster, $translate) {
		var defaults = {
			"visible" : false,
			"mode" : "connexion",
			"tableau_langues_label" : PF.langue.tableau_labels,
			// "connexion" : {"username" : "Bigood", "password" : "azerty"},
			"mot_de_passe_oublie" : {},
			"enregistrement" : {
				"id_capsule" : undefined
			}
		};
		// $scope.$watch('visible', function (new_value, old_value) {
		// 	$scope.$parent.visible = new_value;
		// });

		angular.extend($scope, defaults);

		$scope.connecter = function (){
			
			var _scope = $scope;
			PF.post('utilisateur/login', this.connexion, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok')
				{
					var user = new Utilisateur(retour);
					//On cale l'ID elgg et le nom dans le stockage local
					if($localStorage)
					{
						$localStorage.utilisateur_local = user;
					}
					
					
					PF.getScopeOfController("controller_injecteur").setSession();
					// _scope.verifierRole();
					// Notification.setUpdateTimer();

					// updatePostOs();

					toaster.pop("success","LABEL_CONNEXION_REUSSI");
					_scope.visible = false;
					//Mise à jour des messages de la page
					//IMPORTANT : on efface le cache des messages
					// Message.recupererMessages(true);
					ga('send', 'event', 'user', 'login_success', user.username);

				}
				else
				{
					PF.getScopeOfController('controller_connexion').connexion.password = "";
					// $_pf('#popin-connexion').effect("shake");
					
					
					if(retour['message'] == "ws_utilisateur_invalide")
						toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");
					else
						toaster.pop("error", retour['message']);

					ga('send', 'event', 'user', 'login_fail');
				}

			}, function callback_error (data, status, headers, config) {
				
			});
		}
		$scope.connecterGooglePlus = function (){
		}
		$scope.connecterFacebook = function (){
			var
			  _screenX   = typeof window.screenX      != 'undefined'
			    ? window.screenX
			    : window.screenLeft,
			  screenY    = typeof window.screenY      != 'undefined'
			    ? window.screenY
			    : window.screenTop,
			  outerWidth = typeof window.outerWidth   != 'undefined'
			    ? window.outerWidth
			    : document.documentElement.clientWidth,
			  outerHeight = typeof window.outerHeight != 'undefined'
			    ? window.outerHeight
			    : (document.documentElement.clientHeight - 22), 

			  
			  
			  width    = 475,
			  height   = 222,
			  screenX  = (_screenX < 0) ? window.screen.width + _screenX : _screenX,
			  left     = parseInt(screenX + ((outerWidth - width) / 2), 10),
			  top      = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
			  features = [];

			if (width !== null) {
			  features.push('width=' + width);
			}
			if (height !== null) {
			  features.push('height=' + height);
			}
			features.push('left=' + left);
			features.push('top=' + top);
			features.push('scrollbars=1');

			features = features.join(',');

			var windowObjectReference = window.open(
				// "https://www.facebook.com/dialog/oauth?client_id=730511317075431&redirect_uri=" + PF.globals.url.ws + "login/facebook&display=popup&response_type=token&scope=email",
				PF.globals.url.ws + "utilisateur/login/facebook",
				"FB_login", 
				features);

			window.addEventListener("message", function callback (event) {
				var retour = event.data;
				var user = new Utilisateur(retour);
				//On cale l'ID elgg et le nom dans le stockage local
				if($localStorage)
				{
					$localStorage.utilisateur_local = user;
				}
				
				
				PF.getScopeOfController("controller_injecteur").setSession();
				// $scope.verifierRole();
				// Notification.setUpdateTimer();

				// updatePostOs();

				toaster.pop("success", "LABEL_CONNEXION_REUSSI");
				$scope.visible = false;
				//Mise à jour des messages de la page
				//IMPORTANT : on efface le cache des messages
				// Message.recupererMessages(true);
				ga('send', 'event', 'user', 'login_success', user.username);
			}, false);
		}
		
		$scope.checkForLTIToken = function () {
			//Récuperation des parametres passés en GET dans l'URL depuis la passerelle LTI
			var url_params = getSearchParameters();
			//On récupère le token == elggperm
			var token = url_params['access_token'] || url_params['token'];
			//Si on est bien dans le cas d'une connexion LTI (et qu'on a le PF._localStorage)
			if (typeof(token) != "undefined"){

				$localStorage.premiere_visite = true;

				//Important : prévenir le WS qu'on est dans le cas de LTI, en changeant la valeur de version
				var post_params = {
					"access_token" : token
				}
				var _scope = $scope;
				_scope.deconnecter('', function(){
					//Et log de l'utilisateur.
					PF.post('utilisateur/login/lti', post_params, function callback_success (data, status, headers, config) {
						var retour = data;
						if (retour['status'] == 'ok')
						{
							var user = new Utilisateur(retour);
							//On cale l'ID elgg et le nom dans le stockage local
							if($localStorage)
							{
								$localStorage.utilisateur_local = user;
							}
							
							
							PF.getScopeOfController("controller_injecteur").setSession();
							// _scope.verifierRole();
							// Notification.setUpdateTimer();

							// updatePostOs();

							toaster.pop("success", "LABEL_CONNEXION_REUSSI");
							//Mise à jour des messages de la page
							//IMPORTANT : on efface le cache des messages
							// Message.recupererMessages(true);
							ga('send', 'event', 'user', 'login_success', user.username);

						}
						else
						{
							toaster.pop("error", retour['message']);
							ga('send', 'event', 'user', 'login_fail');
						}

					});
				});
			}
		}
		
		function getSearchParameters () {
			var prmstr = window.location.search.substr(1);
			return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
		}

		function transformToAssocArray ( prmstr ) {
			var params = {};
			var prmarr = prmstr.split("&");
			for ( var i = 0; i < prmarr.length; i++) {
				var tmparr = prmarr[i].split("=");
				params[tmparr[0]] = tmparr[1];
			}
			return params;
		}
		
		$scope.renvoyerMotDePasse = function (){
			
			var _scope = $scope;
			PF.post('utilisateur/reset', this.mot_de_passe_oublie, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok') {
					_scope.mode = "enregistrement-1";
				}
				else {
					toaster.pop("error", retour['message']);
				}
			});
		}
		$scope.enregistrer = function (){
			//Attention : register_cgu à la place de register-cgu
			var _scope = $scope;
			PF.put('utilisateur/enregistrer', this.enregistrement, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok') {
					_scope.mode = "enregistrement-1";
					ga('send', 'event', 'user', 'register', _scope.enregistrement.id_capsule);
				}
				else {
					toaster.pop("error", retour['message']);
				}
			});
		}

		//Vérification LTI
		$scope.checkForLTIToken();
	}]);

	angular.element(document).ready(function() {
		angular.bootstrap(document, ['pf_injecteur']);
		$_pf('div[ng-controller=controller_injecteur]').fadeIn(200);
		
	});
});