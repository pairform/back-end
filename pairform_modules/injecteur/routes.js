var web_services = require("../webservices/lib/webServices"),
		 express = require("express"),
		  CONFIG = require("config"),
		  	  fs = require('fs'),
	  	   async = require('async'),
	  nodemailer = require('nodemailer'),
	pairform_dao = require("../webservices/node_modules/pairform_dao/lib/pairformDAO");


app = exports = module.exports = express();

/*
 * Point d'entrée de pairform web app
 * Renvoie import.js / import.min.js (permet d'alterner entre fichier minifié ou pas sans changer l'url pointé dans les ressources web)
*/

// Récupère les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)
app.get("/injecteur", function (req, res) {
	var path;
	//Si on est sur la prod
	if (CONFIG.app.urls.alias_serveur.match(/(pairform.fr)/))
		//On renvoie la version minifiée
		path = "js/injecteur.min.js";
	//Sinon, renvoi de la version simple
	else
		path = "js/injecteur.js";

	//Test dév
	// path = "js/import.min.js";

	res.sendFile(path, {root : __dirname + "/private/"});
});

// app.get("/derniers_messages", function (req, res) {		
// 	res.render(__dirname + "/../authentification/views/email_templates/derniers_messages",
// 		{
// 			utilisateur : {
// 				pseudo : "Bigood"
// 			}

// 		});
// });


//  * Toutes les vues de la web App
//  * Renvoie tous les .jade / html dans views (voir layer.jade)


// //View contenant tous les élements composants l'interface de l'app wzeb
app.get("/injecteur/layer", function (req, res) {		
	res.render(__dirname + "/views/layer");
});
