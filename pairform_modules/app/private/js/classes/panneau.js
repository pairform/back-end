
function Panneau (options) {
	//Au cas où il n'y a pas d'option, on met un objet vide
	options = options || {};
	this.nom_controller = "";
	this.controller_scope = undefined;
	this.view = undefined;
	this.position = "left";
	this.superpose = false;
	this.should_toggle = true;
	this.should_reload = false;
	this.client_rect = {};

	angular.extend(this,options);

	//S'il doit se recharger, il ne peut pas se toggler
	if (this.should_reload)
		this.should_toggle = false;


	this.attacherView(options.view);
	this.attacherScope();
}

Panneau.pile = {
	"left":[],
	"right":[],
	"bottom":[],
	"top":[],
	"center":[],
	"globale":[]
}
Panneau.cacherDernierDeLaPile = function (position, callback) {
	if(Panneau.pile[position].length != 0){
		var _last_controller_scope = PF.getScopeOfController(Panneau.pile[position][Panneau.pile[position].length -1]);
		
		return _last_controller_scope.panneau.cacherPanneau(callback);
	}
	if (typeof(callback) == "function")
		return callback();
	else
		return false;
}

Panneau.enleverDernierDeLaPile = function () {
	return Panneau.pile[this.position].pop();
}

Panneau.prototype.attacherView = function  (view) {
	this.view = this.view || view;
		//Récuperation de la vue correspondante, si l'élément n'est pas passé en paramètre
	if (typeof(this.view) == "undefined"){
		var _view = PF.getController(this.nom_controller);
		if (_view)
			this.view = _view;
		else
			this.view = null;
	}
}
Panneau.prototype.attacherScope = function  (controller_scope) {
	this.controller_scope = this.controller_scope || controller_scope;
	//Récuperation du controller correspondant
	if ((typeof(this.controller_scope) == "undefined") && (this.nom_controller != "")){
		var _scope = PF.getScopeOfController(this.nom_controller);
		if (_scope){
			this.controller_scope = _scope;
			this.controller_scope.init();
			//Référence au panneau passé au controller
			this.controller_scope.panneau = this;
		}
		else
			this.controller_scope = null;
	}
}

Panneau.prototype.enleverDeLaPile = function () {
	//On enlève de la pile globale également
	Panneau.pile["globale"].splice(this.indexDansPileGlobale())
	return Panneau.pile[this.position].splice(this.indexDansPile());
}
Panneau.prototype.ajouterDansPile = function () {
	//On la met dans la pile globale également
	Panneau.pile["globale"].push(this.nom_controller)
	return Panneau.pile[this.position].push(this.nom_controller);
}
Panneau.prototype.indexDansPile = function () {
	return Panneau.pile[this.position].indexOf(this.nom_controller);
}
Panneau.prototype.indexDansPileGlobale = function () {
	return Panneau.pile["globale"].indexOf(this.nom_controller);
}
Panneau.prototype.estAffiche = function () {
	return (this.indexDansPile() >= 0);
}
Panneau.prototype.togglePanneau = function (callback) {
	var index = this.indexDansPile();
	//S'il est affiché
	//S'il ne doit pas se recharger
	if (this.estAffiche() && this.should_toggle) {
		this.cacherPanneau(callback);
	}
	//S'il doit se recharger
	else if (this.estAffiche() && this.should_reload) {
		var that = this;
		Panneau.cacherDernierDeLaPile(this.position,function () {
			that.afficherPanneau(callback);	
		});
	}
	//S'il n'est pas encore affiché
	else if(!this.estAffiche()){
		//S'il ne doit pas se superposer
		if (!this.superpose) {
			var that = this;
			Panneau.cacherDernierDeLaPile(this.position,function () {
				that.afficherPanneau(callback);	
			});
		}
		else{
			this.afficherPanneau(callback);
		}
	}
	//S'il ne doit rien faire, et updater les informations direct dans le panneau -> on fait rien
}

Panneau.prototype.afficherPanneau = function (callback) {
	//On le flag comme visible
	this.controller_scope.visible = true;
	//S'il y a déjà un panneau avec cet identifiant
	if (this.estAffiche()){
		//On le cache, et on rappelle la fonction
		this.cacherPanneau(this.afficherPanneau);
	}
	//On pousse le panneau dans la pile
	this.ajouterDansPile();

	//Nombre de panneaux encastrés, pour définir l'offset
	var index = this.indexDansPile();
	
	//S'il n'y a qu'un élément
	if (index == 0){

	}
	else if (index == 1){
		$_pf(this.view).addClass('panel-container-'+this.position+'-second-level');
	}
	else {
		//Pas normal, on planque!
		//Flag d'invisibilité
		this.controller_scope.visible = false;
	}

	//Positionnement du panneau
	this.positionnerPanneau();

	if (typeof(callback) == "function"){
		return callback();
	}
	return this;
}

Panneau.prototype.positionnerPanneau = function (){
	if(this.client_rect && this.client_rect.bottom && this.client_rect.left){
		var margin = 200,
				offset = 4;
		//Hauteur minimale pour afficher correctement les messges
		if (this.client_rect.bottom + margin > window.innerHeight){
			//On l'affiche au dessus
			this.view.css("top", (this.client_rect.top - margin - offset)  + "px");
		}
		else{
			this.view.css("top", this.client_rect.bottom + offset + "px");
		}

		this.view.css("left", this.client_rect.left + "px");
	}
}

Panneau.prototype.cacherPanneau = function (callback) {
	//On le flag comme visible

	//Nombre de panneaux encastrés, pour définir l'offset
	var index = this.indexDansPile();

	//On le vire de la pile
	this.enleverDeLaPile();;
	//S'il n'y a qu'un élément
	if (index == 0){
		//Flag d'invisibilité
		this.controller_scope.visible = false;
	}
	else if (index == 1){
		$_pf(this.view).removeClass('panel-container-'+this.position+'-second-level');
		//Flag d'invisibilité
		this.controller_scope.visible = false;
	}
	else {
		//Pas normal, on planque!
		//Flag d'invisibilité
		this.controller_scope.visible = false;
	}

	//Suppression de toute classe détachée pour les fenêtres flottantes
	this.view.removeClass("detached");

	if (typeof(callback) == "function"){
		return callback();
	}
	return this;
}

function PanneauModal (options) {
	// Panneau.call(this);
	var _this = new Panneau(options)
	// angular.extend(this,options);
	_this.attacherScope();
	_this.attacherView();
	_this.position = "center";

	_this.controller_scope.panneau_modal = _this;
	
	return _this;
}
// PanneauModal.prototype = new Panneau();
PanneauModal.prototype.constructor = PanneauModal;

PanneauModal.prototype.togglePanneauModal = function (callback) {
	var index = this.indexDansPile();
	//S'il est affiché
	//S'il doit être unique
	if (this.estAffiche() && this.should_toggle) {
		this.cacherPanneau(callback);
	}
	//S'il ne l'est pas encore
	else{
		//S'il ne doit pas se superposer
		if (!this.superpose) {
			Panneau.cacherDernierDeLaPile(this.position);
			this.afficherPanneau(callback);	
		}
		else{
			this.afficherPanneau(callback);
		}
	}
	
}