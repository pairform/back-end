PF.Notification = function Notification (options) {
	this.id_notification = 0;
	this.id_message = 0;
	this.id_utilisateur = 0;
	this.titre = "";
	this.sous_type = "";
	this.contenu = "";
	this.date_creation = "";
	this.date_vue = "";
	this.image = "";
	this.label = "";
	this.points = "";
	this.type_points = "";

	angular.extend(this,options);
}
PF.Notification.setUpdateTimer = function () {
	//Vérification que les fonctions de general.js sont bien là
	if(PF.window_visible_functions && PF.windowVisibleHandler && PF.windowVisibleAddFunction){
		PF.windowVisibleAddFunction({
			id:"notification_timer",
			visible: PF.Notification.launchUpdateTimer,
			hidden: PF.Notification.removeUpdateTimer
		})
	}
}
PF.Notification.launchUpdateTimer = function () {
	if (!PF.globals.timer_notifications){
		PF.globals.timer_notifications = setInterval(PF.Notification.updateAll, 30000);
	}
}

PF.Notification.removeUpdateTimer = function () {
	if (PF.globals.timer_notifications){
		clearInterval(PF.globals.timer_notifications);
		delete PF.globals.timer_notifications;
	}
}

PF.Notification.updateAll = function () {
	var param = {};
	//Uniquement si on est sur une capsule
	if (PF.globals && PF.globals.capsule && PF.globals.capsule.id_capsule) 
		param.id_capsule = PF.globals.capsule.id_capsule;

	PF.get('notification', param, function (data, status, headers, config){
			   
		var retour = data;
		var scope = PF.getScopeOfController('controller_nav_bar');
		//Si ya pas de soucis
		if (retour['status'] == 'ok')
		{
			if (PF.globals && PF.globals.capsule && PF.globals.capsule.id_capsule) 
				scope.utilisateur_local.getCategorieForCapsule(scope.capsule.id_capsule).score = retour['score'];
			scope.notifications.us = retour['us'];
			scope.notifications.un = retour['un'];
			scope.notifications.count_us = retour['count_us'];
			scope.notifications.count_un = retour['count_un'];
		}
		else{
			if (retour['message'] == "ws_utilisateur_invalide") {
				scope.deconnecter("WS_ERREUR_SUPPRESSION");
			}
		}
	});
}
PF.Notification.updateAllWithDelay = function (delay){
	delay = delay || 1000;
	setTimeout(PF.Notification.updateAll, delay);
}
PF.Notification.prototype.voirContexte = function () {
	//Score utilisateur
	if (this.type == "us"){
		//Si on est dans le cas d'un succes, il n'y a pas de message attaché
		if (!this.id_message){
			displayProfil();
		}
		else{
			PF.redirigerVersMessage(this.id_message);
		}
	}
	//Notification utilisateur
	else{
		switch(this.sous_type){
			// dc : défi créé 
			case "dc" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
			// dt : défi terminé 
			case "dt" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
			// dv : défi validé 
			case "dv" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
			// ar : ajout réseau
			case "ar" : {
				displayProfil(this.contenu);
				break;
			}
			// cr : classe rejointe
			case "cr" : {
				displayProfil(this.contenu);
				break;
			}
			// ru : réponse d'utilisateur
			case "ru" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
			// gm : gagné médaille
			case "gm" : {
				PF.redirigerVersMessage(this.id_message);
				break;
			}
		}
	}
}