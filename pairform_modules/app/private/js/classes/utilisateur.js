function Utilisateur (options) {

	this.id_utilisateur = 0;
	this.username = "";
	this.name = "";
	this.avatar_url = "";
	this.etablissement = "";
	this.email = "";
	this.rank = {};
	if (PF.globals.capsule && PF.globals.capsule.id_capsule)
		this.rank[PF.globals.capsule.id_capsule] = {
			"id_categorie" : 0,
			"nom_categorie": "Lecteur",
			"score": 0
		};
	this.estExpert = false;
	this.elggperm = "";
	this.langue_principale =  PF._localStorage && PF._localStorage["ngStorage-code_langue_app"] ? PF._localStorage["ngStorage-code_langue_app"][0]  : "fr";
	this.autres_langues = ["1","2"];
	this.categorie = {};
	this.est_admin = false;

	this.est_connecte = false;
	this.preferences = {
		"expanded" : false,
		"ordreTri" : 0,
	};

	angular.extend(this,options);
	this.rank_ressources = {};

	var _this = this;
	$_pf.each(this.rank, function (id_capsule, categorie) {
	 	if(!_this.rank_ressources[categorie.id_ressource])
	 		_this.rank_ressources[categorie.id_ressource] = categorie;
	});
	

	//Au cas où il n'y a pas le bon champ renvoyé par le webServices
	if (this.username == "")
		this.username = this.name;
	else if (this.username == "")
		this.name = this.username;
	
}

Utilisateur.prototype.getCategorieForCapsule = function (id_capsule){
	if (!this.rank[id_capsule]) {
		this.rank[id_capsule] = {
			"id_categorie" : 0,
			"nom_categorie": "Lecteur",
			"score": 0
		}
	}
	return this.rank[id_capsule];
}
//Renvoie l'id_categorie
Utilisateur.prototype.getRankForRessource = function (id_ressource){
	for (var i = this.rank.length - 1; i >= 0; i--) {
	 	if(id_ressource == this.rank[i].id_ressource)
	 		return parseInt(this.rank[i].id_categorie);
	 }; 
}
//Renvoie l'objet categorie
Utilisateur.prototype.getCategorieForRessource = function (id_ressource){
	for (var i = this.rank.length - 1; i >= 0; i--) {
	 	if(id_ressource == this.rank[i].id_ressource)
	 		return this.rank[i];
	 }; 
}