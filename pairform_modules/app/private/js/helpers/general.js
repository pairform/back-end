//Utilisation de notre propre instance de jQuery, sans polluer le DOM
$_pf = window.$_pf || jQuery.noConflict(true);

PF.matches = function(el, selector) {
  el = el[0] || el;
  return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
}
PF.getMatchedSelector = function(el, selectors) {
	var selector_list = selectors.split(",");
	for (var i = 0; i < selector_list.length; i++) {
		if(PF.matches(el, selector_list[i]))
			return selector_list[i].trim();
	}
}

PF.getElement = function (query, parent){
	if (parent)
		query = parent + " " + query;
	return angular.element(document.querySelectorAll(query));
}
PF.init = function (){

	//S'il y a des OA definis sur la page, et qu'on a un id de page
	if ((($_pf('[data-oauid]:not([data-oauid=""])').length != 0) || ($_pf('meta[name="uid_page"]').not('[content=""]').length != 0)))
	{
		//On utilisera la version de base d'attachement de messages
		PF.globals.version = "1.5";
		PF.globals.styleAttacheMessages = "OA";
		PF.globals.uid_page = $_pf('meta[name="uid_page"]').attr('content');
	}
	//S'il n'y a pas d'OA definis sur la page
	else
	{
		//On utilisera la version avance d'attachement des messages par identifiants uniques
		PF.globals.version =  "2";
		PF.globals.styleAttacheMessages = "AUTO";
		// var nom_page = window.location.pathname.split("/")[window.location.pathname.split("/").length - 1];
		//S'il n'y a pas de nom de page (cas d'un chemin finissant par "/"), on met "index"
		// PF.globals.nom_page = nom_page.split(".")[0] || "index";
	}

	PF.updateNomPage();

	if (PF.globals.id_res == ID_RES_TEST)
		PF.displayAlert("Attention, vous êtes dans un environnement de test ; les messages ont une durée d'expiration d'une heure.","info");

	//S'il n'y a pas de sélecteur déclaré
	if (!PF.globals.selector) {
		//On essaie de les deviner
		//Selecteur d'éléments
		if(PF.isScenari())
		{
			PF.globals.selector = ".mainContent_co p, .mainContent_co .resInFlow, .mainContent_ti";
			PF.globals.selectorRes = "#label-title";
			PF.globals.selectorPage = "none";
			PF.globals.selectorContent = ".mainContent_co";
		}
		else if(PF.isFromLatex()){
			PF.globals.selector = "#globalWrapper span";
			PF.globals.selectorRes = "#label-title";
			PF.globals.selectorPage = "#globalWrapper";
			PF.globals.selectorContent = "body";
		}
		else if(PF.isSpecialCase()){
			//On ne fait rien, les selecteurs sont déclarés dans la page
		}
		//Sinon, c'est qu'on est sur profeci
		else
		{
			PF.globals.selector = "*[data-oauid]:not(body)";
			// PF.globals.selector = ".feedback, .inner, div.object-properties, a.diagram_link, table.display, table.html-grid";
			PF.globals.selectorRes = "#label-title";
			PF.globals.selectorPage = ".inner > h2";
			PF.globals.selectorContent = "body";
		}
	}
	//De toute façon, les commentaires sont postés sur le titre dans la barre,
	// quel que soit le format
	PF.globals.selectorRes = "#label-title";
	PF.globals.selectorAll = PF.globals.selector + ", " + PF.globals.selectorPage + ", " + PF.globals.selectorRes;

	// $_pf(PF.globals.selectorAll).attr('pf-commentable', '');

	// $_pf(document).on('click', PF.globals.selectorAll, function handlePFClick(event) {
	// 	// PF.addFocusedItem($_pf(this));
	// 	Message.afficher($_pf(this)); 
	// 	event.stopPropagation();
	// });
	// PF._localStorage.array_messages_lus = "[]";
}

/* A utiliser pour les SPA (single page application).
	Permet à PairForm de prendre en compte le changement de contenu
	asynchrone pour afficher les messages dans leur contexte.

Arguments :
 - @html (optionnel) [string || DOMelement || jquery DOMElement]
 	Sélecteur CSS ou élément HTML (jquery-wrapped ou pas) mis à jour
 - @nom_page (optionnel) [string] - nouvelle url de page
 	Nouvelle URL complète de la page (https://ihate.fun/doc.html)
*/

PF.updateHTML = function (html, url) {
	PF.updateNomPage(url);
	//Et sur les liens de page
	PF.compileLinks(null, true);
	PF.compileContent(html);
}

PF.updateNomPage = function (url) {
	var url_page_actuelle_sans_protocole = url ? url.replace(/https?:\/\//, "") : window.location.host + window.location.pathname,
		url_web_section_sans_protocole = PF.globals.capsule.url_web.replace(/https?:\/\//, "");

	PF.globals.nom_page =  url_page_actuelle_sans_protocole.replace(url_web_section_sans_protocole, "");

}
PF.normaliserURL = function (url) {
	//Merci à ce mec pour cette idée géniale : http://stackoverflow.com/a/14781678/1437016
    var link = document.createElement("a");
    link.href = url;
    var abs_url = link.host+link.pathname+link.search+link.hash;
	return abs_url.replace(PF.globals.capsule.url_web.replace(/https?:\/\//, ""), "");
}

PF.compileContent = function (html) {
	//Si on passe un selecteur, on chope l'objet associé
	if (typeof html == "string")
		html = $_pf(html);
	else
		//Html par défaut : tout enfant du body, qui n'est pas un élément pairform
		html = html || $_pf("body > :not(.pf,input,#toast-container)");

	//Rajout de l'attribut pf-commentable sur les grains
	$_pf(PF.globals.selector, html).add(PF.globals.selectorRes).attr('pf-commentable', '');


	//Compilation de ce nouveau html
  	var app = document;
	angular.element(app).injector().invoke(["$compile", function($compile) {
			$scope = PF.getScopeOfController('main_controller');
			$compile(html)($scope);
			// Finally, refresh the watch expressions in the new element
	
			var phase = $scope.$root.$$phase;
			if(phase != '$apply' && phase != '$digest')
				$scope.$apply();
		}]);
}
PF.compileLinks = function (array_messages_capsule, flag_compile) {
	array_messages_capsule = array_messages_capsule || angular.element(document).injector().get('$localStorage').array_messages[PF.globals.capsule.id_capsule];
	// Pour pouvoir choisir les liens où l'on souhaite afficher les messages 
	var a;
	if ($_pf("[pf-link-defined]")) {
		//On prend tous les liens de la page
		a = $_pf('a[href]:not([href^=#])');
		//On prend tous les liens qui n'ont pas l'attribut pf-link (qui ne sont pas des ancres) 
		//dans les endroits définis par l'utilisateur
		//On intersecte tous les liens qui ne sont pas définis explicitement
		a = a.not($_pf("[pf-link-defined] a[href]:not([pf-link]):not([href^=#])"));
		
	}
	//S'il n'y en a pas
	else {
		//On prend tous les liens qui ne sont pas des ancres
		a = $_pf('a[href]:not([href^=#])');
	}
	
	a.filter(function (index, object) {
		var normalized = PF.normaliserURL($_pf(this).attr('href'));
		return typeof(array_messages_capsule[normalized]) !== "undefined";
	});
	a.attr('pf-commentable', '');
	//Lien qui sont des ancres
	$_pf('a[href^=#]:not([href=#])').each(function (index, element) {
		//On rajoute le target self pour qu'angular ne les considère pas comme des routes
		element.setAttribute("target", "_self");
	})
	if (flag_compile) {
		var $compile = angular.element(document).injector().get('$compile');
		$compile(a)(PF.getScopeOfController('main_controller'));
	}

}
PF.displayAlert = function () {}

PF.toObject = function (array) {
	var rv = {};
	for (var i = 0; i < array.length; ++i)
	{
		var id_post = array[i].id_message;
		rv[id_post] = array[i];
	}	  	
	return rv;
}
PF.pageIsMenu = function (url) {
	if(typeof(url) == "undefined")
		//Defaut : page courante	
		var url = window.location.pathname.split('/')[window.location.pathname.split('/').length -1 ];
	
	//On test s'il est de classe ueDiv.
	var result = $_pf('.mnu li div[id="'+ url +'"].mnu_b');
	
	return result.length == 0 ? false : true;
}

PF.getController = function (nom_controller){
	var controller = document.querySelector('[ng-controller="'+ nom_controller +'"]');
	return typeof(controller) != "undefined" ? angular.element(controller) : null;
}

PF.getScopeOfController = function (nom_controller){
	var controller = PF.getController(nom_controller);
	return (controller ? controller.scope() : {});
}
PF.getMainScope = function (){
	var controller = PF.getController('main_controller');
	return (controller ? controller.scope() : {});
}
PF.redirigerVersMessage = function(id_message){
	window.location.replace(PF.globals.url.ws + "message/rediriger?id_message=" + id_message);
}


if (!Array.prototype.findIndex) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function(predicate) {
      'use strict';
      if (this == null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
      }
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }
      var list = Object(this);
      var length = list.length >>> 0;
      var thisArg = arguments[1];
      var value;

      for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
          return i;
        }
      }
      return -1;
    },
    enumerable: false,
    configurable: false,
    writable: false
  });
}

PF.window_visible_functions = [];

PF.windowVisibleHandler = function() {
  var hidden = "hidden";

  // Standards:
  if (hidden in document)
    document.addEventListener("visibilitychange", onchange);
  else if ((hidden = "mozHidden") in document)
    document.addEventListener("mozvisibilitychange", onchange);
  else if ((hidden = "webkitHidden") in document)
    document.addEventListener("webkitvisibilitychange", onchange);
  else if ((hidden = "msHidden") in document)
    document.addEventListener("msvisibilitychange", onchange);
  // IE 9 and lower:
  else if ("onfocusin" in document)
    document.onfocusin = document.onfocusout = onchange;
  // All others:
  else
    window.onpageshow = window.onpagehide
    = window.onfocus = window.onblur = onchange;

  function onchange (evt) {
  	var key = "visible";
  	if (this[hidden])
  		key = "hidden";

		PF.window_visible_functions.forEach(function (item) {
			item[key] && item[key](); 
		})
  }

  //Lance les fonctions
	PF.window_visible_functions.forEach(function (item) {
		item.visible && item.visible(); 
	})
};
PF.windowVisibleAddFunction = function (fun) {
	var is_already_registred = PF.window_visible_functions.find(function (item) {
		return item.id == fun.id;
	})

	if (!is_already_registred) {
		PF.window_visible_functions.push(fun);
		fun.visible && fun.visible();
	}
}
//Lancement immédiat
PF.windowVisibleHandler();

// PF.windowVisibleAddFunction({
// 	id:"test",
// 	visible:function visible() {
// 		PF._test = setInterval(console.count, 1000);
// 	},
// 	hidden:function hidden() {
// 		PF._test && clearInterval(PF._test);
// 	},
// })