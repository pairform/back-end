//Utilisation de notre propre instance de jQuery, sans polluer le DOM
$_pf = window.$_pf || jQuery.noConflict(true);


PF.http = function (argument) {
	if (typeof(PF._http) == "undefined"){
		PF._http = angular.element(document).injector().get('$http');	
	}
	return PF._http;
} 

PF.get = function (nom_webservice, data_requete, callback_succes, callback_echec) {
	PF.request('get', nom_webservice, data_requete, callback_succes, callback_echec);
}
PF.post = function (nom_webservice, data_requete, callback_succes, callback_echec) {
	PF.request('post', nom_webservice, data_requete, callback_succes, callback_echec);
}
PF.put = function (nom_webservice, data_requete, callback_succes, callback_echec) {
	PF.request('put', nom_webservice, data_requete, callback_succes, callback_echec);
}
PF.delete = function (nom_webservice, data_requete, callback_succes, callback_echec) {
	PF.request('delete', nom_webservice, data_requete, callback_succes, callback_echec);
}
PF.request_with_file = function (methode, nom_webservice, data_requete, files, callback_succes, callback_echec) {
	var xhr = PF.http(),
		url_ws = PF.globals.url.ws + nom_webservice,
		http_config = {
	        method: methode,
	        url: url_ws,
			transformRequest: angular.identity,
	        headers: { 'Content-Type': undefined },
			withCredentials: true
	    };

	var formData = new FormData();
	formData.append("os", PF.globals.os);
	formData.append("version", PF.globals.version);


	for (var clef in data_requete) {
	    if (data_requete.hasOwnProperty(clef)) {
	        formData.append(clef, data_requete[clef]);
	    }
	}

	if(files && files.length){
		angular.forEach(files, function(file, index){
			//Inclusion de la position actuelle de la pièce jointe côté client
			file.file.index = index;
			formData.append("position_" + file.name, index); //Index de la PJ
			formData.append(file.name, file.file); //PJ
			formData.append(file.thumbnail_name, file.thumbnail_image); //Thumbnail
		
			console.log("["+ file.name +"]");
			console.log("Thumbanil ["+ file.thumbnail_name +"]");
		});
	}

	if ((methode == "post") || (methode == "put"))
		http_config.data = formData;
	else
		http_config.params = formData;


	var request = xhr(http_config);

	if (typeof(callback_succes) == "function")
    	request.success(callback_succes);
	if (typeof(callback_echec) == "function")
    	request.error(callback_echec);
}

PF.request = function (methode, nom_webservice, data_requete, callback_succes, callback_echec) {
	var xhr = PF.http(),
		url_ws = PF.globals.url.ws + nom_webservice,
		data_requete_plus = {"os" : PF.globals.os, "version" : PF.globals.version},
		http_config = {
	        method: methode,
	        url: url_ws,
	        withCredentials: true
	    };

	// var elggperm = PF._localStorage['ngStorage-session'] ? JSON.parse(PF._localStorage['ngStorage-session']).elggperm : ""; 
	// if(elggperm)
	// 	data_requete_plus.elggperm = elggperm;

    if (typeof(data_requete) == "object")
    	angular.extend(data_requete_plus, data_requete);

	if ((methode == "post") || (methode == "put"))
		http_config.data = data_requete_plus;
	else
		http_config.params = data_requete_plus;


	var request = xhr(http_config);

	if (typeof(callback_succes) == "function")
    	request.success(callback_succes);
	if (typeof(callback_echec) == "function")
    	request.error(callback_echec);
    else {
    	request.error(function (data, status, headers, config) {
			var toaster = angular.element(document).injector().get('toaster');
    		
    		if (status == 0) {
				toaster.pop("warning", "WS_ERREUR_URL_MISMATCH");
    			console.log("WS_ERREUR_URL_MISMATCH");
    		}
    		else{
				toaster.pop("error", "WS_ERREUR_CONTACTER_PAIRFORM");
			}
    	});
    }
} 

//Intercepteur par défaut
PF.http.interceptor = {
  //Regex de captation
  regex: /.*/,		// /\/api\/forum\/(.*?)\?/
  //Flag pour ne pas réactiver l'intercepteur plusieurs fois
  activated: false,
  //Callback par défaut de l'intercepteur, pour le debug.
  // A overrider
  // - xhr : l'objet XHR. Contient l'URL dans xhr._url.
  // - callback : le callback originel, nullable
  callback: function (xhr, callback) {
	  //self.response
    console.trace(xhr.response);
    return (callback && callback());
  },
  //Activation de l'intercepteur : override de XHR pour passer sur tous les 
  activate: function () {
  	if (this.activated)
  		return console.error("L'intercepteur est déjà activé.");  		

  	this.activated = true;

		(function(XHR) {
		    "use strict";

		    var open = XHR.prototype.open;
		    var send = XHR.prototype.send;

		    XHR.prototype.open = function(method, url, async, user, pass) {
		        this._url = url;
		        open.call(this, method, url, async, user, pass);
		    };

		    XHR.prototype.send = function(data) {
		        var self = this;
		        var oldOnReadyStateChange;
		        var url = this._url;

		        function onReadyStateChange() {
		            // console.log(url + " : state #" + self.readyState)
		            if(self.readyState == 4 /* complete */) {
		                PF.http.interceptor.callback(self, url, oldOnReadyStateChange)
		            }
		        }

		        /* Set xhr.noIntercept to true to disable the PF.http.interceptor for a particular call */
		        if(!this.noIntercept) {
		            
		          if (url.match(PF.http.interceptor.regex)) {

		            if(this.addEventListener) {
		                this.addEventListener("readystatechange", onReadyStateChange, false);
		            } else {
		                oldOnReadyStateChange = this.onreadystatechange; 
		                this.onreadystatechange = onReadyStateChange;
		            }
		          }
		        }

		        send.call(this, data);
		    }
		})(XMLHttpRequest);

  }
}

