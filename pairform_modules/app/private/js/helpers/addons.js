PF.addons = {};

PF.addons.activate360Learning = function (html, url) {
  var content_zone = ".screen-wrapper";

  //Utilisation de l'intercepteur http global pour capter les changements de page
  //Dans ce cas, des URLs genre :
  // - https://einstein.360learning.com/api/forum/sheets/58ca3e862c43434c585b248e?token=7e963881e11fbb5658a00760afd6f82d&_=1497286364927
  PF.http.interceptor.regex = /\/api\/forum\/(.*?)\?/;
  
  PF.http.interceptor.callback = function (xhr, callback) {
    //"{"posts":[],"target":{"_id":"564d8b06e6310d9971db6145"…
    var data = JSON.parse(xhr.response);
    //MAJ du nom de la page, et compilation du contenu de la page
    PF.updateHTML(content_zone, data.target._id);

    return (callback && callback());
  }
} 