
var LARGEUR_PANNEAU_PX = "400px",
	LARGEUR_PANNEAU = 400,
	TEMPS_ANIMATION = 200,
	ID_RES_TEST = "51";

//Si on est sur le site vitrine
if(document.querySelector('meta[name="pf_vitrine"]','head'))
	PF.globals.is_vitrine = true;

if (PF.globals.is_vitrine && PF.initVitrine) {
	PF.initVitrine(pf_app);
}

//On ajoute une classe pf-ios-X, pf-ios-X-X et , pf-ios-X-X-X pour adaptations CSS liées à des bugs
//Genre https://hackernoon.com/how-to-fix-the-ios-11-input-element-in-fixed-modals-bug-aaf66c7ba3f8
var ios_match = navigator.platform.match(/iP(hone|od|ad)/)
if (ios_match) {
  // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
  var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/),
      version = [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
  ["pf-" + ios_match[0].toLowerCase(), "pf-ios-" + version[0], "pf-ios-" + version[0] + "-" + version[1], "pf-ios-" + version[0] + "-" + version[1] + "-" + version[2]].forEach(function (e) {
    document.querySelector("html").classList.add(e);
  });
}
//Addon 360Learning pour changements de pages

if (document.location.host.match(/360learning/)){
	PF.addons.activate360Learning();
}
/*
 *	Internationalisation
 */

pf_app.config(["$translateProvider", function ($translateProvider) {
	$translateProvider.useSanitizeValueStrategy('sanitizeParameters');
	$translateProvider.useStaticFilesLoader({
		prefix: PF.globals.url.private + 'json/',
		suffix: '/strings.json'
	});
	//Anglais par défaut, si le navigateur n'a pas la propriété définie
	//userLanguage pour IE
	var code_langue = PF._localStorage["ngStorage-code_langue_app"] ? JSON.parse(PF._localStorage["ngStorage-code_langue_app"])[0] : (window.navigator.userLanguage || window.navigator.language || "en").substr(0, 2);
	
	//S'il n'y a pas de langue compatible
	if (!PF.langue.tableau_inverse[code_langue])
		code_langue = "en";

	$translateProvider.preferredLanguage(code_langue);
	PF._localStorage.setItem("ngStorage-code_langue_app", JSON.stringify([code_langue]));

}]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = true;
  cfpLoadingBarProvider.parentSelector = ".pf.user_menu";
   cfpLoadingBarProvider.spinnerTemplate = "<div class='loading-bar-spinner'><div class='spinner-icon'></div><div class='spinner-icon-reverse'></div></div>";
	cfpLoadingBarProvider.includeBar = false;
}]).config(['$httpProvider', function($httpProvider) {
	// $httpProvider.defaults.useXDomain = true;
	// $httpProvider.defaults.withCredentials = true;
	// $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]).config(['ThumbnailServiceProvider', function(ThumbnailServiceProvider) {
    ThumbnailServiceProvider.defaults.width = 200;
    ThumbnailServiceProvider.defaults.height = 200;
    ThumbnailServiceProvider.defaults.keep_ratio = "height";
}])
.config(["$locationProvider", function($locationProvider) {
	//Sur le site vitrine, on ne peut pas mettre le mode HTML5, ça pête les routes et 
	//le tandem node / angular ne marche plus, node croit qu'on l'appelle direct en cas de lien direct
	if(!PF.globals.is_vitrine)
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: false
    });
}])