pf_app.directive('pfMessageList', ["$document", function ($document) {

  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      //Gestion du déplacement

      var startX, startY, initialMouseX, initialMouseY, margin_offscreen = 40;
      var dragger = angular.element(element[0].querySelector("#message-list-header"));
      scope.init = function () {
        element.removeClass("detached");
      }
      dragger.on('mousedown touchstart', function($event) {
        //Si le drag se fait sur un bouton de la barre (fermer ou agrandir / réduire)
        if($event.originalEvent && $event.originalEvent.target.classList.contains("btn-cross-fermer")){
           //On stop le drag et on laisse l’event bubbler
          return true;
        }
        $_pf("body").addClass("body-dragging");

        initialMouseX = ($event.clientX || ($event.touches || (($event.originalEvent && $event.originalEvent.touches) ? $event.originalEvent.touches : [{pageX:null}]))[0].pageX);
        initialMouseY = ($event.clientY || ($event.touches || (($event.originalEvent && $event.originalEvent.touches) ? $event.originalEvent.touches : [{pageY:null}]))[0].pageY);
        var bubble = false;
        //Si on n'est pas encore en train de déplacer
        if (!startX && !startY) {
          //On n'empêche pas l'event de bubbler
          bubble = true;
        }
        if (initialMouseX && initialMouseY) {
          element.removeClass("expanded");
          element.addClass("detached");
          startX = element.prop('offsetLeft');
          startY = element.prop('offsetTop');
          $document.on('mousemove touchmove', mousemove);
          $document.on('mouseup touchend', mouseup);
          return bubble;
        }
      });

      function mousemove($event) {
        if (typeof startY != "undefined") {

          //Touch à prévoir
          var dx = ($event.clientX || ($event.touches || (($event.originalEvent && $event.originalEvent.touches) ? $event.originalEvent.touches : [{pageX:null}]))[0].pageX) - initialMouseX;
          var dy = ($event.clientY || ($event.touches || (($event.originalEvent && $event.originalEvent.touches) ? $event.originalEvent.touches : [{pageY:null}]))[0].pageY) - initialMouseY;

          //On ne fait ce redimensionnement que dans le cas où on n'est pas sur mobile
          if (dx && dy && window.innerWidth >= 480) {
            var finalY = startY + dy,
                finalX = startX + dx;
            //Anti-offscreen de la barre de déplacement pour l'horizontale
            if (finalX < 0)
              finalX = 0;
            else if (finalX > window.innerWidth - margin_offscreen)
              finalX = window.innerWidth - margin_offscreen;

            //Anti-offscreen de la barre de déplacement pour la verticale
            if (finalY < 0)
              finalY = 0;
            else if (finalY > window.innerHeight - margin_offscreen)
              finalY = window.innerHeight - margin_offscreen;

            //Redimensionnement de la fenêtre des commentaires
            //pour la compresser lorsqu'on est en bas de l'écran
            if (element[0].clientHeight + finalY > window.innerHeight)
              element.css({height: (window.innerHeight - finalY) + 'px'})

            element.css({
              top: finalY  + 'px',
              left: finalX + 'px'
            });

            $event.stopPropagation();
            $event.preventDefault();
          }
        }
        return false;
      }

      function mouseup() {
        $document.unbind('mousemove touchmove', mousemove);
        $document.unbind('mouseup touchend', mouseup);

        $_pf("body").removeClass("body-dragging");


        startX = null;
        startY = null;
      }
    }
  };
}]);
