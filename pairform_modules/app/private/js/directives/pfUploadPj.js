
pf_app.directive('pfUploadPj', ['$timeout', 'toaster', 'ThumbnailService', function($timeout, toaster, ThumbnailService){

	return {
		restrict: 'E',
		template: 	'<div id="pjs-layout">'+
						'<div ng-repeat="piece_jointe in pieces_jointes track by $index">'+
							'<img ng-if="piece_jointe.display_thumbnail_image" ng-src="{{piece_jointe.display_thumbnail_image}}"/>'+
							'<div class="pj-file" ng-if="!piece_jointe.display_thumbnail_image">.{{piece_jointe.extension}}</div>'+
							'<span class="btn-fermer-pj" ng-click="supprimerPJ($index, piece_jointe)">x</span>' +	
						'</div>'+
						'<div id="nouvelle-pj" '+
							'ngf-drop ngf-select ng-model="files" '+
							'ngf-drag-over-class="dragover" '+
							'ngf-multiple="true" '+
							'ngf-allow-dir="false" '+
							'accept="'+ PF.globals.types_mime +'" '+
							'ngf-pattern="\''+ PF.globals.types_mime +'\'">+</div>'+
					'</div>'/* +
					'<div id="cropLayout" class="pjCropArea">' +
						'<img-crop area-type="square" image="imageSelectionne" result-image="imageModifie"></img-crop>' +
						'<span class="btn-valider-crop" ng-click="validerCrop()">ok</span>' +
						'<span class="btn-fermer-pj" ng-click="fermerCrop(200)">x</span>' +
					'</div>'*/
					,
	    link: function ($scope, element, attrs) {
	    	//$scope.imageSelectionne = ''; //Utilisé pour le crop
	    	$scope.pjSelectionne = null;
	    	$scope.imageModifie = '';

	    	//Obligatoire pour le fonctionnement du drag and drop
	    	//Surveille si l'utilisateur dépose un fichier, ou en sélectionne un, à travers la modification du model
	    	$scope.$watch('files', function () {
		        $scope.afficheFichier($scope.files);
		    });
	    	//Méthode affichant le fichier selectionné en vignette
			$scope.afficheFichier = function(files){
				
				if (files && files.length) { //Pour tous les fichiers sélectionnés
					angular.forEach(files, function(file){
						//Si le fichier n'est pas dans le bon format
						if (file.$error === "pattern") {
							toaster.error("Le fichier n'a pas le format autorisé (jpg, jpeg, png)");
						}
						else{
							var file_reader = new FileReader();

							file_reader.onload = function (event) {
								// le timeout permet d'attendre que l'image soit chargée avant de la mettre à jour à l'écran, sinon, le chargement est trop rapide et l'image n'est pas initalisée
								// $timeout(function () {    
								var file_extension = file.name.split('.').pop(),
									pj = {
										file: file, //Je conserve le fichier pour l'uplaod
										data: event.target.result, //Je garde le contenu le temps de générer à l'envoi
										extension: file_extension, //ajout de l'extension
										name : "pj-" + $scope.pieces_jointes.length + "." + file_extension,
										thumbnail_name : "pj-" + $scope.pieces_jointes.length + "-thumbnail.png",
										type : file.type.match(/^(.*?)\/.*$/) ? file.type.match(/^(.*?)\/.*$/)[1] : null,
									},
									options = {
										media_type:pj.type,
										media_extension:pj.extension
									};
								
								//Si le type MIME est inconnu
								if (!pj.type) {
									return;
								}

								ThumbnailService.generate(pj.data, options).then(
					            	function success(data) {
					            		console.log("Thumbnail generation ok");
					            		pj.display_thumbnail_image = data;
					            		pj.thumbnail_image = data.replace(/(data:.*?base64,)/, "");													
										//Récupération des données EXIF via exif-js
										EXIF.getData(pj.file, function () {
											$scope.$apply(function () {
												$scope.pieces_jointes.push(pj);
												$scope.$root.$broadcast("pf:pieces_jointes:ajout", $scope.pieces_jointes)
											})
										});
										//Je rajoute dans le model le fichier qui va s'afficher dans la directive
					            	},
					            	function error(reason) {
					              		console.log('Error de generation de thumbnail ... que faire ? :(');
					            });
							};
							file_reader.readAsDataURL(file);
						}
					});
				}
			}

			//Supprime une pièce jointe du message
			$scope.supprimerPJ = function(index, pj){
				$scope.pieces_jointes.splice(index, 1);
			}

			////////////////////////
			//Méthode pour le crop//
			////////////////////////

			/*$scope.cropImage = function(index, pj){
				$scope.imageSelectionne = pj.image;
				$scope.pjSelectionne = pj;
				$_pf("#cropLayout").show(200);
			}

			$scope.validerCrop = function(){
				$scope.pjSelectionne.image = $scope.imageModifie;
				$scope.fermerCrop(200);
			}

			$scope.fermerCrop = function(timer){
				$_pf("#cropLayout").hide(timer);
			}
			$scope.fermerCrop(0);*/
	  	}
	};
}]);