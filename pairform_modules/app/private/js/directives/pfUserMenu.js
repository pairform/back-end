pf_app.directive('pfUserMenu', ["$document", function ($document) {
  
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      //Gestion du déplacement vertical uniquement
      var startY, initialMouseY, margin_offscreen = 60;
      var dragger = angular.element(element[0].querySelector(".user_badge"));

      dragger.on('mousedown touchstart', function($event) {
        startY = element.prop('offsetTop');
        initialMouseY = ($event.clientY || ($event.touches || $event.originalEvent.touches)[0].pageY);
        $document.on('mousemove touchmove', mousemove);
        $document.on('mouseup touchend', mouseup);
        $_pf("body").addClass("body-dragging");

        return false;
      });

      function mousemove($event) {
        if (typeof startY != "undefined") {
          //Touch à prévoir
          var dy = ($event.clientY || ($event.touches || $event.originalEvent.touches)[0].pageY) - initialMouseY;

          var finalY = startY + dy;
          //Anti-offscreen de la barre de déplacement pour la verticale
          if (finalY < 0)
            finalY = 0;
          else if (finalY > window.innerHeight - margin_offscreen)
            finalY = window.innerHeight - margin_offscreen;

          element.css({
            top:  finalY + 'px'
          });
          $event.stopPropagation();
          $event.preventDefault();
        }
        return false;
      }

      function mouseup($event) {
        //Pour mobiles uniquement
        //Si l'avatar n'a pas bougé
        if ($event.handleObj.type == "touchend" 
          && Math.round(startY) == Math.round($event.target.getBoundingClientRect().y)) {
          //On trigger un clic
          element.toggleClass("active");
        }
        startY = null;
        $document.unbind('mousemove touchmove', mousemove);
        $document.unbind('mouseup touchend', mouseup);
        $_pf("body").removeClass("body-dragging");
      }
    }
  };
}]);