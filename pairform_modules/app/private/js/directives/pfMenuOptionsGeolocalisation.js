pf_app.directive('pfMenuOptionsGeolocalisation', function () {
	return {
		restrict: 'E',
		template: 	'<div class="message-options-geolocalisation-container ui-tooltip-top">'+
						'<pf-carte-message id="menu_message" data="pieces_jointes" style="width:200px;height:140px"/>' +
					'</div>'
	};
});