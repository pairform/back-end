pf_app.directive('pfMenuMedaille', function () {
	return {
		restrict: 'E',
		template: '<div class="comDonnerMedaille-container ui-tooltip-bottom">' +
						'<img class="comDonnerMedaille-option" ng-click="donnerMedaille(message, \'or\')" ng-src="{{res_url_private}}img/medaille_or.png" title="Attribuer une médaille d\'or">' +
						'<img class="comDonnerMedaille-option" ng-click="donnerMedaille(message, \'argent\')" ng-src="{{res_url_private}}img/medaille_argent.png" title="Attribuer une médaille d\'argent">' +
						'<img class="comDonnerMedaille-option" ng-click="donnerMedaille(message, \'bronze\')" ng-src="{{res_url_private}}img/medaille_bronze.png" title="Attribuer une médaille de bronze">' +
						'<img class="comDonnerMedaille-option" ng-click="donnerMedaille(message, \'enlever\')" ng-src="{{res_url_private}}img/error.png" title="Supprimer la médaille">'+
					'</div>'
	};
});