pf_app.directive('pfMenuTags', function () {
	return {
		restrict: 'E',
		template: '<div class="ui-tooltip-bottom">' +
						'<input ng-model="new_tags" ng-click="stopBubbling($event)" placeholder="Tags séparés par des virgules">' +
						'<div class="btn-action" ng-click="ajouterTags(message, new_tags); new_tags = \'\'; ">{{"BUTTON_AJOUTER" | translate}}</div>' +
					'</div>',
    link: function (scope, element, attr) {
      scope.stopBubbling = function (event) {
        event.stopPropagation();
      }
    }
	};
});