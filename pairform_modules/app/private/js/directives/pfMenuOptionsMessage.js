pf_app.directive('pfMenuOptionsMessage', function () {
	return {
		restrict: 'E',
		template: 	'<div class="message-options-container ui-tooltip-bottom">'+
						'<div class="message-visibilite-container">'+
							'<span class="message-visibilite-label">Visibilité :</span>'+
							'<ul class="message-visibilite-list">'+
								'<li ng-show="nouveau.liste_reseaux.length == 0" class="message-visibilite-item message-visibilite-item-public" title="Par défaut">{{(nouveau.message_parent_prive && nouveau.message_parent_prive != \'0\') ? "Restreinte" : "Publique"}}</li>'+
								'<li ng-repeat="reseau in nouveau.liste_reseaux track by reseau.id_collection" ng-click="enleverReseauVisibilite(reseau)" class="message-visibilite-item message-visibilite-item-public" title="Par défaut">{{reseau.nom}}</li>'+
								'<li ng-if="!nouveau.message_parent_prive || nouveau.message_parent_prive == \'0\'" class="message-visibilite-item message-visibilite-add-item" ng-click="afficherReseau(\'usage\', null, $event)" title="Changer la visibilité">+</li>'+								
							'</ul>'+
						'</div>'+
						'<div ng-if="((utilisateur_local.rank[capsule.id_capsule].id_categorie >= 4) || (utilisateur_local.est_admin))" class="message-defi-container">'+
							'<span>Défi : </span>'+
							// '<div class="onoffswitch">'+
								'<input type="checkbox" ng-model="nouveau.est_defi">'+
								// '<label class="onoffswitch-label" for="myonoffswitch">'+
									// '<div class="onoffswitch-inner"></div>'+
									// '<div class="onoffswitch-switch"></div>'+
								// '</label>'+
							// '</div>'+
						'</div>'+
					'</div>'
	};
});