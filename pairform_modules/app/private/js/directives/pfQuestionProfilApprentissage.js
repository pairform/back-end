pf_app.directive('pfQuestionProfilApprentissage', function() {
	return {
		restrict: 'A',
		link: function (scope, element) {
			scope.updateModel = function (new_value, old_value) {
				if(new_value != old_value)
					scope.$parent.updateProfilQuestion(scope.question);
			}
			scope.$watch('question.id_reponse_valide', scope.updateModel);
		}
	}
});