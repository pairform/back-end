pf_app.directive('pfMenuChangerRole', function () {
	return {
		restrict: 'E',
		template: 	
			'<ul class="rank-upgrade-list">' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 4)">{{"LABEL_USER_EXPERT" | translate}}' +
				'</li>' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 3)">{{"LABEL_USER_ANIMATEUR" | translate}}' +
				'</li>' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 2)">{{"LABEL_USER_COLLABORATEUR" | translate}}' +
				'</li>' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 1)">{{"LABEL_USER_PARTICIPANT" | translate}}' +
				'</li>' +
				'<li class="rank-upgrade-list-item btn-action" ng-click="changerRoleUtilisateur(ressource, 0)">{{"WEB_LABEL_REVENIR_ROLE_NORMAL" | translate}}' +
				'</li>' +
			'</ul>'
	}
});