pf_app.directive('pfObjectifApprentissage', function() {
	return {
		restrict: 'A',
		link: function (scope, element) {
			scope.updateModel = function (new_value, old_value) {
				if(new_value != old_value)
					scope.$parent.updateObjectif(scope.objectif);
			}
			scope.$watch('objectif.nom', scope.updateModel);
			scope.$watch('objectif.est_valide', scope.updateModel);
		}
	}
});