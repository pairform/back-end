pf_app.directive('pfCommentable', ['$localStorage', function($localStorage) {
	return {
		restrict: 'A',
		scope:{
			"highlighted": "@"
		},
		link: function (scope, element) {
			//Ajout de la classe commentable
			element.addClass('pf-commentable');
			element.attr("onclick", "angular.noop()")
			
			scope.$storage = $localStorage;
			scope.messages_local = [];
			scope.messages_lus = scope.$storage.array_messages_lus;
			
			// scope.$watch($localStorage.array_messages, scope.updateLocalMessages);
			scope.init = function () {
				//Type de l'élément : 
				//Ressource
				if (PF.matches(element, PF.globals.selectorRes)) {
					scope.nom_tag = "";
					scope.num_occurence = "";
					scope.watcher_messages = "$storage.array_messages['" + PF.globals.capsule.id_capsule + "']['_pf_res']";
				}
				//Page
				// else if (PF.matches(element, PF.globals.selectorPage)) {
				// 	scope.nom_tag = "PAGE";
				// 	scope.num_occurence = "";
				// 	scope.watcher_messages = "$storage.array_messages['" + PF.globals.capsule.id_capsule +"']['" + PF.globals.nom_page + "']";
				// }
				//Grain
				else if (PF.matches(element, PF.globals.selector)) {

					// scope.nom_tag = element[0].nodeName;
					scope.nom_tag = PF.getMatchedSelector(element, PF.globals.selector);
					// scope.num_occurence = $_pf(scope.nom_tag, PF.globals.selectorContent).index(element[0]);
					scope.num_occurence = $_pf(scope.nom_tag).index(element[0]);
					scope.watcher_messages = "$storage.array_messages['" + PF.globals.capsule.id_capsule +"']['" + PF.globals.nom_page + "']";
				}
				//Autre page
				else {
					scope.nom_tag = "";
					scope.num_occurence = "";
					var _nom_page = PF.normaliserURL($_pf(element).attr('href'));
					scope.watcher_messages = "$storage.array_messages['" + PF.globals.capsule.id_capsule +"']['" + _nom_page + "']";
				}
				//Initialisation du tableau de messages
				scope.messages_local = [];
			};

			scope.updateLocalMessages = function (array_messages) {
				if (!array_messages) {
					return;
				}
				//Type de l'élément : 
				//Ressource
				if (PF.matches(element, PF.globals.selectorRes)) {
					scope.messages_local = array_messages || [];
				}
				//Page
				// else if (PF.matches(element, PF.globals.selectorPage)) {
				// 	if (typeof(array_messages) != "undefined") {
				// 		scope.messages_local = $_pf.grep(array_messages,function (message) {
				// 			return (message.nom_tag == "PAGE");
				// 		});
				// 	}
				// 	else
				// 		scope.messages_local = [];
				// }
				//Grain
				else if (PF.matches(element, PF.globals.selector)) {
					if (typeof(array_messages) != "undefined") {

						scope.messages_local = $_pf.grep(array_messages,function (message) {
							return ((message.nom_tag == scope.nom_tag) && (message.num_occurence == scope.num_occurence));
						});
					}
					else
						scope.messages_local = [];
				}
				//Autre page
				else {
					scope.messages_local = array_messages || [];
				}

				scope.updateCount();
			}
			/* MAJ du décompte & actualisation de la meta est_lu des messages */
			scope.updateCount = function () {
				if (scope.messages_local.length){
					var count;

					if (scope.messages_lus){
						count = scope.messages_local.filter(function(message) {
                            if(scope.messages_lus.indexOf(message.id_message) !== -1){
                            	//On modifie la valeur du message directement, pour que 
                            	//la directive soit au courant de l'état du message
								message.est_lu = 1;
								return false;
							}
							return true;
                        }).length;
					}
					else
						count = scope.messages_local.length;

					element.addClass('hasComment');
					if (count == 0) {
						element.addClass('readed');
						element.attr('data-count', scope.messages_local.length);
					}
					else{
						element.attr('data-count', count);
						element.removeClass('readed');
					}
				}
				else{
					element.removeAttr('data-count');
					element.removeClass('hasComment readed');
				}
			}

			scope.afficherDepuisOA = function (e) {
				//Certains bouts de code appellent cette fonction directement, et parfois en passant
				//un id de message à focus en paramètre. Test du paramètre
				typeof e == "object" && typeof e.stopPropagation == "function" && e.stopPropagation();
				$_pf('.focusedItem').removeClass('focusedItem');
				element.addClass('focusedItem');
				PF.getScopeOfController('controller_messages').afficherMessagesDepuisOA(scope.messages_local, scope.nom_tag, scope.num_occurence, scope.highlighted, false, element[0].getClientRects()[0]);
			}

			function handleDragOver(e) {
				if (e.preventDefault) {
				  e.preventDefault(); // Allows us to drop.
				}

				element.addClass('pf-droppable-hover');

				return false;
			};

			function handleDragEnter(e) {
				element.addClass('pf-droppable-hover');
			};

			function handleDragLeave(e) {
				// this/e.target is previous target element.
				element.removeClass('pf-droppable-hover');
			};

			function handleDrop(e) {
				// this/e.target is current target element.

				if (e.stopPropagation) {
				  e.stopPropagation(); // stops the browser from redirecting.
				}

				if (e.target == element[0] || e.target.parentElement == element[0]) {

					var dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer,
						 data_string = dataTransfer.getData("text/javascript");
					if (data_string && data_string != "undefined") {
						var data = JSON.parse(data_string);

						//Tableau de messages
						if (data instanceof Array) {
							var array_messages = data;
							for (var i = 0; i < array_messages.length; i++) {
								var message = array_messages[i],
									post_data = {
										"id_message" : message.id_message,
										"nom_tag" : scope.nom_tag,
										"num_occurence" : scope.num_occurence,
										"id_capsule" : PF.globals.capsule.id_capsule,
										"nom_page" : (scope.nom_tag == "" && scope.num_occurence == "") ? "" : PF.globals.nom_page
									}

								element.removeClass("pf-droppable pf-droppable-hover");
								PF.post("message/deplacer", post_data, function (data) {
									//Si on est sur le dernier des messages à bouger
									if (i == array_messages.length ) {
										var retour = data;
										if (retour['status'] == 'ok')
										{
											scope.$root.$broadcast("pf:dropped", message);
											scope.highlighted = message.id_message;
											scope.afficherDepuisOA(scope.highlighted);
										}
										else{
											scope.$root.$broadcast("pf:dragend");
										}
									}
								});
							};
						}
						//Message unique
						else if(data instanceof Object){
							var message = data,
								post_data = {
									"id_message" : message.id_message,
									"nom_tag" : scope.nom_tag,
									"num_occurence" : scope.num_occurence,
									"id_capsule" : PF.globals.capsule.id_capsule,
									"nom_page" : (scope.nom_tag == "" && scope.num_occurence == "") ? "" : PF.globals.nom_page
								}

							element.removeClass("pf-droppable pf-droppable-hover");
							PF.post("message/deplacer", post_data, function (data) {
								var retour = data;
								if (retour['status'] == 'ok')
								{
									scope.$root.$broadcast("pf:dropped", message);
									scope.highlighted = message.id_message;
									scope.afficherDepuisOA(scope.highlighted);
								}
								else{
									scope.$root.$broadcast("pf:dragend");
								}
							});
						}

					}
				}
			};

			scope.$on("pf:dragstart", function () {
				element.on('dragenter', handleDragEnter);
				element.on('dragover', handleDragOver);
				element.on('dragleave', handleDragLeave);
				element.on('drop', handleDrop);
				
				element.addClass("pf-droppable");


			});

			scope.$on("pf:dragend", function () {
				// body...
				element.removeClass("pf-droppable");

				element.off('dragenter');
				element.off('dragover');
				element.off('dragleave');
				element.off('drop');
			});
			// scope.updateLocalMessages();
			scope.init();

			if (scope.highlighted)
				scope.afficherDepuisOA(scope.highlighted);

			scope.$watch('messages_lus', function (new_value, old_value) {
				// console.log(element);
				if (new_value != scope.messages_lus)
					scope.updateCount()
			}, true);
			scope.$watch(scope.watcher_messages, function (new_value, old_value) {
				//Update uniquement si les valeurs ont changé, et qu'il n'y a pas de message local encore enregistré
				if (!angular.equals(new_value, old_value) || scope.messages_local.length === 0) {
				// if (!angular.equals(new_value[PF.globals.capsule.id_capsule], old_value[PF.globals.capsule.id_capsule])) {
					var nombre_messages_grain = scope.messages_local.length;
					//Maj des messages concernant ce grain
					scope.updateLocalMessages(new_value);
					//Si l'élément à le focus ET que le nombre de message local a changé (ajout ou suppression)
					if (element.hasClass('focusedItem') && nombre_messages_grain != scope.messages_local.length){
						PF.getScopeOfController('controller_messages').afficherMessagesDepuisOA(scope.messages_local, scope.nom_tag, scope.num_occurence, null, true);	
					}
				}
			}, true);

			//Affichage du message, uniquement si l'élément n'est pas un lien vers une autre page
			if(PF.matches(element, PF.globals.selectorRes) || PF.matches(element, PF.globals.selectorPage) || PF.matches(element, PF.globals.selector))
				element.bind('click', scope.afficherDepuisOA);
			// element.attr('style',"background-color: red;");
		}

	}
}]);