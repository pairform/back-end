/**
* Directive permettant d'afficher une carte. Elle fonctionne à partir d'évenement car les communicvation vont dans les deux sens : ctrl -> directive, directive -> ctrl
* Elle prend un paramètre : data. Celui-ci doit être de la forme :
* [{lat: 47.218371, lng: -1.553621, id: 11230} , ...}]
* Cette directive écoute deux événements : 
	- afficherPanneau qui permet de redessiner la carte après affichage du panneau la contenant
	- surClicDataPourCarte:data qui permet de recentrer la carte en fonction de la data passé en paramètre. Celle-ci doit être passé dans le tableau initiale
*/
pf_app.directive('pfCarteMessage', ['OpenLayerLazyLoad', 'toaster', function(OpenLayerLazyLoad, toaster){
	return {
		restrict: 'E',
		transclude: false,
		// scope: {
		// 	'data': '=' //Data correspond à un tableau d'objet ; les objects contiennent un id les identifiants ainsi que leurs positions
		// },
		link: function($scope, elem, attrs){
				//Données des marqueurs
			var data,
				size,
				offset,
				//Marqueur de la position du message de l'utilisateur
				marqueur_message,
				//Calque qui va contenir tous les marqueurs
				coucheMarqueurs,
				//Calque qui va contenir le marqueur de message
				coucheMessage,
				//Carte
				map = false,
				//Référence vers l'image représentant un marqueur
				marqueurIcon = PF.globals.url.root + "private/img/marqueur_carte_actif.png",
				//CHoix de la projection utilisie pour encoder la position gps utilisé pour les messages
				//EPSG:4326 = Google Mercator
				mapProjection = "EPSG:4326",
				//Zoom qui sera utilisé pour toutes les modifications de vue de la carte
				zoomGeneral = 5,
				//Zoom qui sera utilisé pour afficher un marqueur scpécifique
				zoomExtend = 10,
				mapID = attrs.id,
				//Icone utilisé pour représenter un marqueur
				icon;

			//Si le controleur parent implémente la fonction pour intéragir sur le clique d'un marquer alors on récupère cette fonction
			if($scope.$parent.surClicMarqueur){
				$scope.surClicMarqueur = $scope.$parent.surClicMarqueur;
			}
			
			/**
			* Fonction renvoyant un marqueur
			* data : object contenant deux variables : lng et lat
			*/
			function afficherMarqueurPJ(data, index, icon){
				var marqueur = new OpenLayers.Marker(
					latLng(data.lat, data.lng), 
					data.icon);
				
				marqueur.data = data.id;

				marqueur.events.register( 'click', marqueur, function( evt ) {
					deplacerMarqueurMessage(this.lonlat);
				});

				coucheMarqueurs.addMarker(marqueur);
			}

			/**
			* Fonction retournant une position dans la projection de la carte
			* lat et lng sont la latitude et la longitude à transformer
			*/
			function latLng(lat, lng){
				return new OpenLayers.LonLat(lng, lat).transform(new OpenLayers.Projection(mapProjection), map.getProjectionObject());
			}

			/**
			* Fonction qui va initialiser la carte et afficher un marqueur pour chaque 
			* data présente dans le scope
			*/
			function initialise() {
				coucheMarqueurs = new OpenLayers.Layer.Markers("Geolocalisation_pieces_jointes");
				coucheMessage = new OpenLayers.Layer.Markers("Geolocalisation_message");

				//Récupération des cartes type aquaralle : http://maps.stamen.com/#watercolor
				map = new OpenLayers.Map({
					div: mapID,
					layers: [new OpenLayers.Layer.OSM("PairForm", [
						"https://a.tile.openstreetmap.fr/hot/${z}/${x}/${y}.png",
						"https://b.tile.openstreetmap.fr/hot/${z}/${x}/${y}.png"
						// "http://a.tile.stamen.com/watercolor/${z}/${x}/${y}.png",
						// "http://b.tile.stamen.com/watercolor/${z}/${x}/${y}.png",
						// "http://c.tile.stamen.com/watercolor/${z}/${x}/${y}.png",
						// "http://d.tile.stamen.com/watercolor/${z}/${x}/${y}.png"
						], {crossOrigin: null}), coucheMarqueurs, coucheMessage],
					//Cette ligne est commenté car elle n'a pas permis de faire les transformations des positions des marqueurs automatiquement
					//D'ou la méthode : latLng
					//projection: new OpenLayers.Projection("EPSG:4326"),
					controls: [
						new OpenLayers.Control.Navigation({
							dragPanOptions: {
								enableKinetic: true
							}
						}),
						new OpenLayers.Control.Zoom()
					],
					zoom: 1,
					theme: false,
					center : [0, 0]
				});
				
				size = new OpenLayers.Size(21, 25),
				offset = new OpenLayers.Pixel(-(size.w/2), -size.h),
				icon = new OpenLayers.Icon(marqueurIcon,size,offset);        
				
				//Abonnement au notification d'envoi pour destruction du marqueur de message
				$scope.$on("pf:message:envoi", function (argument) {
					detruireMarqueurMessage();
					coucheMarqueurs.clearMarkers();
				});

				afficheData($scope.pieces_jointes);
			}
			/**
			* Création du marqueur uniquement au premier affichage
			*/			
		    $scope.$watch('visible_menu_options', function(newValue, oldValue) {
		    	//Check si marqueur_message est déjà défini, pas d'ancienne valeur non plus
		      if (newValue !== oldValue && newValue == 'geolocalisation' && !marqueur_message) {    

						// Prompt HTML5 geolocalisation 
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function(position) {
								var pos = {
									lat: position.coords.latitude,
									lng: position.coords.longitude
								};
								var newLonLat = latLng(position.coords.latitude, position.coords.longitude);
								//Initialisation du marqueur message
								creerMarqueurMessage(newLonLat);
								map.setCenter(newLonLat, zoomExtend);
								toaster.pop("success", "Votre message est désormais géolocalisé ; cliquez sur le marqueur pour annuler.");
							}, function (error) {
								console.error(error);
							});
						}
						
						toaster.pop("info", "Cliquez sur la carte pour géolocaliser votre message.");
						//Deplacement du marqueur de message après un clic sur la carte
						map.events.register('click', map, function (event) {
							var newLonLat = map.getLonLatFromViewPortPx(event.xy);
							
							if (!marqueur_message) {
								//Initialisation du marqueur message
								creerMarqueurMessage(newLonLat);
								$scope.$apply(function () {
									toaster.pop("success", "Votre message est désormais géolocalisé ; cliquez sur le marqueur pour annuler.");
								})
							}
							else{
								deplacerMarqueurMessage(newLonLat);
							}
						});

						map.events.register('zoomend', map, function(ev){

						});

		        }
		    });

			function marqueurMessageHandler(marqueur){
				detruireMarqueurMessage();
				toaster.pop("warning", "Géolocalisation enlevée.");
			}

			function creerMarqueurMessage(newLonLat) {
				//Initialisation du marqueur message
				marqueur_message = new OpenLayers.Marker(
					newLonLat,
					icon);
				coucheMessage.addMarker(marqueur_message)

				marqueur_message.events.register( 'click', marqueur_message, marqueurMessageHandler);
				var lon_lat = newLonLat.clone();
				lon_lat.transform(map.getProjectionObject(), new OpenLayers.Projection(mapProjection));
				//Emission de la nouvelle position
				$scope.safeApply(function () {
					$scope.$root.$broadcast("pf:message:geolocalisation", lon_lat);
				});
			}
			function deplacerMarqueurMessage(newLonLat) {
				if (!marqueur_message){
					creerMarqueurMessage(newLonLat);
				}
				else {
					var newPx = map.getLayerPxFromLonLat(newLonLat);

					marqueur_message.moveTo(newPx);
					//Emission de la nouvelle position
					var lon_lat = newLonLat.clone();
					lon_lat.transform(map.getProjectionObject(), new OpenLayers.Projection(mapProjection));
					$scope.safeApply(function () {
						$scope.$root.$broadcast("pf:message:geolocalisation", lon_lat)
					});
				}
			}
			function detruireMarqueurMessage() {
				if (marqueur_message) {
					marqueur_message.erase();
					marqueur_message.events.unregister( 'click', marqueur_message, marqueurMessageHandler);
					marqueur_message = null;
					$scope.safeApply(function () {
						//Emission de la nouvelle position
						$scope.$root.$broadcast("pf:message:geolocalisation")
					});
				}
			}

			$scope.safeApply = function(fn) {
				var phase = this.$root.$$phase;
				if(phase == '$apply' || phase == '$digest') {
					if(fn && (typeof(fn) === 'function')) {
						fn();
					}
				} else {
					this.$apply(fn);
				}
			}
			/**
			* Fonction centrant la carte sur la position de la data sélectionné.
			* message : l'objet data correspond
			*/
			function surDataChoisi(data){
				map.setCenter(latLng(data.lat, data.lng), zoomExtend);
			}

			/**
			* Fonction permettant d'ajouter toutes les données en tant que marqueurs sur la carte
			*
			*/
			function afficheData(data){

				//Supprime tous les marqueurs affichés
				coucheMarqueurs.clearMarkers();
				//Si on a des données alors on les affichent en marqueur
				if(data && data.length > 0){
					//On centre la carte sur la première donnée
					// var pj_0 = ParseDMS(EXIF.getTag(data[0], 'GPSPosition'));
					//Donc celui à la position 0
				
					for(var i = 0; i < data.length; i++){
						var pj = data[i],
							pj_geolocation = getPositionFromPJ(pj.file);
						//S'il n'y a pas de geo, pas besoin d'aller plus loin
						if (pj_geolocation) {
							toaster.pop("info", "Cliquez sur une pièce jointe pour géolocaliser votre message à cet endroit.");
							// pj_geolocation.icon = icon;
							pj_geolocation.icon = new OpenLayers.Icon(pj.display_thumbnail_image,size,offset);
							// pj_geolocation.icon = new OpenLayers.Icon('http://localhost:3000/res/avatars/1.png',size,offset);
							pj_geolocation.id = pj.name;

							map.setCenter(latLng(pj_geolocation.lat, pj_geolocation.lng), zoomExtend);
							afficherMarqueurPJ(pj_geolocation, i, pj_geolocation.icon.clone());
						}
					}
				}
			}

			//Fonction de conversion de DMS (Degrees Minutes Seconds) à DD (Decimal Degrees)
			//Les coordonnées GPS dans les données EXIF sont en DMS, et OSM prend du DD
			//Courtoisie de Gavin Miller (http://stackoverflow.com/a/1140335/1437016)
			//47 deg 16' 58.39" N, 1 deg 31' 12.39" W
			function getPositionFromPJ(input) {
			    var exif_lat = EXIF.getTag(input, 'GPSLatitude');
			    var exif_lng = EXIF.getTag(input, 'GPSLongitude');
			    if (!exif_lat || !exif_lng)
			    	return null;
			    var lat = ConvertDMSToDD(exif_lat[0], exif_lat[1], exif_lat[2], EXIF.getTag(input, 'GPSLatitudeRef'));
			    var lng = ConvertDMSToDD(exif_lng[0], exif_lng[1], exif_lng[2], EXIF.getTag(input, 'GPSLongitudeRef'));
			    return {lat:lat, lng:lng};
			}
			function ConvertDMSToDD(degrees, minutes, seconds, direction) {
			    var dd = degrees + minutes/60 + seconds/(60*60);

			    if (direction == "S" || direction == "W") {
			        dd = dd * -1;
			    } // Don't do anything for N or E
			    return dd;
			}

			//Appel au service pour qu'il charge les données d'OpenLayer
			OpenLayerLazyLoad.then(function() {
				initialise();
			}, function(){});

			/**
			* Watch de la variable data pour update des propositions sur la carte
			*/
			$scope.$on("pf:pieces_jointes:ajout", function (event, pieces_jointes) {
				// if(pieces_jointes !== old_value){
					afficheData(pieces_jointes);
				// }
			})
			/**
			* Evenement soulevé afin de réinitialiser la taille de la carte
			* 
			*/
			$scope.$parent.$on('reinitialiserTailleCarte', function (event, data) {
				map.updateSize();
				map.calculateBounds();
			});

		}
	};
}]);