
/*
 *	Main Controller
 */
var pf_app = angular.module('pf_app',[
	"pascalprecht.translate"
	// ,"ngRaven"
	,"ngStorage"
	,"ngSanitize"
	,"ngAnimate"
	,"ngTouch"
	,"angular-carousel"
	,"toaster"
	,"ngCookies"
	,"angular-loading-bar"
	,"ngFileUpload"
	,"ngRoute"
	,"ngImgCrop"
	,"ui.thumbnail"
]);