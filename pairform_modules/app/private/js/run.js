//Obligé d'utiliser un bloc run, au cas ou angular n'a pas fini de bootstrapper
//On passe le PF._localStorage injecté en référence aux deux fonctions ci-dessous
//Parce qu'elles ne seront pas forcément capable de récuperer l'injector() de l'app
pf_app.run(["$localStorage", "$compile", function ($localStorage, $compile) {
	// Obligé de mettre un mini-time out, je sais pas pourquoi.
	setTimeout(function () {
		Message.recupererMessages(false, function callback (array_messages) {
			//Important : ajouter des bulles pour tous les liens
			PF.compileLinks(null, true);
			PF.compileContent();
			Message.contextualiserMessage($compile, array_messages);
			//Si on a une fonction callback a appeler
			if(typeof PF_ready == "function"){
				PF.ready = PF_ready;
				PF_ready(PF);
			}
		}, $localStorage);
	},50);
}]);

angular.element(document).ready(function() {
	PF.init();
	angular.bootstrap(document, ['pf_app']);
	$_pf('div[ng-controller=main_controller]').fadeIn(200);
	

	// Message.recupererMessages();
	// Message.contextualiserMessage();
});