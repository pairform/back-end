pf_app.filter('search', ["$filter", function ($filter) {
	return function(input, term, fields, operator) {
		if (!term) {
			return input;
		}

		fields || (fields = []);

		if (!fields.length) {
			return $filter('filter')(input, term);
		}

		operator || (operator = false); // true=OR, false=AND

		var filtered = [], valid;

		angular.forEach(input, function(value, key) {
			valid = !operator;
			for(var i in fields) {
				var index = value[fields[i]].toLowerCase().indexOf(term.toLowerCase());
				// OR : found any? valid
				if (operator && index >= 0) { 
					valid = true; break;
				} 
				// AND: not found once? invalid 
				else if (!operator && index < 0) { 
					valid = false; break;
				}
			}
			if (valid) {
				this.push(value);
			}
		}, filtered);

		return filtered;
	};
}]);
