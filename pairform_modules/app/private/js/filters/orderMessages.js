pf_app.filter('orderMessages', function() {
		return function(messages_array_before_sort, _scope, reverse) {
			_scope = _scope || $scope;
			messages_array_before_sort = messages_array_before_sort || [];
			var messages_array = [];
			var messages_array_reponse = [];
			//Tableau des posts réarrangés
			var messages_object = {};

			//Stockage des messages lus
			var array_messages_lus = [];
			//On ajoute les messages lus à ceux éventuellements déjà lus
			if (typeof(PF._localStorage.array_messages_lus) != "undefined")
				array_messages_lus = JSON.parse(PF._localStorage.array_messages_lus);

			//S'il n'y a pas de message sur la page, ça sert a rien de faire la suite
			if (messages_array_before_sort.length) {
				//Tableau des messages de l'OA
				$_pf.each(messages_array_before_sort, function (index,message) {
					
					if(PF.globals.display_type == "user"){
						if(message['id_auteur'] == _scope.id_utilisateur){
							messages_array.push(message);
						}	
					}
					else if(PF.globals.display_type == "transversal"){
						messages_array.push(message);
					}
					else{
						if(message['id_message_parent'] == "")	
							messages_array.push(message);
						else
							messages_array_reponse.push(message);
					}
				});

				
				//Si c'est temporel
				// if ((_scope.sort_type == 'date') || (_scope.sort_type == 'default')) {
					//Ordre temporel décroissant
					messages_array.sort(function (a,b) {
						//Si a est créé avant
						if (a.date_creation > b.date_creation)
							return -1;
						if (a.date_creation < b.date_creation)
							return 1;
						return 0;

					});

					//Ordre temporel décroissant
					messages_array_reponse.sort(function (a,b) {
						//Si a est créé avant
						if (a.date_creation > b.date_creation)
							return -1;
						if (a.date_creation < b.date_creation)
							return 1;
						return 0;
					});
				// }
				//Si c'est par votes
				if (_scope.sort_type == 'votes') {
					//Plus voté d'abord
					messages_array.sort(function (a,b) {
						//Si a est plus utile
						if (parseInt(a.somme_votes) > parseInt(b.somme_votes))
							return -1;
						if (parseInt(a.somme_votes) < parseInt(b.somme_votes))
							return 1;
						return 0;

					});

					//Plus voté d'abord
					messages_array_reponse.sort(function (a,b) {
						//Attention, ordre inversé!
						if (parseInt(a.somme_votes) > parseInt(b.somme_votes))
							return 1;
						if (parseInt(a.somme_votes) < parseInt(b.somme_votes))
							return -1;
						return 0;
					});
				}
				// _controller_messages_scope.next_sort_type = Message.getNextSortType(sort_type);

				//Si on est dans le cas de message d'un grain, les réponses sont déjà triés dans un tableau, sinon, non
				if((PF.globals.display_type == "user") || (PF.globals.display_type == "transversal")){

					var messages_object = PF.toObject(messages_array_before_sort);
					//On tri les messages qui sont des réponses de défi
					var temp_messages_array = messages_array.filter(function (message, index) {
						//Si le message est une réponse
						if((message['id_message_parent'] != ""))
						{
							var parent_message = messages_object[message['id_message_parent']];
							//S'il se passe quelque chose de louche
							if(!parent_message)
								//On ne laisse pas passer la réponse vacante
								return false;

							if (parent_message.est_defi == 1 || parent_message.est_defi == "1") {
								//Si c'est le commentaire de l'utilisateur courant
								//Ou que l'utilisateur courant est initiateur du défi
								if((message.id_auteur != _scope.utilisateur_local.id_utilisateur) 
									&& (parent_message.id_auteur != _scope.utilisateur_local.id_utilisateur) 
									&& (_scope.utilisateur_local.rank[_scope.capsule.id_capsule].id_categorie < 4)){
									
									return false;
								}
							}
						}
						return true;
					});

					return temp_messages_array;
				}
				else{
					var messages_object = PF.toObject(messages_array);
					//On tri les messages qui sont des réponses de défi
					$_pf.each(messages_array_reponse, function (index,message) {
						//Si le message est une réponse
						if((message['id_message_parent'] != ""))	
						{
							var parent_message = messages_object[message['id_message_parent']];
							message.message_parent = parent_message;
							var new_index = messages_array.indexOf(parent_message);

							//S'il se passe quelque chose de louche	//On ne laisse pas passer la réponse vacante
							if(parent_message && new_index != -1){
								if (parent_message.est_defi == 1 || parent_message.est_defi == "1") {
									//Si c'est le commentaire de l'utilisateur courant
									//Ou que l'utilisateur courant est initiateur du défi
									if((this.id_auteur == _scope.utilisateur_local.id_utilisateur) 
										|| (parent_message.id_auteur == _scope.utilisateur_local.id_utilisateur) 
										|| (_scope.utilisateur_local.rank[_scope.capsule.id_capsule].id_categorie >= 4)){
										
										messages_array.splice(new_index+1, 0, this);
									}
								}
								else
									messages_array.splice(new_index+1, 0, this);
							}
							else{
								messages_array.splice(0, 0, this);	
								this.id_message_parent = 0;
							}
							
						}
							
					});	

					return messages_array;
				}

			}
			else
				return [];
		};
	});