pf_app.filter('pf_linky', ["$filter", function($filter) {
    return function(text, target, attributes) {
      var content = $filter('linky')(text, target, attributes),
          url_pf = PF.globals.url.root;

      if (content.match(url_pf)) {
        //Traitement des liens de partage d'écriture collaborative
        // Exemple : <a ...>http://127.0.0.1:3000/home/backoffice/capsules/gestionCapsule/20?id_espace=3&amp;nom_espace=IMT%20Atlantique&amp;id_ressource=3&amp;nom_ressource=Transition%20%C3%A9nerg%C3%A9tique&amp;id_capsule=20&amp;nom_capsule=Document%20collaboratif&amp;cree_par=1&amp;phase_creation</a>"
        var regex_doc_backoffice = /">(?:.*?nom_capsule\=)(.*?)(?:\&.*?)<\/a>/,
            match = content.match(regex_doc_backoffice);
        if (match) {
          //Extraction du nom de document
          nom_document = match[1].replace(/%20/i, ' '); // "Document collaboratif" par exemple
          content = content.replace(regex_doc_backoffice, '">'+ nom_document + '</a>');
        }
      }

      return content;
    }
}]);
