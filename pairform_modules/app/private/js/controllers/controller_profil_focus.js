pf_app.controller('controller_profil_focus', ["$scope", "$translate", "toaster", "$timeout", function ($scope, $translate, toaster, $timeout) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"upgrade_ressource" : false,
		"is_connected_user" : false,
		"utilisateur" : {}
	};

	angular.extend($scope, defaults);

	$scope.chargerProfil = function (id_utilisateur, callback) {
		
		//Chargement du profil
		if (!$scope.utilisateur_focus || $scope.utilisateur_focus.id_utilisateur != id_utilisateur) {
			PF.get('utilisateur/profil', {"id_utilisateur": id_utilisateur} , function(retour, status, headers, config) {
				var datas = retour['datas'];
				datas.id_utilisateur = id_utilisateur;
				$scope.utilisateur_focus = new Utilisateur(datas);
				$scope.utilisateur_focus.rank = $scope.utilisateur_focus.ressources;
				$scope.determinerDerniereConnexion();
				callback();
			});
		}
		else
			callback();
	}

	$scope.determinerDerniereConnexion = function  () {
		var lb_st = $scope.labelStatus;
		var t_off = Math.floor((Math.floor(new Date().getTime()/1000) - $scope.utilisateur_focus.offline) / 60); // en minute, le temps d'inactivité
		$translate(['WEB_LABEL_EN_LIGNE', 'WEB_LABEL_INACTIF_DEPUIS', 'WEB_LABEL_MINUTES', 'WEB_LABEL_HEURES', 'WEB_LABEL_JOURS', 'WEB_LABEL_MOIS']).then(function (translation_object) {
			lb_st = translation_object['WEB_LABEL_EN_LIGNE'];
			if (t_off != false && t_off > 5) {
				$scope.online = false;
				if (t_off < 60) {
					t_off = Math.round(t_off);
					lb_st = translation_object['WEB_LABEL_INACTIF_DEPUIS'] + ' ' + t_off + ' ' +translation_object['WEB_LABEL_MINUTES'];
				}
				else if((t_off > 60) && (t_off < 1440)){
					t_off = Math.round(t_off / 60);
					lb_st = translation_object['WEB_LABEL_INACTIF_DEPUIS'] + ' ' + t_off + ' ' +translation_object['WEB_LABEL_HEURES'];
				}
				else if((t_off > 1440) && (t_off < 43200)){
					t_off = Math.round(t_off / 1440);
					lb_st = translation_object['WEB_LABEL_INACTIF_DEPUIS'] + ' ' + t_off + ' ' +translation_object['WEB_LABEL_JOURS'];
				}
				else if((t_off > 43200) && (t_off < 20000000)){
					t_off = Math.round(t_off / 43200);
					lb_st = translation_object['WEB_LABEL_INACTIF_DEPUIS'] + ' ' + t_off + ' ' +translation_object['WEB_LABEL_MOIS'];
				}
				else if (t_off > 20000000) {
					lb_st = translation_object['WEB_LABEL_INACTIF'];
				}
			}
			else 
				$scope.online = true;
			
			$scope.offline = t_off;
			$scope.labelStatus = lb_st;
		});
	}

	$scope.cancelTimeout = function () {
		$timeout.cancel($scope.timeout_display);
	}
	$scope.cacherProfil = function (id_utilisateur) {
		$scope.cancelTimeout();
		//Hide du flair
		$_pf(".user-short-profile").removeClass("displayed");
	}

	$scope.afficherProfilDelay = function (client_rect) {
		$scope.cancelTimeout();
		$scope.timeout_display = $timeout(function  () {
			$scope.afficherProfil(client_rect);
		}, 1000);
	}

	$scope.afficherProfil = function (client_rect) {
		//Affichage du flair avec la bonne position
		var short_profile = $_pf(".user-short-profile");
					
		short_profile.css("top", client_rect.top + "px");
		short_profile.css("left", client_rect.left + "px");
		short_profile.addClass("displayed");
	}


	$scope.changerRole = function (id_ressource, id_categorie_temp, nom_categorie_temp) {
		debugger
		PF.post('utilisateur/role', {"id_utilisateur_cible": $scope.utilisateur_focus.id_utilisateur, "id_ressource": id_ressource, "id_categorie_temp": id_categorie_temp} , function(retour, status, headers, config) {
			if (retour["status"] == "ok"){
				var categorie_ressource = $scope.utilisateur_focus.getCategorieForRessource(($scope.capsule || $scope.ressource).id_ressource);
				categorie_ressource.id_categorie = retour["data"]["id_categorie"];
				categorie_ressource.nom_categorie = retour["data"]["nom_categorie"];
				$scope.upgrade_ressource = false;
			}
			else
				toaster.pop("error", "WS_ERREUR_CONTACTER_PAIRFORM");
		});
	}

}]);