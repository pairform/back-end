pf_app.controller('controller_nav_bar', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
	var defaults = {
		"visible" : true,
		"sidebar_visible" : false,
		"messages_deplacement_visible" : false,
		"notifications" : {'us' : [], 'un' : [], 'count_us' : 0, 'count_un' : 0, 'visible' : ''}
	};
	angular.extend($scope, defaults);

	$scope.actualiser = function () {
		Message.recupererMessages();
		toaster.pop("success", "WEB_LABEL_MESSAGES_ACTUALISES");
		PF.Notification.updateAll();
	}

	$scope.afficherNotification = function (type) {
		if ($scope.notifications.visible != type){
			$scope.notifications.visible = type;

			//Mise à jour du statut lu de la notification
			$scope.notifications[type].forEach(function (notification, index) {
				if(notification.date_vue == null){
					notification.date_vue = Math.round(Date.now() / 1000);
				}
			});

			//Gestion de la synchro des vues des notifications
			if ($scope.notifications['count_'+type])
			{
				var post = {"type" : type};

				PF.post("notification/vue/type", post , function (retour){
					
					if (retour['status'] == 'ko')
					{
						if (retour['message'] == "ws_utilisateur_invalide") {
							$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
						}
						else
							toaster.pop('error', retour['message']);
					}
					else {
						$scope.notifications['count_'+type] = 0;
					}
				});
				
			}
		}
		else
			$scope.notifications.visible = '';
	}

	$scope.actionNotification = function (notification, notification_type) {
		//Score utilisateur
		if (notification_type == "us"){
			//Si on est dans le cas d'un succes, il n'y a pas de message attaché
			if (!notification.id_message){
				$scope.afficherProfil();
			}
			else{
				PF.redirigerVersMessage(notification.id_message);
			}
		}
		//Notification utilisateur
		else{
			switch(notification.sous_type){
				// dc : défi créé 
				case "dc" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// dt : défi terminé 
				case "dt" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// dv : défi validé 
				case "dv" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// ar : ajout réseau
				case "ar" : {
					$scope.afficherProfil(notification.contenu);
					break;
				}
				// cr : classe rejointe
				case "cr" : {
					$scope.afficherProfil(notification.contenu);
					break;
				}
				// ru : réponse d'utilisateur
				case "ru" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// gm : gagné médaille
				case "gm" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// gs : gagné succès
				case "gs" : {
					$scope.afficherProfil();
					break;
				}
				// cc : changé catégorie
				case "cc" : {
					$scope.afficherProfil();
					break;
				}
			}
		}
	}

	$scope.afficherMessagesDeplacement = function () {
		$scope.messages_deplacement_visible = !$scope.messages_deplacement_visible;

		if ($scope.messages_deplacement_visible)
			toaster.pop("info", "WEB_LABEL_DEPLACEZ_COM_SUR_GRAIN");
	}
	$scope.supprimerDeplacementMessage = function (message) {

		var index = $scope.$root.array_messages_deplacement.indexOf(message);

		if (index != -1){
			$scope.$root.array_messages_deplacement.splice(index, 1);
			//On fait pointer une variable globale a tous les controllers sur le local storage,
			//Pour pouvoir watcher ca dans le controller nav_bar
			$localStorage.array_messages_deplacement = $scope.$root.array_messages_deplacement;
		}
	}

	$scope.switchWatchCapsule = function () {

		var post = {'id_utilisateur' : $scope.utilisateur_local.id_utilisateur, 'id_capsule' : $scope.capsule.id_capsule};

		//On inverse la valeur (cast booléen puis string)
		var notif_mail = $scope.utilisateur_local.rank[$scope.capsule.id_capsule].watch_mail = +!$scope.utilisateur_local.rank[$scope.capsule.id_capsule].watch_mail;

		PF.post('utilisateur/notification/watch',post , function (retour){
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				var alert = notif_mail ? 'activées' : 'désactivées';
				toaster.pop('success', "Notifications de tous les messages postés sur ce document "+alert);
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
				}
			}
		});
	}

	$scope.$on("pf:dropped", function (event, message) {
		var index = -1;

		$localStorage.array_messages_deplacement.some(function (msg, idx, arr) {
			if (msg.id_message == message.id_message)
				return index = idx;
		});
		if (index !== -1)
			$localStorage.array_messages_deplacement.splice(index, 1);

		$scope.$root.array_messages_deplacement = $localStorage.array_messages_deplacement;

		toaster.pop("success", "WEB_LABEL_MSG_DEPLACE_AVEC_SUCCES");
		Message.recupererMessages(true);
		
	});
}]);