pf_app.controller('controller_cookie_consent', ["$scope", "$localStorage", function ($scope, $localStorage) {
  var defaults = {
    "visible" : false
  };

  angular.extend($scope, defaults);
  
   // && !PF.globals.is_vitrine && !/(android|ipad|iphone|ipod|mobile)/i.test(navigator.userAgent||navigator.vendor||window.opera)){
  $scope.display = typeof $localStorage.pf_autorise_cookies == "undefined" ? true : false;
  
  //Reset de tout ce qui a pu être mis en place
  window.ga = function () {};
  window._paq = window._paq || [];

  const _site_id = !(PF && PF.globals && PF.globals.is_vitrine) ? "1" : "2";
  
  //Activation des cookies même 
  if (!$scope.display){
    if ($localStorage.pf_autorise_cookies == false)
      _paq.push(['disableCookies']);
    
    activateCookieScripts();
  }

  var _popup_display_time = Date.now();

  $scope.autoriserCookies = function () {
    $localStorage.pf_autorise_cookies = true;
    $scope.display = false;
    _paq.push(['trackEvent',  'Cookies', 'Accepté', getSecondsBetween(_popup_display_time, Date.now())]);
    activateCookieScripts();
  }
  $scope.declinerCookies = function () {
    $localStorage.pf_autorise_cookies = false;
    $scope.display = false;
    _paq.push(['disableCookies']);
    _paq.push(['trackEvent',  'Cookies', 'Decliné', getSecondsBetween(_popup_display_time, Date.now())]);
    activateCookieScripts();
  }
  
  function activateCookieScripts() {
    activatePiwik();
  }
  function getSecondsBetween(date_start, date_end) {
    return Math.round(Math.abs(date_end - date_start) / 1000) + " secondes";
  }

  function activatePiwik() {
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="//piwik.pairform.fr/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', _site_id]);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  }
}]);
