pf_app.controller('main_controller', ["$scope", "$localStorage", "toaster", "$translate", "cfpLoadingBar", function ($scope, $localStorage, toaster, $translate, cfpLoadingBar) {
	$scope.CFP = cfpLoadingBar.start;
	$scope.capsule = PF.globals.capsule;

	//S'il n'y a pas d'utilisateur du tout, ou que l'utilisateur non connecté n'est pas encore venu sur cette capsule
	if (!$localStorage.utilisateur_local 
	|| (!$localStorage.utilisateur_local.est_connecte
	&& !$localStorage.utilisateur_local.rank[$scope.capsule.id_capsule]))
		//On regénère un utilisateur, pour qu'il ait le rang pour cette capsule
		$localStorage.utilisateur_local = new Utilisateur();
	else
		//Sinon, on le récupère
		$localStorage.utilisateur_local = new Utilisateur($localStorage.utilisateur_local);

	$scope.utilisateur_local = $localStorage.utilisateur_local;
	PF.globals.utilisateur_local = $localStorage.utilisateur_local;
	$scope.codeFromId = PF.langue.codeFromId;
	$scope.idFromCode = PF.langue.idFromCode;
	$scope.res_url_private = PF.globals.url.private;
	$scope.res_url_public = PF.globals.url.public;
	$scope.res_url_root = PF.globals.url.root;

	//On récupère les messages en déplacement depuis le PF._localStorage dès qu'on peut
	$scope.$root.array_messages_deplacement = $localStorage.array_messages_deplacement;
	
	$scope.$storage = $localStorage;

	$scope.$storage.flag_special_event_pip = $scope.$storage.flag_special_event_pip || false;

	//Vérification que l'utilisateur a un rôle pour cette ressource
	$scope.verifierRole = function () {
		PF.post("utilisateur/categorie/verifier", {
				"id_ressource" : $scope.capsule.id_ressource,
				"id_capsule" : $scope.capsule.id_capsule
		}, function  (retour) {
			var status = retour["status"];
			//Conflit d'identité (un utilisateur est connecté sur le client et sur le serveur, mais pas les même)
			//Besoin de checker est_connecte, parce que id_utilisateur peut-être égal à 0 s'il n'y avait pas d'utilisateur connecté (la condition est vraie qd même) 
			if (status == "ko" && retour['message'] == "ws_utilisateur_invalide"
				|| (status == "ok" && $scope.utilisateur_local.est_connecte && (retour["id_utilisateur"] != $scope.utilisateur_local.id_utilisateur))) {
				//On déconnecte
				$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
			}
			//S'il y a mise à jour
			else if(status == "up"){
				//S'il faut mettre à jour la categorie
				if (typeof(retour["categorie_ressource"]) != "undefined") {
					//S'il n'y avait rien, ni rôle ni parametres pour cette capsule
					if (retour["categorie_ressource"] == false) {
						//Mise à jour de la catégorie avec les parametres par défaut
						$scope.utilisateur_local.rank[$scope.capsule.id_capsule] = {
							"id_ressource" : $scope.capsule.id_ressource,
							"score" : "0",
							"notification_mail" : "1",
							"watch_mail" : "0",
							"afficher_noms_reels" : "0",
							"nom_categorie" : "Lecteur",
							"id_categorie" : "0"
						}
					}
					//Sinon, l'utilisateur n'avait pas visité cette capsule, mais avait déjà un rôle dans cette ressource
					else{
						//Mise à jour de la catégorie avec les parametres revenant du serveur
						$scope.utilisateur_local.rank[$scope.capsule.id_capsule] = {
							"id_ressource" : $scope.capsule.id_ressource,
							"score" : retour["categorie_ressource"]["score"],
							"notification_mail" : "1",
							"watch_mail" : "0",
							"afficher_noms_reels" : "0",
							"nom_categorie" : retour["categorie_ressource"]["nom_categorie"],
							"id_categorie" : retour["categorie_ressource"]["id_categorie"]
						}
					}
				}
				//Sinon, il y avait déjà les rôles .
				else if (typeof(retour["utilisateur_parametres"]) != "undefined") {
					//Mise à jour des paramètres utilisateurs
					$scope.utilisateur_local.rank[$scope.capsule.id_capsule].notification_mail = "1";
					$scope.utilisateur_local.rank[$scope.capsule.id_capsule].watch_mail = "1";
					$scope.utilisateur_local.rank[$scope.capsule.id_capsule].afficher_noms_reels = "0";
				}
			}
		});
	}


	$scope.afficherConnexion = function (mode){
		var scope_connexion = PF.getScopeOfController('controller_connexion');
		if (scope_connexion){
			// scope_connexion.$parent.setVisible();
			// scope_connexion.$parent.popin_visible = true;
			scope_connexion.visible = true;
			// scope_connexion.popin.visible = true;
			scope_connexion.mode = mode || 'connexion';
		}	
	}

	$scope.setSession = function (){

		// var date = new Date();
		// date.setTime(date.getTime() + (24 * 60 * 1000));

		$scope.utilisateur_local = $localStorage.utilisateur_local;
		PF.globals.utilisateur_local = $scope.utilisateur_local;
		$scope.utilisateur_local.est_connecte = true;
		// $localStorage.session = {"elggperm" : $scope.utilisateur_local.elggperm, "expiration" : date};
	}

	$scope.removeSession = function (){
		$scope.utilisateur_local = $localStorage.utilisateur_local;
		PF.globals.utilisateur_local = $scope.utilisateur_local;
		$scope.utilisateur_local.est_connecte = false;
		$localStorage.session = {};
	}

	$scope.afficherMessagesUtilisateur = function (id_utilisateur) {
		Message.afficher(undefined, id_utilisateur);
	}
	$scope.afficherMessagesTransversal = function () {
		Message.afficher(undefined, undefined);
	}

	$scope.afficherCarrousel = function (array_pj, index_pj) {

		var carrousel_scope = PF.getScopeOfController('controller_carrousel');
		carrousel_scope.array_pj = array_pj;
		carrousel_scope.index_pj = index_pj;
		//Et on affiche le panneau après, qui appelle init()
		var _carrousel = new Panneau({'nom_controller' : 'controller_carrousel', 'position' : 'left'});
		_carrousel.togglePanneau();
	}
	// $scope.afficherProfil = function (id_utilisateur) {
	// 	if (typeof(id_utilisateur) == "undefined")
	// 		var id_utilisateur = $localStorage.utilisateur_local.id_utilisateur;
	// 	//On passe la valeur
	// 	PF.getScopeOfController('controller_profil').utilisateur.id_utilisateur = id_utilisateur;
	// 	//Et on affiche le panneau après, qui appelle init()
	// 	var _profil = new Panneau({'nom_controller' : 'controller_profil', 'position' : 'right', 'should_toggle' : false});
	// 	_profil.togglePanneau();
	// }
	$scope.afficherProfilConfig = function () {
		// if (typeof(id_utilisateur) == "undefined")
		var utilisateur_local = $localStorage.utilisateur_local;
		//On passe la valeur
		PF.getScopeOfController('controller_profil_config').utilisateur = utilisateur_local;
		//Et on affiche le panneau après, qui appelle init()
		var _profil_config = new Panneau({'nom_controller' : 'controller_profil_config', 'position' : 'right', 'superpose' : true});
		_profil_config.togglePanneau();
	}
	$scope.afficherChangerAvatar = function () {
		// if (typeof(id_utilisateur) == "undefined")
		var utilisateur_local = $localStorage.utilisateur_local;
		//On passe la valeur
		PF.getScopeOfController('controller_avatar_upload').utilisateur = utilisateur_local;
		//Et on affiche le panneau après, qui appelle init()
		var _changer_avatar = new Panneau({'nom_controller' : 'controller_avatar_upload', 'position' : 'right', 'superpose' : true});
		_changer_avatar.togglePanneau();
	}
	$scope.afficherSucces = function (id_utilisateur, succes) {
		if (typeof(id_utilisateur) == "undefined")
			var id_utilisateur = undefined;
		//On passe la valeur
		PF.getScopeOfController('controller_succes').id_utilisateur = id_utilisateur;
		PF.getScopeOfController('controller_succes').succes = succes || undefined;
		//Et on affiche le panneau après, qui appelle init()
		var _succes = new Panneau({'nom_controller' : 'controller_succes', 'position' : 'left'});
		_succes.togglePanneau();
	}
	$scope.afficherReseau = function (mode, utilisateur_ajout, $event) {
		var controller_reseau = PF.getScopeOfController('controller_reseau')
		controller_reseau.mode = mode || "gestion";
		controller_reseau.utilisateur_ajout = utilisateur_ajout || undefined;

		var _reseau = new Panneau({'nom_controller' : 'controller_reseau', 'position' : 'right', 'superpose' : true});
		_reseau.togglePanneau();
		// if($event){
		// 	var client_rect = $event.currentTarget.getClientRects()[0];
		// 	controller_reseau.positionnerReseau(client_rect);
		// }
		
	}
	$scope.afficherDashboard = function () {
		// var _dashboard = new Panneau({'nom_controller' : 'controller_dashboard', 'position' : 'left'});
		// _dashboard.togglePanneau();
		$scope.fonctionPasDisponible();
	}
	$scope.afficherRessources = function () {
		var _ressources = new Panneau({'nom_controller' : 'controller_ressources', 'position' : 'left'});
		_ressources.togglePanneau();
	}

	$scope.afficherRecherche = function (mode) {
		PF.getScopeOfController('controller_recherche_profil').mode = mode || "recherche_profil";
		var _recherche_profil = new Panneau({'nom_controller' : 'controller_recherche_profil', 'position' : 'left'});
		_recherche_profil.togglePanneau();
	}
	$scope.afficherPreferences = function () {
		var _preferences = new Panneau({'nom_controller' : 'controller_preferences', 'position' : 'left'});
		_preferences.togglePanneau();
	}
	$scope.afficherPresentation = function () {
		var _presentation = new Panneau({'nom_controller' : 'controller_presentation', 'position' : 'left'});
		_presentation.togglePanneau();
	}

	$scope.$root.afficherFicheRessource = function (id_ressource) {
		
		// var _fiche_ressource = new Panneau({'nom_controller' : 'controller_fiche_ressource', 'position' : 'left', "should_toggle" : false, "superpose" : true});
		
		// _fiche_ressource.togglePanneau();

		var scope_ressource = PF.getScopeOfController('controller_fiche_ressource');
		if (scope_ressource){
			scope_ressource.id_ressource = id_ressource || PF.globals.capsule.id_ressource;
			scope_ressource.visible = true;
			scope_ressource.init();
		}	
	}
	$scope.chargerProfilFocus = function (id_utilisateur) {
		var scope_profil = PF.getScopeOfController('controller_profil_focus');
		scope_profil.chargerProfil(id_utilisateur);
	}

	$scope.chargerEtAfficherProfilFocus = function ($event, id_utilisateur) {
		var scope_profil = PF.getScopeOfController('controller_profil_focus'),
				 client_rect = $event.currentTarget.getClientRects()[0];
		scope_profil.chargerProfil(id_utilisateur, function () {
			scope_profil.afficherProfil(client_rect);
		});
	}
	$scope.cacherProfilFocus = function (id_utilisateur) {

		var scope_profil = PF.getScopeOfController('controller_profil_focus');
		scope_profil.cacherProfil();
	}
	$scope.cancelTimeoutFocus = function (id_utilisateur) {

		var scope_profil = PF.getScopeOfController('controller_profil_focus');
		scope_profil.cancelTimeout();
	}
	$scope.afficherProfilFocus = function (event) {
		var scope_profil = PF.getScopeOfController('controller_profil_focus'),
				 client_rect = event.currentTarget.getClientRects()[0];
		scope_profil.afficherProfil(client_rect);
	}
	$scope.deconnecter = function (toast, callback){

		var _scope = $scope;
		PF.post('utilisateur/logout', {}, function callback_success (data, status, headers, config) {
			var retour = data;
			if (retour['status'] == 'ok')
			{
				//Reset de l'utilisateur
				$localStorage.utilisateur_local = new Utilisateur();
				//On enlève la session stockée
				_scope.removeSession();
				//On arrête les notifications
				PF.Notification.removeUpdateTimer();
				Message.removeUpdateTimer();

				//On rechope tous les messages
				Message.recupererMessages(true);
				//On toast grave
				if (typeof(toast) != "undefined"){
					if(toast != ""){
						toaster.clear();
						toaster.pop("warning",toast);
					}
					if(typeof(callback) == "function")
						callback();
				}
				else{
					toaster.pop("success", "LABEL_DECONNEXION_REUSSI");
					if(typeof(callback) == "function")
						callback();
				}
			}
			else
			{
				toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");
			}

		});
	}
	$scope.safeApply = function(fn) {
		var phase = this.$root.$$phase;
		if(phase == '$apply' || phase == '$digest') {
			if(fn && (typeof(fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	}
	//Init par défaut
	$scope.init = function () {
		this;
	}

	//Remove par défaut
	$scope.remove = function () {
		this.panneau.cacherPanneau();
	}
	//Fonctionnalité pas encore disponible...
	$scope.fonctionPasDisponible = function () {
		toaster.pop("note", "WEB_LABEL_FONCTIONNALITÉ_INDISPONIBLE");
	}
	//Fonctionnalité pas encore disponible...
	$scope.changerLangue = function (code_langue) {
		code_langue = code_langue || "fr";
		$translate.uses(code_langue);
	}
	//Fonctionnalité pas encore disponible...
	$scope.changerLangue = function (code_langue) {
		code_langue = code_langue || "fr";
		$translate.uses(code_langue);
	}

	//Fonctionnalité pas encore disponible...
	$scope.switchToInjecteur = function (code_langue) {
		localStorage.flag_inj = true;
		window.location.reload();
	}


	if ($scope.utilisateur_local.est_connecte){
		//Vérification que l'utilisateur a un rôle pour cette ressource	
		$scope.verifierRole();
		//Lancement du timer de polling des notifications
		PF.Notification.setUpdateTimer();
		//Lancement du timer de polling des messages
		Message.setUpdateTimer();
	}

	//Configuration des raccourcis clavier
	angular.element(document).on("keyup", function (e) {
		if (e.keyCode == 27){
			$scope.safeApply(function () {
				// body...
				Panneau.cacherDernierDeLaPile("globale");
			})
		}
	});
}]);