pf_app.controller('controller_profil_config', ["$scope", "toaster", function ($scope, toaster) {
	var defaults = {
		"visible" : false,
		"tableau_langues_label" : PF.langue.tableau_labels,
		"informations" : {
			"current_password" : "",
			"password" : "",
			"password2" : "",
			"name" : "",
			"etablissement" : "",
			"langue" : "3"
		}
	};
	angular.extend($scope, defaults);

	$scope.init = function () {
		$scope.informations.name = $scope.utilisateur.name;
		$scope.informations.etablissement = $scope.utilisateur.etablissement;
		$scope.informations.langue = $scope.utilisateur.id_langue;
	}

	$scope.enregistrerModifications = function ()  {
		var _scope = $scope;

		//Si un des champs est rempli
		if(typeof($scope.informations.current_password) != "undefined" 
			|| typeof($scope.informations.password) != "undefined" 
			|| typeof($scope.informations.password2) != "undefined")
		{
			//Il faut absolument que tous les champs soient remplis
			if(typeof($scope.informations.current_password) == "undefined" 
			|| typeof($scope.informations.password) == "undefined" 
			|| typeof($scope.informations.password2) == "undefined")
			{
				//Sinon, return
				return toaster.pop("error", "WEB_LABEL_RENSEIGNER_CHAMP");
			}
		}
		//Si ça roule
		PF.post('utilisateur/editer', $scope.informations, function(retour, status, headers, config) {
			if (retour['status'] == 'ok'){
				_scope.utilisateur_local.name = _scope.informations.name;
				_scope.utilisateur_local.etablissement = _scope.informations.etablissement;
				_scope.utilisateur_local.langue = _scope.informations.langue;
				_scope.panneau.togglePanneau();
				toaster.pop("success", "WEB_LABEL_INFO_MODIFIEE_AVEC_SUCCES");
			}
			//Ajout refusé
			else
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
				}
				else
					//Display de l'erreur correspondante
					toaster.pop('error', retour['message']);
			}
		});

	}

}]);