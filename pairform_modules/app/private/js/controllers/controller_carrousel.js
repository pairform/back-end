pf_app.controller('controller_carrousel', ["$scope", "PJService", function ($scope, PJService) {
	var defaults = {
		"visible" : false,
		"array_pj" : [],
		"index_pj" : 0
	};

	angular.extend($scope, defaults);

	$scope.init = function () {

		for (var i = 0; i < $scope.array_pj.length; i++) {
			var pj = $scope.array_pj[i];
			if(pj.nom_serveur_pj.match(/(.png|.jpg|.jpeg)$/))
				pj.type = "image";
			else if(pj.nom_serveur_pj.match(/(.mp4)$/))
				pj.type = "video";
			else
				pj.type = "file";

			pj.url = PF.globals.url.ws + "recupererPieceJointe?nom_serveur_pj=" + pj.nom_serveur_pj;
		};
	}

}]);