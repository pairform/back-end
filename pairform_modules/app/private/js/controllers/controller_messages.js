pf_app.controller('controller_messages', ["$scope", "$localStorage", "$sessionStorage", "$timeout", "toaster", "OpenLayerLazyLoad", "MessageService", "ThumbnailService", function ($scope, $localStorage, $sessionStorage, $timeout, toaster, OpenLayerLazyLoad, MessageService, ThumbnailService) {

	//Sauvegarde du message en cours d'écriture dans le localstorage, pour éviter de le perdre 
	//On stock le localStorage dans le scope pour synchro automatique
	//lorsque l'on rafraichit les messages (connexion / deconnexion...) ou changement de page
	$scope.storage = $sessionStorage.$default({
		pf_nouveau_message : {
			"contenu" : "",
			"est_defi" : "",
			"visibilite" : "",
			"id_langue" : "3",
			"code_langue" : "fr",
			"id_message_parent" : "",
			"visibilite_message_parent" : "",
			"liste_reseaux" : []
		}
	});

	var defaults = {
		"pieces_jointes": [], //Tableau contenant les pièces jointes
		"message_geolocalise": [], //Tableau contenant les messages geolocalises
		"carte_visible": false, //Booléen pour l'affichage de la carte
		"sort_types":["date", "votes"],
		"messages_array":[],
		"transversal":false,
		"limit_begin": 0,
		"defaultLimit": 4,
		"defaultLimitMin": 4,
		"defaultLimitMax": 7,
		"messageLuTimeout" : 5000, //Timeout pour envoyer les messages lus au serveur pour synchro
		"nom_tag":"",
		"num_occurence":"",
		"langueApp":"fr",
		"visible_menu_pj_message": false, //Booléen pour l'affichage des pjs
		"visible_menu_medaille" : undefined,
		"visible_menu_tags" : undefined,
		"nouveau" : $scope.storage.pf_nouveau_message, //Bind à la référence localStorage du scope
		"array_messages_lus_stack" : $localStorage.array_messages_lus_stack
	};

	angular.extend($scope, defaults);



	$scope.init = function  () {
		//Réinitialisation du nombre de messages affichés
        // $scope.resetLimit(); 
		//Evenement émis par la directive pfMessage lorsqu'elle apparait à l'écran
		$scope.$on("pf_message_lu", function (event, id_message) {
			$scope.pushMessagesLus(id_message);
		});

		//Gestion du fil de message lazy loaded
		$_pf('#comBar').on('scroll', function() {
	        if($_pf(this).scrollTop() + $_pf(this).innerHeight() + 20 >= $_pf(this)[0].scrollHeight) {
	            $scope.$apply(function (argument) {
	            	$scope.limit += $scope.defaultLimit; 
	            });
	        }
	        else if($scope.limit_begin !== 0 && $_pf(this).scrollTop() == 0) {
	        	//Sauvegarde de la taille du scroll pour remise à la position actuelle après chargement
	        	var _this = this,
	        		scroll_before_load = this.scrollHeight;
	            $scope.$apply(function (argument) {
	            	//Diminution de l'offset
	            	$scope.limit_begin -= $scope.defaultLimit; 
	            	//En parallele, raise de la limite au même titre que de l'offset
	            	$scope.limit += $scope.defaultLimit; 
	            	//Pas d'offset négatif : ça fait partir de la fin
	            	if($scope.limit_begin < 0)
	            		$scope.limit_begin = 0;
	            	//Evenement émis par la directive pfMessageRenderEvents, associé à un pf-message, pour indiquer 
					var listener = $scope.$on("pf:message_rendered_reverse", function (argument) {
						//Suppression du listener
						listener();
						//Set du scroll là ou il était avant le load
	        			_this.scrollTop = _this.scrollHeight - scroll_before_load;
						
					})
	            });
	        }
	    });

		/**
		* Watch de la variable data pour update des propositions sur la carte
		*/
		$scope.$on("pf:message:geolocalisation", function (event, lon_lat) {
			$scope.storage.pf_nouveau_message.geolocalisation = {};
			if (lon_lat) {
				$scope.storage.pf_nouveau_message.geolocalisation.latitude = lon_lat.lat;
				$scope.storage.pf_nouveau_message.geolocalisation.longitude = lon_lat.lon;
			}
		})
	}

	$scope.setDisplay = function () {
		//Je cache la carte
		$scope.toggleAffichageCarte(false, 0);
		$scope.sort_type = Message.getSavedSortForType(PF.globals.display_type);
		$scope.next_sort_type = $scope.sort_types.indexOf($scope.sort_type) + 1 >= $scope.sort_types.length ? $scope.sort_types[0] : $scope.sort_types[$scope.sort_types.indexOf($scope.sort_type) + 1];
	}

	$scope.pushMessagesLus = function (id_message) {

		if($localStorage.array_messages_lus.indexOf(id_message) == -1){
			$localStorage.array_messages_lus.push(id_message);

			//On synchronise uniquement si l'utilisateur est connecté
			if ($scope.utilisateur_local.est_connecte) {
				$localStorage.array_messages_lus_stack.push(id_message);
				$scope.sendMessagesLus($localStorage.array_messages_lus_stack);
			}
		}
	}
	$scope.sendMessagesLus = function (array_messages_lus) {
		$timeout.cancel($scope.timeout_sendMessagesLus);
		$scope.timeout_sendMessagesLus = $timeout(function sendMessagesLus () {
			PF.post("message/lu", {messages : array_messages_lus},
				function callback_success (retour) {
					if (retour['status'] == "ko") {
						debugger;
					}
					else {
						//Reset du tableau
						$localStorage.array_messages_lus_stack = [];
					}
				}
			);
		}, $scope.messageLuTimeout);
	}

	$scope.resetLimit = function () {
		$scope.limit = $scope.defaultLimit;
		$scope.limit_begin = 0;
	}
	$scope.setLimitMaxValue = function () {
		$scope.limit = 2000;
	}


	/**
	* Fonction appelé sur le clic du bouton carte pour un message
	* Elle permet de notifier la directive de centrer la carte sur le message en question
	* Si la carte est caché on l'affiche
	*/
	$scope.showMarqueurs = function(message){
		var data = {
			lng: message.geo_longitude,
			lat : message.geo_latitude,
			id: message.id_message
		};
		$scope.$emit('surClicDataPourCarte', data);
		$scope.toggleAffichageCarte(true);
	}

	/**
	* Fonction permettant de définir l'état de la carte : visible ou pas
	* Sans parametre elle permet également de changer l'état actuel de la carte
	* valeur : booléen pour l'affichage
	* temps : le temps à octroyer à l'animation
	*/
	$scope.toggleAffichageCarte = function(valeur, temps){
		if(valeur === undefined)
			$scope.carte_visible = !$scope.carte_visible;
		else
			$scope.carte_visible = valeur;	

		// if($scope.carte_visible){
		// 	//Carte visible
		// 	var height = $_pf('#map_').height() + 70;

		// 	$_pf('#formComBar').hide(100);
		// 	$_pf('#map_').show(temps || 500);
		// 	// $_pf('#comList').animate({
		// 	// 	marginTop: height+"px"
		// 	// });
		// }else{
		// 	//Carte caché
		// 	var height = $_pf('#formComBar').height() + 50;

		// 	//hack car par défault la hauteur vaut 0 au lieu de 100 ...
		// 	if(height == 50){
		// 		height += 50;
		// 	}

		// 	$_pf('#map_').hide(100);
		// 	$_pf('#formComBar').show(temps || 500);
		// 	// $_pf('#comList').animate({
		// 	// 	marginTop: height+"px"
		// 	// });
		// }
	}

	/**
	* Fonction appellé par la directive pf-carte lorsqu'un marqueur est cliqué
	* marqueurData correrspond à la valeur {id} passé dans le tableau des marqueurs
	*/
	$scope.surClicMarqueur = function(marqueurData){

		//Affichage de tous les messages avant de highlighter
    	// $scope.setLimitMaxValue(); 
		$scope.highlight(
			marqueurData,
			100, 
			true);
	}
	
	/**
	* Fonction qui va sélectionner les messages géolocalisés parmis la liste des messages
	* Elle va également parametrer les messages contenant des pièces jointes 
	* Une fois terminé elle envoie les informations à la directive de carte
	*/
	$scope.calculProprieteMessage = function(messages){
		$scope.message_geolocalise = [];

		angular.forEach(messages, function(message, key) {

			//Gestion de la géolocalisation
			message.estGeolocalise = false;
			if(message.geo_latitude && message.geo_longitude){
				if(Number(message.geo_latitude) != 0 && Number(message.geo_longitude) != 0){
					$scope.message_geolocalise.push({
						lat: message.geo_latitude,
						lng: message.geo_longitude,
						id: message.id_message
					});
					message.estGeolocalise = true;
				}
			}

			//Gestion des pièces jointes
			if(typeof(message.pieces_jointes) === 'string')
				message.pieces_jointes = JSON.parse(message.pieces_jointes);
			
		});
		
		$scope.$emit('modificationData', $scope.message_geolocalise);
	}

	$scope.repondre = function (message) {
		//Si le message est déjà une réponse
		if (message.id_message_parent != "") 
			$scope.storage.pf_nouveau_message.id_message_parent = message.id_message_parent;
		else
			$scope.storage.pf_nouveau_message.id_message_parent = message.id_message;

		//On donne le focus au textarea
		$_pf('#formComBar textarea').focus();
		//On met le pseudo
		$scope.storage.pf_nouveau_message.contenu = "@"+message.pseudo_auteur+" ";
		$scope.storage.pf_nouveau_message.message_parent_prive = message.prive;
		//On met en avant le message
		$scope.highlight(message.id_message);
	}
	$scope.envoyer = function  (params) {
		if ($scope.utilisateur_local.est_connecte || PF.globals.is_vitrine) {
			var params = params || this.storage.pf_nouveau_message;
			//On trime le contenu du message
			params.contenu = params.contenu.trim();
			//S'il n'y a rien à envoyer
			if (!!params.contenu) {

				//On ne peut envoyer qu'un message à la fois
				if (typeof(PF.globals.sendingMessage) == "undefined")
				{
					//Flag pour éviter les doubles post
					PF.globals.sendingMessage = true;
					var nom_page_temp = ($scope.nom_tag == "" && $scope.num_occurence == "") ? "" : PF.globals.nom_page;

					//Cas spécial pour les démos
					if(PF.globals.is_vitrine){
						//On met un id random directement sur le message
						//Assez haut pour qu'il n'y ait pas de conflit futur
						params.id_message = Math.round(Math.random() * 10000000);
						//On enlève le flag de double post, puisqu'il n'y a pas d'échange avec le serveur
						delete PF.globals.sendingMessage;

						//Si c'est bien l'utilisateur qui a écrit un post,
						//Il n'y a pas de flag demo_reponse_automatique
						if (!params.demo_reponse_automatique) {
							//Si on est sur le site vitrine, on met un compte démo qui n'existe pas
							$scope.utilisateur_local = new Utilisateur({"username" : "Vous", id_utilisateur:0, "avatar_url" : "https://secure.gravatar.com/avatar/?d=mm"});
						
							//On garde en mémoire le nombre de message écrit par l'utilisateur
							//TODO: mettre ça dans le localstorage
							if (!PF.globals.demo_nombre_message_postes)
								PF.globals.demo_nombre_message_postes = 0;

							PF.globals.demo_nombre_message_postes++;
							//Stockage des parametres dans une autre variable, pour contourner le reset post-envoi de params
							var _params = angular.copy(params);
							//On écrit une réponse automatiquement à ce poste dans 2 secondes
							setTimeout(function(){
								//On stocke l'utilisateur démo
								var _utilisateur_demo = $scope.utilisateur_local;
								var contenu_reponse = [
								 "@Vous Bravo, vous avez posté votre premier commentaire! \r\n A présent, essayez de répondre à ce commentaire...",
								 "@Vous Vous pouvez également voter pour ou contre un commentaire. Plus un message est voté, plus il est visible. \r\n Essayez de voter pour ce commentaire!",
								 "@Vous Pour continuer, peut-être voulez-vous créer un compte? Cliquez sur l'avatar en haut à gauche de votre écran.",
								 "@Vous Si vous voulez tester toutes les fonctionalités de PairForm, vous pouvez aller dans la ressource Bac à sable. Rendez vous sur le kiosque pour cela !"];
								

								//Flag de réponse automatique pour ne pas répondre à une réponse automatique
								var index_contenu_reponse = PF.globals.demo_nombre_message_postes > contenu_reponse.length ? contenu_reponse.length - 1 : PF.globals.demo_nombre_message_postes -1,
									demo_reponse = {
									"demo_reponse_automatique" : true,
									"contenu": contenu_reponse[index_contenu_reponse],
									"est_defi":"",
									"visibilite":"",
									"id_langue":"3",
									"code_langue":"fr",
									"id_message_parent": _params.id_message_parent ? _params.id_message_parent : _params.id_message,
									"visibilite_message_parent":"",
									"liste_reseaux":[],
									"message_parent_prive":0
								};

								//Si on a posté déjà un message, on est censé essayer de répondre au message de PairForm
								//Si l'utilisateur ne l'a pas fait
								if (index_contenu_reponse == 1 && !_params.id_message_parent)
								{
									//On reset le nombre de messages postés, pour que l'utilisateur n'avance pas plus dans les étapes
									//(cette variable est incrémenté quand l'utlisateur juste après dans le code, au moment de l'envoi du message.)
									PF.globals.demo_nombre_message_postes--;
									demo_reponse.contenu = "@Vous Pour répondre à un commentaire, cliquez sur le bouton \"répondre\" sur ce dernier!";
								}

								//On publie la réponse
								$scope.envoyer(demo_reponse);

							}, Math.round(Math.random() * 2000) + 1000);
							//Random sur le timing, pour que ça à l'air naturel (avec minimum une seconde de temps de réponse)
						}
						//Dans le cas d'une réponse automatique
						else{
							//On connecte l'utilisateur PairForm, qu'on va déconnecter par la suite
							if(PF.globals.url.root.match("www.pairform.fr")){
								$scope.utilisateur_local = new Utilisateur({
								  "id_utilisateur": 35,
								  "username": "PairForm",
								  "avatar_url": PF.globals.url.res + "avatars/35.png",
								  "rank": {
										"112": {
										  "id_categorie": "5",
										  "nom_categorie": "Administrateur"
										}
								  }
								});
							}
							else{
								$scope.utilisateur_local = new Utilisateur({
								  "id_utilisateur": 1,
								  "username": "PairForm",
								  "avatar_url": PF.globals.url.res + "avatars/1.png",
								  "rank": {
										"2": {
										  "id_categorie": "5",
										  "nom_categorie": "Administrateur"
										}
								  }
								});
								
							}
						}

					}

					angular.extend(params, {
						"id_auteur" : $scope.utilisateur_local.id_utilisateur,
						"id_capsule" : $scope.capsule.id_capsule,
						"id_role_auteur" : $scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie,
						"est_defi" : +params.est_defi,
						"nom_auteur" : $scope.utilisateur_local.name,
						"nom_page" : nom_page_temp,
						"nom_tag" : $scope.nom_tag,
						"num_occurence" : $scope.num_occurence,
						"pseudo_auteur" : $scope.utilisateur_local.username,
						"role_auteur" : $scope.utilisateur_local.rank[$scope.capsule.id_capsule].nom_categorie,
						"url_avatar_auteur" : $scope.utilisateur_local.avatar_url,
						"prive" : ((params.message_parent_prive && (params.message_parent_prive != "0")) || (params.liste_reseaux.length > 0) ? 1 : 0),
						"visibilite" : JSON.stringify(params.liste_reseaux.map(function (reseau) { return reseau.id_collection}))
					});
					
					/* Ajout des parametres de géolocalisation le cas échéant */
					if($scope.storage.pf_nouveau_message.geolocalisation && $scope.storage.pf_nouveau_message.geolocalisation.latitude){
						params.geo_latitude = $scope.storage.pf_nouveau_message.geolocalisation.latitude;
						params.geo_longitude = $scope.storage.pf_nouveau_message.geolocalisation.longitude;
					}
					else{
						delete params.geo_latitude;
						delete params.geo_longitude;
					}

					var nouveau_message = new Message(params);

					if(typeof($localStorage.array_messages[nouveau_message.id_capsule]) == "undefined")
						$localStorage.array_messages[nouveau_message.id_capsule] = {};
					
					if(typeof($localStorage.array_messages[nouveau_message.id_capsule][nouveau_message.nom_page]) == "undefined")
						$localStorage.array_messages[nouveau_message.id_capsule][nouveau_message.nom_page] = [];
					
					$localStorage.array_messages[nouveau_message.id_capsule][nouveau_message.nom_page].push(nouveau_message);
					// $scope.messages_array.push(nouveau_message);

					//Si c'est un message posté par l'utilisateur, et pas une réponse simulée
					if (!params.demo_reponse_automatique){
						toaster.pop("success", "WEB_LABEL_COMMENTAIRE_POSTE");
					}
					
					this.storage.pf_nouveau_message.contenu = "";
					//RAZ géolocalisation
					$scope.storage.pf_nouveau_message.geolocalisation = {};
					$scope.visible_menu_option = null;
					//Remise à la hauteur normal du champ de texte
					document.querySelector('#formComBar textarea').style.removeProperty("height");

					//Si on est sur le site vitrine, on quitte direct : on ne veut pas que les messages soient vraiment postés sur le serveur.
					if(PF.globals.is_vitrine){

						//Si on est sur le site vitrine, on re-met un compte démo qui n'existe pas
						$scope.utilisateur_local = new Utilisateur({"username" : "Vous", "avatar_url" : "https://secure.gravatar.com/avatar/?d=mm"});
						_paq.push(['trackEvent',  'Interaction - Site vitrine', 'Message posté', PF.globals.capsule.nom_long]);
						return;
					}

					MessageService.envoyer(nouveau_message, $scope.pieces_jointes, function(data, status, headers, config) {
						var retour = data;

						if (retour['status'] == "ok") {	
							
							//Obliger de rafraichir dans un timeout, pour éviter le digest en cours
							// $timeout(function () {
								//Important : cast en string de l'identifiant
								nouveau_message.id_message = retour['id_message'];
								
								if(retour["dataPJ"]["dataPJ"]){
									nouveau_message.pieces_jointes = [];
									for(var key in retour["dataPJ"]["dataPJ"]){
										var file = retour["dataPJ"]["dataPJ"][key];

										nouveau_message.pieces_jointes.push({
											nom_original_pj: file.originalname,
											nom_thumbnail: file.nom_thumbnail,
											nom_serveur_pj: file.name,
											taille_pj: file.size,
											extension_pj: file.extension
										});
									}
								}
								//RaZ de la geolocalisation
								delete $scope.storage.pf_nouveau_message.geolocalisation;
								
								$scope.highlight(nouveau_message.id_message, 200, true);

								//Notification a qui veut
								$scope.$root.$broadcast("pf:message:envoi", nouveau_message);
								// nouveau_message.id_message = ""+retour['id_message'];
							// }, 200);
							$scope.pushMessagesLus(nouveau_message.id_message);
							_paq.push(['trackEvent',  'Interaction - App web', 'Message posté', PF.globals.capsule.nom_long]);
						}
						else{
							if (retour['message'] == "ws_utilisateur_invalide") {
								$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
							}
							else
								toaster.pop('error', retour['message']);
							
							_paq.push(['trackEvent',  'Interaction - App web', 'Message raté', PF.globals.capsule.nom_long]);

							$localStorage.array_messages[nouveau_message.id_capsule][nouveau_message.nom_page].pop();
							$scope.messages_array.pop();
						}
						$scope.pieces_jointes = [];
						//Update manuel des notifications
						PF.Notification.updateAllWithDelay();
						PF.globals.sendingMessage = undefined;
					});
						
				}
			}
			else
				toaster.pop("error", "LABEL_ERREUR_SAISIE_MESSAGE");

		}
		else
			$scope.afficherConnexion();

	}
	$scope.voter = function (message, vote) {

		//Si on est pas connecté, on quitte (Sauf si on est sur le site vitrine)
		if (!$scope.utilisateur_local.est_connecte && !PF.globals.is_vitrine) {	
			$scope.afficherConnexion();
			return false;
		}
		//On check si l'utilisateur essaie de voter sur son message
		if($scope.utilisateur_local.id_utilisateur == message.id_auteur)
		{
			toaster.pop("error", "WEB_LABEL_PAS_VOTER_VOTRE_MESSAGE");	
			return;
		}

		//Récupération du type de vote
		//True pour up, false pour down
		var vote_up = vote == 1 ? true : false;

		//Annulation du vote
		if(vote == message.utilisateur_a_vote)
		{
			//Update du décompte
			message.somme_votes = (parseInt(message.somme_votes) + (vote_up ? -1 : 1)) + "";
			message.utilisateur_a_vote = "0";
		}
		else
		{		
			//Nouveau vote	
			if(message.utilisateur_a_vote == 0)
			{
				//Update du décompte
				message.somme_votes = (parseInt(message.somme_votes) + (vote_up ? 1 : -1)) + "";
				message.utilisateur_a_vote = ""+vote;
			}
			//Changement de vote
			else
			{
				//Update du décompte
				message.somme_votes = (parseInt(message.somme_votes) + (vote_up ? 2 : -2)) + "";
				message.utilisateur_a_vote = ""+vote;
			}
		}
		//Si on est sur le site vitrine,
		if(PF.globals.is_vitrine){
			//On stope ici, pas de communication avec le serveur
			return;
		}
		//Variable de post de message
		var post_vote = {'up' : vote_up , "id_message" : message.id_message};

		_paq.push(['trackEvent',  'Interaction - App web', "Vote " + (vote == 1 ? "positif" : "négatif"), PF.globals.capsule.nom_long]);

		PF.post("message/voter", post_vote, function (retour) {
			if (retour['status'] == "ko") {
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
				}
				else
					toaster.pop('note', retour["message"]);
			}
			else{
				PF.Notification.updateAllWithDelay();
			}
		});

		//Highlight du message
		$scope.highlight(message.id_message, 0, true);
	}
	$scope.afficherVotes = function (message) {
		
		//Si on est pas connecté, on quitte (Sauf si on est sur le site vitrine)
		if (!$scope.utilisateur_local.est_connecte) {	
			$scope.afficherConnexion();
			return false;
		}
		if ($scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie < 2) {
			toaster.pop("note", "WS_ERREUR_SUPPRESSION");
			return;
		}

		message.toggle_somme_votes = !message.toggle_somme_votes;
		if (message.toggle_somme_votes) {
			PF.get("message/votes", {"id_message" : message.id_message}, function (retour) {
				if (retour['status'] == "ko") {
					if (retour['message'] == "ws_utilisateur_invalide") {
						$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
					}
					else
						toaster.pop('note', retour["message"]);
				}
				else{
					angular.extend(message, retour.data[0]);
				}
			});
		}

	}
	$scope.afficherMessagesDepuisOA = function (_messages_array, _nom_tag, _num_occurence, highlight, show_all, client_rect){

		//Réinitialisation du nombre de messages affichés
		if (highlight || show_all)
			//Affichage de tous les messages avant de highlighter
        	$scope.setLimitMaxValue(); 
        else
        	$scope.resetLimit(); 


		$scope.toggleAffichageCarte(false, 0);
		$scope.calculProprieteMessage(_messages_array);
		
		//Reset du message parent et de la visibilité de ce dernier
		//Aucune chance de répondre à un message si on change d'endroit
		$scope.storage.pf_nouveau_message.id_message_parent = "";
		$scope.storage.pf_nouveau_message.visibilite_message_parent = "";
		

		$scope.nom_tag = _nom_tag;
		$scope.num_occurence = _num_occurence;
		$scope.messages_array = _messages_array;
		// $scope.messages_array = messagesFilter(_messages_array, $scope);
		PF.globals.display_type = "normal";

		$scope.transversal = false;
		
		//Cachage de la carte, et récupération des paramètres sauvegardés
    //en local storage pour l'affichage en question 
    $scope.setDisplay();

		// var ids_messages_lus = $_pf.map($scope.messages_array, function (message) {
		// 	return message.id_message;
		// });
		// //MAJ des messages lus
		// for (var i = ids_messages_lus.length - 1; i >= 0; i--) {
		// 	if($localStorage.array_messages_lus.indexOf(ids_messages_lus[i]))
		// 		$localStorage.array_messages_lus.push(ids_messages_lus[i]);
		// }
		
		$scope.safeApply(function (argument) {
			// debugger
			if (!$scope.panneau) {
				var panneau = new Panneau({'nom_controller' : 'controller_messages', 'position' : 'bottom'});

				//Si on est sur une fenêtre flottante
				if (panneau.view.attr("pf-message-list"))
					//On passe les bounds de l'élément cliqué
					panneau.client_rect = client_rect;

				panneau.togglePanneau();
			}
			else if (!$scope.panneau.estAffiche()){
				//Si on est sur une fenêtre flottante
				if ($scope.panneau.view.attr("pf-message-list"))
					//On passe les bounds de l'élément cliqué
					$scope.panneau.client_rect = client_rect;

				$scope.panneau.togglePanneau();
			}
			else {
				//Si on est sur une fenêtre flottante et qu'elle n'est pas détachée
				if ($scope.panneau.view.attr("pf-message-list") && !$scope.panneau.view.hasClass("detached")){
					//On passe les bounds de l'élément cliqué
					$scope.panneau.client_rect = client_rect;
					$scope.panneau.positionnerPanneau();
				}
			}
			//On rajoute la classe sticky, qu'on enlève quelque seconde après.
			//Pour éviter que le panneau ne disparaisse si l'utilisateur n'était pas en hover dessus
			// $scope.panneau.view.addClass("sticky");
			// setTimeout(function () {
			// 	$scope.panneau.view.removeClass("sticky");
			// }, 4000);
		});

		//On met un flag sur HTML pour pouvoir contrôler l'overflow quand on est sur mobiles
		angular.element("html").addClass("pf-messages-open");

		if (highlight)
			$scope.highlight(highlight, 200, true);
		_paq.push(['trackEvent',  'Interaction - App web', 'Messages lus (grain)', PF.globals.capsule.nom_long + "(" + ($scope.utilisateur_local.id_utilisateur ? $scope.utilisateur_local.username : "anonymous") +")"]);
	}
	$scope.afficherMessagesTransversal = function (idMessToFocus, idUser){
		//Réinitialisation du nombre de messages affichés
    $scope.resetLimit();
		//Inutile
		//var post = JSON.stringify(array_messages[focusedItem.data("nom_tag")][focusedItem.data("num_occurence")]['messages']);
		var messages_array_before_sort = {};

		$scope.nom_tag = "*";
		$scope.num_occurence = "*";
		$scope.uid_page = "*";
		$scope.uid_oa = "*";


		//On vérifie que l'utilisateur ne commente pas sur un sous menu s'il ne visionne pas les messages de la ressource
		// if(PF.pageIsMenu() && focusedItem.attr('id') !== PF.globals.selectorRes.split("#")[1])
		// {
		// 	toastr.pop('error','web_label_erreur_commenter_sous_menu');
		// 	return false;
		// }

		if (typeof(idUser) != "undefined")
		{
			PF.globals.display_type = "user";
			$scope.id_utilisateur = idUser;
			// recupererMessagesRessourceWithoutLanguageFilter();
		}
		else{
			PF.globals.display_type = "transversal";
		}
    //Cachage de la carte, et récupération des paramètres sauvegardés
    //en local storage pour l'affichage en question 
    $scope.setDisplay();

		//Tableau de tous les messages de la page
		// var messages_page =JSON.parse(PF._localStorage.array_messages);
		//Tableau de tous les messages de toutes les autres pages
		var messages_pages = $localStorage.array_messages[PF.globals.capsule.id_capsule];
		var messages_array_before_sort = [];

		$_pf.each(messages_pages, function (page, messages_of_pages) {
			if(page != "_timestamp_last_message")
				messages_array_before_sort = messages_array_before_sort.concat(messages_of_pages);
		});
		
		
		_paq.push(['trackEvent',  'Interaction - App web', 'Message lus (transversal)', PF.globals.capsule.nom_long]);	

		$scope.messages_array = messages_array_before_sort || [];
		// $scope.messages_array = messagesFilter(messages_array_before_sort, $scope) || [];

		$scope.toggleAffichageCarte(false, 0);
		$scope.calculProprieteMessage($scope.messages_array);

		//Transversal
		$scope.transversal = true;


		// $scope.langueApp = LanguageM.langueApp;

		$scope.safeApply(function (argument) {

			if (!$scope.panneau) {
				var panneau = new Panneau({'nom_controller' : 'controller_messages', 'position' : 'bottom'});
				panneau.togglePanneau();
			}
			else if (!$scope.panneau.estAffiche()){
				$scope.panneau.togglePanneau();
			}
			//On rajoute la classe sticky, qu'on enlève quelque seconde après.
			//Pour éviter que le panneau ne disparaisse si l'utilisateur n'était pas en hover dessus
			$scope.panneau.view.addClass("sticky");
			setTimeout(function () {
				$scope.panneau.view.removeClass("sticky");
			}, 4000);
		});

		// $scope.$apply();

		if (typeof(idMessToFocus) != "undefined")
		{
			$scope.highlight(idMessToFocus, 200, true);
		}
		$_pf('.focusedItem').removeClass('focusedItem');
	}

	$scope.supprimer = function (message, definitif) {
		var params = {'id_message' : message.id_message.toString(),
							'supprime_par' : message.supprime_par.toString(),
							'id_user_op' : message.id_auteur.toString(),
							'id_rank_user_op' : message.id_role_auteur.toString(),
							'id_user_moderator' : $scope.utilisateur_local.id_utilisateur,
							'id_rank_user_moderator' : $scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie,
							'id_capsule' : PF.globals.capsule.id_capsule};

		//Si la suppression est définitive
		if (typeof(definitif) != "undefined"){
			//Si oui, on cast definitif en int et on le rajoute aux paramètres
			params['supprime_def'] = +definitif;
		
			toaster.pop("success", "WEB_LABEL_MSG_SUPPRIME_DEFINITIVEMENT");
			var index = $localStorage.array_messages[message.id_capsule][message.nom_page].indexOf(message);
			$localStorage.array_messages[message.id_capsule][message.nom_page].splice(index, 1);
		}
		else{
			if(message.supprime_par == "0")
			{
				//On set le data et on ajoute la classe
				message.supprime_par = $scope.utilisateur_local.id_utilisateur;
				toaster.pop("success", "WEB_LABEL_MSG_SUPPRIMER_AVEC_SUCCES");
			}
			else
			{
				//Si le message a été supprimé par quelqu'un d'autre, et que l'utilisateur n'a pas au moins un petit coeur (<3) d'animateur
				if (($scope.utilisateur_local.rank[$scope.capsule.id_capsule].id_categorie <3) && (message.supprime_par != $scope.utilisateur_local.id_utilisateur)) {
					//On empêche la réactivation
					return toaster.pop("success", "WEB_LABEL_AVOIR_RANG_ELEVE");
				}
				//Sinon c'est cool
				else{
					//On set le data et on ajoute la classe
					message.supprime_par = "0";
					toaster.pop("success", "WEB_LABEL_MSG_RETABLI_AVEC_SUCCES");
				}
			}
		}

		PF.post("message/supprimer", params, function(data){
		   
		   var retour = data;

			if(retour['status'] == "ok")
			{
				//Si la suppression est définitive
				
			}
			else
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
				}
				else
					toaster.pop('error',retour['message']);

				if (typeof(definitif) != "undefined"){
					$localStorage.array_messages[message.id_capsule][message.nom_page].push(message);
				}
				else{
					//On set le data et on ajoute la classe
					if(message.supprime_par == "0")
						message.supprime_par = $scope.utilisateur_local.id_utilisateur;
					//On set le data et on ajoute la classe
					else
						message.supprime_par = "0";
				}
			}
		});
	}

	$scope.donnerMedaille = function (message, type_medaille) {
		var post = {'id_message' : message.id_message};

		//Pour ne pas garder de référence
		var _old_medaille = ""+message.medaille;
		
		if (type_medaille != "enlever"){
			post.type_medaille = type_medaille;
			message.medaille = type_medaille;
		}
		else
			message.medaille = "";


		PF.post('message/donnerMedaille',post, function (retour) {
			if (retour['status'] == "ko") 
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
				}
				else 
					toaster.pop('error', retour['message']);

				message.medaille = _old_medaille;
			}
		});
	}

	$scope.ajouterTags = function (message, tags) {
		var tags_array = tags.replace(/\s/g, "").split(",");
		var post = {"id_message" : message.id_message, "tags" : tags_array};
		// $scope.fonctionPasDisponible();
		PF.post('message/tags',post, function (retour) {
			if (retour['status'] == "ko") 
			{
				toaster.pop('error', retour['message']);
			}
			else if (retour['status'] == "up") {
				var tags_to_merge = retour['data'];
				toaster.pop('success', "Tags ajoutés avec succès.");
				//Rajout des tags dans le message
				var old_tags = JSON.parse(message.tags);
				message.tags = JSON.stringify(old_tags.concat(tags_to_merge));
			}
			else if (retour['status'] == "ok") {
				toaster.pop('note', "Aucune modification.");
			}
		});
	}
	$scope.supprimerTag = function (message, tag) {
		var post = {"id_message" : message.id_message, "tag" : tag};
		// $scope.fonctionPasDisponible();
		PF.delete('message/tag', post, function (retour) {
			if (retour['status'] == "ko") 
			{
				toaster.pop('error', retour['message']);
			}
			else if (retour['status'] == "ok") {
				toaster.pop('success', "Tags supprimé.");
				//Rajout des tags dans le message
				var old_tags = JSON.parse(message.tags),
					index = old_tags.indexOf(tag);
				old_tags.splice(index, 1);
				message.tags = JSON.stringify(old_tags);
			}
		});
	}
	$scope.validerMessageDefi = function (message) {
		var old_value = message.defi_valide;

		if (old_value == "0") {
			message.defi_valide = "1";
		}
		else{
			message.defi_valide = "0";	
		}
		PF.post('message/defi/valider', {"id_message" : message.id_message, "defi_valide" : message.defi_valide}, function (retour) {
			if (retour['status'] == "ko") 
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
				}
				else
					toaster.pop("error", "WS_ERREUR_CONTACTER_PAIRFORM");

				message.defi_valide = old_value;
			}
		});
	}
	$scope.terminerDefi = function (message) {
		var old_value = message.est_defi;
		//On reverifie bien que le message est un défi
		if (old_value == "1") {
			message.est_defi = "2";

			PF.post('message/defi/terminer',{"id_message" : message.id_message, "id_capsule" : $scope.capsule.id_capsule}, function (retour) {
				if (retour['status'] == "ko") 
				{
					if (retour['message'] == "ws_utilisateur_invalide") {
						$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
					}
					else 
						toaster.pop("error", "WS_ERREUR_CONTACTER_PAIRFORM");
					message.est_defi = old_value;
				}
			});
		}	
	}
	$scope.partagerMessage = function (message) {
		if($scope.visible_menu_partager == message.id_message)
			return $scope.visible_menu_partager = undefined;

		$scope.visible_menu_partager = message.id_message;
		$scope.message_permalien = $scope.res_url_root + 'webServices/message/rediriger?id_message=' + message.id_message;
		
		//timeout, pour qu'angular ait le temps de créer le menu
		setTimeout(function () {
			var text = document.querySelector("pf-menu-partager span"), 
				range, 
				selection;

			if (document.body.createTextRange) {
				range = document.body.createTextRange();
				range.moveToElementText(text);
				range.select();
			} else if (window.getSelection) {
				selection = window.getSelection();        
				range = document.createRange();
				range.selectNodeContents(text);
				selection.removeAllRanges();
				selection.addRange(range);
			}
		}, 250);
	}

	$scope.deplacerMessage = function (message) {

		//Récupération des réponses du messages s'il y en a
		if (!$localStorage.array_messages_deplacement || !$localStorage.array_messages_deplacement.length) {
			$localStorage.array_messages_deplacement = [message];	
		}
		else{
			var index = -1;

			$localStorage.array_messages_deplacement.some(function (msg, idx, arr) {
				if (msg.id_message == message.id_message)
					return index = idx;
			});
			if (index === -1)
				$localStorage.array_messages_deplacement.push(message);
			else
				$localStorage.array_messages_deplacement.splice(index, 1);
		}
		//On fait pointer une variable globale a tous les controllers sur le local storage,
		//Pour pouvoir watcher ca dans le controller nav_bar
		$scope.$root.array_messages_deplacement = $localStorage.array_messages_deplacement;
	}

	$scope.ajouterReseauVisibilite = function (reseau) {
		var reseau_in_liste = $scope.storage.pf_nouveau_message.liste_reseaux.filter(function(e){return e.id_collection == reseau.id_collection})
		if (reseau_in_liste.length == 0)
			$scope.storage.pf_nouveau_message.liste_reseaux.push(reseau);
		else
			$scope.enleverReseauVisibilite(reseau);
	}
	$scope.enleverReseauVisibilite = function (reseau) {
		var reseau_in_liste = $scope.storage.pf_nouveau_message.liste_reseaux.filter(function(e){return e.id_collection == reseau.id_collection})
		if (reseau_in_liste.length > 0){
			var index = $scope.storage.pf_nouveau_message.liste_reseaux.indexOf(reseau_in_liste[0]);
			$scope.storage.pf_nouveau_message.liste_reseaux.splice(index, 1);
		}
	}
	$scope.highlight = function(id_message_to_focus, delay, bounce, scrollDiff){

		bounce = bounce || false;
		delay = delay || 0;
		//On parse en int pour comparaison efficace avec les data contenus dans les pfmessages
		if (typeof id_message_to_focus == "string")
		{
			id_message_to_focus = parseInt(id_message_to_focus);
		}
		//Détermination de la position du message, s'il est affiché ou non
		var index_of_message = $scope.messages_sorted_array.findIndex(function (element, index, array) {
			return element.id_message === id_message_to_focus
		})
		//Si la limite de message a afficher actuel est inférieur à l'index cherché
		if ($scope.limit < index_of_message || $scope.limit_begin > index_of_message) {
			//On pousse la limite jusque là
			$scope.safeApply(function () {
				//Offset -1 pour afficher pile le bon nombre de message pour le highlight
				$scope.limit_begin = index_of_message - ($scope.defaultLimit -1);
				$scope.limit = $scope.defaultLimit;
				//Focus une fois que les messages ont été chargés par ng-repeat (asynchrone)
				var listener = $scope.$on("pf:message_rendered", function (argument) {
					//Suppression du listener
					listener();
					focus(id_message_to_focus, delay, bounce, scrollDiff);
				})
			})
		}	
		else{
			focus(id_message_to_focus, delay, bounce, scrollDiff);
		}

		function focus (id_message_to_focus, delay, bounce, scrollDiff) {
			$timeout(function () {
				var position = 0; //Variable contenant la hauteur de tous les messages avant celui recherché
				var comBoxToFocus; //Balise contenant le message recherché

				//Pour toutes les balises pf-messages
				$_pf('pf-message').each(function (index) {
					if (!angular.element(this))
						return false;

					//Si on trouve notre balise , pn la récupère et on quitte
					if(angular.element(this).data('id_message') == id_message_to_focus){
						comBoxToFocus = angular.element(this);
						return false;
					}
					//Sinon j'ajoute la taille de cette balise à la taille du scroll qui devrat être effectué
					position += this.clientHeight;
					parseInt(window.getComputedStyle(this).marginBottom.replace('px', ''));
								// parseInt(angular.element(this).css('padding-top').replace('px', '')) +
								// parseInt(angular.element(this).css('padding-bottom').replace('px', '')) + 
								// parseInt(angular.element(this).css('margin-bottom').replace('px', '')) + 
				});
				
				if (comBoxToFocus && comBoxToFocus.length){
					$_pf("html, body").animate({
						scrollTop: window.pageYOffset + $_pf(".focusedItem")[0].getBoundingClientRect().top - 100
					}, 2000, "swing", function () {});

					$_pf('#comBar').animate({
						scrollTop: position
					}, 1000, "swing", function () {
						if (bounce) {
							comBoxToFocus.addClass('highlighted');
							setTimeout(function () {
								comBoxToFocus.removeClass('highlighted');
							},1000);
						}
					});
				}
			},delay);
		}
	}
	$scope.changerOrdreTri = function (){
		var current_index = $scope.sort_types.indexOf($scope.sort_type);
		if (current_index + 1 >= $scope.sort_types.length){
			$scope.sort_type = $scope.sort_types[0];
			$scope.next_sort_type = $scope.sort_types[1];
		}
		else {
			$scope.sort_type = $scope.sort_types[current_index + 1];

			if (current_index + 2 >= $scope.sort_types.length)
				$scope.next_sort_type = $scope.sort_types[0];
			else
				$scope.next_sort_type = $scope.sort_types[current_index + 2];
		}
		//Enregistrement en localstorage
		Message.setSavedSortForType($scope.sort_type, PF.globals.display_type);
	}

	if(!$scope.utilisateur_local.preferences)
		$scope.utilisateur_local.preferences = {"expanded" : false};

	$scope.expanded = $scope.utilisateur_local.preferences.expanded || false;
	$scope.defaultLimit = $scope.expanded ? $scope.defaultLimitMax : $scope.defaultLimitMin;
	
	$scope.changerAffichage = function (){
		$scope.expanded = !$scope.expanded;
		$scope.defaultLimit = $scope.expanded ? $scope.defaultLimitMax : $scope.defaultLimitMin;
	
		$scope.utilisateur_local.preferences.expanded = $scope.expanded;
		// document.querySelector("#comBar").style.cssText = "";
	}

	$scope.afficherGeolocalisation = function () {
		$scope.visible_menu_options = $scope.visible_menu_options == 'geolocalisation' ? null : 'geolocalisation';	
	}
	$scope.afficherOptions = function () {
		$scope.visible_menu_options = $scope.visible_menu_options == 'message' ? null : 'message';	
	}
	$scope.afficherReseaux = function () {
		var controller_reseau = PF.getScopeOfController('controller_reseau')
		controller_reseau.init();
		$scope.visible_menu_options = $scope.visible_menu_options == 'reseaux' ? null : 'reseaux';	
	}

	$scope.afficherPiecesJointes = function(){
		$scope.visible_menu_options = $scope.visible_menu_options == 'pieces_jointes' ? null : 'pieces_jointes';	
	}

	$scope.parseJSON = function (json) {
		return JSON.parse(json);
	}

	//Redimensionnement automatique de la textarea quand elle dépasse la taille
	var tx = document.querySelector('#formComBar textarea');
  tx.addEventListener("input", OnInput, false);

	function OnInput() {
		if(this.scrollHeight > 80){
		  this.style.overflowY = "hidden";
		  this.style.height = (this.scrollHeight) + 'px';
		}
		else{
		  this.style.removeProperty("overflow-y");
			this.style.removeProperty("height");
		}
	}
	$scope.$watch("visible", function (new_value, old_value) {
		if (old_value == true && new_value == false) {
				angular.element("html").removeClass("pf-messages-open");
		}
	})
	// Initialisation de la vue
	$scope.init();

}]);