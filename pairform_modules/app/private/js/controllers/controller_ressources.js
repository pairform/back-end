
pf_app.controller('controller_ressources', ["$scope", "$http", "toaster", function ($scope, $http, toaster) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"switch_sort_mode" : 0
	};
	angular.extend($scope, defaults);

	var resParDomaine = {};
	var resParEspace = {};

	$scope.init = function () {
		if (!$scope.loaded) {
			var _scope = $scope;
			PF.get('ressource/liste', {}, function(data, status, headers, config) {
				var retour = data;
				// S'il y a des ressources
				if (retour['status'] == 'ok')
				{
					var ressources = retour['ressources'];
					//On va chercher le nombre de messages / On enlève le & du début de post_OS
					_scope.ressources = ressources;
					//Tri des ressources, on commence à afficher
					_scope.sortRessources();
					//Parallèllement, on chope le nombre de messages pour les afficher sur les ressources
					PF.get("message/nombre", {"id_utilisateur" : $scope.utilisateur_local.id_utilisateur, "langues_affichage" : [1,2,3]}, function(retour, status, headers, config){
					
						if (retour['status'] == "ok")
						{
							_scope.nombre_messages = retour['nombre_messages'];
							// var postLoad = {'messages' : messages, 'ressources' : ressources, 'langueApp': LanguageM.langueApp };
							
						}
					});
					_scope.loaded = true;
				}
			});
		}
	}

	$scope.sortRessources = function () {

		resParDomaine = {};
		resParEspace = {};
		angular.forEach($scope.ressources,function (ressource) {
			ressource.url_logo = PF.globals.url.root + ressource.url_logo;
			if(typeof(resParEspace[ressource.espace_nom_court]) == "undefined")
			{
				resParEspace[ressource.espace_nom_court] = [];
			}
			resParEspace[ressource.espace_nom_court].push(ressource);

			if(typeof(resParDomaine[ressource.theme]) == "undefined")
			{
				resParDomaine[ressource.theme] = [];
			}
			resParDomaine[ressource.theme].push(ressource);
			
		});

		$scope.switch_sort_mode = 0;
		$scope.res_sorted = [];
		$scope.res_sorted[0] = resParEspace;
		$scope.res_sorted[1] = resParDomaine;
	}
	
}]);