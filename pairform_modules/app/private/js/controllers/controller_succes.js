pf_app.controller('controller_succes', ["$scope", function ($scope) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"id_utilisateur" : null,
		"succes" : [],
	};

	angular.extend($scope, defaults);

	$scope.init = function () {
		//S'il y a déjà des succès, on laisse tomber
		if ($scope.succes && $scope.id_utilisateur)
			return;

		//Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
		if ($scope.loaded != $scope.id_utilisateur) {
			var _scope = $scope;
			PF.get('accomplissement/succes', {"id_utilisateur": $scope.id_utilisateur} , function(data, status, headers, config) {
				var retour = data;
				angular.extend(_scope, retour);

				_scope.loaded = _scope.id_utilisateur;
				
			});
		}
	}
}]);