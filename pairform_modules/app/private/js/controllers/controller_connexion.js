
pf_app.controller('controller_connexion', ["$scope", "$http", "$localStorage", "toaster", "$translate", function ($scope, $http, $localStorage, toaster, $translate) {
	var defaults = {
		"visible" : false,
		"mode" : "connexion",
		"tableau_langues_label" : PF.langue.tableau_labels,
		// "connexion" : {"username" : "Bigood", "password" : "azerty"},
		"mot_de_passe_oublie" : {},
		"url" : PF.globals.url,
		"enregistrement" : {
			"id_capsule" : PF.globals.capsule ? PF.globals.capsule.id_capsule : 0
		}
	};
	// $scope.$watch('visible', function (new_value, old_value) {
	// 	$scope.$parent.visible = new_value;
	// });

	angular.extend($scope, defaults);

	$scope.connecter = function (){
		
		var _scope = $scope;
		PF.post('utilisateur/login', this.connexion, function callback_success (data, status, headers, config) {
			var retour = data;
			if (retour['status'] == 'ok')
			{
				var user = new Utilisateur(retour);
				//On cale l'ID elgg et le nom dans le stockage local
				if($localStorage)
				{
					$localStorage.utilisateur_local = user;
				}
				
				
				//S'il existe une fonction dans le root scope qui défini la sessions globalement, sinon on cherche un main_controller
				$scope.setSession ? $scope.setSession() : PF.getScopeOfController("main_controller").setSession();
				//Peut être local ou overridé par le rootScope, en fonction du contexte
				_scope.verifierRole();
				
				//S'il existe une fonction dans le root scope qui permet de déclencher des timers lors de la connexion
				if($scope.setUpdateTimers)
					$scope.setUpdateTimers()
				else{
					//Sinon, on fait par défaut ceux pour un document
					PF.Notification.setUpdateTimer();
					Message.setUpdateTimer();
				}

				// updatePostOs();

				toaster.pop("success", "LABEL_CONNEXION_REUSSI");
				_scope.visible = false;

				if (!$localStorage.utilisateur_local.email_est_valide) {
					toaster.pop(
					  'warning',
					  'WS_MAIL_UTILISATEUR_PAS_VALIDE',
					  '<div class="btn btn-action" onclick="renvoyerConfirmation()">Renvoyer l\'e-mail de confirmation</div>',
					  0,
					  'trustedHtml'
					);
				}

				//Mise à jour des messages de la page
				//IMPORTANT : on efface le cache des messages seulement si on est dans une capsule
				if (PF.globals.capsule) 
					Message.recupererMessages(true);
				_paq.push(['trackEvent',  'Connexion - App web', 'Réussie']);
				// _paq.push(["setCustomDimension", 1, user.email])

				$scope.$emit("pf:utilisateur:login");
			}
			else
			{
				$scope.connexion.password = "";
				// $_pf('#popin-connexion').effect("shake");
				
				
				if(retour['message'] == "ws_utilisateur_invalide")
					toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");
				else
					toaster.pop("error", retour['message'].toUpperCase());

				_paq.push(['trackEvent', 'Connexion - App web', 'Échouée', $scope.connexion.username]);
			}

		});
	}
	$scope.connecterLinkedin = function (){
		$scope.window_sso = window.open(PF.globals.url.root + "webServices/utilisateur/linkedin", 
           "pf_linkedin", 
           "width=400,height=600,resizable,scrollbars,status");		
	}
	$scope.connecterShibboleth = function (){
		$scope.window_sso = window.open(PF.globals.url.root + "/webServices/utilisateur/shibboleth", 
           "pf_shibboleth",            "width=740,height=660,resizable,scrollbars,status");
	}
	$scope.listenMessageSSO = function () {
		//https://stackoverflow.com/a/25098153/1437016
		window.addEventListener('message', function(event) { 
		    // IMPORTANT: Check de l'origine
			console.log(event); 		    

		    if ($scope.window_sso
		    	&& ((event.origin+'/') == PF.globals.url.root)
		    	&& event.data && event.data._source == "pairform") { 
		        $scope.callbackSSO(event.data);
		    } else { 
		        //On stop net sinon
		        return; 
		    } 
		}); 
	}
	//Listen 
	$scope.listenMessageSSO();

	$scope.callbackSSO = function (utilisateur, err) {

		if (utilisateur && ! err)
		{
			var user = new Utilisateur(utilisateur);

			//On cale l'ID elgg et le nom dans le stockage local
			if($localStorage)
			{
				$localStorage.utilisateur_local = user;
			}
			
			
			//S'il existe une fonction dans le root scope qui défini la sessions globalement, sinon on cherche un main_controller
			$scope.setSession ? $scope.setSession() : PF.getScopeOfController("main_controller").setSession();
			//Peut être local ou overridé par le rootScope, en fonction du contexte
			$scope.verifierRole();
			
			//S'il existe une fonction dans le root scope qui permet de déclencher des timers lors de la connexion
			if($scope.setUpdateTimers)
				$scope.setUpdateTimers()
			else{
				//Sinon, on fait par défaut ceux pour un document
				PF.Notification.setUpdateTimer();
				Message.setUpdateTimer();
			}

			// updatePostOs();

			toaster.pop("success", "LABEL_CONNEXION_REUSSI");
			$scope.visible = false;

			//Mise à jour des messages de la page
			//IMPORTANT : on efface le cache des messages seulement si on est dans une capsule
			if (PF.globals.capsule) 
				Message.recupererMessages(true);

			_paq.push(['trackEvent',  'Connexion - Linkedin', 'Réussie']);
			// _paq.push(["setCustomDimension", 1, user.email])
			$scope.window_sso.close();
		}
		else
		{				
			toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");

			_paq.push(['trackEvent', 'Connexion - Linkedin', 'Échouée']);
		}
	}
	$scope.connecterFacebook = function (){
		var
		  _screenX   = typeof window.screenX      != 'undefined'
		    ? window.screenX
		    : window.screenLeft,
		  screenY    = typeof window.screenY      != 'undefined'
		    ? window.screenY
		    : window.screenTop,
		  outerWidth = typeof window.outerWidth   != 'undefined'
		    ? window.outerWidth
		    : document.documentElement.clientWidth,
		  outerHeight = typeof window.outerHeight != 'undefined'
		    ? window.outerHeight
		    : (document.documentElement.clientHeight - 22), 

		  
		  
		  width    = 475,
		  height   = 222,
		  screenX  = (_screenX < 0) ? window.screen.width + _screenX : _screenX,
		  left     = parseInt(screenX + ((outerWidth - width) / 2), 10),
		  top      = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
		  features = [];

		if (width !== null) {
		  features.push('width=' + width);
		}
		if (height !== null) {
		  features.push('height=' + height);
		}
		features.push('left=' + left);
		features.push('top=' + top);
		features.push('scrollbars=1');

		features = features.join(',');

		var windowObjectReference = window.open(
			// "https://www.facebook.com/dialog/oauth?client_id=730511317075431&redirect_uri=" + PF.globals.url.ws + "login/facebook&display=popup&response_type=token&scope=email",
			PF.globals.url.ws + "utilisateur/login/facebook",
			"FB_login", 
			features);

		window.addEventListener("message", function callback (event) {
			var retour = event.data;
			var user = new Utilisateur(retour);
			//On cale l'ID elgg et le nom dans le stockage local
			if($localStorage)
			{
				$localStorage.utilisateur_local = user;
			}
			
			
			//S'il existe une fonction dans le root scope qui défini la sessions globalement, sinon on cherche un main_controller
			$scope.setSession ? $scope.setSession() : PF.getScopeOfController("main_controller").setSession();
			//Peut être local ou overridé par le rootScope, en fonction du contexte
			$scope.verifierRole();
			
			
			//S'il existe une fonction dans le root scope qui permet de déclencher des timers lors de la connexion
			if($scope.setUpdateTimers)
				$scope.setUpdateTimers()
			else{
				//Sinon, on fait par défaut ceux pour un document
				PF.Notification.setUpdateTimer();
				Message.setUpdateTimer();
			}

			// updatePostOs();

			toaster.pop("success", "LABEL_CONNEXION_REUSSI");
			$scope.visible = false;
			//Mise à jour des messages de la page
			//IMPORTANT : on efface le cache des messages seulement si on est dans une capsule
			if (PF.globals.capsule) 
				Message.recupererMessages(true);
			_paq.push(['trackEvent',  'Connexion - Facebook', 'Réussie']);
			// _paq.push(["setCustomDimension", 1, user.email])

		}, false);
	}

	$scope.checkForLTIToken = function () {
		//Récuperation des parametres passés en GET dans l'URL depuis la passerelle LTI
		var url_params = getSearchParameters();
		//On récupère le token == elggperm
		var token = url_params['access_token'] || url_params['token'];
		//Si on est bien dans le cas d'une connexion LTI (et qu'on a le PF._localStorage)
		if (typeof(token) != "undefined"){

			$localStorage.premiere_visite = true;

			//Important : prévenir le WS qu'on est dans le cas de LTI, en changeant la valeur de version
			var post_params = {
				"access_token" : token
			}
			var _scope = $scope;
			//Et log de l'utilisateur.
			PF.post('utilisateur/login/lti', post_params, function callback_success (data, status, headers, config) {
				var retour = data;
				if (retour['status'] == 'ok')
				{
					var user = new Utilisateur(retour);
					//On cale l'ID elgg et le nom dans le stockage local
					if($localStorage)
					{
						$localStorage.utilisateur_local = user;
					}
					
					
					//S'il existe une fonction dans le root scope qui défini la sessions globalement, sinon on cherche un main_controller
					$scope.setSession ? $scope.setSession() : PF.getScopeOfController("main_controller").setSession();
					//Peut être local ou overridé par le rootScope, en fonction du contexte
					_scope.verifierRole();
					
					//S'il existe une fonction dans le root scope qui permet de déclencher des timers lors de la connexion
					if($scope.setUpdateTimers)
						$scope.setUpdateTimers()
					else{
						//Sinon, on fait par défaut ceux pour un document
						PF.Notification.setUpdateTimer();
						Message.setUpdateTimer();
					}

					// updatePostOs();

					toaster.pop("success", "LABEL_CONNEXION_REUSSI");
					//Mise à jour des messages de la page
					//IMPORTANT : on efface le cache des messages seulement si on est dans une capsule
					if (PF.globals.capsule) 
						Message.recupererMessages(true);
					_paq.push(['trackEvent', 'Connexion - LTI', 'Réussie']);
					// _paq.push(["setCustomDimension", 1, user.email])

				}
				else
				{
					toaster.pop("error", retour['message'].toUpperCase());
					_paq.push(['trackEvent', 'Connexion - LTI', 'Échouée']);
				}

			});
		}
	}
	
	function getSearchParameters () {
		var prmstr = window.location.search.substr(1);
		return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
	}

	function transformToAssocArray ( prmstr ) {
		var params = {};
		var prmarr = prmstr.split("&");
		for ( var i = 0; i < prmarr.length; i++) {
			var tmparr = prmarr[i].split("=");
			params[tmparr[0]] = tmparr[1];
		}
		return params;
	}
	
	$scope.renvoyerMotDePasse = function (){
		
		var _scope = $scope,
				_username = this.mot_de_passe_oublie;
		PF.post('utilisateur/reset', this.mot_de_passe_oublie, function callback_success (data, status, headers, config) {
			var retour = data;
			if (retour['status'] == 'ok') {
				_scope.mode = "enregistrement-1";
				_paq.push(['trackEvent', 'Connexion', 'Réinitialisation mot de passe', _username]);
			}
			else {
				toaster.pop("error", retour['message'].toUpperCase());
			}
		});
	}

	$scope.renvoyerConfirmation = function () {
		PF.post('utilisateur/confirm', {}, function callback_success (data) {
			var retour = data;
			if (retour['status'] == 'ok') {
				toaster.pop("success",'Un e-mail de validation vous a été envoyé.');
			}
			else {
				toaster.pop("error",retour['message']);
			}
		});
	}

	$scope.setMode = function (mode) {
		$scope.mode = mode;
		if (mode.match(/enregistrement/)) 
 			_paq.push(['trackEvent',  'Création de compte - App web', 'Affichée']);

	}
	$scope.enregistrer = function (){
		//Attention : register_cgu à la place de register-cgu
		var _scope = $scope,
				_email = this.enregistrement.email;

		PF.put('utilisateur/enregistrer', this.enregistrement, function callback_success (data, status, headers, config) {
			var retour = data;
			if (retour['status'] == 'ok') {
				_scope.mode = "enregistrement-1";
				_paq.push(['trackEvent',  'Création de compte - App web', 'Envoyée', _email]);

				var user = new Utilisateur(retour.utilisateur);
				//On cale l'ID elgg et le nom dans le stockage local
				if($localStorage)
				{
					$localStorage.utilisateur_local = user;
				}
				
				//S'il existe une fonction dans le root scope qui défini la sessions globalement, sinon on cherche un main_controller
				$scope.setSession ? $scope.setSession() : PF.getScopeOfController("main_controller").setSession();
				//Peut être local ou overridé par le rootScope, en fonction du contexte
				_scope.verifierRole();
				
				//S'il existe une fonction dans le root scope qui permet de déclencher des timers lors de la connexion
				if($scope.setUpdateTimers)
					$scope.setUpdateTimers()
				else{
					//Sinon, on fait par défaut ceux pour un document
					PF.Notification.setUpdateTimer();
					Message.setUpdateTimer();
				}

				// updatePostOs();

				toaster.pop("success", "LABEL_CONNEXION_REUSSI");


				//Mise à jour des messages de la page
				//IMPORTANT : on efface le cache des messages seulement si on est dans une capsule
				if (PF.globals.capsule) 
					Message.recupererMessages(true);
			}
			else {
				toaster.pop("error", retour['message'].toUpperCase());
			}
		});
	}
	$scope.$watch('enregistrement', function (new_value, old_value) {
		if (new_value != old_value && !old_value) {
			debugger
			var nom_champ;
			
 			_paq.push(['trackEvent',  'Création de compte - App web', 'Champ', nom_champ]);
		}
	}, true)

	$scope.$watch("visible", function (new_value, old_value) {
		if (old_value == false && new_value == true) {
				angular.element("html").addClass("pf-connexion-open");
		}
		if (old_value == true && new_value == false) {
				angular.element("html").removeClass("pf-connexion-open");
		}
	})
	//Vérification LTI
	$scope.checkForLTIToken();
}]);

window.renvoyerConfirmation = function (){
	var _scope = angular.element($("[ng-controller=controller_connexion]")).scope();
	_scope.$apply(function () {
		_scope.renvoyerConfirmation();
	})
}