document.addEventListener("DOMContentLoaded", function() {
  var interval = setInterval(function () {
    //Storyline 3
    if(window.hasOwnProperty("globalProvideData")) {
      clearInterval(interval);
      var original_globalProvideData = window.globalProvideData;
      
      window.globalProvideData = function overload (type, json) {
        var _patched = false
        original_globalProvideData(type, json);

        var data = JSON.parse(json);
        if("slide" === type) {
            $_pf("body").on("DOMSubtreeModified.pf", ".area-primary", function () {
              if (!_patched &&  $_pf(".area-primary .slide-transition-container").length == 1 && $_pf(".area-primary " + PF.globals.selector).length) {
                _patched = true;
                PF && PF.updateHTML && PF.updateHTML(".area-primary", data.id);
                $_pf("body").off("DOMSubtreeModified.pf", ".area-primary")
              }
            })
        }
      }
    }
  }, 200)
});
