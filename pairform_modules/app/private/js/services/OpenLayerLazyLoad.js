 //Service permettant de lazy load la carte openlayer
pf_app.service('OpenLayerLazyLoad', ['$window', '$q', function ( $window, $q ) {
	var deferred = $q.defer();
	var url = PF.globals.url.private + 'js/openlayers.min.js';

	/**
	* Fonction permettant d'ajouter un script js dans la page en lazy load
	*/
	function addScript( url, callback ) {
		var script = document.createElement( 'script' );
		if( callback ) script.onload = callback;
		script.type = 'text/javascript';
		script.src = url;
		script.setAttribute('async',true);
		document.getElementsByTagName("head")[0].appendChild(script);
	}

	addScript(url, function () {
		deferred.resolve();
	});
		
	return deferred.promise;
}]);