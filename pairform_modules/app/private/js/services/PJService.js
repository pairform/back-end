/*
* Service qui permet de télécharger les images des pièces jointes : preview et full
* 
*/
pf_app.service('PJService', ['$q', function($q){
	var routePJ = "recupererPieceJointe";
	var url = PF.globals.url.ws + "recupererPieceJointe";
	
	var service = {
		getThumbnail: function(image){
			// var deferred = $q.defer();
			var retour = url + "?nom_serveur_pj=" + image.nom_serveur_pj + "&&type=thumbnail";
			
			image.thumbnail_url = retour;

			// 	deferred.resolve(image);
			
			// return deferred.promise;
			return image;
		},

		getPJ: function(image){
			var deferred = $q.defer();
			var retour = url + "?nom_serveur_pj=" + image.nom_serveur_pj;
			
			deferred.resolve(retour);

			return deferred.promise;
		}
	}

	return service;
}]);
