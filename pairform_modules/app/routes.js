var web_services = require("../webservices/lib/webServices"),
		 express = require("express"),
		  CONFIG = require("config"),
		  	  fs = require('fs'),
	  	   async = require('async'),
	  	       _ = require("underscore"),
	  nodemailer = require('nodemailer'),
	  			log = require('metalogger')(),
	  constantes = require("../webservices/lib/constantes"),
	pairform_dao = require("../webservices/node_modules/pairform_dao/lib/pairformDAO");


app = exports = module.exports = express();

/*
 * Toutes les vues de la web App
 * Renvoie tous les .jade / html dans views (voir layer.jade)
*/
//View contenant tous les élements composants l'interface de l'app wzeb
app.get("/app/layer", function (req, res) {		
	res.render(__dirname + "/views/layer", {filename:"layer", cache:"true"});
});
/*

 * Point d'entrée de pairform web app
 * Renvoie import.js / import.min.js (permet d'alterner entre fichier minifié ou pas sans changer l'url pointé dans les ressources web)
*/

// Récupère les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)

app.get("/app", function (req, res) {
	//Mustache-like & angular-like interpolation, genre {{ variable }}
	_.templateSettings = { interpolate: /\{\{\=(.+?)\}\}/g, escape: /\{\{\-(.+?)\}\}/g, evaluate: /\{\{(.+?)\}\}/g };
	var path;
	//Si on est sur la prod
	if (CONFIG.app.urls.alias_serveur.match(/(pairform.fr)/))
		//On renvoie la version minifiée
		path = __dirname + "/private/js/import.min.js";
	//Sinon, renvoi de la version simple
	else
		path = __dirname + "/private/js/import.js";
	//Sinon, on renvoie le fichier pour récupération 
	//des informations en deux temps, avec le hash en métadonnées
	// res.sendFile("js/import.js", {root : __dirname + "/private/"});
	// res.sendFile(path, {root : __dirname + "/private/"});
	fs.readFile(path, 'utf-8', function (err, data) {
		if (err) {
			log.error(JSON.stringify(err, null, 2));
			return res.send("404");
		}
		var capsule = {};
	    var params = {
	    	url_root : CONFIG.app.urls.serveur_node + "/",
	    	capsule : JSON.stringify(capsule),
	    	types_mime : constantes.MESSAGE_PJ_TYPES_MIME,
	    	selecteurs : "",
	    	status  : "ok",
			message  :  ""
	    }

	    var compiled_file = _.template(data, params);

	    res.send(compiled_file);
	});
});

// Récupère les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)
app.get("/app/:hash_capsule", function (req, res) {
	//Mustache-like & angular-like interpolation, genre {{ variable }}
	_.templateSettings = { interpolate: /\{\{\=(.+?)\}\}/g, escape: /\{\{\-(.+?)\}\}/g, evaluate: /\{\{(.+?)\}\}/g };
	var path;
	//Si on est sur la prod
	if (CONFIG.app.urls.alias_serveur.match(/(pairform.fr)/))
		//On renvoie la version minifiée
		path = __dirname + "/private/js/import.min.js";
	//Sinon, renvoi de la version simple
	else
		path = __dirname + "/private/js/import.js";
	//S'il y a un hash en paramètre
	if (req.params.hash_capsule){

		log.debug(req.params);
		//On récupère les infos de la capsule
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.capsule_dao.checkCapsule(
				req.params.hash_capsule,
				function callback_success (capsule) {
					pairform_dao.libererConnexionBDD(_connexion_courante);
					//On check l'URL
					// log.debug("Host : " + req.get('host') + " ; \n " +
					// 			"origin : " + req.get('origin') + " ; \n " +
					// 			"referer : " + req.get('referer') + " ; \n " + 
					// 			"Url web : " + capsule.url_web);
					// log.debug("Match : " + checkUrlMatchFromRequest(req, capsule.url_web));

					fs.readFile(path, 'utf-8', function (err, data) {
						if (err) {
							log.error(JSON.stringify(err, null, 2));
							return res.send("404");
						}
						var params = {
	    					types_mime : constantes.MESSAGE_PJ_TYPES_MIME,
					    	url_root : CONFIG.app.urls.serveur_node + "/"
					    }

					    //Si on a la bonne URL, on passe la capsule
					    if(checkUrlMatchFromRequest(req, capsule.url_web)){
					    	params.capsule = JSON.stringify(capsule);
					    	params.selecteurs = capsule.selecteurs || "";
							params.status = "ok";
							params.message = "";
					    }
					    //Sinon,
						//TODO: passer un message pour prévenir :
						// Le document semble avoir changé d'URL.
						// Soit il faut mettre l'URL à jour dans le back office
						// Soit il faut remettre le document là où il était
						else{
							params.capsule = JSON.stringify({});
					    	params.selecteurs = "";
							params.message = "";
							params.status = "ko";
						}
					    var compiled_file = _.template(data, params);

					    //Ajout des addons spécifiques aux formats de documents

						    //Format StoryLine 2
						    switch(capsule.id_format){
							    case constantes.FORMAT_STORYLINE2 : {
										var storyline2 = fs.readFileSync(__dirname + constantes.ADDONS_FORMATS.STORYLINE2, 'utf-8');
										compiled_file += ";" + storyline2;
										break;
									}
							    case constantes.FORMAT_STORYLINE3 : {
										var storyline3 = fs.readFileSync(__dirname + constantes.ADDONS_FORMATS.STORYLINE3, 'utf-8');
										compiled_file += ";" + storyline3;
										break;
									}
					    }

					    res.send(compiled_file);
					});
				},
				function callback_error (err) {
					log.error(err);
					fs.readFile(path, 'utf-8', function (err, data) {
						if (err) {
							log.error(JSON.stringify(err, null, 2));
							return res.send("404");
						}
					    var params = {
					    	url_root : CONFIG.app.urls.serveur_node + "/",
					    	capsule : JSON.stringify({}),
	    					types_mime : constantes.MESSAGE_PJ_TYPES_MIME,
					    	selecteurs : "",
					    	status : "ko",
					    	message : "hash_not_found"
					    }

					    var compiled_file = _.template(data, params);

					    res.send(compiled_file);
					});
				}
			);
		});
		
	}
	else{
		res.send("404");
	}
	
});

function checkUrlMatchFromRequest (req, url_to_match) {
	//Parfois, l'url est undefined : à checker rapidement
	url_to_match = url_to_match || '';
	var dns = req.get('origin') || null;
	var referer = req.get('referer') ? req.get('referer').replace(/https?:\/\//,"") : "";
	var url = url_to_match.replace(/https?:\/\//,"");

	//Test si l'URL déclaré en BDD est la même que le referer
	if(referer == url)
		return true;
	//Test si les noms de domaines correspondent
	if(dns && !url.match(new RegExp(dns, "i")))
		return false;
	//Test si l'URL déclaré en BDD se retrouve dans le referer
	if(!referer.match(new RegExp(url, "i")))
		return false;

	//Sinon, tutto bene
	return true;

}
app.get("/derniers_messages", function (req, res) {		
	res.render(__dirname + "/../authentification/views/email_templates/derniers_messages",
		{
			utilisateur : {
				pseudo : "Bigood"
			}

		});
});

