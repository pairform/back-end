"use strict";
	var CONFIG = require("config"),
  pairform_dao = require("pairform_dao"),
		   log = require("metalogger")(),
		  cors = require("cors"),
		   url = require("url"),
	constantes = require("./lib/constantes");


module.exports = function(app, passport) {

	// configuration des nom de domaine autorisés à effectuer du cross-domaine avec le serveur Node.js de PairForm
	// Ils sont récupérés automatiquement depuis la base de donnée.
	// Resolution probleme CORS : wildcard par defaut pour tout, sauf pour les webServices.
	// En cas de tentative d'utilisation des webservices sur un site
	// dont le domaine n'est pas enregistre sur les serveurs PF
	// (condition vraie quand le domaine heberge au moins une capsule),
	// on affiche une erreur expliquant a l'utilisateur (internationalise).
	var pf_cors_options = {
		origin: function(origin, callback) {

			if(origin){
				//If not on production
				//Backdoor to allow use of Ionic from localhost and device
				if(!CONFIG.app.urls.serveur_node.match(/pairform/))
					return callback(null, true)

				log.debug(origin);
				pairform_dao.getConnexionBDD(function (_connexion_courante) {
					pairform_dao.capsule_dao.selectCapsuleByURL(
						origin,
						function callbackSuccess (retour_sql) {
							pairform_dao.libererConnexionBDD(_connexion_courante);

							// log.debug(retour_sql);
							var origin_est_enregistree_sur_PF = retour_sql.length !== 0;
							callback(null, origin_est_enregistree_sur_PF);
						},
						callback
					);
				});
			}
			else{
				//On n'autorise pas s'il n'y a pas d'origine
				callback(null, false);
			}
		},
		methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
		credentials: true
	};

	//Controle préliminaire des CORS
	// app.options("/webServices/*", function hey (req, res, next) {
	// 	log.debug("OPTIONS appellé");
	// 	next();
	// }, cors(pf_cors_options));
	// Test de compatibilité du client avec la version du serveur
	app.all("/webServices/*", function corsInjecteur (req, res, next) {
		var requetes_autorises_injecteur = {
			"/webServices/utilisateur/logout" : "post",
			"/webServices/utilisateur/login" : "post",
			"/webServices/utilisateur/reset" : "post",
			"/webServices/injecteur/capsule" : "put",
			"/webServices/utilisateur/enregistrer" : "put",
			"/webServices/infosTableauDeBord" : "get"
		}
		// log.debug(url.parse(req.url).pathname);
		//Si la requête est une des requetes autorisées pour l'injecteur
		//Marche pour le preflight (OPTIONS) ou la méthode autorisée
		if (requetes_autorises_injecteur[url.parse(req.url).pathname] == req.method.toLowerCase()
			|| (req.method.toLowerCase() == "options" && typeof requetes_autorises_injecteur[url.parse(req.url).pathname] != "undefined"))
			//On bypass les règles CORS
			return cors({origin: true, credentials: true})(req, res, next);
		//Sinon
		else
			//On applique les règles CORS normales
			return cors(pf_cors_options)(req, res, next);

	}, function middlewareCompatibilite (req, res, next) {
		// Récupération des paramètres, et de la compatibilité dans le fichier config
		var os_client = req.param("os"),
			version_client = req.param("version"),
			condition = os_client ? CONFIG.app.compatibilite[os_client] : null;

			// log.debug("OS : " + os_client)
			// log.debug("Version : " + version_client)

		// S'il y a bien un champ compatibilité pour l'os donné dans la config
		if (condition) {
			// On extrait l'opérateur et la version
			var index = condition.indexOf(condition.match(/[0-9]/)),
				operateur = condition.substr(0, index),
				version_compatible = condition.substr(index);

			// Pour éviter un éval, on utilise une fonction répertoriant les opérations
			var comparerVersions = {
			    ">" :  function(v1, v2) { return v1 > v2 },
			    ">=" :  function(v1, v2) { return v1 >= v2 },
			    "<" :  function(v1, v2) { return v1 < v2 },
			    "<=" :  function(v1, v2) { return v1 <= v2 },
			    "" :  function(v1, v2) { return v1 === v2 }
			};

			var result = comparerVersions[operateur](version_client, version_compatible);

			// log.debug("Client " + os + " / " + version + (result ? " autorisé" : " refusé"));

			// Si la version est compatible (possibilité de wildcard)
			if (result || version_compatible === "*"){
				next();
			}
			// Sinon, on indique au client que sa version est périmée
			else{
				res.json(constantes.RETOUR_JSON_VERSION_OBSOLETE);
				// var url_redirection = 'https://www.pairform.fr/node_v1_0' + req.originalUrl;
				// res.redirect(307, url_redirection);
			}

		}
		//Sinon on bloque toute requête
		else {
			next();
			// res.json(constantes.RETOUR_JSON_KO);
		}
	});
	require("./routes/routesAuthentification")(app, passport);
	require("./routes/routesBackoffice")(app, passport);
	require("./routes/routesApplications")(app, passport);
	require("./routes/routesInjecteur")(app, passport);
	require("./routes/routesMigrations")(app, passport);
}
