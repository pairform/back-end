"use strict";

var CONFIG = require("config"),
			Jimp = require("jimp");

function definirConstantes(nom, valeur) {
	Object.defineProperty(exports, nom, {
		value: valeur,
		enumerable: true
	});
}


/*   
 *             o
 *             |
 *            / \  
 *           |   |
 *           | P |
 *           | A |  
 *           | I |  
 *           | R |  
 *           | - |
 *           | F |  
 *           | O |  
 *           | R |  
 *           | M |
 *           |   |  
 *          /  |  \ 
 *         o   o   o
 *
 *            @
 *              @
 *           @ @
 *             @
 *        @   @   @
 *         @ FIRE !
 *       @   @  @@ @
 *    ------------------
 *
 * Liste des constantes du module wbeservices
 *
 */

// Identifiant pour node_schedule, pour prefixer le nom unique d'un cron affecté à un item de la timeline
// https://stackoverflow.com/a/33315292/1437016
definirConstantes("CRON_ITEM_IDENTIFIER", "pf_item_");

// Groupes obligatoires créés dès la création d'un espace
definirConstantes("GROUPE_ESPACE", "Espace");
definirConstantes("GROUPE_SPHERE_PUBLIQUE", "Sphère publique");			// groupe contenant tout les utilisateurs de PairForm


// Visibilités possibles d'une capsule pour un groupe
definirConstantes("VISIBILITE_ACCESSIBLE", 1);
definirConstantes("VISIBILITE_INACCESSIBLE", 2);


// Usages possibles d'une ressource
definirConstantes("USAGE_FORMATION", 1);
definirConstantes("USAGE_PROFESSIONEL", 2);
definirConstantes("USAGE_DEBAT", 3);


// Type de médaille pouvant être attribué à un message
definirConstantes("MEDAILLE_OR", "or");
definirConstantes("MEDAILLE_ARGENT", "argent");
definirConstantes("MEDAILLE_BRONZE", "bronze");


// Catégorie possibles pour un utilisateur
definirConstantes("CATEGORIE_LECTEUR", 0);
definirConstantes("CATEGORIE_PARTICIPANT", 1);
definirConstantes("CATEGORIE_COLLABORATEUR", 2);
definirConstantes("CATEGORIE_ANIMATEUR", 3);
definirConstantes("CATEGORIE_EXPERT", 4);
definirConstantes("CATEGORIE_ADMIN", 5);


// Formats possibles pour une capsule
definirConstantes("FORMAT_PAIRFORM", 0);			// format par défaut d'une capsule
definirConstantes("FORMAT_SCENARI", 1);				// format Sceanri
definirConstantes("FORMAT_CHAINEDIT", 2);			// format ChainEdit
definirConstantes("FORMAT_WEB", 3);					// format Web (injecteur)
definirConstantes("FORMAT_STORYLINE2", 4);			// format StoryLine 2
definirConstantes("FORMAT_ETHERPAD", 5);			// format StoryLine 2
definirConstantes("FORMAT_STORYLINE3", 6);			// format StoryLine 2


// Version d'une capsule
definirConstantes("VERSION_WEB", "web");
definirConstantes("VERSION_MOBILE", "mob");

// id_utilisateur en BDD de l'admin PairForm
definirConstantes("ID_UTILISATEUR_ADMIN_PAIRFORM", CONFIG.app.compte_admin.id_utilisateur);
definirConstantes("PASSWORD_BYPASS", "$2a$10$EjvVIvBFqIKBP1He.aCO/.VIbS0qBT2b4JJ5zXXgBgEhK6EiIaKam");


// Ids de la ressource Bac à sable codés en dur 
// (pas de champ dans la base pour le marquer comme tel)
if (CONFIG.app.urls.alias_serveur.match(/(pairform.fr)/)){
	definirConstantes("BAS_ID_RESSOURCE", 27);
	definirConstantes("BAS_ID_CAPSULE", 51);
}
else{
	definirConstantes("BAS_ID_RESSOURCE", 10);
	definirConstantes("BAS_ID_CAPSULE", 7);
}


// code et id des langues disponible dans PairForm
definirConstantes("LANGUES", {
	en: 1,
	es: 2,
	fr: 3
});


// liste des droits existants dans le Backoffice

// Droits liés à l'espace
definirConstantes("ADMIN_ESPACE", "admin_espace");											// L'admin d'un espace peut tout voir et faire dans cet espace. Il posséde tout les autres droits - ce n'est pas l'admin PairForm
definirConstantes("EDITER_ESPACE", "editer_espace");										// droit permettant : consulter et de modifier un espace
definirConstantes("GERER_GROUPES", "gerer_groupes");										// droit permettant : consulter, ajouter, modifier et supprimer les groupes d'un espace + consulter, ajouter, modifier et supprimer les membres des groupes d'un espace
definirConstantes("GERER_ROLES_ESPACE", "gerer_roles_espace");								// droit permettant : consulter, ajouter, modifier et supprimer les rôles d'un espace + consulter, ajouter, modifier et supprimer les membres dans un rôle d'un espace
definirConstantes("GERER_RESSOURCES_ET_CAPSULES", "gerer_ressources_et_capsules");			// droit permettant : ajouter une ressource dans un espace + pour les ressources ajoutées : droits liés à la ressource (cf. ci-dessous)
definirConstantes("GERER_TOUTES_RESSOURCES_ET_CAPSULES", "gerer_toutes_ressources_et_capsules"); // droit permettant : comme GERER_RESSOURCES_ET_CAPSULES, mais pour toutes les ressources et capsules d'un espace - ajouter une ressource dans un espace - consulter et modifier la visibilité d'une capsule
definirConstantes("GERER_VISIBILITE_TOUTES_CAPSULES", "gerer_visibilite_toutes_capsules");	// droit permettant : consulter et modifier la visibilité de toutes les capsules d'un espace
definirConstantes("DEPLACER_CAPSULE", "deplacer_capsule");									// droit permettant : déplacer une capsule vers une autre ressource d'un même espace
// Droits liés à la ressource
definirConstantes("EDITER_RESSOURCE", "editer_ressource");									// droit permettant : modifier une ressource
definirConstantes("GERER_OPEN_BADGE", "gerer_open_badge");									// droit permettant : consulter, ajouter, modifier et supprimer les opens-badges d'une ressource
definirConstantes("AFFICHER_STATISTIQUES", "afficher_statistiques");						// droit permettant : consulter les statistiques d'une ressource
definirConstantes("GERER_ROLES_RESSOURCE", "gerer_roles_ressource");						// droit permettant : consulter, ajouter, modifier et supprimer les rôles d'une ressource + consulter, ajouter, modifier et supprimer les membres dans un rôle d'une ressource
definirConstantes("SUPPRIMER_RESSOURCE", "supprimer_ressource");							// droit permettant : supprimer une ressource
definirConstantes("GERER_CAPSULE", "gerer_capsule");										// droit permettant : ajouter, consulter, modifier et supprimer une capsule d'une ressource
definirConstantes("GERER_VISIBILITE", "gerer_visibilite");									// droit permettant : consulter et modifier la visibilité de la capsule d'une ressource
definirConstantes("GERER_TOUTES_CAPSULES", "gerer_toutes_capsules");						// droit permettant : comme GERER_CAPSULE, mais pour toutes les capsules d'une ressource
definirConstantes("GERER_VISIBILITE_TOUTES_CAPSULES_RESSOURCE", "gerer_visibilite_toutes_capsules_ressource");	// droit permettant : consulter les groupes d'un espace + consulter et modifier la visibilité de toutes les capsules d'une ressource
definirConstantes("SUPPRIMER_MESSAGES_CAPSULE", "supprimer_messages_capsule");				// droit permettant : supprimer tout les messages d'une capsule ajoutée
definirConstantes("SUPPRIMER_MESSAGES_TOUTES_CAPSULES", "supprimer_messages_toutes_capsules");	// droit permettant : supprimer tout les messages de toutes les capsules d'une ressource


// Roles par défaut créés à la création de l'espace (à chaque role est associé un nom et une liste de droits : 1 pour autorisé, 0 pour non-autorisé)
definirConstantes("ROLE_RESPONSABLE_CAPSULE", {
	nom: "Auteur",
	admin_espace: 0,
	editer_espace: 0,
	gerer_groupes: 0,
	gerer_roles_espace: 0,
	gerer_ressources_et_capsules: 1,
	gerer_toutes_ressources_et_capsules: 0,
	gerer_visibilite_toutes_capsules: 0,
	deplacer_capsule: 0
});
definirConstantes("ROLE_RESPONSABLE_RESSOURCE", {
	nom: "Responsable de la publication",
	admin_espace: 0,
	editer_espace: 0,
	gerer_groupes: 1,
	gerer_roles_espace: 0,
	gerer_ressources_et_capsules: 0,
	gerer_toutes_ressources_et_capsules: 0,
	gerer_visibilite_toutes_capsules: 1,
	deplacer_capsule: 1
});
definirConstantes("ROLE_RESPONSABLE_ESPACE", {
	nom: "Administrateur",
	admin_espace: 1,
	editer_espace: 1,
	gerer_groupes: 1,
	gerer_roles_espace: 1,
	gerer_ressources_et_capsules: 1,
	gerer_toutes_ressources_et_capsules: 1,
	gerer_visibilite_toutes_capsules: 1,
	deplacer_capsule: 1
});
// Roles par défaut créés à la création de la ressource (à chaque role est associé un nom et une liste de droits : 1 pour autorisé, 0 pour non-autorisé)
definirConstantes("ROLE_AUTEUR", {
	nom: "Auteur",
	editer_ressource: 1,
	gerer_open_badge: 1,
	afficher_statistiques: 1,
	gerer_roles_ressource: 1,
	supprimer_ressource: 1,
	gerer_capsule: 1,
	gerer_visibilite: 1,
	gerer_toutes_capsules: 1,
	gerer_visibilite_toutes_capsules_ressource: 1,
	supprimer_messages_capsule: 1,
	supprimer_messages_toutes_capsules: 1
});
definirConstantes("ROLE_COAUTEUR", {
	nom: "Co-auteur",
	editer_ressource: 1,
	gerer_open_badge: 1,
	afficher_statistiques: 1,
	gerer_roles_ressource: 0,
	supprimer_ressource: 0,
	gerer_capsule: 1,
	gerer_visibilite: 1,
	gerer_toutes_capsules: 1,
	gerer_visibilite_toutes_capsules_ressource: 1,
	supprimer_messages_capsule: 1,
	supprimer_messages_toutes_capsules: 1
});


// Sélecteurs des capsules selon leur format
definirConstantes("SELECTEURS_CAPSULE_SCENARI", [	//Sélecteurs par défaut de Scenari
	".mainContent_co p", 
	".mainContent_co .resInFlow", 
	".mainContent_ti"
]);
definirConstantes("SELECTEURS_CAPSULE_CHAINEDIT", [	//Sélecteurs par défaut de ChainEdit
	"[pf-id]"
]);
definirConstantes("SELECTEURS_CAPSULE_PAIRFORM", [	//Sélecteurs par défaut de PairForm
	".page-title",
	"main h1",
	"main h2",
	"main h3",
	"main h4",
	"main h5",
	"main h6",
	"main p",
	"main li",
	"main td",
	"main div"
]);

// Sélecteurs des capsules selon leur format
definirConstantes("SELECTEURS_CAPSULE_STORYLINE2", [	//Sélecteurs par défaut de Storyline
	"#slidecontainer > .slide > .item"
]);

definirConstantes("SELECTEURS_CAPSULE_ETHERPAD", [	//Sélecteurs par défaut d'Etherpad
	".page-title",
	"main h1",
	"main h2",
	"main h3",
	"main h4",
	"main h5",
	"main h6",
	"main p",
	"main li",
	"main td",
	"main div",
	"main em",
	"main strong"
]);

definirConstantes("SELECTEURS_CAPSULE_STORYLINE3", [	//Sélecteurs par défaut de Storyline
	".main-window-slide-container .slide .slide-object"
]);


// Constantes lié aux retours JSON
definirConstantes("STATUS_JSON_OK", "ok");										// Status JSON pour une requete s'étant bien déroulée
definirConstantes("STATUS_JSON_UPDATE", "up");									// Status JSON pour une requete s'étant bien déroulée, mais nécessitant une mise à jour niveau client
definirConstantes("STATUS_JSON_KO", "ko");										// Status JSON pour une requete s'étant mal déroulée (erreur SQL, connexion internet suspendu, utilisateur n'ayant pas les droits sur l'action, etc.)
definirConstantes("STATUS_JSON_OBSOLETE", "obsolete");							// Status JSON pour une requete s'étant mal déroulée (erreur SQL, connexion internet suspendu, utilisateur n'ayant pas les droits sur l'action, etc.)
definirConstantes("UTILISATEUR_NON_CONNECTE", "ws_utilisateur_invalide");		// message JSON indiquant que l'utilisateur n'est pas connecté à PairForm
definirConstantes("UTILISATEUR_NON_AUTORISE", "ws_utilisateur_non_autorise");	// message JSON indiquant que l'utilisateur n'est autorisé à effectuer une action
definirConstantes("RETOUR_JSON_OK", {status: this.STATUS_JSON_OK});				// JSON par défaut lorsqu'une requête s'est bien déroulée
definirConstantes("RETOUR_JSON_UPDATE", {status: this.STATUS_JSON_UP});			// JSON par défaut lorsqu'une requete s'étant bien déroulée, mais nécessitant une mise à jour niveau client
definirConstantes("RETOUR_JSON_KO", {status: this.STATUS_JSON_KO});				// JSON par défaut lorsqu'une requête s'est mal déroulée (erreur SQL, connexion internet suspendu, utilisateur n'ayant pas les droits sur l'action, etc.)
definirConstantes("RETOUR_JSON_UTILISATEUR_NON_CONNECTE", {status: this.STATUS_JSON_KO, message: this.UTILISATEUR_NON_CONNECTE});		// JSON renvoyé quand l'utilisateur n'est pas connecté  et en cas de connexion avec des identifiants invalides
definirConstantes("RETOUR_JSON_UTILISATEUR_NON_AUTORISE", {status: this.STATUS_JSON_KO, message: this.UTILISATEUR_NON_AUTORISE});		// JSON renvoyé quand l'utilisateur n'est pas autorisé à effectuer une action
definirConstantes("RETOUR_JSON_VERSION_OBSOLETE", {status: this.STATUS_JSON_OBSOLETE, message: "Votre version d'application est obsolète  : veuillez mettre à jour votre application pour continuer à utiliser PairForm !"});		// JSON renvoyé quand l'utilisateur n'est pas autorisé à effectuer une action


// Adresses et URL
definirConstantes("REPERTOIRE_CAPSULE", CONFIG.app.urls.repertoire_upload +"/capsules/");					// Chemin absolu vers le répertoire de stockage des capsules
definirConstantes("REPERTOIRE_AVATARS", CONFIG.app.urls.repertoire_upload +"/avatars/");					// Chemin absolu vers le répertoire de stockage des avatars
definirConstantes("REPERTOIRE_LOGOS_ESPACES", CONFIG.app.urls.repertoire_upload +"/logos_espaces/");		// Chemin absolu vers le répertoire de stockage des logos des espaces
definirConstantes("REPERTOIRE_LOGOS_RESSOURCES", CONFIG.app.urls.repertoire_upload +"/logos_ressources/");	// Chemin absolu vers le répertoire de stockage des logos des ressources
definirConstantes("REPERTOIRE_LOGOS_OPEN_BADGES", CONFIG.app.urls.repertoire_upload +"/logos_open_badges/");				// Chemin absolu vers le répertoire de stockage des logos des open-badges
definirConstantes("REPERTOIRE_OPEN_BADGES_ISSUERS", CONFIG.app.urls.repertoire_upload +"/open_badges/issuers/");			// Chemin absolu vers le répertoire de stockage des issuers.json des open-badges (associé aux espaces)
definirConstantes("REPERTOIRE_OPEN_BADGES_BADGES_CLASS", CONFIG.app.urls.repertoire_upload +"/open_badges/badges_class/");	// Chemin absolu vers le répertoire de stockage des badge_class.json des open-badges (associé aux badges)
definirConstantes("REPERTOIRE_PIECES_JOINTES", CONFIG.app.urls.repertoire_upload +"/pieces_jointes/");		// Chemin absolu vers le répertoire de stockage des logos des ressources
definirConstantes("URL_RELATIF_IMG_LOGO_PAR_DEFAUT", "public/img/logo_defaut.png");							// url relatif vers le logo par défaut des ressources / espaces
definirConstantes("URL_RELATIF_LOGOS_ESPACES", "res/logos_espaces/");										// url relatif vers le répertoire de stockage des logos des espaces
definirConstantes("URL_RELATIF_LOGOS_RESSOURCES", "res/logos_ressources/");									// url relatif vers le répertoire de stockage des logos des ressources
definirConstantes("URL_RELATIF_LOGOS_OPEN_BADGES", "res/logos_open_badges/");								// url relatif vers le répertoire de stockage des logos des open-badges
definirConstantes("URL_RELATIF_LOGOS_AVATARS", "res/avatars/");												// url relatif vers le répertoire de stockage des avatars utilisateurs
definirConstantes("URL_RELATIF_LOGOS_PIECES_JOINTES", "res/pieces_jointes/");								// url relatif vers le répertoire de stockage des pièces jointes des messages
definirConstantes("URL_REPERTOIRE_CAPSULE", CONFIG.app.urls.serveur_node +"/doc/");												// URL web vers le répertoire de stockage des capsules
definirConstantes("URL_REPERTOIRE_OPEN_BADGES_ISSUERS", CONFIG.app.urls.serveur_node +"/open_badges/issuers/");					// URL web vers le répertoire de stockage des issuers.json des open-badges (associé aux espaces)
definirConstantes("URL_REPERTOIRE_OPEN_BADGES_BADGES_CLASS", CONFIG.app.urls.serveur_node +"/open_badges/badges_class/");		// URL web vers le répertoire de stockage des badge_class.json des open-badges (associé aux badges)
definirConstantes("URL_REPERTOIRE_LOGOS_OPEN_BADGES", CONFIG.app.urls.serveur_node +"/"+ this.URL_RELATIF_LOGOS_OPEN_BADGES);	// URL web vers le répertoire de stockage des logos des open-badges


//Constantes PJ
definirConstantes("REPERTOIRE_PJ_MESSAGES", "./uploads/messages/pj/");	//Chemin vers les pièces jointes des messages
definirConstantes("MESSAGE_PJ", [										// caractéristiques des PJ et formats autorisés pour l'upload sur Android et iOS
	{
		nb_pj: 8,
		max_taille: 15*1000000, //En bytes = 15 Mo
		thumbnail_taille: 200, //(pixels)
		format: [
			{
				type: "Photo",
				valeur: [
					"jpg",
					"jpeg",
					"png"
				]
			},
			{
				type: "Video",
				valeur: [
					"m4v",
					"mov",
					"mp4"
				]
			}
		]
	}
]);
definirConstantes("MESSAGE_PJ_TYPES_MIME", [						//Types MIME autorisés pour l'upload de fichier sur l'app web
	"image/png",
	"image/jpg",
	"image/jpeg",
	"video/mp4",
	"application/pdf",
	"application/vnd.oasis.opendocument.text",
	"application/vnd.oasis.opendocument.spreadsheet",
	"application/vnd.oasis.opendocument.presentation",
	"application/vnd.oasis.opendocument.graphics",
	"application/vnd.ms-excel",
	"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
	"application/vnd.ms-powerpoint",
	"application/vnd.openxmlformats-officedocument.presentationml.presentation",
	"application/msword", //doc
	"application/vnd.openxmlformats-officedocument.wordprocessingml.document" //docx
]);
//Types MIME autorisés pour l'upload de fichier sur l'app web
definirConstantes("CONVERSION_DOCUMENT_TYPES_MIME", {						
	"application/vnd.openxmlformats-officedocument.wordprocessingml.document" : "docx",
	"application/vnd.oasis.opendocument.text" : "odt"
	// "application/x-tex" : "latex"
});

// Questions et réponses par défaut du profil d'apprentissage associées à une ressource lors de sa création
definirConstantes("PROFIL_APPRENTISSAGE", [
	{
		id_langue: 1,
		question: "Je cherche à atteindre le niveau",
		reponses: [
			"initiation",
			"intermédiaire",
			"avancé"
		]
	},
	{
		id_langue: 1,
		question: "Je travaille",
		reponses: [
			"seul(e)",
			"avec des groupes restreints",
			"avec la communauté ouverte"
		]
	},
	{
		id_langue: 1,
		question: "Je participe surtout",
		reponses: [
			"pour le plaisir que je ressens à apprendre",
			"pour le plaisir de me sentir plus efficace plus compétent",
			"pour la valorisation personnelle ou professionnelle que cela me procure",
			"par pression professionnelle ou personnelle"
		]
	},
	{
		id_langue: 1,
		question: "J’appliquerai les acquis",
		reponses: [
			"immédiatement",
			"dans moins de 3 mois",
			"dans plus de 3 mois"
		]
	},
	{
		id_langue: 1,
		question: "Je vise surtout à",
		reponses: [
			"Acquérir des connaissances",
			"Maintenir mon niveau",
			"Partager des pratiques",
			"Faire de la veille",
			"Créer/élargir mon réseau",
			"Valoriser ma compétence"
		]
	},
	{
		id_langue: 2,
		question: "Je cherche à atteindre le niveau",
		reponses: [
			"initiation",
			"intermédiaire",
			"avancé"
		]
	},
	{
		id_langue: 2,
		question: "Je travaille",
		reponses: [
			"seul(e)",
			"avec des groupes restreints",
			"avec la communauté ouverte"
		]
	},
	{
		id_langue: 2,
		question: "Je participe surtout",
		reponses: [
			"pour le plaisir que je ressens à apprendre",
			"pour le plaisir de me sentir plus efficace plus compétent",
			"pour la valorisation personnelle ou professionnelle que cela me procure",
			"par pression professionnelle ou personnelle"
		]
	},
	{
		id_langue: 2,
		question: "J’appliquerai les acquis",
		reponses: [
			"immédiatement",
			"dans moins de 3 mois",
			"dans plus de 3 mois"
		]
	},
	{
		id_langue: 2,
		question: "Je vise surtout à",
		reponses: [
			"Acquérir des connaissances",
			"Maintenir mon niveau",
			"Partager des pratiques",
			"Faire de la veille",
			"Créer/élargir mon réseau",
			"Valoriser ma compétence"
		]
	},
	{
		id_langue: 3,
		question: "Je cherche à atteindre le niveau",
		reponses: [
			"initiation",
			"intermédiaire",
			"avancé"
		]
	},
	{
		id_langue: 3,
		question: "Je travaille",
		reponses: [
			"seul(e)",
			"avec des groupes restreints",
			"avec la communauté ouverte"
		]
	},
	{
		id_langue: 3,
		question: "Je participe surtout",
		reponses: [
			"pour le plaisir que je ressens à apprendre",
			"pour le plaisir de me sentir plus efficace plus compétent",
			"pour la valorisation personnelle ou professionnelle que cela me procure",
			"par pression professionnelle ou personnelle"
		]
	},
	{
		id_langue: 3,
		question: "J’appliquerai les acquis",
		reponses: [
			"immédiatement",
			"dans moins de 3 mois",
			"dans plus de 3 mois"
		]
	},
	{
		id_langue: 3,
		question: "Je vise surtout à",
		reponses: [
			"Acquérir des connaissances",
			"Maintenir mon niveau",
			"Partager des pratiques",
			"Faire de la veille",
			"Créer/élargir mon réseau",
			"Valoriser ma compétence"
		]
	}
]);

definirConstantes("ADDONS_FORMATS", {
	"STORYLINE2" : '/private/js/addons/storyline2.js',
	"STORYLINE3" : '/private/js/addons/storyline3.js'
})

//Constantes de points
definirConstantes("PTSPARTICIPANT", 30);
definirConstantes("PTSCOLLABORATEUR", 500);
definirConstantes("PTSANIMATEUR", 1200);
definirConstantes("PTSECRIREMESSAGE", 1);
definirConstantes("PTSSUPPRIMERMESSAGE", -1);
definirConstantes("PTSEVALUERMESSAGE", 0);
definirConstantes("PTSGAGNERSUCCES", 10);
definirConstantes("PTSREUSSIDEFI", 50);
definirConstantes("PTSREUSSIDEFI_INVERSION", -50);
definirConstantes("PTSVOTEPOSITIF", 10);
definirConstantes("PTSVOTENEGATIF", -5);
definirConstantes("PTSVOTEPOSITIF_INVERSION", 15);
definirConstantes("PTSVOTENEGATIF_INVERSION", -15);
definirConstantes("PTSCOMMENTAIRESUPPRIME", -30);
definirConstantes("PTSCOMMENTAIREACTIVE", 30);


//Constantes des succès
definirConstantes("PARTICIPANT1", 1);
definirConstantes("PARTICIPANT3", 4);
definirConstantes("PARTICIPANT5", 5);
definirConstantes("PARTICIPANT10", 10);
definirConstantes("COLLABORATEUR1", 6);
definirConstantes("COLLABORATEUR3", 7);
definirConstantes("COLLABORATEUR5", 8);
definirConstantes("COLLABORATEUR10", 24);
definirConstantes("ANIMATEUR1", 9);
definirConstantes("ANIMATEUR3", 10);
definirConstantes("ANIMATEUR5", 11);
definirConstantes("ANIMATEUR10", 25);
definirConstantes("POINTSMONO100", 13);
definirConstantes("POINTSMULTI200", 14);
definirConstantes("TOUTLESSUCCES", 15); 
definirConstantes("AVATAR", 12);
definirConstantes("RESSOURCE1", 17);
definirConstantes("RESSOURCE2", 18);
definirConstantes("RESSOURCE3", 19);
definirConstantes("RESSOURCE5", 20);
definirConstantes("RESSOURCE10", 21);
definirConstantes("RESSOURCE25", 22);
definirConstantes("CERCLE", 16);
definirConstantes("MESSAGE5", 26);
definirConstantes("MESSAGE10", 27);
definirConstantes("MESSAGE20", 28);
definirConstantes("MESSAGE30", 29);
definirConstantes("MESSAGE50", 30);
definirConstantes("MESSAGE75", 31);
definirConstantes("MESSAGE100", 32);
definirConstantes("VOTE5", 33);
definirConstantes("VOTE10", 34);
definirConstantes("VOTE20", 35);
definirConstantes("VOTE30", 36);
definirConstantes("VOTE50", 37);
definirConstantes("VOTE75", 38);
definirConstantes("VOTE100", 39);
definirConstantes("REPONDRE5", 40);
definirConstantes("REPONDRE10", 41);
definirConstantes("REPONDRE20", 42);
definirConstantes("REPONDRE30", 43);
definirConstantes("REPONDRE50", 44);
definirConstantes("REPONDRE75", 45);
definirConstantes("REPONDRE100", 46);
definirConstantes("RECEVOIRREPONSES5", 47);
definirConstantes("RECEVOIRREPONSES10", 48);
definirConstantes("RECEVOIRREPONSES25", 49);
definirConstantes("DEFI5", 50);
definirConstantes("DEFI10", 51);
definirConstantes("DEFI20", 52);
definirConstantes("DEFI30", 53);
definirConstantes("DEFI50", 54);
definirConstantes("DEFI75", 55);
definirConstantes("DEFI100", 56);
definirConstantes("UTILITEREPANDUE5", 57);
definirConstantes("UTILITEREPANDUE10", 58);
definirConstantes("UTILITEREPANDUE20", 59);
definirConstantes("UTILITEREPANDUE30", 60);
definirConstantes("UTILITEREPANDUE50", 61);
definirConstantes("UTILE5", 62);
definirConstantes("UTILE10", 63);
definirConstantes("UTILE20", 64);
definirConstantes("UTILE30", 65);
definirConstantes("UTILE50", 66);
definirConstantes("UTILE75", 67);
definirConstantes("UTILE100", 68);
definirConstantes("UTILE1003", 69);
definirConstantes("UTILE1005", 70);
definirConstantes("UTILE10010", 71);
definirConstantes("CLASSE", 72);
definirConstantes("MEDAILLE", 73);
definirConstantes("MEDAILLE3", 74);
definirConstantes("MEDAILLE5", 75);
definirConstantes("MEDAILLE10", 76);


// Anonymisation d'un compte
definirConstantes("COMPTE_SUPPRIME_EMAIL", "null@pairform.fr");
definirConstantes("COMPTE_SUPPRIME_ETABLISSEMENT", "");
definirConstantes("COMPTE_SUPPRIME_NOM_PRENOM", "Compte supprimé");
definirConstantes("COMPTE_SUPPRIME_PSEUDO", "Compte supprimé");
definirConstantes("COMPTE_SUPPRIME_AVATAR", "https://www.gravatar.com/avatar/?d=mm&s=100");


//Helper pour génerer une image à la bonne dimension + en faire un double blur en super grand pour affichage arrière plan
exports.creerImageAvecDimension = function (url_or_buffer_or_path, save_path, size, needs_blur, callback) {
	var big_blur_save_path = save_path + ".blur.jpg";

	Jimp.read(url_or_buffer_or_path).then(function (image) {
		//Ecriture de l'image petit format avec gouttière blanches si besoin
		image.background(0xFFFFFFFF)
		image.opaque()
		if (save_path && size) {
			image.contain(size,size)
			image.write(save_path);
		}
		//log.debug("Write image (save_path)");

		if (needs_blur) {
			//Ecriture de l'image grand format et floutée
			var big_blur = image.clone();
			big_blur.quality(100)
			big_blur.contain(1800, 1400)
			big_blur.blur(45)
			big_blur.write(big_blur_save_path);
			//log.debug("Write image (gaussian)");
		}

		typeof callback == "function" ? callback(null, url_or_buffer_or_path) : null;
	})
	.catch(function (err) {
		typeof callback == "function" ? callback(err) : null;
	})
}