"use strict";

var pairform_dao = require("pairform_dao"),
       async = require("async"),
         log = require("metalogger")(),
    constantes = require("../constantes");


// affiche les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)
exports.getExportDonneesPersonnelles = function(parametres, callback) {

  var id_utilisateur = parametres.id_utilisateur,
    id_langue = parametres.id_langue ? parametres.id_langue : 3;

  pairform_dao.getConnexionBDD(function (_connexion_courante) {
    async.parallel({
      //Paramètres généraux
      // Infos personnelles : cape_utilisateurs_v2
      //********************* Infos personnelles : cape_utilisateurs_v2 ********************* //
      profilUtilisateur : function selectProfilUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectProfilUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Langues des utilisateurs : cape_utilisateurs_langues ********************* //
      languesUtilisateur : function selectLanguesUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectLanguesUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },

      //Réseau social
      //********************* Appartenance à une classe : cape_reseaux_utilisateurs ********************* //
      classesAppartientUtilisateur : function selectClassesAppartientUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectClassesAppartientUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Classe et cercles : cape_reseaux ********************* //
      reseauxCreeParUtilisateur : function selectReseauxCreeParUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectReseauxCreeParUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Notifications : cape_notifications ********************* //
      allNotifications : function selectAllNotifications(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectAllNotifications(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },

      //Messages
      //********************* Messages : cape_messages ********************* //
      messages : function selectMessages(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectMessages(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Messages lus : cape_messages_lus ********************* //
      messagesLus : function selectMessagesLus(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectMessagesLus(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Tags mis sur les messages : cape_messages_tags ********************* //
      tagsUtilisateur : function selectTagsUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectTagsUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Votes sur messages : cape_messages_votes ********************* //
      votesUtilisateur : function selectVotesUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectVotesUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Pièces jointes de messages : cape_messages_pieces_jointes ********************* //
      nomsPiecesJointes : function selectNomsPiecesJointes(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectNomsPiecesJointes(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },

      //Gamification
      //********************* Openbadges :  cape_utilisateurs_openbadges ********************* //
      openBadgesGagnesByUtilisateur : function selectOpenBadgesGagnesByUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectOpenBadgesGagnesByUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Succes :  cape_utilisateurs_succes ********************* //
      succesByUtilisateur : function selectSuccesByUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectSuccesByUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },

      //Documents consultés
      //********************* Accès et rôle par documents : cape_utilisateurs_categorie ********************* //
      categorieRessource : function selectCategorieRessource(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectCategorieRessource(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Accès et paramètre documents : cape_utilisateurs_capsules_parametres ********************* //
      parametres : function selectParametres(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectParametres(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Objectifs d'apprentissages :  cape_utilisateurs_objectifs_apprentissage ********************* //
      objectifsApprentissage : function selectObjectifsApprentissage(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectObjectifsApprentissage(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Profils d'apprentissage :  cape_utilisateurs_profils_apprentissage ********************* //
      profilsQuestions : function selectProfilsQuestions(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectProfilsQuestions(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Accès d'un utilisateur via une instance LTI : cape_sources_authentification_utilisateurs ********************* //
      logSourceAuthUtilisateur : function selectLogSourceAuthUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectLogSourceAuthUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },

      //Rôle dans backoffice
      //********************* Membre d'un groupe de visibilité d'un document : cape_membres_groupes ********************* //
      groupesUtilisateur : function selectGroupesUtilisateur(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectGroupesUtilisateur(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Rôle dans un espace : cape_membres ********************* //
      rolesEspaces : function selectRolesEspaces(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectRolesEspaces(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Rôle dans une ressource : cape_membres_roles_ressources ********************* //
      rolesRessources : function selectRolesRessources(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectRolesRessources(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },

      //Contenus créés
      //********************* Sessions : cape_sessions ********************* //
      sessions : function selectSessions(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectSessions(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Item de session : cape_sessions_items ********************* //
      itemsOfSession : function selectItemsOfSession(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectItemsOfSession(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Ressources : cape_ressources ********************* //
      ressources : function selectRessources(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectRessources(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      },
      //********************* Documents : cape_capsules ********************* //
      capsules : function selectCapsules(callback_parallel) {
        pairform_dao.export_donnees_personnelles_dao.selectCapsules(id_utilisateur, function callback_success (result) {
          callback_parallel(null, result);
        }, callback_parallel)
      }
    },
    function callback_final (err, results) {
      if (err) {
        // Erreur custom (pas d'entrée SQL par exemple)
        if (err.message) {
          // On relache la connexion
          pairform_dao.libererConnexionBDD(_connexion_courante);
        }
        // Sinon, elle est déjà relachée dans le cas d'une erreur SQL 
        log.error(err);
        callback(constantes.RETOUR_JSON_KO);
      }
      else{
        // On relache la connexion
        pairform_dao.libererConnexionBDD(_connexion_courante);

        var retour_json = {
          status : constantes.STATUS_JSON_OK,
          data : results
        };

        callback(retour_json);
      }
    })
  });
};
