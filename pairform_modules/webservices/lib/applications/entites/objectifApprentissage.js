"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes");


// récupèration du profil d'apprentissage d'un utilisateur dans une ressource
exports.getObjectifsApprentissageByUtilisateurEtRessource = function(id_ressource, id_utilisateur, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		selectObjectifsApprentissageByUtilisateurEtRessource(_connexion_courante);
	});

	function selectObjectifsApprentissageByUtilisateurEtRessource(_connexion_courante) {
		pairform_dao.objectif_apprentissage_dao.selectObjectifsApprentissageByUtilisateurEtRessource(
			id_ressource,
			id_utilisateur,
			function (liste_objectifs_apprentissage) {
				retour_json.liste_objectifs_apprentissage = liste_objectifs_apprentissage;
				selectNbObjectifsValidesByUtilisateurEtRessource(_connexion_courante);
			},
			callbackError
		);
	}

	// Récupère le nombre d'objectifs d'apprentissage validé par un utilisateur dans une ressource
	function selectNbObjectifsValidesByUtilisateurEtRessource(_connexion_courante) {
		pairform_dao.objectif_apprentissage_dao.selectNbObjectifsValidesByUtilisateurEtRessource(
			id_ressource,
			id_utilisateur,
			function (nb_objectifs_valides) {
				retour_json.nb_objectifs_valides = nb_objectifs_valides;
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// création d'un nouvelle objectif d'apprentissage
exports.putObjectifApprentissage = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK},
		id_ressource = liste_parametres.id_ressource,
		id_utilisateur = liste_parametres.id_utilisateur,
		nom = liste_parametres.nom;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		insertObjectifApprentissage(_connexion_courante);
	});

	function insertObjectifApprentissage(_connexion_courante) {
		pairform_dao.objectif_apprentissage_dao.insertObjectifApprentissage(
			id_ressource,
			id_utilisateur,
			nom,
			function (reponse_sql) {
				retour_json.id_objectif = reponse_sql.id_objectif;
				
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Mise à jour d'un objectif d'apprentissage
exports.postObjectifApprentissage = function(liste_parametres, callback) {
	var id_objectif = liste_parametres.id_objectif,
		nom = liste_parametres.nom,
		est_valide = liste_parametres.est_valide;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		updateObjectifApprentissage(_connexion_courante);
	});

	function updateObjectifApprentissage(_connexion_courante) {
		pairform_dao.objectif_apprentissage_dao.updateObjectifApprentissage(
			id_objectif,
			nom,
			est_valide,
			function () {
				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};


// Suppression d'un objectif d'apprentissage
exports.deleteObjectifApprentissage = function(param, callback) {
	var id_objectif = param.id_objectif;
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		sqlDeleteObjectifApprentissage(_connexion_courante);
	});

	function sqlDeleteObjectifApprentissage(_connexion_courante) {
		pairform_dao.objectif_apprentissage_dao.deleteObjectifApprentissage(
			id_objectif,
			function () {
				callback(constantes.RETOUR_JSON_OK);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			},
			callbackError
		);
	}
};
