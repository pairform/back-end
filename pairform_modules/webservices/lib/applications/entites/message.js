"use strict";

var web_services = require("../../webServices"),
		pairform_dao = require("pairform_dao"),
		  constantes = require("../../constantes"),
		  		CONFIG = require("config"),
				 request = require("request"),
						 log = require('metalogger')(),
			  	 async = require("async"),
			  	api_rs = require("../api_rs"),
						  fs = require('fs');


// Fonctione permettant de récupérer de la base de données le nom du thumbnail correspondant au nom de la pj associée
function recupereNomThumbnailDuNomOriginal(nom, callback){
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.selectNomsThumbnailPiecesJointesDeOriginal(nom, function (result) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok",
					"nom" : result
				});
		}, callbackError);
	});
}


/*
* Méthode écrite pas les étudiants PIST
* Elle permet d'ajouter toutes les pièces jointes en base de données
*/
exports.ajouterPieceJointe = function(param, files, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_message = param.id_message;
	var id_utilisateur = param.id_utilisateur;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.insertMultiplePiecesJointes(id_message, id_utilisateur, files, function () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "up",
					"dataPJ" : files
				});
			}, callbackError);
		}, callbackError);
	});
};


//TODO
exports.ajouterTags = function(param, callback) {		
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		new_tags = param.tags;

	if (param.os == "and")		
		new_tags = JSON.parse(new_tags);

	//Verification que la limite de tags ne soit pas excédée
	if (new_tags.length > 5) {
		callback({"status" : "ko", "message" : "label_erreur_maximum_tags"});
	}
	else {
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.message_dao.selectAllTags(id_message, function callback_succes (old_tags) {
				//Verification que la limite de tags ne soit pas excédée
				if (old_tags.length + new_tags.length > 5) {
					callback({"status" : "ko", "message" : "label_erreur_maximum_tags"});
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
				else {
					//Lowercase des tags, pour éviter le mismatch lié aux majuscules
					var old_tags_lowercase = old_tags.map(function (value, index) {
						return value["tag"].toLowerCase();
					});

					var tags_to_add = [];
					//Vérification que chaque tag ne soit pas déjà en base
					for (var i = 0; i < new_tags.length; i++) {
						var ind = old_tags_lowercase.indexOf(new_tags[i].toLowerCase());
						//Si le tag ne l'est pas
						if (ind == -1 && new_tags[i] != "")
							//On va le rajouter
							tags_to_add.push(new_tags[i]);
					};
					//S'il y a des tags à rajouter
					if (tags_to_add.length > 0) {
						pairform_dao.message_dao.insertMultipleTags(id_message, id_utilisateur, tags_to_add, function () {
							pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
								pairform_dao.libererConnexionBDD(_connexion_courante);
								callback({
									"status" : "up",
									"data" : tags_to_add
								});
							}, callbackError);
						}, callbackError);
					}
					//Sinon, bye bye
					else {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({"status" : "ok"}); 
					}

				}
			}, callbackError);
		});
	}

};


//TODO
exports.supprimerTag = function(param, callback) {		
	function callbackError () {
		callback({"status" : "ko", "message" : "ws_erreur_contacter_pairform"});
	}	
	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		tag = param.tag;

	//Verification que la limite de tags ne soit pas excédée
	if (tag == "") {
		callback({"status" : "ko", "message" : "ws_erreur_contacter_pairform"});
	}
	else {
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.message_dao.deleteTag(id_message, id_utilisateur, tag, function callback_succes (retour_sql) {
				if (retour_sql.affectedRows > 0) {
					pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({"status" : "ok"});			
					}, callbackError);		
				}
				else {
					callback({"status" : "ko", "message" : "ws_erreur_suppression_tag_autre"});
				}
			}, 
			callbackError);
		});
	}

};


//TODO
exports.recupererVotesMessage = function(param, callback) {		
	function callbackError () {
		callback({"status" : "ko", "message" : "ws_erreur_contacter_pairform"});
	}	
	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.vote_dao.selectVotesMessage(id_message, function callback_succes (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			callback({
				"status" : "ok", 
				"data" : retour_sql
			});
		}, 
		callbackError);
	});


};
// deplacerMessage.php
exports.deplacer = function(param, callback) {
	function callbackError () {
		callback({"status" : "ko", "message" : "ws_erreur_contacter_pairform"});
	}	
	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		id_capsule = param.id_capsule,
		nom_page = param.nom_page,
		nom_tag = param.nom_tag,
		num_occurence = param.num_occurence;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updatePosition(
			id_message,
			id_capsule,
			nom_page,
			nom_tag,
			num_occurence,
			function callback_succes (retour_sql) {
			if (retour_sql.affectedRows > 0) {
				pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
					pairform_dao.libererConnexionBDD(_connexion_courante);
					callback({"status" : "ok"});			
				}, callbackError);		
			}
			else {
				callback({"status" : "ko", "message" : "ws_erreur_suppression_tag_autre"});
			}
		}, 
		callbackError);
	});
};


// donnerMedaille.php
exports.donnerMedaille = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	var medaille = param.medaille || param.type_medaille || "", //String vide pour l'enlever
		id_message = param.id_message,
		id_utilisateur = param.id_utilisateur;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaMedaille(id_message, medaille, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
				api_rs.donnerMedaille(id_utilisateur, id_message);
			}, callbackError);
		}, callbackError);
	});
};


// enregistrerMessage.php
exports.envoyer = function(param, files, utilisateur, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param["id_utilisateur"],
		uid_page = param["uid_page"] && param["uid_page"] != "_pf_res" ? param["uid_page"] : "",
		uid_oa = param["uid_oa"] || "",
		id_espace = param["id_espace"] || "DEFAULT", 
		id_ressource = param["id_ressource"] || "DEFAULT", 
		id_capsule = param["id_capsule"] || "DEFAULT", 
		nom_page = param["nom_page"] || "", 
		nom_tag = param["nom_tag"] || "", 
		num_occurence = param["num_occurence"] || "DEFAULT", 
		geo_latitude = param["geo_latitude"] || "DEFAULT", 
		geo_longitude = param["geo_longitude"] || "DEFAULT", 
		id_langue = param["id_langue"] || param["langue"] || "DEFAULT", 
		id_message_parent = param["id_message_parent"] || param["id_message_original"] || param["parent"] || "DEFAULT", 
		est_defi = param["est_defi"] || "DEFAULT",
		contenu = param["contenu"] || param["value"] || "",
		tags = [],
		visibilite = "0";

	if (param["nom_page"] == "_pf_res")
		nom_page = "";

	if (param["tags"])		
		tags = JSON.parse(param["tags"]);

	if(param["visibilite"]) {
		if (param.os == "and")
			visibilite = param["visibilite"].length > 0 ? param["visibilite"] : "0";
		else
			visibilite = JSON.parse(param["visibilite"]).length > 0 ? JSON.parse(param["visibilite"]).toString() : "0";			
	}


	 log.debug(param);

	async.series({
	  autorisation: function(next){
				//On check l'état de la capsule si on est en train de poster sur une capsule
				if (id_capsule != "DEFAULT" && id_capsule != "0" && id_capsule != 0) {
					pairform_dao.capsule_dao.selectCapsuleVisibiliteParNomGroupe(
						id_capsule,
						constantes.GROUPE_SPHERE_PUBLIQUE,
						function callback_success (retour_sql) {
							//Capsule inexistante ?
							if (!retour_sql.length) {
								//On envoie une 404
								return send_404(res);
							};

							var id_visibilite = retour_sql[0]["id_visibilite"];
							var est_bloquee = retour_sql[0]["est_bloquee"];

							log.debug(retour_sql);

							if (est_bloquee) {
								next({
									status:"ko",
									message:"ws_erreur_autorisation_ecriture"
								});
							}
							//Si la capsule est visible
							else if(id_visibilite == constantes.VISIBILITE_ACCESSIBLE){
								//Rock'n'roule
								next(null);
							}
							else {
								//On vérifie son accès à la capsule demandée
								web_services.capsule.getAutorisationAcces(
									id_capsule,
									utilisateur.email,
									function(autorisation) {
										// S'il est autorisé (au moins une autorisation trouvée dans le retour SQL) || admin pairform
										if (autorisation.length || utilisateur.est_admin_pairform) {
											next(null);

										} else {
											next({
												status:"ko",
												message:"ws_erreur_autorisation_acces"
											});
										}
									}
								);

							}

						},
						next
					)
				}
				//Si on poste sur un espace ou une ressource, on continue
				else{
					id_capsule = "DEFAULT"
					next(null);
				}
	  },
	  insertion: function(next){
			pairform_dao.message_dao.insertMessage(
				id_utilisateur,
				uid_page,
				uid_oa,
				id_espace,
				id_ressource,
				id_capsule,
				nom_page,
				nom_tag,
				num_occurence,
				geo_latitude,
				geo_longitude,
				id_langue,
				id_message_parent,
				est_defi,
				visibilite,
				contenu,
				function callback_succes (retour_sql) {
					var id_message = retour_sql.insertId;

					async.parallel([
						function(done){
							//S'il y a des tags à mettre
							if (tags.length) {
								exports.ajouterTags({"id_utilisateur" : id_utilisateur, "id_message" : id_message, "tags" : tags}, function callbackTags (retour) {
									//Cas ou on a pas de tags ajouté
									if (retour == "ok") {
										done(null, {retour : ""});
									}
									//Cas ou on à ajouter des tags en plus de certains préexistant. 
									//Ici retour = {"status" : "up","data" : tags_to_add})
									else {
										done(null, {retour : retour});
									}
								});
							}
							else {
								done(null, {retour : ""});
							}
						},
						function(done){

							//Le but de cette algo est de regrouper les thumbnails avec
							//les pieces jointes en tant qu'une seul unité.
							var filesKey = Object.keys(files); //tableau contenant les nom originaux de toutes les pieces jointes
							var filesTmp = {}; //Objet final contenant les pièces jointes reconditionné avec son thumbnail
							var thumbNailArray = {}; //Tableau contenant le nom serveur des thumbnails
							var monThumbnailArray = param.nomsThumbnails; //Tableau contenant les thumbnails envoyées en base64

							// log.debug(filesKey);
							// log.debug(filesTmp);
							// log.debug(thumbNailArray);
							// log.debug(monThumbnailArray);
							//Je récupère les thumbnails et leur nom donné par le serveur
							//Si le serveur n'a pas interpréter des thumbnails en base 64 alors les thumbnails sont envoyés en tant que fichier
							if(monThumbnailArray.length == 0){
								for(var i = 0; i < filesKey.length; i++){
									var file = files[filesKey[i]];
									var originalname = file["originalname"];

									//Si le nom de la pj contient "thumbnail" alors c'est un thumbnail
									//Du coup on va stocker dans tableau le nom du serveur à partir du 
									//nom original sans thumbnail = au nom original de la piece jointe correspondante
									if(originalname.indexOf("thumbnail") > -1){
										thumbNailArray[originalname.replace(/-thumbnail\..*/,'')] = file["name"]
									}
								}
							}else{ //Si oui alors on donne le nom que j'ai moi-même créé
								for(var i = 0; i < monThumbnailArray.length; i++)
									thumbNailArray[monThumbnailArray[i].original.replace(/-thumbnail\..*/,'')] = monThumbnailArray[i].nom;
							}

							//on repasse un tour pour récupérer que les pjs qui ne sont pas des thumbnails
							for(var i = 0; i < filesKey.length; i++){
								var file = files[filesKey[i]],
									index = param["position_" + filesKey[i]];

								log.debug(filesKey[i] + " : " + index)
								//var originalname = file["originalname"]
								
								if(filesKey[i].indexOf("thumbnail") > -1){
									continue
								}

								//On ajoute à la pj une variable qui va contenir le nom serveur du thumbnail correspondant
								file.nom_thumbnail = thumbNailArray[filesKey[i].replace(/\..*/, '')];
								file.position = index;
								filesTmp[filesKey[i]]=file
							}
							
							// log.debug(filesKey);
							// log.debug(filesTmp);
							// log.debug(thumbNailArray);
							// log.debug(monThumbnailArray);
							//ici, si il y a des pieces-jointes contenues dans files, alors on va effectuer la fonction qui va ajouter les pieces-jointes dans la BDD (ils sont déjà stocké dans les dossiers de l'ordinateur normalement
							if(filesTmp){
								if (Object.keys(filesTmp).length != 0) {
									exports.ajouterPieceJointe({"id_utilisateur" : id_utilisateur, "id_message" : id_message}, filesTmp, 
										function callbackPiecesJointes (retourPJ) {
											done(null, {retourPJ : retourPJ});
									});
								}else {
									done(null, {retourPJ : ""});
								}
							}else {
								done(null, {retourPJ : ""});
							}
						
						}
					],
					function(err, results){
						if(err){
							next(err);
						}

						next(null, {
							"status" : "ok",
							"id_message" : id_message,
							"data" : results[0].retour,
							"dataPJ" : results[1].retourPJ
						});
				});
				//Si on est dans une ecriture sur une capsule ou sur une ressource
				if (id_espace == "DEFAULT") 
					//On ajoute des points
					api_rs.ecrireMessage(id_utilisateur, id_message);
			}, callbackError);
	  },
	},
	function(err, results) {
	  if (err) {
	  	callback(err)
	  }
	  else {
	  	callback(results.insertion)
	  }
	  
	});

};


//Méthode permettant de récupérer le chemin vers la pj demandé à télécharger
//Possibilité d'amélioration : regarder si la pj est disponible dans la bdd en fonction de la ressource regardé
exports.telechargerPJ = function(param, callback){
	var chemin;
	//Si la requete contient un paramètre type c'est qu'on demande un thumbnail
	if(param["type"]){
		//Je recupère le nom du thumbnail à partir du nom de la piece jointe correspondante
		//param["nom_serveur_pj"] == nom serveur de la pj
		recupereNomThumbnailDuNomOriginal(param["nom_serveur_pj"], function(data){
			// log.debug(data);
			var nom_piece_jointe = data["nom"][0];
			//Si on trouve bien la piece jointe
			if (nom_piece_jointe) {
				//Chemin vers le thumbnail
				chemin = constantes.REPERTOIRE_PIECES_JOINTES + data["nom"][0].nom_thumbnail;
				//Si le fichier existe bien
				if (fs.existsSync(chemin)) {
					callback({status: 'ok', chemin: chemin});
				}
				else {
					callback({status: 'ko'});
				}
			}
			else {
				callback({status: 'ko'});
			}
		});
	}
	else{
		//Chemin pour récupérer la piece jointe
		chemin = constantes.REPERTOIRE_PIECES_JOINTES + param["nom_serveur_pj"];
		if (fs.existsSync(chemin)) {
			callback({status: 'ok', chemin: chemin});
		}
		else {
			callback({status: 'ko'});
		}
	}
};


// modifierLangue.php
exports.modifierLangue = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	var id_langue = param.id_langue || param.langue || 3, //Français par défaut
		id_message = param.id_message;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaLangue(id_message, id_langue, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
			}, callbackError);
		}, callbackError);
	});
};


// recupererMessagesV2.php
exports.getMessages = function(param, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};
	// log.notice("Parametres " + JSON.stringify(param));

	var id_utilisateur = param.id_utilisateur,
	 filtre_timestamp = param.filtre_timestamp ? JSON.parse(param.filtre_timestamp) : undefined,
	 // filtre_ressource = param.filtre_ressource ? JSON.parse(param.filtre_ressource) : undefined,
		   id_capsule = param.id_capsule || param.id_ressource,
	langues_affichage = param.langues_affichage,
			 nom_page = param.nom_page,
			  nom_tag = param.nom_tag,
		num_occurence = param.num_occurence,
			 uid_page = param.uid_page,
			   uid_oa = param.uid_oa,
		   id_message = param.id_message;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
		function callbackError() {
			callback(constantes.RETOUR_JSON_KO);
		}

		function callbackSucces (messages) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			//Cast en string de toutes les valeurs pour les mobiles
			if (param.os != "web") {
				messages = JSON.parse(JSON.stringify(messages, function (key, value) {
					if (typeof(value) == "number") {
						return value + "";
					}
					else 
						return value;
				}));
			}
			callback({"status" : "ok", "messages" : messages});
		}

		/**************************************************************************************************************************/
		/****************************************** Toutes les sections filtrées **************************************************/
		/**************************************************************************************************************************/
		if (filtre_timestamp && !id_capsule && (!nom_page && !uid_page) && (!nom_tag  && !num_occurence && !uid_oa) && !id_message){
			pairform_dao.message_dao.selectMessageWithFiltre(
				id_utilisateur, 
				filtre_timestamp, 
				callbackSucces, 
				callbackError
			);

		}
		/**************************************************************************************************************************/
		/************************************************ Une section *************************************************************/
		/**************************************************************************************************************************/
		else if (!filtre_timestamp && id_capsule && (!nom_page && !uid_page) && (!nom_tag  && !num_occurence && !uid_oa) && !id_message){
			pairform_dao.message_dao.selectAllMessagesOfCapsule(
				id_utilisateur,
				id_capsule,
				callbackSucces,
				callbackError
			);
		}
		/**************************************************************************************************************************/
		/************************************************ Une page ****************************************************************/
		/**************************************************************************************************************************/
		else if (!filtre_timestamp && id_capsule && (nom_page || uid_page) && (!nom_tag  && !num_occurence && !uid_oa) && !id_message){
			if(nom_page){
				pairform_dao.message_dao.selectMessageOfPage(
					id_utilisateur, 
					id_capsule, 
					nom_page, 
					callbackSucces, 
					callbackError
				);
			}
			else {
				pairform_dao.message_dao.selectMessageOfPageOA(
					id_utilisateur, 
					id_capsule, 
					uid_page, 
					callbackSucces, 
					callbackError
				);
			}
		}
		/**************************************************************************************************************************/
		/************************************************ Un grain ***************************************************************/
		/**************************************************************************************************************************/
		else if (!filtre_timestamp && id_capsule && (nom_page || uid_page) && ((nom_tag  && num_occurence) || uid_oa) && !id_message){
			if(nom_page && num_occurence){
				pairform_dao.message_dao.selectMessageOfGrain(
					id_utilisateur, 
					id_capsule, 
					nom_page,
					nom_tag, 
					num_occurence, 
					callbackSucces, 
					callbackError
				);
			}
			else {
				pairform_dao.message_dao.selectMessageOfGrain(
					id_utilisateur, 
					id_capsule, 
					uid_page,
					uid_oa, 
					callbackSucces, 
					callbackError
				);
			}

		}
		/**************************************************************************************************************************/
		/************************************************ Un message **************************************************************/
		/**************************************************************************************************************************/
		else if (!filtre_timestamp && !id_capsule && (!nom_page && !uid_page) && (!nom_tag  && !num_occurence && !uid_oa) && id_message){
			pairform_dao.message_dao.selectMessageFromId(
				id_utilisateur, 
				id_message, 
				callbackSucces, 
				callbackError
			);
		}
		else {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			callbackError();
		}
	});
};


//
exports.getMessagesTransversal = function(param, callback) {
	var flag_filtre = param.flag_filtre,
		id_utilisateur_concerne = flag_filtre ? param.id_utilisateur_concerne : param.id_utilisateur,
		id_utilisateur_connecte = flag_filtre ? param.id_utilisateur_connecte : param.id_utilisateur,
		langues_affichage = param.langues_affichage || [1,2,3];

	// log.debug(param);
	// log.debug("Messages transversal (id_utilisateur_concerne : " + id_utilisateur_concerne + "), (id_utilisateur_connecte : " + id_utilisateur_connecte + ")" );
	pairform_dao.utilisateur_dao.selectToutesCapsulesSuivies(id_utilisateur_concerne, function callback_succes (retour_sql) {
		if (!retour_sql.length) {
			log.error("Pas de categorie.");
			return callback(constantes.RETOUR_JSON_KO);
		}

		var retour = {
			"status" : "ok",
			"data" : []
		};

		var capsules = retour_sql;

		// log.debug("Capsules : " + JSON.stringify(capsules));
		async.eachLimit(capsules, 10, function (capsule, next) {
			// log.debug("Step capsule : " + JSON.stringify(capsule));
			if(flag_filtre){

				pairform_dao.message_dao.selectAllMessagesOfCapsuleAvecFiltre(
					id_utilisateur_connecte,
					capsule.id_capsule,
					id_utilisateur_concerne,
					function callback_succes (retour_sql) {
						if (retour_sql.length) {
							capsule.messages = retour_sql;
							retour.data.push(capsule);
						}
						next(null);
						// log.debug("Step" );
					},
					next
				);
			}
			else{
				pairform_dao.message_dao.selectAllMessagesOfCapsule(
					id_utilisateur_connecte,
					capsule.id_capsule,
					function callback_succes (retour_sql) {
						if (retour_sql.length) {
							capsule.messages = retour_sql;
							retour.data.push(capsule);
						}
						next(null);
						// log.debug("Step" );
					},
					next
				);
			}
		}, function callback_final (err) {
			// log.debug("Fin" );
			if (err) {
				log.error(err);
				callback(constantes.RETOUR_JSON_KO);		
			}
			else{
				callback(retour);
			}
		});
		
	}, function callback_error (erreur) {
		log.error(erreur);
		callback(constantes.RETOUR_JSON_KO);
	});

};


//
exports.getDerniersMessagesTransversal = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000,
		langues_affichage = param.langues_affichage || [1,2,3];

	// log.debug("Messages transversal (id_utilisateur : " + id_utilisateur + ")" );
	pairform_dao.utilisateur_dao.selectToutesCapsulesSuivies(id_utilisateur, function callback_succes (retour_sql) {
		if (!retour_sql.length) {
			log.error("Pas de categorie.");
			return callback(constantes.RETOUR_JSON_KO);
		}

		var retour = {
			"status" : "ok",
			"data" : []
		};

		var capsules = retour_sql,
			array_id_capsules = [];

		for (var i = 0; i < capsules.length; i++) {
			array_id_capsules.push(capsules[i].id_capsule);
		}

		pairform_dao.message_dao.selectMessageWithinCapsules(
			id_utilisateur,
			array_id_capsules,
			function callback_succes (retour_sql) {
				retour.data = retour_sql;
				callback(retour);
			},
			function callback_final (err) {
				if (err) {
					log.error(erreur);
					callback(constantes.RETOUR_JSON_KO);		
				}
			}
		);
		
	}, function callback_error (erreur) {
		log.error(erreur);
		callback(constantes.RETOUR_JSON_KO);
	});

};


//
exports.nombre = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000,
		langues_affichage = param.langues_affichage || [1,2,3];

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.selectNombreAllRessources(id_utilisateur, langues_affichage, function callback_succes (nombre_messages) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_messages_objet = {};
			for (var i = 0; i < nombre_messages.length; i++) {
				nombre_messages_objet[nombre_messages[i]['id_ressource']] = nombre_messages[i]['count'];
			};
			var retour = {
				"status" : "ok",
				"nombre_messages" : nombre_messages_objet
			};
			callback(retour);
		}, callbackError)
	});
};


//
exports.nombreParCapsules = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000,
		langues_affichage = param.langues_affichage || [1,2,3];

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.selectNombreAllRessourcesByCapsules(id_utilisateur, langues_affichage, function callback_succes (nombre_messages) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_messages_objet = {};
			for (var i = 0; i < nombre_messages.length; i++) {
				nombre_messages_objet[nombre_messages[i]['id_capsule']] = nombre_messages[i]['count'];
			};
			
			callback(nombre_messages_objet);
		}, callbackError)
	});
};


//
exports.nombreDansRessource = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000,
		langues_affichage = param.langues_affichage || [1,2,3],
		id_ressource = param.id_ressource;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.selectNombreFromRessource(id_utilisateur, langues_affichage, id_ressource, function callback_succes (nombre_messages) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_messages_objet = {};
			for (var i = 0; i < nombre_messages.length; i++) {
				nombre_messages_objet[nombre_messages[i]['id_capsule']] = nombre_messages[i]['count'];
			};
			var retour = {
				"status" : "ok",
				"nombre_messages" : nombre_messages_objet
			};
			callback(retour);
		}, callbackError)
	});
};


// redirigerVersMessage.php
exports.rediriger = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_message = parseInt(param.id_message); // Parse pour être sûr d'avoir un entier en entrée
	
	// Si l'id demandé n'est pas valide
	if (!id_message)
		return callbackError();

	pairform_dao.message_dao.selectMessagePourRedirection(id_message, function callback_succes (retour_sql) {
		
		if (!retour_sql.length) {
			return callbackError();
		}

		var message = retour_sql[0];

		if (message.type_message == "capsule") {

			var base_url = message.url_web,
				nom_page,
				anchor;

			//Sur la ressource
			if (!message.nom_page && !message.uid_page && !message.uid_oa) {
				nom_page = "index.html";
				anchor = "res";
			}
			// Ou sur un grain
			else{
				//Mode automatique AUTO
				if (message.nom_tag && message.nom_page) {
					//Si on est sur un format Storyline 2
					if ((message.id_format_capsule == constantes.FORMAT_STORYLINE2) || (message.id_format_capsule == constantes.FORMAT_STORYLINE3)) {
						nom_page = "index.html";
						anchor = "page";
					}
					//Si on est sur un format Scenari, ChainEdit, PairForm ou Injecteur
					// if ([0,1,2,3].indexOf(message.id_format_capsule) != -1) {
					else {
						//Sur une page
						nom_page = message.nom_page;
						anchor = "page";
					}
					//TODO: trouver une solution pour identifier les ressources scenari pour rajouter le /co
					// base_url += "/co";
				}
				//Mode identifié OA
				else if (message.uid_page) {
				  nom_page = message.uid_page != "" ? message.uid_page + '.html' : "";
				  anchor = message.uid_page != "" ? "page" : "res";
				}
			}
			var final_url = base_url + nom_page + '#' + anchor + '-' + id_message;
			callback({
				"status" : "ok",
				"url" : final_url
			});
		}
		else if (message.type_message == "ressource") {
			var final_url = CONFIG.app.urls.serveur_node + '/doc/' + message.id_espace + '/' + message.id_ressource;
			callback({
				"status" : "ok",
				"url" : final_url
			});
		}
	}, callbackError);
};


// supprimerMessage.php
exports.supprimer = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		supprime_par_precedent = +param.supprime_par, //Moche cast en int pour être sûr d'avoir un entier (parseInt bug si c'est deja un int)
		id_message = param.id_message;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaSupprime(id_message, id_utilisateur, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
				api_rs.supprimerMessage(id_utilisateur, id_message, supprime_par_precedent);
			}, callbackError);
		}, callbackError);
	});
};


//
exports.supprimerDefinitivement = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		id_rank_user_moderator = param.id_rank_user_moderator,
		est_admin_pairform = param.est_admin_pairform;

	//Si c'est un admin
	//On vérifie quand même côté serveur si c'est un admin (système anti-petit-malin)
	if(id_rank_user_moderator == "5" || est_admin_pairform){
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.message_dao.deleteMessage(id_message, function callback_succes () {
				pairform_dao.notification_dao.deleteNotificationsLinkedToMessage(id_message, function () {
					pairform_dao.libererConnexionBDD(_connexion_courante);
					callback({
						"status" : "ok"
					});
				}, callbackError);
			}, callbackError);	
		});
		
	}
	else {
		callback({
			"status" : "ko",
			"message" : "ws_erreur_suppression"
		});
	}
};


// terminerDefi.php
exports.terminerDefi = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	
	var id_message = param.id_message,
		id_utilisateur = param.id_utilisateur;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaEstDefi(id_message, 2, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
				api_rs.terminerDefi(id_utilisateur, id_message);
			}, callbackError);
		}, callbackError);
	});
};


// validerDefi.php
exports.validerReponseDefi = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
		
	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		defi_valide = param.defi_valide || 1;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.message_dao.updateMetaDefiValide(id_message, defi_valide, function callback_succes () {
			pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback({
					"status" : "ok"
				});
				api_rs.validerReponseDefi(id_utilisateur, id_message);
			}, callbackError);
		}, callbackError);
	});
};


// voterUtilite.php
exports.voter = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		id_message = param.id_message,
		vote = param.vote || param.up;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.vote_dao.selectVote(id_message, id_utilisateur, function callback_succes (a_vote) {

			//Conversion en int si c'est pas encore le cas
			if (typeof(vote) == "string") 
				vote = vote == "true" ? 1 : -1;
			else
				//Empecher des éventuels petits malins d'ajouter +2836394 à leur potes en tronquant à 1 / -1
				vote = (vote ? 1 : -1);
			//Copie avec + pour eviter un pointeur, pour s'en servir dans la notification
			var nouveau_vote = +vote;
			// }
			if (a_vote.length) {
				//Annulation du vote le cas échéant
				if (a_vote[0]["vote"] == vote)
					vote = 0;

				pairform_dao.vote_dao.updateVote(id_message, id_utilisateur, vote, function callback_succes () {
					pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({
							"status" : "ok"
						});
						api_rs.evaluerMessage(id_utilisateur, id_message, nouveau_vote, a_vote[0]["vote"]);
					}, callbackError);
				}, callbackError);
			}
			else {
				pairform_dao.vote_dao.insertVote(id_message, id_utilisateur, vote, function callback_succes () {
					pairform_dao.message_dao.updateMetaDateModification(id_message, function () {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({
							"status" : "ok"
						});
						api_rs.evaluerMessage(id_utilisateur, id_message, nouveau_vote, undefined);
					}, callbackError);
				}, callbackError);	
			}
		}, callbackError);
	});
};


// GET : récupère les id des messages lus pour un utilisateur
//		ressources - @array : identifiants des ressources à récupérer
//		capsules - @array : identifiants des capsules à récupérer
exports.getMessagesLus = function (param, callback){
	var	array_id, DAO_select;
	var id_utilisateur = param.id_utilisateur,
					os = param.os;

	if (typeof param.ressources != "undefined") {
		DAO_select =  pairform_dao.message_dao.selectMessagesLusParRessource;
		array_id = param.ressources;
	}
	else if (typeof param.capsules != "undefined") {
		DAO_select =  pairform_dao.message_dao.selectMessagesLusParCapsule;
		array_id = param.capsules;
	}
	else{
		return callback({status : constantes.STATUS_JSON_KO, message : "no_data"});
	}
	if (array_id && array_id.length) {
		if (os == "and")
			array_id = JSON.parse(array_id);
		
		//Si jamais on a pas un tableau, mais un entier (car un seul paramètre passé)
		//Les navigateurs transforment le tableau en un entier
		//On le remet au bon format pour le `join` en DAO
		if (typeof array_id != "object")
			array_id = [array_id];
		
		DAO_select(array_id, id_utilisateur, 
			function callback_succes (retour_sql) {
				// log.debug(JSON.stringify(retour_sql, null, true));
				callback({status : "ok", "data" : retour_sql});
			}, 
			function callback_error (err) {
				callback(constantes.RETOUR_JSON_KO);
		});
	}
	else{
		callback({status : constantes.STATUS_JSON_KO, message : "no_data"});
	}
	api_rs.updateLastTimeActive(id_utilisateur);
}


// POST : ajoute en BDD l'état lu d'une liste de messages pour un utilisateur
//		messages - @array [id_message, ...] 
//		- identifiants des messages
exports.setMessagesLus = function (param, callback){
	var messages = param.messages,
		id_utilisateur = param.id_utilisateur,
		os = param.os;

	if(os == "and")
		messages = JSON.parse(messages);

	if (messages && messages.length) {
		pairform_dao.message_dao.insertMessagesLus(messages, id_utilisateur, os, 
			function callback_succes () {
				callback({"status" : "ok"});
			},
			function callback_error (err) {
				callback(constantes.RETOUR_JSON_KO);
		});
	}
	else{
		callback({status : constantes.STATUS_JSON_KO, message : "no_data"});
	}
}
