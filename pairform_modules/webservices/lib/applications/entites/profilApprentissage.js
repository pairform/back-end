"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes");


// récupèration du profil d'apprentissage d'un utilisateur dans une ressource
exports.getProfilApprentissageByUtilisateurEtRessource = function(id_ressource, id_utilisateur, code_langue, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		selectQuestionsByUtilisateurEtRessource(constantes.LANGUES[code_langue], _connexion_courante);
	});

	
	// Récupère la liste des questions du profil d'apprentissage d'un utilisateur dans une ressource
	function selectQuestionsByUtilisateurEtRessource(id_langue, _connexion_courante) {
		pairform_dao.profil_apprentissage_dao.selectQuestionsByUtilisateurEtRessource(
			id_ressource,
			id_utilisateur,
			id_langue,
			function (liste_questions) {
				// s'il y a des questions, on va récupérer les réponses
				if (liste_questions.length > 0) {
					retour_json.liste_questions = liste_questions;
					selectReponsesByQuestion(id_langue, 0, _connexion_courante);
				} else {
					// sinon, cela veut dire que l'utilisateur consulte pour la première fois le profil d'apprentisage de la ressource
					// on initialise avec les valeurs par défaut (null) les réponses valides aux questions
					selectIdQuestionsByRessource(_connexion_courante)
				}
			},
			callbackError
		);
	}

	// Récupère la liste des réponses à une question
	function selectReponsesByQuestion(id_langue, index_question, _connexion_courante) {
		pairform_dao.profil_apprentissage_dao.selectReponsesByQuestion(
			retour_json.liste_questions[index_question].id_question,
			id_langue,
			function (liste_reponses) {
				retour_json.liste_questions[index_question].liste_reponses = liste_reponses;

				index_question++
				// s'il y a encore une question pour laquelle les réponses n'ont pas été récupérées
				if (index_question < retour_json.liste_questions.length) {
					selectReponsesByQuestion(id_langue, index_question, _connexion_courante);
				} else {
					callback(retour_json);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}

	// Récupère la liste des id des questions du profil d'apprentissage d'une ressource
	function selectIdQuestionsByRessource(_connexion_courante) {
		pairform_dao.profil_apprentissage_dao.selectIdQuestionsByRessource(
			id_ressource,
			function (liste_id_questions) {
				insertReponseValideByQuestion(liste_id_questions, 0, _connexion_courante);
			},
			callbackError
		);
	}

	// créé la reponse par défaut (null) d'un utilisateur pour une question du profil d'apprentissage d'une ressource
	function insertReponseValideByQuestion(liste_id_questions, index_question, _connexion_courante) {
		pairform_dao.profil_apprentissage_dao.insertReponseValideByQuestion(
			liste_id_questions[index_question].id_question,
			id_utilisateur,
			function () {
				index_question++
				// s'il y a encore une question pour laquelle une réponse par défaut n'a pas été inséré
				if (index_question < liste_id_questions.length) {
					insertReponseValideByQuestion(liste_id_questions, index_question, _connexion_courante)
				} else {
					selectQuestionsByUtilisateurEtRessource(constantes.LANGUES[code_langue], _connexion_courante);
				}
			},
			callbackError
		);
	}
};


// Mise à jour d'un profil d'apprentissage
exports.postProfilApprentissage = function(param, callback) {
	var liste_reponses_valides = param.liste_reponses_valides,
 		id_utilisateur = param.id_utilisateur;
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		if (liste_reponses_valides.length > 0)
			updateReponseValideByQuestion(0, _connexion_courante);
	});

	function updateReponseValideByQuestion(index_reponse, _connexion_courante) {
		pairform_dao.profil_apprentissage_dao.updateReponseValideByQuestion(
			liste_reponses_valides[index_reponse].id_question,
			liste_reponses_valides[index_reponse].id_reponse_valide,
			id_utilisateur,
			function () {
				index_reponse++;
				// s'il y a encore des reponse dans la liste
				if (index_reponse < liste_reponses_valides.length) {
					updateReponseValideByQuestion(index_reponse, _connexion_courante);
				} else {
					callback(constantes.RETOUR_JSON_OK);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
			},
			callbackError
		);
	}
};

