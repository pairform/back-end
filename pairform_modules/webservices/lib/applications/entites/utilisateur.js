"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		  api_rs = require("../api_rs"),
			 log = require('metalogger')(),
		passport = require("passport"),
		 request = require("request"),
		   async = require("async"),
    		  fs = require('fs'),
		  bCrypt = require("bcrypt-nodejs"),
		  crypto = require("crypto"),
		  CONFIG = require("config");


// editerAvatar.php
exports.editerAvatar = function(param, callback) {
	var id_utilisateur = param.id_utilisateur,
		avatar = param.avatar,
		avatar_blob, 
		avatar_path_fs = constantes.REPERTOIRE_AVATARS + id_utilisateur +".png";

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	// Si on rétabli l'avatar par défaut gravatar
	if (!avatar) {
		var md5 = crypto.createHash('md5').update(id_utilisateur + "").digest("hex");

		//Génération d'une image avec gravatar
		avatar = "https://www.gravatar.com/avatar/" + md5 + ".jpg?d=identicon&s=100";
		
		constantes.creerImageAvecDimension(avatar, avatar_path_fs, 100, true, updateAvatarURL)
		
		// //Récupération et enregistrement de l'image en local
		// request(avatar).on('error', function(err) {
		// 	log.error(err)
		//   }).pipe(fs.createWriteStream(avatar_path_fs)).on('close', function (argument) {
		// 	updateAvatarURL();
		// });
	
	}
	else{
		//Fix : Les "+" dans la chaine Base64 se font convertir en espace (p-e par Express) à un moment
		avatar = avatar.replace(/ /g, "+");

		avatar_blob = new Buffer(avatar, "base64");							// on transforme sa représentation base64 en hexadécimale
		//url_or_buffer_or_path, save_path, size, needs_blur, callback
		constantes.creerImageAvecDimension(avatar_blob, avatar_path_fs, 100, true, updateAvatarURL)
		// fs.writeFile(avatar_path_fs, avatar_blob, updateAvatarURL);			// stockage du avatar dans le repertoire dédié
	}

	function updateAvatarURL () {
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.utilisateur_dao.updateAvatar(
				id_utilisateur,
				avatar_path_fs,
				function () {
					//Timestamp dans l'URL de l'avatar pour effacer le cache client
					callback({
						"status" : "up",
						"data" : {
							"url" :CONFIG.app.urls.serveur_node + "/" + constantes.URL_RELATIF_LOGOS_AVATARS + id_utilisateur + ".png?v="+ Date.now()
						}
					});
					api_rs.changerAvatar(id_utilisateur);
				},
				callbackError
			);
		});
	}
};



// editerLangues.php
exports.editerLangues = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
	 id_langue_principale = param.langue_principale || 3,
		array_id_autre_langues = param.autres_langues ? JSON.parse(param.autres_langues) : [];

	var langues = [[id_utilisateur, id_langue_principale, 1]];

	array_id_autre_langues.forEach(function (id_langue){
		langues.push([id_utilisateur, id_langue, 0]);
	});
	pairform_dao.utilisateur_dao.deleteLanguesUtilisateur(
		id_utilisateur,
		function (retour_sql) {
			pairform_dao.utilisateur_dao.insertLanguesUtilisateur(
				langues,
				function (retour_sql) {
					callback({
						"status" : "ok"
					});
				},
				callbackError
			);
		},
		callbackError
	);
};


exports.liste = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.selectUtilisateurs(
			function (retour_sql) {

				pairform_dao.libererConnexionBDD(_connexion_courante);

				if (retour_sql.length)
					callback({
						"status" : "ok",
						"profils" : retour_sql
					});
				else
					callbackError();
			},
			callbackError
		);
	
	});
};

exports.listeParametresNotifications = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	var id_utilisateur = param.id_utilisateur;

	pairform_dao.utilisateur_dao.selectParametresNotificationsParCapsule(
		id_utilisateur,
		function (retour_sql) {
			callback({
				"status" : "ok",
				"data" : retour_sql
			});
			
		},
		callbackError
	);
};

//changerNewsParMail.php
exports.changerNewsParMail = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	var id_utilisateur = param.id_utilisateur,
			id_capsule = param.id_capsule;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.updateNotificationsMailUtilisateur(
			id_utilisateur,
			id_capsule,
			function (retour_sql) {

				pairform_dao.libererConnexionBDD(_connexion_courante);

				if (retour_sql.changedRows){
					callback({
						"status" : "ok"
					});
				}
				else
					callbackError();
			},
			callbackError
		);
	});
};

exports.changerWatchCapsule = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	var id_utilisateur = param.id_utilisateur,
			id_capsule = param.id_capsule;

	pairform_dao.utilisateur_dao.updateWatchMailUtilisateur(
		id_utilisateur,
		id_capsule,
		function (retour_sql) {
			if (retour_sql.changedRows){
				callback({
					"status" : "ok"
				});
			}
			else
				callbackError();
		},
		callbackError
	);
};


exports.changerNouvellesNotifications = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	var id_utilisateur = param.id_utilisateur,
			id_capsule = param.id_capsule;

	pairform_dao.utilisateur_dao.updateNouvellesNotificationsUtilisateur(
		id_utilisateur,
		function (retour_sql) {
			if (retour_sql.changedRows){
				callback({
					"status" : "ok"
				});
			}
			else
				callbackError();
		},
		callbackError
	);
};


exports.changerToutesNotifications = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	var id_utilisateur = param.id_utilisateur,
			value = param.value || "0";

	async.parallel({
	    one: function(callback){
				pairform_dao.utilisateur_dao.updateNouvellesNotificationsUtilisateurWithValue(
					id_utilisateur,
					value,
					function (retour_sql) {
							callback();
					},
					callback
				);
	    },
	    two: function(callback){
	    	pairform_dao.utilisateur_dao.updateAllNotificationsUtilisateurWithValue(
					id_utilisateur,
					value,
					function (retour_sql) {
							callback();
					},
					callback
				);
	    },
	},
	function(err, results) {
	    // results now equals: {one: 1, two: 2}
	  if (err) {
			callback({
				"status" : "ko",
				"error" : err
			});
	  }
	  else {
	  	callback({
				"status" : "ok"
			});
	  }
	});
};


// Fonction dépréciée
//TODO : revoir la fonctionnalité 
exports.changerNomsReels = function(param, callback) {
	// function callbackError() {
	// 	callback(constantes.RETOUR_JSON_KO);
	// }
	// var id_utilisateur = param.id_utilisateur,
	// 		id_capsule = param.id_capsule;
		
	// pairform_dao.getConnexionBDD(function (_connexion_courante) {
	// 	pairform_dao.utilisateur_dao.selectVraieCategorieCapsule(
	// 		id_utilisateur,
	// 		id_capsule,
	// 		function (retour_sql) {

	// 		},
	// 		callbackError
	// 	);
	// });
	
	callbackError();
};


//changerRole.php
exports.changerRole = function(param, callback) {
	var id_utilisateur_cible = param.id_utilisateur_cible, 
		id_ressource = param.id_ressource, 
		id_categorie_temp = param.id_categorie_temp;

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.updateCategorieTempUtilisateur(
			id_utilisateur_cible,
			id_ressource,
			id_categorie_temp,
			function (retour_sql) {
				pairform_dao.utilisateur_dao.selectCategorieRessource(
					id_utilisateur_cible,
					id_ressource,
					function (retour_sql) {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						callback({"status" : constantes.STATUS_JSON_OK,
								"data" : retour_sql[0]});
						api_rs.changerCategorie(id_utilisateur_cible, retour_sql[0]["nom_categorie"], id_ressource);
					},
					callbackError
				);
			},
			callbackError
		);
		
	});
};

// Vérification du role de l'utilisateur dans une ressource, sans check des paramètres d'une capsule
exports.verifierCategorieRessource = function(param, callback_final) {
	var id_utilisateur = param.id_utilisateur, 
		id_ressource = param.id_ressource;

	//IMPORTANT : pour détecter les éventuels conflits de session
	//Si l'id utilisateur sur le serveur est différent de celui sur le navigateur, 
	//il fermera la session
	var retour_json_final = {id_utilisateur:id_utilisateur};

	function callbackError(err) {
		callback_final({status:"ko", err : err});
	}

	pairform_dao.utilisateur_dao.selectCategorieRessource(id_utilisateur, id_ressource, function (categorie_ressource) {
		//Si l'utilisateur a déjà un rôle
		if (categorie_ressource.length) {

			retour_json_final.status = constantes.STATUS_JSON_OK;
			retour_json_final.categorie_ressource = categorie_ressource[0];
			callback_final(retour_json_final);
		}
		else{
			retour_json_final.status = constantes.STATUS_JSON_UPDATE;
			//Insertion du rôle par défaut
			pairform_dao.utilisateur_dao.insertCategorieUtilisateur(id_utilisateur, id_ressource, constantes.CATEGORIE_LECTEUR, false, function (categorie_ressource) {
				//Flag pour indiquer qu'il faut updater au niveau client
				retour_json_final.categorie_ressource = {score: 0, nom_categorie: "Lecteur", id_categorie: 0};

				callback_final(retour_json_final);
				//Trigger du succès qui va bien
				api_rs.visiterRessource(id_utilisateur);
			}, 
			callbackError);
		}
	}, 
	callbackError);
}
//Verification que l'utilisateur a bien un rôle (implique qu'il a déjà visité la ressource)
exports.verifierCategorie = function(param, callback_final) {
	var id_utilisateur = param.id_utilisateur, 
		id_ressource = param.id_ressource, 
		id_capsule = param.id_capsule;

	function callbackError() {
		callback_final(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.selectParametres(
			id_utilisateur,
			id_capsule,
			function (utilisateur_parametres) {
				var retour_json_final = {"id_utilisateur" : id_utilisateur};

				//Si l'utilisateur a les paramêtres configurés
				if (utilisateur_parametres.length) {
					retour_json_final.status = constantes.STATUS_JSON_OK;
					pairform_dao.libererConnexionBDD(_connexion_courante);
					callback_final(retour_json_final);
				}
				//Sinon, on va les créer
				else{

					retour_json_final.status = constantes.STATUS_JSON_UPDATE;
					//Insertion des parametres par défaut
					pairform_dao.utilisateur_dao.insertParametres(id_utilisateur, id_capsule, function (utilisateur_parametres) {

						//Flag pour indiquer qu'il faut updater au niveau client
						retour_json_final.utilisateur_parametres = false;
						
						//Recupération du rôle de l'utilisateur, pour être sûr
						pairform_dao.utilisateur_dao.selectCategorieRessource(id_utilisateur, id_ressource, function (categorie_ressource) {

							//Si l'utilisateur a déjà un rôle
							if (categorie_ressource.length) {
								//Flag pour indiquer qu'il faut updater au niveau client
								retour_json_final.categorie_ressource = categorie_ressource[0];
								pairform_dao.libererConnexionBDD(_connexion_courante);
								callback_final(retour_json_final);
							}
							else{
								//Insertion du rôle par défaut
								pairform_dao.utilisateur_dao.insertCategorieUtilisateur(id_utilisateur, id_ressource, constantes.CATEGORIE_LECTEUR, false, function (categorie_ressource) {
									
									//Flag pour indiquer qu'il faut updater au niveau client
									retour_json_final.categorie_ressource = false;
									pairform_dao.libererConnexionBDD(_connexion_courante);
									callback_final(retour_json_final);
									//Trigger du succès qui va bien
									api_rs.visiterRessource(id_utilisateur);
								}, 
								callbackError);
							}
						}, 
						callbackError);
					}, 
					callbackError);
				}
			},
			callbackError
		);
		
	});
};

//Verification que l'utilisateur a bien un rôle (implique qu'il a déjà visité la ressource)
exports.getPreferences = function(param, callback_final) {
	var id_utilisateur = param.id_utilisateur,
		id_capsule = param.id_capsule;

	function callbackError() {
		callback_final(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.selectParametres(
			id_utilisateur,
			id_capsule,
			function (utilisateur_parametres) {
				// var retour_json_final = {'status': constantes.STATUT_JSON_OK};
				var retour_json_final = {"status" : constantes.STATUS_JSON_OK};

				//Si l'utilisateur a les paramêtres configurés
				if (utilisateur_parametres.length) {
					retour_json_final.preferences = utilisateur_parametres[0];
					pairform_dao.libererConnexionBDD(_connexion_courante);
					callback_final(retour_json_final);
				}
				else
					callbackError();
			},
			callbackError
		);
	});
};


// donner un openbadge à un utilisateur
exports.donnerOpenBadge = function(liste_parametres, callback) {
	var id_utilisateur_cible = liste_parametres.id_utilisateur_cible,
				id_openbadge = liste_parametres.id_openbadge;

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {

		pairform_dao.openbadge_dao.selectOpenBadgeOfUtilisateur(
			id_openbadge,
			id_utilisateur_cible,
			function (retour_sql) {
				//Si l'utilisateur l'a déjà, on sort
				if (retour_sql.length !== 0){
					pairform_dao.libererConnexionBDD(_connexion_courante);
					
					return callback({
						status: "ko",
						message: "ws_badge_deja_gagne"
					});
				}
				//Si l'utilisateur ne l'a pas encore
				pairform_dao.openbadge_dao.insertOpenBadgeUtilisateur(
					id_openbadge,
					id_utilisateur_cible,
					function () {
						callback(constantes.RETOUR_JSON_OK);

						//Trigger de la notification
						pairform_dao.openbadge_dao.selectOpenBadgeById(id_openbadge, function (retour_sql) {
							pairform_dao.libererConnexionBDD(_connexion_courante);

							var badge = retour_sql[0];
							api_rs.donnerBadge(id_utilisateur_cible, badge.nom);
							
						})

					},
					callbackError
				);
			},
		callbackError);
		
	});
};


// Récupérer les openbadges que l'utilisateur connceté peut attribuer
exports.getOpenBadgesAttribuables = function(liste_parametres, callback) {
	var id_utilisateur = liste_parametres.id_utilisateur;	

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.openbadge_dao.selectOpenBadgesAttribuablesByUtilisateur(
			id_utilisateur,
			function (retour_sql) {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				log.debug(retour_sql);
				if (retour_sql.length) {
					callback({
						status : "ok",
						data : retour_sql
					});
				}
				else
					callback(constantes.RETOUR_JSON_KO);
			},
			function callbackError() {
				callback(constantes.RETOUR_JSON_KO);
			}
		);
		
	});
};

exports.supprimerCompte = function(param, callback) {

	var id_utilisateur = param.id_utilisateur;

	async.waterfall([
	  function resetAvatar(next){
			//Génération d'une image par défaut avec gravatar		
			var avatar_path_fs = constantes.REPERTOIRE_AVATARS + id_utilisateur +".png";
			constantes.creerImageAvecDimension(constantes.COMPTE_SUPPRIME_AVATAR, avatar_path_fs, 100, true, next)  
	  },
	  function resetPassword(url, next){
			// log.debug("url : ", url)
			//Création d'un password par défaut
			var salt = bCrypt.genSaltSync(),
					rand = Math.random().toString(36).substring(2),
					password = crypto.createHash('md5').update(rand).digest("hex");
			// log.debug("salt : ", salt)
			// log.debug("rand : ", rand)
			// log.debug("password : ", password)
			bCrypt.hash(password, salt, null, next);
	  },
	  function anonymisationUtilisateur(hash, next){
	  	// log.debug("hash : ", hash);
			pairform_dao.utilisateur_dao.updateUtilisateurAnonymisation(
				id_utilisateur,
				hash,
				function (retour_sql) {
					log.debug(retour_sql);
					next(null, {
						"status" : "ok"
					});
				},
				next
			);
	  }
	], function (err, result) {
	  if (err) {
	  	log.error(err);
	  	callback(constantes.RETOUR_JSON_KO, {msg : err});
	  }
	  else{
	  	callback(result);
	  }
	});

};
