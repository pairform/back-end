"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		 request = require("request"),
		   async = require("async");


// changerVueNotification.php
exports.changerVueNotification = function(param, callback) {
	var id_notifications = param.id_notifications || [0];
	
	//Si on est sur mobile, c'est un tableau JSON qui arrive
	if (param.os != "web") {
		id_notifications = JSON.parse(id_notifications);
	}
	
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	if (id_notifications.length) {
		pairform_dao.notification_dao.updateNotificationsVues(
			id_notifications,
			function () {
				callback(constantes.RETOUR_JSON_OK);
			},
			callbackError
		);
	}
	else
		callback(constantes.RETOUR_JSON_OK);

};

exports.changerVueNotificationType = function(id_utilisateur, type, callback) {
	
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.notification_dao.updateNotificationsVuesUtilisateurType(
		id_utilisateur,
		type,
		function () {
			callback(constantes.RETOUR_JSON_OK);
		},
		callbackError
	);

};


// recupererNotifications.php
exports.recuperer = function(param, callback) {
	var id_utilisateur = param.id_utilisateur || -1000, 
		id_capsule = param.id_capsule  || null;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		async.parallel({
			"un" : function selectUserNotifications (done) {
				pairform_dao.notification_dao.selectNotifications(
					"un",
					id_utilisateur,
					function (un_notifications) {
						if (un_notifications.length)
							done(null, un_notifications);
						else								
							done(null);
					},
					done
				);

			},
			"count_un" : function selectUserNotificationsCount (done) {
				pairform_dao.notification_dao.selectNotificationsCount(
					"un",
					id_utilisateur,
					function (count_un) {
						if (count_un.length)
							done(null, count_un[0]['count']);
						else								
							done(null);
					},
					done
				);

			},
			"us" : function selectUserScore (done) {
				pairform_dao.notification_dao.selectNotifications(
					"us",
					id_utilisateur,
					function (us_notifications) {
						if (us_notifications.length){
							for (var i = us_notifications.length - 1; i >= 0; i--) {
								us_notifications[i].points = constantes[us_notifications[i].type_points];
							}
							done(null, us_notifications);
						}
						else								
							done(null);
					},
					done
				);
			},
			"count_us" : function selectUserScoreCount (done) {
				pairform_dao.notification_dao.selectNotificationsCount(
					"us",
					id_utilisateur,
					function (count_us) {
						if (count_us.length)
							done(null, count_us[0]['count']);
						else								
							done(null);
					},
					done
				);

			},
			"score" : function selectUserScoreCapsule (done) {
				//Si on a passé en parametre une capsule, c'est que le client veut une mise à jour du score pour cette capsule
				if (id_capsule){
					pairform_dao.utilisateur_dao.selectScoreCapsule(
						id_utilisateur,
						id_capsule,
						function (score) {
							if (score.length)
								done(null, score[0]['score']);
							else								
								done(null);
						},
						done
					);
				}
				else{
					done(null);
				}

			}

		}, 
		function callback_final (err, results) {
			pairform_dao.libererConnexionBDD(_connexion_courante);

			if (err) {
				return callback(constantes.RETOUR_JSON_KO);
			}
			else {
				var retour_json_final = results;
				retour_json_final.status = "ok";

				callback(retour_json_final);
			}
		});
	});
};