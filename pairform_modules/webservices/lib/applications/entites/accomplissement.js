"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		 request = require("request");

// listeSucces.php
exports.liste = function(param, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectSuccesByUtilisateur(
			param.id_utilisateur,
			param.id_langue || 3,
			function (liste_succes) {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				// retour_json.datas.succes = liste_succes;
				// retour_json.datas.nb_succes = liste_succes.length;
				callback({
					"status" : "ok",
					"data" : liste_succes
				});
			},
			callbackError
		);
	});

};

