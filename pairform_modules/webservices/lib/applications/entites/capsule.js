"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes");


// vérifie si une capsule a une visibilité "Ouvert" pour un utilisateur via son email
exports.getAutorisationAcces = function(id_capsule, email_utilisateur, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.capsule_dao.selectCapsuleAccessibleByEmail(
			id_capsule,
			email_utilisateur,
			function callbackSuccess (autorisation) {
				pairform_dao.libererConnexionBDD(_connexion_courante);
				callback(autorisation);
			},
			callbackError
		);
	});
};


exports.rediriger = function(param, callback) {
	function callbackError() {
		log.error("Redirection capsule, id inexistant : " + param.id_capsule);
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_capsule = parseInt(param.id_capsule); // Parse pour être sûr d'avoir un entier en entrée
	
	// Si l'id demandé n'est pas valide
	if (!id_capsule)
		return callbackError();

	pairform_dao.capsule_dao.selectCapsulePourRedirection(id_capsule, function callback_succes (capsule) {					
		callback({
			"status" : "ok",
			"url" : capsule.url_web
		});
	}, callbackError);
}
