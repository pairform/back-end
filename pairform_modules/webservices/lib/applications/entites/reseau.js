"use strict";

var pairform_dao = require("pairform_dao"),
		constantes = require("../../constantes"),
			api_rs = require("../api_rs"),
			log = require("metalogger")(),
		 request = require("request"),
		 async = require("async");


exports.liste = function(param, callback) {

	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	//Helper pour synchroniser les appels
	//http://stackoverflow.com/a/4631909/1437016
	function fork (async_calls, shared_callback) {
		var counter = async_calls.length;
		var all_results = [];
		function makeCallback (index) {
			return function () {
				counter --;
				var results = [];
				// we use the arguments object here because some callbacks 
				// in Node pass in multiple arguments as result.
				for (var i=0;i<arguments.length;i++) {
					results.push(arguments[i]);
				}
				all_results[index] = results;
				if (counter == 0) {
					shared_callback(all_results);
				}
			}
		}

		for (var i=0;i<async_calls.length;i++) {
			async_calls[i](makeCallback(i));
		}
	}
	//Singleton de connexion au pool
	var _connexion_courante;

	//Variables de retour
	var cercles = [],
		classes = [],
		classesRejointes = [];

	//Variable récupérée de la requête
	var id_utilisateur = param.id_utilisateur;

	pairform_dao.getConnexionBDD(function (connexion) {
		_connexion_courante = connexion;
		// fork([selectReseauxCreeParUtilisateur], callbackFinal);

		async.parallel({
			capsulesEtClasses : function (callback_parallel) {
				pairform_dao.reseau_dao.selectReseauxCreeParUtilisateur(id_utilisateur, function (retour_sql) {
					//S'il n'y a pas de cercles ni classes, on va chercher les classes qu'il a possiblement rejointes
					if (retour_sql.length == 0)
						callback_parallel();
					else{
						var i = 0;
						//On construit les fonctions à forker
						async.whilst(
						    function () { return i < retour_sql.length; },
						    function (callback_whilst) {
								recuperationUtilisateurs(retour_sql[i], callback_whilst);
						        i++;
						    },
						    function (err) {
						        callback_parallel();
						    }
						);
					}
				}, callback_parallel);
			},
			classesRejointes : function (callback_parallel) {
				pairform_dao.reseau_dao.selectReseauxAppartientUtilisateur(id_utilisateur, function (retour_sql) {
					//Si ya rien non plus, on se casse
					if (retour_sql.length == 0)
						callback_parallel();
					else{
						var i = 0;
						//On construit les fonctions à forker
						async.whilst(
						    function () { return i < retour_sql.length; },
						    function (callback_whilst) {
								retour_sql[i].classe_rejointe_flag = true;
								recuperationUtilisateurs(retour_sql[i], callback_whilst);
						        i++;
						    },
						    function (err) {
						        callback_parallel();
						    }
						);
					}
				}, callback_parallel);
			}
		},
		function (err, results) {
			if(!err) {
				pairform_dao.libererConnexionBDD(_connexion_courante);

				callback({
					"status" : "ok",
					"cercles" : cercles,
					"classes" : classes,
					"classesRejointes" : classesRejointes
				});
			}
			else{
				callbackError();
			}
		});
	});
	// function selectReseauxCreeParUtilisateur (callback) {
	// 	pairform_dao.reseau_dao.selectReseauxCreeParUtilisateur(id_utilisateur, function (retour_sql) {
			
	// 		//S'il n'y a pas de cercles ni classes, on va chercher les classes qu'il a possiblement rejointes
	// 		if (retour_sql.length == 0)
	// 			selectReseauxAppartientUtilisateur();
	// 		else{			
	// 			//On construit les fonctions à forker
	// 			var forks = [];
	// 			for (var i = retour_sql.length - 1; i >= 0; i--) {
	// 				var reseau = retour_sql[i];
	// 				reseau.members = [];

	// 				forks.push(function (reseau, callback) {
	// 					recuperationUtilisateurs(reseau, callback);
	// 				});
	// 			};
	// 			fork(forks, callback);
	// 		}
	// 	}, callbackError);
	// }

	// function selectReseauxAppartientUtilisateur (callback) {
	// 	pairform_dao.reseau_dao.selectReseauxAppartientUtilisateur(id_utilisateur, function (retour_sql) {

	// 		//S'il n'y a pas de cercles ni classes, on va chercher les classes qu'il a possiblement rejointes
	// 		if (retour_sql.length == 0)
	// 			callbackFinal();
	// 		else{			
	// 			//On construit les fonctions à forker
	// 			var forks = [];
					
	// 			for (var i = retour_sql.length - 1; i >= 0; i--) {
	// 				var reseau = retour_sql[i];
	// 				reseau.members = [];
	// 				reseau.classe_rejointe_flag = true;
	// 				forks.push(function (reseau, callback) {
	// 					recuperationUtilisateurs(reseau, callback);
	// 				});
	// 				// (function recuperationUtilisateurs (_classe_rejointe) {
	// 				// 	pairform_dao.reseau_dao.selectUtilisateursDansReseau(_classe_rejointe.id_reseau, function (retour_sql) {			
	// 				// 		_classe_rejointe.members.push(retour_sql);
	// 				// 		//Dernier cycle, on enchaine
	// 				// 		if (i == -1) {	
	// 				// 			callback();
	// 				// 		}
	// 				// 	}, callbackError);
	// 				// })(classe_rejointe);
	// 			}
	// 			fork(forks, callback);
	// 		}
	// 	}, callbackError);
	// }

	function recuperationUtilisateurs (reseau, callback) {
		pairform_dao.reseau_dao.selectUtilisateursDansReseau(reseau.id_collection, function (retour_sql) {	
			reseau.profils = retour_sql;
			//S'il y a une clé, c'est une classe
			if(reseau.classe_rejointe_flag)
				classesRejointes.push(reseau);
			else if (reseau.cle)
				classes.push(reseau);
			else 
				cercles.push(reseau);
			//Dernier cycle, on enchaine
			callback();
		}, callbackError);
	}
};

exports.ajouterMembre = function(param, callback) {
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	var id_utilisateur = param.id_utilisateur,
		pseudo_utilisateur = param.pseudo_utilisateur || param.username,
		id_utilisateur_concerne = param.id_utilisateur_concerne,
		id_reseau = param.id_reseau || param.id_collection;
	
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.reseau_dao.insertUtilisateurDansReseau(id_utilisateur_concerne, id_reseau, function callback_success (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			callback({
				"status" : "ok"
			});
			
			//Envoi de la notification d'ajout à un réseau à l'utilisateur concerné
			api_rs.ajouterReseau(id_utilisateur, pseudo_utilisateur, id_utilisateur_concerne);

		}, function callback_error (erreur) {
			//Duplicate key
			//{"code":"ER_DUP_ENTRY","errno":1062,"sqlState":"23000","index":0}
			if (erreur.errno == 1062) {
				callback({
					"status" : "ko",
					"message" : "ws_erreur_ajout_reseau_deja_existant"
				});
			}
			//Foreign key fail ?
			//{"code":"ER_NO_REFERENCED_ROW_","errno":1452,"sqlState":"23000","index":0}
			else{
				callback({
					"status" : "ko",
					"message" : "ws_erreur_ajout_reseau"
				});
			}
		});
	});
		
};

exports.creerReseau = function(param, callback) {
	// var id_utilisateur = param.id_utilisateur;
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	function randomString( len ) {
	  var str = "";
	  for(var i=0; i<len; i++){
	    var rand = Math.floor( Math.random() * 62 );
	    var charCode = rand+= rand>9? (rand<36?55:61) : 48;
	    str += String.fromCharCode( charCode );
	  }
	  return str;
	}

	
	var id_utilisateur = param.id_utilisateur,
		nom = param.nom,
		//Si on est dans le cas d'une classe, on a flaggé avec un booléen au niveau de routeApplication
		//On génère une clé pour l'inserer dans la base de données derrière
		cle = param.reseau_est_classe ? randomString(5) : "";
	
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.reseau_dao.insertReseau(id_utilisateur, nom, cle, function callback_success (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var id_collection = retour_sql.insertId,
				retour = {
					"status" : "ok",
					"id_collection" : id_collection
				};

			//Si on est dans le cas d'une classe, on a flaggé avec un booléen au niveau de routeApplication
			//Et on check qu'on a bien une clé en même temps
			if (param.reseau_est_classe && cle)
				//On la rajoute au retour client
				retour.cle = cle;

			callback(retour);
			
			//Envoi de la notification d'ajout à un réseau à l'utilisateur concerné
			api_rs.creerReseau(id_utilisateur);

		}, function callback_error (erreur) {
			//Duplicate key
			//{"code":"ER_DUP_ENTRY","errno":1062,"sqlState":"23000","index":0}
			//TODO : internationaliesr le retour de message
			if (erreur.errno == 1062) {
				callback({
					"status" : "ko",
					"message" : "nom_deja_utilise"
				});
				//TODO : attention, pas localisé sur iPhone / Android
			}
			//Foreign key fail ?
			//{"code":"ER_NO_REFERENCED_ROW_","errno":1452,"sqlState":"23000","index":0}
			else{
				callback({
					"status" : "ko",
					"message" : "ws_erreur_ajout_reseau"
				});
			}
		});
	});
};

exports.rejoindreClasse = function(param, callback) {
	
	var id_utilisateur = param.id_utilisateur,
		pseudo_utilisateur = param.pseudo_utilisateur || param.username,
		cle = param.cle;


	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.reseau_dao.selectReseauxParCle(cle, function callback_success (retour_sql) {
			//Si la classe n'a pas été trouvée
			if(retour_sql.length == 0) {
				callback({
					"status" : "ko",
					"message" : "cle_invalide"
				});
			}
			else{
				var id_createur_classe = retour_sql[0].id_utilisateur,
					id_reseau = retour_sql[0].id_reseau,
					nom = retour_sql[0].nom;

				//Si l'utilisateur essaie de rejoindre sa propre classe
				if (id_utilisateur == id_createur_classe) {
					callback({
						"status" : "ko",
						"message" : "utilisateur_deja_membre"
					});
				}
				//Sinon, on continue, on attaque l'insertion dans la classe
				else{
					//
					pairform_dao.reseau_dao.insertUtilisateurDansReseau(id_utilisateur, id_reseau, function callback_success (retour_sql) {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						
						callback({
							"status" : "ok",
							"id_collection" : id_reseau,
							"nom" : nom,
							"cle" : cle
						});
						
						//Envoi de la notification de jonction à une classe au créateur de la classe
						api_rs.rejoindreClasse(id_utilisateur, pseudo_utilisateur, id_createur_classe);

					}, function callback_error (erreur) {
						//Duplicate key
						//{"code":"ER_DUP_ENTRY","errno":1062,"sqlState":"23000","index":0}
						if (erreur.errno == 1062) {
							callback({
								"status" : "ko",
								"message" : "utilisateur_deja_membre"
							});
						}
						//Foreign key fail ?
						//{"code":"ER_NO_REFERENCED_ROW_","errno":1452,"sqlState":"23000","index":0}
						else{
							callback({
								"status" : "ko",
								"message" : "ws_erreur_contacter_pairform"
							});
						}
					});
				}
			}
		});
	});


};
// supprimerCercle.php
exports.supprimerCercle = function(param, callback) {
	function callbackError() {
		callback({
			"status" : "ko",
			"message" : "ws_erreur_contacter_pairform"
		});
	}

	var id_reseau = param.id_reseau || param.id_collection;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.reseau_dao.deleteReseau(id_reseau, function callback_success (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			log.debug(JSON.stringify(retour_sql));
			callback({
				"status" : "ok",
			});
		}, callbackError);
	});
};
// supprimerMembre.php
exports.supprimerMembre = function(param, callback) {
	function callbackError() {
		callback({
			"status" : "ko",
			"message" : "ws_erreur_contacter_pairform"
		});
	}

	var id_reseau = param.id_reseau || param.id_collection,
		id_utilisateur_concerne = param.id_utilisateur_concerne;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.reseau_dao.deleteUtilisateurDeReseau(id_reseau, id_utilisateur_concerne, function callback_success (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			
			callback({
				"status" : "ok",
			});
		}, callbackError);
	});
};