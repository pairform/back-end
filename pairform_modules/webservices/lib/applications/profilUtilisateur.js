"use strict";

var pairform_dao = require("pairform_dao"),
		   async = require("async"),
		     log = require("metalogger")(),
	  constantes = require("../constantes");


// affiche les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)
exports.getProfilUtilisateur = function(parametres, callback) {

	var id_utilisateur = parametres.id_utilisateur,
		id_langue = parametres.id_langue ? parametres.id_langue : 3;


	async.parallel({
		// Récupère les infos sur le profil de l'utilisateur (pseudo, avatar, email, etablissement, langue principale, etc.)
		profil : function selectProfilUtilisateur(callback_parallel) {
			pairform_dao.utilisateur_dao.selectProfilUtilisateur(
				id_utilisateur,
				function (retour_sql) {
					if (retour_sql){
						//Si l'utilisateur est supprimé, on s'arrête direct
						if (retour_sql.est_banni == 1){
							callback_parallel({"message" :"ws_utilisateur_supprime"});
						}
						else
							callback_parallel(null, retour_sql);
					}
					else
						callback_parallel({"message" : "ws_utilisateur_inexistant"});
				},
				callback_parallel
			);
		},

		// Liste des ressources consultées par l'utilisateur
		ressources : function selectRessourcesByUtilisateur(callback_parallel) {
			pairform_dao.ressource_dao.selectRessourcesByUtilisateur(
				id_utilisateur,
				function (liste_ressources) {
					if (liste_ressources)
						callback_parallel(null, liste_ressources);
					else
						callback_parallel({"message" : "ws_utilisateur_inexistant"});
				},
				callback_parallel
			);
		},

		// Tableau de tous les succes
		succes : function selectSuccesByUtilisateur(callback_parallel) {
			pairform_dao.succes_dao.selectSuccesByUtilisateur(
				id_utilisateur,
				id_langue,
				function (liste_succes) {
					if (liste_succes)
						callback_parallel(null, liste_succes);
					else
						callback_parallel({"message" : "ws_utilisateur_inexistant"});
				},
				callback_parallel
			);
		},

		// Récupère les 5 derniers succès gagnés par l'utilisateur
		derniers_succes : function selectDernierSuccesGagnesByUtilisateur(callback_parallel) {
			pairform_dao.succes_dao.selectDernierSuccesGagnesByUtilisateur(
				id_utilisateur,
				function (liste_derniers_succes_gagnes) {
					if (liste_derniers_succes_gagnes)
						callback_parallel(null, liste_derniers_succes_gagnes);
					else
						callback_parallel({"message" : "ws_utilisateur_inexistant"});
				},
				callback_parallel
			);
		},

		// Tableau de tous les openbadges gagnés par l'utilisateur
		openbadges : function selectOpenBadgesGagnesByUtilisateur(callback_parallel) {
			pairform_dao.openbadge_dao.selectOpenBadgesGagnesByUtilisateur(
				id_utilisateur,
				function (liste_openbadges) {
					if (liste_openbadges)
						callback_parallel(null, liste_openbadges);
					else
						callback_parallel({"message" : "ws_utilisateur_inexistant"});
				},
				callback_parallel
			);
		}
	},
	function callback_final (err, results) {
		if (err) {
			callback({
				status: constantes.STATUS_JSON_KO,
				// Erreur custom (pas d'entrée SQL par exemple)
				message: err.message
			});
		}
		else{

			// On rajoute les clés récupérées pour le profil 
			// On double la clé, pour les clients utilisant encore username
			results.profil.username = results.profil.pseudo;
			// Liste des ressources consultées par l'utilisateur
			results.profil.ressources = results.ressources;
			// Tableau de tous les succes
			results.profil.succes = results.succes;
			// Nombre total de succès total
			results.profil.nb_succes = results.succes.length;
			// Nombre total de succès gagnés
			results.profil.nb_succes_gagnes = results.derniers_succes.length;
			// On récupère seulement les 5 derniers succès gagnés (ou moins si l'utilisateur à moins de 5 succès gagnés)
			results.profil.derniers_succes = results.derniers_succes.slice(0, Math.min(results.derniers_succes.length, 5));
			// Tableau de tous les openbadges
			results.profil.openbadges = results.openbadges;
			// Nombre total de openbadges total
			results.profil.nb_openbadges = results.openbadges.length;

			var retour_json = {
				status : constantes.STATUS_JSON_OK,
				datas : results.profil
			};

			callback(retour_json);
		}
	})
};
