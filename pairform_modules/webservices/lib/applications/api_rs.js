"use strict";

var pairform_dao = require("pairform_dao"),
			 log = require('metalogger')(),
	  constantes = require("../constantes"),
	  		 apn = require('apn'),
	  	 request = require('request'),
	  	   async = require('async'),
	  	  CONFIG = require('config');

var options = { 
	"cert" : __dirname + "/../../../../" + CONFIG.app.push.cert,
	"key" : __dirname + "/../../../../" + CONFIG.app.push.key,
	"passphrase" : CONFIG.app.push.passphrase
};

var apnConnection = new apn.Connection(options);
	/*
	* Renvoie les notifications de l'utilisateur connecté
	*
	* Paramètre :
	* (optionnel) [web] id_capsule (int) : id de la capsule en cours, pour récupération du score
	*
	* Retour : 
	* un -> user notifications (notifications concernants l'utilisateur)
	*   id_notification (int) : Id de la notification
	*   id_message (int) : Id du message concerné par la notification - peut être null dans le cas d'une notif ne concernant pas un message en particulier
	*   id_utilisateur (int) : Id de l'utilisateur concerné par la notification - peut être null dans le cas d'une notif broadcastée
	*   titre (string) : Titre principal de la notification (non internationalisé)
	*   sous_type (string) : Sous type de notification 
	*     dc : défi créé 
	*       - Cible : broadcast de la notification (pas d'utilisateur cible)
	*       - Contenu associé : Nom court de la ressource concernée
	*       - Action : Renvoyer vers le message
	*     dt : défi terminé 
	*       - Cible : broadcast de la notification (pas d'utilisateur cible)
	*       - Contenu associé : Nom court de la ressource concernée
	*       - Action : Renvoyer vers le message
	*     dv : défi validé 
	*       - Cible : broadcast de la notification (pas d'utilisateur cible)
	*       - Contenu associé : Nom court de la ressource concernée
	*       - Action : Renvoyer vers le message
	*     ar : ajout réseau
	*       - Cible : Utilisateur ajouté au réseau de quelqu'un
	*       - Contenu associé : ID de l'utilisateur qui a fait l'action
	*       - Action : Renvoyer vers le profil utilisateur
	*     cr : classe rejointe
	*       - Cible : Utilisateur qui a créé la classe
	*       - Contenu associé : ID de l'utilisateur qui a fait l'action
	*       - Action : Renvoyer vers le profil utilisateur
	*     ru : réponse d'utilisateur
	*       - Cible : Auteur du post original
	*       - Contenu associé : Nom court de la ressource concernée
	*       - Action : Renvoyer vers le message
	*     gm : gagné médaille
	*       - Cible : Utilisateur ayant gagné la médaille
	*       - Contenu associé : Nom court de la ressource concernée
	*       - Action : Renvoyer vers le message
	*     gs : gagné succès
	*       - Cible : Utilisateur ayant gagné le succès
	*       - Contenu associé : Nom du succès internationalisé
	*       - Action : Renvoyer vers le profil
	*     gb : gagné badge
	*       - Cible : Utilisateur ayant gagné le badge
	*       - Contenu associé : Nom du badge
	*       - Action : Renvoyer vers le profil
	*     cc : changé catégorie
	*       - Cible : Utilisateur ayant vu son rôle changé par un expert
	*       - Contenu associé : Nom court de la ressource concernée
	*       - Action : Renvoyer vers le profil
	*     da : document ajouté
	*       - Cible : Utilisateur ayant droit de modification sur ce document, via une ressource
	*       - Contenu associé : Nom court du document concerné
	*       - Action : Renvoyer vers le document
	*     du : document updaté
	*       - Cible : Utilisateur ayant droit de modification sur ce document, via une ressource
	*       - Contenu associé : Nom court du document concerné
	*       - Action : Renvoyer vers le document
	*     nan : défaut (à gérer)
	*       - Cible : Utilisateur concerné
	*       - Contenu associé : Nom court de la ressource concernée
	*       - Action : Renvoyer vers le message
	*   contenu (string) : Dépend du sub-type 
	*   date_creation (int - timestamp) : Timestamp de création
	*   date_vue (int - timestamp) : Timestamp de vision (peut être null si pas encore vu)
	*   image (string) : Nom de l'image (sans extension), déterminé en fonction du sous type
	*   label (string) : Sous-titre, déterminé en fonction du sous type
	*
	* us -> user score (notifications concernants les points / succès gagnés par l'utilisateur)
	* 
	*   id_notification (int) : Id de la notification
	*   id_message (int) : Id du message concerné par la notification - peut être null dans le cas d'une notif ne concernant pas un message en particulier
	*   titre (string) : Titre principal de la notification (non internationalisé)
	*   label (string) : Nom court de la ressource concernée
	*   points (string) : Nombre de points (négatif ou positif), récupéré dans la base cape_definitions_points à partir de la constante
	*   contenu (string) : Dépend du sub-type 
	*   date_creation (int - timestamp) : Timestamp de création
	*   date_vue (int - timestamp) : Timestamp de vision (peut être null si pas encore vu)
	* 
	* score : (int) Score de l'utilisateur pour une capsule donnée
	*/

/*
*
*
*		Helpers
*
*
*/

// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
function callbackError() {
	// callback(constantes.RETOUR_JSON_KO);
	console.trace();
	log.error("############################### Erreur dans api_rs.js");
}

function capitaliseFirstLetter(str) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}

function recupererMessage (id_utilisateur, id_message, callback) {
	pairform_dao.message_dao.selectMessageFromId(id_utilisateur, id_message, function callback_succes (retour_sql) {
		if (retour_sql.length)
			callback(retour_sql[0]);
		else
			callbackError();
	}, callbackError);
}

/*
*
*
*		Exports d'actions
*
*
*/

exports.ecrireMessage = function (id_utilisateur, id_message) {
	recupererMessage(id_utilisateur, id_message, function (message) {
		majScore(message.id_ressource, constantes.PTSECRIREMESSAGE, id_utilisateur);
		insererNotification(id_utilisateur, "us", "Message posté", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" : "PTSECRIREMESSAGE"}, false);
		// postWebhook(message);
		if (message.est_defi != 0)
			insererNotification(0, "un", "Un nouveau défi a été créé!", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "su" : "dc"});

		if (message.id_message_parent != 0) {
			//Récuperation du message parent pour envoyer une notification a son auteur
			recupererMessage(id_utilisateur, message.id_message_parent, function (message_parent) {
				insererNotification(message_parent.id_auteur, "un", "Réponse de " + capitaliseFirstLetter(message.pseudo_auteur), {"c" : message_parent.id_capsule, "im" : id_message, "su" : "ru"});
				
				gagnerSuccesRepondre(id_utilisateur);

				if (message_parent.est_defi)
					gagnerSuccesParticiperDefi(id_utilisateur);

				gagnerSuccesRecevoirReponses(message_parent.id_auteur);
			});
		}
		gagnerSuccesMessage(id_utilisateur);
	});
}

exports.evaluerMessage = function (id_utilisateur, id_message, nouveau_vote, vote_precedent) {

	recupererMessage(id_utilisateur, id_message, function (message) {
		//Traitement de l'utilisateur actif (qui a voté)
		//On ne lui accorde les points que s'il n'a pas déjà voté ce message 
		if(typeof(vote_precedent) == "undefined"){
			// majScore(message.id_ressource, constantes.PTSEVALUERMESSAGE, id_utilisateur);
			// insererNotification(id_utilisateur, "us", "Vote sur un message", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" : "PTSVOTEPOSITIF"}, false);

			gagnerSuccesVote(id_utilisateur);
		}

		//Traitement de l'utilisateur passif (celui dont le message a été voté)

		//Annulation du vote
		if(nouveau_vote == vote_precedent)
		{
			if (nouveau_vote == 1){
				//Traitement de l'utilisateur passif (celui dont le message a été voté)
				majScore(message.id_ressource, -constantes.PTSVOTEPOSITIF, message.id_auteur);
				insererNotification(message.id_auteur, "us", "Vote d'un utilisateur annulé", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" :  "PTSVOTENEGATIF"});
			}
			else{
				majScore(message.id_ressource, -constantes.PTSVOTENEGATIF, message.id_auteur);
				insererNotification(message.id_auteur, "us", "Vote d'un utilisateur annulé", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" :  "PTSVOTEPOSITIF"});
			}
		}
		else
		{   
			//Nouveau vote  
			if(!vote_precedent || vote_precedent == 0)
			{
				if (nouveau_vote == 1){
					majScore(message.id_ressource, constantes.PTSVOTEPOSITIF, message.id_auteur);
					insererNotification(message.id_auteur, "us", "Vote d'un utilisateur", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" :  "PTSVOTEPOSITIF"});
				}
				else{
					majScore(message.id_ressource, constantes.PTSVOTENEGATIF, message.id_auteur);
					insererNotification(message.id_auteur, "us", "Vote d'un utilisateur", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" :  "PTSVOTENEGATIF"});
				}
			}
			//Changement de vote
			else
			{
				if (nouveau_vote == 1){
					majScore(message.id_ressource, constantes.PTSVOTEPOSITIF_INVERSION, message.id_auteur);
					insererNotification(message.id_auteur, "us", "Vote d'un utilisateur inversé", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" :  "PTSVOTEPOSITIF_INVERSION"});
				}
				else{
					majScore(message.id_ressource, constantes.PTSVOTENEGATIF_INVERSION, message.id_auteur);
					insererNotification(message.id_auteur, "us", "Vote d'un utilisateur inversé", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" :  "PTSVOTENEGATIF_INVERSION"});
				}
			}
		}

		gagnerSuccesUtiliteRecu(message.id_auteur);
	});
}

exports.supprimerMessage = function (id_utilisateur, id_message, supprime_par_precedent) {
	recupererMessage(id_utilisateur, id_message, function (message) {
		//Si le message vient d'être supprimé
		if (message.supprime_par != 0) {
			if (id_utilisateur == message.id_auteur) {
				majScore(message.id_ressource, constantes.PTSSUPPRIMERMESSAGE, id_utilisateur);
				insererNotification(message.id_auteur, "us", "Message supprimé", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" : "PTSSUPPRIMERMESSAGE"});
			}
			else{
				majScore(message.id_ressource, constantes.PTSCOMMENTAIRESUPPRIME, message.id_auteur);
				insererNotification(message.id_auteur, "us", "Message modéré", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" : "PTSCOMMENTAIRESUPPRIME"});
			}
		}
		else{
			//Si l'utilisateur avait lui-même supprimé son message
			if (message.id_auteur == supprime_par_precedent) {
				//On ne lui remet que les points qu'il s'était sucrés
				majScore(message.id_ressource, constantes.PTSECRIREMESSAGE, id_utilisateur);
				insererNotification(message.id_auteur, "us", "Message rétabli", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" : "PTSECRIREMESSAGE"});
			}
			//Sinon, on remet les 30 pts
			else{
				majScore(message.id_ressource, constantes.PTSCOMMENTAIREACTIVE, message.id_auteur);
				insererNotification(message.id_auteur, "us", "Message réactivé", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" : "PTSCOMMENTAIREACTIVE"});
			}
		}
	});
}



exports.donnerMedaille = function (id_utilisateur, id_message) {
	recupererMessage(id_utilisateur, id_message, function (message) {
		if (message.medaille != "") {
			insererNotification(message.id_auteur, "un", "Vous avez gagné une médaille", {"im" : id_message, "c" : "Medaille - " + message.medaille, "su" : "gm"});
			gagnerSuccesMedaille(message.id_auteur);
		}
	});
}

exports.validerReponseDefi = function (id_utilisateur, id_message) {
	recupererMessage(id_utilisateur, id_message, function (message) {
		if (message.defi_valide == 1) {
			majScore(message.id_ressource,constantes.PTSREUSSIDEFI, message.id_auteur);
			insererNotification(message.id_auteur, "us", "Vous avez gagné un défi", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" : "PTSREUSSIDEFI"});
		}
		else{
			majScore(message.id_ressource,constantes.PTSREUSSIDEFI_INVERSION, message.id_auteur);
			insererNotification(message.id_auteur, "us", "Vous n'avez plus gagné un défi", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "st" : "PTSREUSSIDEFI_INVERSION"}); 
		}
	});
}

exports.terminerDefi = function (id_utilisateur, id_message) {
	recupererMessage(id_utilisateur, id_message, function (message) {
		insererNotification(0, "un", "Un défi a été cloturé", {"im" : id_message, "c" : message.id_capsule || message.id_ressource, "su" : "dt"});
	});
}

//Actions complexes

exports.changerCategorie = function (id_utilisateur_cible, nom_categorie, id_ressource) {
	insererNotification(id_utilisateur_cible, "un", "Un expert vous a passé '"+nom_categorie+"'", {"c" : id_ressource, "su" : "cc"});
}

exports.creerReseau = function (id_utilisateur) {
	gagnerSuccesCercle(id_utilisateur);
}

exports.changerAvatar = function (id_utilisateur) {
	gagnerSuccesAvatar(id_utilisateur);
}

exports.visiterRessource = function (id_utilisateur) {
	gagnerSuccesRessource(id_utilisateur);
}

exports.ajouterReseau = function (id_utilisateur, utilisateur_connecte_username, id_utilisateur_ajoute) {
	insererNotification(id_utilisateur_ajoute, "un", capitaliseFirstLetter(utilisateur_connecte_username) + " vous a ajouté dans son réseau", {"c" : id_utilisateur, "su" : "ar"});
}

exports.rejoindreClasse = function (id_utilisateur, utilisateur_connecte_username, id_createur_classe) {
	insererNotification(id_createur_classe, "un", capitaliseFirstLetter(utilisateur_connecte_username) + " a rejoint votre classe", {"c" : id_utilisateur, "su" : "cr"});
	gagnerSuccesRejoindreClasse(id_utilisateur);
}

exports.donnerBadge = function (id_utilisateur, nom_badge) {
	insererNotification(id_utilisateur, "un", "Vous avez gagné un badge !", {"c" : nom_badge, "su" : "gb"});
}
/**
*     da : document ajouté
	*       - Cible : Utilisateur ayant droit de modification sur ce document, via une ressource
	*       - Contenu associé : Nom court du document concerné
	*       - Action : Renvoyer vers le document
**/
exports.documentCreate = function (id_utilisateur, id_capsule, nom_capsule, nom_ressource) {
	insererNotification(id_utilisateur, "un", nom_capsule + " a été ajouté", {"c" : id_capsule, "su" : "da"});
}
/**	
	*     du : document updaté
	*       - Cible : Utilisateur ayant droit de modification sur ce document, via une ressource
	*       - Contenu associé : Nom court du document concerné
	*       - Action : Renvoyer vers le document
**/
exports.documentUpdate = function (id_utilisateur, id_capsule, nom_capsule, nom_ressource) {
	insererNotification(id_utilisateur, "un", nom_capsule + " a été mis à jour", {"c" : id_capsule, "su" : "du"});
}

// Fonction pour mettre à jour le timestamp de dernière activité dans la base

exports.updateLastTimeActive = function (id_utilisateur) {
	pairform_dao.utilisateur_dao.updateLastTimeActive(id_utilisateur, function (retour_sql) {
		log.debug("Maj date active ", id_utilisateur)
	}, function (err) {
		log.error("Maj date active erreur : ", err)
	});
}
//Fonction interne pour gagner les succes

function insererNotification(id_utilisateur, type, title, options, should_push) {
	//Par défaut, on push
	var notification = options, should_push = typeof should_push != "undefined" ? should_push : true;

	if (!(id_utilisateur == "" || id_utilisateur == 0 || typeof(id_utilisateur) == "undefined"))
		notification["iu"] = id_utilisateur;
	
	notification["t"] = type;

	// Create the payload notification
	notification['aps'] = {
		'alert' : title,
		'sound' : 'default',
		'badge' : 1
	};

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.notification_dao.selectProchainIdNotification(function (retour_sql) {
			//Renseignement de l'id de notification (qui n'a pas encore été insérée dans la base)
			notification['in'] = retour_sql[0]["AUTO_INCREMENT"];
			//Si on est dans le cas de points attribués
			
			notification['pt'] = constantes[notification["st"]];

			//S'il y a une image (cas des succès)
			if (options["image"])
				notification["image"] = options["image"];
			else
				options["image"] = "NULL";
			
			//Encodage de la notification push en JSON
			var push_json = JSON.stringify(notification);

			if(!options['im'])
				options['im'] = "NULL";
			//Si la requête est trop grosse
			//Si la requête ne doit pas être pushé (on ne notifie pas l'utilisateur qui a fait l'action)
			if ((should_push == false) || (push_json.length > 190)) {
				push_json = "";
			}

			var functionToAsync = [];
			//Si pas d'id specifié, broadcast !
			if (id_utilisateur == "" || id_utilisateur == 0 || typeof(id_utilisateur) == "undefined") {		

				functionToAsync.push(function(done){
					pairform_dao.notification_dao.selectUtilisateursPourBroadcast(options['c'], function (retour_sql) {
						async.each(retour_sql, function(user, each_done){
							pairform_dao.notification_dao.insertNotificationForBroadcast(
								user['id_utilisateur'],
								options['im'],
								title,
								type,
								options['su'],
								options['st'],
								options['c'],
								options["image"],
								user["device_token"],
								push_json,
								function () {
									each_done(null);
								},
							each_done);
						}, done);
					},
					done);
				});
			}
			else {

				functionToAsync.push(function(done){
					pairform_dao.notification_dao.insertNotification(
						id_utilisateur,
						options['im'],
						title,
						type,
						options['su'],
						options['st'],
						options['c'],
						options["image"],
						push_json,
						function (retour_sql) {
							//TODO: gérer le retour
							done(null);
					},
					done);			
				});
			}
			if (should_push) {

				functionToAsync.push(function(done){
					pairform_dao.utilisateur_dao.selectUtilisateurPourNotification(
						id_utilisateur,
						function callback_succes (retour_sql) {
							var utilisateur = retour_sql[0];
							
							if (utilisateur && utilisateur.os_mobile == "ios") {
								//Envoi de la notification à l'utilisateur
								var device = new apn.Device(utilisateur.token_mobile);
								var apn_notification = new apn.Notification();
								
								apn_notification.expiry = Math.floor(Date.now() / 1000) + 3600; 
								// Expires 1 hour from now.
								
								//Infos spécifiques Apple 
								apn_notification.badge = notification.aps.badge;
								apn_notification.sound = notification.aps.sound;
								apn_notification.alert = notification.aps.alert;
								
								//Suppression des infos spécifiques Apple de la notification
								delete notification.aps;

								apn_notification.payload = notification; 
								apnConnection.pushNotification(apn_notification, device);
							}
							else if (utilisateur && utilisateur.os_mobile == "android") {
								
							}
							done(null);
						},
						done
					);
				});
			}

			async.parallel(functionToAsync, function callback_final (err) {
				if(!err)
					pairform_dao.libererConnexionBDD(_connexion_courante);
				else
					log.error(err);
			});
		},
		callbackError);
	});

}

function postWebhook(message) {
	//On ne poste que quand on est sur la prod
	if(!CONFIG.app.urls.alias_serveur.match(/pairform\.fr/))
		return;

	var url_webhook = CONFIG.app.webhook,
		url_contexte = CONFIG.app.urls.serveur_node + "/webServices/message/rediriger?id_message=" + message.id_message,
		payload = {
			text : message.contenu,
			username : message.pseudo_auteur + " (" + message.nom_auteur + ")",
			icon_url : message.url_avatar_auteur,
			unfurl_links : true,
			attachments:[
				{
					fallback : "<"+ url_contexte +"|Voir le contexte>",
					color : "#0000D0",
					fields : [
						{

			               title : "Infos",
			               value : "Message posté sur la capsule #" + message.id_capsule || message.id_ressource + " \n <"+ url_contexte +"|Voir le contexte>",
			               short : true
						}

					]
				}
			]
		};


	request.post(
	    url_webhook,
	    { json : payload },
	    function (error, response, body) {
	        if (!error && response.statusCode == 200) {
	            // log.debug(body)
	        }
	        else{
	        	log.error(error)
	        	log.error(body)
	        }
	    }
	);

}

function majScore(id_ressource, score_a_ajouter, id_utilisateur) {
	//Récupération des points
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.utilisateur_dao.selectVraieCategorieCapsule(
			id_utilisateur, 
			id_ressource,
			function (retour_sql) {
				if (retour_sql.length) {

					var score = retour_sql[0]["score"],
							id_categorie = retour_sql[0]["id_categorie"],
							id_ressource = retour_sql[0]["id_ressource"];

					//Ajout des nouveaux points
					score += score_a_ajouter;

					//Variables sortis du scope pour être utilisées dans le callback de l'update de score
					var old_id_categorie = +id_categorie,
							nom_categorie = "";

					//On vérifie s'il y a besoin de changer de rôle
					//Les experts et les admins ne sont de toute façon pas sujet au changement de points
					if (id_categorie < 4) {
						if ((score >= 0) && (score <= constantes.PTSPARTICIPANT)) {
							id_categorie = 0;
						}
						else if ((score >= constantes.PTSPARTICIPANT) && (score < constantes.PTSCOLLABORATEUR)) {
							id_categorie = 1;
							nom_categorie = "Participant";
						}
						else if ((score >= constantes.PTSCOLLABORATEUR) && (score < constantes.PTSANIMATEUR)) {
							id_categorie = 2;
							nom_categorie = "Collaborateur";
						}
						else if (score >= constantes.PTSANIMATEUR) {
							id_categorie = 3;
							nom_categorie = "Animateur";
						}
					}

					//Mise à jour des points et du niveau
					pairform_dao.utilisateur_dao.updateScoreUtilisateurAvecCapsule(
						id_utilisateur, 
						id_ressource,
						score,
						id_categorie,
						function (retour_sql) {
							pairform_dao.libererConnexionBDD(_connexion_courante);
							gagnerSuccesPoints(id_utilisateur);

							if (old_id_categorie < id_categorie){
								insererNotification(id_utilisateur, "un", "Vous êtes passé " + nom_categorie, {"c" : id_ressource , "su" : "cc"});
								//On passe le nom de la catégorie en paramètre, ça va nous permettre de récuperer les constantes de succcès grâce au nom
								gagnerSuccesRang(id_utilisateur, id_categorie, nom_categorie);
							}
					},
					callbackError);
				}
				else{
					log.debug("Pas de score pour l'utilisateur #" + id_utilisateur + " dans la ressource " + id_ressource);
					pairform_dao.libererConnexionBDD(_connexion_courante);
				}
		},
		callbackError);
	});
}
function verifierSucces(id_utilisateur, array_id_succes) {
	//Conversion en tableau pour être sûr
	if (typeof(array_id_succes) != "object")
		array_id_succes = [array_id_succes];

	//Si ya pas de succès a checker
	if (array_id_succes.length == 0)
		//On se casse
		return;

	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectSuccesSontGagnesByUtilisateur(
			id_utilisateur, 
			array_id_succes, 
			function (retour_sql) {
				var array_succes_gagnes = [];
				//Détermination des succès gagnés
				for (var i = 0; i < retour_sql.length; i++) {
					array_succes_gagnes.push(retour_sql[i]["id_succes"]);
				}

				async.each(array_id_succes, function (id_succes, callback) {
					//Si le succès n'a pas encore été gagné
					if (array_succes_gagnes.indexOf(id_succes) < 0){
						insererSucces(id_utilisateur, id_succes, function (err) {
							callback(err);
						});
					}
					else{
						async.setImmediate(function(){
							callback(null);
						});

					}
				}, function callback_final (err) {
					if (!err)
						pairform_dao.libererConnexionBDD(_connexion_courante);
					else
						callbackError();
				});
		},
		callbackError);
	 })
}

function insererSucces(id_utilisateur, id_succes, callback) {
	pairform_dao.succes_dao.insertSucces(
		id_utilisateur, 
		id_succes,
		function (retour_sql) {
			pairform_dao.succes_dao.selectSuccesFromId(
				id_utilisateur,
				id_succes,
				function  (retour_sql) {
					insererNotification(id_utilisateur, "un", "Vous avez gagné un succès", {"su" : "gs", "c" : retour_sql[0]['nom'], "image" : retour_sql[0]["nom_logo"]});
					callback();
					// gagnerToutLesSucces();
			},
			callbackError);
	}, callbackError);
}

//L'utilisateur a plus de 100 points sur une ressource
//Ou l'utilisateur a plus de 200 points sur plusieurs ressources
function gagnerSuccesPoints(id_utilisateur) {
	// MONO
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectScoreMaxUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			if (retour_sql[0]["score_max"] >= 100 ) 
				verifierSucces(id_utilisateur, constantes.POINTSMONO100);
		},
		callbackError);
	});
	
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectScoreTotalUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			if (retour_sql[0]["score_total"] >= 200 ) 
				verifierSucces(id_utilisateur, constantes.POINTSMULTI200);
		},
		callbackError);
	});
}

function gagnerSuccesAvatar(id_utilisateur) {
	verifierSucces(id_utilisateur, constantes.AVATAR);
}


function gagnerSuccesRessource(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreRessourcesUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_ressources = retour_sql[0]["nombre_ressources"],
					const_nombre_ressources = [];
			if (nombre_ressources >= 1) 
				const_nombre_ressources.push(constantes.RESSOURCE1);
			if (nombre_ressources >= 2) 
				const_nombre_ressources.push(constantes.RESSOURCE2);
			if (nombre_ressources >= 3) 
				const_nombre_ressources.push(constantes.RESSOURCE3);
			if (nombre_ressources >= 5) 
				const_nombre_ressources.push(constantes.RESSOURCE5);
			if (nombre_ressources >= 10) 
				const_nombre_ressources.push(constantes.RESSOURCE10);
			if (nombre_ressources >= 25) 
				const_nombre_ressources.push(constantes.RESSOURCE25);

			verifierSucces(id_utilisateur, const_nombre_ressources);
		},
		callbackError);
	});
}

function gagnerSuccesCercle(id_utilisateur) {
	verifierSucces(id_utilisateur,constantes.CERCLE);
}

function gagnerSuccesMessage(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreMessagesUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_messages = retour_sql[0]["nombre_messages"],
					const_nombre_messages = [];
			if (nombre_messages >= 5) 
				const_nombre_messages.push(constantes.MESSAGE5);
			if (nombre_messages >= 10) 
				const_nombre_messages.push(constantes.MESSAGE10);
			if (nombre_messages >= 20) 
				const_nombre_messages.push(constantes.MESSAGE20);
			if (nombre_messages >= 30) 
				const_nombre_messages.push(constantes.MESSAGE30);
			if (nombre_messages >= 50) 
				const_nombre_messages.push(constantes.MESSAGE50);
			if (nombre_messages >= 75) 
				const_nombre_messages.push(constantes.MESSAGE75);
			if (nombre_messages >= 100) 
				const_nombre_messages.push(constantes.MESSAGE100);

			verifierSucces(id_utilisateur, const_nombre_messages);
		},
		callbackError);
	});
}

function gagnerSuccesVote(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreVotesUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_votes = retour_sql[0]["nombre_votes"],
					const_nombre_votes = [];
			if (nombre_votes >= 5) 
				const_nombre_votes.push(constantes.VOTE5);
			if (nombre_votes >= 10) 
				const_nombre_votes.push(constantes.VOTE10);
			if (nombre_votes >= 20) 
				const_nombre_votes.push(constantes.VOTE20);
			if (nombre_votes >= 30) 
				const_nombre_votes.push(constantes.VOTE30);
			if (nombre_votes >= 50) 
				const_nombre_votes.push(constantes.VOTE50);
			if (nombre_votes >= 75) 
				const_nombre_votes.push(constantes.VOTE75);
			if (nombre_votes >= 100) 
				const_nombre_votes.push(constantes.VOTE100);

			verifierSucces(id_utilisateur, const_nombre_votes);
		},
		callbackError);
	});
}


function gagnerSuccesRepondre(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreReponsesUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_reponses = retour_sql[0]["nombre_reponses"],
					const_nombre_reponses = [];
			if (nombre_reponses >= 5) 
				const_nombre_reponses.push(constantes.REPONDRE5);
			if (nombre_reponses >= 10) 
				const_nombre_reponses.push(constantes.REPONDRE10);
			if (nombre_reponses >= 20) 
				const_nombre_reponses.push(constantes.REPONDRE20);
			if (nombre_reponses >= 30) 
				const_nombre_reponses.push(constantes.REPONDRE30);
			if (nombre_reponses >= 50) 
				const_nombre_reponses.push(constantes.REPONDRE50);
			if (nombre_reponses >= 75) 
				const_nombre_reponses.push(constantes.REPONDRE75);
			if (nombre_reponses >= 100) 
				const_nombre_reponses.push(constantes.REPONDRE100);

			verifierSucces(id_utilisateur, const_nombre_reponses);
		},
		callbackError);
	});
}

//TODO : ATTENTION - réponses pas encore uniques
function gagnerSuccesRecevoirReponses(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreReponsesSurMessageUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_reponses = retour_sql[0]["nombre_reponses"],
					const_nombre_reponses = [];
			if (nombre_reponses >= 5) 
				const_nombre_reponses.push(constantes.RECEVOIRREPONSES5);
			if (nombre_reponses >= 10) 
				const_nombre_reponses.push(constantes.RECEVOIRREPONSES10);
			if (nombre_reponses >= 25 ) 
				const_nombre_reponses.push(constantes.RECEVOIRREPONSES25);

			verifierSucces(id_utilisateur, const_nombre_reponses);
		},
		callbackError);
	});
}

function gagnerSuccesParticiperDefi(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreParticipationsDefi(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_reponses_defi = retour_sql[0]["nombre_reponses_defi"],
					const_nombre_reponses_defi = [];
			if (nombre_reponses_defi >= 5) 
				const_nombre_reponses_defi.push(constantes.DEFI5);
			if (nombre_reponses_defi >= 10) 
				const_nombre_reponses_defi.push(constantes.DEFI10);
			if (nombre_reponses_defi >= 20) 
				const_nombre_reponses_defi.push(constantes.DEFI20);
			if (nombre_reponses_defi >= 30) 
				const_nombre_reponses_defi.push(constantes.DEFI30);
			if (nombre_reponses_defi >= 50) 
				const_nombre_reponses_defi.push(constantes.DEFI50);
			if (nombre_reponses_defi >= 75) 
				const_nombre_reponses_defi.push(constantes.DEFI75);
			if (nombre_reponses_defi >= 100) 
				const_nombre_reponses_defi.push(constantes.DEFI100);

			verifierSucces(id_utilisateur, const_nombre_reponses_defi);
		},
		callbackError);
	});
}


function gagnerSuccesUtiliteRecu(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreVotesMessagesUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_votes_total = retour_sql[0]["nombre_votes_total"],
					const_nombre_votes_total = [],
					max_vote = retour_sql[0]["max_vote"],
					const_max_vote = [],
					nombre_votes_sup100 = retour_sql[0]["nombre_votes_sup100"],
					const_nombre_votes_sup100 = [];


			if (nombre_votes_total >= 5) 
				const_nombre_votes_total.push(constantes.UTILITEREPANDUE5);
			if (nombre_votes_total >= 10) 
				const_nombre_votes_total.push(constantes.UTILITEREPANDUE10);
			if (nombre_votes_total >= 20) 
				const_nombre_votes_total.push(constantes.UTILITEREPANDUE20);
			if (nombre_votes_total >= 30) 
				const_nombre_votes_total.push(constantes.UTILITEREPANDUE30);
			if (nombre_votes_total >= 50) 
				const_nombre_votes_total.push(constantes.UTILITEREPANDUE50);

			verifierSucces(id_utilisateur, const_nombre_votes_total);

			if (max_vote >= 5) 
				const_max_vote.push(constantes.UTILE5);
			if (max_vote >= 10) 
				const_max_vote.push(constantes.UTILE510);
			if (max_vote >= 20) 
				const_max_vote.push(constantes.UTILE520);
			if (max_vote >= 30) 
				const_max_vote.push(constantes.UTILE530);
			if (max_vote >= 50) 
				const_max_vote.push(constantes.UTILE550);
			if (max_vote >= 75) 
				const_max_vote.push(constantes.UTILE575);
			if (max_vote >= 100) 
				const_max_vote.push(constantes.UTILE5100);

			verifierSucces(id_utilisateur, const_max_vote);

			if (nombre_votes_sup100 >= 3) 
				const_nombre_votes_sup100.push(constantes.UTILE1003);
			if (nombre_votes_sup100 >= 5)
				const_nombre_votes_sup100.push(constantes.UTILE1005);
			if (nombre_votes_sup100 >= 10)
				const_nombre_votes_sup100.push(constantes.UTILE10010);

			verifierSucces(id_utilisateur, const_nombre_votes_sup100);
		},
		callbackError);
	});
}

function gagnerSuccesRejoindreClasse(id_utilisateur) {
	verifierSucces(id_utilisateur,constantes.CLASSE);
}


function gagnerSuccesMedaille(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreMedaillesUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_medailles = retour_sql[0]["nombre_medailles"],
					const_nombre_medailles = [];
			if (nombre_medailles >= 1) 
				const_nombre_medailles.push(constantes.MEDAILLE);
			if (nombre_medailles >= 3) 
				const_nombre_medailles.push(constantes.MEDAILLE3);
			if (nombre_medailles >= 5) 
				const_nombre_medailles.push(constantes.MEDAILLE5);
			if (nombre_medailles >= 10) 
				const_nombre_medailles.push(constantes.MEDAILLE10);

			verifierSucces(id_utilisateur, const_nombre_medailles);
		},
		callbackError);
	});
}

function gagnerTousLesSucces(id_utilisateur) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreSuccesRestantsUtilisateur(id_utilisateur, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			var nombre_succes_restant = retour_sql[0]["nombre_succes_restant"];
			if (nombre_succes_restant == 1)
				verifierSucces(id_utilisateur,constantes.TOUTLESSUCCES);
		},
		callbackError);
	});
}

//Privé


function gagnerSuccesRang(id_utilisateur, id_categorie, nom_categorie) {
	pairform_dao.getConnexionBDD(function (_connexion_courante) {
		pairform_dao.succes_dao.selectNombreRessourcesAvecCategorieUtilisateur(id_utilisateur, id_categorie, function (retour_sql) {
			pairform_dao.libererConnexionBDD(_connexion_courante);
			
			var nombre_ressources_categorie = retour_sql[0]["nombre_ressources_categorie"],
					const_nombre_ressources_categorie = [];
			if (nombre_ressources_categorie >= 1) 
				const_nombre_ressources_categorie.push(constantes[nom_categorie.toUpperCase() + "1"]);
			if (nombre_ressources_categorie >= 3) 
				const_nombre_ressources_categorie.push(constantes[nom_categorie.toUpperCase() + "3"]);
			if (nombre_ressources_categorie >= 5) 
				const_nombre_ressources_categorie.push(constantes[nom_categorie.toUpperCase() + "5"]);
			if (nombre_ressources_categorie >= 10) 
				const_nombre_ressources_categorie.push(constantes[nom_categorie.toUpperCase() + "10"]);

			verifierSucces(id_utilisateur, const_nombre_ressources_categorie);
		},
		callbackError);
	});
}
