
/*
	/!\ Attention : pour le Backoffice, les webservices du dossier "entites" 
					doivent être suffixés avec "_backoffice"
					pour éviter les conflits avec les webservices des Applications
*/


/*
 * Authentification
 */
exports.authentification = require("./authentification/authentification");

/*
 * Backoffice
 */
exports.tableau_de_bord = require("./backoffice/tableauDeBord");
exports.creation_ressource = require("./backoffice/creationRessource");
exports.attribution_roles = require("./backoffice/attributionRoles");
exports.regeneration_docs_PF = require("./backoffice/regenerationDocsPF");
exports.statistiques = require("./backoffice/statistiques");

exports.espace_backoffice = require("./backoffice/entites/espace");
exports.ressource_backoffice = require("./backoffice/entites/ressource");
exports.capsule_backoffice = require("./backoffice/entites/capsule");
exports.groupe_backoffice = require("./backoffice/entites/groupe");
exports.role_backoffice = require("./backoffice/entites/role");
exports.message_backoffice = require("./backoffice/entites/message");
exports.openbadge_backoffice = require("./backoffice/entites/openbadge");
exports.format_backoffice = require("./backoffice/entites/format");


/*
 * Transverse
 */

exports.session = require("./backoffice/entites/session");
exports.session_item = require("./backoffice/entites/session_item");


/*
 * Applications (iOs, web, Android, ...)
 */
exports.profil_utilisateur = require("./applications/profilUtilisateur");
exports.export_donnees_personnelles = require("./applications/exportDonneesPersonnelles");

// exports.annotation = require("./applications/entites/annotation");
exports.objectif_apprentissage = require("./applications/entites/objectifApprentissage");
exports.profil_apprentissage = require("./applications/entites/profilApprentissage");
exports.accomplissement = require("./applications/entites/accomplissement");
exports.message = require("./applications/entites/message");
exports.notification = require("./applications/entites/notification");
exports.reseau = require("./applications/entites/reseau");
exports.ressource = require("./applications/entites/ressource");
exports.capsule = require("./applications/entites/capsule");
exports.utilisateur = require("./applications/entites/utilisateur");
exports.api_rs = require("./applications/api_rs");


/*
 * Migrations
 */
// Exemple :
//exports.migration = require("./migrations/2017_01_regenerationDocsPF");
