var pairform_dao = require("pairform_dao"),
          fs = require("fs"),
          ws = require("../webServices"),
       async = require("async"),
       log = require("metalogger")(),
    constantes = require("../constantes"),
     adler32 = require('adler32'),
      AdmZip = require("adm-zip"),
    fs_extra = require("fs-extra"),
         _ = require("underscore"),
     request = require("request"),
      jade = require("jade"),
      CONFIG = require("config");

exports.regenerationDocsPF = function(id_capsule, callback) {
  //Recup de toutes les capsules au format pairform
  pairform_dao.requete("SELECT "+
    " cc.id_capsule, cc.clef_pairform, cc.id_format, cc.id_ressource, cr.nom_court as 'nom_ressource', cc.nom_court, cc.nom_long, cc.url_mobile, cc.url_web, cc.poid AS 'taille', cc.description, cc.licence, cc.auteurs, cc.cree_par, cr.id_espace, "+
    " IFNULL(UNIX_TIMESTAMP(cc.date_creation), 0) AS date_creation, IFNULL(UNIX_TIMESTAMP(cc.date_edition), 0) AS date_edition, "+
    " cu.pseudo AS pseudo_createur "+
    " FROM cape_capsules AS cc "+
    " JOIN cape_utilisateurs_v2 AS cu ON cc.cree_par = cu.id_utilisateur "+
    " JOIN cape_ressources AS cr ON cr.id_ressource = cc.id_ressource "+
    // " JOIN cape_espaces AS ce ON ce.id_espace = cc.id_espace "+
    " WHERE id_format = 0 " +
    (!!id_capsule && typeof id_capsule == "number" ? "AND cc.id_capsule = " + id_capsule : "") +
    " ORDER BY cc.nom_court;",
  function callback_success(retour_sql) {
    if(!retour_sql){
      return log.error('Pas de capsule au format PF');
    }
    
    log.debug("Migration de " + retour_sql.length + " document(s)…");
    async.eachLimit(retour_sql, 1, function (capsule, next) {
      //Récupération de leur outline.json
      try{
        capsule.sommaire = fs.readFileSync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/outline.json");
        capsule.sommaire = JSON.parse(capsule.sommaire);

        capsule.contenu_ck = {};
          
        //Iteration dans le outline pour reconstituer les chemins des fichiers ck
        for (var i = 0; i < capsule.sommaire.liste_pages.length; i++) {
          var page = capsule.sommaire.liste_pages[i];
          capsule.contenu_ck[page.id] = {
            path : constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/contenu_ckeditor_"+ page.id +".html"
          };
        }
        // log.debug(capsule);
        //Appel du ws avec tous les flags de modifs à true,
        enregistrerFichiersCapsules(
          capsule.contenu_ck, //contenu
          true, //page_est_modifiee
          [], //liste_pages_a_supprimer
          capsule.sommaire, //sommaire_json
          true, //sommaire_est_modifie
          capsule.nom_long, //nom_long
          capsule.pseudo_createur, //auteurs
          capsule.clef_pairform, //clef_pairform
          capsule.id_espace, //id_espace
          capsule.id_ressource, //id_ressource
          capsule.nom_ressource, //nom_ressource
          capsule.id_capsule, //id_capsule
          function (retour) {
            log.debug("Migration du document #" + capsule.id_capsule + " '" + capsule.nom_long + "' réussie");
            next();
          })
        }
        catch(e){
            log.error("Migration du document #" + capsule.id_capsule + " '" + capsule.nom_long + "' erreur");
            log.error(e);
            next();
        }
    }, function callback_final (err) {
      if (err)
        callback(log.error(err));
      else{
        log.debug("Migration terminée.");
        callback({status: "ok", count: retour_sql.length });
      }
    })
  },
  function callback_error(err) {
    callback(log.error(err));
  });

  function enregistrerFichiersCapsules(contenu, page_est_modifiee, liste_pages_a_supprimer, sommaire_json, sommaire_est_modifie, nom_long, auteurs, clef_pairform, id_espace, id_ressource, nom_ressource, id_capsule, callback) {

    var poid, page, sommaire_html = "<ul>", sommaire_xml = "<?xml version='1.0' encoding='UTF-8'?><outline>";
    var zip;

    // si la capsule n'existe pas, on crée les répertoires
    if (!fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule)) {
      fs.mkdirSync(constantes.REPERTOIRE_CAPSULE + id_capsule);
      fs.mkdirSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE);
      fs.mkdirSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB);

    }
    // sinon si la capsule existe déjà, on renomme le répertoire/archive qui la contient
    else {
      if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip")) {  // version mobile, on fait une copie de l'archive, puis on dézippe
        fs_extra.copySync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip", constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip");
        
        zip = new AdmZip(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip");
        zip.extractAllTo(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE);
      }
      if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB)) {       // version web, on fait une copie du répertoire
        fs_extra.copySync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB, constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule);
      }
    }
    
    // pour chaque page : on créé les sommaires (version XML et HTML), et on enregistre les pages
    async.eachSeries(sommaire_json.liste_pages, function enregistrerPage (page, callback_each) {
      // si le sommaire a été modifié
      if (sommaire_est_modifie) {
        sommaire_xml += "<l u='"+ page.id +".html' t='"+ page.titre +"'/>";
        sommaire_html += "<li><a href='"+ page.id +".html'>"+ page.titre +"</a></li>";
      }

      // si la page a été modifié, ou si c'est une nouvelle page
      if (contenu[page.id] && page_est_modifiee) {
        var datas_capsule = {
          "auteurs" : auteurs,
          "nom_long" : nom_long,
          "nom_page" : page.titre,
          "url_espace" : CONFIG.app.urls.serveur_node + '/doc/' + id_espace,
          "logo_espace" : CONFIG.app.urls.serveur_node + '/res/logos_espaces/' + id_espace + '.png',
          "url_ressource" : CONFIG.app.urls.serveur_node + '/doc/' + id_espace + '/' + id_ressource,
          "logo_ressource" : CONFIG.app.urls.serveur_node + '/res/logos_ressources/' + id_ressource + '.png',
          "nom_ressource" : nom_ressource,
          "contenu_page" : fs.readFileSync(contenu[page.id].path),
          "clef_pairform" : clef_pairform,
          "url_serveur_node" : CONFIG.app.urls.serveur_node
        };

        async.parallel([
          function enregistrerContenuCkeditor(callback_parallel) {

            // on sauvegarde le contenu html généré avec ckeditor, on le renverra au client pour les modifications du contenu de la page
            if (contenu[page.id].path != constantes.REPERTOIRE_CAPSULE + id_capsule +"/contenu_ckeditor_"+ page.id +".html")
              fs.renameSync(
                contenu[page.id].path,
                constantes.REPERTOIRE_CAPSULE + id_capsule +"/contenu_ckeditor_"+ page.id +".html"
              );
            
            callback_parallel();
          },
          function creerPageVersionWeb(callback_parallel) {
            // si la page existait, on la supprime
            if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/"+ page.id +".html"))
              fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/"+ page.id +".html");

            associationJade("/../backoffice/views/template_capsule_web.jade", datas_capsule, function(contenu_capsule) {
              fs.writeFileSync(
                constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/"+ page.id +".html",
                contenu_capsule
              );
              callback_parallel();
            });
          },
          function creerPageVersionMobile(callback_parallel) {
            // si la page existait, on la supprime
            if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ page.id +".html"))
              fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ page.id +".html");

            associationJade("/../backoffice/views/template_capsule_mob.jade", datas_capsule, function(contenu_capsule) {
              fs.writeFileSync(
                constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ page.id +".html",
                contenu_capsule
              );
              callback_parallel();
            })
          }
        ],
        function callbackFinal(err) {
          callback_each(err);
        });
      } else {
        callback_each();
      }
    }, function callback_each_final (err) {
      if (err){
        log.error(err);
        return err;
      }

      // on supprime les fichiers associés aux pages supprimées
      for(var i=0; i<liste_pages_a_supprimer.length; i++) {
        fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/contenu_ckeditor_"+ liste_pages_a_supprimer[i] +".html");
        fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/"+ liste_pages_a_supprimer[i] +".html");
        fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ liste_pages_a_supprimer[i] +".html");
      }
      
      // si le sommaire a été modifié
      if (sommaire_est_modifie) {
        // création de l'index.html (pour la version web)
        fs.readFile(__dirname + "/../backoffice/views/template_index_capsule.jade", 'utf8', function (err, data) {
          if (err) 
            return next(err);

          var jade_compiler = jade.compile(data);
          var contenu_index_html = jade_compiler({
            "auteurs" : auteurs,
            "nom_long" : nom_long,
            "url_premiere_page" : sommaire_json.liste_pages[0].id +".html"
          });
          fs.writeFileSync(
            constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/index.html",
            contenu_index_html
          );
        });

        // on enregistre les sommaires JSON, XML et HTML
        sommaire_xml += "</outline>";
        // Fermeture du ul
        sommaire_html += "</ul>";
        fs.writeFileSync(
          constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/outline.xml",
          sommaire_xml
        );
        fs.writeFileSync(
          constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/outline.html",
          sommaire_html
        );
        fs.writeFileSync(
          constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/outline.json",
          JSON.stringify(sommaire_json)
        );
        fs.writeFileSync(
          constantes.REPERTOIRE_CAPSULE + id_capsule +"/outline.json",
          JSON.stringify(sommaire_json)
        );
      }

      // création du fichier css pour la version mobile (on dublique le fichier contenant le css commun aux versions mobile et web, cf. balise link des fichiers template_capsule_mob/web.jade) 
      fs_extra.copySync(__dirname +"/../../../../public/css/ckeditor_capsule.css", constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/ckeditor_capsule.css");

      // on créé l'archive mobile
      zip = new AdmZip();
      zip.addLocalFolder(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE);
      zip.writeZip(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip");
      supprimerRepertoire(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE);

      // on récupère le poid de l'archive mobile (en Mo), on met 1 Mo minimum
      var stats = fs.statSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip");
      poid = stats["size"] / (1024 * 1024);
      if (poid < 1)
        poid = 1;

      // s'il y en avait une, suppression de l'ancienne version de la capsule mobile
      if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip"))
        fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip");

      // s'il y en avait une, suppression de l'ancienne version de la capsule web
      if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule))
        supprimerRepertoire(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule);

      callback(
        constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ constantes.VERSION_WEB +"/",
        constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ constantes.VERSION_MOBILE,
        poid
      );
    })
    
    // associe les champs du template avec les données de la page
    function associationJade(url_template, datas_capsule, callback_association) {
      fs.readFile(__dirname + url_template, 'utf8', function (err, data) {
        if (err) {
          return next(err);
        }
        
        var jade_compiler = jade.compile(data);
        var contenu_capsule = jade_compiler(datas_capsule);

        callback_association(contenu_capsule);
      });
    }
  }

  // Supprime récursivement un repertoire
function supprimerRepertoire(chemin) {
  var fichiers = [];

  if( fs.existsSync(chemin) ) {
    fichiers = fs.readdirSync(chemin);

    fichiers.forEach( function(fichier,index) {
      var chemin_courant = chemin + "/" + fichier;
      
      if(fs.lstatSync(chemin_courant).isDirectory()) {
        supprimerRepertoire(chemin_courant);
      } else {
        // supprimer fichier associé au chemin courant
        fs.unlinkSync(chemin_courant);
      }
    });

    fs.rmdirSync(chemin);
  }
};
}