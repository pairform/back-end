"use strict";

var pairform_dao = require("pairform_dao"),
  web_services = require("../../webServices"),
    constantes = require("../../constantes"),
        async = require("async"),
        log = require("metalogger")();

// var LISTE_GROUPES_OBLIGATOIRES = [constantes.GROUPE_SPHERE_PUBLIQUE, constantes.GROUPE_ESPACE];                         // liste des groupes obligatoires dans un espace
// var LISTE_ROLES_PAR_DEFAUT = [constantes.ROLE_RESPONSABLE_CAPSULE, constantes.ROLE_RESPONSABLE_RESSOURCE, constantes.ROLE_RESPONSABLE_ESPACE];  // liste des roles par défaut d'un espace


// Récupération d'une session
exports.getSession = function(param, callback) {
  function callbackError() {
    callback(constantes.RETOUR_JSON_KO);
  }

  pairform_dao.session_dao.selectSession(
    param.id_session,
    function (session) {
      retour_json.session = session;
      callback(retour_json);
    },
    callbackError
  );
};


// Récupération d'une session avec un id_ressource
exports.getSessions = function(param, callback) {
  function callbackError() {
    callback(constantes.RETOUR_JSON_KO);
  }
  var itemsFunctionToCall;
  if (param.id_utilisateur) {
    itemsFunctionToCall = pairform_dao.session_item_dao.selectItemsOfSessionForUser;
    pairform_dao.session_dao.selectSessionsOfRessourceAndUser(
      param.id_ressource,
      param.id_utilisateur,
      getItemsAndEnd,
      callbackError
    );
  }
  else{
    itemsFunctionToCall = pairform_dao.session_item_dao.selectItemsOfSession;
    pairform_dao.session_dao.selectSessionsOfRessource(
      param.id_ressource,
      getItemsAndEnd,
      callbackError
    );
  }

  function getItemsAndEnd(sessions) {
    async.forEach(
      sessions, 
      function(session,next) {
        itemsFunctionToCall(
          session.id_session,
          function (items) {
            session.items = items || [];
            next(null);
          },
          next
        );
      }, 
      function(err) {
        // if any of the saves produced an error, err would equal that error
        if(err) {
          callback({
            status:"ko",
            error : err
          });
        }
        else {
          callback({
            status :"ok",
            data : {
              sessions : sessions
            }
          });  
        }
      }
    );
  }
};


// Création d'une nouvelle session vide
exports.putSession = function(param, callback) {
  function callbackError() {
    callback(constantes.RETOUR_JSON_KO);
  }

  //Check des paramètres
  var _session = {
    id_ressource : param.id_ressource,
    nom : param.nom,
    id_utilisateur : param.id_utilisateur
  };
  pairform_dao.session_dao.insertSession(
    _session,
    function (retour_sql) {
      // if (retour_sql.insertedRows > 0){
        var id_session = retour_sql.insertId;
        callback({
          status: "ok",
          data : {
            id_session : id_session
          }
        });
      // }
      // else 
      //   callbackError();
    },
    callbackError
  );
};
// MAJ d'une session
exports.postSession = function(param, callback) {
  function callbackError() {
    callback(constantes.RETOUR_JSON_KO);
  }

  //Check des paramètres
  var _session = {
    id_groupe : param.id_groupe,
    nom : param.nom
  };

  pairform_dao.session_dao.updateSession(
    param.id_session,
    _session,
    function (retour_sql) {
      if (retour_sql.affectedRows > 0) {
        callback({
          status: "ok"
        });
      }
    },
    callbackError
  );
};

// Suppression d'une session
exports.deleteSession = function(param, callback) {
  function callbackError() {
    callback(constantes.RETOUR_JSON_KO);
  }
  log.debug(param);
  pairform_dao.session_dao.deleteSession(
    param.id_session,
    function (retour_sql) {
      callback({
        status: "ok"
      });
      log.debug(retour_sql);
    },
    callbackError
  );
};
