"use strict";

var pairform_dao = require("pairform_dao"),
	web_services = require("../../webServices"),
	  constantes = require("../../constantes"),
			  log = require("metalogger")(),
			  Jimp = require("jimp"),
			  fs = require("fs");

var LISTE_GROUPES_OBLIGATOIRES = [constantes.GROUPE_SPHERE_PUBLIQUE, constantes.GROUPE_ESPACE];													// liste des groupes obligatoires dans un espace
var LISTE_ROLES_PAR_DEFAUT = [constantes.ROLE_RESPONSABLE_CAPSULE, constantes.ROLE_RESPONSABLE_RESSOURCE, constantes.ROLE_RESPONSABLE_ESPACE];	// liste des roles par défaut d'un espace


// Récupération d'un espace
exports.getEspace = function(id_espace, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectEspace();

	function selectEspace() {
		pairform_dao.espace_dao.selectEspace(
			id_espace,
			function (espace) {
				retour_json.espace = espace;
				callback(retour_json);
			},
			callbackError
		);
	}
};


// Création d'un nouvel espace
exports.putEspace = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var nom_court = liste_parametres.nom_long,
		 nom_long = liste_parametres.nom_long,
			  url = liste_parametres.url ? liste_parametres.url : null,
			 logo = liste_parametres.logo;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	insertEspace();

	function insertEspace() {
		pairform_dao.espace_dao.insertEspace(
			nom_court,
			nom_long,
			url,
			function (reponse_sql) {
				// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
				enregistrementLogo(reponse_sql.id_espace);
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function enregistrementLogo(id_espace) {
		// si un logo est défini pour l'espace
		if (logo) {
			var logo_blob = new Buffer(logo, "base64");
			constantes.creerImageAvecDimension(logo_blob, constantes.REPERTOIRE_LOGOS_ESPACES + id_espace + ".png", 400, true, function () {
				creerIssuerJSON(id_espace);
			});
		}
		else
			creerIssuerJSON(id_espace);

	}

	// création d'un fichier issuer.json (nécessaire à l'utilisation des openbadges)
	function creerIssuerJSON(id_espace) {
		// si l'url a été renseigné (ce n'est pas un champ obligatoire)
		if (url) {
			var issuer_json = {};
			issuer_json.name = nom_court;
			issuer_json.url = url;

			// on créé le fichier issuer.json en à partir des infos de l'espace
			fs.writeFileSync(constantes.REPERTOIRE_OPEN_BADGES_ISSUERS + id_espace +".json", JSON.stringify(issuer_json));
		}
		// on insert le premier groupe de la liste des groupes obligatoires dans le nouvelle espace
		insertGroupe(0, id_espace);
	}

	// création des groupes obligatoires pour le nouvelle espace
	function insertGroupe(index_groupe, id_espace) {
		pairform_dao.groupe_dao.insertGroupe(
			LISTE_GROUPES_OBLIGATOIRES[index_groupe],
			id_espace,
			true,
			function () {
				index_groupe++;
				// SI il reste des groupes obligatoires à insérer
				if (index_groupe < LISTE_GROUPES_OBLIGATOIRES.length) {
					insertGroupe(index_groupe, id_espace);
				} else {
					// on insert le premier role de la liste des roles par défaut dans le nouvelle espace
					insertRoleEspace(0, id_espace);
				}
			},
			callbackError
		);
	}

	// création des rôles par défaut pour le nouvelle espace
	function insertRoleEspace(index_role, id_espace) {
		pairform_dao.role_dao.insertRoleEspace(
			id_espace,
			LISTE_ROLES_PAR_DEFAUT[index_role].admin_espace,
			LISTE_ROLES_PAR_DEFAUT[index_role].nom,
			LISTE_ROLES_PAR_DEFAUT[index_role].editer_espace,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_groupes,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_roles_espace,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_ressources_et_capsules,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_toutes_ressources_et_capsules,
			LISTE_ROLES_PAR_DEFAUT[index_role].gerer_visibilite_toutes_capsules,
			LISTE_ROLES_PAR_DEFAUT[index_role].deplacer_capsule,
			function () {
				index_role++;
				// SI il reste des rôles par défaut à insérer
				if (index_role < LISTE_ROLES_PAR_DEFAUT.length) {
					insertRoleEspace(index_role, id_espace);
				} else {
					retour_json.id_espace = id_espace;
					callback(retour_json);
				}
			},
			callbackError
		);
	}
};


// Mise à jour d'un espace
exports.postEspace = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_KO};

	var id_espace = liste_parametres.id_espace,
		nom_court = liste_parametres.nom_long,
		 nom_long = liste_parametres.nom_long,
			  url = liste_parametres.url ? liste_parametres.url : null,
			 logo = liste_parametres.logo;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	updateEspace();

	function updateEspace() {		
		pairform_dao.espace_dao.updateEspace(
			id_espace,
			nom_court,
			nom_long,
			url,
			function () {
				enregistrementLogo();
			},
			function (erreur_sql) {
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function enregistrementLogo() {																	// stockage du logo dans le repertoire dédié
		if (liste_parametres.logo) {
			var logo_blob = new Buffer(logo, "base64");
			constantes.creerImageAvecDimension(logo_blob, constantes.REPERTOIRE_LOGOS_ESPACES + id_espace + ".png", 400, true, editerIssuerJSON);
		}
		else
			editerIssuerJSON();
	}
	
	// édition du fichier issuer.json (nécessaire à l'utilisation des openbadges)
	function editerIssuerJSON() {
		// si l'url a été renseigné (ce n'est pas un champ obligatoire)
		if (url) {
			var issuer_json = {};
			issuer_json.name = nom_long;
			issuer_json.url = url;

			// on créé un nouveau fichier issuer.json en à partir des infos de l'espace
			fs.writeFileSync(constantes.REPERTOIRE_OPEN_BADGES_ISSUERS + id_espace +".json", JSON.stringify(issuer_json));
		}
		callback(constantes.RETOUR_JSON_OK);
	}
};


// Suppression d'un espace
exports.deleteEspace = function(id_espace, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	deleteRessources();

	// supprime les ressources contenues dans l'espace à supprimer
	function deleteRessources() {
		pairform_dao.ressource_dao.selectRessourcesByEspaces([{
				id_espace: id_espace
			}],
			function (liste_ressources) {
				if (liste_ressources.length > 0) {
					web_services.ressource_backoffice.deleteRessources({
							liste_ressources: liste_ressources,
							id_espace: id_espace
						},
						function () {
							deleteGroupes();
						},
						callbackError
					);
				}
				else {
					deleteGroupes();
				}
			},
			callbackError
		);
	}

	// supprime les groupes de l'espace, leurs membres et leurs rôles
	function deleteGroupes() {
		// recupère les membres des groupes de l'espace
		pairform_dao.membre_dao.selectMembresByIdEspace(
			id_espace,
			null,
			function (liste_membres) {
				if (liste_membres.length > 0) {
					// supprime les membres des groupes
					pairform_dao.groupe_dao.deleteMembresGroupeEspace(
						liste_membres,
						function () {
							// supprime les membres
							pairform_dao.membre_dao.deleteMembres(
								liste_membres,
								function () {
									// supprime les roles backoffice de l'espace
									pairform_dao.role_dao.deleteRolesByEspace(
										id_espace,
										function () {
											// supprime les groupes
											pairform_dao.groupe_dao.deleteGroupesByEspace(
												id_espace,
												function () {
													deleteEspace();
												},
												callbackError
											);
										},
										callbackError
									);
								},
								callbackError
							);
						},
						callbackError
					);
				}
				else {
					// supprime les roles backoffice de l'espace
					pairform_dao.role_dao.deleteRolesByEspace(
						id_espace,
						function () {
							// supprime les groupes
							pairform_dao.groupe_dao.deleteGroupesByEspace(
								id_espace,
								function () {
									deleteEspace();
								},
								callbackError
							);
						},
						callbackError
					);
				}
			},
			callbackError
		);
	}

	// supprime l'espace
	function deleteEspace() {
		pairform_dao.espace_dao.deleteEspace(
			id_espace,
			function () {
				// si un fichier logo existe pour l'espace, on le supprime
				if ( fs.existsSync(constantes.REPERTOIRE_LOGOS_ESPACES + id_espace +".png") )
					fs.unlinkSync(constantes.REPERTOIRE_LOGOS_ESPACES + id_espace +".png");

				callback(constantes.RETOUR_JSON_OK);
			},
			callbackError
		);
	}
};