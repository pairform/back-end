"use strict";

var pairform_dao = require("pairform_dao"),
	web_services = require("../../webServices"),
	  constantes = require("../../constantes"),
			  fs = require("fs");


// Récupération d'un ressource
exports.getRessource = function(id_ressource, code_langue, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectRessource();

	function selectRessource() {
		pairform_dao.ressource_dao.selectRessource(
			id_ressource,
			function (ressource) {
				retour_json.ressource = ressource;
				selectAllThemes();
			},
			callbackError
		);
	}

	// Récupére toutes les thèmes (traduit dans la langue choisi)
	function selectAllThemes() {
		pairform_dao.theme_dao.selectAllThemes(
			code_langue,
			function (liste_themes) {
				retour_json.liste_themes = liste_themes;
				selectAllUsages();
			},
			callbackError
		);
	}

	// Récupére toutes les usages (traduits dans la langue choisie)
	function selectAllUsages() {
		pairform_dao.usage_dao.selectAllUsages(
			function (liste_usages) {
				retour_json.liste_usages = liste_usages;
				selectAllLangues();
			},
			callbackError
		);
	}

	// Récupére toutes les langues (traduit dans la langue choisi)
	function selectAllLangues() {
		pairform_dao.langue_dao.selectAllLangues(
			code_langue,
			function (liste_langues) {
				retour_json.liste_langues = liste_langues;
				callback(retour_json);
			},
			callbackError
		);
	}
};


// Création d'un nouvelle ressource
exports.putRessource = function(liste_parametres, utilisateur, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var nom_court = liste_parametres.nom_long,
		id_espace = liste_parametres.id_espace,
		 nom_long = liste_parametres.nom_long,
	  description = liste_parametres.description || "",
		 id_theme = liste_parametres.id_theme,
		 id_usage = liste_parametres.id_usage,
		id_langue = liste_parametres.id_langue,
			 logo = liste_parametres.logo,
		 cree_par = utilisateur.id_utilisateur;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	insertRessource();

	function insertRessource() {
		pairform_dao.ressource_dao.insertRessource(
			nom_court,
			id_espace,
			nom_long,
			description,
			id_theme,
			id_usage,
			id_langue,
			cree_par,
			function (reponse_sql) {
				retour_json.id_ressource = reponse_sql.id_ressource;

				// on met à jour les droits de l'utilisateur ayant créé la ressource (côté client et côté serveur)
				if (!utilisateur.est_admin_pairform) {
					retour_json.role_auteur = constantes.ROLE_AUTEUR;
					utilisateur.liste_roles[id_espace][reponse_sql.id_ressource] = constantes.ROLE_AUTEUR;
				}

				EnregistrementLogo(reponse_sql.id_ressource);
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;

				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function EnregistrementLogo(id_ressource) {
		if (logo) {			
			var logo_blob = new Buffer(logo, "base64");														// on transforme la représentation base64 en hexadécimale
			// fs.writeFileSync(constantes.REPERTOIRE_LOGOS_RESSOURCES + id_ressource +".png", logo_blob);		// stockage du logo dans le repertoire dédié
			constantes.creerImageAvecDimension(logo_blob,constantes.REPERTOIRE_LOGOS_RESSOURCES + id_ressource + ".png", 100, true);
		}

		// si le createur de la ressource n'est pas l'admin pairform
		if (cree_par != constantes.ID_UTILISATEUR_ADMIN_PAIRFORM) {
			// le créateur de la ressource est défini comme admin de la nouvelle ressource
			insertAdmin(cree_par, id_ressource, true);
		} else {
			// l'admin pairform est défini comme admin de la nouvelle ressource
			insertAdmin(constantes.ID_UTILISATEUR_ADMIN_PAIRFORM, id_ressource, false);
		}
	}

	// défini un utilisateur comme admin de la nouvelle ressource
	function insertAdmin(id_utilisateur, id_ressource, categorie_temp) {
		pairform_dao.utilisateur_dao.insertCategorieUtilisateur(
			id_utilisateur,
			id_ressource,
			constantes.CATEGORIE_ADMIN,
			categorie_temp,						// true si c'est un rôle temporaire, false sinon
			function () {
				// si l'admin pairform n'a pas encore été défini comme admin de la nouvelle ressource
				if(id_utilisateur != constantes.ID_UTILISATEUR_ADMIN_PAIRFORM) {
					// l'admin pairform est défini comme admin de la nouvelle ressource
					insertAdmin(constantes.ID_UTILISATEUR_ADMIN_PAIRFORM, id_ressource, false);
				} else {
					// ajout du rôle auteur pour la nouvelle ressource
					insertRoleRessource(id_ressource, constantes.ROLE_COAUTEUR);
				}
			},
			callbackError
		);
	}

	// création des rôles par défaut pour la nouvelle ressource
	function insertRoleRessource(id_ressource, role_par_defaut) {
		pairform_dao.role_dao.insertRoleRessource(
			id_ressource,
			role_par_defaut.nom,
			role_par_defaut.editer_ressource,
			role_par_defaut.gerer_open_badge,
			role_par_defaut.afficher_statistiques,
			role_par_defaut.gerer_roles_ressource,
			role_par_defaut.supprimer_ressource,
			role_par_defaut.gerer_capsule,
			role_par_defaut.gerer_visibilite,
			role_par_defaut.gerer_toutes_capsules,
			role_par_defaut.gerer_visibilite_toutes_capsules_ressource,
			role_par_defaut.supprimer_messages_capsule,
			role_par_defaut.supprimer_messages_toutes_capsules,
			function (id_role_ressource) {
				if (role_par_defaut.nom == constantes.ROLE_COAUTEUR.nom)
					// ajout du rôle co-auteur pour la nouvelle ressource
					insertRoleRessource(id_ressource, constantes.ROLE_AUTEUR);
				else if (cree_par != constantes.ID_UTILISATEUR_ADMIN_PAIRFORM)		// si l'utilisateur n'est pas l'admin PF
					selectMembre(id_ressource, id_role_ressource);
				else if (id_usage == constantes.USAGE_FORMATION)					// si la ressource est dédié à un usage de formation, on associe à la ressource la liste de questions/réponses du profil d'apprentissage
					insertQuestion(id_ressource, 0);
				else
					callback(retour_json);
			},
			callbackError
		);
	}

	// on récupére le membre associé à l'utilisateur dans cet espace, il est nommé au rôle auteur
	function selectMembre(id_ressource, id_role_ressource) {
		pairform_dao.membre_dao.selectMembreEspaceByIdUtilisateur(
			id_espace,
			cree_par,
			function (membre) {
				insertMembreRoleRessource(id_ressource, id_role_ressource, membre.id_membre);
			},
			callbackError
		);
	}

	// on nomme le createur de la ressource au rôle Auteur
	function insertMembreRoleRessource(id_ressource, id_role_ressource, id_membre) {
		pairform_dao.membre_dao.insertMembreRoleRessource(
			id_membre,
			id_role_ressource,
			function () {
				// si la ressource est dédié à un usage de formation, on associe à la ressource la liste de questions/réponses du profil d'apprentissage
				if (id_usage == constantes.USAGE_FORMATION) {
					insertQuestion(id_ressource, 0);
				} else {
					callback(retour_json);
				}
			},
			callbackError
		);
	}

	// créé une question dans le profil d'apprentissage d'une ressource
	function insertQuestion(id_ressource, index_question) {
		pairform_dao.profil_apprentissage_dao.insertQuestion(
			id_ressource,
			function (reponse_sql) {
				insertTraductionQuestion(id_ressource, index_question, reponse_sql.id_question);
			},
			callbackError
		);
	}

	// créé la traduction d'une question du profil d'apprentissage d'une ressource
	function insertTraductionQuestion(id_ressource, index_question, id_question) {
		pairform_dao.profil_apprentissage_dao.insertTraductionQuestion(
			id_question,
			constantes.PROFIL_APPRENTISSAGE[index_question].id_langue,
			constantes.PROFIL_APPRENTISSAGE[index_question].question,
			function () {
				insertReponse(id_ressource, index_question, id_question, 0);
			},
			callbackError
		);
	}

	// créé une réponse à une question du profil d'apprentissage d'une ressource
	function insertReponse(id_ressource, index_question, id_question, index_reponse) {
		pairform_dao.profil_apprentissage_dao.insertReponse(
			id_question,
			function (reponse_sql) {
				insertTraductionReponse(id_ressource, index_question, id_question, index_reponse, reponse_sql.id_reponse);
			},
			callbackError
		);
	}

	// créé la traduction d'une reponse du profil d'apprentissage d'une ressource
	function insertTraductionReponse(id_ressource, index_question, id_question, index_reponse, id_reponse) {
		pairform_dao.profil_apprentissage_dao.insertTraductionReponse(
			id_reponse,
			constantes.PROFIL_APPRENTISSAGE[index_question].id_langue,
			constantes.PROFIL_APPRENTISSAGE[index_question].reponses[index_reponse],
			function () {
				index_reponse++;
				// s'il y a encore des réponses à ajouter
				if(index_reponse < constantes.PROFIL_APPRENTISSAGE[index_question].reponses.length) {
					insertReponse(id_ressource, index_question, id_question, index_reponse);
				} else {
					index_question++;
					// s'il y a encore des questions à ajouter
					if (index_question < constantes.PROFIL_APPRENTISSAGE.length) {
						insertQuestion(id_ressource, index_question);
					} else {
						callback(retour_json);
					}
				}
			},
			callbackError
		);
	}
};


// Mise à jour d'un ressource
exports.postRessource = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_KO};

	var id_ressource = liste_parametres.id_ressource,
		   nom_court = liste_parametres.nom_long,
		   id_espace = liste_parametres.id_espace,
			nom_long = liste_parametres.nom_long,
		 description = liste_parametres.description || "",
			id_theme = liste_parametres.id_theme,
		   id_langue = liste_parametres.id_langue,
				logo = liste_parametres.logo;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	updateRessource();

	function updateRessource() {
		pairform_dao.ressource_dao.updateRessource(
			id_ressource,
			nom_court,
			id_espace,
			nom_long,
			description,
			id_theme,
			id_langue,
			function () {
				EnregistrementLogo();
			},
			function (erreur_sql) {
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function EnregistrementLogo() {
		if (logo) {			
			var logo_blob = new Buffer(logo, "base64");														// on transforme la représentation base64 en hexadécimale
			constantes.creerImageAvecDimension(logo_blob,constantes.REPERTOIRE_LOGOS_RESSOURCES + id_ressource + ".png", 100, true);
		}
		callback(constantes.RETOUR_JSON_OK);
	}
};


// Suppression d'une liste de ressources
exports.deleteRessources = function(liste_parametres, callback) {
	var liste_ressources = liste_parametres.liste_ressources;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	deleteCapsules();

	// supprime les capsules contenues dans les ressources à supprimer
	function deleteCapsules() {
		pairform_dao.capsule_dao.selectCapsulesByRessources(
			liste_ressources,
			function (liste_capsules) {
				if (liste_capsules.length > 0) {
					web_services.capsule_backoffice.deleteCapsules({
							liste_capsules: liste_capsules
						},
						function () {
							deleteUtilisateursCategorie();
						},
						callbackError
					);
				}
				else {
					deleteUtilisateursCategorie();
				}
			},
			callbackError
		);
	}

	// supprime les role des utilisateurs associés aux ressources
	function deleteUtilisateursCategorie() {
		pairform_dao.utilisateur_dao.deleteUtilisateursCategorieByRessources(
			liste_ressources,
			function () {
				deleteObjectifsApprentissage();
			},
			callbackError
		);
	}

	// supprime les objectifs d'apprentissage
	function deleteObjectifsApprentissage() {
		pairform_dao.objectif_apprentissage_dao.deleteObjectifsApprentissageByRessources(
			liste_ressources,
			function () {
				deleteProfilsApprentissage();
			},
			callbackError
		);
	}

	// supprime les profils d'apprentissage
	function deleteProfilsApprentissage() {
		// supprime les profils d'apprentissage des utilisateurs sur une liste de ressources
		pairform_dao.profil_apprentissage_dao.deleteProfilsApprentissageUtilisateursByRessources(
			liste_ressources,
			function () {
				// supprime les traductions des réponses au profil d'apprentissage des utilisateurs pour une liste de ressources
				pairform_dao.profil_apprentissage_dao.deleteTraductionsReponsesByRessources(
					liste_ressources,
					function () {
						// supprime les réponses au profil d'apprentissage des utilisateurs pour une liste de ressources
						pairform_dao.profil_apprentissage_dao.deleteReponsesByRessources(
							liste_ressources,
							function () {
								// supprime les traductions des questions au profil d'apprentissage des utilisateurs pour une liste de ressources
								pairform_dao.profil_apprentissage_dao.deleteTraductionsQuestionsByRessources(
									liste_ressources,
									function () {
										// supprime les questions au profil d'apprentissage des utilisateurs pour une liste de ressources
										pairform_dao.profil_apprentissage_dao.deleteQuestionsByRessources(
											liste_ressources,
											function () {
												deleteOpenBadges();
											},
											callbackError
										);
									},
									callbackError
								);
							},
							callbackError
						);
					},
					callbackError
				);
			},
			callbackError
		);
	}

	// supprime les openbadges associés aux ressources
	// ATTENTION : les fichiers json associéss aux badges ne seront pas supprimés -> cela conserve la compatibilité avec les openbadges Mozilla
	function deleteOpenBadges() {
		pairform_dao.openbadge_dao.deleteOpenBadgesByRessources(
			liste_ressources,
			function () {
				deleteRoles();
			},
			callbackError
		);
	}

	// supprime les rôles backoffice des ressources, et les membres ayant ces rôles
	function deleteRoles() {
		// suppresssion des membres ayant ces rôles
		pairform_dao.membre_dao.deleteAllMembresRoleRessourceByRessources(
			liste_ressources,
			function () {
				// suppresssion des rôles
				pairform_dao.role_dao.deleteRolesByRessources(
					liste_ressources,
					function () {
						deleteRessources();
					},
					callbackError
				);
			},
			callbackError
		);
	}

	// supprime les ressources
	function deleteRessources() {
		pairform_dao.ressource_dao.deleteRessources(
			liste_ressources,
			function () {
				// suppression des logos
				for (var i in liste_ressources) {
					// si un fichier logo existe pour la ressource courante, on le supprime
					if ( fs.existsSync(constantes.REPERTOIRE_LOGOS_RESSOURCES + liste_ressources[i].id_ressource +".png") )
						fs.unlinkSync(constantes.REPERTOIRE_LOGOS_RESSOURCES + liste_ressources[i].id_ressource +".png");
				}

				callback(constantes.RETOUR_JSON_OK);
			},
			callbackError
		);
	}
};
