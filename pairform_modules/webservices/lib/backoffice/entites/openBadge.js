"use strict";

var pairform_dao = require("pairform_dao"),
	web_services = require("../../webServices"),
	  constantes = require("../../constantes"),
			  fs = require("fs");


// Récupération de la liste des openbadges via sa ressource
exports.getOpenBadgesByIdRessource = function(id_ressource, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectOpenBadgesByIdRessource();

	function selectOpenBadgesByIdRessource() {
		pairform_dao.openbadge_dao.selectOpenBadgesByIdRessource(
			id_ressource,
			function (liste_openbadges) {
				retour_json.liste_openbadges = liste_openbadges;
				callback(retour_json);
			},
			callbackError
		);
	}
};


// Création d'un nouvel openbadge
exports.putOpenBadge = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var nom = liste_parametres.nom,
	description = liste_parametres.description,
		   logo = liste_parametres.logo,
	  id_espace = liste_parametres.id_espace,
   id_ressource = liste_parametres.id_ressource,
   		// si l'utilisateur est déjà émetteurs d'open badges sur une autre plateforme, il a déjà un fichier issuer.json, il peut le réutiliser ici
		 issuer_externe = liste_parametres.issuer_externe ? liste_parametres.issuer_externe : null;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectEspace();

	function selectEspace() {
		pairform_dao.espace_dao.selectEspace(
			id_espace,
			function (espace) {
				// si l'url de l'espace n'est pas défni, on ne peut pas créer d'openbadge
				if(!espace.url)
					callbackError();
				else
					insertOpenBadge();
			},
			callbackError
		);
	}

	function insertOpenBadge() {
		pairform_dao.openbadge_dao.insertOpenBadge(
			id_ressource,
			nom,
			description,
			issuer_externe,
			function (reponse_sql) {
				// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
				enregistrementLogo(reponse_sql.id_openbadge);
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function enregistrementLogo(id_openbadge) {
		var logo_blob = new Buffer(logo, "base64");														// on transforme sa représentation base64 en hexadécimale
		fs.writeFileSync(constantes.REPERTOIRE_LOGOS_OPEN_BADGES + id_openbadge +".png", logo_blob);	// stockage du logo dans le repertoire dédié

		creerBadgeClassJSON(id_openbadge);
	}

	// création d'un fichier issuer.json (nécessaire à l'utilisation des openbadges)
	function creerBadgeClassJSON(id_openbadge) {
		var badge_class_json = {};
		badge_class_json.name = nom;
		badge_class_json.description = description;
		badge_class_json.image = constantes.URL_REPERTOIRE_LOGOS_OPEN_BADGES + id_openbadge +".png";
		badge_class_json.issuer_externe = constantes.URL_REPERTOIRE_OPEN_BADGES_ISSUERS + id_espace +".json";
		badge_class_json.criteria = constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource;

		// on créé le fichier badge_class.json à partir des infos du badge
		fs.writeFileSync(constantes.REPERTOIRE_OPEN_BADGES_BADGES_CLASS + id_openbadge +".json", JSON.stringify(badge_class_json));

		retour_json.id_openbadge = id_openbadge;
		retour_json.url_logo = constantes.URL_REPERTOIRE_LOGOS_OPEN_BADGES + id_openbadge +".png";

		callback(retour_json);
	}
};


// Mise à jour d'un openbadge
exports.postOpenBadge = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var id_openbadge = liste_parametres.id_openbadge,
				 nom = liste_parametres.nom,
		 description = liste_parametres.description,
				logo = liste_parametres.logo ? liste_parametres.logo : null,
		   id_espace = liste_parametres.id_espace,
		id_ressource = liste_parametres.id_ressource,
	  issuer_externe = liste_parametres.issuer_externe ? liste_parametres.issuer_externe : null;	// si l'utilisateur est déjà émetteurs d'open badges sur une autre plateforme, il a déjà un fichier issuer.json, il peut le réutiliser ici

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectEspace();

	function selectEspace() {
		pairform_dao.espace_dao.selectEspace(
			id_espace,
			function (espace) {
				// si l'url de l'espace n'est pas défni, on ne peut pas créer d'openbadge
				if(!espace.url)
					callbackError();
				else
					updateOpenBadge();
			},
			callbackError
		);
	}

	function updateOpenBadge() {
		pairform_dao.openbadge_dao.updateOpenBadge(
			id_openbadge,
			nom,
			description,
			issuer_externe,
			function () {
				// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
				enregistrementLogo();
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}

	// stockage du logo dans un repertoire du serveur et enregistrement des infos en BDD (logo_blob + URL d'accès)
	function enregistrementLogo() {
		if(logo) {
			var logo_blob = new Buffer(logo, "base64");														// on transforme sa représentation base64 en hexadécimale
			fs.writeFileSync(constantes.REPERTOIRE_LOGOS_OPEN_BADGES + id_openbadge +".png", logo_blob);	// stockage du logo dans le repertoire dédié
		}
		editerBadgeClassJSON(id_openbadge);
	}

	// édition du fichier issuer.json (nécessaire à l'utilisation des openbadges)
	function editerBadgeClassJSON(id_openbadge) {
		var badge_class_json = {};
		badge_class_json.name = nom;
		badge_class_json.description = description;
		badge_class_json.image = constantes.URL_REPERTOIRE_LOGOS_OPEN_BADGES + id_openbadge +".png";
		badge_class_json.issuer_externe = constantes.URL_REPERTOIRE_OPEN_BADGES_ISSUERS + id_espace +".json";
		badge_class_json.criteria = constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource;

		// on créé un nouveau fichier badge_class.json à partir des infos du badge
		fs.writeFileSync(constantes.REPERTOIRE_OPEN_BADGES_BADGES_CLASS + id_openbadge +".json", JSON.stringify(badge_class_json));

		retour_json.id_openbadge = id_openbadge;
		callback(retour_json);
	}
};
