"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
		   async = require("async");


// Récupération de la liste des groupes via son espace
exports.getGroupesByIdEspace = function(id_espace, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	selectGroupesByIdEspace();

	function selectGroupesByIdEspace() {
		pairform_dao.groupe_dao.selectGroupesByIdEspace(
			id_espace,
			function (liste_groupes) {
				retour_json.liste_groupes = liste_groupes;
				selectMembres();
			},
			callbackError
		);
	}

	// Récupère la liste des emails (respectivement la liste des noms de domaines) des membres de chaque groupe
	function selectMembres() {
		var index_groupe_nom_domaine = 0, index_groupe_email=0;

		async.parallel([
			// Récupère la liste des emails des membres d'un groupe
			function selectEmailMembresByIdGroupe(callback_parallel) {
				pairform_dao.membre_dao.selectMembresByIdGroupe(
					retour_json.liste_groupes[index_groupe_email].id_groupe,
					false,
					function (liste_membres) {
						retour_json.liste_groupes[index_groupe_email].liste_membres_email = liste_membres;
						index_groupe_email++;
						
						if (index_groupe_email < retour_json.liste_groupes.length) {
							selectEmailMembresByIdGroupe(callback_parallel);
						} else {
							callback_parallel();
						}
					},
					callbackError
				);
			},
			// Récupère la liste des noms de domaines des membres d'un groupe (le nom de domaine est compté comme un membre mais peut faire référence à plusieurs utilisateurs)
			function selectNomDomaineMembresByIdGroupe(callback_parallel) {
				pairform_dao.membre_dao.selectMembresByIdGroupe(
					retour_json.liste_groupes[index_groupe_nom_domaine].id_groupe,
					true,
					function (liste_membres) {
						retour_json.liste_groupes[index_groupe_nom_domaine].liste_membres_nom_domaine = liste_membres;
						index_groupe_nom_domaine++;
						
						if (index_groupe_nom_domaine < retour_json.liste_groupes.length) {
							selectNomDomaineMembresByIdGroupe(callback_parallel);
						} else {
							callback_parallel();
						}
					},
					callbackError
				);
			}
		],
		function callbackFinal () {
			callback(retour_json);
		});
	}
};


// Création d'un nouveau groupe
exports.putGroupe = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	insertGroupe();

	function insertGroupe() {
		pairform_dao.groupe_dao.insertGroupe(
			liste_parametres.nom,
			liste_parametres.id_espace,
			liste_parametres.obligatoire,
			function (id_groupe) {
				retour_json.id_groupe = id_groupe;
				callback(retour_json);
			},
			function (erreur_sql) {
				retour_json = constantes.RETOUR_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}
};


// Mise à jour d'un groupe
exports.postGroupe = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_KO};

	updateGroupe();

	function updateGroupe() {
		pairform_dao.groupe_dao.updateGroupe(
			liste_parametres.nom,
			liste_parametres.id_groupe,
			function () {
				callback(constantes.RETOUR_JSON_OK);
			},
			function (erreur_sql) {
				retour_json.message = erreur_sql.code_erreur;
				callback(retour_json);
			}
		);
	}
};


// Mise à jour de la liste des membres d'un groupe
exports.postMembres = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	
	if (liste_parametres.liste_membres_supprimes.length > 0) {	// S'il y a des membres à supprimer dans le groupe
		// si le groupe concerné est l'espace
		if(liste_parametres.groupe_espace) {
			deleteMembresRoleRessource();
		} else {
			deleteMembresGroupeByIdGroupe();
		}
	} else if (liste_parametres.liste_nouveaux_membres.length > 0) {		// Sinon, s'il y a des membres à ajouter dans le groupe
		// si le groupe concerné est l'espace
		if(liste_parametres.groupe_espace) {
			insertMembre();
		} else {
			insertMembreGroupe(liste_parametres.liste_nouveaux_membres);
		}
	} else {
		callback(constantes.RETOUR_JSON_OK);
	}

	// cas spécifique au groupe Espace
	// supprime les rôles des membres à supprimer qui possédaient un rôle dans une ressource
	function deleteMembresRoleRessource() {
		pairform_dao.membre_dao.deleteMembresRoleRessource(
			liste_parametres.liste_membres_supprimes,
			null,
			function () {
				deleteMembresGroupeEspace();
			},
			callbackError
		);
	}

	// cas spécifique au groupe Espace
	// supprime une liste de membres dans le groupe Espace et dans ses sous-groupes
	function deleteMembresGroupeEspace() {
		pairform_dao.groupe_dao.deleteMembresGroupeEspace(
			liste_parametres.liste_membres_supprimes,
			function () {
				deleteMembres();
			},
			callbackError
		);
	}

	// cas spécifique au groupe Espace
	// supprime une liste de membres
	function deleteMembres() {
		pairform_dao.membre_dao.deleteMembres(
			liste_parametres.liste_membres_supprimes,
			function () {
				// S'il y a des membres à ajouter dans le groupe
				if (liste_parametres.liste_nouveaux_membres.length > 0) {
					insertMembre();
				} else {
					selectEmailMembresByIdGroupe();
				}
			},
			callbackError
		);
	}

	// supprime une liste de membres d'un groupe
	function deleteMembresGroupeByIdGroupe() {
		pairform_dao.groupe_dao.deleteMembresGroupeByIdGroupe(
			liste_parametres.id_groupe,
			liste_parametres.liste_membres_supprimes,
			function () {
				// S'il y a des membres à ajouter dans le groupe
				if (liste_parametres.liste_nouveaux_membres.length > 0) {
					insertMembreGroupe(liste_parametres.liste_nouveaux_membres);
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}

	// cas spécifique au groupe Espace
	// créé un membre
	function insertMembre() {
		pairform_dao.membre_dao.insertMembre(
			liste_parametres.liste_nouveaux_membres,
			function (liste_id_membre) {
				insertMembreGroupe(liste_id_membre)
			},
			callbackError
		);
	}

	// ajoute un membre à un groupe
	function insertMembreGroupe(liste_id_nouveaux_membres) {
		pairform_dao.groupe_dao.insertMembreGroupe(
			liste_parametres.id_groupe,
			liste_id_nouveaux_membres,
			function () {				
				// si le groupe concerné est l'espace
				if(liste_parametres.groupe_espace) {
					selectEmailMembresByIdGroupe();
				} else {
					callback(constantes.RETOUR_JSON_OK);
				}
			},
			callbackError
		);
	}

	// Récupère la liste des emails des membres de l'espace
	function selectEmailMembresByIdGroupe() {
		pairform_dao.membre_dao.selectMembresByIdGroupe(
			liste_parametres.id_groupe,
			false,
			function (liste_membres) {
				retour_json.liste_membres_email = liste_membres;
				selectNomDomaineMembresByIdGroupe();
			},
			callbackError
		);
	}

	// Récupère la liste des noms de domaines des membres de l'espace
	function selectNomDomaineMembresByIdGroupe() {
		pairform_dao.membre_dao.selectMembresByIdGroupe(
			liste_parametres.id_groupe,
			true,
			function (liste_membres) {				
				retour_json.liste_membres_nom_domaine = liste_membres;

				callback(retour_json);
			},
			callbackError
		);
	}
};


// Suppression d'un groupe (qui ne peut pas être un groupe obligatoire)
exports.deleteGroupe = function(id_groupe, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	deleteAllMembresGroupeByIdGroupe();
	
	// on vide le groupe de ses membres
	function deleteAllMembresGroupeByIdGroupe() {
		pairform_dao.groupe_dao.deleteAllMembresGroupeByIdGroupe(
			id_groupe,
			function () {
				sqlDeleteGroupe();
			},
			callbackError
		);
	}

	function sqlDeleteGroupe() {
		pairform_dao.groupe_dao.deleteGroupe(
			id_groupe,
			function () {
				callback(constantes.RETOUR_JSON_OK);
			},
			callbackError
		);
	}
};