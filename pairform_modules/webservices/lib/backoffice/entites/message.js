"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes");


// Suppression de tout les messages d'une capsule
exports.deleteCapsuleMessages = function(id_capsule, callback) {
	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	deleteMessages();

	// supprime les messages (donc, supprime aussi les notifications, les votes et les tags liés à ces messages) - pyramid of doom !
	// le paramètre envoyé (id_capsule) dans chaque DAO doit être un tableau (car le DAO gère aussi la possibilité de supprimer les messages de plusieurs capsules à la fois)
	function deleteMessages() {
		// suppression notifications
		pairform_dao.notification_dao.deleteNofificationsByCapsules(
			[{id_capsule: id_capsule}],
			function () {
				// suppression votes
				pairform_dao.vote_dao.deleteVotesByCapsules(
					[{id_capsule: id_capsule}],
					function () {
						// suppression tags
						pairform_dao.tag_dao.deleteTagsByCapsules(
							[{id_capsule: id_capsule}],
							function () {
								// suppression messages
								pairform_dao.message_dao.deleteMessagesByCapsules(
									[{id_capsule: id_capsule}],
									function () {
										callback(constantes.RETOUR_JSON_OK);
									},
									callbackError
								);
							},
							callbackError
						);
					},
					callbackError
				);
			},
			callbackError
		);
	}
};