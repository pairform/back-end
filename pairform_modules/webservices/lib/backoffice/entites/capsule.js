"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../../constantes"),
			 log = require('metalogger')(),
		 adler32 = require('adler32'),
		  AdmZip = require("adm-zip"),
			  fs = require("fs"),
		fs_extra = require("fs-extra"),
			   _ = require("underscore"),
		   async = require("async"),
		 request = require("request"),
			jade = require("jade"),
		api_rs = require("../../applications/api_rs"),
		  CONFIG = require("config");


// Récupération d'une capsule
exports.getCapsule = function(id_capsule, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	async.parallel([
		function selectCapsule(callback_parallel) {
			pairform_dao.capsule_dao.selectCapsule(
				id_capsule,
				function (capsule) {
					retour_json.capsule = capsule;
					retour_json.script_clef_pairform = "<script type='text/javascript' src='"+ CONFIG.app.urls.serveur_node +"/app/"+ capsule.clef_pairform +"'></script>";		// le script à ajouter dans les fichiers html de la capsule

					// si c'est le format pairform, on renvoit aussi le sommaire (s'il existe, c'est à dire si du contenue a été créé)
					if (capsule.id_format == constantes.FORMAT_PAIRFORM || capsule.id_format == constantes.FORMAT_ETHERPAD) {
						if(fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/outline.json")) {
							capsule.sommaire = fs.readFileSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/outline.json");
							// log.debug(capsule.sommaire);
							capsule.sommaire = JSON.parse(capsule.sommaire);
						}
						if(fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/css_personalise.css")) {
							capsule.css_personalise = fs.readFileSync(constantes.REPERTOIRE_CAPSULE + id_capsule + "/"+ constantes.VERSION_WEB +"/css_personalise.css", "utf-8");
							// log.debug(capsule.css_personalise);
						}
					}
				
					callback_parallel();
				},
				callbackError
			);
		},
		function selectAllFormats(callback_parallel) {
			pairform_dao.format_dao.selectAllFormats(
				function (liste_formats) {
					retour_json.liste_formats = liste_formats;
					callback_parallel();
				},
				callbackError
			);
		}
	],
	function callbackFinal() {
		callback(retour_json);
	});
};


// récupèration des informations sur la visibilité d'une capsule (pour chaque groupe de l'espace)
exports.getCapsuleVisibilite = function(id_capsule, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	async.parallel([
		function selectCapsuleVisibilite(callback_parallel) {
			pairform_dao.capsule_dao.selectCapsuleVisibilite(
				id_capsule,
				function (liste_groupes) {
					retour_json.liste_groupes = liste_groupes;	// liste des groupes (nom, id, visibilité...)
					callback_parallel();
				},
				callbackError
			);
		},
		function selectAllVisibilites(callback_parallel) {
			pairform_dao.capsule_dao.selectAllVisibilites(
				function (liste_visibilites) {
					retour_json.liste_visibilites = liste_visibilites;
					callback_parallel();
				},
				callbackError
			);
		}
	],
	function callbackFinal () {
		selectMembres();
	});

	// Récupère la liste des emails (respectivement la liste des noms de domaines) des membres de chaque groupe
	function selectMembres() {
		var index_groupe_nom_domaine = 0, index_groupe_email=0;

		async.parallel([
			// Récupère la liste des emails des membres d'un groupe
			function selectEmailMembresByIdGroupe(callback_parallel) {
				pairform_dao.membre_dao.selectMembresByIdGroupe(
					retour_json.liste_groupes[index_groupe_email].id_groupe,
					false,
					function (liste_membres) {
						retour_json.liste_groupes[index_groupe_email].liste_membres_email = liste_membres;
						index_groupe_email++;
						
						if (index_groupe_email < retour_json.liste_groupes.length) {
							selectEmailMembresByIdGroupe(callback_parallel);
						} else {
							callback_parallel();
						}
					},
					callbackError
				);
			},
			// Récupère la liste des noms de domaines des membres d'un groupe (le nom de domaine est compté comme un membre mais peut faire référence à plusieurs utilisateurs)
			function selectNomDomaineMembresByIdGroupe(callback_parallel) {
				pairform_dao.membre_dao.selectMembresByIdGroupe(
					retour_json.liste_groupes[index_groupe_nom_domaine].id_groupe,
					true,
					function (liste_membres) {
						retour_json.liste_groupes[index_groupe_nom_domaine].liste_membres_nom_domaine = liste_membres;
						index_groupe_nom_domaine++;
						
						if (index_groupe_nom_domaine < retour_json.liste_groupes.length) {
							selectNomDomaineMembresByIdGroupe(callback_parallel);
						} else {
							callback_parallel();
						}
					},
					callbackError
				);
			}
		],
		function callbackFinal () {
			callback(retour_json);
		});
	}
};


// Création d'un nouvelle capsule
exports.putCapsule = function(liste_parametres, cree_par, callback_final) {
	var retour_json = {status: constantes.STATUS_JSON_OK};
	var selecteurs;

	var id_format = liste_parametres.id_format,
		id_ressource = liste_parametres.id_ressource,
			nom_long = liste_parametres.nom_long,
		  url_mobile = liste_parametres.url_mobile,
			 url_web = liste_parametres.url_web,
				poid = liste_parametres.poid,
		 description = liste_parametres.description || "",
			 licence = liste_parametres.licence || "Privée",
			 auteurs = liste_parametres.auteurs,
	  archive_mobile = liste_parametres.archive_mobile || false, 
		 archive_web = liste_parametres.archive_web || false;
	
	// on spécifie les sélecteurs de la capsule selon le format
	switch (id_format) {
		case 0:
			selecteurs = constantes.SELECTEURS_CAPSULE_PAIRFORM;
			break;
		case 1:
			selecteurs = constantes.SELECTEURS_CAPSULE_SCENARI;
			break;
		case 2:
			selecteurs = constantes.SELECTEURS_CAPSULE_CHAINEDIT;
			break;
		case 3:
			selecteurs = liste_parametres.selecteurs;
			break;
		case 4:
			selecteurs = constantes.SELECTEURS_CAPSULE_STORYLINE2;
			break;
		case 5:
			selecteurs = constantes.SELECTEURS_CAPSULE_ETHERPAD;
			break;
		case 6:
			selecteurs = constantes.SELECTEURS_CAPSULE_STORYLINE3;
			break;
	}

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback_final(constantes.RETOUR_JSON_KO);
	}

	async.waterfall([
		function insertCapsule (callback_waterfall) {
			pairform_dao.capsule_dao.insertCapsule(
				id_format,
				id_ressource,
				nom_long,
				nom_long,
				url_mobile,
				url_web,
				poid,
				description,
				licence,
				auteurs,
				cree_par,
				function (reponse_sql) {
					retour_json.id_capsule = reponse_sql.id_capsule;
					callback_waterfall(null, retour_json.id_capsule);
				},
				function (erreur_sql) {
					retour_json.status = constantes.STATUS_JSON_KO;
					retour_json.message = erreur_sql.code_erreur;

					callback_waterfall(retour_json);
				}
			);
		},
		function insertCapsuleSelecteurs(id_capsule, callback_waterfall) {
			pairform_dao.capsule_dao.insertCapsuleSelecteurs(
				id_capsule,
				selecteurs,
				function () {
					callback_waterfall(null, id_capsule);
				},
				callback_waterfall
			);
		},
		// ajoute la clef pairform de la capsule et met à jour les urls mobile et web (elles peuvent avoir été modifié si une capsule a été uploadé)
		function updateClefPairFormCapsule(id_capsule, callback_waterfall) {
			var buffer_id_capsule = new Buffer(id_capsule);
			var clef_pairform = adler32.sum(buffer_id_capsule).toString(16);	// hash de l'id avec Adler-32 pour obtenir la clef pairform

			// Ajout en retour pour les clients : la clef pairform  			
			retour_json.clef_pairform = clef_pairform;
			// Ajout en retour pour les clients : le script à ajouter dans la capsule
			retour_json.script_clef_pairform = "<script type='text/javascript' src='"+ CONFIG.app.urls.serveur_node +"/app/"+ clef_pairform +"'></script>";
			
			pairform_dao.capsule_dao.updateClefPairFormCapsule(
				id_capsule,
				clef_pairform,
				function () {
					callback_waterfall(null, id_capsule);
				},
				callback_waterfall
			);
		},
		// Récupère les groupes obligatoires d'un espace via l'id d'une ressource de cet espace (groupes obligatoires : sphère publique, espace...)
		function selectGroupesObligatoiresByIdRessource(id_capsule, callback_waterfall) {
			pairform_dao.groupe_dao.selectGroupesObligatoiresByIdRessource(
				id_ressource,
				function (liste_groupes_obligatoires) {
					// on insert la visibilité par défaut pour le premier groupe de la liste des groupes obligatoires
					callback_waterfall(null, liste_groupes_obligatoires, id_capsule);
				},
				callback_waterfall
			);
		},
		function insertCapsuleVisibilite(liste_groupes_obligatoires, id_capsule, callback_waterfall) {
			async.each(liste_groupes_obligatoires, function insertionVisibilite (groupe, callback_each) {
				pairform_dao.capsule_dao.insertCapsuleVisibilite(
					id_capsule,
					groupe.id_groupe,
					constantes.VISIBILITE_INACCESSIBLE,
					function () {
						callback_each();
					},
					callback_each
				);
			}, function callback_each_final (err) {
				callback_waterfall(err, id_capsule);
			})
		}
	], function callback_waterfall_final (err, id_capsule) {			
		if (err) {
			log.error(err);
			callback_final(retour_json);
		}
		else {
			callback_final(retour_json);
			//Notifications des utilisateurs

			notifierCoAdministrateursCapsuleCreation(id_ressource, id_capsule, nom_long);
		}
	});
};


// Mise à jour d'un capsule
exports.postCapsule = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var id_capsule = liste_parametres.id_capsule,
		id_ressource = liste_parametres.id_ressource,
			nom_long = liste_parametres.nom_long,
		  url_mobile = liste_parametres.url_mobile,
			 url_web = liste_parametres.url_web,
				poid = liste_parametres.poid,
		 description = liste_parametres.description || "",
			 licence = liste_parametres.licence,
			 auteurs = liste_parametres.auteurs;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	updateCapsule();

	function updateCapsule() {
		pairform_dao.capsule_dao.updateCapsule(
			id_capsule,
			id_ressource,
			nom_long,
			nom_long,
			url_mobile,
			url_web,
			poid,
			description,
			licence,
			auteurs,
			function () {
				callback(retour_json);
				notifierCoAdministrateursCapsuleUpdate(id_ressource, id_capsule, nom_long);
			},
			function (erreur_sql) {
				retour_json.status = constantes.STATUS_JSON_KO;
				retour_json.message = erreur_sql.code_erreur;

				callback(retour_json);
			}
		);
	}
};


// déplacement d'une capsule dans une autre ressource du même espace
exports.deplacerCapsule = function(liste_parametres, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var id_capsule = liste_parametres.id_capsule,
	id_nouvelle_ressource = liste_parametres.id_ressource,
				 nom_long = liste_parametres.nom_long,
			   url_mobile = liste_parametres.url_mobile,
				  url_web = liste_parametres.url_web,
					 poid = liste_parametres.poid,
			  description = liste_parametres.description || "",
				  licence = liste_parametres.licence,
				  auteurs = liste_parametres.auteurs;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	comparerEspaces();

	// on vérifie que la nouvelle ressource se trouve dans le même espace que l'ancienne
	function comparerEspaces() {
		pairform_dao.espace_dao.selectComparaisonEspaces(
			id_capsule,
			id_nouvelle_ressource,
			function (espace_identique) {
				if(espace_identique)
					deplacerCapsule();
				else
					callbackError();
			},
			callbackError
		);
	}

	function deplacerCapsule() {
		pairform_dao.capsule_dao.updateCapsule(
			id_capsule,
			id_nouvelle_ressource,
			nom_long,
			nom_long,
			url_mobile,
			url_web,
			poid,
			description,
			licence,
			auteurs,
			function () {
				retour_json.nom_long = nom_long;
				ajouterRoles();
			},
			function(erreur_sql) {
				// en cas de doublon, on modifie le nom de la capsule, et on recommence
				if(erreur_sql.code_erreur == "ER_DUP_ENTRY") {
					nom_long += " bis";
					callback(retour_json);
				} else {
					callbackError();
				}
			}
		);
	}

	// Pour les auteurs de messages sur la capsule déplacée : s'ils n'ont pas de rôle dans la nouvelle ressource, on leur en ajoute un
	function ajouterRoles() {
		pairform_dao.utilisateur_dao.selectUtilisateursSansCategorie(
			id_capsule,
			id_nouvelle_ressource,
			function (liste_utilisateurs) {
				if(!_.isEmpty(liste_utilisateurs)) {
					async.each(liste_utilisateurs, function insertionVisibilite (utilisateur, callback_each) {
						pairform_dao.utilisateur_dao.insertCategorieUtilisateur(
							utilisateur.id_utilisateur,
							id_nouvelle_ressource,
							constantes.CATEGORIE_PARTICIPANT,
							constantes.CATEGORIE_LECTEUR,
							function () {
								callback_each();
							},
							callback_each
						);
					}, function callback_each_final (err) {
						callback(retour_json);
					})
				} else {
					callback(retour_json);
				}
			},
			callbackError
		);
	}
};


// upload d'une capsule (fichier ou archives, selon le format de la capsule)
// si son format est une chainEditorial (ChainEdit, Scenari...) : représenté par une archive (version mobile ou version web)
// si son format est pairform : représenté par une liste de fichiers (1 par page) et un sommaire
exports.postUploadCapsule = function(liste_parametres, liste_uploads, callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	var id_espace = liste_parametres.id_espace,
				id_ressource = liste_parametres.id_ressource,
				nom_ressource = liste_parametres.nom_ressource,
				  id_capsule = liste_parametres.id_capsule,
				   id_format = liste_parametres.id_format,
			   clef_pairform = liste_parametres.clef_pairform || null,
					nom_long = liste_parametres.nom_long || null,
					 auteurs = liste_parametres.auteurs || null,
				// si le format est format pairform
			   sommaire_json = liste_parametres.sommaire,
			 contenu_capsule = liste_uploads || {},										// contient la liste des pages modifiées (donc toutes les pages dans le cas de la création d'une capsule)
		sommaire_est_modifie = liste_parametres.sommaire_est_modifie,		
		   page_est_modifiee = liste_parametres.page_est_modifiee,				
	 liste_pages_a_supprimer = liste_parametres.liste_pages_a_supprimer || [],			// contient la liste des pages supprimées
			 css_est_modifie = liste_parametres.css_est_modifie,
			 css_personalise = liste_parametres.css_personalise,						// contient le css sur-mesure de la capsule
				// si le format est une chainEditorial 
				 archive_mob = liste_uploads.mob || false,
				 archive_web = liste_uploads.web || false;

	// dans le cas d'une création d'une nouvelle capsule, sommaire_est_modifie et page_est_modifiee valent null
	(sommaire_est_modifie === "true" || sommaire_est_modifie == null) ? sommaire_est_modifie = true : sommaire_est_modifie = false;	// sommaire_est_modifie vaut true si le titre d'une page du sommaire a été modifié OU si une page a été ajouté au sommaire 
	(page_est_modifiee === "true" || page_est_modifiee == null) ? page_est_modifiee = true : page_est_modifiee = false;				// page_est_modifiee vaut true si au moins une page a été modifiée ou ajouté



	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	enregistrerCapsule();

	// enregistre le contenu de la capsule (archives (web et/ou mobile) ou fichier)
	function enregistrerCapsule() {
		if(archive_web) {
			enregistrerArchive(archive_web, id_espace, id_ressource, id_capsule, constantes.VERSION_WEB, function(nouvelle_url_web) {
				if ((id_format == constantes.FORMAT_STORYLINE2) || (id_format == constantes.FORMAT_STORYLINE3)) {
					modifierPourStoryline(id_capsule, clef_pairform);
				}
				updateUrlCapsule(constantes.VERSION_WEB, nouvelle_url_web);
			});
		} else if(archive_mob) {
			enregistrerArchive(archive_mob, id_espace, id_ressource, id_capsule, constantes.VERSION_MOBILE, function(nouvelle_url_mobile) {
				updateUrlCapsule(constantes.VERSION_MOBILE, nouvelle_url_mobile);
			});
		} else if (sommaire_est_modifie || page_est_modifiee || !_.isEmpty(liste_pages_a_supprimer) || css_est_modifie) {			// si le sommaire a été modifié OU si une page a été modifiée/ajoutée/supprimée 
			enregistrerFichiersCapsules(contenu_capsule, page_est_modifiee, liste_pages_a_supprimer, sommaire_json, sommaire_est_modifie, css_est_modifie, css_personalise, nom_long, auteurs, clef_pairform, id_espace, id_ressource, nom_ressource, id_capsule, function(nouvelle_url_web, nouvelle_url_mobile, nouveau_poid) {
				updateUrlsCapsule(nouvelle_url_mobile, nouvelle_url_web, nouveau_poid);
			});
		} else {
			callback(retour_json);
		}
	}

	// mise à jour des urls de la capsule
	function updateUrlsCapsule(nouvelle_url_mobile, nouvelle_url_web, nouveau_poid) {
		pairform_dao.capsule_dao.updateUrlsCapsule(
			id_capsule,
			nouvelle_url_mobile,
			nouvelle_url_web,
			nouveau_poid,
			function () {
				callback(retour_json);
			},
			callbackError
		);
	}

	// mise à jour d'une url de la capsule
	function updateUrlCapsule(version, nouvelle_url) {
		pairform_dao.capsule_dao.updateUrlCapsule(
			id_capsule,
			version,
			nouvelle_url,
			function () {
				callback(retour_json);
			},
			callbackError
		);
	}
};


// Mise à jour de la visibilité d'une capsule
exports.postCapsuleVisibilite = function(visibilite_capsule, callback) {
	updateCapsuleVisibiliteSpherePublique();

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	// Mise à jour de la visibilité de la capsule pour le groupe obligatoire Sphere publique
	function updateCapsuleVisibiliteSpherePublique() {
		pairform_dao.capsule_dao.updateCapsuleVisibilite(
			visibilite_capsule.id_capsule,
			visibilite_capsule.sphere_publique.id_groupe,
			visibilite_capsule.sphere_publique.id_visibilite,
			function () {
				updateCapsuleVisibiliteEspace();
			},
			callbackError
		);
	}

	// Mise à jour de la visibilité de la capsule pour le groupe obligatoire Sphere publique
	function updateCapsuleVisibiliteEspace() {
		pairform_dao.capsule_dao.updateCapsuleVisibilite(
			visibilite_capsule.id_capsule,
			visibilite_capsule.espace.id_groupe,
			visibilite_capsule.espace.id_visibilite,
			function () {
				// si un groupe non-obligatoire avait déjà une visibilité sur la capsule
				if (visibilite_capsule.id_ancien_groupe_selectionne) {
					deleteCapsuleVisibilite();
				}
				// sinon, si un nouveau groupe non-obligatoire doit avoir une visibilité sur la capsule
				else if (visibilite_capsule.nouveau_groupe_selectionne) {
					insertCapsuleVisibilite();
				} else {
					callback(constantes.RETOUR_JSON_OK);
					
				}
			},
			callbackError
		);
	}

	// Suppression la visibilité de la capsule pour un groupe
	function deleteCapsuleVisibilite() {
		pairform_dao.capsule_dao.deleteCapsuleVisibilite(
			visibilite_capsule.id_capsule,
			visibilite_capsule.id_ancien_groupe_selectionne,
			function () {
				// si un nouveau groupe non-obligatoire doit avoir une visibilité sur la capsule
				if (visibilite_capsule.nouveau_groupe_selectionne) {
					insertCapsuleVisibilite();
				} else {
					callback(constantes.RETOUR_JSON_OK);					
				}
			},
			callbackError
		);
	}

	// Ajout de la visibilité de la capsule pour un nouveau groupe
	function insertCapsuleVisibilite() {
		pairform_dao.capsule_dao.insertCapsuleVisibilite(
			visibilite_capsule.id_capsule,
			visibilite_capsule.nouveau_groupe_selectionne.id_groupe,
			visibilite_capsule.nouveau_groupe_selectionne.id_visibilite,
			function () {
				callback(constantes.RETOUR_JSON_OK);				
			},
			callbackError
		);
	}
}

// Mise à jour de la visibilité de toutes les capsules d'une ressource
exports.postCapsuleVisibilitePourToutesCapsulesDeMemeRessource = function(visibilite_capsule, callback) {
	updateCapsuleVisibiliteSpherePubliquePourToutesCapsulesDeMemeRessource();

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}
	// Mise à jour de la visibilité de la capsule pour le groupe obligatoire Sphere publique
	function updateCapsuleVisibiliteSpherePubliquePourToutesCapsulesDeMemeRessource() {
		pairform_dao.capsule_dao.updateCapsuleVisibilitePourToutesCapsulesDeMemeRessource(
			visibilite_capsule.id_capsule,
			visibilite_capsule.sphere_publique.id_groupe,
			visibilite_capsule.sphere_publique.id_visibilite,
			function () {
				updateCapsuleVisibiliteEspacePourToutesCapsulesDeMemeRessource();
			},
			callbackError
		);
	}

	// Mise à jour de la visibilité de la capsule pour le groupe obligatoire Sphere publique
	function updateCapsuleVisibiliteEspacePourToutesCapsulesDeMemeRessource() {
		pairform_dao.capsule_dao.updateCapsuleVisibilitePourToutesCapsulesDeMemeRessource(
			visibilite_capsule.id_capsule,
			visibilite_capsule.espace.id_groupe,
			visibilite_capsule.espace.id_visibilite,
			function () {
				// si un groupe non-obligatoire avait déjà une visibilité sur la capsule
				if (visibilite_capsule.id_ancien_groupe_selectionne) {
					deleteCapsuleVisibilitePourToutesCapsulesDeMemeRessource();
				}
				// sinon, si un nouveau groupe non-obligatoire doit avoir une visibilité sur la capsule
				else if (visibilite_capsule.nouveau_groupe_selectionne) {
					insertCapsuleVisibilitePourToutesCapsulesDeMemeRessource();
				} else {
					callback(constantes.RETOUR_JSON_OK);
					
				}
			},
			callbackError
		);
	}

	// Suppression la visibilité de la capsule pour un groupe
	function deleteCapsuleVisibilitePourToutesCapsulesDeMemeRessource() {
		pairform_dao.capsule_dao.deleteCapsuleVisibilitePourToutesCapsulesDeMemeRessource(
			visibilite_capsule.id_capsule,
			visibilite_capsule.id_ancien_groupe_selectionne,
			function () {
				// si un nouveau groupe non-obligatoire doit avoir une visibilité sur la capsule
				if (visibilite_capsule.nouveau_groupe_selectionne) {
					insertCapsuleVisibilitePourToutesCapsulesDeMemeRessource();
				} else {
					callback(constantes.RETOUR_JSON_OK);					
				}
			},
			callbackError
		);
	}

	// Ajout de la visibilité de la capsule pour un nouveau groupe
	function insertCapsuleVisibilitePourToutesCapsulesDeMemeRessource() {
		pairform_dao.capsule_dao.insertCapsuleVisibilitePourToutesCapsulesDeMemeRessource(
			visibilite_capsule.id_capsule,
			visibilite_capsule.nouveau_groupe_selectionne.id_groupe,
			visibilite_capsule.nouveau_groupe_selectionne.id_visibilite,
			function () {
				callback(constantes.RETOUR_JSON_OK);				
			},
			callbackError
		);
	}
}


// Suppression d'une liste de capsules
exports.deleteCapsules = function(liste_parametres, callback) {
	var liste_capsules = liste_parametres.liste_capsules;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	deleteCapsulesVisibilite();

	// supprime les visibilités
	function deleteCapsulesVisibilite() {
		pairform_dao.capsule_dao.deleteCapsulesVisibilite(
			liste_capsules,
			function () {
				deleteUtilisateursCapsulesParametres();
			},
			callbackError
		);
	}

	// supprime les parametres des utilisateurs
	function deleteUtilisateursCapsulesParametres() {
		pairform_dao.utilisateur_dao.deleteUtilisateursCapsulesParametresByCapsules(
			liste_capsules,
			function () {
				deleteMessagesCapsules();
			},
			callbackError
		);
	}

	// supprime les messages (donc, supprime aussi les notifications, les votes et les tags liés à ces messages) - pyramid of doom !
	function deleteMessagesCapsules() {
		// suppression notifications
		pairform_dao.notification_dao.deleteNofificationsByCapsules(
			liste_capsules,
			function () {
				// suppression votes
				pairform_dao.vote_dao.deleteVotesByCapsules(
					liste_capsules,
					function () {
						// suppression tags
						pairform_dao.tag_dao.deleteTagsByCapsules(
							liste_capsules,
							function () {
								// suppression messages
								pairform_dao.message_dao.deleteMessagesByCapsules(
									liste_capsules,
									function () {
										deleteCapsulesSelecteurs();
									},
									callbackError
								);
							},
							callbackError
						);
					},
					callbackError
				);
			},
			callbackError
		);
	}

	// supprime les selecteurs des capsules
	function deleteCapsulesSelecteurs() {
		pairform_dao.capsule_dao.deleteCapsulesSelecteurs(
			liste_capsules,
			function () {
				deleteCapsules();
			},
			callbackError
		);
	}

	// supprime les capsules
	function deleteCapsules() {
		// suppression en BDD des capsules
		pairform_dao.capsule_dao.deleteCapsules(
			liste_capsules,
			function () {
				// suppression physique des capsules = supprime les fichiers stockés sur le serveur (logo, pages html, etc.)
				for (var i in liste_capsules) {
					// si la capsule existe, on la supprime (version web et version mobile)
					if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + liste_capsules[i].id_capsule)) {
						// suppression du repertoire contenant la capsule
						supprimerRepertoire(constantes.REPERTOIRE_CAPSULE + liste_capsules[i].id_capsule);
					}					
				}

				callback(constantes.RETOUR_JSON_OK);				
			},
			callbackError
		);
	}
};


// enregistre l'archive de la capsule uploadé
function enregistrerArchive(archive, id_espace, id_ressource, id_capsule, version, callback) {
	var url;
	if (version == constantes.VERSION_MOBILE) {
		// si la capsule n'existe pas, on créé le répertoire
		if (!fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule)) {
			fs.mkdirSync(constantes.REPERTOIRE_CAPSULE + id_capsule);
		}
		// sinon si la verson mobile de la capsule existe déjà, on renomme l'archive qui la contient
		else if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ version +".zip")) {
			fs.renameSync(
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ version +".zip", 
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip"
			);
		}

		// on enregistre l'archive mob de la capsule dans : constantes.REPERTOIRE_CAPSULE / id_capsule / mob.zip
		fs.renameSync(
			archive.path, 
			constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ version +".zip"
		);
		url = constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version;

		// s'il y en avait une, suppression de l'ancienne version de la capsule
		if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip"))
			fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip");

	} else {
		// si la version web de la capsule existe déjà, on renomme le répertoire qui la contient
		if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ version)) {
			fs.renameSync(
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ version,
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule
			);
		}

		// dézippage de l'archive web de la capsule dans : constantes.REPERTOIRE_CAPSULE / id_capsule / web
		var zip = new AdmZip(archive.path);
		zip.extractAllTo(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ version, true);
		url = constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ version +"/";
		
		// s'il y en avait une, suppression de l'ancienne version de la capsule
		if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule))
			supprimerRepertoire(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule);

		// suppression de la version temporaire de la capsule
		fs.unlinkSync(archive.path);
	}
	
	callback(url);
}


// Supprime récursivement un repertoire
function supprimerRepertoire(chemin) {
	var fichiers = [];

	if( fs.existsSync(chemin) ) {
		fichiers = fs.readdirSync(chemin);

		fichiers.forEach( function(fichier,index) {
			var chemin_courant = chemin + "/" + fichier;
			
			if(fs.lstatSync(chemin_courant).isDirectory()) {
				supprimerRepertoire(chemin_courant);
			} else {
				// supprimer fichier associé au chemin courant
				fs.unlinkSync(chemin_courant);
			}
		});

		fs.rmdirSync(chemin);
	}
};


// enregistre le contenu d'une capsule au format PairForm
function enregistrerFichiersCapsules(contenu, page_est_modifiee, liste_pages_a_supprimer, sommaire_json, sommaire_est_modifie, css_est_modifie, css_personalise, nom_long, auteurs, clef_pairform, id_espace, id_ressource, nom_ressource, id_capsule, callback) {

	var poid, page, sommaire_html = "<ul>", sommaire_xml = "<?xml version='1.0' encoding='UTF-8'?><outline>";
	var zip;

	// si la capsule n'existe pas, on crée les répertoires
	if (!fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule)) {
		fs.mkdirSync(constantes.REPERTOIRE_CAPSULE + id_capsule);
		fs.mkdirSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE);
		fs.mkdirSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB);

	}
	// sinon si la capsule existe déjà, on renomme le répertoire/archive qui la contient
	else {
		if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip")) {	// version mobile, on fait une copie de l'archive, puis on dézippe
			fs_extra.copySync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip", constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip");
			
			zip = new AdmZip(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip");
			zip.extractAllTo(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE);
		}
		if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB)) {				// version web, on fait une copie du répertoire
			fs_extra.copySync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB, constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule);
		}
	}
	
	// pour chaque page : on créé les sommaires (version XML et HTML), et on enregistre les pages
	async.eachSeries(sommaire_json.liste_pages, function enregistrerPage (page, callback_each) {
		// si le sommaire a été modifié
		if (sommaire_est_modifie) {
			sommaire_xml += "<l u='"+ page.id +".html' t='"+ page.titre +"'/>";
			sommaire_html += "<li><a href='"+ page.id +".html'>"+ page.titre +"</a></li>";
		}

		// si la page a été modifié, ou si c'est une nouvelle page
		if (contenu[page.id] && page_est_modifiee) {
			var datas_capsule = {
				"auteurs" : auteurs,
				"nom_long" : nom_long,
				"nom_page" : page.titre,
				"url_espace" : CONFIG.app.urls.serveur_node + '/doc/' + id_espace,
				"logo_espace" : CONFIG.app.urls.serveur_node + '/res/logos_espaces/' + id_espace + '.png',
				"url_ressource" : CONFIG.app.urls.serveur_node + '/doc/' + id_espace + '/' + id_ressource,
				"logo_ressource" : CONFIG.app.urls.serveur_node + '/res/logos_ressources/' + id_ressource + '.png',
				"nom_ressource" : nom_ressource,
				"contenu_page" : fs.readFileSync(contenu[page.id].path),
				"clef_pairform" : clef_pairform,
				"url_serveur_node" : CONFIG.app.urls.serveur_node
			};

			async.parallel([
				function enregistrerContenuCkeditor(callback_parallel) {
					// si la page existait, on la supprime
					if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/contenu_ckeditor_"+ page.id +".html"))
						fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/contenu_ckeditor_"+ page.id +".html");

					// on sauvegarde le contenu html généré avec ckeditor, on le renverra au client pour les modifications du contenu de la page
					if (contenu[page.id].path != constantes.REPERTOIRE_CAPSULE + id_capsule +"/contenu_ckeditor_"+ page.id +".html")
						fs.renameSync(
							contenu[page.id].path,
							constantes.REPERTOIRE_CAPSULE + id_capsule +"/contenu_ckeditor_"+ page.id +".html"
						);
					
					callback_parallel();
				},
				function creerPageVersionWeb(callback_parallel) {
					// si la page existait, on la supprime
					if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/"+ page.id +".html"))
						fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/"+ page.id +".html");

					associationJade("/../views/template_capsule_web.jade", datas_capsule, function(contenu_capsule) {
						fs.writeFileSync(
							constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/"+ page.id +".html",
							contenu_capsule
						);
						callback_parallel();
					});
				},
				function creerPageVersionMobile(callback_parallel) {
					// si la page existait, on la supprime
					if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ page.id +".html"))
						fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ page.id +".html");

					associationJade("/../views/template_capsule_mob.jade", datas_capsule, function(contenu_capsule) {
						fs.writeFileSync(
							constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ page.id +".html",
							contenu_capsule
						);
						callback_parallel();
					})
				}
			],
			function callbackFinal(err) {
				callback_each(err);
			});
		} else {
			callback_each();
		}
	}, function callback_each_final (err) {
		if (err){
			log.error(err);
			return err;
		}

		// on supprime les fichiers associés aux pages supprimées
		for(var i=0; i<liste_pages_a_supprimer.length; i++) {
			fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/contenu_ckeditor_"+ liste_pages_a_supprimer[i] +".html");
			fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/"+ liste_pages_a_supprimer[i] +".html");
			fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ liste_pages_a_supprimer[i] +".html");
		}
		
		// si le sommaire a été modifié
		if (sommaire_est_modifie) {
			// création de l'index.html (pour la version web)
			fs.readFile(__dirname + "/../views/template_index_capsule.jade", 'utf8', function (err, data) {
				if (err) 
					return next(err);

				var jade_compiler = jade.compile(data);
				var contenu_index_html = jade_compiler({
					"auteurs" : auteurs,
					"nom_long" : nom_long,
					"url_premiere_page" : sommaire_json.liste_pages[0].id +".html"
				});
				fs.writeFileSync(
					constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/index.html",
					contenu_index_html
				);
			});

			// on enregistre les sommaires JSON, XML et HTML
			sommaire_xml += "</outline>";
			// Fermeture du ul
			sommaire_html += "</ul>";
			fs.writeFileSync(
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/outline.xml",
				sommaire_xml
			);
			fs.writeFileSync(
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/outline.html",
				sommaire_html
			);
			fs.writeFileSync(
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/outline.json",
				JSON.stringify(sommaire_json)
			);
			fs.writeFileSync(
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/outline.json",
				JSON.stringify(sommaire_json)
			);
		}
		
		// si le css personnalisé de la capsule à été modifié
		if (css_est_modifie) {
			fs.writeFileSync(
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/css_personalise.css",
				css_personalise
			);
			fs.writeFileSync(
				constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/css_personalise.css",
				css_personalise
			);
		}

		// création du fichier css pour la version mobile (on dublique le fichier contenant le css commun aux versions mobile et web, cf. balise link des fichiers template_capsule_mob/web.jade) 
		fs_extra.copySync(__dirname +"/../../../../../public/css/ckeditor_capsule.css", constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +"/ckeditor_capsule.css");

		// on créé l'archive mobile
		zip = new AdmZip();
		zip.addLocalFolder(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE);
		zip.writeZip(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip");
		supprimerRepertoire(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE);

		// on récupère le poid de l'archive mobile (en Mo), on met 1 Mo minimum
		var stats = fs.statSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_MOBILE +".zip");
		poid = stats["size"] / (1024 * 1024);
		if (poid < 1)
			poid = 1;

		// s'il y en avait une, suppression de l'ancienne version de la capsule mobile
		if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip"))
			fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule +".zip");

		// s'il y en avait une, suppression de l'ancienne version de la capsule web
		if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule))
			supprimerRepertoire(constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ id_capsule);

		callback(
			constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ constantes.VERSION_WEB +"/",
			constantes.URL_REPERTOIRE_CAPSULE + id_espace +"/"+ id_ressource +"/"+ id_capsule +"/"+ constantes.VERSION_MOBILE,
			poid
		);
	})
	
	// associe les champs du template avec les données de la page
	function associationJade(url_template, datas_capsule, callback_association) {
		fs.readFile(__dirname + url_template, 'utf8', function (err, data) {
			if (err) {
				return next(err);
			}
			
			var jade_compiler = jade.compile(data);
			var contenu_capsule = jade_compiler(datas_capsule);

			callback_association(contenu_capsule);
		});
	}
}

// Modification du html pour "pairFormiser" un capsule au format StoryLine 2
function modifierPourStoryline (id_capsule, hash_capsule) {
	var srcpath = constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/story_html5.html",
		dstpath = constantes.REPERTOIRE_CAPSULE + id_capsule +"/"+ constantes.VERSION_WEB +"/index.html";

	log.debug(srcpath + "; " + dstpath);
	async.series([
		function inclusionLigneScriptDansHtml(next) {
			fs.readFile(srcpath, function (err, data) {
				data += '<script type="text/javascript" src="' + CONFIG.app.urls.serveur_node + "/app/"+ hash_capsule+ '"></script>';
				fs.writeFile(srcpath, data, next);
			})
		},
		function creationLienSymbolique(next) {
			fs_extra.ensureSymlink(srcpath, dstpath, next)
		}
		]
	, function callback_final(err, data) {
		if (err) 
			log.error(err);

	})
}

// Converti le contenu d'un fichier (.docx, ...) en HTML via Pandoc 
exports.convertirFichierToHTML = function (liste_parametres, liste_uploads, callback, callback_end, callback_error) {
	var spawn = require('child_process').spawn,
		root_dir = require("express")().get("root_dir"),
		file = liste_uploads.doc,
		cmd = 'bin/pandoc-1.19.2.1';

	var format = constantes.CONVERSION_DOCUMENT_TYPES_MIME[file.mimetype];

	//Si le format n'est pas autorisé
	if (!format){
		fs.unlink(file.path);
		return callback_error({
			status:"ko", 
			message:"format_incorrect"
		});
	}

	var args = [file.path, '-f', format, '-t', 'html5', '--template', __dirname + '/../views/template_pandoc.html', '--section-divs', '--self-contained'];
	var pandoc = spawn(cmd, args);

	pandoc.stdout.on('data', function (data) {
		callback(data);
	})
	pandoc.stderr.on('data', function (data) {
		log.error('stderr: ' + data);
	});
	pandoc.on('exit', function (code) {
		log.debug('Conversion réussie (exit code ' + code + ')');
		fs.unlink(file.path);
		callback_end();
	});
}

function notifierCoAdministrateursCapsuleCreation(id_ressource, id_capsule, nom_capsule) {
	notifierCoAdministrateursCapsule("create", id_ressource, id_capsule, nom_capsule);
}
function notifierCoAdministrateursCapsuleUpdate(id_ressource, id_capsule, nom_capsule) {
	notifierCoAdministrateursCapsule("update", id_ressource, id_capsule, nom_capsule);
}
function notifierCoAdministrateursCapsule(create_or_update, id_ressource, id_capsule, nom_capsule) {
	if ((create_or_update != "create") && (create_or_update != "update"))
		return console.trace("Mauvaise utilisation de la fonction notifierCoAdministrateursCapsule : update ou create doit être passé en premier paramètre");

	async.parallel({
	  co_administrateurs: function getCoAdministrateurs (next){
			//Récupération des utilisateurs concernés
			pairform_dao.membre_dao.selectCoAdministrateursParRessource(id_ressource,
				function (co_administrateurs) {
					if (co_administrateurs && co_administrateurs.length > 0)
      			next(null, co_administrateurs);
      		else
      			//On sort, pas d'utilisateurs à notifier
      			next("no_users");
			}, next)
	  },
	  ressource: function getNomRessource (next){
			//Récupération des informations sur la ressource ?
      pairform_dao.ressource_dao.selectRessource(id_ressource,
				function (ressource) {
					if (ressource)
      			next(null, ressource);
      		else
      			//On sort, pas d'utilisateurs à notifier
      			next("no_users");
			}, next)
	  }
	}, function (err, result) {
	  if (err)
	  	console.error(err);
	  else{
	  	//Boucle sur tous les utilisateurs pour insertion notification
	  	for (var i = result.co_administrateurs.length - 1; i >= 0; i--) {
	  		var co_administrateur = result.co_administrateurs[i];
	  		if (create_or_update == "create")
	  			api_rs.documentCreate(co_administrateur.id_utilisateur, id_capsule, nom_capsule, result.ressource.nom_long);
	  		else if (create_or_update == "update")
	  			api_rs.documentUpdate(co_administrateur.id_utilisateur, id_capsule, nom_capsule, result.ressource.nom_long);
	  	}
	  }
	});
	
}