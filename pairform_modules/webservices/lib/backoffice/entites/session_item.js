"use strict";

var pairform_dao = require("pairform_dao"),
        schedule = require("node-schedule"),
       async = require("async"),
  web_services = require("../../webServices"),
    constantes = require("../../constantes"),
        fs = require('fs'),
        jade = require('jade'),
        CONFIG = require("config"),
        cluster = require("cluster"),
      nodemailer = require('nodemailer'),
        log = require("metalogger")();

// Création d'une nouvel item vide
exports.putItem = function(param, callback) {
  function callbackError() {
    callback(constantes.RETOUR_JSON_KO);
  }
  log.debug(param);
  //Check des paramètres
  var _item = {
    id_session : param.id_session,
    id_utilisateur : param.id_utilisateur,
    id_capsule : param.id_capsule,
    date_diffusion : param.date_diffusion,
    description : param.description,
    icone : param.icone,
    sub_type : param.sub_type,
    titre : param.titre,
    type : param.type,
    type_icone : param.type_icone,
    url : param.url,
    diffusion_automatique : typeof param.diffusion_automatique == "undefined" ? 0 : param.diffusion_automatique
  };
  pairform_dao.session_item_dao.insertItem(
    _item,
    function (retour_sql) {
      var id_item = retour_sql.insertId;
      callback({
        status: "ok",
        data : {
          id_item : id_item
        }
      });

      //Si la diffusion automatique est choisie
      if (_item.diffusion_automatique) {
        exports.scheduleItem(_item);
      }
      //Sinon, on annule
      else {
        exports.cancelScheduleItem(_item);
      }
    },
    callbackError
  );
};



// MAJ d'un item
exports.postItem = function(param, callback) {
  function callbackError() {
    callback(constantes.RETOUR_JSON_KO);
  }

  //Check des paramètres
  var _item = {
    date_diffusion : param.date_diffusion,
    description : param.description,
    titre : param.titre,
    url : param.url,
    diffusion_automatique : param.diffusion_automatique
  };
  pairform_dao.session_item_dao.updateItem(
    param.id_item,
    _item,
    function (retour_sql) {
      callback({
        status: "ok"
      });
      //On remet l'id_item pour les fonctions suivantes
      _item.id_item = param.id_item;

      //Si la diffusion automatique est choisie
      if (_item.diffusion_automatique) {
        exports.scheduleItem(_item);
      }
      //Sinon, on annule
      else {
        exports.cancelScheduleItem(_item);
      }
    },
    callbackError
  );
}

// Suppression d'un item
exports.deleteItem = function(param, callback) {
  function callbackError() {
    callback(constantes.RETOUR_JSON_KO);
  }

  pairform_dao.session_item_dao.deleteItem(
    param.id_item,
    function (retour_sql) {
      callback({
        status: "ok"
      });
      exports.cancelScheduleItem(param);
    },
    callbackError
  );
}

//Synchronisation des clusters : il faut absolument que ce soit le cluster master
//qui fasse la planification, parce que l'objet schedule a une liste de tous les clusters
//planifié (fait au lancement) ; pour reschedule / suppression, il faut être certain 
//d'avoir le bon objet schedule, ce qui n'est pas le cas sur un environnement clusteré 
// Traite les messages de workers envoyé comme ça : cluster.worker.send(item);
// item doit avoir la propriété handler, qui doit être une fonction dans exports.
//https://nodejs.org/api/cluster.html
if (cluster.isMaster) {
  cluster.on('message', function (worker, item, handle) {
    //Si on passe une session
    log.debug("[Cluster] Master reçoit l'item : ", item);
    //Il faut
    if (item.id_item && item.handler){
      exports[item.handler](item);
    }
  });
}

//Planification d'un envoi de mail à un utilisateur
//item doit avoir la propriété id_groupe
exports.scheduleItem = function (item, callback) {
  //On ne gère les schedule que sur le cluster principal
  if (cluster.isWorker) {
    item.handler = "scheduleItem";
    log.debug("[Cluster] Cluster envoie l'item : ", item);
    return cluster.worker.send(item);
  }

  var id_groupe = item.id_groupe;
  
  //item doit avoir la propriété id_groupe
  if (typeof id_groupe == "undefined") {
    //On va chercher l'id_groupe en base
    return pairform_dao.session_item_dao.selectItemForDiffusion(
      item.id_item,
      function (retour_sql) {
        log.debug(retour_sql);
        //Et on relance la fonction si on l'a
        if(retour_sql && retour_sql.length){
          var _item = retour_sql[0];
          exports.scheduleItem(_item, callback);
        }
        else
          callback && typeof callback == "function" && callback("no_id_groupe");
      },
      function (err) {
        callback && typeof callback == "function" && callback("no_id_groupe");
      }
    );
  }
  //On check qu'il n'y a pas déjà de cron prévu
  var item_diffusion = schedule.scheduledJobs[constantes.CRON_ITEM_IDENTIFIER + item.id_item];
  
  // log.debug("Job name : ", cron_job_name);
  // log.debug("schedule.scheduledJobs : ", Object.keys(schedule.scheduledJobs));
  //S'il y en a déjà, on reschedule, et basta
  if (item_diffusion) {
    return exports.rescheduleItem(item);
  }


  var date_diffusion = new Date(parseInt(item.date_diffusion));

  log.debug("[CRON] ", "Item #%d (%d): diffusion le %s ", item.id_item, item.diffusion_automatique, date_diffusion.toString());

  //Utilisation d'un identifiant pour annuler le cron éventuellement plus tard,
  //si l'événement est supprimé ou modifié.
  //https://stackoverflow.com/a/33315292/1437016
  var item_diffusion = schedule.scheduleJob(constantes.CRON_ITEM_IDENTIFIER + item.id_item, date_diffusion, function(_item){
    async.waterfall([
      function recuperationItem(next){
        // Récupération des dernières infos de l'item en base de données        
        pairform_dao.session_item_dao.selectItem(
          _item.id_item,
          function callbackSuccess(retour_sql) {
            //Très bizarre si on arrive là : l'item a été supprimé sans que la tache cron ne l'ai été
            if(retour_sql.length == 0)
              return next("no_item");
            
            var item = retour_sql[0];
            // log.debug(utilisateurs);
            // Ouverture des droits
            
            next(null, item);

          },
          function (err) {
            next("no_item");
          }
        )
      },
      function recuperationUtilisateurs (item, next){
        //Récupération des utilisateurs
        pairform_dao.utilisateur_dao.selectUtilisateursFromGroupe(
          id_groupe,
          function callbackSuccess(utilisateurs) {
            if(utilisateurs.length == 0)
              return next("no_users");
            item.utilisateurs = utilisateurs;
            
            next(null, item);

          },
          function (err) {
            next("no_users");
          }
        )
      },
      // Ouverture des droits
      function ouvertureDroits(item, next){
        //Si c'est un document (capsule)
        if (item.type == "document") {
          //On ouvre la visibilité du document en groupe en question
          //Insert ou update si besoin
          pairform_dao.capsule_dao.insertIgnoreCapsuleVisibilite(
            item.id_capsule,
            item.id_groupe,
            constantes.VISIBILITE_ACCESSIBLE,
            function callbackSuccess(retour_sql) {
              log.debug(retour_sql);
              next(null, item);

            },
            function (err) {
              next("no_capsule");
            }
          )
        }
        else
          next(null, item);
      },
      function recuperationInfosExtra (item, next){
        if (item.diffusion_automatique) {
          //Récupération d'infos supplémentaires pour mail : nom et logo_espace, nom ressource et logo, nom session
          pairform_dao.session_dao.selectInfosForDiffusion(
            item.id_session,
            function callbackSuccess(retour_sql) {
              if(retour_sql.length == 0)
                return next("no_infos");

              item.extra = retour_sql[0];
              
              next(null, item);

            },
            function (err) {
              next("no_infos");
            }
          )
        }
        else
          next(null, item);
      },
      function envoiEmail(item, next){
        // Envoi de mail si l'option est cochée
        if (item.diffusion_automatique) {
          var smtp_transport = nodemailer.createTransport(CONFIG.app.smtp);

          fs.readFile(global.pairform_modules + 'cron/node_modules/session_item/views/session_item.jade', 'utf8', function (err, data) {
            if (err) 
              return next(err);

            var jade_compiler = jade.compile(data);

            async.eachLimit(item.utilisateurs, 10, function (utilisateur, done) {
              // Utilisateur a une adresse validée
              if (utilisateur.email_est_valide) {
                log.debug(utilisateur);
                var mail_body = jade_compiler({
                  "utilisateur" : utilisateur,
                  "item" : item,
                  "extra" : item.extra,
                  "url_serveur_node" : CONFIG.app.urls.serveur_node
                });

                var mail_options = {
                  to: utilisateur.email,
                  // to: "mjuganaikloo@gmail.com",
                  from: 'noreply@pairform.fr',
                  replyTo: 'contact@pairform.fr',
                  subject: '['+ item.extra.nom_ressource +'] ' + item.titre,
                  html: mail_body
                };

                smtp_transport.sendMail(mail_options, function(err) {
                  // On passe au suivant
                  if (!err) 
                    log.debug("### CRON SESSION : Mail envoyé à " + utilisateur.pseudo + " (" + utilisateur.email + ")");
                  else{
                    log.error("!!! CRON SESSION : Mail non envoyé à " + utilisateur.pseudo + " (" + utilisateur.email + ")");
                    log.error(err);
                  }

                  done(err);
                });
                  
              }
              else
                //On passe au suivant
                done();

            }, next);
          });

        }
        else {
          next(null);
        }
      }
    ], function (err, result) {
      // result now equals 'done'    
      if(err)
        log.debug("[CRON] ", "Item #%d : erreur diffusion", item.id_item, err);

      else
        log.debug("[CRON] ", "Item #%d : diffusion OK ", item.id_item);
    });
    
  }.bind(null, item));

  // log.debug(item_diffusion);
  callback && typeof callback == "function" && callback(null, item_diffusion);

}

exports.rescheduleItem = function (item, callback) {
  //On ne gère les schedule que sur le cluster principal
  if (cluster.isWorker) {
    item.handler = "rescheduleItem";
    log.debug("[Cluster] Cluster envoie l'item : ", item);
    return cluster.worker.send(item);
  }
  //Si l'item avait une diffusion prévue
  var item_diffusion = schedule.scheduledJobs[constantes.CRON_ITEM_IDENTIFIER + item.id_item];
  
  if (item_diffusion) {
    log.debug("Reschedule Item", " #%d : %s", item.id_item, item.date_diffusion);
    //On reprogramme
    var date_diffusion = new Date(parseInt(item.date_diffusion)),
      success = item_diffusion.reschedule(date_diffusion);
      //success = schedule.rescheduleJob(item_diffusion, date_diffusion);

    callback && typeof callback == "function" && callback(success);
  }
  else 
    callback && typeof callback == "function" && callback("no_cron");
}

exports.cancelScheduleItem = function (item, callback) {
  //On ne gère les schedule que sur le cluster principal
  if (cluster.isWorker) {
    item.handler = "cancelScheduleItem";
    log.debug("[Cluster] Cluster envoie l'item : ", item);
    return cluster.worker.send(item);
  }
  //Si l'item avait une diffusion prévue
  var item_diffusion = schedule.scheduledJobs[constantes.CRON_ITEM_IDENTIFIER + item.id_item];

  if (item_diffusion) {
    log.debug("Suppression schedule Item", " #%d ", item.id_item);
    //On l'annule
    item_diffusion.cancel();
    callback && typeof callback == "function" && callback(true);
  }
  else 
    callback && typeof callback == "function" && callback("no_cron");
}