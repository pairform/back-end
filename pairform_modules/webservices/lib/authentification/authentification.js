"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes"),
			 log = require('metalogger')(),
		   async = require("async"),
	  nodemailer = require("nodemailer"),
	  		jade = require('jade'),
    		  fs = require('fs'),
		  bCrypt = require("bcrypt-nodejs"),
		  crypto = require("crypto"),
		  CONFIG = require("config");


// resetPassword.php
exports.validerEmail = function(param, callback) {
	var token = param.token || "";

	pairform_dao.getConnexionBDD(function (_connexion_courante){
		async.waterfall([
			function recuperationUtilisateur (done) {
				pairform_dao.utilisateur_dao.selectUtilisateurAvecTokenValidation(
					token,
					function (retour_sql) {
						//Si on ne trouve pas l'utilisateur avec ce token 
						if (!retour_sql.length) {
							//C'est que le token est soit faux, soit périmé
							pairform_dao.libererConnexionBDD(_connexion_courante);
							//On envoie un token expiré
							done(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
						}
						else{
							//Sinon, ça roule
							var utilisateur = retour_sql[0];
							//Et on va enregistrer le mot de passe
							done(null, utilisateur);
						}
					},
					//S'il y a une erreur pendant la requete
					done
				);
			},
			function validerEmail (utilisateur, done) {
				pairform_dao.utilisateur_dao.updateEmailValidationUtilisateur(
					utilisateur.id_utilisateur,
					function (retour_sql) {
						//Si on ne trouve pas l'utilisateur avec ce token 
						if (!retour_sql.changedRows) {
							//C'est que le token est soit faux, soit périmé
							pairform_dao.libererConnexionBDD(_connexion_courante);
							//On envoie un token expiré
							done({
								"status" : "ko",
								"message" : "ws_utilisateur_deja_valide"
							});
						}
						else{
							done(null, utilisateur);
						}
					},
					//S'il y a une erreur pendant la requete
					done
				);
				
			}], function (err, utilisateur) {
				//Gestion des erreurs
				if (err) {
					//Si on est sur une erreur syst§me
					if (typeof err == "Object") {
						log.error(err);
					}
					//On renvoie le message associé
					callback({
						"status" : "ko",
						"message" : err.message
					});
					
				}
				else {
					callback({
						"status" : "ok",
						"data" : {
							"utilisateur" : utilisateur
						}
					});
				}
			}
		);
	});
};

// resetPassword.php
exports.resetPassword = function(param, callback) {
	var email_ou_pseudo = param.email || param.username;

	pairform_dao.getConnexionBDD(function (_connexion_courante){
		async.waterfall([
			function generationToken (done) {
				crypto.randomBytes(20, function(err, buf) {
					var token = buf.toString('hex');
					done(err, token);
				});
			},
			function verificationUtilisateur (token, done) {

				//Récuperation de l'utilisateur correspondant à l'adresse e-mail
				pairform_dao.utilisateur_dao.selectUtilisateurPourResetPassword(
					email_ou_pseudo,
					function callback_success (retour_sql) {
						if (!retour_sql.length){
							//Erreur à gérer
							done(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);

							pairform_dao.libererConnexionBDD(_connexion_courante);
						}
						else{
							var utilisateur = retour_sql[0];


							//S'il est connecté avec Linkedin
							if (utilisateur.provider == "linkedin") {
								return done({
									'status' : 'ko', 
									'message' : 'ws_utilisateur_linkedin'
								});
							}

							//Vérification de la validation de l'email de l'utilisateur
							//S'il l'a bien validé
							if (utilisateur["email_est_valide"] == 1) {
								done(null, token, utilisateur);
							}
							//Sinon, on le notifie
							else{
								done({
									'status':'ko',
									'message' : 'ws_mail_utilisateur_pas_valide'
								});
							}
						}
					},
					done
				);
			},
			function updateTokenResetUtilisateur (token, utilisateur, done) {
				pairform_dao.utilisateur_dao.updateTokenResetUtilisateur(
					utilisateur.id_utilisateur,
					token,
					function callback_success (retour_sql) {
						pairform_dao.libererConnexionBDD(_connexion_courante);
						//Si l'utilisateur n'a pas été trouvé, pas de token changé
						if (!retour_sql.changedRows) {
							//Erreur à gérer
							done(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);

						}
						//Si le token a bien été mis à jour
						else{
							//Envoi de l'email à l'utilisateur
							done(null, token, utilisateur);
						}
					},
					done
				);
			},
			function envoiLienReset (token, utilisateur, done) {
				var smtp_transport = nodemailer.createTransport(CONFIG.app.smtp);

				fs.readFile(__dirname + '/../../../authentification/views/email_templates/compte_reset_phase_1.jade', 'utf8', function (err, data) {
				    if (err) 
				    	return done(err);
				    var jade_compiler = jade.compile(data);

				    var url_reset = CONFIG.app.urls.serveur_node + "/reset/" + token;

				    var mail_body = jade_compiler({
				    	"utilisateur" : utilisateur,
				    	"url_reset" : url_reset
				    });


					var mail_options = {
						to: utilisateur.email,
						from: 'noreply@pairform.fr',
						subject: '[PairForm] Demande de réinitialisation de votre mot de passe',
						html: mail_body
					};

					smtp_transport.sendMail(mail_options, function(err) {
						done(err);
					});
				});
			}], function (err) {
				//Gestion des erreurs
				if (err) {
					//Si on est sur une erreur provoquée
					if (err.status = "ko") {
						//On renvoie le message associé
						callback(err);
					}
					else{
						log.error(err);
						//On renvoie le message associé
						callback({
							"status" : "ko",
							"message" : err.message
						});
					}
					
				}
				else{
					callback({
						"status" : "ok"
					});
				}
			}
		);
	});
};


// resetPasswordStep2.php
exports.resetPasswordStep2 = function(param, callback) {
	var token = param.token || "";

	pairform_dao.getConnexionBDD(function (_connexion_courante){
		async.waterfall([
			function recuperationUtilisateur (done) {
				pairform_dao.utilisateur_dao.selectUtilisateurAvecTokenReset(
					token,
					function (retour_sql) {
						//Si on ne trouve pas l'utilisateur avec ce token 
						if (!retour_sql.length) {
							//C'est que le token est soit faux, soit périmé
							pairform_dao.libererConnexionBDD(_connexion_courante);
							//On envoie un token expiré
							done(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
						}
						else{
							//Sinon, ça roule
							var utilisateur = retour_sql[0];
							//Et on va enregistrer le mot de passe
							done(null, utilisateur);
						}
					},
					//S'il y a une erreur pendant la requete
					done
				);
			},
			function generationPassword (utilisateur, done) {
				crypto.randomBytes(8, function(err, buf) {
					var password = buf.toString('hex');
					done(err, utilisateur, password);
				});
			},
			function enregistrementPasswordUtilisateur (utilisateur, password, done) {
				var salt = bCrypt.genSaltSync(),
					password_hash = bCrypt.hashSync(password, salt);

				pairform_dao.utilisateur_dao.updatePasswordUtilisateur(
					utilisateur.id_utilisateur,
					password_hash,
					function (retour_sql) {
						//Si on ne trouve pas l'utilisateur avec ce token 
						if (!retour_sql.changedRows) {
							//C'est que le token est soit faux, soit périmé
							pairform_dao.libererConnexionBDD(_connexion_courante);
							//On envoie un token expiré
							done(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
						}
						else{
							//On envoie le mot de passe par e-mail
							done(null, utilisateur, password);
						}
					},
					//S'il y a une erreur pendant la requete
					done
				);
				
			},
			function envoiPasswordGenere (utilisateur, password, done) {
				var smtp_transport = nodemailer.createTransport(CONFIG.app.smtp);


				fs.readFile(__dirname + '/../../../authentification/views/email_templates/compte_reset_phase_2.jade', 'utf8', function (err, data) {
				    if (err) 
				    	return done(err);
				    var jade_compiler = jade.compile(data);

				    var mail_body = jade_compiler({
				    	"utilisateur" : utilisateur,
				    	"password" : password
				    });


					var mail_options = {
						to: utilisateur.email,
						from: 'noreply@pairform.fr',
						subject: '[PairForm] Demande de réinitialisation de votre mot de passe',
						html: mail_body
					};

					smtp_transport.sendMail(mail_options, function(err) {
						done(err);
					});
				});
			}], function (err) {
				//Gestion des erreurs
				if (err) {
					//Si on est sur une erreur syst§me
					if (typeof err == "Object") {
						log.error(err);
					}
					//On renvoie le message associé
					callback({
						"status" : "ko",
						"message" : err.message
					});
					
				}
				else {
					callback({
						"status" : "ok"
					});
				}
			}
		);
	});
};

exports.setTokenValidation = function(utilisateur, callback) {
	log.debug("Utilisateur demande un lien de validation : ", utilisateur);
	async.waterfall([
		function generationToken (done) {
			crypto.randomBytes(20, function(err, buf) {
				var token = buf.toString('hex');
				done(err, token);
			});
		},
		function updateTokenValidationUtilisateur (token, done) {
			pairform_dao.utilisateur_dao.updateTokenValidationUtilisateur(
				utilisateur.id_utilisateur,
				token,
				function callback_success (retour_sql) {
					//Si l'utilisateur n'a pas été trouvé, pas de token changé
					if (!retour_sql.changedRows) {
						//Erreur à gérer
						done(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);

					}
					//Si le token a bien été mis à jour
					else{
						//Envoi de l'email à l'utilisateur
						done(null, token);
					}
				},
				done
			);
		}], function (err, token) {
			//Gestion des erreurs
			if (err) {
				//Si on est sur une erreur syst§me
				if (typeof err == "Object") {
					log.error(err);
				}
				//On renvoie le message associé
			}
			callback(err, token);		
		}
	);

}
exports.editerCompte = function(param, passport, callback) {
	function callbackError() {
		callback({
			"status" : "ko",
			"message" : "ws_erreur_suppression"
		});
	}
	//Récupération des autres params du formulaire utilisateur, en trimant tout et en echappant les textes
	//TODO : echapper les textes
	var id_utilisateur = param.id_utilisateur,
		nom_prenom = param.name ? param.name.trim() : null,
		etablissement = param.etablissement ? param.etablissement.trim() : null,
		current_password = param.current_password ? param.current_password.trim() : null,
		password = param.password ? param.password.trim() : null,
		password2 = param.password2 ? param.password2.trim() : null,
		id_langue_principale = param.langue ? param.langue : null,
		array_id_autre_langues = param.autres_langues ? param.autres_langues.trim() : [],
		salt = null;
		
		// TODO : async pour synchroniser les modifications 
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			pairform_dao.utilisateur_dao.selectUtilisateurPourLoginAvecId(id_utilisateur, 
				function callback_success (retour_sql) {
					// L'utilisateur n'existe pas...
					if (!retour_sql.length)
						callbackError();

					var utilisateur = retour_sql[0];


					//S'il n'y a aucun champ de passé
					if (!nom_prenom && !etablissement && !current_password && !password && !password2 && !id_langue_principale && !array_id_autre_langues.length)
						return callback({
							"status" : "ko",
							'message' : 'label_erreur_saisie_champ_vide'
						});

					else{
						async.parallel([
							function majPassword (done) {
								//Si l'utilisateur tente de changer son mot de passe
								if(current_password || password || password2)
								{
									//Mais qu'il manque un des champs obligatoires
									if(!current_password || !password || !password2){
										return done({
											"status" : "ko",
											'message' : 'label_erreur_saisie_champ_vide'
										});
									}
									
									//Si la confirmation de mot de passe est eronnée
									if(password != password2)
										return done({
											"status" : "ko",
											'message' : 'label_erreur_saisie_mot_de_passe_confirmation'
										});

									passport.validerPassword(utilisateur, current_password, 
										function callback_success () {
											var bCrypt = require("bcrypt-nodejs");
											//Sinon, on génère un mot de passe
											var salt = bCrypt.genSaltSync(),
												password_hash = bCrypt.hashSync(password, salt);

											//On affecte le nouveau password hashé à la variable password
											//Password est soit null, soit affecté au password hashé
											//Parce qu'elle est utilisée dans la requête d'update de toute façon
											password = password_hash;

											done(null, "password");
											
										},
										function callback_error (message) {
											//La connexion n'est pas libérée automatiquement, parce qu'on n'est pas dans le cas d'une erreur SQL
											pairform_dao.libererConnexionBDD(_connexion_courante);	

											return done({
												"status" : "ko",
												'message' : 'label_erreur_saisie_mot_de_passe'
											});
										}
									);
								}
								else{
									done();
								}
							},
							function majLangues (done) {
								if (id_langue_principale) {
									var langues = [[id_utilisateur, id_langue_principale, 1]];

									array_id_autre_langues.forEach(function (id_langue){
										langues.push([id_utilisateur, id_langue, 0]);
									});
									pairform_dao.utilisateur_dao.deleteLanguesUtilisateur(
										id_utilisateur,
										function (retour_sql) {
											pairform_dao.utilisateur_dao.insertLanguesUtilisateur(
												langues,
												function (retour_sql) {
													done(null, "langue");
												},
												callbackError
											);
										},
										callbackError
									);
								}
								else{
									done();
								}
								
							}
						],
						function (err, result) {
							if (err) {
								callback(err);
							} else {
								pairform_dao.utilisateur_dao.updateUtilisateur(id_utilisateur, etablissement, nom_prenom, password, salt,
									function callback_success () {
										pairform_dao.libererConnexionBDD(_connexion_courante);	

										callback({
											"status" : "ok"
										});
									},
								callbackError);
							}
						});
						
					}
				}	

				,callbackError
			);
		});


};

exports.editerPseudo = function(param, callback) {
	var id_utilisateur = param.id_utilisateur,
		pseudo = param.pseudo ? param.pseudo.trim() : null;
	
	log.debug(param);
	
	pairform_dao.utilisateur_dao.updatePseudo(
		id_utilisateur,
		pseudo,
		function (retour_sql) {
			log.debug(retour_sql);
			callback({
				"status" : "ok"
			});
		},
		function callbackError (err) {
			log.error(err);
			//On renvoie le message associé
			callback({
				"status" : "ko",
				"message" : err
			});
		}
	);
};
