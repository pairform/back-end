/*"use strict";

var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes"),
			 log = require('metalogger')(),
		 adler32 = require('adler32'),
		  AdmZip = require("adm-zip"),
			  fs = require("fs"),
		fs_extra = require("fs-extra"),
			   _ = require("underscore"),
		   async = require("async"),
		 request = require("request"),
			jade = require("jade"),
		  CONFIG = require("config");


exports.postCapsuleFormatPF = function(callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}

	//var liste_capsules = [{"id_capsule": 490,"clef_pairform": "90409a1c","id_format": 0,"id_ressource": 190,"nom_court": "le modèle d\'Henry Mintzberg","nom_long": "le modèle d\'Henry Mintzberg","url_mobile": "https://www.pairform.fr/doc/62/190/490/mob","url_web": "https://www.pairform.fr/doc/62/190/490/web/","taille": 1,"description": "","licence": "CC","auteurs": "anne-marie feron","cree_par": 15477,"date_creation": "1481395137059","date_edition": null,"pseudo_createur": "amferon"}, {"id_capsule": 491,"clef_pairform": "7757b1af","id_format": 0,"id_ressource": 156,"nom_court": "test","nom_long": "test","url_mobile": "https://www.pairform.fr/doc/57/156/491/mob","url_web": "https://www.pairform.fr/doc/57/156/491/web/","taille": 1,"description": "","licence": "cc","auteurs": "anne-marie feron","cree_par": 14043,"date_creation": "1481395137059","date_edition": null,"pseudo_createur": "open"}, {"id_capsule": 493,"clef_pairform": "d632a5c7","id_format": 0,"id_ressource": 191,"nom_court": 20161208,"nom_long": 20161208,"url_mobile": "https://www.pairform.fr/doc/76/191/493/mob","url_web": "https://www.pairform.fr/doc/76/191/493/web/","taille": 1,"description": "Relevé de décisions de la rencontre des 8 et 9 décembre à Nantes","licence": "TDR","auteurs": "ChC","cree_par": 255,"date_creation": "1481395137059","date_edition": "2016-12-13 18:10:43","pseudo_createur": "Gribouillis"}]
	//migrerCapsulesFormatPF(liste_capsules);
	recupererCapsulesFormatPF();


	function recupererCapsulesFormatPF() {
		pairform_dao.requete(
			"SELECT "+
				"cc.id_capsule, cc.clef_pairform, cc.id_format, cc.id_ressource, cc.nom_court, cc.nom_long, cc.url_mobile, cc.url_web, cc.poid AS 'taille', cc.description, cc.licence, cc.auteurs, cc.cree_par, "+
				"UNIX_TIMESTAMP(cc.date_creation) AS date_creation, "+
				"cu.pseudo AS pseudo_createur "+
			"FROM cape_capsules AS cc "+
			"JOIN cape_utilisateurs_v2 AS cu ON cc.cree_par = cu.id_utilisateur "+
			"WHERE cc.id_format = 0;",
			function (liste_capsules) {
				migrerCapsulesFormatPF(liste_capsules);
			},
			callbackError
		);
	}

	function migrerCapsulesFormatPF(liste_capsules) {
		var capsule;
		
		// pour chaque capsule
		async.each(liste_capsules, function enregistrerPage (capsule, callback_each) {
			var zip,
			sommaire_json = {
				"liste_pages":[{
					"id": capsule.date_creation,
					"titre": capsule.nom_long,
					"position":"0"
				}]
			},
			sommaire_html = "",
			sommaire_xml = "<?xml version='1.0' encoding='UTF-8'?><outline>";

			// la capsule existe déjà, on renomme le répertoire/archive qui la contient
			if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE +".zip")) {	// version mobile, on fait une copie de l'archive, puis on dézippe
				fs_extra.copySync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE +".zip", constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ capsule.id_capsule +".zip");
				
				zip = new AdmZip(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE +".zip");
				zip.extractAllTo(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE);
			}
			if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_WEB)) {				// version web, on fait une copie du répertoire
				fs_extra.copySync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_WEB, constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ capsule.id_capsule);
			}	
			// création du fichier css pour la version mobile (on dublique le fichier contenant le css commun aux versions mobile et web, cf. balise link des fichiers template_capsule_mob/web.jade) 
			fs_extra.copySync(__dirname +"/../../../../public/css/ckeditor_capsule.css", constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE +"/ckeditor_capsule.css");
			
			sommaire_xml += "<l u='"+ capsule.date_creation +".html' t='"+ capsule.nom_long +"'/>";
			sommaire_html += "<li><a href='"+ capsule.date_creation +".html'>"+ capsule.nom_long +"</a></li>";
			
			// si la page a été modifié, ou si c'est une nouvelle page
			var datas_capsule = {
				"auteurs" : capsule.auteurs,
				"nom_long" : capsule.nom_long,
				"contenu_page" : fs.readFileSync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/contenue_ckeditor.html"),
				"clef_pairform" : capsule.clef_pairform,
				"url_serveur_node" : CONFIG.app.urls.serveur_node
			};

			async.parallel([
				function enregistrerContenuCkeditor(callback_parallel) {
					fs.renameSync(
						constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/contenue_ckeditor.html",
						constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/contenu_ckeditor_"+ capsule.date_creation +".html"
					);
					callback_parallel();
				},
				function creerPageVersionWeb(callback_parallel) {
					associationJade("/../backoffice/views/template_capsule_web.jade", datas_capsule, function(contenu_capsule) {
						fs.writeFileSync(
							constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_WEB +"/"+ capsule.date_creation +".html",
							contenu_capsule
						);
						callback_parallel();
					});
				},
				function creerPageVersionMobile(callback_parallel) {
					associationJade("/../backoffice/views/template_capsule_mob.jade", datas_capsule, function(contenu_capsule) {
						fs.writeFileSync(
							constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE +"/"+ capsule.date_creation +".html",
							contenu_capsule
						);
						callback_parallel();
					})
				}
			],
			function callbackFinal(err) {
				if (err)
					callback_each(err);

				// création de l'index.html (pour la version web)
				fs.readFile(__dirname + "/../backoffice/views/template_index_capsule.jade", 'utf8', function (err, data) {
					if (err) 
						return next(err);

					var jade_compiler = jade.compile(data);
					var contenu_index_html = jade_compiler({
						"auteurs" : capsule.auteurs,
						"nom_long" : capsule.nom_long,
						"url_premiere_page" : capsule.date_creation +".html"
					});
					fs.writeFileSync(
						constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_WEB +"/index.html",
						contenu_index_html
					);
				});

				// on enregistre les sommaires JSON, XML et HTML
				sommaire_xml += "</outline>";
				fs.writeFileSync(
					constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE +"/outline.xml",
					sommaire_xml
				);
				fs.writeFileSync(
					constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_WEB +"/outline.html",
					sommaire_html
				);
				fs.writeFileSync(
					constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE +"/outline.json",
					JSON.stringify(sommaire_json)
				);
				fs.writeFileSync(
					constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/outline.json",
					JSON.stringify(sommaire_json)
				);

				// on créé l'archive mobile
				zip = new AdmZip();
				zip.addLocalFolder(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE);
				zip.writeZip(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE +".zip");
				supprimerRepertoire(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ constantes.VERSION_MOBILE);

				// s'il y en avait une, suppression de l'ancienne version de la capsule mobile
				if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ capsule.id_capsule +".zip"))
					fs.unlinkSync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ capsule.id_capsule +".zip");

				// s'il y en avait une, suppression de l'ancienne version de la capsule web
				if (fs.existsSync(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ capsule.id_capsule))
					supprimerRepertoire(constantes.REPERTOIRE_CAPSULE + capsule.id_capsule +"/"+ capsule.id_capsule);

				callback_each();
			});
		}, function callback_each_final (err) {
			if (err)
				log.error(err);

			callback(retour_json);
		})
		
		// associe les champs du template avec les données de la page
		function associationJade(url_template, datas_capsule, callback_association) {
			fs.readFile(__dirname + url_template, 'utf8', function (err, data) {
				if (err) {
					return next(err);
				}
				
				var jade_compiler = jade.compile(data);
				var contenu_capsule = jade_compiler(datas_capsule);

				callback_association(contenu_capsule);
			});
		}

		// Supprime récursivement un repertoire
		function supprimerRepertoire(chemin) {
			var fichiers = [];

			if( fs.existsSync(chemin) ) {
				fichiers = fs.readdirSync(chemin);

				fichiers.forEach( function(fichier,index) {
					var chemin_courant = chemin + "/" + fichier;
					
					if(fs.lstatSync(chemin_courant).isDirectory()) {
						supprimerRepertoire(chemin_courant);
					} else {
						// supprimer fichier associé au chemin courant
						fs.unlinkSync(chemin_courant);
					}
				});

				fs.rmdirSync(chemin);
			}
		};
	}
};*/
