ALTER TABLE `cape_messages` ADD `id_espace` BIGINT(20) NOT NULL AFTER `uid_oa`, ADD `id_ressource` BIGINT(20) NOT NULL AFTER `id_espace`;
ALTER TABLE `cape_messages` CHANGE `id_espace` `id_espace` BIGINT(20) NULL DEFAULT NULL, CHANGE `id_ressource` `id_ressource` BIGINT(20) NULL DEFAULT NULL, CHANGE `id_capsule` `id_capsule` BIGINT(20) NULL DEFAULT NULL;
