ALTER TABLE  `cape_usages` CHANGE  `nom`  `nom` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;


CREATE TABLE IF NOT EXISTS `cape_formats` (
	`id_format` bigint(20) NOT NULL auto_increment,
	`nom` varchar(250) NOT NULL,
	PRIMARY KEY  (`id_format`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='formats possibles pour une capsule' AUTO_INCREMENT=4 ;

INSERT INTO `cape_formats` (`id_format`, `nom`) VALUES
(0, 'PairForm'),
(1, 'Scenari'),
(2, 'ChaînEdit'),
(3, 'Web');

ALTER TABLE `cape_capsules` CHANGE  `format`  `id_format` BIGINT( 20 ) NOT NULL DEFAULT  1 COMMENT  'Format de la capsule : Scenari, ChainEdit, autre...';
ALTER TABLE `cape_capsules` ADD INDEX (  `id_format` );
ALTER TABLE `cape_capsules` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';

UPDATE  `cape_capsules` SET  `id_format` =1;
ALTER TABLE `cape_capsules` ADD FOREIGN KEY (  `id_format` ) REFERENCES  `cape_formats` (`id_format`);

ALTER TABLE `cape_ressources` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';