CREATE TABLE  `cape_messages_lus` (
	`id_message` BIGINT( 20 ) NOT NULL ,
	`id_utilisateur` BIGINT( 20 ) NOT NULL ,
	`date_lecture` INT( 20 ) NOT NULL ,
	`os` VARCHAR( 80 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
	UNIQUE (
		`id_message` ,
		`id_utilisateur`
	)
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT =  'Synchronisation des messages lus cross-platform';



ALTER TABLE  `cape_messages_lus` ADD FOREIGN KEY (  `id_message` ) REFERENCES  `cape_messages` (
`id_message`
) ON DELETE CASCADE ON UPDATE NO ACTION ;


ALTER TABLE  `cape_messages_lus` ADD FOREIGN KEY (  `id_utilisateur` ) REFERENCES  `cape_utilisateurs_v2` (
`id_utilisateur`
) ON DELETE CASCADE ON UPDATE NO ACTION ;
