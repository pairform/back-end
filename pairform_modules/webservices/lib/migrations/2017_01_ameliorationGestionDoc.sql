ALTER TABLE `cape_roles` ADD `deplacer_capsule` TINYINT(1) NULL DEFAULT '0' AFTER `gerer_visibilite_toutes_capsules`;

UPDATE `cape_roles` SET `deplacer_capsule`=1 WHERE `nom`="Administrateur";

ALTER TABLE `cape_roles` RENAME `cape_roles_espace`;

ALTER TABLE `cape_roles_espace` CHANGE `id_role` `id_role_espace` BIGINT(20) NOT NULL AUTO_INCREMENT;

CREATE TABLE IF NOT EXISTS `cape_roles_ressource` (
  `id_role_ressource` bigint(20) NOT NULL auto_increment,
  `id_ressource` bigint(20) NOT NULL default '0',
  `nom` varchar(250) NOT NULL,
  `editer_ressource` tinyint(1) NOT NULL default '0',
  `gerer_open_badge` tinyint(1) NOT NULL default '0',
  `afficher_statistiques` tinyint(1) NOT NULL default '0',
  `gerer_roles_ressource` tinyint(1) NOT NULL default '0',
  `supprimer_ressource` tinyint(1) NOT NULL default '0',
  `gerer_capsule` tinyint(1) NOT NULL default '0',
  `gerer_visibilite` tinyint(1) NOT NULL default '0',
  `gerer_toutes_capsules` tinyint(1) NOT NULL default '0',
  `gerer_visibilite_toutes_capsules_ressource` tinyint(1) NOT NULL default '0',
  `supprimer_messages_capsule` tinyint(1) NOT NULL default '0',
  `supprimer_messages_toutes_capsules` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_role_ressource`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Représente les roles définis dans le backoffice pour une ressource';

ALTER TABLE  `cape_roles_ressource` ADD INDEX (  `id_ressource` );
ALTER TABLE  `cape_roles_ressource` ADD FOREIGN KEY (  `id_ressource` ) REFERENCES  `cape_ressources` (`id_ressource`);

CREATE TABLE IF NOT EXISTS `cape_membres_roles_ressources` (
  `id_membre` bigint(20) NOT NULL,
  `id_role_ressource` bigint(20) NOT NULL,
  PRIMARY KEY  (`id_membre`, `id_role_ressource`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table croisée entre les rôles par ressource et les membres';

ALTER TABLE  `cape_membres_roles_ressources` ADD FOREIGN KEY (  `id_membre` ) REFERENCES  `cape_membres` (`id_membre`);
ALTER TABLE  `cape_membres_roles_ressources` ADD FOREIGN KEY (  `id_role_ressource` ) REFERENCES  `cape_roles_ressource` (`id_role_ressource`);

ALTER TABLE  `cape_membres` CHANGE  `id_role`  `id_role_espace` BIGINT( 20 ) NULL DEFAULT NULL COMMENT  'role attribué au membre';

ALTER TABLE  `cape_roles_espace` DROP  `gerer_visibilite_capsule`;
ALTER TABLE  `cape_roles_espace` DROP  `attribuer_role`;
ALTER TABLE  `cape_roles_espace` DROP  `role_par_defaut`;
ALTER TABLE  `cape_roles_espace` CHANGE `editer_toutes_ressources_et_capsules`  `gerer_toutes_ressources_et_capsules` TINYINT( 1 ) NOT NULL DEFAULT  '0';
ALTER TABLE  `cape_roles_espace` CHANGE `gerer_roles` `gerer_roles_espace` TINYINT( 1 ) NOT NULL DEFAULT '0';

/*
>>> récupérer les id de toutes les ressources :

SELECT `id_ressource`
FROM `cape_ressources`

>>> Insérer le rôle auteur :

INSERT INTO `cape_roles_ressource` (`id_role_ressource` ,`id_ressource` ,`nom` ,`editer_ressource` ,`gerer_open_badge` ,`afficher_statistiques` ,`gerer_roles_ressource` ,`supprimer_ressource` ,`gerer_capsule` ,`gerer_visibilite` ,`gerer_toutes_capsules` ,`gerer_visibilite_toutes_capsules_ressource` ,`supprimer_messages_capsule` ,`supprimer_messages_toutes_capsules`)
VALUES
(NULL , '1', 'Auteur', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
...
(NULL , '3', 'Auteur', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

>>> Insérer le rôle co-auteur :

INSERT INTO `cape_roles_ressource` (`id_role_ressource` ,`id_ressource` ,`nom` ,`editer_ressource` ,`gerer_open_badge` ,`afficher_statistiques` ,`gerer_roles_ressource` ,`supprimer_ressource` ,`gerer_capsule` ,`gerer_visibilite` ,`gerer_toutes_capsules` ,`gerer_visibilite_toutes_capsules_ressource` ,`supprimer_messages_capsule` ,`supprimer_messages_toutes_capsules`)
VALUES
(NULL , '1', 'Co-auteur', '1', '1', '1', '0', '0', '1', '1', '1', '1', '1', '1'),
...
(NULL , '3', 'Co-auteur', '1', '1', '1', '0', '0', '1', '1', '1', '1', '1', '1');

>>> récupérer les créateurs des resources existantes :

SELECT cm.id_membre, crr.id_role_ressource
FROM cape_ressources AS cr
JOIN cape_roles_ressource AS crr ON cr.id_ressource = crr.id_ressource AND crr.nom = 'Auteur'
JOIN cape_utilisateurs_V2 AS cu ON cr.cree_par = cu.id_utilisateur
JOIN cape_groupes AS cg ON cr.id_espace = cg.id_espace
JOIN cape_membres_groupes AS cmg ON cg.id_groupe = cmg.id_groupe
JOIN cape_membres AS cm ON cu.email = cm.nom AND cmg.id_membre = cm.id_membre
GROUP BY cm.id_membre, crr.id_role_ressource

>>> Insérer les auteurs :

INSERT INTO `cape_membres_roles_ressources` (`id_membre` ,`id_role_ressource`)
VALUES 
('4904', '2'),
...
('4908', '22');
*/


