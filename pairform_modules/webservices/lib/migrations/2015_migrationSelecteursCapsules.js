var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes");


// Ajout des profils d'apprentissage pour chaque ressource stockée en BDD
exports.ajouterSelecteurs = function(callback) {


//Ajout table selecteurs
// CREATE TABLE IF NOT EXISTS `cape_capsules_selecteurs` (
//   `id_selecteur` bigint(20) NOT NULL auto_increment,
//   `id_capsule` bigint(20) NOT NULL,
//   `nom_selecteur` varchar(1000) NOT NULL,
//   PRIMARY KEY  (`id_selecteur`,`id_capsule`)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des sélecteurs CSS par capsules ' AUTO_INCREMENT=1 ;

//Ajout colonne format
//ALTER TABLE `cape_capsules`  ADD `format` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'scenari' COMMENT 'Format de la capsule : Scenari, chainedit, profeci, autre...' AFTER `clef_pairform`

//Ajout selecteur scenari par défaut numéro 1
//INSERT INTO cape_capsules_selecteurs (id_capsule, nom_selecteur) SELECT cc.id_capsule, ".mainContent_co p" FROM cape_capsules AS cc WHERE cc.format = "scenari"
//INSERT INTO cape_capsules_selecteurs (id_capsule, nom_selecteur) SELECT cc.id_capsule, ".mainContent_co .resInFlow" FROM cape_capsules AS cc WHERE cc.format = "scenari"
//INSERT INTO cape_capsules_selecteurs (id_capsule, nom_selecteur) SELECT cc.id_capsule, ".mainContent_ti" FROM cape_capsules AS cc WHERE cc.format = "scenari"

//Mise à jour des sélecteurs des messages pour pointer sur des IDs
//UPDATE cape_messages cm SET cm.nom_tag = (SELECT ccs.id_selecteur FROM cape_capsules_selecteurs ccs WHERE ccs.nom_selecteur = ".mainContent_co p" AND cm.id_capsule = ccs.id_capsule) WHERE cm.nom_tag = "P"
//UPDATE cape_messages cm SET cm.nom_tag = (SELECT ccs.id_selecteur FROM cape_capsules_selecteurs ccs WHERE ccs.nom_selecteur = ".mainContent_co .resInFlow" AND cm.id_capsule = ccs.id_capsule) WHERE cm.nom_tag = "DIV"
//UPDATE cape_messages cm SET cm.nom_tag = (SELECT ccs.id_selecteur FROM cape_capsules_selecteurs ccs WHERE ccs.nom_selecteur = ".mainContent_ti" AND cm.id_capsule = ccs.id_capsule) WHERE cm.nom_tag = "PAGE"

//Changement du type de colonne
//ALTER TABLE `cape_messages` CHANGE `nom_tag` `id_selecteur` BIGINT(20) NOT NULL

//MAJ du chemin des pages
//UPDATE cape_messages cm SET cm.nom_page = CONCAT("co/", cm.nom_page, ".html") WHERE cm.nom_page != ""

//Url web pour site vitrine
//UPDATE  `PairForm_v1_2_100`.`cape_capsules` SET  `url_web` =  'https://www.pairform.fr/' WHERE  `cape_capsules`.`id_capsule` =112;
};