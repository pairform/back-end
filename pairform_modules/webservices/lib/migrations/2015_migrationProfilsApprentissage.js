/*var pairform_dao = require("pairform_dao"),
	  constantes = require("../constantes");


// Ajout des profils d'apprentissage pour chaque ressource stockée en BDD
exports.putProfilsApprentissage = function(callback) {
	var retour_json = {status: constantes.STATUS_JSON_OK};
	var liste_ressources, index_ressource = 0;

	// callback par défaut en cas d'erreur lors de l'execution d'une requete SQL
	function callbackError() {
		callback(constantes.RETOUR_JSON_KO);
	}


	var liste_ressources = [
		{id_ressource:234, id_usage:1},
		{id_ressource:236, id_usage:1}
	];
	selectRessource(liste_ressources[0]);

*/
	/*function selectAllRessources() {
		pairform_dao.ressource_dao.selectAllRessources(
			function (ressources) {
				liste_ressources = ressources;
				selectRessource(liste_ressources[0]);
			},
			callbackError
		);
	}*/
	
/*
	function selectRessource(ressource) {
		// si la ressource est dédié à un usage de formation, on associe à la ressource la liste de questions/réponses du profil d'apprentissage
		if (ressource.id_usage == constantes.USAGE_FORMATION) {
			insertQuestion(ressource.id_ressource, 0);
		} else {
			index_ressource++;
			if (index_ressource < liste_ressources.length) {
				selectRessource(liste_ressources[index_ressource]);
			} else {
				callback(retour_json);
				pairform_dao.libererConnexionBDD(_connexion_courante);
			}
		}
	}

	// créé une question dans le profil d'apprentissage d'une ressource
	function insertQuestion(id_ressource, index_question) {
		pairform_dao.profil_apprentissage_dao.insertQuestion(
			id_ressource,
			function (reponse_sql) {
				insertTraductionQuestion(id_ressource, index_question, reponse_sql.id_question);
			},
			callbackError
		);
	}

	// créé la traduction d'une question du profil d'apprentissage d'une ressource
	function insertTraductionQuestion(id_ressource, index_question, id_question) {
		pairform_dao.profil_apprentissage_dao.insertTraductionQuestion(
			id_question,
			constantes.PROFIL_APPRENTISSAGE[index_question].id_langue,
			constantes.PROFIL_APPRENTISSAGE[index_question].question,
			function () {
				insertReponse(id_ressource, index_question, id_question, 0);
			},
			callbackError
		);
	}

	// créé une réponse à une reponse du profil d'apprentissage d'une ressource
	function insertReponse(id_ressource, index_question, id_question, index_reponse) {
		pairform_dao.profil_apprentissage_dao.insertReponse(
			id_question,
			function (reponse_sql) {
				insertTraductionReponse(id_ressource, index_question, id_question, index_reponse, reponse_sql.id_reponse);
			},
			callbackError
		);
	}

	// créé la traduction d'une reponse du profil d'apprentissage d'une ressource
	function insertTraductionReponse(id_ressource, index_question, id_question, index_reponse, id_reponse) {
		pairform_dao.profil_apprentissage_dao.insertTraductionReponse(
			id_reponse,
			constantes.PROFIL_APPRENTISSAGE[index_question].id_langue,
			constantes.PROFIL_APPRENTISSAGE[index_question].reponses[index_reponse],
			function () {
				index_reponse++;
				// s'il y a encore des réponses à ajouter
				if(index_reponse < constantes.PROFIL_APPRENTISSAGE[index_question].reponses.length) {
					insertReponse(id_ressource, index_question, id_question, index_reponse);
				} else {
					index_question++;
					// s'il y a encore des questions à ajouter
					if (index_question < constantes.PROFIL_APPRENTISSAGE.length) {
						insertQuestion(id_ressource, index_question);
					} else {
						index_ressource++;
						if (index_ressource < liste_ressources.length) {
							selectRessource(liste_ressources[index_ressource]);
						} else {
							callback(retour_json);
						}
					}
				}
			},
			callbackError
		);
	}
};*/