CREATE TABLE `cape_sessions` ( 
 `id_session` BIGINT(20) NOT NULL AUTO_INCREMENT ,
 `id_ressource` BIGINT(20) NOT NULL ,
 `id_groupe` BIGINT(20) NULL,
 `nom` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `id_utilisateur` BIGINT(20) NOT NULL ,
 `date_creation` BIGINT(20) NOT NULL ,
 PRIMARY KEY (`id_session`)) 
 ENGINE = InnoDB CHARACTER 
 SET utf8 COLLATE utf8_general_ci COMMENT = 'Sessions de formations / timeline';

 CREATE TABLE `cape_sessions_items` ( 
 `id_item` BIGINT(20) NOT NULL AUTO_INCREMENT ,
 `id_session` BIGINT(20) NOT NULL ,
 `id_utilisateur` BIGINT(20) NOT NULL ,
 `id_capsule` BIGINT(20) NULL ,
 `date_creation` BIGINT(20) NOT NULL ,
 `date_diffusion` BIGINT(20) NOT NULL ,
 `description`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
 `icone`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `sub_type`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `titre`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `type`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `type_icone`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `url`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `diffusion_automatique` INT(1) NOT NULL DEFAULT '0', 
 PRIMARY KEY (`id_item`)) 
 ENGINE = InnoDB CHARACTER 
 SET utf8 COLLATE utf8_general_ci COMMENT = 'Items des sessions de formations / timeline';
