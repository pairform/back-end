"use strict";

var web_services = require("../lib/webServices"),
		 express = require("express"),
			 log = require("metalogger")(),
		  multer = require("multer"),
			  fs = require("fs"),
	  constantes = require("../lib/constantes");


module.exports = function(app, passport) {

	// si l'utilisateur n'est pas connecté, on ne traite pas la requête
	function utilisateurEstConnecte(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
		}
	}

	// si l'utilisateur n'est pas autorisé à réaliser l'action, on ne traite pas la requête
	function utilisateurEstAutorise(res, autorisation, traiterRequete) {
		if (autorisation) {
			traiterRequete();
		} else {
			log.warning("Backoffice - autorisation action : pas ok");
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_AUTORISE);
		}
	}


	//--------------------------------- Backoffice ---------------------------------

	/*
	 * Tableau de bord
	 */

	// Récupération des données du tableau de bord (liste des espaces, liste des ressources, liste des capsules)
	app.get("/webServices/infosTableauDeBord", utilisateurEstConnecte, function(req, res) {
		// si l'utilisateur connecté est l'admin de PairForm, on renvoit tout les espaces, toutes les ressources et toutes les capsules
		if(req.user.est_admin_pairform) {
			web_services.tableau_de_bord.getTableauDeBordAdminPairForm( function(retour_json) {
				res.json(retour_json);
			});
		}
		else if(req.user.liste_roles && Object.keys(req.user.liste_roles).length > 0) {
			// sinon, si l'utilisateur à des roles dans le backoffice
			// on renvoit les espaces dans lesquels l'utilisateur à un rôle, ainsi que les ressources et capsules que ces espaces contiennent
			web_services.tableau_de_bord.getTableauDeBord(
				req.user.email,
				function(retour_json) {
					res.json(retour_json);
				}
			);
		}
		else {
			// sinon, l'utilisateur n'a aucun role, il n'est pas autorisé à accéder au backoffice
			log.warning("Backoffice - autorisation action : pas ok");
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_AUTORISE);
		}
	});


	/*
	 * Creation ressource
	 */

	// récupèration des données liées à la page de création d'une ressource (liste des thèmes, listes des langues ; traduit dans la langue choisi)
	app.get("/webServices/infosCreationRessource/langue/:code_langue/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.creation_ressource.getCreationRessource(
				req.params.code_langue,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Attribution roles
	 */

	// récupèration des données lièes à la page d'attribution des rôles aux utilisateurs d'un espace
	app.get("/webServices/espace/:id_espace/attributionRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_ROLES_ESPACE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.attribution_roles.getAttributionRolesEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// récupèration des données lièes à la page d'attribution des rôles aux utilisateurs d'une ressource
	app.get("/webServices/espace/:id_espace/ressource/:id_ressource/attributionRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_ROLES_RESSOURCE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.attribution_roles.getAttributionRolesRessource(
				req.params.id_espace,
				req.params.id_ressource,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Statistiques
	 */

	// récupèration des statistiques d'une ressource
	app.get("/webServices/statistiques/espace/:id_espace/ressource/:id_ressource/:ressource_cree_par", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.AFFICHER_STATISTIQUES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.statistiques.getStatistiquesRessource(
				req.params.id_ressource,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	//--------------------------------- Entités ---------------------------------

	/*
	 * Espaces
	 */

	// récupèration d'un espace
	app.get("/webServices/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.EDITER_ESPACE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.espace_backoffice.getEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// création d'un nouvelle espace
	//!\ un changement de cette url doit être répercuter dans le fichier server_modules/server/app.js
	app.put("/webServices/espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform;

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.espace_backoffice.putEspace(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// mise à jour d'un espace
	//!\ un changement de cette url doit être répercuter dans le fichier server_modules/server/app.js
	app.post("/webServices/espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.EDITER_ESPACE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.espace_backoffice.postEspace(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// suppression d'un espace
	app.delete("/webServices/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform;

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.espace_backoffice.deleteEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Ressources
	 */

	// récupèration d'une ressource
	app.get("/webServices/ressource/:id_ressource/:cree_par/langue/:code_langue/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.EDITER_RESSOURCE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.ressource_backoffice.getRessource(
				req.params.id_ressource,
				req.params.code_langue,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// création d'une nouvelle ressource
	//!\ un changement de cette url doit être répercuter dans le fichier server_modules/server/app.js
	app.put("/webServices/ressource", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.ressource_backoffice.putRessource(
				req.body,
				req.user,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// mise à jour d'une ressource
	//!\ un changement de cette url doit être répercuter dans le fichier server_modules/server/app.js
	app.post("/webServices/ressource", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.EDITER_RESSOURCE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.ressource_backoffice.postRessource(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// suppression d'une ressource
	app.delete("/webServices/ressource/:id_ressource/:cree_par/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.SUPPRIMER_RESSOURCE];

		req.params.liste_ressources = [{id_ressource: req.params.id_ressource}];		// le paramètre envoyé doit être un tableau contenant les id des ressource à supprimer

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.ressource_backoffice.deleteRessources(
				req.params,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Capsules
	 */


	// récupèration d'une capsule
	app.get("/webServices/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_TOUTES_CAPSULES] ||
			( req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_CAPSULE] && req.user.id_utilisateur == req.params.cree_par );

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.getCapsule(
				req.params.id_capsule,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// récupèration des informations sur la visibilité d'une capsule
	app.get("/webServices/visibilite/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_VISIBILITE_TOUTES_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_VISIBILITE_TOUTES_CAPSULES_RESSOURCE] ||
			( req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_VISIBILITE] && req.user.id_utilisateur == req.params.cree_par );

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.getCapsuleVisibilite(
				req.params.id_capsule,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// récupèration du contenu (ckeditor) de la capsule
	app.get("/webServices/contenu_ckeditor/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace/page/:id_page_contenue_capsule", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_TOUTES_CAPSULES] ||
			( req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_CAPSULE] && req.user.id_utilisateur == req.params.cree_par );

		utilisateurEstAutorise(res, autorisation, function() {
			res.sendFile(constantes.REPERTOIRE_CAPSULE + req.params.id_capsule +"/contenu_ckeditor_"+ req.params.id_page_contenue_capsule +".html");
		});
	});

	// création d'une nouvelle capsule
	app.put("/webServices/capsule", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform 
				|| ((req.user.liste_roles && req.user.liste_roles[req.body.id_espace]) 
						? (req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES]
							|| req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_CAPSULE])
						: false
					);

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.putCapsule(
				req.body,
				req.user.id_utilisateur,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// mise à jour d'une capsule
	app.post("/webServices/capsule", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_TOUTES_CAPSULES] ||
			( req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_CAPSULE] && req.user.id_utilisateur == req.body.cree_par );

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.postCapsule(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// déplacement d'une capsule dans une autre ressource du même espace
	app.post("/webServices/capsule/deplacer", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.DEPLACER_CAPSULE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.deplacerCapsule(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// upload des archives/fichiers d'une capsule
	//!\ un changement de cette url doit être répercuter dans le fichier server_modules/server/app.js
	app.post("/webServices/upload/capsule", utilisateurEstConnecte, multer({
			dest: constantes.REPERTOIRE_CAPSULE,
			limits: {
				fileSize: 1024*1024*1024,			// 1024 Mo = 1 Go
			},
			onFileUploadComplete: function(file, req, res) {
				var autorisation = req.user.est_admin_pairform ||
					req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_TOUTES_CAPSULES] ||
					( req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_CAPSULE] && req.user.id_utilisateur == req.body.cree_par );

				// si l'utilisateur n'est pas autorisé à réaliser l'action, on supprime le fichier uploader
				if (!autorisation) {
					fs.unlink('./' + file.path);
				}
			},
			onFileSizeLimit: function (file) {
				//Si le fichier est plus gros que la limite imposé, on supprime le fichier reçu.
				fs.unlink('./' + file.path);
			},
			onError: function (error, next) {
				log.error(error);
				next(error);
			}
		}),
		function(req, res, next) {
			var autorisation = req.user.est_admin_pairform ||
				req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
				req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_TOUTES_CAPSULES] ||
				( req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_CAPSULE] && req.user.id_utilisateur == req.body.cree_par );

			// gestion erreur chelou : quand la capsule a une vingtene de page, le tableau des pages se transforme en objet
			if (req.body.sommaire) {
				if ( !(req.body.sommaire.liste_pages instanceof Array) ) {
					log.error("Erreur : transformation du tableau en objet !!! C'est de la magie quantique !?");

					// on force à repasser en tableau
					var liste_pages_temp = [];
					for (var page in req.body.sommaire.liste_pages)
						liste_pages_temp.push(req.body.sommaire.liste_pages[page]);

					req.body.sommaire.liste_pages = liste_pages_temp;
				}
			}
			
			utilisateurEstAutorise(res, autorisation, function() {
				web_services.capsule_backoffice.postUploadCapsule(
					req.body,
					req.files,		//variable générée contenant la listes des archive/fichiers récupérés
					function(retour_json) {
						res.json(retour_json);
					}
				)
			});
		}
	);

	// upload d'un fichier (.docx, .odt...) à convertir en HTML, ce contenu HTML est ensuite renvoyé au client pour être intégré dans ckeditor
	//!\ un changement de cette url doit être répercuter dans le fichier server_modules/server/app.js
	app.post("/webServices/upload/conversion", utilisateurEstConnecte, multer({
			dest: constantes.REPERTOIRE_CAPSULE,
			limits: {
				files: 1,							// nb fichiers
				fileSize: 1024*1024*1024,			// 1024 Mo = 1 Go
			},
			onFileUploadComplete: function(file, req, res) {
				var autorisation = req.user.est_admin_pairform ||
					req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_TOUTES_CAPSULES] ||
					( req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_CAPSULE] && req.user.id_utilisateur == req.body.cree_par );

				// si l'utilisateur n'est pas autorisé à réaliser l'action, on supprime le fichier uploader
				if (!autorisation) {
					fs.unlink('./' + file.path);
				}
			},
			onFileSizeLimit: function (file) {
				//Si le fichier est plus gros que la limite imposé, on supprime le fichier reçu.
				fs.unlink('./' + file.path);
			},
			onError: function (error, next) {
				log.error(error);
				next(error);
			}
		}),
		function(req, res, next) {
			var autorisation = req.user.est_admin_pairform ||
				req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
				req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_TOUTES_CAPSULES] ||
				( req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_CAPSULE] && req.user.id_utilisateur == req.body.cree_par );

			utilisateurEstAutorise(res, autorisation, function() {
				web_services.capsule_backoffice.convertirFichierToHTML(
					req.body,
					req.files,		//variable générée contenant la listes des fichiers récupérés
					function(data) {
						res.write(new Buffer(data));
					},
					function (code) {
						res.end();
					},
					function callback_error (error) {
						res.status(403).json(error);
					}
				)
			});
		}
	);

	// mise à jour de la visibilité d'une capsule
	app.post("/webServices/visibilite/capsule", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.body.id_espace][constantes.GERER_VISIBILITE_TOUTES_CAPSULES] ||
			req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_VISIBILITE_TOUTES_CAPSULES_RESSOURCE] ||
			( req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_VISIBILITE] && req.user.id_utilisateur == req.body.cree_par );

		utilisateurEstAutorise(res, autorisation, function() {
			if (req.body.modifier_toutes_capsules) {
				web_services.capsule_backoffice.postCapsuleVisibilitePourToutesCapsulesDeMemeRessource(
					req.body,
					function(retour_json) {
						res.json(retour_json);
					}
				)
			}
			else {
				web_services.capsule_backoffice.postCapsuleVisibilite(
					req.body,
					function(retour_json) {
						res.json(retour_json);
					}
				)
			}

		});
	});

	// suppression d'une capsule
	app.delete("/webServices/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_TOUTES_CAPSULES] ||
			( req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_CAPSULE] && req.user.id_utilisateur == req.params.cree_par );

		req.params.liste_capsules = [{			// le paramètre envoyé doit être un tableau contenant les id des capsules à supprimer et l'id de la ressource associé
			id_capsule: req.params.id_capsule
		}];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.capsule_backoffice.deleteCapsules(
				req.params,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	// Regnération d'une capsule au format PF
	app.post("/webServices/capsule/regenerer", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform;
		utilisateurEstAutorise(res, autorisation, function() {
			web_services.regeneration_docs_PF.regenerationDocsPF(
				parseInt(req.param("id_capsule")),
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	/*
	 * Formats de capsule
	 */

	// récupèration de la liste des formats
	app.get("/webServices/listeFormats", utilisateurEstConnecte, function(req, res) {
		var autorisation = true;			// n'importe qui peut récupérer la liste des formats, ce n'est pas une info privée

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.format_backoffice.getAllFormats(
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Groupes
	 */

	// récupèration de la liste des groupes d'un espace
	app.get("/webServices/espace/:id_espace/listeGroupes", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.getGroupesByIdEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// création d'un nouveau groupe
	app.put("/webServices/groupe", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.putGroupe(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour d'un groupe
	app.post("/webServices/groupe", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.postGroupe(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour de la liste des membres d'un groupe
	app.post("/webServices/groupe/listeMembres", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.postMembres(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// suppression d'un groupe
	app.delete("/webServices/espace/:id_espace/groupe/:id_groupe", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_GROUPES];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.groupe_backoffice.deleteGroupe(
				req.params.id_groupe,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Roles
	 */

	// récupèration de la liste des roles d'un espace
	app.get("/webServices/espace/:id_espace/listeRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.params.id_espace][constantes.GERER_ROLES_ESPACE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.getRolesByIdEspace(
				req.params.id_espace,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// récupèration de la liste des roles d'une ressource
	app.get("/webServices/espace/:id_espace/ressource/:id_ressource/listeRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_ROLES_RESSOURCE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.getRolesByIdRessource(
				req.params.id_ressource,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour des roles d'un espace
	app.post("/webServices/espace/listeRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_ROLES_ESPACE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.postRolesEspace(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour des roles d'une ressource
	app.post("/webServices/ressource/listeRoles", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_ROLES_RESSOURCE];
		
		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.postRolesRessource(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour de la liste des membres d'un role d'un espace
	app.post("/webServices/roleEspace/listeMembres", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform || req.user.liste_roles[req.body.id_espace][constantes.GERER_ROLES_ESPACE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.postMembresRoleEspace(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// Mise à jour de la liste des membres d'un role d'une ressource
	app.post("/webServices/roleRessource/listeMembres", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_ROLES_RESSOURCE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.role_backoffice.postMembresRoleRessource(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Messages
	 */

	// suppression des messages d'une capsule
	app.delete("/webServices/messages/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.SUPPRIMER_MESSAGES_TOUTES_CAPSULES] ||
			( req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.SUPPRIMER_MESSAGES] && req.user.id_utilisateur == req.params.cree_par );

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.message_backoffice.deleteCapsuleMessages(
				req.params.id_capsule,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});


	/*
	 * Open-badges
	 */

	 // récupération de la liste des openbadge d'une ressource
	app.get("/webServices/openbadges/espace/:id_espace/ressource/:id_ressource/:ressource_cree_par", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.params.id_espace][req.params.id_ressource][constantes.GERER_OPEN_BADGE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.openbadge_backoffice.getOpenBadgesByIdRessource(
				req.params.id_ressource,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// création d'un nouvel openbadge
	app.put("/webServices/openbadge", utilisateurEstConnecte, function(req, res) {		
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_OPEN_BADGE];


		utilisateurEstAutorise(res, autorisation, function() {
			web_services.openbadge_backoffice.putOpenBadge(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	// mise à jour d'un openbadge
	app.post("/webServices/openbadge", utilisateurEstConnecte, function(req, res) {
		var autorisation = req.user.est_admin_pairform ||
			req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
			req.user.liste_roles[req.body.id_espace][req.body.id_ressource][constantes.GERER_OPEN_BADGE];

		utilisateurEstAutorise(res, autorisation, function() {
			web_services.openbadge_backoffice.postOpenBadge(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			)
		});
	});

	


	/* Sessions */




	app.get("/webServices/session/ressource/:id_ressource", utilisateurEstConnecte, function (req, res) {
		req.params.id_utilisateur = req.user.id_utilisateur;
		web_services.session.getSessions(
			req.params,
			function (retour_json) {
				res.json(retour_json)
			}
		)
	});

	app.get("/webServices/session/ressource/:id_espace/:id_ressource", utilisateurEstConnecte, function (req, res) {
		var autorisation = true || req.user.est_admin_pairform ||
				req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES];

			utilisateurEstAutorise(res, autorisation, function() {
				web_services.session.getSessions(
					req.params,
					function (retour_json) {
						res.json(retour_json)
					}
				)
			});
	});

	app.put("/webServices/session", utilisateurEstConnecte, function (req, res) {
		var autorisation = true || req.user.est_admin_pairform ||
				req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES];

			utilisateurEstAutorise(res, autorisation, function() {
				req.body.id_utilisateur = req.user.id_utilisateur;
				web_services.session.putSession(
					req.body,
					function (retour_json) {
						res.json(retour_json)
					}
				)
			});
	});

	app.post("/webServices/session", utilisateurEstConnecte, function (req, res) {
		var autorisation = true || req.user.est_admin_pairform ||
				req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES];

			utilisateurEstAutorise(res, autorisation, function() {
				web_services.session.postSession(
					req.body,
					function (retour_json) {
						res.json(retour_json)
					}
				)
			});
	});

	app.delete("/webServices/session/:id_espace/:id_ressource/:id_session", utilisateurEstConnecte, function (req, res) {
		var autorisation = true || req.user.est_admin_pairform ||
				req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES];

			utilisateurEstAutorise(res, autorisation, function() {
				web_services.session.deleteSession(
					req.params,
					function (retour_json) {
						res.json(retour_json)
					}
				)
			});
	});


	/* Items */


	app.put("/webServices/session/item", utilisateurEstConnecte, function (req, res) {
		var autorisation = true || req.user.est_admin_pairform ||
				req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES];

			utilisateurEstAutorise(res, autorisation, function() {
				req.body.id_utilisateur = req.user.id_utilisateur;
				web_services.session_item.putItem(
					req.body,
					function (retour_json) {
						res.json(retour_json)
					}
				)
			});
	});

	app.post("/webServices/session/item", utilisateurEstConnecte, function (req, res) {
		var autorisation = true || req.user.est_admin_pairform ||
				req.user.liste_roles[req.body.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES];

			utilisateurEstAutorise(res, autorisation, function() {
				req.body.id_utilisateur = req.user.id_utilisateur;
				web_services.session_item.postItem(
					req.body,
					function (retour_json) {
						res.json(retour_json)
					}
				)
			});
	});

	app.delete("/webServices/session/item/:id_espace/:id_ressource/:id_item", utilisateurEstConnecte, function (req, res) {
		var autorisation = true || req.user.est_admin_pairform ||
				req.user.liste_roles[req.params.id_espace][constantes.GERER_TOUTES_RESSOURCES_ET_CAPSULES];

			utilisateurEstAutorise(res, autorisation, function() {
				req.params.id_utilisateur = req.user.id_utilisateur;
				web_services.session_item.deleteItem(
					req.params,
					function (retour_json) {
						res.json(retour_json)
					}
				)
			});
	});

}