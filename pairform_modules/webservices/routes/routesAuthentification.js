"use strict";

var web_services = require("../lib/webServices"),
		 express = require("express"),
		   	 log = require('metalogger')(),
				 jwt = require('jsonwebtoken'),
	  constantes = require("../lib/constantes"),
		  CONFIG = require("config");


module.exports = function(app, passport) {

	/*
	 * Authentification via LTI
	 */

	// Point d'entrée d'outils LTI
	app.post("/lti", function(req, res, next) {
		passport.authenticate("login_LTI_server", function(err, user, info) {
			log.debug("Err", err)
			log.debug("user", user)
			log.debug("info", info)
			// Si l'utilisateur n'a pas encore de compte
			// Ou qu'il n'y a pas d'URL passée
			if (!user || (!req.query.url && !req.body.custom_url)) {
				var lti_params = {
					"user_image" : req.body.user_image && !req.body.user_image.match(/avatar-50/) ? req.body.user_image : "", //Anti avatar par défaut de canvas
				 	"nom_prenom" : req.body.lis_person_name_full || req.body.lis_person_sourcedid,
					"email" : req.body.lis_person_contact_email_primary,
					"langue" : req.body.launch_presentation_locale || "LTI",
					"url_to_follow" : req.query.url || req.body.custom_url,
					"consumer_name" : req.body.tool_consumer_info_product_family_code || "LTI",
					//URL de retour pour les LMS qui proposent de la selection de ressource
					"launch_presentation_return_url" : req.body.launch_presentation_return_url,
					// Id de source d'authentification, passée dans l'erreur à l'arrache
					// en cas de création de compte de l'utilisateur
					"id_source_auth" : req.body.id_source_auth || (err ? err.id_source_auth : 0),
					"user_role" : req.body.roles || "defaut"
				}
				// if(!err)
				var retour = {"status" : "ok", "messages" : []};

				// HACK : Cas d'utilisateur guest sous Moodle
				// Pour simuler un guest, moodle a un compte par défaut, inchangeable à priori,
				// avec un e-mail root@localhost et un paramètre supplémentaire non standard indiquant
				// que c'est un invité. Exemple :
				//   - ext_user_username : guest
				//   - lis_person_contact_email_primary : root@localhost
				// On teste donc le mail à la recherche de localhost seulement (suffisant), et
				// si l'admin n'a pas autorisé le passage d'email pour ce module LTI,
				// il faut quand même savoir que c'est un guest, donc on teste le parametre supplémentaire
				// s'il est présent
				if (lti_params.email && lti_params.email.match(/@localhost/i) || (req.body.ext_user_username && req.body.ext_user_username === "guest")) {
					// Redirection de l'utilisateur sans token
					return res.redirect(lti_params.url_to_follow);
				};
				// S'il n'y a pas d'URL, c'est une erreur
				if (!lti_params.url_to_follow){
					retour.status = "no_url";
					retour.messages.push('no_url');
				}
				// S'il n'y a pas d'email, c'est une erreur
				if (!lti_params.email){
					retour.status = "ko";
					retour.messages.push('no_email');
				}
				// Traitement des erreurs de stratégie LTI
				// Qui ne renvoient que des infos
				if (info) {
					retour.status = "ko";
					if (info == 'Request isn\'t LTI launch'){
						retour.messages.push('no_lti');
					}
					else if (info.match(/^(Tool consumer)/gi)){
						// log.debug(JSON.stringify(req.body, null, '\t'));
						retour.messages.push('bad_credentials');
					}
					else
						retour.messages.push(info);

				}

				// On lui envoie la page de bridge pour qu'il se créé un compte
				return res.render(__dirname + "/../../authentification/node_modules/lti/views/lti", {
					"url_serveur_node" : CONFIG.app.urls.serveur_node,
					"status" : retour.status,
					"messages" : retour.messages,
					"lti_params" : lti_params
				});
			}
			// Si c'est la première fois que l'utilisateur se connecte sur ce LMS
			// On va lui demander son mot de passe pour confirmer sa connexion
			else if (user.should_confirm){

				// On lui envoie la page de bridge pour qu'il se créé un compte
				return res.render(__dirname + "/../../authentification/node_modules/lti/views/lti", {
					"url_serveur_node" : CONFIG.app.urls.serveur_node,
					"status" : "up",
					"utilisateur" : user
				});
			}
			// Sinon, tout roule, on redirige
			else {
				// Construction de l'URL de redirection avec le token d'authentification
				var url_to_follow = (req.query.url || req.body.custom_url) + "?token=" + user.token_oauth;

				// Redirection de l'utilisateur
				return res.redirect(url_to_follow);
			}
		})(req, res, next);
	});


	// Login d'un utilisateur avec LTI
	app.post("/webServices/utilisateur/login/lti", function(req, res, next) {
		//Patch pour bearerStrategy
		req.body.access_token = req.body.access_token || req.body.token;

		passport.authenticate("login_LTI_client", function(err, user, info) {
			if (err){
				log.error("/webServices/utilisateur/login/lti");
				log.error(err);
				//Erreur maison, on forward
				if (err.message)
					return res.json(err);
				//Sinon, on renvoie une erreur générique
				else
					return res.json({
						"status" : "ko",
						"message" : "ws_erreur_contacter_pairform"
					});
			}
			else if(!user) {
				log.error("/webServices/utilisateur/login/lti");
				log.error(info);
				return res.json({
					"status" : "ko",
					"message" : "ws_erreur_contacter_pairform"
				});
			}
			else{
				passport.deserializeUser(user.id_utilisateur, function (err, user) {
					req.login(user, function(err) {
						if (err) {
							return next(err);
						}
						req.user.status = "ok";
						return res.json(req.user);
					});
				});
			}
		})(req, res, next);
	});


	// Login d'un utilisateur avec LTI
	app.post("/webServices/utilisateur/signup/lti", function(req, res, next) {
		passport.authenticate("signup_LTI", function(err, user, info) {
			if (err){
				log.error("/webServices/utilisateur/signup/lti");
				log.error(err);
				//Si c'est une erreur custom
				if (err.status){
					return res.json(err);
				}

				else
					return res.json({
						"status" : "ko",
						"message" : "ws_erreur_contacter_pairform"
					});
			}
			else {
				passport.genererTokenOAuth(req.body.email, function (err, user, info) {
					// Si l'utilisateur n'a pas encore de compte
					if (err) {
						log.error(err);
						//Problème dans la génération du token
						return res.json({
							"status" : "ko"
						});
					}
					else {
						var url_final = req.body.url_to_follow + "?token=" + user.token_oauth;
						return res.json({
							"status" : "ok",
							"url_final" : url_final
						});
					}
				});
			}
		})(req, res, next);
	});


	// Confirmation du mot de passe d'un utilisateur pour lti LTI
	app.post("/webServices/utilisateur/confirm/lti", function(req, res, next) {
		passport.authenticate("login", function(err, user, info) {
			if (err){
				log.error("/webServices/utilisateur/confirm/lti");
				log.error(err);
				return res.json({
					"status" : "ko",
					"message" : "ws_erreur_contacter_pairform"
				});
			}
			else if(!user) {
				return res.json({
					"status" : "ko",
					"message" : info.message
				});
			}
			else{
				// Confirmation du mot de passe : on met un flag a 1 pour valider en BDD
				passport.confirmSourceAuth(user.id_utilisateur, req.body.id_source_auth, 1, function (erreur) {

					if (erreur) {
						return res.json({
							"status" : "ko",
							"message" : erreur.message
						});
					}
					else{
						return res.json({
							"status" : "ok"
						});
					}
				})
			}
		})(req, res, next);
	});


	/*
	 *	Authentification locale
	 */

	// connexion sur les applications (web, ios, and)
	app.post("/webServices/utilisateur/login", function(req, res, next) {
		passport.authenticate("login", function(err, utilisateur, info) {
			if (err){
				return res.json({
					"status" : "ko",
					"message" : "ws_erreur_contacter_pairform"
				});
			}
			else if(!utilisateur) {
				return res.json({
					"status" : "ko",
					"message" : info.message
				});
			}
			else{
				passport.deserializeUser(utilisateur.id_utilisateur, function (err, utilisateur) {
					req.login(utilisateur, function(err) {
						if (err) {
							return next(err);
						}
						req.user.status = "ok";
						return res.json(req.user);
					});
				});
			}
		})(req, res, next);
	});


	// connexion sur le Backoffice
	app.post("/webServices/utilisateur/login/backoffice", function(req, res, next) {
		passport.authenticate("login", function(err, utilisateur, info) {
			if (err){
				return res.json({
					"status" : "ko",
					"message" : "ws_erreur_contacter_pairform"
				});
			}
			else if(!utilisateur) {
				return res.json({
					"status" : "ko",
					"message" : info.message
				});
			}
			else{
				// si l'utilisateur à au moins un rôle dans le Backoffice OU est un admin PairForm
				passport.deserializeUser(utilisateur.id_utilisateur, function (err, utilisateur) {
					if(utilisateur.liste_roles || utilisateur.est_admin_pairform) {
						req.login(utilisateur, function(err) {
							if (err) {
								return next(err);
							}
							return res.json({
								"status" : "ok",
								"utilisateur_connecte" : req.user
							});
						});
					} else {
						// sinon, l'utilisateur n'est pas autorisé à accèder au Backoffice
						res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_AUTORISE);
					}
				});
			}
		})(req, res, next);
	});


	// déconnexion d'un utilisateur de l'application web
	app.post("/webServices/utilisateur/logout", function(req, res) {
		req.logout();
		req.session.destroy();
   		res.clearCookie('session.pf');

		res.json({
			"status" : "ok"
		});
	});


	// Attention : PUT sur web, POST sur anciennes versions mobiles
	app.all("/webServices/utilisateur/enregistrer", function(req, res, next) {
		passport.authenticate("signup", function(err, id_utilisateur, info) {
		if (err){
			log.error("/webServices/utilisateur/enregistrer");
			log.error(err);
			return res.json({
				"status" : "ko",
				"message" : "ws_erreur_contacter_pairform"
			});
		}
		else if(!id_utilisateur) {
			 if(info) {
				return res.json({
					"status" : "ko",
					"message" : info.message
				});
			}
			else{
				return res.json({
					"status" : "ko",
					"message" : "ws_erreur_contacter_pairform"
				});
			}
		}
		else{
			log.debug("Création de compte : ",id_utilisateur);
			passport.deserializeUser(id_utilisateur, function (err, utilisateur) {
				req.login(utilisateur, function(err) {
					if (err) {
						return res.json({
							"status" : "ko",
							"message" : "ws_erreur_contacter_pairform"
						});
					}
					else{
						return res.json({
							"status" : "ok",
							"id_utilisateur" : id_utilisateur,
							"utilisateur" : utilisateur
						});
					}
				});
			});
		}

		})(req, res, next);
	});


	// Réinitialisation du mot de passe d'un utilisateur (via son email)
	app.post("/webServices/utilisateur/reset", function(req, res) {
		web_services.authentification.resetPassword(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

  // Réinitialisation du mot de passe d'un utilisateur (via son email)
  app.post("/webServices/utilisateur/confirm", utilisateurEstConnecte, function(req, res) {
    web_services.authentification.setTokenValidation(
      req.user,
      function(err, token){
        if (err){ 
          return res.json({
            "status" : "ko",
            "message" : "ws_erreur_contacter_pairform"
          });
        }
        passport.envoyerEmailConfirmation(
          req.user,
          token,
          function(retour_json) {
            return res.json({
              "status" : "ok"
            });
          },
          function(retour_json) {
            return res.json({
              "status" : "ko",
              "message" : "ws_erreur_contacter_pairform"
            });
          }
        );
      });
  });

  /* LinkedIn */

	app.get('/webServices/utilisateur/linkedin', passport.authenticate("linkedin"));

	app.get('/webServices/utilisateur/linkedin/callback', function(req, res, next) {
		//Si l'utilisateur annule pour une raison ou une autre
		if (req.param("error") && req.param("error_description")) {
				return res.render(__dirname + "/../../authentification/views/callback_sso",{
					error: req.param("error_description"),
					url_serveur_node: CONFIG.app.urls.serveur_node
				});
		}
		//Sinon, on 
	  passport.authenticate("linkedin", function(err, nouveau_utilisateur, utilisateur) {
	  	log.error(err, nouveau_utilisateur, utilisateur);
			if (err){
				return res.render(__dirname + "/../../authentification/views/callback_sso",{
					error: "ws_erreur_contacter_pairform",
					url_serveur_node: CONFIG.app.urls.serveur_node
				});
			}
			else{
				var id_utilisateur;
				//Si on vient de lui créer un compte
				if (typeof nouveau_utilisateur == "object") {
					id_utilisateur = nouveau_utilisateur.id_utilisateur;
				}
				//Si l'utilisateur existe déjà
				else {
					//La variable utilisateur contient déjà l'utilisateur récupéré de la BDD
					id_utilisateur = utilisateur.id_utilisateur;
				}
				
				//On désarialise pour le renvoyer
				passport.deserializeUser(id_utilisateur, function (err, utilisateur) {
					// Et on le log pour le style.
					req.login(utilisateur, function(err) {
						if (err) {
							return next(err);
						}
						req.user.status = "ok";
						req.user._source = "pairform"; //Flag pour être sûr que la fenêtre parent récupère des infos venant de pairform, et pas d'autre messages postés par des popups ou jsaipaki

						if(nouveau_utilisateur) {
							return res.render(__dirname + "/../../authentification/views/callback_sso",{
								nouveau_utilisateur: JSON.stringify(req.user),
								url_serveur_node: CONFIG.app.urls.serveur_node
							});
						}
						else {
							return res.render(__dirname + "/../../authentification/views/callback_sso",{
								utilisateur: JSON.stringify(req.user),
								url_serveur_node: CONFIG.app.urls.serveur_node
							});
						}
					});
				});
			}
		})(req, res, next);
	 });

	/* Shibboleth */


	app.get('/webServices/utilisateur/shibboleth', function(req, res, next) {
	  passport.authenticate('shibboleth', function(err, nouveau_utilisateur, utilisateur) {
	    log.debug("err : ", err);
	    log.debug("nouveau_utilisateur : ", nouveau_utilisateur);
	    log.debug("utilisateur : ", utilisateur);

			if (err){
				return res.render(__dirname + "/../../authentification/views/callback_sso",{
					error: err.error,
					url_serveur_node: CONFIG.app.urls.serveur_node
				});
			}
			else{
				var id_utilisateur;
				//Si on vient de lui créer un compte
				if (typeof nouveau_utilisateur == "object") {
					id_utilisateur = nouveau_utilisateur.id_utilisateur;
				}
				//Si l'utilisateur existe déjà
				else {
					//La variable utilisateur contient déjà l'utilisateur récupéré de la BDD
					id_utilisateur = utilisateur.id_utilisateur;
				}
				
				passport.deserializeUser(id_utilisateur, function (err, utilisateur) {
					// Et on le log pour le style.
					req.login(utilisateur, function(err) {
						if (err) {
							return next(err);
						}
						req.user.status = "ok";
						req.user._source = "pairform"; //Flag pour être sûr que la fenêtre parent récupère des infos venant de pairform, et pas d'autre messages postés par des popups ou jsaipaki
						
						if(nouveau_utilisateur) {
							return res.render(__dirname + "/../../authentification/views/callback_sso",{
								nouveau_utilisateur: JSON.stringify(req.user),
								url_serveur_node: CONFIG.app.urls.serveur_node
							});
						}
						else {
							return res.render(__dirname + "/../../authentification/views/callback_sso",{
								utilisateur: JSON.stringify(req.user),
								url_serveur_node: CONFIG.app.urls.serveur_node
							});
						}
					});
				});
			}
	  })(req, res, next);
	});

	app.get('/webServices/utilisateur/shibboleth/error', function (req, res, next) {
	  log.debug(req.query);
	  res.render("error", {
	    query: req.query
	  });
	});

	/* JWT */


	app.post("/webServices/utilisateur/login_jwt", function(req, res, next) {
		log.debug("Going on login/jwt")
		passport.authenticate("login", function(err, utilisateur, info) {
			if (err){
				return res.json({
					"status" : "ko",
					"message" : "ws_erreur_contacter_pairform"
				});
			}
			else if(!utilisateur) {
				return res.json({
					"status" : "ko",
					"message" : info.message
				});
			}
			else{
				passport.deserializeUser(utilisateur.id_utilisateur, function (err, utilisateur) {
					req.login(utilisateur, function(err) {
						if (err) {
							return next(err);
						}

						var jwtoken = jwt.sign(req.user, CONFIG.app.jwt.secret, {
				        expiresIn: 0
				    });


						return res.json({
							"status" : "ok",
							"utilisateur" : req.user,
							"token" : jwtoken
						});
					});
				});
			}
		})(req, res, next);
	});

	function utilisateurEstConnecte(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    } else {
      // log.debug("Non connecté : " + req.originalUrl);
      res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
    }
  }
}
