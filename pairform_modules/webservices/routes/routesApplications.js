"use strict";

var web_services = require("../lib/webServices"),
		 express = require("express"),
	  constantes = require("../lib/constantes"),
		  multer = require('multer'),
		  CONFIG = require("config"),
			  fs = require('fs'),
			 log = require('metalogger')(),
		  crypto = require('crypto');


module.exports = function(app, passport) {

	// si l'utilisateur n'est pas connecté, on ne traite pas la requête
	function utilisateurEstConnecte(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			// log.debug("Non connecté : " + req.originalUrl);
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
		}
	}

	/*
	* Middleware qui va intercepter les thumbnails envoyé en base 64 du navigateur web
	* Elle va créer les fichiers sur le disque
	*/
	function fichiertiseLesBase64(req, res, next){
		var keys = Object.keys(req.body);
		req.body.nomsThumbnails = [];
		// log.debug(keys);
		//On intere sur tous les éléments du body
		for(var i = 0; i < keys.length; i++){

			//Si une clef contient le mot thumbnail alors c'est un thumbnail en base 64 envoyé par le navigateur web
			if(keys[i].indexOf("thumbnail") > -1){
				// log.debug(keys[i]);
				// log.debug(req.body[keys[i]]);
				var buffer = new Buffer(req.body[keys[i]], 'base64');
				//Je génére une clef unique de la même taille que multer
				var id = crypto.randomBytes(16).toString('hex');
				//Je récupére l'extension du fichier
				var extension = keys[i].split('.').pop();
				//J'assemble le nom du fichier
				var name = id + ".png";
				//J'ajoute dans le tableau des thumbnail recu qui sera traiter apres (voir la méthode envoyer dans messages.js:entities)
				req.body.nomsThumbnails.push({
					original: keys[i], //Le nom du thumbnail reliant avec la pj original
					nom: name //Le nom du fichier sur le serveur
				});

				//On créée le thumbnail sur le serveur
				fs.writeFile(constantes.REPERTOIRE_PIECES_JOINTES+"/"+name, buffer, function(err) {
					if(err) {
						return log.error(err);
					}
				}); 
				//On supprime les données base64 du body qui ne sont plus utile
				delete req.body[keys[i]];
			}
		}

		return next();
	}

	//--------------------------------- Application ---------------------------------

	/*
	 * Profil utilisateur
	 */
	
	// Récupère les informations sur le profil d'un utilisateur (nom/prénom, succès, ressources utilisées, etc.)
	app.get("/webServices/utilisateur/profil", function(req, res) {
		web_services.profil_utilisateur.getProfilUtilisateur(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	/*
	 * Export données personnelles
	 */
	
	// Exporte toutes les informations d'un utilisateur
	app.get("/webServices/utilisateur/exporter", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.export_donnees_personnelles.getExportDonneesPersonnelles(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	/*
	 * Suppression compte
	 */
	
	// Exporte toutes les informations d'un utilisateur
	app.post("/webServices/utilisateur/supprimer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.supprimerCompte(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	//--------------------------------- Entités ---------------------------------

	/*
	 * Ressource
	 */

	// Vérification de la présence de la ressource en BDD
	app.post("/webServices/ressource/verifier", function(req, res) {
		web_services.ressource.verifier(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Récuperer toutes les infos d'une ressource
	app.get("/webServices/ressource", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		req.query.email = req.isAuthenticated() ? req.user.email : undefined;
		web_services.ressource.getRessource(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Récupère toutes les ressources accessibles à l'utilisateur
	app.get("/webServices/ressource/liste", function(req, res) {
		req.query.email = req.isAuthenticated() ? req.user.email : undefined;
		web_services.ressource.getRessourcesAccessibles(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Récupère les dernières ressources publiés, accessibles à l'utilisateur
	app.get("/webServices/ressource/liste/recentes", function(req, res) {
		req.query.email = req.isAuthenticated() ? req.user.email : undefined;
		web_services.ressource.getRessourcesRecentes(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	/*
	 * Utilisateur
	 */

	// Vérification du role de l'utilisateur dans une ressource, sans check des paramètres d'une capsule
	app.post("/webServices/utilisateur/categorie/verifier/ressource", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.verifierCategorieRessource(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	

	// Vérification du role de l'utilisateur
	app.post("/webServices/utilisateur/categorie/verifier", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.verifierCategorie(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	
	// Recuperation des préférences 
	app.get("/webServices/utilisateur/preferences", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.getPreferences(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// editerAvatar
	app.post("/webServices/utilisateur/avatar", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.editerAvatar(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// editerCompte
	app.post("/webServices/utilisateur/editer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.authentification.editerCompte(
			req.body,
			passport,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// editer pseudo
	app.post("/webServices/utilisateur/editer/pseudo", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.authentification.editerPseudo(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// editerLangues
	// Sans passport
	// Patch pour la version mobile, qui envoie l'id d'utilisateur directement 
	app.post("/webServices/utilisateur/editerLangues", function(req, res) {
		if(req.isAuthenticated())
			req.body.id_utilisateur = req.user.id_utilisateur;
		
		web_services.utilisateur.editerLangues(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// afficherListeDeProfils
	app.get("/webServices/utilisateur/liste", function(req, res) {
		web_services.utilisateur.liste(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Récupère toutes les capsules pour parametrage des notifications
	app.get("/webServices/utilisateur/notification", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.listeParametresNotifications(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// activer / désactiver la notification par email
	app.post("/webServices/utilisateur/notification/mail", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.changerNewsParMail(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// activer / désactiver le watch de capsule pour notification par email
	app.post("/webServices/utilisateur/notification/watch", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.changerWatchCapsule(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	// activer / désactiver le watch de capsule pour notification par email
	app.post("/webServices/utilisateur/notification/nouvelles", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.changerNouvellesNotifications(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// changerNomsReels
	app.post("/webServices/utilisateur/noms/reels", utilisateurEstConnecte, function(req, res) {
		web_services.utilisateur.changerNomsReels(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// changerRole
	app.post("/webServices/utilisateur/role", utilisateurEstConnecte, function(req, res) {
		web_services.utilisateur.changerRole(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// donner un openbadge à un utilisateur
	app.put("/webServices/utilisateur/openbadge", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.donnerOpenBadge(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// récupère les openbadges qu'un utilisateur à le droit d'attribuer
	app.get("/webServices/utilisateur/openbadge/attribuables", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.utilisateur.getOpenBadgesAttribuables(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});



	/*
	 * Messages
	 */

	 // Suppression d'un tag
	app.get("/webServices/message/votes", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.message.recupererVotesMessage(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Suppression d'un tag
	app.delete("/webServices/message/tag", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.message.supprimerTag(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// ajouter une liste de Tags
	app.post("/webServices/message/tags", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.ajouterTags(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// deplacer un message
	app.post("/webServices/message/deplacer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.deplacer(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// donner une médaille à un message
	app.post("/webServices/message/donnerMedaille", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.donnerMedaille(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// enregistrer un nouveau message
	app.put("/webServices/message", utilisateurEstConnecte, multer({
			//destination de la sauvegarde des pièces jointes sur le serveur
			dest: constantes.REPERTOIRE_PIECES_JOINTES,
			limits: {
				files: constantes.MESSAGE_PJ.nb_pj,
				fileSize: constantes.MESSAGE_PJ.max_taille, //On passe de méga octets en Bytes
			},
			onFileUploadStart: function (file, req, res) {
				log.debug("Multer : onFileUploadStart", file)
			},
			onFileUploadData: function (file, data, req, res) {
				log.debug("Multer : onFileUploadData : " + data.length + ' of ' + file.fieldname + ' arrived')
			},
			onFileUploadComplete: function (file, req, res) {
				log.debug("Multer : onFileUploadComplete", file)
			},
			onFileSizeLimit: function (file) {
				log.debug("Multer : onFileSizeLimit", file)

				//Si le fichier est plus gros que la limite imposé
				//On supprime le fichier reçu.
				fs.unlink('./' + file.path) // delete the partially written file
			},
			onFilesLimit: function () {
				log.debug("Multer : onFilesLimit")
			},
			onFieldsLimit: function () {
				log.debug("Multer : onFieldsLimit")
			},
			onPartsLimit: function () {
				log.debug("Multer : onPartsLimit")
			},
			onParseStart: function () {
				log.debug("Multer : onParseStart")
			},
			onParseEnd: function (req, next) {
				log.debug("Multer : onParseEnd")

				// call the next middleware
				next();
			},
			onError: function (error, next) {
				log.error("onError")

				log.error(error)
				next(error)
			}
		}), fichiertiseLesBase64, function(req, res, next) {
			req.body.id_utilisateur = req.user.id_utilisateur;

			// log.debug("nomsThumbnails : ");
			// log.debug(req.body.nomsThumbnails);
			// log.debug("Nb files : " + Object.keys(req.files));
			log.debug(req.files);
			web_services.message.envoyer(
				req.body,
				req.files, //variable générée contenant la listes des fichiers récupérés
				req.user,
				function(retour_json) {
					res.json(retour_json);
				}
			);
		}
	);

	// recuperer une piece-jointe associé a un message
	app.get('/webServices/recupererPieceJointe', function(req, res){
		web_services.message.telechargerPJ(
			req.query,
			function(retour_json) {
				if(retour_json.status == 'ok'){
					//Warning : download met des headers spéciaux :
					// Content-Disposition
					// attachment
					//Ils servent au client iOS à savoir qu'on est dans le cas d'un téléchargement de fichier
					//Et pas d'une requête normale.
					res.download(retour_json.chemin, req.query['nom_serveur_pj']);
				}
				else
					res.json(retour_json);
			}
		);
	});

	// recuperer les informations sur les pièces jointes : limite de nombres, poids, formats, ...
	app.get('/webServices/informationsGeneral', function(req, res){
		res.json({
			"status" : "ok",
			"pj_data" : constantes.MESSAGE_PJ,
		});
	});

	// modifierLangue
	app.all("/webServices/message/modifierLangue", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.modifierLangue(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// recuperer une liste de messages (filtrée)
	app.get("/webServices/message", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.getMessages(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// recuperer une liste de messages (filtrée)
	app.get("/webServices/message/transversal", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		req.query.flag_filtre = false;

		web_services.message.getMessagesTransversal(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// recuperer une liste de messages (filtrée)
	app.get("/webServices/message/transversal/utilisateur", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur_concerne = req.query.id_utilisateur ? req.query.id_utilisateur : req.user.id_utilisateur;
		req.query.id_utilisateur_connecte = req.user.id_utilisateur;
		req.query.flag_filtre = true;
		web_services.message.getMessagesTransversal(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// recuperer une liste de messages (filtrée)
	app.get("/webServices/message/transversal/derniers", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.getDerniersMessagesTransversal(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// recupererNombreMessages
	app.get("/webServices/message/nombre", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.nombre(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Pour le bridge avec les anciennes version.
	app.get("/webServices/message/nombre/capsules", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.nombreParCapsules(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	//
	app.get("/webServices/message/nombre/ressource", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.message.nombreDansRessource(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// redirigerVersMessage
	app.get("/webServices/message/rediriger", function(req, res) {
		web_services.message.rediriger(
			req.query,
			function(retour_json) {
				if (retour_json.status == "ok") 
					res.redirect(retour_json.url);
				else
					res.redirect(CONFIG.app.urls.serveur_node + "/404");

			}
		);
	});
	
	// redirigerVersMessage
	app.post("/webServices/message/rediriger", function(req, res) {
		web_services.message.rediriger(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// supprimer un message
	app.post("/webServices/message/supprimer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		if (req.body.supprime_def) {
			req.body.est_admin_pairform = req.user.est_admin_pairform;
			web_services.message.supprimerDefinitivement(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			);
		}
		else{
			web_services.message.supprimer(
				req.body,
				function(retour_json) {
					res.json(retour_json);
				}
			);
		}
	});

	// terminer un défi
	app.post("/webServices/message/defi/terminer", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.terminerDefi(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// valider la reponse à un défi
	app.post("/webServices/message/defi/valider", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.validerReponseDefi(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	
	// vote +1 / -1 sur un message
	app.post("/webServices/message/voter", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.voter(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// GET : récupère les id des messages lus pour un utilisateur
	//		ressources - @array : identifiants des ressources à récupérer
	//		capsules - @array : identifiants des capsules à récupérer
	app.get("/webServices/message/lu", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.message.getMessagesLus(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// POST : ajoute en BDD l'état lu d'une liste de messages pour un utilisateur
	//		messages - @array [id_message, ...] 
	//		- identifiants des messages
	app.post("/webServices/message/lu", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.message.setMessagesLus(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
 

	/*
	 * Capsule
	 */


	// Rediriger vers une capsule avec l'id_capsule
	app.get("/webServices/capsule/rediriger", function(req, res) {
		web_services.capsule.rediriger(
			req.query,
			function(retour_json) {
				if (retour_json.status == "ok") 
					res.redirect(retour_json.url);
				else{
					res.redirect(CONFIG.app.urls.serveur_node + "/404");
				}

			}
		);
	});
	
	/*
	 * Succès
	 */

	//
	app.get("/webServices/accomplissement/succes", function(req, res) {
		req.query.id_utilisateur = req.isAuthenticated() ? req.user.id_utilisateur : -1000;
		web_services.accomplissement.liste(
			req.query,
			function(retour_json) {
				res.send(retour_json);
			}
		);
	});


	/*
	 * Notifications
	 */

	//
	app.get("/webServices/notification", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.notification.recuperer(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	//
	app.post("/webServices/notification/vue", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.notification.changerVueNotification(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	//
	app.post("/webServices/notification/vue/type", utilisateurEstConnecte, function(req, res) {
		var id_utilisateur = req.user.id_utilisateur;
		web_services.notification.changerVueNotificationType(
			id_utilisateur,
			req.body.type,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	/*
	 * Réseau
	 */

	// afficherListeCercles
	app.get("/webServices/reseau/liste", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.liste(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// ajouter un membre
	app.put("/webServices/reseau/utilisateur", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		req.body.pseudo_utilisateur = req.user.pseudo;
		web_services.reseau.ajouterMembre(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// creer un ercle
	app.put("/webServices/reseau/cercle", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.creerReseau(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// creer une classe
	app.put("/webServices/reseau/classe", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		req.body.reseau_est_classe = true;
		web_services.reseau.creerReseau(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// rejoindre une classe
	app.put("/webServices/reseau/classe/utilisateur", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		req.body.pseudo_utilisateur = req.user.pseudo;
		web_services.reseau.rejoindreClasse(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// supprimer un cercle
	app.delete("/webServices/reseau", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.supprimerCercle(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// supprimer un membre d'une classe/cercle (réseau)
	app.delete("/webServices/reseau/utilisateur", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.reseau.supprimerMembre(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});


	/*
	 * Objectifs d'apprentissage
	 */


	// Mise à jour d'un profil d'apprentissage
	app.get("/webServices/objectif/:id_ressource", utilisateurEstConnecte, function(req, res) {
		var id_utilisateur = req.user.id_utilisateur;
		web_services.objectif_apprentissage.getObjectifsApprentissageByUtilisateurEtRessource(
			req.param("id_ressource"),
			id_utilisateur,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// création d'un nouvelle objectif d'apprentissage
	app.put("/webServices/objectif", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.objectif_apprentissage.putObjectifApprentissage(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// Mise à jour d'un objectif d'apprentissage
	app.post("/webServices/objectif", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.objectif_apprentissage.postObjectifApprentissage(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	// suppression d'un objectif d'apprentissage
	app.delete("/webServices/objectif", utilisateurEstConnecte, function(req, res) {
		req.query.id_utilisateur = req.user.id_utilisateur;
		web_services.objectif_apprentissage.deleteObjectifApprentissage(
			req.query,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	/*
	 * Profils d'apprentissage
	 */

	// Mise à jour d'un profil d'apprentissage
	app.get("/webServices/profil/:id_ressource", utilisateurEstConnecte, function(req, res) {
		var id_utilisateur = req.user.id_utilisateur;
		web_services.profil_apprentissage.getProfilApprentissageByUtilisateurEtRessource(
			req.param("id_ressource"),
			id_utilisateur,
			"fr",
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});
	// Mise à jour d'un profil d'apprentissage
	app.post("/webServices/profil", utilisateurEstConnecte, function(req, res) {
		req.body.id_utilisateur = req.user.id_utilisateur;
		web_services.profil_apprentissage.postProfilApprentissage(
			req.body,
			function(retour_json) {
				res.json(retour_json);
			}
		);
	});

	app.get("/debug/mail", function (req, res) {
		var nodemailer = require("nodemailer");

		var smtp_transport = nodemailer.createTransport(CONFIG.app.smtp);

		var mail_options = {
			to: "contact@pairform.fr",
			from: 'postmaster@mail.pairform.fr',
			subject: '[PairForm] Test de mail',
			html: "Test de message"
		};
		smtp_transport.sendMail(mail_options, function(err) {
			// On passe au suivant
			log.debug("Mail : " + err);
			res.send(err);
		});
	})
}