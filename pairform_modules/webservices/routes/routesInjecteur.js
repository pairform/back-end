"use strict";

var web_services = require("../lib/webServices"),
		 express = require("express"),
			 log = require('metalogger')(),
	  constantes = require("../lib/constantes");


module.exports = function(app, passport) {

	// si l'utilisateur n'est pas connecté, on ne traite pas la requête
	function utilisateurEstConnecte(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
		}
	}

	// si l'utilisateur n'est pas autorisé à réaliser l'action, on ne traite pas la requête
	function utilisateurEstAutorise(res, autorisation, traiterRequete) {
		if (autorisation) {
			traiterRequete();
		} else {
			log.warning("Injecteur - autorisation action : pas ok");
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_AUTORISE);
		}
	}


	/*
	 *	Routes de l'injecteur
	 */

	// connexion sur les applications (web, ios, and)
	app.post("/webServices/injecteur/utilisateur/login", function(req, res, next) {
		res.redirect("/webServices/utilisateur/login");
	});

	// déconnexion d'un utilisateur de l'application web
	app.post("/webServices/injecteur/utilisateur/logout", function(req, res) {
		res.redirect("/webServices/utilisateur/logout");
	});

	// Réinitialisation du mot de passe d'un utilisateur (via son email)
	app.post("/webServices/injecteur/utilisateur/reset", function(req, res) {
		res.redirect("/webServices/utilisateur/reset");
	});

	// Attention : PUT sur web, POST sur anciennes versions mobiles
	app.all("/webServices/injecteur/utilisateur/enregistrer", function(req, res, next) {
		res.redirect("/webServices/utilisateur/enregistrer");
	});

	// Récupération des données du tableau de bord (liste des espaces, liste des ressources, liste des capsules)
	app.get("/webServices/injecteur/infosTableauDeBord", utilisateurEstConnecte, function(req, res) {
		res.redirect("/webServices/infosTableauDeBord");
	});

	// création d'une nouvelle capsule, créé aussi la ressource s'il elle n'existe pas
	app.put("/webServices/injecteur/capsule", utilisateurEstConnecte, function(req, res) {
		var capsule = req.body;
		var autorisation = req.user.est_admin_pairform || (req.user.liste_roles[capsule.id_espace] && req.user.liste_roles[capsule.id_espace][constantes.GERER_RESSOURCES_ET_CAPSULES]);

		utilisateurEstAutorise(res, autorisation, function() {
			log.debug(capsule.ressource);
			//On copie le nom court dans le nom long
			capsule.nom_long = capsule.nom_court;
			
			//S'il n'y a pas de ressource attachée
			if (capsule.ressource.id_ressource === 0) {
				//On change le titre "Nouveau document" par le nom de la capsule, pour éviter les duplicates
				capsule.ressource.nom_court = capsule.nom_court;
				capsule.ressource.nom_long = capsule.nom_long;
				//On créé la ressource avec les parametres de la capsule
				utilisateurEstAutorise(res, autorisation, function() {
					web_services.ressource_backoffice.putRessource(
						capsule.ressource,
						req.user.id_utilisateur,
						function(retour_json) {
							if (retour_json.status === "ok") {
								//On change le paramètre dans le corps de la requête
								capsule.id_ressource = retour_json.id_ressource;
								//Et ensuite on créé la capsule
								web_services.capsule_backoffice.putCapsule(
									capsule,
									req.user.id_utilisateur,
									function(retour_json) {
										res.json(retour_json);
									}
								)
							}
							else {
								res.json(retour_json);
							}
						}
					)
				});
			}
			else {
				//Sinon, on créé la capsule normalement
				web_services.capsule_backoffice.putCapsule(
					capsule,
					req.user.id_utilisateur,
					function(retour_json) {
						res.json(retour_json);
					}
				)
			}
		});
	});
}