"use strict";

var express = require("express"),
	request = require("request"),
		log = require('metalogger')(),
	 CONFIG = require("config");


/*
 *		Redirection des requetes HTTP des applications iOs et Android
 *		Tout ce qui est lié aux ressources/capsules est redirigé vers l'ancienne BDD (SCElgg)
 *		Les reste est redirigé vers la nouvelle BDD (PairForm_V2) 
 *				------------
 *		Fichier temporaire
 *		Supprimer le  fichier quand les applications mobiles seront à jour et que les utilisateurs n'utiliseront plus les anciennes versions
 */


module.exports = function(app, passport) {

	app.post("/webServices/bridge/:type/:entite_1/:action/:entite_2", function(req, res) {
		var url; 
		var	type = req.params.type;

		if(req.params.entite_1 && req.params.action != "0" && req.params.entite_2 != "0" ) {
			url = CONFIG.app.urls.serveur_node + ":" + CONFIG.app.port  + "/webServices/" + req.params.entite_1 +"/"+ req.params.action +"/"+ req.params.entite_2;
		}
		else if(req.params.entite_1 && req.params.action != "0" && req.params.entite_2 == "0") {
			url = CONFIG.app.urls.serveur_node + ":" + CONFIG.app.port  + "/webServices/" + req.params.entite_1 +"/"+ req.params.action;
		}
		else if(req.params.entite_1 && req.params.action == "0" && req.params.entite_2 == "0") {
			url = CONFIG.app.urls.serveur_node + ":" + CONFIG.app.port  + "/webServices/" + req.params.entite_1;
		}
		log.debug("### BRIDGE WS");
		log.debug("URL : " + req.url + " URL finale :" + url);
		log.debug("Methode : " + type);
		log.debug("Parametres : " + JSON.stringify(req.body));
		request({
			uri: url,
			method: type,
			qs: req.body,
			form: req.body
		}, function(erreur, response, body) {
			// log.debug("Reponse : " + JSON.stringify(response));
			if (erreur) {
				log.error("erreur bridge redirection mobile : " + erreur);
				res.json({"status" : "ko"});
			} else {
				var retour_json = JSON.parse(body);
				res.json(retour_json);
			}
		});
	});

}