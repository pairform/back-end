var crypto = require("crypto"),
    CONFIG = require("config");

exports.encrypt = function (text){
  if (typeof text == "number"){
    text += "";
  }
  var cipher = crypto.createCipher('aes-256-ctr', CONFIG.app.notifications.secret)
  var crypted = cipher.update(CONFIG.app.notifications.salt + text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
exports.decrypt = function (text){
  var decipher = crypto.createDecipher('aes-256-ctr', CONFIG.app.notifications.secret)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec.replace(CONFIG.app.notifications.salt, "");
}
 
