var express = require("express"),
	 CONFIG = require("config");


app = exports = module.exports = express();

// On ne laisse passer que si l'utilisateur est connecté & admin pairform
app.get("/utils/compatibilite", function(req, res) {
	res.render(__dirname + "/views/compatibilite",{
		"url_serveur_node" : CONFIG.app.urls.serveur_node
	});
});
