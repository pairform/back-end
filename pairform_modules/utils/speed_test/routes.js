var CONFIG = require("config");

module.exports = function(app, passport) {

	// On ne laisse passer que si l'utilisateur est connecté & admin pairform
	app.get("/utils/speed_test", function utilisateurEstConnecteEtAdmin (req, res, next) {
		var unauthorized = false;
		if (req.isAuthenticated()) {
			if (req.user.est_admin_pairform) {
				return next();
			}
			else
				unauthorized = true;
		}	

		//Sinon, on envoie la page jade login avec les infos de la ressource
		res.render(__dirname + "/../../authentification/views/gate_keeper",{
			url_serveur_node : CONFIG.app.urls.serveur_node,
			url_to_follow : "/utils/speed_test",
			espace_nom_long : "PairForm",
			id_capsule : 0,
			id_ressource : 0,
			unauthorized : unauthorized
		});
	
	}, function(req, res) {
		res.render(__dirname + "/views/speed_test", {
			url_serveur_node : CONFIG.app.urls.serveur_node
		});
	});
}