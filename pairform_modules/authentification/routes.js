var web_services = require("../webservices/lib/webServices"),
		  CONFIG = require("config");



//Page autonome de login

// app.get("/login", function (req, res) {		
// 	res.render(__dirname + "/views/login",{
// 		url_to_follow : "",
// 		// unauthorized : true
// 	});
// });

module.exports = function(app, passport) {

	// resetPassword
	app.get("/reset/:token", function(req, res) {
		web_services.authentification.resetPasswordStep2(
			req.params,
			function(retour) {
				//Flag de token invalide :
				//Le token est soit expiré, soit non retrouvé dans la BDD
				var token_expired = retour.status == "ko";
				//On passe ça en paramètre à la view (dossier root views)
				res.render("reset_password", {
					"token_expired" : token_expired,
					"url_serveur_node" : CONFIG.app.urls.serveur_node
				});
			}
		);
	});


	// resetPassword
	app.get("/validation/:token", function(req, res) {
		web_services.authentification.validerEmail(
			req.params,
			function(retour) {

				var context = {
					"url_serveur_node" : CONFIG.app.urls.serveur_node
				};
				//Flag de token invalide :
				//Le token est soit expiré, soit non retrouvé dans la BDD
				if (retour.status == "ko") {
					context.erreur = retour.message;
					//On passe ça en paramètre à la view (dossier root views)
					res.render("validation_compte", context);
				}
				else{
					//Forcing MAJ de l'utilisateur actuel pour passport, pour refléter le changement
					req.session.passport.user.email_est_valide = 1;
					passport.deserializeUser(retour.data.utilisateur.id_utilisateur, function (err, utilisateur) {
						//On passe ça en paramètre à la view (dossier root views)
						res.render("validation_compte", context);
						context.utilisateur = utilisateur;
					});
				}
			}
		);
	});

	app.get("/lti/test/:status", function (req, res, next) {
    res.render(__dirname + "/node_modules/lti/views/lti.jade", {
					"url_serveur_node" : CONFIG.app.urls.serveur_node,
					"status" : req.param("status") || "ko",
					"messages" : [
						"no_url",
						"already_defined",
						"not_authorized_yet",
						"no_key",
						"not_authorized",
						"no_lti",
						"no_email",
						"bad_credentials"
					],
					"lti_params" : {
						user_image: "/res/avatars/1.png",
						nom_prenom: "Octo Pus",
						email: "octopus@test.net",
						launch_presentation_return_url: "https://www.test.fr"
					},
					utilisateur : {
						avatar_url: "/res/avatars/1.png",
						pseudo: "Octopus",
						email: "octopus@test.net",
						id_source_auth: 1
					}
				})
  })

	app.get("/sso/test/:nouveau", function (req, res, next) {
    var param = {
					"url_serveur_node" : CONFIG.app.urls.serveur_node,
					"status" : "ok",
					"messages" : [
						"no_url",
						"already_defined",
						"not_authorized_yet",
						"no_key",
						"not_authorized",
						"no_lti",
						"no_email",
						"bad_credentials"
					]
			},
			user = {
				avatar_url: "/res/avatars/1.png",
				pseudo: "Octopus",
				email: "octopus@test.net",
				id_source_auth: 1
			}
		if(req.param("nouveau") == "nouveau")
			param["nouveau_utilisateur"] = user;
		else 
			param["utilisateur"] = user;

		res.render(__dirname + "/views/callback_sso", param)
  })
}