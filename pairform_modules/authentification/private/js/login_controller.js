$_pf(function () {
	window.setTimeout(function(){
		$_pf('html').removeClass("animated");
	}, 700);

});

var request_pending = false, number_of_attempts = 0;
var pf_login = angular.module('pf_login', ["pascalprecht.translate", "toaster"]);


pf_login.config(["$translateProvider", function ($translateProvider) {
		$translateProvider.useSanitizeValueStrategy('sanitizeParameters');
		$translateProvider.useStaticFilesLoader({
			files:[{
				prefix: PF.url_serveur_node + '/private/json/authentificationLangue_',
				suffix: '.json'
			},{
				prefix: PF.url_serveur_node + '/private/json/',
				suffix: '/strings.json'
			}]
		});
		//Anglais par défaut, si le navigateur n'a pas la propriété définie
		//userLanguage pour IE
		// var code_langue = PF._localStorage["ngStorage-code_langue_app"] ? JSON.parse(PF._localStorage["ngStorage-code_langue_app"])[0] : (window.navigator.userLanguage || window.navigator.language || "en").substr(0, 2);
		var code_langue =(window.navigator.userLanguage || window.navigator.language || "en").substr(0, 2);

		//S'il n'y a pas de langue compatible
		// if (!PF.langue.tableau_inverse[code_langue])
			// code_langue = "en";

		$translateProvider.preferredLanguage(code_langue);
		// PF._localStorage.setItem("ngStorage-code_langue_app", JSON.stringify([code_langue]));
	}]);


pf_login.controller('controller_connexion', ["$scope", "$translate", "toaster", "$http", function ($scope, $translate, toaster, $http) {
		var defaults = {
			"visible" : false,
			"mode" : "connexion",
			"mot_de_passe_oublie" : {}
		};

		angular.extend($scope, defaults);

		$translate(["WEB_LABEL_VALIDER_MAIL", "WEB_LABEL_ACCES_INTERDIT"]).then(function (translation) {
			if (PF.unauthorized){
				toaster.pop("error",translation.WEB_LABEL_ACCES_INTERDIT);
				
				if (PF.unauthorized_explication == "email_non_valide"){
					toaster.pop(
							"warning",
						  translation.WEB_LABEL_VALIDER_MAIL,
						  '<div class="btn btn-action" onclick="renvoyerConfirmation()">Renvoyer l\'e-mail de confirmation</div>',
						  0,
						  'trustedHtml'
						);
				}
			}
		})

		$scope.connecter = function (){
			if (request_pending) {
				toaster.pop("warning","Veuillez patienter, la connexion est en cours...");
				return false;
			}
			request_pending = true;
			number_of_attempts++;

			toaster.pop("info","Connexion en cours...");
			// var postData = $_pf(this).serialize()+"&os=web&version=2";
			angular.extend($scope.connexion, {"os" : "web", "version" : "2"});
			$http.post(PF.url_serveur_node + "/webServices/utilisateur/login", $scope.connexion).then(function(data){
				request_pending = false;
				var retour = data.data;
				if (retour['status'] == 'ko')
				{
					if (retour["message"] == "ws_utilisateur_invalide")
						toaster.pop("error","Identifiants invalides : veuillez réessayer.");
					else
						toaster.pop("error",retour["message"].toUpperCase());

					if (number_of_attempts >= 2) {
						toaster.pop("warning","Mot de passe oublié ?","<div class='btn-action' onclick='afficherResetPass()'>Cliquez ici</div>", 0, 'trustedHtml');
					}
				}
				else
				{
					toaster.pop("success","Connexion réussie, redirection vers la page en cours...");
					//On stocke dans le localstorage l'utilisateur
					//Avec le flag connecté
					var user = new Utilisateur(retour);

					user.est_connecte = true;

					localStorage["ngStorage-utilisateur_local"] = JSON.stringify(user);
					$_pf('html').addClass('animated');
					$_pf('body').addClass('fade-out');

					window.setTimeout(function(){
						// window.location.replace(PF.url_to_follow);
						window.location.reload();
					}, 1000);
				}
			});
			return false; // ne change pas de page
		}
		$scope.connecterLinkedin = function (){
			$scope.window_sso = window.open(PF.url_serveur_node + "/webServices/utilisateur/linkedin", 
	           "pf_linkedin", 
	           "width=400,height=600,resizable,scrollbars,status");
		}

		$scope.connecterShibboleth = function (){
			$scope.window_sso = window.open(PF.url_serveur_node + "/webServices/utilisateur/shibboleth", 
	           "pf_shibboleth", 
	           "width=740,height=660,resizable,scrollbars,status");
		}
		$scope.listenMessageSSO = function () {
			//https://stackoverflow.com/a/25098153/1437016
			window.addEventListener('message', function(event) { 
			    // IMPORTANT: Check de l'origine
			    //Check qu'il y a bien une fenêtre SSO ouverte
			    //Et check de la source, pour être sûr que ça vient bien de pairform
					console.log(event); 
			    if ($scope.window_sso
			    	&& (event.origin+'/').indexOf(PF.url_serveur_node) != -1 
			    	&& event.data && event.data._source == "pairform") { 
			        $scope.callbackSSO(event.data);
			    } else { 
			        //On stop net sinon
			        return; 
			    } 
			}); 
		}
		$scope.listenMessageSSO();
		$scope.callbackSSO = function (utilisateur, err) {
			if (utilisateur && ! err)
			{
				toaster.pop("success","Connexion réussie, redirection vers la page en cours...");
				//On stocke dans le localstorage l'utilisateur
				//Avec le flag connecté
				var user = new Utilisateur(utilisateur);

				user.est_connecte = true;

				localStorage["ngStorage-utilisateur_local"] = JSON.stringify(user);
				$_pf('html').addClass('animated');
				$_pf('body').addClass('fade-out');
				$scope.window_sso.close();
				
				window.setTimeout(function(){
					// window.location.replace(PF.url_to_follow);
					window.location.reload();
				}, 1000);
			}
			else
			{				
				toaster.pop("error", "TITLE_ERREUR_IDENTIFIANTS");

				_paq.push(['trackEvent', 'Connexion - Linkedin', 'Échouée']);
			}
		}
		$scope.renvoyerMotDePasse = function (){
			
			var _scope = $scope;
			angular.extend(this.mot_de_passe_oublie, {"os" : "web", "version" : "2"});
			
			$http.post(PF.url_serveur_node + '/webServices/utilisateur/reset', this.mot_de_passe_oublie).then(function callback_success (data) {
				var retour = data.data;
				if (retour['status'] == 'ok') {
					toaster.pop("success",'Un e-mail de réinitialisation a été envoyé à l\'adresse indiquée.');
				}
				else {
					toaster.pop("error",retour['message'].toUpperCase());
				}
			});
		}
		$scope.enregistrer = function (){
			//Attention : register_cgu à la place de register-cgu
			var _scope = $scope;
			angular.extend(this.enregistrement, {"os" : "web", "version" : "2", "langue" : 3});
			$http.put(PF.url_serveur_node + '/webServices/utilisateur/enregistrer', this.enregistrement)
			.then(function callback_success (data) {
					var retour = data.data;
					if (retour['status'] == 'ok') {
						toaster.pop("success",'Votre compte a été créé avec succès!');
						_scope.mode = "enregistrement-1";
						//On stocke dans le localstorage l'utilisateur
						//Avec le flag connecté
						var user = new Utilisateur(retour.utilisateur);

						user.est_connecte = true;

						localStorage["ngStorage-utilisateur_local"] = JSON.stringify(user);
					}
					else {
						toaster.pop("error", retour['message'].toUpperCase());
					}
				}
			);
		}
		$scope.rafraichirPage = function () {
			$_pf('html').addClass('animated');
			$_pf('body').addClass('fade-out');

			window.setTimeout(function(){
				// window.location.replace(PF.url_to_follow);
				window.location.reload();
			}, 1000);
		}

		$scope.afficherResetPass = function () {
			$scope.mode = 'mot_de_passe';
		}
		
		$scope.renvoyerConfirmation = function () {
			$http.post(PF.url_serveur_node + '/webServices/utilisateur/confirm').then(function callback_success (data) {
				var retour = data.data;
				if (retour['status'] == 'ok') {
					toaster.pop("success",'Un e-mail de validation vous a été envoyé.');
				}
				else {
					toaster.pop("error",retour['message']);
				}
			});
		}
	}]);

function afficherResetPass (){
	var _scope = angular.element($_pf("[ng-controller=controller_connexion]")).scope();
	_scope.$apply(function () {
		_scope.afficherResetPass();
	})
}
function renvoyerConfirmation (){
	var _scope = angular.element($_pf("[ng-controller=controller_connexion]")).scope();
	_scope.$apply(function () {
		_scope.renvoyerConfirmation();
	})
}

function Utilisateur (options) {
	this.id_utilisateur = 0;
	this.username = "";
	this.name = "";
	this.avatar_url = "";
	this.etablissement = "";
	this.email = "";
	this.rank = {};
	this.rank[PF.gate_keeper.id_capsule] = {
		"id_categorie" : 0,
		"id_ressource" : PF.gate_keeper.id_ressource,
		"nom_categorie": "Lecteur",
		"score": 0
	};

	this.estExpert = false;
	this.elggperm = "";
	this.langue_principale = "fr";
	this.autres_langues = ["1","2"];
	this.categorie = {};
	this.est_admin = false;

	this.est_connecte = false;
	this.preferences = {
		"expanded" : false,
		"ordreTri" : 0,
	};

	angular.extend(this,options);
	this.rank_ressources = {};

	var _this = this;
	$_pf.each(this.rank, function (id_capsule, categorie) {
		if(!_this.rank_ressources[categorie.id_ressource])
			_this.rank_ressources[categorie.id_ressource] = categorie;
	});

	//Au cas où il n'y a pas le bon champ renvoyé par le webServices
	if (this.username == "")
		this.username = this.name;
	else if (this.username == "")
		this.name = this.username;
	
}