$(function () {
  toastr.options = {
    "positionClass": "toast-bottom-full-width"
  }
  var request_pending = false;
  $('#form_pseudo').submit(function (e) {
    if (request_pending) {
      toastr["warning"]("Veuillez patienter, l'enregistrement est en cours...");
      return false;
    }
    request_pending = true;
    e.preventDefault();
    toastr["info"]("Enregistrement en cours...");
    var postData = $(this).serialize();
    $.post(PF.url_serveur_node + "/webServices/utilisateur/editer/pseudo", postData, function(retour){
      request_pending = false;
      if (retour['status'] == 'ko') {
        if (retour['message'] == "label_erreur_saisie_champ_vide")
          toastr["error"]("Tous les champs obligatoires du formulaire n'ont pas &eacute;t&eacute; renseign&eacute;s.");
        else if (retour['message'] == "ws_erreur_nom_utilise")
          toastr["error"]("Votre nom d'utilisateur est d&eacute;jà utilis&eacute;. Veuillez en choisir un autre.");
        else
            toastr["error"]("Erreur d'enregistrement : veuillez contacter l'équipe PairForm par mail à l'adresse contact@pairform.fr.");
      }
      else {
        toastr["success"]("Enregistrement r&eacute;ussi.");
        
        //On remplace le pseudo dans l'utilisateur à logger
        user._source = "pairform";
        user.username = document.querySelector("[name='pseudo']").value;
        user.pseudo = user.username;

        window.opener.postMessage(user, "*");
      }
    });
    return false; // ne change pas de page
  });
});