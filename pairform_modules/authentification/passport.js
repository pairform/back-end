"use strict";

var async = require('async'),
	  log = require('metalogger')(),
	  constantes = require("../webservices/lib/constantes"),
	pairform_dao = require("../webservices/node_modules/pairform_dao");


module.exports = function(passport) {

	/* Inclusion des stratégies */
	
	require("local")(passport);
	require("lti")(passport);
	require("linkedin")(passport);
	require("shibboleth")(passport);

	// serialize un utilisateur pour lui attribuer une session
	passport.serializeUser( function(utilisateur_connecte, done) {
		done(null, utilisateur_connecte);
	});

	// deserialize l'utilisateur connecte
	passport.deserializeUser( function(utilisateur_ou_id, done) {		
		// La première fois, on essaie de choper l'utilisateur via son ID
		// Si ce n'est pas le cas, on passe l'objet tel quel
		if (typeof utilisateur_ou_id === "object" && typeof utilisateur_ou_id.id_utilisateur === "number")
			return done(null, utilisateur_ou_id);

		//Sinon, on va requeter la base comme avant
		var id_utilisateur = utilisateur_ou_id;
		pairform_dao.getConnexionBDD(function (_connexion_courante) {
			async.waterfall([
				function selectUtilisateur(next) {
					pairform_dao.utilisateur_dao.selectUtilisateurPourSerialization(id_utilisateur,
						function callback_success(utilisateurs) {
							if (!utilisateurs.length) {
								//TODO : gérer l'erreur
								log.error("Pas d'utilisateur correspondant");
								//On arrête ici
								return next({code : "no_user"});
							};
							var utilisateur_BDD = utilisateurs[0];
							var utilisateur = {
								id_utilisateur: utilisateur_BDD.id_utilisateur,
								pseudo: utilisateur_BDD.pseudo,
								username: utilisateur_BDD.pseudo,
								avatar_url: utilisateur_BDD.avatar_url,
								email: utilisateur_BDD.email,
								email_est_valide: utilisateur_BDD.email_est_valide,
								etablissement: utilisateur_BDD.etablissement,
								est_admin_pairform: utilisateur_BDD.est_admin,
								notifications_envoyees_par_mail: utilisateur_BDD.notifications_envoyees_par_mail
							};
							next(null, utilisateur);
						},
						function callback_error (erreur) {
							log.error("erreur requete SQL : " + erreur.code);
							next(erreur, null);
						}
					);
				},
				function selectLanguesUtilisateur(utilisateur, next) {
					pairform_dao.utilisateur_dao.selectLanguesUtilisateur(utilisateur.id_utilisateur,
						function callback_success(langues) {
							if (!langues.length) {
								//TODO : gérer l'erreur
								log.notice("Pas de langues utilisateur correspondantes");
							} else {
								utilisateur.langue_principale = langues[0].code_langue;		// la langue principale a été placé dans le première élément du tableau
								utilisateur.id_langue_principale = langues[0].id_langue;		// la langue principale a été placé dans le première élément du tableau
								utilisateur.autres_langues = [];

								for (var i=1; i<langues.length; i++) {
									utilisateur.autres_langues.push( langues[i] );
								};
							}
							next(null, utilisateur);
						},
						function callback_error (erreur) {
							log.error("erreur requete SQL : " + erreur.code);
							next(erreur, null);
						}
					);
				},
				function selectRolesByEmail(utilisateur, next) {
					// récupération du nom de domaine de l'email
					var nom_de_domaine = utilisateur.email.substring( utilisateur.email.indexOf("@") + 1 );

					pairform_dao.utilisateur_dao.selectUtilisateurRolesEspaceParUtilisateur(utilisateur.email, nom_de_domaine,
						function callback_success(liste_roles_espace) {
							if (liste_roles_espace.length) {
								utilisateur.liste_roles = {};
								// pour chaque rôle de l'utilisateur
								for (var clef in liste_roles_espace) {
									utilisateur.liste_roles[liste_roles_espace[clef].id_espace] = liste_roles_espace[clef];
								};
							}

							pairform_dao.utilisateur_dao.selectUtilisateurRolesRessourceParUtilisateur(utilisateur.email, nom_de_domaine,
								function callback_success(liste_roles_ressource) {
									if (liste_roles_ressource.length) {

										if (!utilisateur.liste_roles)
											utilisateur.liste_roles = {};

										// pour chaque rôle de l'utilisateur
										for (var clef in liste_roles_ressource) {
											// si, pour cette espace, un rôle n'a pas déjà été ajouté à la liste
											if (!utilisateur.liste_roles[liste_roles_ressource[clef].id_espace]) {
												utilisateur.liste_roles[liste_roles_ressource[clef].id_espace] = {};
											}
											utilisateur.liste_roles[liste_roles_ressource[clef].id_espace][liste_roles_ressource[clef].id_ressource] = liste_roles_ressource[clef];
										};
									}
									next(null, utilisateur);
								},
								function callback_error (erreur) {
									log.error("erreur requete SQL : " + erreur.code);
									next(erreur, null);
								}
							);
						},
						function callback_error (erreur) {
							log.error("erreur requete SQL : " + erreur.code);
							next(erreur, null);
						}
					);
				},
				function selectGroupesUtilisateur(utilisateur, next) {
					//Récupération des groupes ordonnées par id_espace
					pairform_dao.groupe_dao.selectGroupesUtilisateur(utilisateur.id_utilisateur,
						function callback_success(groupes) {
							if (groupes.length) {
								utilisateur.groupes = {};
								// pour chaque rôle de l'utilisateur
								for (var index in groupes) {
									utilisateur.groupes[groupes[index].id_espace] = groupes[index];
								};
							}
									next(null, utilisateur);
						},
						function callback_error (erreur) {
							log.error("erreur requete SQL : " + erreur.code);
							next(erreur, null);
						}
					);
				},
				function selectSessionsUtilisateur(utilisateur, next) {
					//Récupération des sessions ordonnées par id_ressource
					pairform_dao.session_dao.selectSessionsUtilisateur(utilisateur.id_utilisateur,
						function callback_success(sessions) {
							if (sessions.length) {
								utilisateur.sessions = {};
								// pour chaque rôle de l'utilisateur
								for (var index in sessions) {
									utilisateur.sessions[sessions[index].id_ressource] = sessions[index];
								};
							}
									next(null, utilisateur);
						},
						function callback_error (erreur) {
							log.error("erreur requete SQL : " + erreur.code);
							next(erreur, null);
						}
					);
				},
				function selectParametresEtCategories(utilisateur, next) {
					pairform_dao.utilisateur_dao.selectTousParametresEtCategories(utilisateur.id_utilisateur,
						function callback_success(retour_sql) {
							utilisateur.rank = {};
							// Champ spécifique - pour les versions mobiles
							//  Besoin de savoir si un utilisateur est expert sur au moins une section
							//  pour activer / désactiver la création de classes sur iOS / Android
							utilisateur.estExpert = false;

							// Itération à travers les rangs récupérés dans la BDD
							for (var index in retour_sql) {
								var rang = retour_sql[index];
								utilisateur.rank[rang.id_capsule] = rang;
								// S'il est expert || admin
								if (rang.id_categorie >= 4)
									// On met le flag
									utilisateur.estExpert = true;
							}
							pairform_dao.libererConnexionBDD(_connexion_courante);
							next(null, utilisateur);
						},
						function callback_error (erreur) {
							log.error("erreur requete SQL : " + erreur.code);
							next(erreur, null);
						}
					);
				},
				function envoieDonneesPJ (utilisateur, next) {
					process.nextTick(function () {
						utilisateur.pj_data = constantes.MESSAGE_PJ;
						setImmediate(function(){
							next(null, utilisateur);
						})
					})
				}
			],
			done);
		});
	});

};

function castAllToStrings (object) {
	var type = object instanceof Array ? "array" : typeof object;

	try{
		var retour = JSON.parse(JSON.stringify([object], function(k, v){ 
		   if(typeof v !== "string" && typeof v !== "object" && typeof v !== "array") 
		       return String(v); 
		   return v;
		}));
		return type === "object" ? retour[0] : retour;
	}
	catch(e){
		return type === "object" ? {} : [];
	}
}