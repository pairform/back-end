"use strict";

// ajout des modules
var app = angular.module("backoffice", [
	"ngRoute",						// module de gestion des routes
	"ngStorage",					// module de gestion du local storage
	"pascalprecht.translate",		// module internationalisation
	"ngFileUpload",					// module permettant l'upload de fichier (archives des capsules)
	"angularFileUpload",			// module permettant l'upload de fichier (logo)
	"toaster",						// module de gestion des toasts de notification
	"backofficeFiltres",			// module des filtres
	"backofficeServices",			// module des filtres
	"backofficeControlleurs"		// module des controllers
]);

/*
 * déclaration des constantes
 */

var OS_BACKOFFICE = "backoffice";				// type de la plateforme - envoyé en paramètre de chaque requete aux webservices PairForm
var VERSION_BACKOFFICE = "0.9.002";				// version courante du backoffice (cf. package.json) - envoyé en paramètre de chaque requete aux webservices PairForm
var URL_SERVEUR_NODE = PF.globals.url.root; // url racine du serveur node
var URL_SERVEUR_ETHERPAD = PF.globals.url.etherpad;	// url racine du serveur node
// var URL_BACKOFFICE_RELATIVE = "backoffice";	// url relative du backoffice
var URL_BACKOFFICE = "/backoffice";	// url racine du backoffice
var RETOUR_JSON_OK = "ok";						// Status d'une requete JSON s'étant bien déroulée
var RETOUR_JSON_KO = "ko";						// Status d'une requete JSON s'étant mal déroulée (erreur SQL, connexion internet suspendu, utilisateur n'ayant pas les droits sur l'action, etc.)
var ERREUR_DOUBLON = "ER_DUP_ENTRY";										// code d'erreur correspondant à un champ déjà existant en BDD (ex : nom d'un espace)
var ERREUR_UTILISATEUR_NON_CONNECTE = "ws_utilisateur_invalide";			// code d'erreur correspondant à un un utilisateur inexistant (couple login / mot de passe inexistant lors de la connexion, email in-existant, etc.)
var ERREUR_UTILISATEUR_NON_AUTORISE = "ws_utilisateur_non_autorise";		// code d'erreur correspondant à une action non autorisé pour un utilisateur
var ERREUR_EMAIL_UTILISATEUR_NON_VALIDE = "ws_mail_utilisateur_pas_valide";	// code d'erreur correspondant à un utilisateur inscrit mais qui n'a pas encore validé son email d'inscription

// liste des types possible pour une archive zip
var ARCHIVES_ZIP_TYPES = [
	"application/zip", 
	"application/x-zip", 
	"application/x-zip-compressed", 
	"application/x-compressed"
];

var IMG_DATA_TYPE = "data:image/png;base64,";				// informations préfixant l'attribut "src" des balises html "img" - permet l'affichage d'une image à partir de son encodage en base64
var DIMENSION_LOGO_ESPACE = 400;							// Dimensions en px (longueur et largeur) de l'image correspondant au logo de l'espace
var DIMENSION_LOGO_RESSOURCE = 100;							// Dimensions en px (longueur et largeur) de l'image correspondant au logo de la ressource
var DIMENSION_LOGO_BADGE = 100;								// Dimensions en px (longueur et largeur) de l'image correspondant au logo d'un badge
var POIDS_MAX_LOGO_BADGE = 256;								// poids max en ko de l'image correspondant au logo d'un badge

// liste des droits existants dans le Backoffice 
// Rq commentaires : cf. fichier webservices/lib/constantes.js

// Espace
var ADMIN_ESPACE = "admin_espace";
var EDITER_ESPACE = "editer_espace";
var GERER_GROUPES = "gerer_groupes";
var GERER_ROLES_ESPACE = "gerer_roles_espace";
var GERER_RESSOURCES_ET_CAPSULES = "gerer_ressources_et_capsules";
var GERER_TOUTES_RESSOURCES_ET_CAPSULES = "gerer_toutes_ressources_et_capsules";
var GERER_VISIBILITE_TOUTES_CAPSULES = "gerer_visibilite_toutes_capsules";
var DEPLACER_CAPSULE = "deplacer_capsule";
// Ressource
var EDITER_RESSOURCE = "editer_ressource";
var GERER_OPEN_BADGE = "gerer_open_badge";
var AFFICHER_STATISTIQUES = "afficher_statistiques";
var GERER_ROLES_RESSOURCE = "gerer_roles_ressource";
var SUPPRIMER_RESSOURCE = "supprimer_ressource";
var GERER_CAPSULE = "gerer_capsule";
var GERER_VISIBILITE = "gerer_visibilite";
var GERER_TOUTES_CAPSULES = "gerer_toutes_capsules";
var GERER_VISIBILITE_TOUTES_CAPSULES_RESSOURCE = "gerer_visibilite_toutes_capsules_ressource";
var SUPPRIMER_MESSAGES_CAPSULE = "supprimer_messages_capsule";
var SUPPRIMER_MESSAGES_TOUTES_CAPSULES = "supprimer_messages_toutes_capsules";

// Regex
var REGEX_EMAIL = /^[a-zA-Z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/;
var REGEX_NOM_DE_DOMAINE = /^[a-z0-9._-]+\.[a-z]{2,6}$/;

// Visibilités possibles d'une capsule pour un groupe
var VISIBILITE_ACCESSIBLE = 1;
var VISIBILITE_INACCESSIBLE = 2;

// Formats possibles pour une capsule
var FORMAT_PAIRFORM = 0;				// format par défaut d'une capsule
var FORMAT_SCENARI = 1;					// format Sceanri
var FORMAT_CHAINEDIT = 2;				// format ChainEdit
var FORMAT_WEB = 3;						// format Web (injecteur)
var FORMAT_STORYLINE2 = 4;        // format Storyline 2
var FORMAT_ETHERPAD = 5;        // format Etherpad
var FORMAT_STORYLINE3 = 6;				// format Storyline 3

// configuration des boutons pour ckeditor (le mode expert donne accès aux boutons permettant d'afficher le code source et d'incorporer des iframes)
// le reste de la config ckeditor se trouve dans public/lib/ckeditor/config.js
var CONFIG_BUTTONS_CKEDITOR_EXPERT = 'Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Redo,Undo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,HiddenField,RemoveFormat,CreateDiv,Language,BidiRtl,BidiLtr,Unlink,Anchor,Flash,PageBreak,FontSize,Font,Styles,BGColor,ShowBlocks,About,SpecialChar,HorizontalRule,ImageButton,CopyFormatting';
var CONFIG_BUTTONS_CKEDITOR = 'Source,Iframe,' + CONFIG_BUTTONS_CKEDITOR_EXPERT;



var backoffice_controlleurs = angular.module("backofficeControlleurs", []);

backoffice_controlleurs.config( [
    '$compileProvider','$sceDelegateProvider',
    function( $compileProvider, $sceDelegateProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
        $sceDelegateProvider.resourceUrlWhitelist([
          "self",
          URL_SERVEUR_ETHERPAD + "**"
        ]);
    }
]);

