"use strict";


/*
 * Controleurs de gestion des ressources
 */
 

// Page de création d'une ressource
backoffice_controlleurs.controller("controleurCreationRessource", ["$scope", "$http", "toaster", "$routeParams", "$location", "$translate", "$timeout", "connexion", function($scope, $http, toaster, $routeParams, $location, $translate, $timeout, connexion){
	var succes_enregistrement;
	var erreur_requete;
	$scope.formulaire_soumis = false;
	$scope.logo_selectionne = false;		// false tant qu'aucun logo respectant les contraintes (format, taille, etc.) n'a été associé à la ressource
	$scope.format_incorrect = false;		// false si le format du logo de la ressource n'est pas le bon
	$scope.dimensions_incorrectes = false;	// false si les dimensions du logo de la ressource ne sont pas les bonnes

	// traduction des labels de la page
	$translate('LABEL.RESSOURCE.CREER').then(function (traduction) {
		$scope.titre = traduction;
	});
	$translate('LABEL.CREER').then(function (traduction) {
		$scope.bouton_formulaire = traduction;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})

	$scope.id_espace = parseInt($routeParams.id_espace, 10);
	
	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	
	// récupération des infos de la page (la liste des themes, liste des langues) traduites dans la langue choisie
	$http.get(URL_SERVEUR_NODE +"webServices/infosCreationRessource/langue/" + PF.langue.codeFromId(3) +"/espace/"+ $routeParams.id_espace)
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.liste_themes = retour_json.liste_themes;
				$scope.liste_usages = retour_json.liste_usages;
				$scope.liste_langues = retour_json.liste_langues;
			} else {
				toaster.pop("error", erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/infosCreationRessource/langue/:code_langue/espace/:id_espace");
			toaster.pop("error", erreur_requete);
		});

	// récupération des informations sur le champ "Logo"
	$scope.recupererLogo = function($files) {
		// si le fichier n'est pas une image dans le bon format
		if (["image/png", "image/jpg", "image/jpeg", "image/tiff", "image/gif"].indexOf($files[0].type) == -1) {
			$scope.format_incorrect = true;
			$scope.logo_selectionne = false;
			$scope.dimensions_incorrectes = false;

		} else {
			var _URL = window.URL || window.webkitURL;
			var image = new Image();

			image.onload = function() {
				// si l'image du logo n'a pas les bonnes dimensions
				if (this.width < DIMENSION_LOGO_RESSOURCE || this.height < DIMENSION_LOGO_RESSOURCE) {
					// le timeout permet d'attendre que les valeurs soient chargées avant de les mettre à jour à l'écran, sinon, le chargement est trop rapide et les valeurs ne sont pas initalisées
					$timeout(function () {
						$scope.dimensions_incorrectes = true;
						$scope.logo_selectionne = false;
						$scope.format_incorrect = false;
					}, 500);

				} else {
					// on affiche les infos sur le logo
					$scope.logo_selectionne = true;
					$scope.format_incorrect = false;
					$scope.dimensions_incorrectes = false;

					// affichage du nom du logo sélectionné
					$scope.nom_logo = $files[0].name;

					// affichage du logo selectionné
					var file_reader = new FileReader();

					file_reader.onload = function (e) {
						// le timeout permet d'attendre que l'image soit chargée avant de la mettre à jour à l'écran, sinon, le chargement est trop rapide et l'image n'est pas initalisée
						$timeout(function () {
							$scope.src_logo = e.target.result;
						}, 500);
					};

					file_reader.readAsDataURL($files[0]);
				}
			}

			// transformation du fichier en image, la fonction onload sera appelé à la fin
			image.src = _URL.createObjectURL($files[0]);			
		}
	}

	$scope.gererRessource = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			var logo;
			
			// si un logo a été sélectionné
			if($scope.logo_selectionne) {
				logo = $scope.src_logo.substring(IMG_DATA_TYPE.length)		// on envoie l'image encodé en base64, seul le code base64 de l'image est conservé
			}
			
			// on ajoute la ressource en BDD
			$http.put(URL_SERVEUR_NODE + "webServices/ressource", {
					nom_long: $scope.nom_long,
					id_espace: $scope.id_espace,
					description: $scope.description,
					id_theme: $scope.theme_selectionne.id,
					id_usage: $scope.usage_selectionne.id_usage,
					id_langue: $scope.langue_selectionne.id,
					logo: logo
				})
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						if (!connexion.getUtilisateurConnecte().est_admin_pairform)
							connexion.getUtilisateurConnecte().liste_roles[$scope.id_espace][retour_json.id_ressource] = retour_json.role_auteur;

						$location.path(URL_BACKOFFICE).search({
							id_espace: $scope.id_espace,
							id_ressource: retour_json.id_ressource
						});
						toaster.pop("success", succes_enregistrement);
					} else {
						// SI le nom_ourt est identique à celui d'une ressource existante
						if (retour_json.message === ERREUR_DOUBLON) {
							// on indique la précesence du doublon
							$scope.ressource_doublon = true;
							$scope.gestion_ressource_form.nom_long.$invalid = false;
						} else {
							toaster.pop("error", erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http put /webServices/ressource");
					toaster.pop("error", erreur_requete);
				});
		}
	}
}]);


// Page d'édition d'une ressource
backoffice_controlleurs.controller("controleurGestionRessource", ["$scope", "$http", "toaster", "$routeParams", "$location", "$translate", "$timeout", function($scope, $http, toaster, $routeParams, $location, $translate, $timeout){
	var succes_mise_a_jour;
	var erreur_requete;
	$scope.formulaire_soumis = false;
	$scope.logo_selectionne = false;		// false tant qu'aucun logo respectant les contraintes (format, taille, etc.) n'a été associé à la ressource
	$scope.format_incorrect = false;		// false si le format du logo de la ressource n'est pas le bon
	$scope.dimensions_incorrectes = false;	// false si les dimensions du logo de la ressource ne sont pas les bonnes

	// traduction des labels de la page
	$translate('LABEL.RESSOURCE.EDITER').then(function (traduction) {
		$scope.titre = traduction;
	});
	$translate('LABEL.ENREGISTRER').then(function (traduction) {
		$scope.bouton_formulaire = traduction;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.MISE_A_JOUR').then(function (traduction) {
		succes_mise_a_jour = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;

	// récupération des informations sur la ressource à éditer (les themes sont récupérés dans la langue de l'interface)
	$http.get(URL_SERVEUR_NODE +"webServices/ressource/"+ $routeParams.id_ressource +"/"+ $routeParams.cree_par +"/langue/"+ PF.langue.codeFromId(3) +"/espace/"+ $routeParams.id_espace)
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.id_ressource = retour_json.ressource.id_ressource;
				$scope.nom_long = retour_json.ressource.nom_long;
				$scope.id_espace = retour_json.ressource.id_espace;
				$scope.description = retour_json.ressource.description;
				$scope.pseudo_createur = retour_json.ressource.pseudo_createur;
				$scope.url_logo = retour_json.ressource.url_logo;

				$scope.liste_themes = retour_json.liste_themes;
				$scope.liste_usages = retour_json.liste_usages;
				$scope.liste_langues = retour_json.liste_langues;

				// affichage du thème, de l'usage et de la langue de la ressource
				for (var id_theme in retour_json.liste_themes) {
					if (retour_json.liste_themes[id_theme].id == retour_json.ressource.id_theme) {
						$scope.theme_selectionne = retour_json.liste_themes[id_theme];
						break;
					}
				}
				for (var id_usage in retour_json.liste_usages) {
					if (retour_json.liste_usages[id_usage].id_usage == retour_json.ressource.id_usage) {
						$scope.usage_selectionne = retour_json.liste_usages[id_usage];
						break;
					}
				}
				for (var id_langue in retour_json.liste_langues) {
					if (retour_json.liste_langues[id_langue].id == retour_json.ressource.id_langue) {
						$scope.langue_selectionne = retour_json.liste_langues[id_langue];
						break;
					}
				}
			} else {
				toaster.pop("error", erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/ressource/:id_ressource/:cree_par/langue/:code_langue/espace/:id_espace");
			toaster.pop("error", erreur_requete);
		});

	// récupération des informations sur le champ "Logo"
	$scope.recupererLogo = function($files) {
		// si le fichier n'est pas une image dans le bon format
		if (["image/png", "image/jpg", "image/jpeg", "image/tiff", "image/gif"].indexOf($files[0].type) == -1) {
			$scope.format_incorrect = true;
			$scope.logo_selectionne = false;
			$scope.dimensions_incorrectes = false;

		} else {
			var _URL = window.URL || window.webkitURL;
			var image = new Image();

			image.onload = function() {
				// si l'image du logo n'a pas les bonnes dimensions
				if (this.width < DIMENSION_LOGO_RESSOURCE || this.height < DIMENSION_LOGO_RESSOURCE) {
					// le timeout permet d'attendre que les valeurs soient chargées avant de les mettre à jour à l'écran, sinon, le chargement est trop rapide et les valeurs ne sont pas initalisées
					$timeout(function () {
						$scope.dimensions_incorrectes = true;
						$scope.logo_selectionne = false;
						$scope.format_incorrect = false;
					}, 500);

				} else {
					// on affiche les infos sur le logo
					$scope.logo_selectionne = true;
					$scope.format_incorrect = false;
					$scope.dimensions_incorrectes = false;

					// affichage du nom du logo sélectionné
					$scope.nom_logo = $files[0].name;

					// affichage du logo selectionné
					var file_reader = new FileReader();

					file_reader.onload = function (e) {
						// le timeout permet d'attendre que l'image soit chargée avant de la mettre à jour à l'écran, sinon, le chargement est trop rapide et l'image n'est pas initalisée
						$timeout(function () {
							$scope.src_logo = e.target.result;
						}, 500);
					};

					file_reader.readAsDataURL($files[0]);
				}
			}

			// transformation du fichier en image, la fonction onload sera appelé à la fin
			image.src = _URL.createObjectURL($files[0]);			
		}
	}

	$scope.gererRessource = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			var logo;

			// si un logo a été sélectionné
			if($scope.logo_selectionne) {
				logo = $scope.src_logo.substring(IMG_DATA_TYPE.length)		// on envoie l'image encodé en base64, seul le code base64 de l'image est conservé
			}
			
			// on modifie la ressource en BDD
			$http.post(URL_SERVEUR_NODE +"webServices/ressource", {
					id_ressource: $scope.id_ressource,
					nom_long: $scope.nom_long,
					id_espace: $scope.id_espace,
					description: $scope.description,
					id_theme: $scope.theme_selectionne.id,
					id_langue: $scope.langue_selectionne.id,
					cree_par: $routeParams.cree_par,
					logo: logo
				})
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						$location.path(URL_BACKOFFICE).search({
							id_espace: $scope.id_espace,
							id_ressource: $routeParams.id_ressource
						});
						toaster.pop("success", succes_mise_a_jour);
					} else {
						// SI le nom_ourt est identique à celui d'une ressource existante
						if (retour_json.message === ERREUR_DOUBLON) {
							// on indique la précesence du doublon
							$scope.ressource_doublon = true;
							$scope.gestion_ressource_form.nom_long.$invalid = false;
						} else {
							toaster.pop("error", erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http post /webServices/ressource");
					toaster.pop("error", erreur_requete);
				});
		}
	}
}]);