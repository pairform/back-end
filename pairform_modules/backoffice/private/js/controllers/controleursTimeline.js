
pf_app.controller('backoffice_ressource', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
	var defaults = {
	};
	angular.extend($scope, defaults);

	$scope.init = function () {

	}

}]);

pf_app.controller('backoffice_session_formation', ["$scope", "toaster", "$localStorage", "$http", "$timeout", function ($scope, toaster, $localStorage, $http, $timeout) {
	var defaults = {
		session_defaut : {
			id_session : 1,
			nom : "Session ",
			items : []
		},
		session_items : [
			{
				type : "item",
				sub_type : "presentiel",
				titre : "Session présentielle",
				icone : "fa-users",
				type_icone : "fa",
				description : "Exemple : de 9h à 14h, metro Courneuve, 24 rue d'Allonville.",
				url : "https://maps.google.fr"
			},
			{
				type : "item",
				sub_type : "video",
				titre : "Vidéo conférence",
				icone : "fa-video-camera",
				type_icone : "fa",
				description : "Exemple : rendez vous 15 minutes en avance.",
				url : "https://meet.jit.si/pairform"
			},
			{
				type : "item",
				sub_type : "quizz",
				titre : "Quizz",
				icone : "fa-check",
				type_icone : "fa",
				description : "Exemple : vous pouvez réessayer autant de fois que nécessaire.",
				url : "https://quizlet.com"
			},
			{
				type : "item",
				sub_type : "ressource",
				titre : "Ressource web",
				icone : "fa-globe",
				type_icone : "fa",
				description : "Exemple : mettez l'adresse web de votre choix.",
				url : "https://www.google.fr"
			}
		]
	};

	angular.extend($scope, defaults);

	$scope.init = function () {
		// récupération des groupes
		$http.get(URL_SERVEUR_NODE +"webServices/espace/"+ $scope.selection.espace.id_espace +"/listeGroupes")
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.liste_groupes = retour_json.liste_groupes;
				// ajout de listes fictives pour le groupe Sphère publique (fictives, car elles ne seront jamais affichées)
				// permet d'éviter des erreurs JS lors d'utilisation de "concat()" sur les listes
				$scope.liste_groupes[0].liste_membres_email = [];
				$scope.liste_groupes[0].liste_membres_nom_domaine = [];
			} else {
				console.log(retour_json);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/espace/:id_espace/listeGroupes");
			// toaster.pop("error", "");
		});
		
		$scope.initSession();
	}

	$scope.initSession = function () {
		//Pas d'initialisation de session s'il n'y a pas de ressource ou d'espace
		if (!$scope.selection.espace || !$scope.selection.ressource) 
			return;
		
		var _params = [
			$scope.selection.espace.id_espace,
			$scope.selection.ressource.id_ressource
		]
		//Créations du tableau session au cas où
		$scope.selection.ressource.sessions = $scope.selection.ressource.sessions || [];
		// récupération des timelines
		$http.get(URL_SERVEUR_NODE +"webServices/session/ressource/"+ _params.join("/"))
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.sessions = retour_json.data.sessions;
						
				if ($scope.selection.ressource) {
					$scope.selection.ressource.sessions = $scope.sessions.filter(function (session) {
						return session.id_ressource == $scope.selection.ressource.id_ressource;
					});
					$scope.$parent.session = $scope.selection.ressource.sessions[0];
				}
			} else {
				// toaster.pop("error", "");
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/session/ressource/:id_espace/:id_ressource");
			// toaster.pop("error", "");
		});

	}
	$scope.addItem = function (session, item, from_item) {
		var _item = new SessionItem(item);

		//On ajuste la date de l'item pour être juste avant l'élément survolé
		if (typeof from_item != "undefined") {
			var date = from_item.date_diffusion;
	      _item.date_diffusion = moment(date, "x", "fr").subtract(1, 'seconds').format("x");
		}
		//S'il y a déjà des items, et qu'on est en train de placer à la fin
		else if(session.items.length){
			//Tri des items, le plus loin en date en premier
			var _items_sorted = session.items.sort(function (item, item2) {
				return parseInt(item.date_diffusion) < parseInt(item2.date_diffusion)
			})
			//On met la date après le dernier item
	    _item.date_diffusion = moment(_items_sorted[0].date_diffusion, "x").add(1, 'seconds').format("x");
		}
    
    session.items.push(_item);
		// $scope.sortItems(session)

    $timeout(function () {
      $scope.displayItem(_item);  
    }, 400)


		var _post = angular.extend(_item, {
			id_session : session.id_session,
			id_espace : $scope.selection.espace.id_espace,
			id_ressource : $scope.selection.ressource.id_ressource
		})

		$http.put(URL_SERVEUR_NODE +"webServices/session/item", _post)
		.success(function(retour_json){
			if (retour_json.status == RETOUR_JSON_OK) {
				_item.id_item = retour_json.data.id_item;
				console.log("Item enregistré.");
			}
		})
		.error(function(){
			console.log("erreur requete http put /webServices/session/item");
			toaster.pop("error", "");
		});	
	}

  function SessionItem (item) {
    return {
      id_item : moment().unix(), //Id temporaire
      id_capsule : item.id_capsule, //Id du document s'il y a, pour gestion visibilité future
      type : item.type || "document",
      sub_type : item.sub_type || item.id_format,
      titre : item.titre || item.nom_long || "Item",
      icone : item.icone || PF.globals.url.root + "public/img/icone/" + $scope.formats_noms_icones[item.id_format],
      type_icone : item.icone && item.icone.indexOf("fa-") >= 0 ? "fa" : "img",
      description : item.description || "",
      date_diffusion : moment().format("x"),
      url : item.url || item.url_web,
      diffusion_automatique : true
    }
  }

	$scope.displayItem = function (item, $event) {
		if($scope.$parent.display_item == item){
			$scope.$parent.display_item = false;
		}
		else{
			var html_item;

			if ($event) {
				html_item = $event.target.parentElement;
			}
			else {
				//Récupération de l'index de l'item en cours de display, après tri du tableau d'items
				var index = $scope.session.items.sort(function (item, item2) {
					//Tri descendant
					return parseInt(item.date_diffusion) > parseInt(item2.date_diffusion)
				}).findIndex(function (_item) {
					return _item.id_item == item.id_item;
				})
				//récupération de l'item
				html_item = document.querySelectorAll(".timeline-item")[index];
			}

			//Récupération de la position de l'élément
			var rect = html_item.getBoundingClientRect(),
	 					html_item_more = html_item.querySelector(".timeline-more");
	 		//Positionnement à droite
 			html_item_more.style.left = rect.x + rect.width + window.scrollX + "px";
 			html_item_more.style.top = rect.y + window.scrollY + "px";
 			//Affichage du panel
	 		$scope.$parent.display_item = item;
		}
	}
	$scope.saveItem = function (item) {
		$scope.displayItem(item);
		//PUT ws

		var _params = angular.extend(item, {
			id_session : $scope.session.id_session,
			id_espace : $scope.selection.espace.id_espace,
			id_ressource : $scope.selection.ressource.id_ressource
		})


		$http.post(URL_SERVEUR_NODE +"webServices/session/item", _params)
		.success(function(retour_json){
			if (retour_json.status == RETOUR_JSON_OK) {
				console.log("Item enregistré.");
			}
		})
		.error(function(){
			console.log("erreur requete http put /webServices/session");
			toaster.pop("error", "Votre item n'a pas été enregistré, merci de réessayer.");
		});

	}
	$scope.sortItems = function (session) {
		var _session = session || $scope.session
		// return $scope.moment(item.date_diffusion, "x", "fr").format("x")
		_session.items = _session.items.sort(function (item, item2) {
			return parseInt(item.date_diffusion) > parseInt(item2.date_diffusion)
		})
	}

	$scope.deleteItem = function (item) {
		var _index = $scope.session.items.findIndex(function(e){
			return e.id_item == item.id_item
		});
		$scope.session.items.splice(_index, 1);

		//Delete ws
		var _post = [
			$scope.selection.espace.id_espace,
			$scope.selection.ressource.id_ressource,
			item.id_item
		]

		$http.delete(URL_SERVEUR_NODE +"webServices/session/item/" + _post.join("/"))
		.success(function(retour_json){
			if (retour_json.status == RETOUR_JSON_OK) {
				console.log("Session supprimée.");
			}
			else
				$scope.session.items.splice(_index, 1, item);

		})
		.error(function(){
			console.log("erreur requete http delete /webServices/session");
			toaster.pop("error", "");
			$scope.session.items.splice(_index, 1, item);
		});	
	}


	/* SESSIONS */


	$scope.creerSession = function (session) {
		//Créations du tableau session au cas où
		$scope.selection.ressource.sessions = $scope.selection.ressource.sessions || [];

		var _session = angular.copy(session || $scope.session_defaut);
		_session.id_session = moment().unix();
		_session.nom += ($scope.selection.ressource.sessions.length + 1);

		$scope.selection.ressource.sessions.push(_session);
		$scope.session = $scope.selection.ressource.sessions[$scope.selection.ressource.sessions.length -1];
		$timeout(function () {
			$scope.editerSession($scope.session);
		}, 400)
		//PUT ws

		var _post = angular.extend(_session, {
			id_espace : $scope.selection.espace.id_espace,
			id_ressource : $scope.selection.ressource.id_ressource
		})

		$http.put(URL_SERVEUR_NODE +"webServices/session", _post)
		.success(function(retour_json){
			if (retour_json.status == RETOUR_JSON_OK) {
				$scope.session.id_session = retour_json.data.id_session;
				console.log("Session enregistrée.");
			}
		})
		.error(function(){
			console.log("erreur requete http put /webServices/session");
			toaster.pop("error", "");
		});	
	}

	$scope.editerSession = function (session, $event) {		
		if($scope.$parent.display_session == session){
			$scope.$parent.display_session = false;
		}
		else{
			var html_session;

			if ($event) {
				html_session = $event.target.closest(".session-tab");
			}
			else {
				//Récupération de l'index de session en cours de display
				var index = $scope.selection.ressource.sessions.findIndex(function (_session) {
					return _session.id_session == session.id_session;
				})
				//récupération de l'html session
				html_session = document.querySelectorAll(".session-tab")[index];
			}

			//Récupération de la position de l'élément
			var rect = html_session.getBoundingClientRect(),
	 					html_session_more = html_session.querySelector(".session-more");
	 		//Positionnement à droite
 			html_session_more.style.left = rect.x + rect.width + window.scrollX + "px";
 			html_session_more.style.top = rect.y + window.scrollY + "px";
 			//Affichage du panel
	 		$scope.$parent.display_session = session;
		}
	}

	$scope.dupliquerSession = function (session) {
		$scope.creerSession(session);
		//PUT items session
	}
	$scope.supprimerSession = function (session) {
		if(confirm("Êtes vous sûr•e de supprimer cette session ?")){
			var _index = $scope.selection.ressource.sessions.findIndex(function(e){
				return e.id_session == session.id_session
			});
			$scope.selection.ressource.sessions.splice(_index, 1);
			$scope.session = $scope.selection.ressource.sessions[0];
			//Delete ws
			var _post = [
				$scope.selection.espace.id_espace,
				$scope.selection.ressource.id_ressource,
				session.id_session
			]

			$http.delete(URL_SERVEUR_NODE +"webServices/session/" + _post.join("/"))
			.success(function(retour_json){
				if (retour_json.status == RETOUR_JSON_OK) {
					console.log("Session supprimée.");
				}
				else
					$scope.selection.ressource.sessions.splice(_index, 1, session);

			})
			.error(function(){
				console.log("erreur requete http delete /webServices/session");
				toaster.pop("error", "");
				$scope.selection.ressource.sessions.splice(_index, 1, session);
			});	
		}
	}
	$scope.saveSession = function (session) {
		$scope.editerSession(session);
		//PUT ws

		var _params = angular.extend(session, {
			id_espace : $scope.selection.espace.id_espace,
			id_ressource : $scope.selection.ressource.id_ressource
		})


		$http.post(URL_SERVEUR_NODE +"webServices/session", _params)
		.success(function(retour_json){
			if (retour_json.status == RETOUR_JSON_OK) {
				console.log("Session enregistrée.");
			}
		})
		.error(function(){
			console.log("erreur requete http put /webServices/session");
			toaster.pop("error", "Votre session n'a pas été enregistrée, merci de réessayer.");
		});
	}


	$scope.$watch("selection.espace", function (new_value, old_value) {
		if (new_value !== old_value && typeof new_value != "undefined"  && typeof new_value.id_espace != "undefined") {
			$scope.init();
		}
	})
	$scope.$watch("selection.ressource", function (new_value, old_value) {
		if (new_value !== old_value && typeof new_value != "undefined" && typeof new_value.id_ressource != "undefined" && typeof $scope.sessions != "undefined") {
			$scope.initSession();
		}
	})

	//Polyfill node.closest() 
	//https://developer.mozilla.org/fr/docs/Web/API/Element/closest
	if (!Element.prototype.matches)
    Element.prototype.matches = Element.prototype.msMatchesSelector || 
                                Element.prototype.webkitMatchesSelector;

	if (!Element.prototype.closest)
	    Element.prototype.closest = function(s) {
	        var el = this;
	        if (!document.documentElement.contains(el)) return null;
	        do {
	            if (el.matches(s)) return el;
	            el = el.parentElement || el.parentNode;
	        } while (el !== null); 
	        return null;
	    };
}]);