"use strict";


/*
 * Controleurs gérant la connexion
 */

// Page de connexion
backoffice_controlleurs.controller("controleurConnexion", ["$scope", "$http", "toaster", "$location", "$translate", "$rootScope", "connexion", function($scope, $http, toaster, $location, $translate, $rootScope, connexion) {
	var succes_identification, succes_reinitialisation_mot_de_passe;
	var erreur_requete, erreur_acces_utilisateur_non_autorise, erreur_email_inexistant, erreur_acces_email_utilisateur_non_valide;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.IDENTIFICATION').then(function (traduction) {
		succes_identification = traduction;
	});
	$translate('LABEL.SUCCES.REINITIALISATION_MOT_DE_PASSE').then(function (traduction) {
		succes_reinitialisation_mot_de_passe = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});
	$translate('LABEL.ERREUR.ACCES_UTILISATEUR_NON_AUTORISE').then(function (traduction) {
		erreur_acces_utilisateur_non_autorise = traduction;
	});
	$translate('LABEL.ERREUR.ACCES_EMAIL_UTILISATEUR_NON_VALIDE').then(function (traduction) {
		erreur_acces_email_utilisateur_non_valide = traduction;
	});
	$translate('LABEL.ERREUR.EMAIL_INEXISTANT').then(function (traduction) {
		erreur_email_inexistant = traduction;
	});

	$scope.connecterUtilisateur = function(formulaire_valide) {
		$scope.formulaire_soumis = true;
		$scope.utilisateur_non_connecte = false;
		// si tout les champs du formulaire sont valides
		if(formulaire_valide) {
			// On envoie une demande de connexion pour l'utilisateur pour récupérer ses informations (id, droits, etc.)
			$http.post(URL_SERVEUR_NODE +"webservices/utilisateur/login/backoffice", {
					os: OS_BACKOFFICE,
					version: VERSION_BACKOFFICE,
					username: $scope.login,
					password: window.btoa(encodeURIComponent( escape($scope.mot_de_passe) ))	// encodage base64 du mot de passe
				})
				.success(function(retour_json){
					console.log(retour_json);
					if (retour_json.status === RETOUR_JSON_OK) {
						// mise à jour de l'utilisateur connecté pour que ses infos soient accessible dans l'application
						connexion.setUtilisateurConnecte(retour_json.utilisateur_connecte);
						$location.path(URL_BACKOFFICE);
						
						toaster.pop("success", succes_identification);
						// broadcast d'un evenement pour mettre à jour l'affichage avec les infos sur l'utilisateur connecté (ex: le menu)
						$rootScope.$broadcast("connexion");
					} else {
						// Si le couple login / mot de passe ne correspond à aucuns utilisateurs
						if (retour_json.message === ERREUR_UTILISATEUR_NON_CONNECTE) {
							$scope.utilisateur_non_connecte = true;
							$scope.formulaire_soumis = false;
						}
						// sinon si l'utilisateur n'a pas encore validé son email d'inscription
						else if (retour_json.message === ERREUR_EMAIL_UTILISATEUR_NON_VALIDE) {
							$scope.utilisateur_non_connecte = true;
							$scope.formulaire_soumis = false;
							toaster.pop("error", erreur_acces_email_utilisateur_non_valide);
						}
						// sinon si l'utilisateur n'est pas autorisé à se connecter au Backoffice
						else if (retour_json.message === ERREUR_UTILISATEUR_NON_AUTORISE) {
							$scope.formulaire_soumis = false;
							toaster.pop("error", erreur_acces_utilisateur_non_autorise);
						}
						else {
							toaster.pop("error", erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http post /utilisateur/login/backoffice");
					toaster.pop("error", erreur_requete);
				});
		}
	}

	$scope.reinitialiserMotDePasse = function(email_valide) {
		// si tout les champs du formulaire sont valides
		if(email_valide) {
			// On envoie une demande de reinitialisation du mot de passe de l'utilisateur
			$http.post(URL_SERVEUR_NODE +"webservices/utilisateur/reset", {
					os: OS_BACKOFFICE,
					version: VERSION_BACKOFFICE,
					email: $scope.email
				})
				.success(function(retour_json){
					console.log(retour_json);
					if (retour_json.status === RETOUR_JSON_OK) {						
						toaster.pop("success", succes_reinitialisation_mot_de_passe);
					} else {
						if (retour_json.message === ERREUR_UTILISATEUR_NON_CONNECTE) {
							toaster.pop("error", erreur_email_inexistant);
						}
						else {
							toaster.pop("error", erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http post /utilisateur/reset");
					toaster.pop("error", erreur_requete);
				});
		}
	}
}]);
