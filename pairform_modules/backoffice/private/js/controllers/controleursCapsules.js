"use strict";


/*
 * Controleurs de gestion des capsules
 */

// Page de création d'une capsule
backoffice_controlleurs.controller("controleurCreationCapsule", ["$scope", "$http", "toaster", "$routeParams", "$location", "$translate", "$timeout", "Upload", "connexion", function($scope, $http, toaster, $routeParams, $location, $translate, $timeout, Upload, connexion){
	var succes_enregistrement, erreur_requete, attention_recuperer_clef_pairform;
	var parametres_requete;
	$scope.metadonnees_affichees = false;
	$scope.formats_noms_icones = [
		"doc_pf.png",
		"doc_scenari.png",
		"doc_chainedit.png",
		"doc_injecteur.png",
		"doc_storyline2.png",
		"doc_etherpad.png",
		"doc_storyline3.png"
	];

	$scope.formulaire_soumis = false;
	$scope.url_root = PF.globals.url.root;
	
	// traduction des labels de la page
	$translate('LABEL.CAPSULE.CREER').then(function (traduction) {
		$scope.titre = traduction;
	});
	$translate('LABEL.CREER').then(function (traduction) {
		$scope.bouton_formulaire = traduction;
	});	

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})
	$translate('LABEL.CAPSULE.GESTION.RECUPERER_CLEF_PAIRFORM').then(function (traduction) {
		attention_recuperer_clef_pairform = traduction;
	})

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;

	$scope.capsule = {};
	$scope.capsule.id_ressource = parseInt($routeParams.id_ressource, 10);

	// récupération de la liste des formats de capsule
	$http.get(URL_SERVEUR_NODE +"webServices/listeFormats")
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.liste_formats = retour_json.liste_formats;
				$scope.format_selectionne = $scope.liste_formats[FORMAT_PAIRFORM];
			} else {
				toaster.pop("error", erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/listeFormats");
			toaster.pop("error", erreur_requete);
		});

	$scope.gererCapsule = function(formulaireValide) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			// si le champs "auteurs" n'a pas été rempli, on le remplit avec le pseudo du créateur du document
			if(!$scope.capsule.auteurs)
				$scope.capsule.auteurs = connexion.getUtilisateurConnecte().pseudo;

			parametres_requete = $scope.capsule;
			parametres_requete.id_espace = $routeParams.id_espace;
			parametres_requete.ressource_cree_par = $routeParams.ressource_cree_par;
			parametres_requete.nom_ressource = $routeParams.nom_ressource;
			parametres_requete.id_format = $scope.format_selectionne.id_format;
			
			// on ajoute la capsule en BDD
			$http.put(URL_SERVEUR_NODE + "webServices/capsule", parametres_requete)
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						// redirection vers l'écran d'édition de la capsule
						redirigerUtilisateur(retour_json.id_capsule);

						// si la capsule est au format Scenari ou ChainEdit
						if ($scope.format_selectionne.id_format == FORMAT_SCENARI || $scope.format_selectionne.id_format == FORMAT_CHAINEDIT) 
							toaster.pop("info", attention_recuperer_clef_pairform +" : "+ retour_json.clef_pairform);						
					} else {
						// SI le nom_court est identique à celui d'une capsule existante
						if (retour_json.message === ERREUR_DOUBLON) {
							// on indique la précesence du doublon
							$scope.capsule_doublon = true;
						} else if (retour_json.message === ERREUR_UTILISATEUR_NON_AUTORISE) {
							$translate('LABEL.ERREUR.ACCES_UTILISATEUR_NON_AUTORISE').then(function (traduction) {
								toaster.pop("error", traduction);
							});
						} else {
							toaster.pop("error", erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http put /webServices/capsule");
					toaster.pop("error", erreur_requete);
				});
		}
	}

	// redirection vers l'écran approprié quand l'enregistrement du formulaire est réussi
	function redirigerUtilisateur(id_capsule) {
		$location.path(URL_BACKOFFICE + "/capsules/gestionCapsule/"+ id_capsule).search({
			id_espace: $routeParams.id_espace,
			nom_espace: $routeParams.nom_espace,
			id_ressource: $routeParams.id_ressource,
			nom_ressource: $routeParams.nom_ressource,
			id_capsule: id_capsule,
			nom_capsule: $scope.capsule.nom_long,
			cree_par: connexion.getUtilisateurConnecte().id_utilisateur,
			phase_creation: true
		});
		toaster.pop("success", succes_enregistrement);
	}
}]);


// Page d'édition d'une capsule
// Si l'utilisateur est pas en phase de creation d'un nouveau document
// 		phase_creation = true, l'utilisateur vient de créer son document
//		l'écran de creation redirige vers l'écran d'édition qui utilise les mêmes fichiers .jade / .js, les variables restent, attention, cela peut des erreurs javascript (mais sans conséquence)
backoffice_controlleurs.controller("controleurGestionCapsule", ["$scope", "$http", "toaster", "$routeParams", "$location", "$translate", "$timeout", "Upload", "connexion", "$sce", function($scope, $http, toaster, $routeParams, $location, $translate, $timeout, Upload, connexion, $sce){
	var succes_mise_a_jour, erreur_requete, titre_page_par_defaut, supprimer_page, config_boutons_ckeditor;
	var parametres_requete, archive_mobile, archive_web, url_web_capsule;
	var liste_contenu_pages = {}, liste_pages_a_supprimer = [], date = new Date();
	$scope.sommaire_est_modifie = false;
	$scope.page_est_modifiee = false;
	$scope.formulaire_soumis = false;
	$scope.upload_archive_mobile = true;
	$scope.upload_archive_web = true;
	$scope.archive_mobile_selectionne = false;	// false tant qu'aucune archive mobile respectant les contraintes (format, etc.) n'a été associé à la capsule
	$scope.archive_web_selectionne = false;		// false tant qu'aucune archive web respectant les contraintes (format, etc.) n'a été associé à la capsule
	$scope.format_mobile_incorrect = false;		// false si le format de l'archive mobile de la ressource n'est pas le bon
	$scope.format_web_incorrect = false;		// false si le format de l'archive web de la ressource n'est pas le bon

	$scope.url_root = PF.globals.url.root;
	//Autorisation
	$scope.url_etherpad = URL_SERVEUR_ETHERPAD;
	$scope.url_etherpad_export = URL_SERVEUR_ETHERPAD + $routeParams.id_capsule + "/export/pure_html";

	$scope.utilisateur_local = connexion.getUtilisateurConnecte();

	// traduction des labels de la page
	$translate('LABEL.CAPSULE.EDITER').then(function (traduction) {
		$scope.titre = traduction;
	});

	$translate('LABEL.CAPSULE.GESTION.TITRE_PAGE_PAR_DEFAUT').then(function (traduction) {
		titre_page_par_defaut = traduction;

		// Pour le format PF : initialisation du sommaire, on créé la première page (qui est vide)
		// on utilise le timestamp pour que chaque page ait un id unique, la première page est sélectionnée par défaut
		$scope.sommaire = {
			liste_pages: [{
				id: date.getTime(),
				titre: titre_page_par_defaut,
				position: 0
			}]
		};
		$scope.page_editee = 0;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.MISE_A_JOUR').then(function (traduction) {
		succes_mise_a_jour = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})
	$translate('LABEL.CAPSULE.GESTION.SUPPRIMER_PAGE').then(function (traduction) {
		supprimer_page = traduction;
	});

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;
	$scope.arborescence.nom_capsule = $routeParams.nom_capsule;
	$scope.arborescence.id_capsule = $routeParams.id_capsule;

	// récupération des informations sur la capsule à éditer
	$http.get(URL_SERVEUR_NODE +"webServices/capsule/"+ $routeParams.id_capsule +"/"+ $routeParams.cree_par +"/ressource/"+ $routeParams.id_ressource +"/espace/"+ $routeParams.id_espace)
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.liste_formats = retour_json.liste_formats;
				$scope.capsule = retour_json.capsule;
				url_web_capsule = retour_json.capsule.url_web;

				// affichage du format de la capsule
				for (var id_format in retour_json.liste_formats) {
					if (retour_json.liste_formats[id_format].id_format == retour_json.capsule.id_format) {
						$scope.format_selectionne = retour_json.liste_formats[id_format];
						break;
					}
				}

				//Chargement des bonnes traductions
				if ($scope.capsule.id_format == FORMAT_ETHERPAD)
					$translate('LABEL.PUBLIER_QUITTER').then(function (traduction) {
						$scope.bouton_formulaire = traduction;
					});
				else
					$translate('LABEL.ENREGISTRER_QUITTER').then(function (traduction) {
						$scope.bouton_formulaire = traduction;
					});

				if($scope.capsule.id_format == FORMAT_PAIRFORM) {
					// on initialise ckeditor
					config_boutons_ckeditor = CONFIG_BUTTONS_CKEDITOR; 

					CKEDITOR.replace("ckeditor_capsule", {
						removeButtons : config_boutons_ckeditor,

						//Inclusion des CSS de boostrap et le css personalisé
						contentsCss : [URL_SERVEUR_NODE+'public/lib/css/bootstrap.min.css', URL_SERVEUR_NODE+'public/css/ckeditor_capsule.css', retour_json.capsule.url_web + 'css_personalise.css'],
						// CKEDITOR.config.contentsCss = '/public/lib/css/bootstrap.min.css';

						//contentsCss : URL_SERVEUR_NODE +"public/css/ckeditor_capsule.css",		// on peut aussi faire ["a.css", "b.css"]
						on: {
							//Wrap du contenu dans une balise main s'il n'y en a pas (quand l'utilisateur ne fait pas d'import de document)
							getData: function( evt ) {
								if (!evt.data.dataValue.match(/\<main\>/i))
									evt.data.dataValue = "<main>" + evt.data.dataValue + "</main>";
							},
							focus: function( evt ) {
								$scope.page_est_modifiee = true;
							}
						}
					});

					// si du contenue a déjà été créé
					if (retour_json.capsule.sommaire) {
						$scope.sommaire = retour_json.capsule.sommaire;

						// récupération du contenu de la première page (à "injecter" dans ckeditor) de la capsule
						$http.get(URL_SERVEUR_NODE +"webServices/contenu_ckeditor/capsule/"+ $routeParams.id_capsule +"/"+ $scope.capsule.cree_par +"/ressource/"+ $routeParams.id_ressource +"/espace/"+ $routeParams.id_espace +"/page/"+ $scope.sommaire.liste_pages[0].id)
							.success(function(retour){
								CKEDITOR.instances.ckeditor_capsule.setData(retour, {
									callback: function() {
										CKEDITOR.instances.ckeditor_capsule.setData(retour);
									}
								});
							})
							.error(function(){
								console.log("erreur requete http get /webServices/contenu_ckeditor/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace/page/:id_page_contenue_capsule");
								toaster.pop("error", erreur_requete);
							});
					} else {
						// sinon, on initialise le sommaire
						// Pour le format PF : initialisation du sommaire, on créé la première page (qui est vide)
						// on utilise le timestamp pour que chaque page ait un id unique
						$scope.sommaire = {
							liste_pages: [{
								id: date.getTime(),
								titre: titre_page_par_defaut,
								position: 0
							}]
						};
						$scope.sommaire_est_modifie = true;
						$scope.page_est_modifiee = true;
					}
					// on sélectionne par défaut la première page	
					$scope.page_editee = 0;			

					//Récupération du css personnalisé
					$scope.css_personalise = retour_json.capsule.css_personalise;

				} else if($scope.capsule.id_format == FORMAT_WEB) {
					$scope.upload_archive_web = false;
				}
				else if ($scope.capsule.id_format == FORMAT_ETHERPAD) {
					//debugger
					if (retour_json.capsule.sommaire) {
						$scope.sommaire = retour_json.capsule.sommaire;
					}
				}
			} else {
				toaster.pop("error", erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace");
			toaster.pop("error", erreur_requete);
		});

	// spécifie que le titre d'une page a été modifié
	$scope.editerTitrePage = function() {
		$scope.sommaire_est_modifie = true;
		$scope.page_est_modifiee = true; 
	}

	// ajout d'une nouvelle page dans le document
	$scope.ajouterPage = function() {
		date = new Date();
		$scope.sommaire_est_modifie = true;
		$scope.page_est_modifiee = true; 

		// on sauvegarde le contenu de la page en cours d'édition
		nettoyerHTML();
		liste_contenu_pages[$scope.sommaire.liste_pages[$scope.page_editee].id] = CKEDITOR.instances.ckeditor_capsule.getData();	// on utilise l'id de la page comme clef

		// ajout de la page dans le sommaire, on utilise le timestamp pour que chaque page est un id unique
		$scope.sommaire.liste_pages.push({
			id: date.getTime(),
			titre: titre_page_par_defaut,
			position: $scope.sommaire.liste_pages.length
		});

		// on vide ckeditor, la nouvelle page est une "page blanche"
		CKEDITOR.instances.ckeditor_capsule.setData(null, {
			callback: null
		});

		// la nouvelle page devient la page en cours d'édition 
		$scope.page_editee = $scope.sommaire.liste_pages.length - 1;
	}

	// affichage du contenu de la page séléctionnée par l'utilisateur pour modification (du titre ou du contenu)
	$scope.editerPage = function(position_page) {
		// on sauvegarde le contenu de la page en cours d'édition
		nettoyerHTML();
		liste_contenu_pages[$scope.sommaire.liste_pages[$scope.page_editee].id] = CKEDITOR.instances.ckeditor_capsule.getData();	// on utilise l'id de la page comme clef

		// récupération du contenu de la page sélectionnée
		var contenu_page_a_editer = liste_contenu_pages[$scope.sommaire.liste_pages[position_page].id];

		// si la page est effectivement stocké dans le session storage
		if (contenu_page_a_editer) {
			// mise à jour de ckeditor avec le contenu de la page sélectionné 
			CKEDITOR.instances.ckeditor_capsule.setData(contenu_page_a_editer, {
				callback: null
			});
		}
		else {
			// sinon, récupération du contenu de la page (à "injecter" dans ckeditor)
			$http.get(URL_SERVEUR_NODE +"webServices/contenu_ckeditor/capsule/"+ $routeParams.id_capsule +"/"+ $scope.capsule.cree_par +"/ressource/"+ $routeParams.id_ressource +"/espace/"+ $routeParams.id_espace +"/page/"+ $scope.sommaire.liste_pages[position_page].id)
				.success(function(retour){
					CKEDITOR.instances.ckeditor_capsule.setData(retour, {
						callback: function() {
							CKEDITOR.instances.ckeditor_capsule.setData(retour);
						}
					});
				})
				.error(function(){
					console.log("erreur requete http get /webServices/contenu_ckeditor/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace/page/:id_page_contenue_capsule");
					toaster.pop("error", erreur_requete);
				});
		}

		// la page sélectionné devient la page en cours d'édition 
		$scope.page_editee = position_page;
	}

	// suppression d'une page du document
	$scope.supprimerPage = function(position_page_a_supprimer) {
		// popup de confirmation de l'action
		if( confirm(supprimer_page +" "+ $scope.sommaire.liste_pages[position_page_a_supprimer].titre +" ?") ) {
			$scope.sommaire_est_modifie = true;

			// ajout de la page dans la liste des fichiers à supprimer
			liste_pages_a_supprimer.push( $scope.sommaire.liste_pages[position_page_a_supprimer].id );

			// suppression du contenu de la page à supprimer
			delete liste_contenu_pages[ $scope.sommaire.liste_pages[position_page_a_supprimer].id ];

			// suppression de la page dans le sommaire
			$scope.sommaire.liste_pages.splice(position_page_a_supprimer, 1);

			// si la page à supprimer était la dernière du document
			if($scope.sommaire.liste_pages.length == 0) {
				$scope.ajouterPage();
			}
			else {
				// dans le sommaire, on décale la position de chaque page située après la page supprimée
				for (var i = position_page_a_supprimer; i < $scope.sommaire.liste_pages.length; i++) {
					$scope.sommaire.liste_pages[i].position--;
				};

				// mise à jour de ckeditor avec le contenu de la 1ère page (après suppression d'une page, on sélectionne par défaut la 1ère page)
				var contenu_page_a_editer = liste_contenu_pages[$scope.sommaire.liste_pages[0].id];

				CKEDITOR.instances.ckeditor_capsule.setData(contenu_page_a_editer, {
					callback: null
				});
				$scope.page_editee = 0;
			}
		}
	}

	// si l'utilisateur active la checkbox "fonctionnalités avancées", il a accès aux boutons experts (voir le code source, incorporer des iframes...)	
	$scope.changerConfig = function() {
		if (config_boutons_ckeditor == CONFIG_BUTTONS_CKEDITOR) {
			config_boutons_ckeditor = CONFIG_BUTTONS_CKEDITOR_EXPERT;
		} else {
			config_boutons_ckeditor = CONFIG_BUTTONS_CKEDITOR;
		}

		// récupération du contenu de la page en cours d'édition
		var contenu_page_a_editer = CKEDITOR.instances.ckeditor_capsule.getData();

		CKEDITOR.instances.ckeditor_capsule.destroy();

		// 
		CKEDITOR.replace("ckeditor_capsule", {
			removeButtons : config_boutons_ckeditor,
			//contentsCss : URL_SERVEUR_NODE +"public/css/ckeditor_capsule.css",		// on peut aussi faire ["a.css", "b.css"]
			on: {
				//Wrap du contenu dans une balise main s'il n'y en a pas (quand l'utilisateur ne fait pas d'import de document)
				getData: function( evt ) {
					if (!evt.data.dataValue.match(/\<main\>/i))
						evt.data.dataValue = "<main>" + evt.data.dataValue + "</main>";
				},
				focus: function( evt ) {
					$scope.page_est_modifiee = true;
				}
			}
		});

		CKEDITOR.instances.ckeditor_capsule.setData(contenu_page_a_editer, {
			callback: null
		});
	}


	// récupération de l'archive de la capsule (version mobile)
	$scope.recupererArchiveMobile = function($file) {
		if($file) {
			// si le fichier n'est pas dans le format zip
			if ( ARCHIVES_ZIP_TYPES.indexOf($file.type) < 0 ) {
				$scope.format_mobile_incorrect = true;
				$scope.archive_mobile_selectionne = false;
			} else {
				$scope.format_mobile_incorrect = false;
				$scope.archive_mobile_selectionne = true;
				$scope.nom_archive_mobile = $file.name;
				$scope.capsule.poid = Math.round($file.size / 1024 / 1024);	 // poid en Mo arrondi à un entier
				archive_mobile = $file;
			}
		}
	}

	// récupération de l'archive de la capsule (version web)
	$scope.recupererArchiveWeb = function($file) {
		if($file) {
			// si le fichier n'est pas dans le format zip
			if ( ARCHIVES_ZIP_TYPES.indexOf($file.type) < 0 ) {
				$scope.format_web_incorrect = true;
				$scope.archive_web_selectionne = false;
			} else {
				$scope.format_web_incorrect = false;
				$scope.archive_web_selectionne = true;
				$scope.nom_archive_web = $file.name;
				archive_web = $file;
			}
		}
	}

	// récupération d'un fichier (.doc, .odt...) pour conversion en html
	$scope.convertirFichier = function($file) {
		if($file) {
			$scope.page_est_modifiee = true;
			parametres_requete = $scope.capsule;
			parametres_requete.id_espace = $routeParams.id_espace;
			//convertirFichierToHTML(Upload, document, parametres_requete, avancer_upload, erreur_requete, toaster, callback)
			convertirFichierToHTML(Upload, $file, parametres_requete, function(avancement) {
				$scope.avancement_conversion = avancement
			}, 
			function (error) {
				$scope.avancement_conversion = null;
				console.log(error);
			}, 
			toaster,
			function(data) {
				$scope.avancement_conversion = null;				
				var nouveau_contenu = CKEDITOR.instances.ckeditor_capsule.getData() + data;

				CKEDITOR.instances.ckeditor_capsule.setData(nouveau_contenu, {
					callback: nettoyerHTML
				});
			});
		}
	}

	$scope.gererCapsule = function(formulaireValide, rediriger_apercu) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide) {
			parametres_requete = $scope.capsule;
			parametres_requete.id_espace = $routeParams.id_espace;
			parametres_requete.nom_ressource = $routeParams.nom_ressource;
			
			// on modifie la capsule en BDD
			$http.post(URL_SERVEUR_NODE +"webServices/capsule", parametres_requete)
				.success(function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						// si le format de la capsule est ChainEdit ou Scenari ou Storyline
						if($scope.format_selectionne.id_format == FORMAT_SCENARI || $scope.format_selectionne.id_format == FORMAT_CHAINEDIT || $scope.format_selectionne.id_format == FORMAT_STORYLINE2 || $scope.format_selectionne.id_format == FORMAT_STORYLINE3) {
							// si une archive mobile a été séléctionné et que la checkbox demandant d'uploader l'archive est cochée
							if($scope.archive_mobile_selectionne && $scope.upload_archive_mobile) {
								uploaderArchiveCapsule(Upload, archive_mobile, "mob", parametres_requete, function(avancement) {$scope.avancement_upload_mobile = avancement}, erreur_requete, toaster, function() {
									// si une archive web a été séléctionné et que la checkbox demandant d'uploader l'archive est cochée
									if($scope.archive_web_selectionne && $scope.upload_archive_web) {
										uploaderArchiveCapsule(Upload, archive_web, "web", parametres_requete, function(avancement) {$scope.avancement_upload_web = avancement}, erreur_requete, toaster, function() {
											redirigerUtilisateur(retour_json, connexion.getUtilisateurConnecte(), rediriger_apercu);
										});
									} else {
										redirigerUtilisateur(retour_json, connexion.getUtilisateurConnecte(), rediriger_apercu);
									}
								});
							}
							// si une archive web a été séléctionné et que la checkbox demandant d'uploader l'archive est cochée
							else if($scope.archive_web_selectionne && $scope.upload_archive_web) {
								uploaderArchiveCapsule(Upload, archive_web, "web", parametres_requete, function(avancement) {$scope.avancement_upload_web = avancement}, erreur_requete, toaster, function() {
									redirigerUtilisateur(retour_json, connexion.getUtilisateurConnecte(), rediriger_apercu);
								});
							} else {
								redirigerUtilisateur(retour_json, connexion.getUtilisateurConnecte(), rediriger_apercu);
							}

						} else if ($scope.format_selectionne.id_format == FORMAT_PAIRFORM) {		// sinon, si la capsule est au format pairform
							parametres_requete.sommaire = $scope.sommaire;
							parametres_requete.liste_pages_a_supprimer = liste_pages_a_supprimer;
							parametres_requete.sommaire_est_modifie = $scope.sommaire_est_modifie;
							parametres_requete.page_est_modifiee = $scope.page_est_modifiee;
							parametres_requete.css_est_modifie = $scope.gestion_capsule_form.css_personalise.$dirty;
							parametres_requete.css_personalise = $scope.css_personalise;

							// on sauvegarde le contenu de la page en cours d'édition
							nettoyerHTML();
							liste_contenu_pages[$scope.sommaire.liste_pages[$scope.page_editee].id] = CKEDITOR.instances.ckeditor_capsule.getData();

							uploaderCapsule(Upload, parametres_requete, liste_contenu_pages, function(avancement) {$scope.avancement_upload_ckeditor = avancement}, erreur_requete, toaster, function() {
								redirigerUtilisateur(retour_json, connexion.getUtilisateurConnecte(), rediriger_apercu);
							});
						} else if ($scope.format_selectionne.id_format == FORMAT_ETHERPAD) {		// sinon, si la capsule est au format pairform
							parametres_requete.sommaire = $scope.sommaire;
							parametres_requete.liste_pages_a_supprimer = [];
							parametres_requete.sommaire_est_modifie = true;
							parametres_requete.page_est_modifiee = true;
							parametres_requete.css_est_modifie = true;
							parametres_requete.css_personalise = true;

							$http.get($scope.url_etherpad_export)
							.success(function(html){
								//debugger
								uploaderEtherpad(Upload, parametres_requete, html, function(avancement) {$scope.avancement_upload_ckeditor = avancement}, erreur_requete, toaster, function() {
									redirigerUtilisateur(retour_json, connexion.getUtilisateurConnecte(), rediriger_apercu);
								});
							});

						} else {													// sinon, la capsule est au format web, redirection vers l'écran d'édition de la visibilité de la capsule
							redirigerUtilisateur(retour_json, connexion.getUtilisateurConnecte(), rediriger_apercu);
						}
					} else {
						// SI le nom_court est identique à celui d'une capsule existante
						if (retour_json.message === ERREUR_DOUBLON) {
							// on indique la précesence du doublon
							$scope.capsule_doublon = true;
							$scope.gestion_capsule_form.nom_long.$invalid = false;
						} else {
							toaster.pop("error", erreur_requete);
						}
					}
				})
				.error(function(){
					console.log("erreur requete http post /webServices/capsule");
					toaster.pop("error", erreur_requete);
				});
		}
	}


	// redirection vers l'écran approprié quand l'enregistrement du formulaire est réussi
	function redirigerUtilisateur(retour_json, utilisateur_connecte, rediriger_apercu) {
		/* Gestion de l'accès aux fonctionnalités en fonction des droits de l'utilisateur connecté
		 * ATTENTION : reporter les changements dans controleursTableauDeBord.js 
		 */
		 var aAccesGererVisibiliteCapsule = function () {
			return utilisateur_connecte.est_admin_pairform ||
				utilisateur_connecte.liste_roles[$routeParams.id_espace][GERER_VISIBILITE_TOUTES_CAPSULES] ||
				utilisateur_connecte.liste_roles[$routeParams.id_espace][$routeParams.id_ressource][GERER_VISIBILITE_TOUTES_CAPSULES_RESSOURCE] ||
				( utilisateur_connecte.liste_roles[$routeParams.id_espace][$routeParams.id_ressource][GERER_VISIBILITE] && $scope.capsule.cree_par == utilisateur_connecte.id_utilisateur );
		}
		if (typeof rediriger_apercu != "undefined") {
			//Si on ne souhaite pas être redirigé vers l'aperçu
			if (rediriger_apercu === true) {
				var preview = window.open(URL_SERVEUR_NODE + "doc/" + $routeParams.id_espace + "/" + $routeParams.id_ressource + "/" + $scope.capsule.id_capsule, $scope.capsule.id_capsule)
				preview.focus();
			}
		}
		// Si l'utilisateur est en phase de creation d'un nouveau document 
		// ET s'il est autorisé à modifier la visibilité des document
		else if($routeParams.phase_creation && aAccesGererVisibiliteCapsule()) {	// redirection vers l'écran de gestion de la visibilité de la capsule
			$location.path(URL_BACKOFFICE + "/capsules/gestionVisibiliteCapsule/"+ $scope.capsule.id_capsule).search({
				id_espace: $routeParams.id_espace,
				nom_espace: $routeParams.nom_espace,
				id_ressource: $routeParams.id_ressource,
				nom_ressource: $routeParams.nom_ressource,
				nom_capsule: $scope.capsule.nom_long,
				cree_par: $scope.capsule.cree_par,
			});
		} else {															// redirection vers le tableau de bord
			$location.path(URL_BACKOFFICE).search({
				id_espace: $routeParams.id_espace,
				id_ressource: $routeParams.id_ressource,
				id_capsule: $scope.capsule.id_capsule,
				ancienne_url_web_capsule: url_web_capsule					// en envoyant l'url web actuelle de la capsule, on pourra vérifier si cette url a été mise à jour (et si oui, prévenir l'utilisateur)
			});			
		}
		$scope.formulaire_soumis = false;
		toaster.pop("success", succes_mise_a_jour);
	}


	// upload l'archive mobile ou web d'une capsule
	function uploaderArchiveCapsule(Upload, archive, version_archive, parametres_requete, avancer_upload, erreur_requete, toaster, callback) {
		var datas = {
			url: URL_SERVEUR_NODE + "webServices/upload/capsule",
			method: "POST",
			data: parametres_requete
		};
		datas.data[version_archive] = archive;

		Upload.upload(datas).progress(function (evenement) {
			avancer_upload( parseInt(100 * evenement.loaded / evenement.total) );	// mise à jour au fur et à mesure de l'avancement de l'upload
		}).success(function (data, status, headers, config) {
			callback(data);
		}).error(function () {
			console.log("erreur requete http post /webServices/upload/capsule, version "+ version_archive);
			toaster.pop("error", erreur_requete);
		});
	}


	// upload le contenu d'une capsule au format PairForm 
	function uploaderCapsule(Upload, parametres_requete, liste_contenu_pages, avancer_upload, erreur_requete, toaster, callback) {
		var fichier_contenu, page;
		var datas = {
			url: URL_SERVEUR_NODE + "webServices/upload/capsule",
			method: "POST",
			data: parametres_requete
		};

		// on récupère toutes les pages de la capsules
		for(var i=0 ; i < parametres_requete.sommaire.liste_pages.length ; i++) {
			page = parametres_requete.sommaire.liste_pages[i];
			fichier_contenu = liste_contenu_pages[page.id];
		
			if (fichier_contenu)
				datas.data[page.id] = new Blob([fichier_contenu], {type: "text/html"});
		}

		// upload du contenu web (html) de la capsule
		Upload.upload(datas).progress(function (evenement) {
			avancer_upload( parseInt(100 * evenement.loaded / evenement.total) );	// mise à jour au fur et à mesure de l'avancement de l'upload
		}).success(function (data, status, headers, config) {
			callback();
		}).error(function () {
			console.log("erreur requete http post /webServices/upload/capsule, capsule au format pairform");
			toaster.pop("error", erreur_requete);
		});
	}



	// upload le contenu d'une capsule au format Etherpad 
	function uploaderEtherpad(Upload, parametres_requete, html, avancer_upload, erreur_requete, toaster, callback) {
		var fichier_contenu, page;
		

		var datas = {
			url: URL_SERVEUR_NODE + "webServices/upload/capsule",
			method: "POST",
			data: parametres_requete
		};

		datas.data[parametres_requete.sommaire.liste_pages[0].id.toString()] = new Blob([html], {type: "text/html"});;

		// upload du contenu web de la capsule
		Upload.upload(datas).progress(function (evenement) {
			avancer_upload( parseInt(100 * evenement.loaded / evenement.total) );	// mise à jour au fur et à mesure de l'avancement de l'upload
		}).success(function (data, status, headers, config) {
			callback();
		}).error(function () {
			console.log("erreur requete http post /webServices/upload/capsule, capsule au format pairform");
			toaster.pop("error", erreur_requete);
		});
	}




	// upload un document (.docx ...) pour conversion en HTML côté server
	function convertirFichierToHTML(Upload, document, parametres_requete, avancer_upload, erreur_requete, toaster, callback) {
		var datas = {
			url: URL_SERVEUR_NODE + "webServices/upload/conversion",
			method: "POST",
			data: parametres_requete
		};
		datas.data["doc"] = document;

		Upload.upload(datas).progress(function (evenement) {
			avancer_upload( parseInt(100 * evenement.loaded / evenement.total) );	// mise à jour au fur et à mesure de l'avancement de l'upload
		}).success(function (data, status, headers, config) {
			callback(data);
		}).error(function (error) {
			console.log("erreur requete http post /webServices/upload/conversion, version "+ version);
			toaster.pop("error", erreur_requete);
		});
	}


	// supprime les balises br, les élements vides et les blockquotes
	function nettoyerHTML() {
		// attention : si le mode code source est activé, il n'y a pas d'iframe, donc "document.querySelector("iframe").contentWindow.document;" renvera une erreur
		// si le bouton "Source" de la barre d'outils est activé, on le désactive 
		if(CKEDITOR.instances.ckeditor_capsule.getCommand("source").state == CKEDITOR.TRISTATE_ON) {
			CKEDITOR.instances.ckeditor_capsule.execCommand("source");
		}

		//Référence au document CKEditor
		var cke_document = document.querySelector("iframe").contentWindow.document;
		//Enlève les éléments vides et leurs parents
		cke_document.querySelectorAll("br").forEach(function(elem){
			var parent = elem.parentElement;
			elem.remove();
			if (parent.innerHTML == "" && parent.nodeName != "TD")
				parent.remove();
		});
		//Enllève les blockquotes
		// cke_document.querySelectorAll("blockquote").forEach(function (elem) {
		// 	elem.insertAdjacentHTML("afterend",elem.innerHTML);
		// 	elem.parentNode.removeChild(elem);
		// })
	}
}]);

function addToBookmark() {
  if (window.sidebar && window.sidebar.addPanel) { // Firefox <23
    
    window.sidebar.addPanel(document.title,this.href,'');

  } else if(window.external && ('AddFavorite' in window.external)) { // Internet Explorer

    window.external.AddFavorite(this.href, document.title); 

  } else if(window.opera && window.print || window.sidebar && ! (window.sidebar instanceof Node)) { // Opera <15 and Firefox >23
    /**
     * For Firefox <23 and Opera <15, no need for JS to add to bookmarks
     * The only thing needed is a `title` and a `rel="sidebar"`
     */
    // triggerBookmark.attr('rel', 'sidebar').attr('title', document.title);
    return true;
  
  } else { // For the other browsers (mainly WebKit) we use a simple alert to inform users that they can add to bookmarks with ctrl+D/cmd+D
    
    alert('Vous pouvez ajouter ce bookmark en glissant ce bouton dans votre barre de favoris.');
  
  }
  //debugger
  event.preventDefault();
  // If you have something in the `href` of your trigger
  return false;
}