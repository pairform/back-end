"use strict";


/*
 * Controleurs de gestion de la visibilité des capsules
 */


// page de gestion de la visibilité d'une capsule par les Groupes
backoffice_controlleurs.controller("controleurGestionVisibiliteCapsule", ["$scope", "$http", "toaster", "$routeParams", "$location", "$translate", "connexion", function($scope, $http, toaster, $routeParams, $location, $translate, connexion) {
	var succes_enregistrement, erreur_requete;
	var a_une_visibilite;
	var groupe_selectionne;
	var initialisation_finie = false;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	})

	// traduction des labels de la page
	$translate('LABEL.CAPSULE.VISIBILITE.A_UNE_VISIBILITE').then(function (traduction) {
		a_une_visibilite = traduction;
	})

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;
	$scope.arborescence.nom_capsule = $routeParams.nom_capsule;
	$scope.arborescence.id_capsule = $routeParams.id_capsule;

	/*
	 * Gestion de la visibilité des différents boutons en fonction des droits de l'utilisateur connecté
	 * ATTENTION : reporter les changements dans controleursTableauDeBord.js 
	 */
	$scope.aAccesBoutonGererGroupes = function () {
		var utilisateur_connecte = connexion.getUtilisateurConnecte();
		return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$routeParams.id_espace][GERER_GROUPES];
	}

	// récupération des informations à afficher sur la page
	$http.get(URL_SERVEUR_NODE +"webServices/visibilite/capsule/"+ $routeParams.id_capsule +"/"+ $routeParams.cree_par +"/ressource/"+ $routeParams.id_ressource +"/espace/"+ $routeParams.id_espace)
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				// on divise la liste des groupes en 3 : sphere publique, espace (les 2 groupes obligatoires) et une liste contenant les groupes restants
				$scope.groupe_sphere_publique = retour_json.liste_groupes[0];
				$scope.groupe_espace = retour_json.liste_groupes[1];
				$scope.liste_groupes = retour_json.liste_groupes.slice(2);

				$scope.liste_visibilites_sphere_publique = retour_json.liste_visibilites;
				$scope.liste_visibilites_espace = retour_json.liste_visibilites.slice(0);
				$scope.liste_visibilites_groupe = retour_json.liste_visibilites.slice(0, 2);	// version de la liste des visibilité sans "Invisible" (utilisé pour le groupe car, pour lui, une visibilité Invisible n'a pas de sens)
				
				$scope.sous_titre_visibilite = $scope.arborescence.nom_capsule + a_une_visibilite;

				// si l'utilisateur n'est pas l'administrateur, il ne peut pas utiliser la visibilité "Invisible"
				$scope.utilisateur_autorise = connexion.getUtilisateurConnecte().est_admin_pairform;
				
				// Affichage sur la page de la visibilité associée au groupe Sphère publique 
				switch ($scope.groupe_sphere_publique.id_visibilite) {
					case retour_json.liste_visibilites[0].id_visibilite:
						$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[0];
						break
					case retour_json.liste_visibilites[1].id_visibilite:
						$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[1];
						break
					case retour_json.liste_visibilites[2].id_visibilite:
						$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[2];
						break
				}

				// Affichage sur la page de la visibilité associée au groupe Espace
				switch ($scope.groupe_espace.id_visibilite) {
					case retour_json.liste_visibilites[0].id_visibilite:
						$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[0];
						break
					case retour_json.liste_visibilites[1].id_visibilite:
						$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[1];
						break
					case retour_json.liste_visibilites[2].id_visibilite:
						$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[2];
						console.log("$scope.espace_visibilite_selectionne : "+ $scope.espace_visibilite_selectionne);
						break
				}
				
				// Affichage sur la page du groupe selectionné (s'il y en a un) et de sa visibilité
				for(var clef in $scope.liste_groupes) {
					groupe_selectionne = $scope.liste_groupes[clef];

					if (groupe_selectionne.id_visibilite) {
						$scope.groupe_selectionne = groupe_selectionne;

						// Affichage sur la page de la visibilité associée au groupe groupe_selectionne
						switch (groupe_selectionne.id_visibilite) {
							case retour_json.liste_visibilites[0].id_visibilite:
								$scope.groupe_visibilite_selectionne = $scope.liste_visibilites_groupe[0];
								break
							case retour_json.liste_visibilites[1].id_visibilite:
								$scope.groupe_visibilite_selectionne = $scope.liste_visibilites_groupe[1];
								break
						}
						break;
					}
				}
				initialisation_finie = true;

			} else {
				toaster.pop("error", erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/visibilite/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace");
				toaster.pop("error", erreur_requete);
		});

	// Edite les visibilités de l'Espace et du groupe quand la visibilité de la Sphère publique a été modifiée
	$scope.editerVisibilitesViaSpherePublique = function() {
		// si l'initialisation de l'écran est terminé
		if (initialisation_finie) {
			// Si la visibilité de la Sphere publique devient accessible, alors, les visibilités de l'Espace et du groupe deviennent accessible
			if ($scope.sphere_publique_visibilite_selectionne.id_visibilite == VISIBILITE_ACCESSIBLE) {
				$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[0];
				$scope.groupe_visibilite_selectionne = $scope.liste_visibilites_groupe[0];
			}
		}
	}

	// Edite les visibilités de Sphere publique et du groupe quand la visibilité de l'Espace a été modifiée
	$scope.editerVisibilitesViaEspace = function() {
		// si l'initialisation de l'écran est terminé
		if (initialisation_finie) {
			// Si la visibilité de l'Espace devient accessible, alors, la visibilités du groupe devient accessible
			if ($scope.espace_visibilite_selectionne.id_visibilite == VISIBILITE_ACCESSIBLE) {
				$scope.groupe_visibilite_selectionne = $scope.liste_visibilites_groupe[0];
			}
			// Si la visibilité de l'Espace devient inaccessible ET que la visibilité de la Sphere publique est accessible, alors, la visibilités de la Sphere publique devient inaccessible
			else if ($scope.espace_visibilite_selectionne.id_visibilite == VISIBILITE_INACCESSIBLE && $scope.sphere_publique_visibilite_selectionne.id_visibilite == VISIBILITE_ACCESSIBLE) {
				$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[1];
			}
		}
	}

	// Edite les visibilités de l'Espace et de Sphere publique quand la visibilité du groupe a été modifiée
	$scope.editerVisibilitesViaGroupe = function() {
		// si l'initialisation de l'écran est terminé
		if (initialisation_finie) {
			// Si la visibilité du groupe devient inaccessible ET que la visibilité de l'Espace est accessible, alors, la visibilités de l'Espace devient inaccessible et celle de la Sphere publique invisible
			if ($scope.groupe_visibilite_selectionne && $scope.groupe_visibilite_selectionne.id_visibilite == VISIBILITE_INACCESSIBLE && $scope.espace_visibilite_selectionne.id_visibilite == VISIBILITE_ACCESSIBLE) {
				$scope.espace_visibilite_selectionne = $scope.liste_visibilites_espace[1];
				$scope.sphere_publique_visibilite_selectionne = $scope.liste_visibilites_sphere_publique[2];
			}
		}
	}

	$scope.editerCapsuleVisibilite = function() {
		// récupération des paramètres pour la requete
		var parametres_requete = {};
		parametres_requete.id_espace = $routeParams.id_espace;
		parametres_requete.id_ressource = $routeParams.id_ressource;
		parametres_requete.cree_par = $routeParams.cree_par;

		// ajout des groupes obligatoires, mis à jour, aux parametres
		parametres_requete.sphere_publique = $scope.groupe_sphere_publique;
		parametres_requete.sphere_publique.id_visibilite = $scope.sphere_publique_visibilite_selectionne.id_visibilite;
		parametres_requete.espace = $scope.groupe_espace;
		parametres_requete.espace.id_visibilite = $scope.espace_visibilite_selectionne.id_visibilite;

		// si un groupe non-obligatoire était sélectionné avant l'édition
		if (groupe_selectionne) {
			// on l'ajoute aux parametres
			parametres_requete.id_ancien_groupe_selectionne = groupe_selectionne.id_groupe;
		}
		// si un nouveau groupe non-obligatoire a été selectionne et qu'une visibilité lui est associé
		if ($scope.groupe_selectionne && $scope.groupe_visibilite_selectionne) {
			// on l'ajoute aux parametres
			parametres_requete.nouveau_groupe_selectionne = {
				id_groupe: $scope.groupe_selectionne.id_groupe,
				id_capsule: $routeParams.id_capsule,
				id_visibilite: $scope.groupe_visibilite_selectionne.id_visibilite
			};
		}

		parametres_requete.id_capsule = $routeParams.id_capsule;

		// Ajout du flag pour appliquer la visibilité à toutes les capsules de cette ressource
		parametres_requete.modifier_toutes_capsules = $scope.modifier_toutes_capsules;

		// on edite la visibilité de la capsule en BDD
		$http.post(URL_SERVEUR_NODE +"webServices/visibilite/capsule", parametres_requete)
			.success(function(retour_json){
				if (retour_json.status === RETOUR_JSON_OK) {
					$location.path(URL_BACKOFFICE).search({
						id_espace: $scope.arborescence.id_espace,
						id_ressource: $scope.arborescence.id_ressource,
						id_capsule: $routeParams.id_capsule
					});
					toaster.pop("success", succes_enregistrement);
				} else {
					toaster.pop("error", erreur_requete);
				}
			})
			.error(function(){
				console.log("erreur requete http post /webServices/visibilite/capsule");
				toaster.pop("error", erreur_requete);
			});		
	}
}]);
