"use strict";


/*
 * Controleurs de gestion des open badges
 */

// page de gestion des openbadges
backoffice_controlleurs.controller("controleurGestionOpenBadges", ["$scope", "$http", "toaster", "$translate", "$routeParams", "$timeout", "connexion", function($scope, $http, toaster, $translate, $routeParams, $timeout, connexion){
	var succes_enregistrement, erreur_requete;
	var titre_creer, titre_editer, bouton_creer, bouton_editer;
	$scope.formulaire_soumis = false;
	$scope.logo_selectionne = false;		// false tant qu'aucun logo respectant les contraintes (format, taille, etc.) n'a été associé à la ressource
	$scope.format_incorrect = false;		// false si le format du logo de la ressource n'est pas le bon
	$scope.dimensions_incorrectes = false;	// false si les dimensions du logo de la ressource ne sont pas les bonnes
	$scope.poids_incorrecte = false;		// false si le poids du logo de la ressource est trop gros

	// traduction des labels de la page
	$translate('LABEL.OPENBADGE.CREER').then(function (traduction) {
		$scope.titre = traduction;
		titre_creer = traduction;
	});
	$translate('LABEL.OPENBADGE.EDITER').then(function (traduction) {
		titre_editer = traduction;
	});
	$translate('LABEL.CREER').then(function (traduction) {
		bouton_creer = traduction;
		$scope.bouton_formulaire = traduction;
	});
	$translate('LABEL.ENREGISTRER').then(function (traduction) {
		bouton_editer = traduction;
	});

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.ENREGISTREMENT').then(function (traduction) {
		succes_enregistrement = traduction;
	})
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});

	// initialisation du bouton de sélection des openbadges
	$translate('LABEL.OPENBADGE.SELECTIONNER').then(function (traduction) {
		$scope.nom_openbadge_selectionne = traduction;
		$scope.nom_openbadge_selectionne_defaut = traduction;
	});

	// vide le formulaire de création/édition d'openbadge
	var nettoyerFomulaire = function() {
		$scope.id_openbadge = null;
		$scope.nom = null;
		$scope.description = null;
		$scope.url_logo = null;
		$scope.src_logo = null;
		$scope.issuer_externe = null;
		$scope.deja_emetteur_openbadges = false;
		$scope.recherche_openbadge = null;
	}

	// Mise à jour de l'arborescence espace / ressource / capsule
	$scope.arborescence = {};
	$scope.arborescence.nom_espace = $routeParams.nom_espace;
	$scope.arborescence.id_espace = $routeParams.id_espace;
	$scope.arborescence.nom_ressource = $routeParams.nom_ressource;
	$scope.arborescence.id_ressource = $routeParams.id_ressource;

	// si l'url de l'espace n'est pas défini, l'utilisateur ne doit pas pouvoir créer des badges
	$scope.url_espace = $routeParams.url_espace;

	/*
	 * Gestion de la visibilité des différents boutons en fonction des droits de l'utilisateur connecté
	 * ATTENTION : reporter les changements dans controleursTableauDeBord.js 
	 */
	$scope.aAccesBoutonEditerEspace = function () {
		var utilisateur_connecte = connexion.getUtilisateurConnecte();
		return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$routeParams.id_espace][EDITER_ESPACE];
	}

	// récupération des informations à afficher sur la page
	$http.get(URL_SERVEUR_NODE +"webServices/openbadges/espace/"+ $routeParams.id_espace +"/ressource/"+ $routeParams.id_ressource +"/"+ $routeParams.ressource_cree_par)
		.success(function(retour_json){
			if (retour_json.status === RETOUR_JSON_OK) {
				$scope.liste_openbadges = retour_json.liste_openbadges;
			} else {
				toaster.pop("error", erreur_requete);
			}
		})
		.error(function(){
			console.log("erreur requete http get /webServices/openbadges/espace/:id_espace/ressource/:id_ressource/:ressource_cree_par");
			toaster.pop("error", erreur_requete);
		});


	// selectionne l'openbadge choisi par l'utilisateur
	$scope.selectionnerOpenBadge = function (openbadge) {
		$scope.openbadge_selectionne = openbadge;
		$scope.nom_openbadge_selectionne = openbadge.nom;
		$scope.editer_openbadge = false;
	}

	// récupération des informations sur le champ "Logo"
	$scope.recupererLogo = function($files) {
		// si le fichier n'est pas une image dans le bon format
		if ($files[0].type != "image/png") {
			$scope.format_incorrect = true;
			$scope.logo_selectionne = false;
			$scope.dimensions_incorrectes = false;
			$scope.poids_incorrecte = false;

		} else {
			var _URL = window.URL || window.webkitURL;
			var image = new Image();

			image.onload = function() {
				// si l'image du logo n'a pas les bonnes dimensions
				if (this.width != DIMENSION_LOGO_BADGE || this.height != DIMENSION_LOGO_BADGE) {
					// le timeout permet d'attendre que les valeurs soient chargées avant de les mettre à jour à l'écran, sinon, le chargement est trop rapide et les valeurs ne sont pas initalisées
					$timeout(function () {
						$scope.dimensions_incorrectes = true;
						$scope.logo_selectionne = false;
						$scope.format_incorrect = false;
						$scope.poids_incorrecte = false;
					}, 500);

				}	// sinon, si l'image du logo est trop grosse
				else if (this.size > POIDS_MAX_LOGO_BADGE) {
					// le timeout permet d'attendre que les valeurs soient chargées avant de les mettre à jour à l'écran, sinon, le chargement est trop rapide et les valeurs ne sont pas initalisées
					$timeout(function () {
						$scope.poids_incorrecte = true;
						$scope.logo_selectionne = false;
						$scope.format_incorrect = false;
						$scope.dimensions_incorrectes = false;
					}, 500);

				}	// sinon, on affiche les infos sur le logo
				else {
					$scope.logo_selectionne = true;
					$scope.format_incorrect = false;
					$scope.dimensions_incorrectes = false;
					$scope.poids_incorrecte = false;

					// affichage du nom du logo sélectionné
					$scope.nom_logo = $files[0].name;

					// affichage du logo selectionné
					var file_reader = new FileReader();

					file_reader.onload = function (e) {
						// le timeout permet d'attendre que l'image soit chargée avant de la mettre à jour à l'écran, sinon, le chargement est trop rapide et l'image n'est pas initalisée
						$timeout(function () {
							$scope.src_logo = e.target.result;
						}, 500);
					};

					file_reader.readAsDataURL($files[0]);
				}
			}

			// transformation du fichier en image, la fonction onload sera appelé à la fin
			image.src = _URL.createObjectURL($files[0]);			
		}
	}

	// mise à jour du formulaire pour créer un openbadge
	$scope.creerOpenBadge = function() {
		nettoyerFomulaire();

		// mise à jour du bouton et du titre du formulaire
		$scope.titre = titre_creer;
		$scope.bouton_formulaire = bouton_creer;

		$scope.editer_openbadge = false;
		$scope.openbadge_selectionne = false;
		$scope.formulaire_soumis = false;
		$scope.nom_openbadge_selectionne = $scope.nom_openbadge_selectionne_defaut;
	}

	// mise à jour du formulaire pour éditer un openbadge
	$scope.editerOpenBadge = function(openbadge_selectionne) {
		$scope.id_openbadge = openbadge_selectionne.id_openbadge;
		$scope.nom = openbadge_selectionne.nom;
		$scope.description = openbadge_selectionne.description;
		$scope.url_logo = openbadge_selectionne.url_logo;
		$scope.issuer_externe = openbadge_selectionne.issuer_externe;

		openbadge_selectionne.issuer_externe ? $scope.deja_emetteur_openbadges = true : $scope.deja_emetteur_openbadges = false;

		// mise à jour du bouton et du titre du formulaire
		$scope.titre = titre_editer;
		$scope.bouton_formulaire = bouton_editer;

		$scope.editer_openbadge = true;
		$scope.formulaire_soumis = false;
	}

	$scope.gererOpenBadge = function(formulaireValide, edition) {
		$scope.formulaire_soumis = true;
		// si tout les champs du formulaire sont valides
		if(formulaireValide && ($scope.logo_selectionne || $scope.url_logo)) {
			// si c'est une édition de badge
			if (edition) {
				var logo;

				// si le logo a été modifié
				if ($scope.src_logo)
					logo = $scope.src_logo.substring(IMG_DATA_TYPE.length);		// on envoie l'image encodé en base64, seul le code base64 de l'image est conservé

				// on met à jour l'openbadge en BDD
				$http.post(URL_SERVEUR_NODE +"webServices/openbadge", {
						id_openbadge: $scope.id_openbadge,
						nom: $scope.nom,
						description: $scope.description,
						logo: logo,
						issuer_externe: $scope.issuer_externe,
						id_espace: $routeParams.id_espace,
						id_ressource: $routeParams.id_ressource,
						ressource_cree_par: $routeParams.ressource_cree_par
					})
					.success(function(retour_json){
						if (retour_json.status === RETOUR_JSON_OK) {
							$scope.openbadge_selectionne.nom = $scope.nom
							$scope.openbadge_selectionne.description = $scope.description
							$scope.openbadge_selectionne.issuer_externe = $scope.issuer_externe
							$scope.nom_openbadge_selectionne = $scope.nom;
							$scope.editer_openbadge = false;

							nettoyerFomulaire();
							toaster.pop("success", succes_enregistrement);
						} else {
							// Si le nom est identique à celui d'un badge existant
							if (retour_json.message === ERREUR_DOUBLON) {
								// on indique la présence du doublon
								$scope.openbadge_doublon = true;
								$scope.gestion_openbadge_form.nom.$invalid = false;
							} else {
								toaster.pop("error", erreur_requete);
							}
						}
					})
					.error(function(){
						console.log("erreur requete http post /webServices/openbadge");
						toaster.pop("error", erreur_requete);
					});
			} else {	// sinon, c'est une création d'un nouveau badge
				// on ajoute l'openbadge en BDD
				$http.put(URL_SERVEUR_NODE + "webServices/openbadge", {
						nom: $scope.nom,
						description: $scope.description,
						logo: $scope.src_logo.substring(IMG_DATA_TYPE.length),		// on envoie l'image encodé en base64, seul le code base64 de l'image est conservé
						issuer_externe: $scope.issuer_externe,
						id_espace: $routeParams.id_espace,
						id_ressource: $routeParams.id_ressource,
						ressource_cree_par: $routeParams.ressource_cree_par
					})
					.success(function(retour_json){
						if (retour_json.status === RETOUR_JSON_OK) {
							var nouvel_openbadge = {
								id_openbadge: retour_json.id_openbadge,
								id_ressource: $routeParams.id_ressource,
								nom: $scope.nom,
								description: $scope.description,
								url_logo: retour_json.url_logo,
								issuer_externe: $scope.issuer_externe
							};

							$scope.liste_openbadges.push(nouvel_openbadge);
							$scope.selectionnerOpenBadge(nouvel_openbadge);
							nettoyerFomulaire();

							toaster.pop("success", succes_enregistrement);
						} else {
							// Si le nom est identique à celui d'un badge existant
							if (retour_json.message === ERREUR_DOUBLON) {
								// on indique la présence du doublon
								$scope.openbadge_doublon = true;
								$scope.gestion_openbadge_form.nom.$invalid = false;
							} else {
								toaster.pop("error", erreur_requete);
							}
						}
					})
					.error(function(){
						console.log("erreur requete http put /webServices/openbadge");
						toaster.pop("error", erreur_requete);
					});
			}
		}
	}
}]);
