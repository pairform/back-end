"use strict";

/*
 * Filtres utilisé dans le backoffice
 */
angular.module("backofficeFiltres", [])

	// filtre la liste des capsules pour ne conserver que les capsules d'une ressource
	.filter("filtreByIdRessource", function() {
		return function(liste_capsules, id_ressource) {
			var liste_capsules_filtres = new Array();

			// si une ressource a été sélectionnée 
			if (id_ressource) {
				for(var i in liste_capsules) {
					// si la ressource sélectionnée contient la capsule
					if (id_ressource === liste_capsules[i].id_ressource) {
						liste_capsules_filtres.push(liste_capsules[i]);
					}
				}
				return liste_capsules_filtres;

			} else {
				return liste_capsules;
			}
		};
	})

	// filtre la liste des ressource pour ne conserver que les ressources d'un espace
	.filter("filtreByIdEspace", function() {
		return function(liste_ressources, id_espace) {
			var liste_ressources_filtres = new Array();

			// si un espace a été sélectionné 
			if (id_espace) {
				for(var i in liste_ressources) {
					// si l'espace sélectionné contient la ressource
					if (id_espace === liste_ressources[i].id_espace) {
						liste_ressources_filtres.push(liste_ressources[i]);
					}
				}
				return liste_ressources_filtres;

			} else {
				return liste_ressources;
			}
		};
	});