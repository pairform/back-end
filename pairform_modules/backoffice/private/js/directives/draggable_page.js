backoffice_controlleurs.directive('draggable', ["$document", function($document) {
	return {
		restrict: 'A',
		scope:{
			capsule : "=capsule",
			item : "=item"
		},
		link: function(scope, element, attr) {


			function handleDragStart(e) {
				//On empèche l'event de bubbler 
				//Sur un DnD d'un message, ca bubble au btn-bar
				e.stopPropagation();

				var data,
					dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer;

				dataTransfer.effectAllowed = 'move';
				
				//Si on déplace un item
				if (scope.item)
					data = JSON.stringify(scope.item);
				//Sinon, on déplace une capsule
				else if (scope.capsule)
					data = JSON.stringify(scope.capsule);

				dataTransfer.setData('text/javascript', data);
				// this/e.target is the source node.
				// this.addClassName('moving');
				scope.$root.$broadcast("pf:dragstart");
			};

			function handleDragEnd(e) {
				scope.$root.$broadcast("pf:dragend");
			};
			//S'il n'y a pas d'URL définie, on interdit le drag'n'drop
			if (scope.item || (scope.capsule && scope.capsule.url_web)) {
				// col.setAttribute('draggable', 'true');  // Enable columns to be draggable.
				element.on('dragstart', handleDragStart);
				element.on('dragend', handleDragEnd);
			}
		}
	}
}]);
