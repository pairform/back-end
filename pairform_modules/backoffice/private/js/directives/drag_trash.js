backoffice_controlleurs.directive('dragTrash', ["$document", "$timeout", function($document, $timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {

      function handleDragOver(e) {
        if (e.preventDefault) {
          e.preventDefault(); // Allows us to drop.
        }
        if (e.target == element[0] || e.target.parentElement == element[0]) 
          element.addClass('pf-droppable-hover');

        return false;
      };

      function handleDragEnter(e) {
        if (e.target == element[0] || e.target.parentElement == element[0]) 
          element.addClass('pf-droppable-hover');

      };

      function handleDragLeave(e) {
        if (e.target == element[0] || e.target.parentElement == element[0]) 
          element.removeClass('pf-droppable-hover');

      };

      function handleDrop(e) {
        // this/e.target is current target element.

        if (e.stopPropagation) {
          e.stopPropagation(); // stops the browser from redirecting.
        }

        var dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer,
           data_string = dataTransfer.getData("text/javascript");
        if (data_string && data_string != "undefined") {
          var item = JSON.parse(data_string);
          //Check qu'on a bien objet
          if(item instanceof Object && item.id_item){
            // debugger
            scope.$apply(function () {
              scope.deleteItem(item);
            })
          }

        }
        scope.$root.$broadcast("pf:dragend");
      };

      scope.$on("pf:dragstart", function () {
        element.on('dragenter', handleDragEnter);
        element.on('dragover', handleDragOver);
        element.on('dragleave', handleDragLeave);
        element.on('drop', handleDrop);
        
        element.addClass("pf-droppable");


      });

      // $scope.displayItem = function (item) {
      //   $scope.$parent.display_item = $scope.$parent.display_item == item ? false : item;
      //   //PUT ws
      // }

      scope.$on("pf:dragend", function () {
        // body...
        element.removeClass("pf-droppable");

        element.off('dragenter');
        element.off('dragover');
        element.off('dragleave');
        element.off('drop');
      });
    }
  }
}]);