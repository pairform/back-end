var express = require("express"),
	 CONFIG = require("config");


module.exports = function(app, passport) {

	app.get("/backoffice", function (req, res) {
		// res.render(__dirname + "/views/index", {
		// 	utilisateur: req.user ? req.user : "-1",
		// 	url_serveur_node : CONFIG.app.urls.serveur_node + "/"
		// });
		res.redirect(CONFIG.app.urls.serveur_node + "/home/backoffice");
	});

	app.get("/backoffice_lite", function (req, res) {
		res.render(__dirname + "/views/index_lite", {
			utilisateur: req.user ? req.user : "-1",
			url_serveur_node : CONFIG.app.urls.serveur_node + "/"
		});
	});

	app.get("/backoffice/:branche/:nom_view", function (req, res) {
		res.render(__dirname + "/views/pages/" + req.params.nom_view);
	});
}
