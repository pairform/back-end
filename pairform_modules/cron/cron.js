var web_services = require("../webservices/lib/webServices"),
		  CONFIG = require("config"),
pairform_dao = require(global.pairform_modules + "webservices/node_modules/pairform_dao"),
		  cluster = require("cluster"),
      pf_crypto = require(global.pairform_modules + "utils/pf_crypto");

// Protection anti-doublon par les différents clusters, verification qu'on est sur la prod, et pas en instance de développement.
if(cluster.isMaster && CONFIG.app.urls.alias_serveur.match(/pairform.fr/)){
  pairform_dao.attendreCreationPool(function (_connexion_courante) {
    require("mail_resume");
    require("mail_inbox");
    require("suppression_bas");
    require("mail_watch");
    require("mail_notifications");
    require("session_item");
    //require("suppression_compte_inactif");
  });
}


app.get("/mail/resume", function (req, res) {   
  res.render(__dirname + "/node_modules/mail_resume/views/derniers_messages",
    {
      utilisateur : {
        id_utilisateur: 1,
        pseudo: 'admin',
        email: 'contact@pairform.fr',
        email_est_valide: 1,
      },
      messages: [{
        id_message: 75,
        nom_court_ressource: 'Transition énergétique',
        nom_court_capsule: null,
        id_auteur: 11,
        contenu: '@admin Pranadol ?',
        date_creation: 'Le 28/12/17 à 19:41',
        id_espace: null,
        id_ressource: 3,
        id_capsule: null,
        nom_page: '',
        nom_tag: '',
        num_occurence: 0,
        geo_latitude: 0,
        geo_longitude: 0,
        id_message_parent: 74,
        id_langue: 3,
        supprime_par: 0,
        medaille: '',
        est_defi: 0,
        defi_valide: 0,
        prive: 0,
        visibilite: '0',
        somme_votes: 0,
        tags: '[]',
        pseudo_auteur: 'Francois',
        nom_auteur: 'Francois Hollande',
        id_role_auteur: 0,
        role_auteur: 'Lecteur',
        url_avatar_auteur: 'http://127.0.0.1:3000/res/avatars/11.png' 
      }],
      url_serveur_node : CONFIG.app.urls.serveur_node,
      number_of_messages_to_show : 20,
      hash: pf_crypto.encrypt(1)
    }
  );
});

app.get("/mail/inbox", function (req, res) {   
  res.render(__dirname + "/node_modules/mail_inbox/views/derniers_messages",
    {
      utilisateur : {
        id_utilisateur: 1,
        pseudo: 'admin',
        email: 'contact@pairform.fr',
        email_est_valide: 1,
        id_messages: '75'
      },
      messages: [{
        id_message: 75,
        nom_court_ressource: 'Transition énergétique',
        nom_court_capsule: null,
        id_auteur: 11,
        contenu: '@admin Pranadol ?',
        date_creation: 'Le 28/12/17 à 19:41',
        id_espace: null,
        id_ressource: 3,
        id_capsule: null,
        nom_page: '',
        nom_tag: '',
        num_occurence: 0,
        geo_latitude: 0,
        geo_longitude: 0,
        id_message_parent: 74,
        id_langue: 3,
        supprime_par: 0,
        medaille: '',
        est_defi: 0,
        defi_valide: 0,
        prive: 0,
        visibilite: '0',
        somme_votes: 0,
        tags: '[]',
        pseudo_auteur: 'Francois',
        nom_auteur: 'Francois Hollande',
        id_role_auteur: 0,
        role_auteur: 'Lecteur',
        url_avatar_auteur: 'http://127.0.0.1:3000/res/avatars/11.png' 
      }],
      url_serveur_node : CONFIG.app.urls.serveur_node,
      hash: pf_crypto.encrypt(1)
    }
  );
});

app.get("/mail/watch", function (req, res) {   
  res.render(__dirname + "/node_modules/mail_watch/views/derniers_messages",
    {
      utilisateur : {
        id_utilisateur: 1,
        pseudo: 'admin',
        email: 'contact@pairform.fr',
        email_est_valide: 1,
        id_messages: '75'
      },
      messages: [{
        id_message: 75,
        nom_court_ressource: 'Transition énergétique',
        nom_court_capsule: null,
        id_auteur: 11,
        contenu: '@admin Pranadol ?',
        date_creation: 'Le 28/12/17 à 19:41',
        id_espace: null,
        id_ressource: 3,
        id_capsule: null,
        nom_page: '',
        nom_tag: '',
        num_occurence: 0,
        geo_latitude: 0,
        geo_longitude: 0,
        id_message_parent: 74,
        id_langue: 3,
        supprime_par: 0,
        medaille: '',
        est_defi: 0,
        defi_valide: 0,
        prive: 0,
        visibilite: '0',
        somme_votes: 0,
        tags: '[]',
        pseudo_auteur: 'Francois',
        nom_auteur: 'Francois Hollande',
        id_role_auteur: 0,
        role_auteur: 'Lecteur',
        url_avatar_auteur: 'http://127.0.0.1:3000/res/avatars/11.png' 
      }],
      url_serveur_node : CONFIG.app.urls.serveur_node,
      hash: pf_crypto.encrypt(1)
    }
  );
});

app.get("/mail/notifications", function (req, res) {   
  res.render(__dirname + "/node_modules/mail_notifications/views/dernieres_notifications",
    {
      utilisateur :  
      {
        "email": "mjuganaikloo@gmail.com",
        "id_utilisateur": 2,
        "notifications": [
          {
            "background_image": "url(http://localhost:3000/private/img/du.png)",
            "contenu": "4",
            "date_creation": "2017-08-02T12:31:43.000Z",
            "email": "mjuganaikloo@gmail.com",
            "href": "http://localhost:3000/webServices/capsule/rediriger?id_capsule=4",
            "id_message": null,
            "id_notification": 55,
            "id_utilisateur": 2,
            "image": "du",
            "label": null,
            "pseudo": "azerty",
            "sous_type": "du",
            "titre": "Test 2003 a été mis à jour"
          },
          {
            "background_image": "url(http://localhost:3000/private/img/succes/medailles_4.png)",
            "contenu": "Médaille",
            "date_creation": "2017-08-02T12:32:49.000Z",
            "email": "mjuganaikloo@gmail.com",
            "href": "http://localhost:3000/home/profil/Médaille",
            "id_message": null,
            "id_notification": 59,
            "id_utilisateur": 2,
            "image": "medailles_4",
            "label": "Médaille",
            "pseudo": "azerty",
            "sous_type": "gs",
            "titre": "Vous avez gagné un succès"
          },
          {
            "background_image": "url(http://localhost:3000/private/img/medaille_or.png)",
            "contenu": "Medaille - or",
            "date_creation": "2017-08-02T12:32:49.000Z",
            "email": "mjuganaikloo@gmail.com",
            "href": "http://localhost:3000/webServices/message/rediriger?id_message=18",
            "id_message": 18,
            "id_notification": 58,
            "id_utilisateur": 2,
            "image": "medaille_",
            "label": "Medaille - or",
            "pseudo": "azerty",
            "sous_type": "gm",
            "titre": "Vous avez gagné une médaille"
          },
          {
            "background_image": "url(http://localhost:3000/private/img/comment_unreaded_64.png)",
            "contenu": "1",
            "date_creation": "2017-08-02T12:32:59.000Z",
            "email": "mjuganaikloo@gmail.com",
            "href": "http://localhost:3000/webServices/message/rediriger?id_message=21",
            "id_message": 21,
            "id_notification": 61,
            "id_utilisateur": 2,
            "image": "comment_unreaded_64",
            "label": "Bac à truc",
            "pseudo": "azerty",
            "sous_type": "ru",
            "titre": "Réponse de Admin"
          },
          {
            "background_image": "url(http://localhost:3000/private/img/comment_unreaded_64.png)",
            "contenu": "1",
            "date_creation": "2017-08-02T12:32:59.000Z",
            "email": "mjuganaikloo@gmail.com",
            "href": "http://localhost:3000/webServices/message/rediriger?id_message=21",
            "id_message": 21,
            "id_notification": 61,
            "id_utilisateur": 2,
            "image": "comment_unreaded_64",
            "label": "Cliquez pour voir son profil",
            "pseudo": "azerty",
            "sous_type": "an",
            "titre": "Réponse de Admin"
          }
        ],
        "pseudo": "azerty"
      },
      urls: CONFIG.app.urls,
      moment : require("moment"),
      hash: pf_crypto.encrypt(2)
    }
  );
});

//Affichage du mail envoyé automatiquement pour la diffusion des items de session (timeline)
//Utilisation de l'utilisateur connecté pour exemple
app.get("/mail/session/:id_item", function (req, res) { 
  pairform_dao.session_item_dao.selectItem(
    req.params.id_item,
    function (retour_sql) {
      if (retour_sql.length) {
        var item = retour_sql[0];
        //Récupération d'infos supplémentaires pour mail : 
        //nom et logo_espace, nom ressource et logo, nom session
        pairform_dao.session_dao.selectInfosForDiffusion(
          item.id_session,
          function callbackSuccess(retour_sql) {
            res.render(__dirname + "/node_modules/session_item/views/session_item", {
              "utilisateur" : {
                pseudo : "PSEUDO",
              },
              "item" : item,
              "extra" : retour_sql[0],
              "url_serveur_node" : CONFIG.app.urls.serveur_node
            });

          },
          function (err) {
            res.render("404",{
              "url_serveur_node" : CONFIG.app.urls.serveur_node
            });
          }
        )
      }
      else 
        res.render("404",{
          "url_serveur_node" : CONFIG.app.urls.serveur_node
        });
    })
});

//Affichage du mail envoyé automatiquement pour avertir un utilisateur de la suppression de son compte
//Utilisation de l'utilisateur connecté pour exemple
app.get("/mail/suppression_compte_inactif", function (req, res) { 
  res.render(__dirname + "/node_modules/suppression_compte_inactif/views/suppression_compte_inactif",
    {
      utilisateur :  
      {
        "email": "mjuganaikloo@gmail.com",
        "id_utilisateur": 2,
        "pseudo": "Francois l'embrouille"
      },
      url_serveur_node : CONFIG.app.urls.serveur_node,
      nombre_annees_suppression : 3,
      nombre_semaines_prevention : 2,
      date_suppression : "05/06/2018",
      hash : pf_crypto.encrypt(2)
    }
  );
});