
function Reseau (options) {
	this.id_collection = "0";
	this.nom = "Sans titre";
	this.profils = [];

	angular.extend(this,options);
}

function Classe (options) {
	Reseau.call(this);

	this.cle = "";

	angular.extend(this,options);
}

Classe.prototype = new Reseau();
Classe.prototype.constructor = Classe;