typeof PF == "undefined" ? PF = {} : null;

PF.langue = {
	"tableau" : {
		"1" : "en",
		"2" : "es",
		"3" : "fr"
	},
	"tableau_labels" : [
		{"id" :"1", "label": "English"},
		{"id" :"2", "label": "Español"},
		{"id" :"3", "label": "Français"}
	],
	"tableau_inverse" : {
		"en" : "1",
		"es" : "2",
		"fr" : "3"
	},
	"app" : "fr"
};

PF.langue.codeFromId = function (id_langue) {
	return PF.langue.tableau[id_langue] || "fr";
}

PF.langue.idFromCode = function (code) {
	return PF.langue.tableau_inverse[code] || "3";
}
PF.langue.initLangueNavigateur = function() {
	if (typeof (PF.langue.langueNavigateur = navigator.language) === 'string');
	else if (typeof (PF.langue.langueNavigateur = navigator.userLanguage) === 'string');
	else PF.langue.langueNavigateur = "en";

	if (PF.langue.langueNavigateur.length>2)
		PF.langue.langueNavigateur=PF.langue.langueNavigateur.charAt(0)+PF.langue.langueNavigateur.charAt(1);

	if (PF.langue.tableau_inverse[PF.langue.langueNavigateur] == undefined) {
		PF.langue.langueNavigateur = "en";
	}
}

PF.LanguageM = {
	langueApp: "en",
	langueNavigateur: "en",
	langueUserPrincipale: "fr",
	languesUserAutres: [],
	arrayLanguesValuesName: [],
	arrayRealLanguesValuesName: [],
	arrayLanguesValuesCode: [],
	arrayLanguesValuesId: [],

	init: function() {
		this.initLanguageArrays();
		this.getLangueNavigateur();
	},

	initLanguageArrays: function() {
		var tmp_arrays;

		$.ajaxSetup({ async: false });
		$.getJSON(globals.dynRes + "json/arrays.json", function(data) {
			tmp_arrays = data;
		});
		$.ajaxSetup({ async: true });

		this.arrayRealLanguesValuesName = tmp_arrays.langue_values_string;
		this.arrayLanguesValuesCode = tmp_arrays.langue_values_code;
		this.arrayLanguesValuesId = tmp_arrays.langue_values_id;

		return this;
	},

	setLangueValuesNameArray: function() {
		var tmp_arrays;

		$.ajaxSetup({ async: false });
		$.getJSON(globals.dynRes + "json/"+ this.langueApp +"/arrays.json", function(data) {
			tmp_arrays = data;
		});
		$.ajaxSetup({ async: true });

		this.arrayLanguesValuesName = tmp_arrays.langue_values_string;
	},

	getLangueNavigateur: function() {
		if (typeof (this.langueNavigateur = navigator.language) === 'string');
		else if (typeof (this.langueNavigateur = navigator.userLanguage) === 'string');
		else this.langueNavigateur = "en";

		if (this.langueNavigateur.length>2)
			this.langueNavigateur=this.langueNavigateur.charAt(0)+this.langueNavigateur.charAt(1);

		if ($.inArray(this.langueNavigateur, this.arrayLanguesValuesCode) == -1) {
			this.langueNavigateur = "en";
		}
	},

	stringLanguesAffichageForBDD: function(isConnected) {
		var tmpStringLangues = "";
		if(isConnected) {
			tmpStringLangues += this.idLangueWithCode(this.langueUserPrincipale).toString();

			for(var i=0; i<this.languesUserAutres.length; i++) {
				tmpStringLangues += "," + this.idLangueWithCode(this.languesUserAutres[i]);
			}

			return tmpStringLangues;
		} else {
			tmpStringLangues += this.arrayLanguesValuesId[0].toString();
			
			for(var i=1; i<this.arrayLanguesValuesId.length; i++) {
				tmpStringLangues += "," + this.arrayLanguesValuesId[i];
			}

			return tmpStringLangues;
		}
	},

	arrayFromBDDOtherLanguages: function(array) {
		var tmpArray = new Array();
		
		for(var i=0; i<array.length; i++) {
			tmpArray.push(array[i]["code_langue"]);
		}

		return tmpArray;
	},

	arrayLanguageIdWithArrayLanguageCode: function(arrayLanguageCode) {
		for(var i=0; i<arrayLanguageCode.length; i++) {
			arrayLanguageCode[i] = this.idLangueWithCode(arrayLanguageCode[i]);
		}

		return arrayLanguageCode;
	},

	idLangueWithCode: function(codeLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesCode.indexOf(codeLangue);
		return this.arrayLanguesValuesId[tmpArrayIndex];
	},

	idLangueWithName: function(nomLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesName.indexOf(nomLangue);
		return this.arrayLanguesValuesId[tmpArrayIndex];
	},

	nameLangueWithCode: function(codeLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesCode.indexOf(codeLangue);
		return this.arrayLanguesValuesName[tmpArrayIndex];
	},

	nameRealLangueWithCode: function(codeLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesCode.indexOf(codeLangue);
		return this.arrayRealLanguesValuesName[tmpArrayIndex];
	},

	codeLangueWithId: function(idLangue) {
		return this.arrayLanguesValuesCode[this.arrayIndexWithId(idLangue)];
	},

	codeLangueWithName: function(nomLangue) {
		var tmpArrayIndex = this.arrayLanguesValuesName.indexOf(nomLangue);
		return this.arrayLanguesValuesCode[tmpArrayIndex];
	},

	arrayIndexWithId: function(idLangue) {
		return this.arrayLanguesValuesId.indexOf(idLangue);
	}
};