typeof PF == "undefined" ? PF = {} : null;

var LARGEUR_PANNEAU_PX = "400px",
    LARGEUR_PANNEAU = 400,
    TEMPS_ANIMATION = 200,
    ID_RES_TEST = "51";


PF.includeHead = function (url, callback)
{
    var head = window.document.getElementsByTagName('head')[0];
    var type = url.slice(url.lastIndexOf('.'));
    switch (type)
    {
        case '.js':
        var addedHead = window.document.createElement('script');
        addedHead.setAttribute('src', url);

        if(callback)
        {
            var completed = false;
            addedHead.onload = addedHead.onreadystatechange = function () {
                if (!completed && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                    completed = true;
                    callback();
                    addedHead.onload = addedHead.onreadystatechange = null;
                    head.removeChild(addedHead);
                }
            };
        }
        break;
        case '.css':
        var addedHead = window.document.createElement('link');
        addedHead.setAttribute('href', url);
        addedHead.setAttribute('type', 'text/css');
        addedHead.setAttribute('rel', 'stylesheet');
        break;
        case '.less':
        var addedHead = window.document.createElement('link');
        addedHead.setAttribute('href', url);
        addedHead.setAttribute('type', 'text/css');
        addedHead.setAttribute('rel', 'stylesheet/less');
        break;
        default:
        var addedHead = '<!--[if lte IE 8]>    <script type="text/javascript"      src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>    <sc     // Le code conditionnel qui check si Google Chrome Frame est déjà installé     // Il ouvre une iFrame pour proposer le téléchargement.     window.attachEvent("onload", function() {       CFInstall.check({         mode: "overlay" // the default       });     });    </script>        //La balise indiquant à IE d`utiliser GCF si présent    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->'
        break;

    }
    addedHead.async =true;
    head.appendChild(addedHead);
}

//Récupération du titre de la ressource
PF.getHash = function () {
   
    var hash_ressource = document.querySelector('meta[name="hash_ressource"]');
    if(hash_ressource)
        return hash_ressource.content;
    else 
        return false;
    
}

//Le site est-il construit avec Scenari
PF.isScenari = function () {
    //"<meta name='generator' content='* scenari || SCENARI *'>"
    var meta_tag_content = document.querySelector('meta[name="generator"]','head');
    if(!meta_tag_content)
        return false;

    if(meta_tag_content.content.match(/scenari/i))
        return true;
    else
        return false;
}
//Le site est-il dans un cas spécial, ou le sélécteur est défini directement dans la page
PF.isSpecialCase = function () {
    var meta_tag_content = document.querySelector('meta[name="generator"]','head');
    if(!meta_tag_content)
        return false;

    if(meta_tag_content.content == "special")
        return true;
    else
        return false;
}


//Le site est-il généré depuis LaTeX
PF.isFromLatex = function () {
    //"<meta name='generator' content='* scenari || SCENARI *'>"
    var meta_tag_content = document.querySelector('meta[name="generator"]','head');
    if(!meta_tag_content)
        return false;

    if(meta_tag_content.content == "http://www.nongnu.org/elyxer/")
        return true;
    else
        return false;
}


PF.isWebVersion = function () {
    var meta_tag_content = document.querySelector('meta[name="platform"]','head');
    if(!meta_tag_content)
        return true;
    else if(meta_tag_content.content === "web")
        return true;
    else 
        return false;
}

PF.isMobVersion = function () {
    var meta_tag_content = document.querySelector('meta[name="platform"]','head');
    if(!meta_tag_content)
        return false;
    else if(meta_tag_content.content === "mob")
        return true;
    else 
        return false;
}

PF.hasLocalStorage = function (callback_success, callback_error, callback_fix) {
     try {
        PF._localStorage = localStorage;
        //Premier test I/O
        PF._localStorage.setItem("_pf_test", "_pf_test");
        PF._localStorage.removeItem("_pf_test");
        return callback_success();
    } catch(e) {
        try {
            //On tente un polyfill sur le prototype du localStorage
            PF.polyfillLocalStorage(Storage.prototype);
            //On teste à nouveau, après avoir polyfillé le localstorage
            PF._localStorage = localStorage;
            PF._localStorage.setItem("_pf_test", "_pf_test");
            PF._localStorage.removeItem("_pf_test");
            PF.showCompatibilityWarning(0, "Votre navigateur n'est pas pleinement compatible avec PairForm. ");
            return callback_success();
        } catch(e) {
            try {
                //Sinon, on utilise un objet temporaire pour stocker les infos
                PF._localStorage = {};
                PF.polyfillLocalStorage(PF._localStorage);
                PF._localStorage.setItem("_pf_test", "_pf_test");
                PF._localStorage.removeItem("_pf_test");
                PF.showCompatibilityWarning(0, "Votre navigateur n'est pas pleinement compatible avec PairForm. ");
                return callback_success();
            } catch(e) {
                return callback_error(e);
            }
        }
    }
}
PF.polyfillLocalStorage = function (polyfilled_object) {
    // polyfilled_object._setItem = polyfilled_object.setItem;
    polyfilled_object.setItem = function (key, value) { 
        Object.defineProperty(polyfilled_object, key, {
            configurable: true,
            get: function myProperty() {
                return value;
            }
        });
        return value;
         // PF.__localStorage[key] = String(value); 
    };
     
    // polyfilled_object._getItem = polyfilled_object.getItem;
    polyfilled_object.getItem = function (key) { 
        
        if (polyfilled_object.hasOwnProperty(key))
            return polyfilled_object[key];
        else return undefined;
    };
     
    // polyfilled_object._removeItem = polyfilled_object.removeItem;
    polyfilled_object.removeItem = function (key) { 
        
        if (polyfilled_object.hasOwnProperty(key))
            return delete polyfilled_object[key];
        else return false;
    };

    // polyfilled_object._clear = polyfilled_object.clear;
    polyfilled_object.clear = function () { 
        for(var key in polyfilled_object){
            if (polyfilled_object.hasOwnProperty(key))
                delete polyfilled_object[key];
        }
        return true; 
    };

    return polyfilled_object;
}
PF.showCompatibilityWarning = function (top_position, message) {
    document.onreadystatechange = function () {
        if (document.readyState == "complete") {
            var warn = document.createElement("div");
            warn.style.cssText = "position:fixed; z-index: 10000; top:" + (top_position || "0") + "px; right: 0px; background-color: rgba(255, 219, 51, 1); box-shadow: rgba(237, 3, 3, 0.156863) 0px -10px 10px inset; color: rgb(18, 18, 18); font-weight: 600; padding: 8px; border-bottom-left-radius: 20px; background-position: initial initial; background-repeat: initial initial;"; 
            warn.id = "pf-incompatible-warning";
            warn.innerHTML = message || 'Votre navigateur n\'est pas compatible avec PairForm. ';
            var warn_link = document.createElement("a");
            warn_link.href = PF.globals.url.root + "utils/compatibilite";
            warn_link.innerHTML = "Cliquez ici pour plus d\'information";
            warn.appendChild(warn_link);
            document.querySelector('body').appendChild(warn);
            // alert("Les paramètres de votre navigateur ne permettent pas d'utliser PairForm. Cliquez ici pour en savoir plus.");
        }
    }
}
PF.matches = function(el, selector) {
  el = el[0] || el;
  return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
}
PF.getMatchedSelector = function(el, selectors) {
    var selector_list = selectors.split(",");
    for (var i = 0; i < selector_list.length; i++) {
        if(PF.matches(el, selector_list[i]))
            return selector_list[i].trim();
    }
}

PF.getElement = function (query, parent){
    if (parent)
        query = parent + " " + query;
    return angular.element(document.querySelectorAll(query));
}
PF.init = function (){

    //S'il y a des OA definis sur la page, et qu'on a un id de page
    if ((($('[data-oauid]:not([data-oauid=""])').length != 0) || ($('meta[name="uid_page"]').not('[content=""]').length != 0))
        || !(PF.isScenari() || PF.isSpecialCase() || PF.isFromLatex()))
    {
        //On utilisera la version de base d'attachement de messages
        PF.globals.version = "1.5";
        PF.globals.styleAttacheMessages = "OA";
        PF.globals.uid_page = $('meta[name="uid_page"]').attr('content');
    }
    //S'il n'y a pas d'OA definis sur la page
    else
    {
        //On utilisera la version avance d'attachement des messages par identifiants uniques
        PF.globals.version =  "2";
        PF.globals.styleAttacheMessages = "AUTO";
        // var nom_page = window.location.pathname.split("/")[window.location.pathname.split("/").length - 1];
        //S'il n'y a pas de nom de page (cas d'un chemin finissant par "/"), on met "index"
        // PF.globals.nom_page = nom_page.split(".")[0] || "index";
    }
    var url_page_actuelle_sans_protocole = window.location.host + window.location.pathname,
        url_web_section_sans_protocole = PF.globals.capsule.url_web.replace(/https?:\/\//, "");

    PF.globals.nom_page =  url_page_actuelle_sans_protocole.replace(url_web_section_sans_protocole, "");


    if (PF.globals.id_res == ID_RES_TEST)
        PF.displayAlert("Attention, vous êtes dans un environnement de test ; les messages ont une durée d'expiration d'une heure.","info");

    //S'il n'y a pas de sélecteur déclaré
    if (!PF.globals.selector) {
        //On essaie de les deviner
        //Selecteur d'éléments
        if(PF.isScenari())
        {
            PF.globals.selector = ".mainContent_co p, .mainContent_co .resInFlow, .mainContent_ti";
            PF.globals.selectorRes = "#label-title";
            PF.globals.selectorPage = "none";
            PF.globals.selectorContent = ".mainContent_co";
        }
        else if(PF.isFromLatex()){
            PF.globals.selector = "#globalWrapper span";
            PF.globals.selectorRes = "#label-title";
            PF.globals.selectorPage = "#globalWrapper";
            PF.globals.selectorContent = "body";
        }
        else if(PF.isSpecialCase()){
            //On ne fait rien, les selecteurs sont déclarés dans la page
        }
        //Sinon, c'est qu'on est sur profeci
        else
        {
            PF.globals.selector = "*[data-oauid]:not(body)";
            // PF.globals.selector = ".feedback, .inner, div.object-properties, a.diagram_link, table.display, table.html-grid";
            PF.globals.selectorRes = "#label-title";
            PF.globals.selectorPage = ".inner > h2";
            PF.globals.selectorContent = "body";
        }
    }
    //De toute façon, les commentaires sont postés sur le titre dans la barre,
    // quel que soit le format
    PF.globals.selectorRes = "#label-title";
    PF.globals.selectorAll = PF.globals.selector + ", " + PF.globals.selectorPage + ", " + PF.globals.selectorRes;

    $(PF.globals.selectorAll).attr('pf-commentable', '');

    // $(document).on('click', PF.globals.selectorAll, function handlePFClick(event) {
    //  // PF.addFocusedItem($(this));
    //  Message.afficher($(this)); 
    //  event.stopPropagation();
    // });
    // PF._localStorage.array_messages_lus = "[]";

    //Pas d'analytics en dev
    if (PF.globals.url.root.match(/imedia.emn.fr/))
    {
        ga = function () {};
    }
    else{
        //PIWIK d'application
        window._paq = window._paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
          var u="//piwik.pairform.fr/";
          _paq.push(['setTrackerUrl', u+'piwik.php']);
          _paq.push(['setSiteId', '1']);
          var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
          g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        })();
    }

}

PF.normaliserURL = function (url) {
    //Merci à ce mec pour cette idée géniale : http://stackoverflow.com/a/14781678/1437016
    var link = document.createElement("a");
    link.href = url;
    var abs_url = link.host+link.pathname+link.search+link.hash;
    return abs_url.replace(PF.globals.capsule.url_web.replace(/https?:\/\//, ""), "");
}

PF.displayAlert = function () {}

PF.toObject = function (array) {
    var rv = {};
    for (var i = 0; i < array.length; ++i)
    {
        var id_post = array[i].id_message;
        rv[id_post] = array[i];
    }       
    return rv;
}
PF.pageIsMenu = function (url) {
    if(typeof(url) == "undefined")
        //Defaut : page courante    
        var url = window.location.pathname.split('/')[window.location.pathname.split('/').length -1 ];
    
    //On test s'il est de classe ueDiv.
    var result = $('.mnu li div[id="'+ url +'"].mnu_b');
    
    return result.length == 0 ? false : true;
}

PF.getController = function (nom_controller){
    var controller = document.querySelector('[ng-controller="'+ nom_controller +'"]');
    return typeof(controller) != "undefined" ? angular.element(controller) : null;
}

PF.getScopeOfController = function (nom_controller){
    var controller = PF.getController(nom_controller);
    return controller ? controller.scope() : null;
}
PF.getMainScope = function (){
    var controller = PF.getController('main_controller');
    return controller ? controller.scope() : null;
}
PF.redirigerVersMessage = function(id_message){
    window.location.replace(PF.globals.url.ws + "message/rediriger?id_message=" + id_message);
}
PF.redirigerVersCapsule = function (id_capsule){
    window.location.replace(PF.globals.url.ws + "capsule/rediriger?id_capsule=" + id_capsule);
}

// trouve un élement dans une liste via son id
PF.trouverElementListe = function (id, liste, clef_id) {
    for(var clef in liste) {
        if (id == liste[clef][clef_id]) {
            return liste[clef];
        }
    }
}