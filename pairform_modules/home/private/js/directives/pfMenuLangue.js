pf_app.directive('pfMenuLangue', function () {
	return {
		restrict: 'E',
		template: 	'<div class="comModifierLangue-container ui-tooltip-bottom">'+
						'<select ng-select="nouveau.code_langue" id="message-set-language-list">'+
							'<option value="1">English </option>'+
							'<option value="2">Español </option>'+
							'<option value="3">Français </option>'+
						'</select>'+
					'</div>'
	};
});