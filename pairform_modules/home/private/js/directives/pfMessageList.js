pf_app.directive('pfMessageList', function () {
	
	return {
		restrict: 'E',
		templateUrl:  PF.globals.url.views + 'partial/message_list',
		scope:{
			messages:'=messages',
			transversal:'=transversal',
			user:'=user',
			utilisateur_local:'=utilisateurLocal',
			limit:'=?limit'
		},
		link: function (scope, element, attr) {
			scope.defaultLimit = scope.limit || 5;
			scope.limit = scope.defaultLimit;
			// scope.init = function (message_updated) {
			// 	scope.messages_array = [{"contenu":"@admin Image, .doc, .ppt, .pdf et .odt depuis web","date_creation":1453285072,"date_modification":1454415091,"defi_valide":0,"est_defi":0,"est_lu":"0","geo_latitude":0,"geo_longitude":0,"id_auteur":8476,"id_capsule":40,"id_langue":3,"id_message":11468,"id_message_parent":11467,"id_role_auteur":1,"medaille":"","nom_auteur":"TY azer","nom_page":"co/_catalogue_1.html","nom_tag":".mainContent_co p","noms_auteurs_tags":"[]","num_occurence":6,"prive":0,"pseudo_auteur":"azerty","role_auteur":"Participant","somme_votes":1,"supprime_par":0,"tags":"[]","url_avatar_auteur":"http://imedia.emn.fr:3000/res/avatars/8476.png","utilisateur_a_vote":1,"uid_page":"_pf_res","uid_oa":"","pieces_jointes":[{"nom_original_pj":"CahierDesCharges.ppt","nom_serveur_pj":"823f010fcb593fd8ffdabd31e4c9e80b.ppt","nom_thumbnail":"b278f6d854cd408860c0cb7df15ac04b.png","extension_pj":"ppt","taille_pj":"100864"},{"nom_original_pj":"Annexe Technique 2014.doc","nom_serveur_pj":"c43d982831d7f8325240594356c64004.doc","nom_thumbnail":"2ad50fd1c0773a0fa46333bf83f4b9d4.png","extension_pj":"doc","taille_pj":"47104"},{"nom_original_pj":"Just - Structure.odt","nom_serveur_pj":"c72c3382d5a5c9e65edef74b40312d78.odt","nom_thumbnail":"d652a60372d78d67ffe90f4893183fd6.png","extension_pj":"odt","taille_pj":"10957"},{"nom_original_pj":"525036620.jpg","nom_serveur_pj":"e08314f7c44cfe9e968a622296db8b31.jpg","nom_thumbnail":"31207116f110687a0cf46c83156e8457.png","extension_pj":"jpg","taille_pj":"87583"},{"nom_original_pj":"C6 - Horaires du 25-08-14 au 10-07-15.pdf","nom_serveur_pj":"fdf4afb61547487a81d78a29884c303b.pdf","nom_thumbnail":"14496cd0ee9bbfc1593eb1bd40997123.png","extension_pj":"pdf","taille_pj":"1072321"}],"estGeolocalise":false},{"contenu":"Vidéo et photo depuis iOS après correction","date_creation":1453284918,"date_modification":1454414404,"defi_valide":0,"est_defi":0,"est_lu":"0","geo_latitude":0,"geo_longitude":0,"id_auteur":35,"id_capsule":40,"id_langue":3,"id_message":11467,"id_message_parent":0,"id_role_auteur":5,"medaille":"","nom_auteur":"Admin","nom_page":"co/_catalogue_1.html","nom_tag":".mainContent_co p","noms_auteurs_tags":"[]","num_occurence":6,"prive":0,"pseudo_auteur":"admin","role_auteur":"Administrateur","somme_votes":0,"supprime_par":0,"tags":"[]","url_avatar_auteur":"http://imedia.emn.fr:3000/res/avatars/35.png","utilisateur_a_vote":0,"uid_page":"_pf_res","uid_oa":"","pieces_jointes":[{"nom_original_pj":"pj-0.mp4","nom_serveur_pj":"3b2c80b4572c521e0ed0fbb5b8355066.mp4","nom_thumbnail":"299e4e1b6751325d2cbdddcca95e5716.mp4","extension_pj":"mp4","taille_pj":"1337702"},{"nom_original_pj":"pj-1.jpg","nom_serveur_pj":"ca1d3b5e518785d893be7aea93adb641.jpg","nom_thumbnail":"d20797e6c20cafb18e2265c7f56031c4.jpg","extension_pj":"jpg","taille_pj":"289630"}],"estGeolocalise":false}];
			// 	scope.transversal = true;
			// }
			// scope.init();
			scope.increaseLimit = function () {
				scope.limit += scope.defaultLimit;
			}

			element.on('scroll', function() {
		        if($(this).scrollTop() + $(this).innerHeight() + 20 >= $(this)[0].scrollHeight) {
		            scope.increaseLimit();
		        }
		    });

		}
	};
});