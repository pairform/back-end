pf_app.directive('draggable', ["$document", function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attr) {

			function handleDragStart(e) {
				//On empèche l'event de bubbler 
				//Sur un DnD d'un message, ca bubble au btn-bar
				e.stopPropagation();

                e.originalEvent.dataTransfer.effectAllowed = 'move';
                var data;
                
                //Si on déplace un seul message
                if (scope.message)
                	data = JSON.stringify(scope.message);
                //Sinon, on déplace tous les messages
                else
                	data = JSON.stringify(scope.array_messages_deplacement);

                e.originalEvent.dataTransfer.setData('text/javascript', data);
				// this/e.target is the source node.
				// this.addClassName('moving');
				scope.$root.$broadcast("pf:dragstart");
			};


			function handleDragEnd(e) {
				scope.$root.$broadcast("pf:dragend");
			};

			element.on('dragstart', handleDragStart);
			element.on('dragend', handleDragEnd);
		}
	}
}]);