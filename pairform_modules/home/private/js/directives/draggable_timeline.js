pf_app.directive('draggable-timeline', ["$document", function($document) {
	return {
		restrict: 'A',
		scope : {
			draggableCapsule:'='
		},
		link: function(scope, element, attr) {

			function handleDragStart(e) {
				//On empèche l'event de bubbler 
				//Sur un DnD d'un message, ca bubble au btn-bar
				e.stopPropagation();

                e.originalEvent.dataTransfer.effectAllowed = 'move';
                var data;
                
                //Si on déplace un seul message
                if (scope.draggableCapsule)
                	data = JSON.stringify(scope.draggableCapsule);
                else
                	console.log("Erreur : aucune donnée dans le draggable. Utiliser draggable-capsule='capsule' comme attribut")
                e.originalEvent.dataTransfer.setData('text/javascript', data);
				// this/e.target is the source node.
				// this.addClassName('moving');
				scope.$root.$broadcast("pf:dragstart");
				element[0].style.opacity = 0.5;
			};


			function handleDragEnd(e) {
				scope.$root.$broadcast("pf:dragend");
				element[0].style.opacity = 1;
			};

			element.on('dragstart', handleDragStart);
			element.on('dragend', handleDragEnd);
		}
	}
}]);