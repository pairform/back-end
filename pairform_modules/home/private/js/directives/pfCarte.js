/**
* Directive permettant d'afficher une carte. Elle fonctionne à partir d'évenement car les communicvation vont dans les deux sens : ctrl -> directive, directive -> ctrl
* Elle prend un paramètre : data. Celui-ci doit être de la forme :
* [{lat: 47.218371, lng: -1.553621, id: 11230} , ...}]
* Cette directive écoute deux événements : 
	- afficherPanneau qui permet de redessiner la carte après affichage du panneau la contenant
	- surClicDataPourCarte:data qui permet de recentrer la carte en fonction de la data passé en paramètre. Celle-ci doit être passé dans le tableau initiale
*/
pf_app.directive('pfCarte', ['OpenLayerLazyLoad', function(OpenLayerLazyLoad){
	return {
		restrict: 'E',
		trnasclude: false,
		scope: {
			'data': '=' //Data correspond à un tableau d'objet ; les objects contiennent un id les identifiants ainsi que leurs positions
		},
		link: function($scope, elem, attrs){
				//Données des marqueurs
			var data,
				//Calque qui va contenir tous les marqueurs
				coucheMarqueurs,
				//Carte
				map = false,
				//Référence vers l'image représentant un marqueur
				marqueurIcon = PF.globals.url.root + "private/img/marqueur_carte_actif.png",
				//CHoix de la projection utilisie pour encoder la position gps utilisé pour les messages
				//EPSG:4326 = Google Mercator
				mapProjection = "EPSG:4326",
				//Zoom qui sera utilisé pour toutes les modifications de vue de la carte
				zoomGeneral = 5,
				//Zoom qui sera utilisé pour afficher un marqueur scpécifique
				zoomExtend = 15,
				mapID = attrs.id,
				//Icone utilisé pour représenter un marqueur
				icon;

			//Si le controleur parent implémente la fonction pour intéragir sur le clique d'un marquer alors on récupère cette fonction
			if($scope.$parent.surClicMarqueur)
				$scope.surClicMarqueur = $scope.$parent.surClicMarqueur;
			
			/**
			* Fonction renvoyant un marqueur
			* data : object contenant deux variables : lng et lat
			* actionSurClique : callback sur la selection du marqueur
			*/
			function genererMarqueur(data, index, icon, actionSurClique){
				var marqueur = new OpenLayers.Marker(
					latLng(data.lat, data.lng), 
					icon);
				
				marqueur.data = data.id;

				if(actionSurClique){
					marqueur.events.register( 'click', marqueur, function( evt ) {
						actionSurClique(marqueur.data);
					});
				}

				coucheMarqueurs.addMarker(marqueur);
			}

			/**
			* Fonction retournant une position dans la projection de la carte
			* lat et lng sont la latitude et la longitude à transformer
			*/
			function latLng(lat, lng){
				return new OpenLayers.LonLat(lng, lat).transform(new OpenLayers.Projection(mapProjection), map.getProjectionObject());
			}

			/**
			* Fonction qui va initialiser la carte et afficher un marqueur pour chaque 
			* data présente dans le scope
			*/
			function initialise() {
				coucheMarqueurs = new OpenLayers.Layer.Markers("Marqueurs");

				//Récupération des cartes type aquaralle : http://maps.stamen.com/#watercolor
				map = new OpenLayers.Map({
					div: mapID,
					layers: [new OpenLayers.Layer.OSM("PairForm", [
						"http://a.tile.openstreetmap.fr/hot/${z}/${x}/${y}.png",
						"http://b.tile.openstreetmap.fr/hot/${z}/${x}/${y}.png"
						// "http://a.tile.stamen.com/watercolor/${z}/${x}/${y}.png",
						// "http://b.tile.stamen.com/watercolor/${z}/${x}/${y}.png",
						// "http://c.tile.stamen.com/watercolor/${z}/${x}/${y}.png",
						// "http://d.tile.stamen.com/watercolor/${z}/${x}/${y}.png"
						], {crossOrigin: null}), coucheMarqueurs],
					//Cette ligne est commenté car elle n'a pas permis de faire les transformations des positions des marqueurs automatiquement
					//D'ou la méthode : latLng
					//projection: new OpenLayers.Projection("EPSG:4326"),
					controls: [
						new OpenLayers.Control.Navigation({
							dragPanOptions: {
								enableKinetic: true
							}
						}),
						new OpenLayers.Control.Attribution(),
						new OpenLayers.Control.Zoom()
					],
					zoom: 1,
					center : [0, 0]
				});
				
				var size = new OpenLayers.Size(21, 25);
				var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
				icon = new OpenLayers.Icon(marqueurIcon,size,offset);        
				
				afficheData();
			}

			/**
			* Fonction centrant la carte sur la position de la data sélectionné.
			* message : l'objet data correspond
			*/
			function surDataChoisi(data){
				map.setCenter(latLng(data.lat, data.lng), zoomExtend);
			}

			/**
			* Fonction permettant d'ajouter toutes les données en tant que marqueurs sur la carte
			*
			*/
			function afficheData(){

				//Supprime tous les marqueurs affichés
				coucheMarqueurs.clearMarkers();
				//Si on a des données alors on les affichent en marqueur
				if(data && data.length > 0){
					//On centre la carte sur la première donnée
					//Donc celui à la position 0
					map.setCenter(latLng(data[0].lat, data[0].lng), zoomGeneral);
				
					for(var i = 0; i < data.length; i++){
						genererMarqueur(data[i], i, icon.clone(), $scope.surClicMarqueur);
					}
				}
			}

			//Appel au service pour qu'il charge les données d'OpenLayer
			OpenLayerLazyLoad.then(function() {
				initialise();
			}, function(){});

			/**
			* Evenement soulevé afin de réinitialiser la taille de la carte
			* 
			*/
			$scope.$parent.$on('reinitialiserTailleCarte', function (event, data) {
				map.updateSize();
				map.calculateBounds();
			});

			/**
			* Evenement soulevé dès qu'on choisi une donnée hors carte pour centraliser la carte sur le marqueur
			* marqueurData : donnée de la data qui est choisi hors carte (lat, lng et id)
			*/
			$scope.$parent.$on('surClicDataPourCarte', function(event, marqueurData){
				surDataChoisi(marqueurData);
			});

			/**
			* Evenement soulevé dès qu'une donnée doit être changer sur la carte
			* datas : les nouvelles données à afficher
			*/
			$scope.$parent.$on('modificationData', function(event, datas){
				data = datas;
				
				if(!map) {
					return;
				}

				afficheData();
			});
		}
	};
}]);