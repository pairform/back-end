typeof PF == "undefined" ? PF = {globals : {url : {} }} : null;
ga = function () {
	console.log(arguments);
}
//Global vars
// PF.globals.url.root = PF.globals.url.root || "http://localhost:3000/";

PF.globals.url.ws = PF.globals.url.root+"webServices/";
PF.globals.url.home = PF.globals.url.root + "home";
PF.globals.url.backoffice = PF.globals.url.home + "/backoffice";
PF.globals.url.views = PF.globals.url.root + "home/";

PF.globals.url.private = PF.globals.url.root + "private/";
PF.globals.url.public = PF.globals.url.root + "public/";
PF.globals.url.res = PF.globals.url.root + "res/";

/*
 *	Main Controller
 */


var pf_app = angular.module('pf_app',[
	"pascalprecht.translate"
	,"ngRaven"
	,"ngStorage"
	,"ngSanitize"
	,"ngAnimate"
	,"ngTouch"
	,"angular-carousel"
	,"toaster"
	,"ngCookies"
	,"angular-loading-bar"
	,"ngFileUpload"
	,"ngRoute"
	,"ngImgCrop"
	,"ui.thumbnail"
	,"backoffice"
	,"720kb.datepicker"
	,"moment-picker"
]);
