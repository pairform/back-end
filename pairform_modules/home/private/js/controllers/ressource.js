pf_app.controller('ressource', ["$scope", function ($scope) {
	var defaults = {
		"id_ressource" : null,
		"loaded" : null,
		"panneau_affiche" : 2,							//Panneau affiché par défaut
		"objectif_apprentissage_affiche" : 1,			//Objectif affiché par défaut
		"profil_apprentissage_affiche" : 0,			//Profil caché par défaut
		"objectif_apprentissage_edition" : 0,			//Mode edition off
		"ressource" :{
			"nom_court" : "Chargement...",
			"nom_long" : "Chargement...",
			"etablissement" : "Chargement...",
			"url_logo" : PF.globals.url.root + "public/img/logo_defaut.png",
			"theme" : "Chargement...",
			"auteurs" : "Chargement...",
			"licence" : "Chargement...",
			"date_creation" : "Chargement...",
			"date_modification" : "Chargement...",
			"description" : "Chargement...",
			"capsules" : [
			],
			"objectifs_apprentissage" : {
				"liste_objectifs_apprentissage": [
				],
				"nb_objectifs_valides": 0
			}
		}
	};

	angular.extend($scope, defaults);

	$scope.init = function () {
		//Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
		if ($scope.loaded != $scope.id_ressource) {
			var _scope = $scope;
			PF.get('ressource', {"id_ressource": $scope.id_ressource, "id_utilisateur": $scope.utilisateur_local.id_utilisateur} , function(data, status, headers, config) {
				if (data['status'] == "ok") {
					data.ressource.url_logo = PF.globals.url.root + data.ressource.url_logo;
					
					angular.extend(_scope, data);
					_scope.loaded = _scope.id_ressource;
				}
				
			});
		}
	}
	
	//Update du nombre d'objectif d'apprentissage validés
	$scope.$watch('ressource.objectifs_apprentissage.liste_objectifs_apprentissage', function (liste_objectifs_apprentissage_updated, old_value) {
		var count = 0;
		for (var i = liste_objectifs_apprentissage_updated.length - 1; i >= 0; i--) {
			count += +liste_objectifs_apprentissage_updated[i].est_valide;
		}
		$scope.ressource.objectifs_apprentissage.nb_objectifs_valides = count;
	},true);

	// $scope.panneau_affiche = 1;
	
	
	$scope.objectif_apprentissage_affiche = 1;
	
	$scope.objectif_apprentissage_edition = 0;

	$scope.ajouterObjectif = function () {
		PF.put('objectif', {"id_ressource": $scope.id_ressource, "nom" : ""} , function(data, status, headers, config) {
			if (data['status'] == "ok") {
				$scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.push({"id_objectif" : data['id_objectif'], "nom" : "", "est_valide" : 0});
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					toaster.pop("error", "ws_erreur_contacter_pairform");
			}
			
		});
	}

	$scope.updateObjectif = function (objectif) {
		var id_objectif = objectif.id_objectif,
			nom = objectif.nom,
			est_valide = objectif.est_valide;

		PF.post('objectif', {"id_objectif": id_objectif, "nom" : nom, "est_valide" : est_valide} , function(data, status, headers, config) {
			if (data['status'] == "ok") {
				//Cast de est_valide en -1 / 1 por false / true
				// var new_count = -1 + 2* (+est_valide);
				//Changement du décompte d'objectifs d'apprentissage valides 
				// $scope.ressource.objectifs_apprentissage.nb_objectifs_valides += new_count;
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					toaster.pop("error", "ws_erreur_contacter_pairform");
			}
			
		});
	}

	$scope.enleverObjectif = function (objectif) {
		PF.delete('objectif', {"id_objectif" : objectif.id_objectif} , function(data, status, headers, config) {
			if (data['status'] == "ok") {
				var index_objectif = $scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.indexOf(objectif);
				$scope.ressource.objectifs_apprentissage.liste_objectifs_apprentissage.splice(index_objectif,1);

				//Cast de est_valide en -1 / 1 por false / true
				// var new_count = -1 + 2* (+objectif.est_valide);
				//Changement du décompte d'objectifs d'apprentissage valides 
				// $scope.ressource.objectifs_apprentissage.nb_objectifs_valides -= new_count;
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					toaster.pop("error", "ws_erreur_contacter_pairform");
			}
			
		});
	}
	$scope.editerObjectifs = function () {
		//Mode edition off / on
		$scope.objectif_apprentissage_edition = +!$scope.objectif_apprentissage_edition;
	}

	$scope.updateProfilQuestion = function (question) {
		var id_question = question.id_question,
			id_reponse_valide = question.id_reponse_valide,
			param = {"id_utilisateur" : $scope.utilisateur_local.id_utilisateur, 
				"liste_reponses_valides" : [
					{
						"id_question": id_question,
						"id_reponse_valide" : id_reponse_valide
					}
				]
			};

		PF.post('profil', param , function(data, status, headers, config) {
			if (data['status'] == "ok") {
				//Rien a faire
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					toaster.pop("error", "ws_erreur_contacter_pairform");
			}
			
		});
	}
	$scope.afficherSousSection = function (index) {
		$scope.panneau_affiche = index;
	}
	$scope.changerAffichageObjectif = function () {
		$scope.objectif_apprentissage_affiche = +!$scope.objectif_apprentissage_affiche;
	}
	$scope.changerAffichageProfil = function () {
		$scope.profil_apprentissage_affiche = +!$scope.profil_apprentissage_affiche;
	}
}]);