/*
 * Controleurs du tableau de bord
 */

// Page d'accueil du backoffice
pf_app.controller('backoffice', ["$scope", "toaster", "$localStorage", "$routeParams",  "$translate", "$location", "connexion", function ($scope, toaster, $localStorage, $routeParams, $translate, $location, connexion) {
	var defaults = {
		selection : {},	
		formats_noms_icones : [
			"doc_pf.png",
			"doc_scenari.png",
			"doc_chainedit.png",
			"doc_injecteur.png",
			"doc_storyline2.png",
			"doc_etherpad.png",
			"doc_storyline3.png"
		]
	};
	angular.extend($scope, defaults);

	var succes_suppression, succes_deplacement, erreur_requete, attention_modification_url_web_capsule;
	var supprimer_espace, supprimer_ressource, supprimer_capsule, supprimer_messages_capsule;
	var capsule_a_deplacer, parametres_requete = {};
	$scope.$root.fenetre_selection_ressource = false;	
	$scope.defaultLimit = 10;
	$scope.limit = $scope.defaultLimit;

	// traduction des messages d'informations (erreurs, succès, etc.)
	$translate('LABEL.SUCCES.SUPPRESSION').then(function (traduction) {
		succes_suppression = traduction;
	});
	$translate('LABEL.SUCCES.DEPLACEMENT').then(function (traduction) {
		succes_deplacement = traduction;
	});
	$translate('LABEL.ERREUR.DISFONCTIONNEMENT').then(function (traduction) {
		erreur_requete = traduction;
	});
	$translate('LABEL.ESPACE.SUPPRIMER').then(function (traduction) {
		supprimer_espace = traduction;
	});
	$translate('LABEL.RESSOURCE.SUPPRIMER').then(function (traduction) {
		supprimer_ressource = traduction;
	});
	$translate('LABEL.CAPSULE.SUPPRIMER').then(function (traduction) {
		supprimer_capsule = traduction;
	});
	$translate('LABEL.CAPSULE.SUPPRIMER_MESSAGES_DE').then(function (traduction) {
		supprimer_messages_capsule = traduction;
	});
	$translate('LABEL.CAPSULE.MODIFICATION_URL_WEB_CAPSULE').then(function (traduction) {
		attention_modification_url_web_capsule = traduction;
	});

	// initialisation des boutons de sélection des espaces / ressources / capsules
	$translate('LABEL.ESPACE.SELECTIONNER').then(function (traduction) {
		$scope.nom_espace_selectionne = traduction;
		$scope.nom_espace_selectionne_defaut = traduction;
	});
	$translate('LABEL.RESSOURCE.SELECTIONNER').then(function (traduction) {
		$scope.nom_ressource_selectionne = traduction;
		$scope.nom_ressource_selectionne_defaut = traduction;
	});
	$translate('LABEL.CAPSULE.SELECTIONNER').then(function (traduction) {
		$scope.nom_capsule_selectionne = traduction;
		$scope.nom_capsule_selectionne_defaut = traduction;
	});

	var utilisateur_connecte = connexion.getUtilisateurConnecte();
	// si l'utilisateur connecté est admin 
	$scope.est_admin_pairform = utilisateur_connecte.est_admin_pairform;

	$scope.arborescence = {};

	$scope.init = function () {
		// récupération des informations à afficher sur la page
		PF.get("infosTableauDeBord", {}, function(data){
			if (data.status === "ok") {
				$scope.liste_espaces = data.liste_espaces; 
				$scope.liste_ressources = data.liste_ressources;    	
				$scope.liste_capsules = data.liste_capsules;
				
				// Si l'utilisateur arrive sur la tableau de bord via un clic sur un élément de l'arborescence Espace > Ressource > Capsule, on pré-sélectionne les bons éléments
				// SI un espace doit être pré-selectionné
				if ($routeParams.id_espace) {
					$scope.selection.espace = PF.trouverElementListe($routeParams.id_espace, $scope.liste_espaces, "id_espace");
					$scope.nom_espace_selectionne = $scope.selection.espace.nom_long;
				}
				// SI une ressource doit être pré-selectionnée
				if ($routeParams.id_ressource) {
					$scope.selection.ressource = PF.trouverElementListe($routeParams.id_ressource, $scope.liste_ressources, "id_ressource");
					$scope.nom_ressource_selectionne = $scope.selection.ressource.nom_long;
					// Mise à jour des informations sur l'arborescence espace / ressource / capsule - l'arborescence est affiché en haut de chaque écran (sauf dans le tableau de bord)
					$scope.arborescence.nom_espace = PF.trouverElementListe($scope.selection.ressource.id_espace, $scope.liste_espaces, "id_espace").nom_long;
					$scope.arborescence.url_espace = PF.trouverElementListe($scope.selection.ressource.id_espace, $scope.liste_espaces, "id_espace").url;
				}
				// SI une capsule doit être pré-selectionnée
				if ($routeParams.id_capsule) {
					$scope.selection.capsule = PF.trouverElementListe($routeParams.id_capsule, $scope.liste_capsules, "id_capsule");
					$scope.nom_capsule_selectionne = $scope.selection.capsule.nom_long;
					// Mise à jour des informations sur l'arborescence espace / ressource / capsule - l'arborescence est affiché en haut de chaque écran (sauf dans le tableau de bord)
					var arborescence_ressource = PF.trouverElementListe($scope.selection.capsule.id_ressource, $scope.liste_ressources, "id_ressource");
					$scope.arborescence.nom_ressource = arborescence_ressource.nom_long;
					$scope.arborescence.id_espace = arborescence_ressource.id_espace;
					$scope.arborescence.nom_espace = PF.trouverElementListe(arborescence_ressource.id_espace, $scope.liste_espaces, "id_espace").nom_long;
					$scope.arborescence.url_espace = PF.trouverElementListe(arborescence_ressource.id_espace, $scope.liste_espaces, "id_espace").url;

					// si l'url web de la capsule a changé, on informe l'utilisateur
					if ($routeParams.ancienne_url_web_capsule && $routeParams.ancienne_url_web_capsule != $scope.selection.capsule.url_web)
						toaster.pop("info", attention_modification_url_web_capsule + $scope.selection.capsule.url_web);
				}
			} else {
				if (data.message == "ws_utilisateur_non_autorise")
					toaster.pop("warning", "Vous ne faites parti d'aucun espace", "Vous ne pouvez donc pas créer de documents. Demandez un accès à l'administrateur de votre organisation.");
				else
					toaster.pop("error", data.message);
			}
		}, function(){
			console.log("erreur requete http get /webServices/infosTableauDeBord");
			toaster.pop("error", erreur_requete);
		});

	}
	$scope.init();


	// selectionne l'espace choisi par l'utilisateur
	$scope.selectionnerEspace = function (espace) {
		if (!espace) {
			$scope.selection = {};

			delete $scope.nom_espace_selectionne;
			$location.search('id_espace', null);
			$location.search('id_ressource', null);
		}
		else {
			$scope.selection = { 
				espace : espace
			};

			$scope.nom_espace_selectionne = espace.nom_long;
			$location.search('id_espace', espace.id_espace);
		}
	}

	// selectionne la ressource choisi par l'utilisateur
	$scope.selectionnerRessource = function (ressource) {
		$scope.selection.ressource = ressource;
		$scope.nom_ressource_selectionne = ressource.nom_long;
		// Mise à jour des informations sur l'arborescence espace / ressource / capsule - l'arborescence est affiché en haut de chaque écran (sauf dans le tableau de bord
		var espace = PF.trouverElementListe(ressource.id_espace, $scope.liste_espaces, "id_espace");
		$scope.selection.espace = espace;
		$scope.arborescence.nom_espace = espace.nom_long;
		$scope.arborescence.url_espace = espace.url;

		$location.search('id_espace', espace.id_espace);
		$location.search('id_ressource', ressource.id_ressource);
	}

	// selectionne la capsule choisi par l'utilisateur
	$scope.selectionnerCapsule = function (capsule) {
		$scope.selection.capsule = capsule;
		$scope.nom_capsule_selectionne = capsule.nom_long;
		// Mise à jour des informations sur l'arborescence espace / ressource / capsule - l'arborescence est affiché en haut de chaque écran (sauf dans le tableau de bord)
		var arborescence_ressource = PF.trouverElementListe(capsule.id_ressource, $scope.liste_ressources, "id_ressource");
		$scope.arborescence.nom_ressource = arborescence_ressource.nom_long;
		$scope.arborescence.id_espace = arborescence_ressource.id_espace;
		var espace = PF.trouverElementListe(arborescence_ressource.id_espace, $scope.liste_espaces, "id_espace");
		$scope.arborescence.nom_espace = espace.nom_long;
		$scope.arborescence.url_espace = espace.url;
	}

	// supprime l'espace séléctionné par l'utilisateur
	$scope.supprimerEspace = function (espace) {
		// popup de confirmation de l'action
		if( confirm(supprimer_espace +" "+ espace.nom_long +" ?") ) {
			// suppression en BDD
			PF.delete("espace/"+ espace.id_espace, {}, function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						// on recharge l'écran
						$location.path(URL_BACKOFFICE).search({});
						toaster.pop("success", succes_suppression);
					} else {
						toaster.pop("error", erreur_requete);
					}
				}, function(){
					console.log("erreur requete http delete /webServices/espace/:id_espace");
					toaster.pop("error", erreur_requete);
				});
		}
	}

	// supprime la ressource séléctionnée par l'utilisateur
	$scope.supprimerRessource = function (ressource, id_espace) {
		// popup de confirmation de l'action
		if( confirm(supprimer_ressource +" "+ ressource.nom_long +" ?") ) {
			// suppression en BDD
			PF.delete("ressource/"+ ressource.id_ressource +"/"+ ressource.cree_par +"/espace/"+ id_espace, {}, function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						// on recharge l'écran
						$location.path(URL_BACKOFFICE).search({});
						toaster.pop("success", succes_suppression);
					} else {
						toaster.pop("error", erreur_requete);
					}
				}, function(){
					console.log("erreur requete http delete /webServices/ressource/:id_ressource/espace/:id_espace");
					toaster.pop("error", erreur_requete);
				});
		}
	}

	// supprime la capsule séléctionnée par l'utilisateur
	$scope.supprimerCapsule = function (capsule, id_ressource, id_espace) {
		// popup de confirmation de l'action
		if( confirm(supprimer_capsule +" "+ capsule.nom_long +" ?") ) {
			// suppression en BDD
			PF.delete("capsule/"+ capsule.id_capsule +"/"+ capsule.cree_par +"/ressource/"+ id_ressource +"/espace/"+ id_espace, {}, function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						// on recharge l'écran
						$location.path(URL_BACKOFFICE).search({});
						toaster.pop("success", succes_suppression);
					} else {
						toaster.pop("error", erreur_requete);
					}
				}, function(){
					console.log("erreur requete http delete /webServices/capsule/:id_capsule/ressource/:id_ressource/espace/:id_espace");
					toaster.pop("error", erreur_requete);
				});
		}
	}

	// supprime les messages de la capsule séléctionnée par l'utilisateur
	$scope.supprimerMessagesCapsule = function (capsule, id_ressource, id_espace) {
		// popup de confirmation de l'action
		if( confirm(supprimer_messages_capsule +" "+ capsule.nom_long +" ?") ) {
			// suppression en BDD
			PF.delete("messages/capsule/"+ capsule.id_capsule +"/"+ capsule.cree_par +"/ressource/"+ id_ressource +"/espace/"+ id_espace, {}, function(retour_json){
					if (retour_json.status === RETOUR_JSON_OK) {
						toaster.pop("success", succes_suppression);
					} else {
						toaster.pop("error", erreur_requete);
					}
				}, function(){
					console.log("erreur requete http delete /webServices/messages/capsule/:id_capsule/:cree_par/ressource/:id_ressource/espace/:id_espace");
					toaster.pop("error", erreur_requete);
				});
		}
	}

	// déplace la capsule séléctionnée vers une nouvelle ressource séléctionnée par l'utilisateur
	$scope.deplacerCapsule = function (id_nouvelle_ressource) {
		parametres_requete = capsule_a_deplacer;
		parametres_requete.id_ressource = id_nouvelle_ressource;				// cette ligne a aussi pour effet (point de vue utilisateur) de déplacer visuellement la capsule 
		parametres_requete.id_espace = $scope.selection.espace.id_espace;

		// déplacement en BDD
		PF.post("capsule/deplacer", parametres_requete, function(retour_json){
				if (retour_json.status === RETOUR_JSON_OK) {
					capsule_a_deplacer.nom_long = retour_json.nom_long;

					toaster.pop("success", succes_deplacement);
				} else {
					toaster.pop("error", erreur_requete);
				}
			}, function(){
				console.log("erreur requete http post /webServices/capsule/deplacer");
				toaster.pop("error", erreur_requete);
			});

		$scope.$root.fenetre_selection_ressource = false;
	}

	// affiche la fenêtre permettant de choisir la ressource dans laquelle déplacer une capsule
	$scope.afficherFenetreSelectionRessource = function (capsule) {
		$scope.$root.fenetre_selection_ressource = true;
		capsule_a_deplacer = capsule;
	}

	$scope.increaseLimit = function () {
		$scope.limit += $scope.defaultLimit;
	}


	/*
	 * Gestion de la visibilité des différents boutons du tableau de bord en fonction des droits de l'utilisateur connecté
	 * ATTENTION : reporter les changements dans controleursVisibiliteCapsules.js + controleursCapsules.js + controleursRolesEspace.js + controleursRolesRessource.js + controleursOpenBadges.js  
	 */
	$scope.aAccesBoutonEditerEspace = function () {
		if ($scope.selection.espace) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][EDITER_ESPACE];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonCreerRessource = function () {
		if ($scope.selection.espace) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_RESSOURCES_ET_CAPSULES];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonGererGroupes = function () {
		if ($scope.selection.espace) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_GROUPES];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonGererRolesEspace = function () {
		if ($scope.selection.espace) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_ROLES_ESPACE]
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonEditerRessource = function () {
		if ($scope.selection.espace && $scope.selection.ressource) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][EDITER_RESSOURCE];
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonCreerCapsule = function () {
		if ($scope.selection.espace && $scope.selection.ressource) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_CAPSULE];
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonGererRolesRessource = function () {
		if ($scope.selection.espace && $scope.selection.ressource) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_ROLES_RESSOURCE];
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonSupprimerRessource = function () {
		if ($scope.selection.espace && $scope.selection.ressource) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][SUPPRIMER_RESSOURCE];
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonAfficherStatistiquesRessource = function () {
		if ($scope.selection.espace && $scope.selection.ressource) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][AFFICHER_STATISTIQUES];
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonGererOpenBadgesRessource = function () {
		if ($scope.selection.espace && $scope.selection.ressource) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_OPEN_BADGE];
			} catch(e){}
		} else {
			return false;
		}
	}

	$scope.aAccesTimeline = function () {
		if ($scope.selection.espace && $scope.selection.ressource) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES];
			} catch(e){}
		} else {
			return false;
		}
	}

	$scope.aAccesBoutonEditerCapsule = function (capsule) {
		var capsule = $scope.selection.capsule || capsule;
		if ($scope.selection.espace && $scope.selection.ressource && capsule) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_TOUTES_CAPSULES] ||
					( utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_CAPSULE] && capsule.cree_par == utilisateur_connecte.id_utilisateur );
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonDeplacerCapsule = function () {
		if ($scope.selection.espace) {
			return utilisateur_connecte.est_admin_pairform || utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][DEPLACER_CAPSULE];
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonSupprimerCapsule = function (capsule) {
		var capsule = $scope.selection.capsule || capsule;
		if ($scope.selection.espace && $scope.selection.ressource && capsule) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_TOUTES_CAPSULES] ||
					( utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_CAPSULE] && capsule.cree_par == utilisateur_connecte.id_utilisateur );
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonSupprimerMessagesCapsule = function (capsule) {
		var capsule = $scope.selection.capsule || capsule;
		if ($scope.selection.espace && $scope.selection.ressource && capsule) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][SUPPRIMER_MESSAGES_TOUTES_CAPSULES] ||
					( utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][SUPPRIMER_MESSAGES] && capsule.cree_par == utilisateur_connecte.id_utilisateur );
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesBoutonGererVisibiliteCapsule = function (capsule) {
		var capsule = $scope.selection.capsule || capsule;
		if ($scope.selection.espace && $scope.selection.ressource && capsule) {
			try {
				return utilisateur_connecte.est_admin_pairform ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][GERER_VISIBILITE_TOUTES_CAPSULES] ||
					utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_VISIBILITE_TOUTES_CAPSULES_RESSOURCE] ||
					( utilisateur_connecte.liste_roles[$scope.selection.espace.id_espace][$scope.selection.ressource.id_ressource][GERER_VISIBILITE] && capsule.cree_par == utilisateur_connecte.id_utilisateur );
			} catch(e){}
		} else {
			return false;
		}
	}
	$scope.aAccesRessource = function (ressource, index, array) {
		//Soit l'utilisateur a un rôle, soit c'est l'admin
		if(utilisateur_connecte.est_admin_pairform) {
			return true;
		}
		else if(typeof utilisateur_connecte.liste_roles != "undefined"){
			if(typeof utilisateur_connecte.liste_roles[ressource.id_espace] != "undefined"){
				if (utilisateur_connecte.liste_roles[ressource.id_espace][GERER_TOUTES_RESSOURCES_ET_CAPSULES]) {
					return true;
				}
				else if (typeof utilisateur_connecte.liste_roles[ressource.id_espace][ressource.id_ressource] != "undefined") {
					return true;
				}
			}
		}
		return false;	
	}
}]);


pf_app.controller('backoffice_espace', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
	var defaults = {};
	angular.extend($scope, defaults);

	$scope.init = function () {}
}]);

pf_app.controller('backoffice_items', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
	var defaults = {};
	angular.extend($scope, defaults);

	$scope.init = function () {}
}]);
