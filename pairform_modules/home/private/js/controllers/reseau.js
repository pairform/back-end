pf_app.controller('reseau', ["$scope", "toaster", function ($scope, toaster) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"mode" : "gestion", //ajout || usage
		"sous_mode" : false,
		"cercles":[],
		"classes":[],
		"classesRejointes":[],
		"utilisateur_ajout" : undefined,
		"reseau_selected" : undefined
	};

	angular.extend($scope, defaults);

	var resParDomaine = {};
	var resParEtablissement = {};

	$scope.init = function () {
		if ($scope.loaded != $scope.id_utilisateur) {
			var _scope = $scope;
			PF.get('reseau/liste', {}, function(data, status, headers, config) {
				var retour = data;	
				if (retour["status"] == "ok") {
					_scope.loaded = _scope.id_utilisateur;
					_scope.cercles = retour['cercles'];
					_scope.classes = retour['classes'];
					_scope.classesRejointes = retour['classesRejointes'];
				}
				else{
					if (retour['message'] == "ws_utilisateur_invalide") {
						$scope.deconnecter("ws_utilisateur_invalide");
					}
				}
			});
		}
	}
	$scope.init();
	$scope.ajouterReseau = function (type) {
		if (!$scope.utilisateur_local.est_connecte)
			return;

		//Récupération du nom de la collection
		var name = $scope.nouveau_reseau;

		//Si champ vide
		if ($.trim(name) == ''){
			toaster.pop('error', 'web_label_erreur_renseigner_nom');
			return;
		}

		var post = {'nom' : name};

		var message = 'web_label_votre ' + type + ' web_label_creee_avec_succes';
		var ws = '',
			post,
			nouveau_reseau,
			array_reseaux;

		if (type == "classe"){
			nouveau_reseau = new Classe({'nom' : name});
			array_reseaux = $scope.classes;
			ws = 'reseau/classe';
			var estExpert = false;
			for(var i in $scope.utilisateur_local.rank)
			{
				if($scope.utilisateur_local.rank[i].id_categorie >= 4){
					estExpert = true;
					break;
			    }
			}    
			//Si on est pas au moins expert
			if (!estExpert){
				//On saute
				toaster.pop('error', 'web_label_erreur_role_creer_classe');
				return;
			}
		}
		else if (type == "cercle"){
			nouveau_reseau = new Reseau({'nom' : name});
			array_reseaux = $scope.cercles;
			ws = 'reseau/cercle';
		}
		//Si on est dans le cas d'un élève qui rejoint une classe
		else if (type == "classeRejoindre"){
			nouveau_reseau = new Classe({'nom' : name});
			array_reseaux = $scope.classesRejointes;
			//On change l'intitulé du paramètre et le WS
			post.cle = name;
			message = 'web_label_classe_rejointe';
			ws = 'reseau/classe/utilisateur';
		}
		//Ajout dans le tableau
		array_reseaux.splice(0,0,nouveau_reseau);
		//Notif
		// toaster.pop('success', message);

		ga('send', 'event', 'user', 'create_network', $scope.utilisateur_local.id_utilisateur);
		//Envoi de la requête d'ajout
		PF.put(ws, post, function (retour) {
			//Ajout validé
			if (retour['status'] == 'ok'){
				//Update de l'affichage & notification
				nouveau_reseau.id_collection = retour['id_collection'];
				//Si on est dans le cas d'une classe
				if(type != "cercle"){
					//Ajout de la clé
					nouveau_reseau.cle = retour['cle'];
				}
			}
			//Ajout refusé
			else
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					//Display de l'erreur correspondante
					toaster.pop('error', retour['message']);

				//Suppression dans le tableau
				array_reseaux.splice(0,1);
			}
		});
	}
	$scope.afficherFormReseau = function (type_reseau) {
		$scope.sous_mode = type_reseau;
		
	}
	$scope.utiliserReseau = function (reseau) {
		PF.getScopeOfController('controller_messages').ajouterReseauVisibilite(reseau);
		
	}
	$scope.ajouterUtilisateurDansReseauDepuisReseau = function (reseau) {
		if ($scope.utilisateur_ajout)
			$scope.ajouterUtilisateurDansReseau(reseau, $scope.utilisateur_ajout);
		else
			toaster.pop("error", "Pas d'utilisateur sélectionné");

	}
	$scope.ajouterUtilisateurDansReseauDepuisRecherche = function (utilisateur) {
		if ($scope.reseau_selected)
			$scope.ajouterUtilisateurDansReseau($scope.reseau_selected, utilisateur);
		else
			toaster.pop("error", 'Pas de réseau sélectionné');
	}
	$scope.ajouterUtilisateurDansReseau = function (reseau, utilisateur) {
		//S'il est déjà dedans, on coupe court
		if (reseau.profils.map(function(e){return e.id_utilisateur.toString();}).indexOf(utilisateur.id_utilisateur) >= 0) {
			//Display de l'erreur correspondante
			return toaster.pop('error', 'ws_erreur_ajout_reseau_deja_existant');

		}
		//Envoi de la requête d'ajout
		var post = {'id_collection' : reseau.id_collection, 'id_utilisateur_concerne' : utilisateur.id_utilisateur};
		reseau.profils.splice(0,0,utilisateur);
		ga('send', 'event', 'user', 'add_to_network', $scope.utilisateur_local.id_utilisateur);

		PF.put('reseau/utilisateur', post,function (retour) {		
			if (retour['status'] == 'ok'){
				//Update de l'affichage & notification
				toaster.pop('success', 'web_label_utilisateur_ajouté_avec_succes');

			}
			//Ajout refusé
			else
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					//Display de l'erreur correspondante
					toaster.pop('error', retour['message']);

				reseau.profils.splice(0,1);
			}
		});
	}
	$scope.selectionUtilisateurPourReseau = function (reseau) {
		// $scope.afficherRecherche('reseau');
		$scope.$root.select_utilisateur = true;
		$scope.reseau_selected = reseau;

		PF.getScopeOfController('utilisateurs').mode = "reseau";

	}
	$scope.supprimerUtilisateurReseau = function (utilisateur, reseau) {
		var post = {'id_collection' : reseau.id_collection, 'id_utilisateur_concerne' : utilisateur.id_utilisateur};
		var index = reseau.profils.indexOf(utilisateur);
		reseau.profils.splice(index, 1);

		PF.delete('reseau/utilisateur', post,function (retour) {		
			if (retour['status'] == 'ok'){
				//Update de l'affichage & notification
				toaster.pop('success', 'web_label_utilisateur_sup_reseau');
				
			}
			//Ajout refusé
			else
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					//Display de l'erreur correspondante
					toaster.pop('error', retour['message']);

				reseau.profils.splice(index,0,reseau);
			}
		});
	}
	$scope.supprimerReseau = function (reseau, array_reseaux) {
		var post = {'id_collection': reseau.id_collection};
		var index = array_reseaux.indexOf(reseau);
		array_reseaux.splice(index, 1);

		PF.delete('reseau', post,function (retour) {		
			if (retour['status'] == 'ok'){
				//Update de l'affichage & notification
				toaster.pop('success', 'web_label_sup_reseau');
			}
			//Ajout refusé
			else
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
				else
					//Display de l'erreur correspondante
					toaster.pop('error', retour['message']);

				array_reseaux.splice(index,0,reseau);
			}
		});
	}
}]);
