pf_app.controller('messages', ["$scope", "$localStorage", "$sessionStorage", "$timeout", "toaster", "$routeParams", "$filter", function ($scope, $localStorage, $sessionStorage, $timeout, toaster, $routeParams, $filter) {
	var defaults = {
		"utilisateur" : {
			id_utilisateur : $routeParams.id_utilisateur == "moi" ? $scope.utilisateur_local.id_utilisateur : parseInt($routeParams.id_utilisateur)
		},
		defaultLimit : 2,
		limit : 6
	};

	angular.extend($scope, defaults);

	$scope.init = function () {
		if (!$scope.utilisateur.id_utilisateur)
			$scope.getMessages();
		else
			$scope.getMessagesUtilisateur();

		$scope.transversal = true;
	}

	$scope.getMessages = function () {
		PF.get('message/transversal', {}, function (data, status, headers, config){
			   
			var retour = data;
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				$scope.messages_par_capsules = $filter('orderBy')(retour["data"], 'nom_long_ressource');
			}
			else{
				console.log(retour['message']);
			}
		});
	}

	$scope.getMessagesUtilisateur = function () {
		PF.get('message/transversal/utilisateur', {"id_utilisateur" : $scope.utilisateur.id_utilisateur}, function (data, status, headers, config){
			   
			var retour = data;
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				$scope.messages_par_capsules = $filter('orderBy')(retour["data"], 'nom_long_ressource');
			}
			else{
				console.log(retour['message']);
			}
		});
	}
	$scope.increaseLimit = function () {

		if($scope.limit < $scope.messages_par_capsules.length){
			$scope.$apply(function(){
				$scope.limit += $scope.defaultLimit;
			})
		}
	}
	$scope.init();

    var scrollTimer;

    $('.messages-control').hover(function () {
      var timeline_container = $('.last-messages-container .messages-wrapper')[0];
      var timeline_control = $(this);
      scrollTimer = window.setInterval(function () {
        if(timeline_control.hasClass('left'))
          timeline_container.scrollLeft -= 5;
        else
          timeline_container.scrollLeft += 5;
      }, 1);
      },function() {
        window.clearInterval(scrollTimer);
    });
    $('.last-messages-container .messages-wrapper').scroll(function () {
      var offsetX = (this.scrollLeft) * 100 / (this.scrollWidth -this.clientWidth);

      if (offsetX >= 80){
      	$scope.increaseLimit();
      }
      if (offsetX >= 99)
        $('.right.messages-control').addClass('control-hidden');
      else if(offsetX <= 1)
        $('.left.messages-control').addClass('control-hidden');
      else{
        $('.messages-control').removeClass('control-hidden');
      }
    })

}]);