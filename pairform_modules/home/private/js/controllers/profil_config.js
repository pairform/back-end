pf_app.controller('profil_config', ["$scope", "toaster", "$localStorage", "$translate", function ($scope, toaster, $localStorage, $translate) {
	var defaults = {
		"visible" : false,
		"tableau_langues_label" : PF.langue.tableau_labels,
		"utilisateur" : $localStorage.utilisateur_local,
		"informations" : {
			"current_password" : "",
			"password" : "",
			"password2" : "",
			"name" : "",
			"etablissement" : "",
			"langue" : "3"
		}
	};
	angular.extend($scope, defaults);
	var LABEL_RGPD_DELETE_CONFIRM = "";

	$scope.init = function () {
		$scope.informations.name = $scope.utilisateur.name;
		$scope.informations.etablissement = $scope.utilisateur.etablissement;
		$scope.informations.langue = $scope.utilisateur.id_langue_principale;

		// traduction du message de confirmation pour *confirm*
		$translate('LABEL_RGPD_DELETE_CONFIRM').then(function (traduction) {
			LABEL_RGPD_DELETE_CONFIRM = traduction;
		});
	}

	$scope.enregistrerModifications = function ()  {
		var _scope = $scope;

		//Si un des champs est rempli
		if(typeof($scope.informations.current_password) != "undefined" 
			|| typeof($scope.informations.password) != "undefined" 
			|| typeof($scope.informations.password2) != "undefined")
		{
			//Il faut absolument que tous les champs soient remplis
			if(typeof($scope.informations.current_password) == "undefined" 
			|| typeof($scope.informations.password) == "undefined" 
			|| typeof($scope.informations.password2) == "undefined")
			{
				//Sinon, return
				return toaster.pop('error', "web_label_renseigner_champ");
			}
		}
		//Si ça roule
		PF.post('utilisateur/editer', $scope.informations, function(retour, status, headers, config) {
			if (retour['status'] == 'ok'){
				$scope.utilisateur_local.name = _scope.informations.name;
				$scope.utilisateur_local.etablissement = _scope.informations.etablissement;
				$scope.utilisateur_local.langue = _scope.informations.langue;
				$scope.informations.current_password = "";
				$scope.informations.password = "";
				$scope.informations.password2 = "";
				toaster.pop('success', "WEB_LABEL_INFO_MODIFIEE_AVEC_SUCCES");
				$scope.$root.modal_profil_config = false;
			}
			//Ajout refusé
			else
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
				}
				else
					//Display de l'erreur correspondante
					toaster.pop('error', retour['message'].toUpperCase());
			}
		});

	}
	$scope.deleteAccount = function () {
		//Confirmation de suppression
		if (!confirm(LABEL_RGPD_DELETE_CONFIRM))
			return;
		
		//Flag pour spinner
		$scope.is_deleting = true;

		//Si ça roule
		PF.post('utilisateur/supprimer', {}, function(retour, status, headers, config) {
			$scope.is_deleting = false;
			if (retour['status'] == 'ok'){

				toaster.pop("success", "LABEL_RGPD_DELETE_CONFIRMED");
				setTimeout($scope.deconnecter, 3000);
			}
			//Suppression refusée
			else
			{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("WS_UTILISATEUR_INVALIDE");
				}
				else
					//Display de l'erreur correspondante
					toaster.pop('error', retour['message'].toUpperCase());
			}
		});
	}

	$scope.init();

}]);