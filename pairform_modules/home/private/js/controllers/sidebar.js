pf_app.controller('sidebar', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
	var defaults = {
		"visible" : true,
		"sidebar_visible" : false,
		"messages_deplacement_visible" : false,
		"notifications" : {'us' : [], 'un' : [], 'count_us' : 0, 'count_un' : 0, 'visible' : ''}
	};
	angular.extend($scope, defaults);

	$scope.actualiser = function () {
		Message.recupererMessages();
		toaster.pop('success',"web_label_messages_actualises");
		Notification.updateAll();
	}

	$scope.afficherNotification = function (type) {
		if ($scope.notifications.visible != type){
			$scope.notifications.visible = type;

			//Récupération des ids des notifications non vues
			var notifications_non_vues = $scope.notifications[type].map(function (notification, index) {
				if(notification.date_vue == null){
					notification.date_vue = Math.round(Date.now() / 1000);
					return notification.id_notification;
				}
			}).filter(function(n){ return n != undefined });

	
			//Gestion de la synchro des vues des notifications
			if (notifications_non_vues.length > 0)
			{
				var old_count = ""+$scope.notifications['count_'+type];
				$scope.notifications['count_'+type] = 0;

				var post = {"id_notifications" : notifications_non_vues};

				PF.post("notification/vue", post , function (retour){
					
					if (retour['status'] == 'ko')
					{
						if (retour['message'] == "ws_utilisateur_invalide") {
							$scope.deconnecter("ws_utilisateur_invalide");
						}
						else
							toaster.pop('error', retour['message']);
						
						$scope.notifications['count_'+type] = old_count;
					}
				});
				
			}
		}
		else
			$scope.notifications.visible = '';
	}

	$scope.actionNotification = function (notification, notification_type) {
		//Score utilisateur
		if (notification_type == "us"){
			//Si on est dans le cas d'un succes, il n'y a pas de message attaché
			if (!notification.id_message){
				$scope.afficherProfil();
			}
			else{
				PF.redirigerVersMessage(notification.id_message);
			}
		}
		//Notification utilisateur
		else{
			switch(notification.sous_type){
				// dc : défi créé 
				case "dc" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// dt : défi terminé 
				case "dt" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// dv : défi validé 
				case "dv" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// ar : ajout réseau
				case "ar" : {
					$scope.afficherProfil(notification.contenu);
					break;
				}
				// cr : classe rejointe
				case "cr" : {
					$scope.afficherProfil(notification.contenu);
					break;
				}
				// ru : réponse d'utilisateur
				case "ru" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// gm : gagné médaille
				case "gm" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// gs : gagné succès
				case "gs" : {
					$scope.afficherProfil();
					break;
				}
				// cc : changé catégorie
				case "cc" : {
					$scope.afficherProfil();
					break;
				}
			}
		}
	}

}]);