pf_app.controller('dashboard', ["$scope", "toaster", "$localStorage", function ($scope, toaster, $localStorage) {
	var defaults = {
		"visible" : true,
		"sidebar_visible" : false,
		"messages_deplacement_visible" : false,
		"notifications" : {'us' : [], 'un' : [], 'count_us' : 0, 'count_un' : 0, 'visible' : ''}
	};
	angular.extend($scope, defaults);

	$scope.init = function () {
		$scope.getRessourcesRecentes();
		$scope.getNotifications();
		$scope.getMessages();
		$scope.transversal = true;
	}

	$scope.getRessourcesRecentes = function () {
		PF.get('ressource/liste/recentes', {}, function(data, status, headers, config) {
			var retour = data;
			// S'il y a des ressources
			if (retour['status'] == 'ok')
			{
				var ressources = retour['ressources'];
				//On va chercher le nombre de messages / On enlève le & du début de post_OS
				$scope.ressources = ressources;
				//Tri des ressources, on commence à afficher
				//Parallèllement, on chope le nombre de messages pour les afficher sur les ressources
				PF.get("message/nombre", {"id_utilisateur" : $scope.utilisateur_local.id_utilisateur, "langues_affichage" : [1,2,3]}, function(retour, status, headers, config){
				
					if (retour['status'] == "ok")
					{
						$scope.nombre_messages = retour['nombre_messages'];
						// var postLoad = {'messages' : messages, 'ressources' : ressources, 'langueApp': LanguageM.langueApp };
						
					}
				});
				$scope.loaded = true;
			}
		});
	}

	$scope.getNotifications = function () {
		PF.get('notification', {}, function (data, status, headers, config){
			   
			var retour = data;
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				$scope.notifications.us = retour['us'];
				$scope.notifications.un = retour['un'];
				$scope.notifications.count_us = retour['count_us'];
				$scope.notifications.count_un = retour['count_un'];
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
				}
			}
		});
	}

	$scope.getMessages = function () {
		PF.get('message/transversal/derniers', {'id_capsule' : 1, "langues_affichage" : 3}, function (data, status, headers, config){
			   
			var retour = data;
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				$scope.messages = retour["data"];
			}
			else{
				console.log(retour['message']);
			}
		});
	}

	$scope.afficherNotification = function (type) {
		if ($scope.notifications.visible != type){
			$scope.notifications.visible = type;

			//Récupération des ids des notifications non vues
			var notifications_non_vues = $scope.notifications[type].map(function (notification, index) {
				if(notification.date_vue == null){
					notification.date_vue = Math.round(Date.now() / 1000);
					return notification.id_notification;
				}
			}).filter(function(n){ return n != undefined });

	
			//Gestion de la synchro des vues des notifications
			if (notifications_non_vues.length > 0)
			{
				var old_count = ""+$scope.notifications['count_'+type];
				$scope.notifications['count_'+type] = 0;

				var post = {"id_notifications" : notifications_non_vues};

				PF.post("notification/vue", post , function (retour){
					
					if (retour['status'] == 'ko')
					{
						if (retour['message'] == "ws_utilisateur_invalide") {
							$scope.deconnecter("ws_utilisateur_invalide");
						}
						else
							toaster.pop('error', retour['message']);
						
						$scope.notifications['count_'+type] = old_count;
					}
				});
				
			}
		}
		else
			$scope.notifications.visible = '';
	}

	$scope.actionNotification = function (notification, notification_type) {
		//Score utilisateur
		if (notification_type == "us"){
			//Si on est dans le cas d'un succes, il n'y a pas de message attaché
			if (!notification.id_message){
				$scope.afficherProfil();
			}
			else{
				PF.redirigerVersMessage(notification.id_message);
			}
		}
		//Notification utilisateur
		else{
			switch(notification.sous_type){
				// dc : défi créé 
				case "dc" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// dt : défi terminé 
				case "dt" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// dv : défi validé 
				case "dv" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// ar : ajout réseau
				case "ar" : {
					$scope.afficherProfil(notification.contenu);
					break;
				}
				// cr : classe rejointe
				case "cr" : {
					$scope.afficherProfil(notification.contenu);
					break;
				}
				// ru : réponse d'utilisateur
				case "ru" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// gm : gagné médaille
				case "gm" : {
					PF.redirigerVersMessage(notification.id_message);
					break;
				}
				// gs : gagné succès
				case "gs" : {
					$scope.afficherProfil();
					break;
				}
				// cc : changé catégorie
				case "cc" : {
					$scope.afficherProfil();
					break;
				}
				// dc : document created
				case "dc" : {
					PF.redirigerVersCapsule(notification.contenu);
					break;
				}
				// du : document updated
				case "du" : {
					PF.redirigerVersCapsule(notification.contenu);
					break;
				}
			}
		}
	}
	$scope.init();
}]);