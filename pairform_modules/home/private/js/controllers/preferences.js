pf_app.controller('preferences', ["$scope", "toaster", "$localStorage", "$translate", function ($scope, toaster, $localStorage, $translate) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"preferences" : {
			"connexion_automatique" : false,
			"notifications_envoyees_par_mail" : $scope.utilisateur_local.notifications_envoyees_par_mail,
			"afficher_noms_reels" : false,
			"current_code_langue" : $localStorage.code_langue_app && $localStorage.code_langue_app.length ? $localStorage.code_langue_app[0] : "fr"
		},
		"tableau_labels" : PF.langue.tableau_labels
	};
	angular.extend($scope, defaults);
	// angular.extend($scope, $scope.utilisateur_local.rank[$scope.capsule.id_capsule]);

	$scope.init = function () {
		// Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
		PF.get('utilisateur/notification', {}, function(retour, status, headers, config) {
			if (retour["status"] == "ko") {
				//Rien de neuf
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				};
			}
			else{
				var capsules = retour.data,
					capsules_notifications = {};
				capsules.forEach(function(capsule){
					if (typeof capsules_notifications[capsule.id_ressource] === "undefined") {
						capsules_notifications[capsule.id_ressource] = {
							id_ressource : capsule.id_ressource,
							url_logo : capsule.url_logo,
							nom_court : capsule.nom_court_ressource,
							nom_long : capsule.nom_long_ressource,
							notification_mail : capsule.notification_mail,
							capsules : []
						}
					}
					capsules_notifications[capsule.id_ressource].capsules.push(capsule);
				});
				
				//Conversion en tableau
				$scope.capsules_notifications = []
				for(var id_ressource in capsules_notifications){
					if (capsules_notifications.hasOwnProperty(id_ressource)) {
						$scope.capsules_notifications.push(capsules_notifications[id_ressource])
					}
				}

			}				
		});

	}
	$scope.switchNotificationMail = function (capsule) {

		var post = {'id_capsule' : capsule.id_capsule};

		//On inverse la valeur (cast booléen puis string)
		var notif_mail = capsule.notification_mail = +!capsule.notification_mail;

		PF.post('utilisateur/notification/mail', post , function (retour){
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				var alert = notif_mail ? 'activées' : 'désactivées';
				toaster.pop('success', "Notifications par email " + alert + " sur ce document");
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
			}
		});
	}

	$scope.switchWatchMail = function (capsule) {

		var post = {'id_capsule' : capsule.id_capsule};

		//On inverse la valeur (cast booléen puis string)
		var notif_mail = capsule.watch_mail = +!capsule.watch_mail;

		PF.post('utilisateur/notification/watch', post , function (retour){
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				var alert = notif_mail ? 'activé' : 'désactivé';
				toaster.pop('success', "Favori " + alert);
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
			}
		});
	}
	$scope.switchNouvellesNotificationsMail = function () {



		//On inverse la valeur (cast booléen puis string)
		var notif_mail = $scope.utilisateur_local.notifications_envoyees_par_mail = +!$scope.utilisateur_local.notifications_envoyees_par_mail;

		PF.post('utilisateur/notification/nouvelles', function (retour){
			//Si ya pas de soucis
			if (retour['status'] == 'ok')
			{
				var alert = notif_mail ? 'activé' : 'désactivé';
				toaster.pop('success', "Favori " + alert);
			}
			else{
				if (retour['message'] == "ws_utilisateur_invalide") {
					$scope.deconnecter("ws_utilisateur_invalide");
				}
			}
		});
	}

	$scope.switchNomsReels = function () {
		$scope.fonctionPasDisponible();
	}
	$scope.selectLangue = function (id_langue) {
		var code_langue = PF.langue.tableau[id_langue] || "en";

		$translate.use(code_langue);
		// Maj de la langue de l'application dans l'utilisateur local
		$localStorage.code_langue_app = [code_langue];
	}

	$scope.reinitialiserMessagesLus = function () {
		delete $localStorage.array_messages_lus;
		toaster.pop('success',"web_label_messages_lus_reinitialises");

		setTimeout(function () {
			location.reload();
		},1000);
	}

	$scope.effacerToutesDonnees = function () {
		$localStorage.$reset();
		PF._localStorage.clear();

		setTimeout(function () {
			location.reload();
		},400);
	}
	$scope.init()
}]);