pf_app.controller('root', ["$scope", "$localStorage", "toaster", "$translate", function ($scope, $localStorage, toaster, $translate) {
	
	$scope.capsule = PF.globals.capsule;
	$scope.moment = moment;

	//S'il n'y a pas d'utilisateur du tout, ou que l'utilisateur non connecté n'est pas encore venu sur cette capsule
	if (!$localStorage.utilisateur_local 
	|| !$localStorage.utilisateur_local.est_connecte)
		//On regénère un utilisateur, pour qu'il ait le rang pour cette capsule
		$localStorage.utilisateur_local = new Utilisateur();
	else
		//Sinon, on le récupère
		$localStorage.utilisateur_local = new Utilisateur($localStorage.utilisateur_local);

	$scope.utilisateur_local = $localStorage.utilisateur_local;
	PF.globals.utilisateur_local = $localStorage.utilisateur_local;

	$scope.utilisateur_local.avatar_url_blur = $scope.utilisateur_local.avatar_url.replace(/(.*\/)(.*?)(\?.*)?$/,'$1$2.blur.jpg$3');

	$scope.codeFromId = PF.langue.codeFromId;
	$scope.idFromCode = PF.langue.idFromCode;
	$scope.res_url_private = PF.globals.url.private;
	$scope.res_url_public = PF.globals.url.public;
	$scope.res_url_root = PF.globals.url.root;

	$scope.afficherConnexion = function (mode){
		var scope_connexion = PF.getScopeOfController('controller_connexion');
		if (scope_connexion){
			// scope_connexion.$parent.setVisible();
			// scope_connexion.$parent.popin_visible = true;
			scope_connexion.visible = true;
			// scope_connexion.popin.visible = true;
			scope_connexion.mode = mode || 'connexion';
		}	
	}


	$scope.afficherMessagesUtilisateur = function (id_utilisateur) {
		Message.afficher(undefined, id_utilisateur);
	}
	$scope.afficherMessagesTransversal = function () {
		Message.afficher(undefined, undefined);
	}

	$scope.afficherCarrousel = function (array_pj, index_pj) {

		var carrousel_scope = PF.getScopeOfController('controller_carrousel');
		carrousel_scope.array_pj = array_pj;
		carrousel_scope.index_pj = index_pj;
		//Et on affiche le panneau après, qui appelle init()
		var _carrousel = new Panneau({'nom_controller' : 'controller_carrousel', 'position' : 'left'});
		_carrousel.togglePanneau();
	}
	$scope.afficherProfil = function (id_utilisateur) {
		if (typeof(id_utilisateur) == "undefined")
			var id_utilisateur = $localStorage.utilisateur_local.id_utilisateur;
		//On passe la valeur
		PF.getScopeOfController('controller_profil').utilisateur.id_utilisateur = id_utilisateur;
		//Et on affiche le panneau après, qui appelle init()
		var _profil = new Panneau({'nom_controller' : 'controller_profil', 'position' : 'right', 'should_toggle' : false});
		_profil.togglePanneau();
	}
	$scope.afficherProfilConfig = function () {
		// if (typeof(id_utilisateur) == "undefined")
		var utilisateur_local = $localStorage.utilisateur_local;
		//On passe la valeur
		PF.getScopeOfController('controller_profil_config').utilisateur = utilisateur_local;
		//Et on affiche le panneau après, qui appelle init()
		var _profil_config = new Panneau({'nom_controller' : 'controller_profil_config', 'position' : 'right', 'superpose' : true});
		_profil_config.togglePanneau();
	}
	$scope.afficherChangerAvatar = function () {
		// if (typeof(id_utilisateur) == "undefined")
		var utilisateur_local = $localStorage.utilisateur_local;
		//On passe la valeur
		PF.getScopeOfController('controller_avatar_upload').utilisateur = utilisateur_local;
		//Et on affiche le panneau après, qui appelle init()
		var _changer_avatar = new Panneau({'nom_controller' : 'controller_avatar_upload', 'position' : 'right', 'superpose' : true});
		_changer_avatar.togglePanneau();
	}
	$scope.afficherSucces = function (id_utilisateur, succes) {
		if (typeof(id_utilisateur) == "undefined")
			var id_utilisateur = undefined;
		//On passe la valeur
		PF.getScopeOfController('controller_succes').id_utilisateur = id_utilisateur;
		PF.getScopeOfController('controller_succes').succes = succes || undefined;
		//Et on affiche le panneau après, qui appelle init()
		var _succes = new Panneau({'nom_controller' : 'controller_succes', 'position' : 'left'});
		_succes.togglePanneau();
	}
	$scope.afficherReseau = function (mode, utilisateur_ajout) {
		PF.getScopeOfController('controller_reseau').mode = mode || "gestion";
		PF.getScopeOfController('controller_reseau').utilisateur_ajout = utilisateur_ajout || undefined;

		var _reseau = new Panneau({'nom_controller' : 'controller_reseau', 'position' : 'right', 'superpose' : true});
		_reseau.togglePanneau();
		
	}
	$scope.afficherDashboard = function () {
		// var _dashboard = new Panneau({'nom_controller' : 'controller_dashboard', 'position' : 'left'});
		// _dashboard.togglePanneau();
		$scope.fonctionPasDisponible();
	}
	$scope.afficherRessources = function () {
		var _ressources = new Panneau({'nom_controller' : 'controller_ressources', 'position' : 'left'});
		_ressources.togglePanneau();
	}

	$scope.afficherRecherche = function (mode) {
		PF.getScopeOfController('controller_recherche_profil').mode = mode || "recherche_profil";
		var _recherche_profil = new Panneau({'nom_controller' : 'controller_recherche_profil', 'position' : 'left'});
		_recherche_profil.togglePanneau();
	}
	$scope.afficherPreferences = function () {
		var _preferences = new Panneau({'nom_cont^roller' : 'controller_preferences', 'position' : 'left'});
		_preferences.togglePanneau();
	}
	$scope.afficherPresentation = function () {
		var _presentation = new Panneau({'nom_controller' : 'controller_presentation', 'position' : 'left'});
		_presentation.togglePanneau();
	}

	$scope.$root.afficherFicheRessource = function (id_ressource) {
		var ressource_scope = PF.getScopeOfController('ressource');
		$scope.$root.modal_ressource = true;
		
		ressource_scope.id_ressource = id_ressource || PF.globals.capsule.id_ressource;
		ressource_scope.init();
	}

	$scope.safeApply = function(fn) {
		var phase = this.$root.$$phase;
		if(phase == '$apply' || phase == '$digest') {
			if(fn && (typeof(fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	}
	//Init par défaut
	$scope.init = function () {
		this;
	}

	//Remove par défaut
	$scope.remove = function () {
		this.panneau.cacherPanneau();
	}
	//Fonctionnalité pas encore disponible...
	$scope.fonctionPasDisponible = function () {
		toaster.pop("note", "web_label_fonctionnalité_indisponible");
	}
	//Fonctionnalité pas encore disponible...
	$scope.changerLangue = function (code_langue) {
		code_langue = code_langue || "fr";
		$translate.uses(code_langue);
	}
	//Fonctionnalité pas encore disponible...
	$scope.changerLangue = function (code_langue) {
		code_langue = code_langue || "fr";
		$translate.uses(code_langue);
	}

	//Fonctionnalité pas encore disponible...
	$scope.switchToInjecteur = function (code_langue) {
		localStorage.flag_inj = true;
		window.location.reload();
	}

  $scope.$on("pf:utilisateur:logout", function(){
    window.location.reload();
  });

}]);