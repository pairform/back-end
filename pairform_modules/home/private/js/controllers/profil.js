pf_app.controller('profil', ["$scope", "$http", "$translate", "toaster", "$routeParams", function ($scope, $http, $translate, toaster, $routeParams) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"upgrade_ressource" : false,
		"is_connected_user" : false,
		messages : [],
		limit: 5,
		"utilisateur" : {
			id_utilisateur : $routeParams.id_utilisateur == "moi" ? $scope.utilisateur_local.id_utilisateur : parseInt($routeParams.id_utilisateur)
		}
	};

	angular.extend($scope, defaults);

	$scope.init = function () {
		//Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
		if ($scope.loaded != $scope.utilisateur.id_utilisateur) {
			var _scope = $scope;
			PF.get('utilisateur/profil', {"id_utilisateur": $scope.utilisateur.id_utilisateur} , function(retour, status, headers, config) {
				if (retour["status"] == "ok") {

					var datas = retour['datas'];
					datas.id_utilisateur = $scope.utilisateur.id_utilisateur;
					var utilisateur = new Utilisateur(datas);

					_scope.is_connected_user = (utilisateur.id_utilisateur == _scope.utilisateur_local.id_utilisateur);
					
					angular.extend(_scope.utilisateur, utilisateur);

					//Si l'utilisateur regarde son propre profil
					if (_scope.is_connected_user){
						//On va utiliser certains champs de l'utilisateur_local plutot qu'une copie
						//Pour que les M.A.J. des infos utilisateur (avatar etc...) se reflètent
						_scope.$watch("utilisateur_local", function (new_value, old_value) {
							if (new_value != old_value) {
								_scope.utilisateur.avatar_url = _scope.utilisateur_local.avatar_url;
								_scope.utilisateur.name = _scope.utilisateur_local.name;
								_scope.utilisateur.etablissement = _scope.utilisateur_local.etablissement;
								_scope.utilisateur.langue_principale = _scope.utilisateur_local.langue_principale;							
							}
						}, true);
						
					}

					_scope.loaded = _scope.utilisateur.id_utilisateur;
					_scope.determinerDerniereConnexion();
					// var post = {'id_utilisateur':id_user, 'profil': retour, 'is_connected_user': is_connected_user, 'langueApp': LanguageM.langueApp};

				}
				else {
					if (retour.message) {
						//Cet utilisateur n'existe plus.
						//ou Cet utilisateur n'existe pas.
						$scope.error = retour.message.toUpperCase()
					}
				}
				
			});

			PF.get('message/transversal/utilisateur', {'id_utilisateur' : $scope.utilisateur.id_utilisateur, "langues_affichage" : 3}, function (data, status, headers, config){
			   
				var retour = data;
				//Si ya pas de soucis
				if (retour['status'] == 'ok')
				{
					$scope.messages_raw = retour["data"];
					var _messages = [];
					$scope.messages_raw.forEach(function (capsule, index) {
						_messages = _messages.concat(capsule.messages)
					})
					_messages.sort(function (a,b) {
            //Si a est créé avant
            if (a.date_creation > b.date_creation)
              return -1;
            if (a.date_creation < b.date_creation)
              return 1;
            return 0;

          });
          $scope.messages = _messages;
				}
				else{
					console.log(retour['message']);
				}
			});
		}
	}
	$scope.init();

	$scope.determinerDerniereConnexion = function  () {
		var lb_st = $scope.labelStatus;
		var t_off = $scope.utilisateur.offline;
		$translate(['web_label_en_ligne', 'web_label_inactif_depuis', 'web_label_minutes', 'web_label_heures', 'web_label_jours', 'web_label_mois']).then(function (translation_object) {
			lb_st = translation_object['web_label_en_ligne'];
			if (t_off != false) {

				if (t_off < 60) {
					t_off = Math.round(t_off);
					lb_st = translation_object['web_label_inactif_depuis'] + ' ' + t_off + ' ' +translation_object['web_label_minutes'];
				}
				else if((t_off > 60) && (t_off < 1440)){
					t_off = Math.round(t_off / 60);
					lb_st = translation_object['web_label_inactif_depuis'] + ' ' + t_off + ' ' +translation_object['web_label_heures'];
				}
				else if((t_off > 1440) && (t_off < 43200)){
					t_off = Math.round(t_off / 1440);
					lb_st = translation_object['web_label_inactif_depuis'] + ' ' + t_off + ' ' +translation_object['web_label_jours'];
				}
				else if((t_off > 43200) && (t_off < 20000000)){
					t_off = Math.round(t_off / 43200);
					lb_st = translation_object['web_label_inactif_depuis'] + ' ' + t_off + ' ' +translation_object['web_label_mois'];
				}
				else if (t_off > 20000000) {
					lb_st = translation_object['web_label_inactif'];
				}
			}
			$scope.offline = t_off;
			$scope.labelStatus = lb_st;
		});
	}

	$scope.changerRoleUtilisateur = function (categorie_ressource, id_categorie_temp, nom_categorie_temp) {
		PF.post('utilisateur/role', {"id_utilisateur_cible": $scope.utilisateur.id_utilisateur, "id_ressource": categorie_ressource.id_ressource, "id_categorie_temp": id_categorie_temp} , function(retour, status, headers, config) {
			if (retour["status"] == "ok"){
				categorie_ressource.id_categorie = retour["data"]["id_categorie"];
				categorie_ressource.nom_categorie = retour["data"]["nom_categorie"];
				$scope.upgrade_ressource = false;
			}
			else
				toaster.pop("error", "ws_erreur_contacter_pairform");
		});
	}
}]);