
pf_app.controller('kiosque', ["$scope", "$http", "toaster", function ($scope, $http, toaster) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"sort_type" : 0,
		"sort_types" : [
				{index : 0, label : "KIOSQUE.ESPACE"}, 
				{index : 1, label : "KIOSQUE.THEME"}
			],
		"order_type" : "ressources.length",
		"order_types" : [
				{value : "ressources.length", label : "KIOSQUE.NOMBRE_DE_DOCUMENTS"}, 
				{value : "nom_court", label : "KIOSQUE.ORDRE_ALPHABETIQUE"},
				{value : "date_creation_espace", label : "KIOSQUE.ANCIENNETE"}
		]
	};
	angular.extend($scope, defaults);

	$scope.init = function () {
		if (!$scope.loaded) {
			var _scope = $scope;
			PF.get('ressource/liste', {}, function(data, status, headers, config) {
				var retour = data;
				// S'il y a des ressources
				if (retour['status'] == 'ok')
				{
					var ressources = retour['ressources'];
					//On va chercher le nombre de messages / On enlève le & du début de post_OS
					_scope.ressources = ressources;
					//Tri des ressources, on commence à afficher
					_scope.sortRessources();
					//Parallèllement, on chope le nombre de messages pour les afficher sur les ressources
					PF.get("message/nombre", {"langues_affichage" : [1,2,3]}, function(retour, status, headers, config){
					
						if (retour['status'] == "ok")
						{
							_scope.nombre_messages = retour['nombre_messages'];
							// var postLoad = {'messages' : messages, 'ressources' : ressources, 'langueApp': LanguageM.langueApp };
							
						}
					});
				
					_scope.loaded = true;
				}
			});
		}
	}

	$scope.showRessources = function (espace) {
		$scope.$root.ressource_affiche = espace.nom_court;
		setTimeout(function() {
			$scope.msnry.layout();
		}, 50);
	}

	$scope.sortRessources = function () {

		var resParDomaine = {};
		var resParEspace = {};
		angular.forEach($scope.ressources,function (ressource) {
			ressource.url_logo = PF.globals.url.root + ressource.url_logo;
			ressource.url_logo_espace = PF.globals.url.root + ressource.url_logo_espace;
			if(typeof(resParEspace[ressource.espace_nom_court]) == "undefined")
			{
				resParEspace[ressource.espace_nom_court] = {
					"nom_court" : ressource.espace_nom_court,
					"url_logo" : ressource.url_logo_espace,
					"id_espace" : ressource.id_espace,
					"date_creation_espace" : ressource.date_creation_espace,
					"ressources" : []
				};
			}
			resParEspace[ressource.espace_nom_court].ressources.push(ressource);

			if(typeof(resParDomaine[ressource.theme]) == "undefined")
			{
				resParDomaine[ressource.theme] = {
					"nom_court" : ressource.theme,
					"date_creation_espace" : ressource.date_creation_espace,
					"ressources" : []
				}
			}
			resParDomaine[ressource.theme].ressources.push(ressource);
			
		});

		$scope.sort_type = 0;
		$scope.res_sorted = [];

		function object2Array(input) {
			var out = []; 
			for(i in input){
				out.push(input[i]);
			}
			return out;
		
		}
		$scope.res_sorted[0] = object2Array(resParEspace);
		$scope.res_sorted[1] = object2Array(resParDomaine);
	}
	$scope.colorFromString = function(string) {
		var hash = 0;
		if (string.length == 0) return hash;
		for (var i = 0; i < string.length; i++) {
			hash = string.charCodeAt(i) + ((hash << 5) - hash);
			hash = hash & hash; // Convert to 32bit integer
		}
		var shortened = hash % 255;
		return "hsl(" + shortened + ",100%,40%)";
	}

	//Initialisation du module
	$scope.init();
}]);