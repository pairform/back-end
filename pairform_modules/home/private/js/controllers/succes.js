pf_app.controller('succes', ["$scope", "$routeParams", function ($scope, $routeParams) {
	var defaults = {
		"visible" : false,
		"loaded" : false,
		"id_utilisateur" : $routeParams.id_utilisateur == "me" ? $scope.utilisateur_local.id_utilisateur : parseInt($routeParams.id_utilisateur),
		"succes" : [],
	};

	angular.extend($scope, defaults);

	$scope.init = function () {
		//S'il y a déjà des succès, on laisse tomber
		// if ($scope.succes && $scope.id_utilisateur)
		// 	return;

		//Le cache est relatif à l'utilisateur chargé : on compare le dernier utilisateur chargé et celui demandé
		if ($scope.loaded != $scope.id_utilisateur) {
			var _scope = $scope;
			PF.get('accomplissement/succes', {"id_utilisateur": $scope.id_utilisateur} , function(retour, status, headers, config) {
				if (retour["status"] == "ok") {
					var succes = retour["data"];
					_scope.succes = succes;

					_scope.loaded = _scope.id_utilisateur;
				}
			});
		}
	}
	$scope.init();
}]);