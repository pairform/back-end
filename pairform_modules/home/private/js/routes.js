pf_app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
	$routeProvider
		// Dashboard
		.when("/", {
			templateUrl: PF.globals.url.views + "partial/dashboard"
		})
		.when("/profil/:id_utilisateur", {
			templateUrl: PF.globals.url.views + "partial/profil"
		})
		.when("/profil/:id_utilisateur/succes", {
			templateUrl: PF.globals.url.views + "partial/succes"
		})
		.when("/profil/:id_utilisateur/badges", {
			templateUrl: PF.globals.url.views + "partial/badges"
		})
		.when("/dashboard", {
			templateUrl: PF.globals.url.views + "partial/dashboard"
		})
		.when("/profil/moi", {
			templateUrl: PF.globals.url.views + "partial/profil"
		})
		.when("/reseau", {
			templateUrl: PF.globals.url.views + "partial/reseau"
		})
		.when("/kiosque", {
			templateUrl: PF.globals.url.views + "partial/kiosque"
		})
		.when("/messages", {
			templateUrl: PF.globals.url.views + "partial/messages"
		})
		.when("/messages/utilisateur/:id_utilisateur", {
			templateUrl: PF.globals.url.views + "partial/messages"
		})
		.when("/utilisateurs", {
			templateUrl: PF.globals.url.views + "partial/utilisateurs"
		})
		.when("/preferences", {
			templateUrl: PF.globals.url.views + "partial/preferences"
		})

		.when("/backoffice", {
			templateUrl: PF.globals.url.views + "partial/backoffice",
			reloadOnSearch: false
		})

		// tableau de bord du backoffice
		.when("/backoffice/tableauDeBord", {
			redirectTo: "/backoffice",
		})

		// espaces
		.when("/backoffice/espaces/creationEspace", {
			templateUrl: PF.globals.url.root + "backoffice/espaces/gestionEspace",
			controller: "controleurCreationEspace"
		})
		.when("/backoffice/espaces/gestionEspace/:id_espace", {
			templateUrl: PF.globals.url.root + "backoffice/espaces/gestionEspace",
			controller: "controleurGestionEspace"
		})

		// ressources
		.when("/backoffice/ressources/creationRessource/:id_espace", {
			templateUrl: PF.globals.url.root + "backoffice/ressources/gestionRessource",
			controller: "controleurCreationRessource"
		})
		.when("/backoffice/ressources/gestionRessource/:id_ressource", {
			templateUrl: PF.globals.url.root + "backoffice/ressources/gestionRessource",
			controller: "controleurGestionRessource"
		})

		// capsules
		.when("/backoffice/capsules/creationCapsule/:id_ressource", {
			templateUrl: PF.globals.url.root + "backoffice/capsules/gestionCapsule",
			controller: "controleurCreationCapsule"
		})
		.when("/backoffice/capsules/gestionCapsule/:id_capsule", {
			templateUrl: PF.globals.url.root + "backoffice/capsules/gestionCapsule",
			controller: "controleurGestionCapsule"
		})
		.when("/backoffice/capsules/gestionVisibiliteCapsule/:id_capsule", {
			templateUrl: PF.globals.url.root + "backoffice/capsules/gestionVisibiliteCapsule",
			controller: "controleurGestionVisibiliteCapsule"
		})

		// groupes
		.when("/backoffice/groupes/gestionGroupes/:id_espace", {
			templateUrl: PF.globals.url.root + "backoffice/groupes/gestionGroupes",
			controller: "controleurGestionGroupes"
		})

		// rôles
		.when("/backoffice/roles/gestionRolesEspace/:id_espace", {
			templateUrl: PF.globals.url.root + "backoffice/roles/gestionRolesEspace",
			controller: "controleurGestionRolesEspace"
		})
		.when("/backoffice/roles/gestionRolesRessource/:id_ressource", {
			templateUrl: PF.globals.url.root + "backoffice/roles/gestionRolesRessource",
			controller: "controleurGestionRolesRessource"
		})

		// statistiques
		.when("/backoffice/statistiques/statistiquesRessource/:id_ressource", {
			templateUrl: PF.globals.url.root + "backoffice/statistiques/statistiquesRessource",
			controller: "controleurStatistiquesRessource"
		})

		// openbadges
		.when("/backoffice/openBadges/gestionOpenBadges/:id_ressource", {
			templateUrl: PF.globals.url.root + "backoffice/openBadges/gestionOpenBadges",
			controller: "controleurGestionOpenBadges"
		})
		// redirection des routes inconnues vers l'accueil
		.otherwise({
			redirectTo: "/"
		});
}]);
