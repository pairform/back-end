var web_services = require("../webservices/lib/webServices"),
		 express = require("express"),
		  CONFIG = require("config"),
		  	  fs = require('fs'),
	  	   async = require('async'),
	  	       _ = require("underscore"),
	  nodemailer = require('nodemailer'),
			 log = require('metalogger')(),
	 webservices = require("../webservices"),
	  constantes = require("../webservices/lib/constantes"),
	pairform_dao = require("../webservices/node_modules/pairform_dao/lib/pairformDAO");



module.exports = function(app, passport) {

	//Configuration d'Underscore
	//Mustache-like & angular-like interpolation, genre {{ variable }}
	// _.templateSettings = { interpolate: /\{\{\=(.+?)\}\}/g, escape: /\{\{\-(.+?)\}\}/g, evaluate: /\{\{(.+?)\}\}/g };

	// si l'utilisateur n'est pas connecté, on ne traite pas la requête
	function utilisateurEstConnecte(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			res.json(constantes.RETOUR_JSON_UTILISATEUR_NON_CONNECTE);
		}
	}

	function send404 (res) {
		res.status(404).render("404", {
			"url_serveur_node" : CONFIG.app.urls.serveur_node
		});
	}


	app.get("/home/partial/:nom_view", function (req, res) {
		res.render(__dirname + "/views/" + req.params.nom_view, {
			"url_serveur_node" : CONFIG.app.urls.serveur_node
		});
	});

	app.get("/home/*", function gate_keeper (req, res, next) {
			if (req.isAuthenticated()) {
				return next();
			} else {
				res.render(__dirname + "/../authentification/views/gate_keeper",{
					url_to_follow : "#",
					unauthorized : false,
					url_logo_espace : CONFIG.app.urls.serveur_node + '/public/img/logo.png',
					url_serveur_node : CONFIG.app.urls.serveur_node,
					id_capsule : 0,
					id_ressource : 0
				});
			}
		},
		function (req, res) {
		res.render(__dirname + "/views/template", {
			url_serveur_node : CONFIG.app.urls.serveur_node,
			url_etherpad : CONFIG.app.urls.etherpad
		});
	});

	// app.get("/home/:id_home", function (req, res) {		
	// 	res.render(__dirname + "/views/profil", {
	// 		url_serveur_node : CONFIG.app.urls.serveur_node
	// 	});
	// });

	//View contenant tous les élements composants l'interface de l'app wzeb
	// app.get("/home/me", function (req, res) {		
	// 	res.render(__dirname + "/views/me",{
	// 		"url_serveur_node" : CONFIG.app.urls.serveur_node
	// 	});
	// });


}