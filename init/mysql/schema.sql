-- phpMyAdmin SQL Dump
-- version 3.5.8
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 21 Mars 2017 à 17:17
-- Version du serveur: 5.0.92-log
-- Version de PHP: 5.3.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `PairForm_v1_2_400`
--

-- --------------------------------------------------------

--
-- Structure de la table `cape_capsules`
--

CREATE TABLE IF NOT EXISTS `cape_capsules` (
  `id_capsule` bigint(20) NOT NULL auto_increment,
  `clef_pairform` varchar(32) default NULL COMMENT 'clef identifiant la capsule',
  `id_format` bigint(20) NOT NULL default '0' COMMENT 'Format de la capsule : Scenari, ChainEdit, autre...',
  `id_ressource` bigint(20) NOT NULL,
  `nom_court` varchar(250) NOT NULL,
  `nom_long` varchar(250) NOT NULL,
  `url_mobile` varchar(250) default NULL COMMENT 'Url de la version mobile',
  `url_web` varchar(250) default NULL COMMENT 'url de la version web',
  `poid` int(11) default NULL,
  `description` text NOT NULL,
  `licence` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Privée',
  `auteurs` varchar(250) NOT NULL,
  `cree_par` bigint(20) NOT NULL,
  `date_edition` datetime default NULL,
  `date_creation` datetime NOT NULL,
  `est_bloquee` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Lecture seule',

  PRIMARY KEY  (`id_capsule`),
  UNIQUE KEY `nom_court` (`nom_court`,`id_ressource`),
  KEY `id_ressource` (`id_ressource`),
  KEY `cree_par` (`cree_par`),
  KEY `id_format` (`id_format`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table des ressources disponibles au téléchargement';

-- --------------------------------------------------------

--
-- Structure de la table `cape_capsules_selecteurs`
--

CREATE TABLE IF NOT EXISTS `cape_capsules_selecteurs` (
  `id_selecteur` bigint(20) NOT NULL auto_increment,
  `id_capsule` bigint(20) NOT NULL,
  `nom_selecteur` varchar(1000) NOT NULL,
  PRIMARY KEY  (`id_selecteur`,`id_capsule`),
  KEY `id_capsule` (`id_capsule`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table des sélecteurs CSS par capsules ';

-- --------------------------------------------------------

--
-- Structure de la table `cape_categories`
--

CREATE TABLE IF NOT EXISTS `cape_categories` (
  `id_categorie` bigint(20) NOT NULL auto_increment,
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Rajouter des colonnes pour chaque droit';

-- --------------------------------------------------------

--
-- Structure de la table `cape_classes`
--

CREATE TABLE IF NOT EXISTS `cape_classes` (
  `id_classe` int(11) NOT NULL,
  `cle` text NOT NULL,
  PRIMARY KEY  (`id_classe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Classes créées par les experts.';

-- --------------------------------------------------------

--
-- Structure de la table `cape_definitions_points`
--

CREATE TABLE IF NOT EXISTS `cape_definitions_points` (
  `id_definition_point` bigint(20) NOT NULL auto_increment,
  `code_definition_point` varchar(200) NOT NULL,
  `nombre_points` varchar(200) NOT NULL,
  PRIMARY KEY  (`id_definition_point`),
  UNIQUE KEY `code_definition_point` (`code_definition_point`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Définition des constantes de points hors PHP - utile pour jo';

-- --------------------------------------------------------

--
-- Structure de la table `cape_espaces`
--

CREATE TABLE IF NOT EXISTS `cape_espaces` (
  `id_espace` bigint(20) NOT NULL auto_increment,
  `nom_court` varchar(250) NOT NULL,
  `nom_long` varchar(250) NOT NULL,
  `url` varchar(250) default NULL,
  `url_logo` varchar(500) NOT NULL,
  `logo_blob` mediumblob,
  `date_edition` datetime default NULL,
  `date_creation` datetime NOT NULL,
  PRIMARY KEY  (`id_espace`),
  UNIQUE KEY `nom_court` (`nom_court`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table des établissements pour classer les ressources';

-- --------------------------------------------------------

--
-- Structure de la table `cape_formats`
--

CREATE TABLE IF NOT EXISTS `cape_formats` (
  `id_format` bigint(20) NOT NULL auto_increment,
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_format`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='formats possibles pour une capsule';

-- --------------------------------------------------------

--
-- Structure de la table `cape_groupes`
--

CREATE TABLE IF NOT EXISTS `cape_groupes` (
  `id_groupe` bigint(20) NOT NULL auto_increment,
  `nom` varchar(250) NOT NULL,
  `id_espace` bigint(20) NOT NULL,
  `obligatoire` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_groupe`),
  UNIQUE KEY `nom` (`nom`,`id_espace`),
  KEY `id_espace` (`id_espace`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Représente les groupes d''utilisateurs définis dans le backof';

-- --------------------------------------------------------

--
-- Structure de la table `cape_groupe_visibilite_capsule`
--

CREATE TABLE IF NOT EXISTS `cape_groupe_visibilite_capsule` (
  `id_groupe` bigint(20) NOT NULL,
  `id_capsule` bigint(20) NOT NULL,
  `id_visibilite` bigint(20) NOT NULL,
  PRIMARY KEY  (`id_groupe`,`id_capsule`,`id_visibilite`),
  KEY `id_groupe` (`id_groupe`),
  KEY `id_capsule` (`id_capsule`),
  KEY `id_visibilite` (`id_visibilite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Visibilité d''une capsule pour un groupe';

-- --------------------------------------------------------

--
-- Structure de la table `cape_langues`
--

CREATE TABLE IF NOT EXISTS `cape_langues` (
  `id_langue` bigint(20) NOT NULL,
  `code_langue` varchar(50) NOT NULL,
  PRIMARY KEY  (`id_langue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_membres`
--

CREATE TABLE IF NOT EXISTS `cape_membres` (
  `id_membre` bigint(20) NOT NULL auto_increment,
  `nom` varchar(50) NOT NULL COMMENT 'Membre d''un groupe représenté par un email ou un nom de domaine',
  `nom_de_domaine` tinyint(1) NOT NULL default '0' COMMENT 'Membre représenté par un nom de domaine',
  `id_role_espace` bigint(20) default NULL COMMENT 'role attribué au membre',
  PRIMARY KEY  (`id_membre`),
  KEY `nom` (`nom`),
  KEY `id_role_espace` (`id_role_espace`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='liste des membres des groupes';

-- --------------------------------------------------------

--
-- Structure de la table `cape_membres_groupes`
--

CREATE TABLE IF NOT EXISTS `cape_membres_groupes` (
  `id_groupe` bigint(20) NOT NULL,
  `id_membre` bigint(20) NOT NULL,
  PRIMARY KEY  (`id_groupe`,`id_membre`),
  KEY `id_groupe` (`id_groupe`),
  KEY `id_membre` (`id_membre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_membres_roles_ressources`
--

CREATE TABLE IF NOT EXISTS `cape_membres_roles_ressources` (
  `id_membre` bigint(20) NOT NULL,
  `id_role_ressource` bigint(20) NOT NULL,
  PRIMARY KEY  (`id_membre`,`id_role_ressource`),
  KEY `id_role_ressource` (`id_role_ressource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table croisée entre les rôles par ressource et les membres';

-- --------------------------------------------------------

--
-- Structure de la table `cape_messages`
--

CREATE TABLE IF NOT EXISTS `cape_messages` (
  `id_message` bigint(20) NOT NULL auto_increment,
  `id_utilisateur` bigint(20) NOT NULL,
  `uid_page` varchar(50) NOT NULL,
  `uid_oa` varchar(50) NOT NULL,
  `id_espace` bigint(20) NULL DEFAULT NULL, 
  `id_ressource` bigint(20) NULL DEFAULT NULL, 
  `id_capsule` bigint(20) NULL DEFAULT NULL,
  `nom_page` varchar(250) NOT NULL,
  `id_selecteur` bigint(20) NOT NULL,
  `num_occurence` bigint(20) NOT NULL,
  `geo_latitude` decimal(10,8) NOT NULL,
  `geo_longitude` decimal(11,8) NOT NULL,
  `supprime_par` bigint(20) NOT NULL,
  `id_message_parent` bigint(20) NOT NULL,
  `est_defi` int(20) NOT NULL default '0',
  `defi_valide` int(20) NOT NULL default '0',
  `medaille` varchar(200) NOT NULL,
  `visibilite` varchar(200) NOT NULL default '0',
  `contenu` varchar(20000) NOT NULL,
  `date_creation` int(20) NOT NULL,
  `date_modification` int(20) NOT NULL,
  `id_langue` bigint(20) NOT NULL default '3',
  PRIMARY KEY  (`id_message`),
  KEY `id_langue` (`id_langue`),
  KEY `nom_page_2` (`nom_page`,`id_selecteur`,`num_occurence`),
  KEY `nom_page` (`nom_page`),
  KEY `id_capsule` (`id_capsule`),
  KEY `id_selecteur` (`id_selecteur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Blogs elgg enregistrés par supCast pour etendre les informat';

-- --------------------------------------------------------

--
-- Structure de la table `cape_messages_lus`
--

CREATE TABLE IF NOT EXISTS `cape_messages_lus` (
  `id_message` bigint(20) NOT NULL,
  `id_utilisateur` bigint(20) NOT NULL,
  `date_lecture` int(20) NOT NULL,
  `os` varchar(80) NOT NULL,
  UNIQUE KEY `id_message` (`id_message`,`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Synchronisation des messages lus cross-platform';

-- --------------------------------------------------------

--
-- Structure de la table `cape_messages_pieces_jointes`
--

CREATE TABLE IF NOT EXISTS `cape_messages_pieces_jointes` (
  `id_message` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `nom_serveur` varchar(40) character set utf8 collate utf8_general_ci NOT NULL,
  `nom_original` varchar(105) character set utf8 collate utf8_general_ci NOT NULL,
  `extension` varchar(10) character set utf8 collate utf8_general_ci NOT NULL,
  `taille` varchar(14) character set utf8 collate utf8_general_ci NOT NULL,
  `nom_thumbnail` varchar(40) character set utf8 collate utf8_general_ci default NULL,
  `position` tinyint(10) NOT NULL default '0' COMMENT 'Position de la pièce jointe',
  PRIMARY KEY  (`id_message`,`nom_serveur`),
  UNIQUE KEY `nom_thumbnail` (`nom_thumbnail`),
  UNIQUE KEY `nom_thumbnail_2` (`nom_thumbnail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_messages_tags`
--

CREATE TABLE IF NOT EXISTS `cape_messages_tags` (
  `id_utilisateur` bigint(20) NOT NULL,
  `id_message` bigint(20) NOT NULL,
  `tag` varchar(200) NOT NULL,
  `date_creation` int(20) NOT NULL,
  `date_modification` int(20) NOT NULL,
  PRIMARY KEY  (`id_utilisateur`,`id_message`,`tag`),
  KEY `id_message` (`id_message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table croisée des utilisateurs et de leurs tags';

-- --------------------------------------------------------

--
-- Structure de la table `cape_messages_votes`
--

CREATE TABLE IF NOT EXISTS `cape_messages_votes` (
  `id_utilisateur` bigint(20) NOT NULL,
  `id_message` bigint(20) NOT NULL,
  `vote` int(20) NOT NULL,
  `date_creation` int(20) NOT NULL,
  `date_modification` int(20) NOT NULL,
  PRIMARY KEY  (`id_utilisateur`,`id_message`,`vote`),
  KEY `id_message` (`id_message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table croisée des utilisateurs et de leurs votes : -1 pour n';

-- --------------------------------------------------------

--
-- Structure de la table `cape_notifications`
--

CREATE TABLE IF NOT EXISTS `cape_notifications` (
  `id_notification` bigint(20) NOT NULL auto_increment,
  `id_utilisateur` bigint(20) default NULL,
  `id_message` bigint(20) default NULL,
  `titre` varchar(200) NOT NULL,
  `type_notification` varchar(200) NOT NULL,
  `sous_type` varchar(200) default NULL,
  `type_points` varchar(200) default NULL,
  `contenu` varchar(200) NOT NULL,
  `image` varchar(200) default NULL,
  `date_creation` datetime NOT NULL,
  `date_vue` datetime default NULL,
  `token` varchar(64) default NULL,
  `push_json` varchar(256) default NULL,
  `date_pushed` datetime default NULL,
  PRIMARY KEY  (`id_notification`),
  KEY `id_utilisateur` (`id_utilisateur`,`id_message`,`type_notification`),
  KEY `id_message` (`id_message`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Stockage des notifications pour récupération par client (pol';

-- --------------------------------------------------------

--
-- Structure de la table `cape_openbadges`
--

CREATE TABLE IF NOT EXISTS `cape_openbadges` (
  `id_openbadge` bigint(20) NOT NULL auto_increment,
  `id_ressource` bigint(20) NOT NULL,
  `nom` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `issuer_externe` varchar(250) default NULL,
  `date_creation` datetime NOT NULL,
  `date_edition` datetime default NULL,
  PRIMARY KEY  (`id_openbadge`),
  UNIQUE KEY `id_ressource_2` (`id_ressource`,`nom`),
  KEY `id_ressource` (`id_ressource`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cape_questions_profils_apprentissage`
--

CREATE TABLE IF NOT EXISTS `cape_questions_profils_apprentissage` (
  `id_question` bigint(20) NOT NULL auto_increment,
  `id_ressource` bigint(20) NOT NULL,
  PRIMARY KEY  (`id_question`),
  KEY `id_ressource` (`id_ressource`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_questions_profils_apprentissage_langues`
--

CREATE TABLE IF NOT EXISTS `cape_questions_profils_apprentissage_langues` (
  `id_question` bigint(20) NOT NULL,
  `id_langue` bigint(20) NOT NULL,
  `question` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_question`,`id_langue`),
  KEY `id_langue` (`id_langue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_reponses_profils_apprentissage`
--

CREATE TABLE IF NOT EXISTS `cape_reponses_profils_apprentissage` (
  `id_reponse` bigint(20) NOT NULL auto_increment,
  `id_question` bigint(20) NOT NULL,
  PRIMARY KEY  (`id_reponse`),
  KEY `id_question` (`id_question`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_reponses_profils_apprentissage_langues`
--

CREATE TABLE IF NOT EXISTS `cape_reponses_profils_apprentissage_langues` (
  `id_reponse` bigint(20) NOT NULL,
  `id_langue` bigint(20) NOT NULL,
  `reponse` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_reponse`,`id_langue`),
  KEY `id_langue` (`id_langue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_reseaux`
--

CREATE TABLE IF NOT EXISTS `cape_reseaux` (
  `id_reseau` bigint(20) NOT NULL auto_increment,
  `id_utilisateur` bigint(20) NOT NULL,
  `nom` varchar(200) NOT NULL,
  `cle` varchar(200) NOT NULL,
  PRIMARY KEY  (`id_reseau`,`id_utilisateur`),
  KEY `id_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Cercles et classes, et leur métadonnées';

-- --------------------------------------------------------

--
-- Structure de la table `cape_reseaux_utilisateurs`
--

CREATE TABLE IF NOT EXISTS `cape_reseaux_utilisateurs` (
  `id_reseau` bigint(20) NOT NULL,
  `id_utilisateur` bigint(20) NOT NULL,
  PRIMARY KEY  (`id_reseau`,`id_utilisateur`),
  KEY `id_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table croisée des utilisateurs et de leurs cercles';

-- --------------------------------------------------------

--
-- Structure de la table `cape_ressources`
--

CREATE TABLE IF NOT EXISTS `cape_ressources` (
  `id_ressource` bigint(20) NOT NULL auto_increment,
  `nom_court` varchar(200) NOT NULL,
  `cree_par` bigint(20) NOT NULL,
  `id_espace` bigint(20) NOT NULL,
  `nom_long` varchar(200) NOT NULL,
  `id_theme` bigint(20) NOT NULL,
  `id_usage` bigint(20) NOT NULL,
  `id_langue` bigint(20) NOT NULL,
  `description` text NOT NULL,
  `url_logo` varchar(500) NOT NULL,
  `logo_blob` mediumblob COMMENT 'icone au format blob',
  `date_edition` datetime default NULL,
  `date_creation` datetime NOT NULL,
  PRIMARY KEY  (`id_ressource`),
  UNIQUE KEY `nom_court` (`nom_court`,`id_espace`),
  KEY `id_espace` (`id_espace`),
  KEY `id_theme` (`id_theme`),
  KEY `id_usage` (`id_usage`),
  KEY `id_langue` (`id_langue`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table des ressources (collections)';

-- --------------------------------------------------------

--
-- Structure de la table `cape_roles_espace`
--

CREATE TABLE IF NOT EXISTS `cape_roles_espace` (
  `id_role_espace` bigint(20) NOT NULL auto_increment,
  `nom` varchar(250) NOT NULL,
  `admin_espace` tinyint(1) NOT NULL default '0',
  `id_espace` bigint(20) NOT NULL,
  `editer_espace` tinyint(1) NOT NULL default '0',
  `gerer_groupes` tinyint(1) NOT NULL default '0',
  `gerer_roles_espace` tinyint(1) NOT NULL default '0',
  `gerer_ressources_et_capsules` tinyint(1) NOT NULL default '0',
  `gerer_toutes_ressources_et_capsules` tinyint(1) NOT NULL default '0',
  `gerer_visibilite_toutes_capsules` tinyint(1) NOT NULL default '0',
  `deplacer_capsule` tinyint(1) default '0',
  PRIMARY KEY  (`id_role_espace`),
  KEY `id_espace` (`id_espace`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Représente les roles définis dans le backoffice pour un espa';

-- --------------------------------------------------------

--
-- Structure de la table `cape_roles_ressource`
--

CREATE TABLE IF NOT EXISTS `cape_roles_ressource` (
  `id_role_ressource` bigint(20) NOT NULL auto_increment,
  `id_ressource` bigint(20) NOT NULL default '0',
  `nom` varchar(250) NOT NULL,
  `editer_ressource` tinyint(1) NOT NULL default '0',
  `gerer_open_badge` tinyint(1) NOT NULL default '0',
  `afficher_statistiques` tinyint(1) NOT NULL default '0',
  `gerer_roles_ressource` tinyint(1) NOT NULL default '0',
  `supprimer_ressource` tinyint(1) NOT NULL default '0',
  `gerer_capsule` tinyint(1) NOT NULL default '0',
  `gerer_visibilite` tinyint(1) NOT NULL default '0',
  `gerer_toutes_capsules` tinyint(1) NOT NULL default '0',
  `gerer_visibilite_toutes_capsules_ressource` tinyint(1) NOT NULL default '0',
  `supprimer_messages_capsule` tinyint(1) NOT NULL default '0',
  `supprimer_messages_toutes_capsules` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_role_ressource`),
  KEY `id_ressource` (`id_ressource`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Représente les roles définis dans le backoffice pour une res';

-- --------------------------------------------------------

--
-- Structure de la table `cape_sources_authentification`
--

CREATE TABLE IF NOT EXISTS `cape_sources_authentification` (
  `id_source_auth` int(20) NOT NULL auto_increment,
  `id_source_auth_client` varchar(200) NOT NULL,
  `context_id` varchar(200) NOT NULL,
  `context_title` varchar(400) NOT NULL,
  `nom_client` varchar(200) NOT NULL,
  `mail_administrateur` varchar(200) NOT NULL,
  `est_autorise` int(1) NOT NULL default '0',
  `oauth_consumer_key` varchar(200) NOT NULL,
  `oauth_consumer_secret` varchar(200) NOT NULL,
  `date_creation` int(11) NOT NULL,
  PRIMARY KEY  (`id_source_auth`),
  UNIQUE KEY `oauth_consumer_key` (`oauth_consumer_key`),
  UNIQUE KEY `id_source_auth_client` (`id_source_auth_client`,`context_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_sources_authentification_utilisateurs`
--

CREATE TABLE IF NOT EXISTS `cape_sources_authentification_utilisateurs` (
  `id_log` int(20) NOT NULL auto_increment,
  `id_utilisateur` int(20) NOT NULL,
  `id_source_auth` int(20) NOT NULL,
  `est_valide` int(1) NOT NULL default '0',
  `date_connexion` int(11) NOT NULL,
  PRIMARY KEY  (`id_log`,`id_utilisateur`),
  UNIQUE KEY `id_utilisateur` (`id_utilisateur`,`id_source_auth`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Log des connexions LTI & autorisation utilisation';

-- --------------------------------------------------------

--
-- Structure de la table `cape_succes`
--

CREATE TABLE IF NOT EXISTS `cape_succes` (
  `id_succes` bigint(20) NOT NULL auto_increment,
  `place` int(11) NOT NULL,
  `nom_logo` varchar(250) NOT NULL,
  `image_blob` mediumblob NOT NULL,
  PRIMARY KEY  (`id_succes`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Badges disponibles';

-- --------------------------------------------------------

--
-- Structure de la table `cape_succes_langues`
--

CREATE TABLE IF NOT EXISTS `cape_succes_langues` (
  `id_succes` bigint(20) NOT NULL default '0',
  `id_langue` bigint(20) NOT NULL default '0',
  `nom` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_succes`,`id_langue`),
  UNIQUE KEY `id_langue_2` (`id_langue`,`nom`),
  KEY `id_langue` (`id_langue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_themes`
--

CREATE TABLE IF NOT EXISTS `cape_themes` (
  `id_theme` bigint(20) NOT NULL auto_increment,
  PRIMARY KEY  (`id_theme`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Thèmes (domaines) pour classer les ressources';

-- --------------------------------------------------------

--
-- Structure de la table `cape_themes_langues`
--

CREATE TABLE IF NOT EXISTS `cape_themes_langues` (
  `id_theme` bigint(20) NOT NULL default '0',
  `id_langue` bigint(20) NOT NULL default '0',
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_theme`,`id_langue`),
  UNIQUE KEY `id_langue_2` (`id_langue`,`nom`),
  KEY `id_langue` (`id_langue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_traduction_langues`
--

CREATE TABLE IF NOT EXISTS `cape_traduction_langues` (
  `id_traduction` bigint(20) NOT NULL default '0',
  `id_langue` bigint(20) NOT NULL default '0',
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_traduction`,`id_langue`),
  KEY `id_langue` (`id_langue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_usages`
--

CREATE TABLE IF NOT EXISTS `cape_usages` (
  `id_usage` bigint(20) NOT NULL auto_increment,
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_usage`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Usages possibles pour une ressource';

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs` (
  `id_utilisateur` bigint(20) NOT NULL,
  `etablissement` varchar(250) NOT NULL,
  `device_token` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des informations sur les utilisateurs';

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_capsules_parametres`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_capsules_parametres` (
  `id_capsule` bigint(20) NOT NULL,
  `id_utilisateur` bigint(20) NOT NULL,
  `notification_mail` tinyint(1) NOT NULL default '0',
  `watch_mail` tinyint(1) NOT NULL default '0',
  `afficher_noms_reels` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_capsule`,`id_utilisateur`),
  KEY `id_utilisateur` (`id_utilisateur`),
  KEY `id_capsule` (`id_capsule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table n-to-n de paramètres par capsule';

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_categorie`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_categorie` (
  `id_ressource` bigint(20) NOT NULL,
  `id_utilisateur` bigint(20) NOT NULL,
  `id_categorie` bigint(20) NOT NULL,
  `id_categorie_temp` bigint(20) NOT NULL,
  `score` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_ressource`,`id_utilisateur`,`id_categorie`),
  UNIQUE KEY `Antidoublon` (`id_ressource`,`id_utilisateur`),
  KEY `id_utilisateur` (`id_utilisateur`),
  KEY `id_categorie` (`id_categorie`),
  KEY `id_categorie_temp` (`id_categorie_temp`),
  KEY `id_ressource` (`id_ressource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table n-to-n de catégorie par ressource par utilisateur';

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_classes`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_classes` (
  `id_utilisateur` bigint(20) NOT NULL,
  `id_classe` int(11) NOT NULL,
  PRIMARY KEY  (`id_utilisateur`,`id_classe`),
  KEY `id_classe` (`id_classe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table d''appartenance d''un utilisateur à une classe';

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_langues`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_langues` (
  `id_utilisateur` bigint(20) NOT NULL default '0',
  `id_langue` bigint(20) NOT NULL default '0',
  `principale` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_utilisateur`,`id_langue`),
  KEY `id_langue` (`id_langue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_objectifs_apprentissage`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_objectifs_apprentissage` (
  `id_objectif` bigint(20) NOT NULL auto_increment,
  `id_ressource` bigint(20) NOT NULL,
  `id_utilisateur` bigint(20) NOT NULL,
  `nom` varchar(250) NOT NULL,
  `est_valide` tinyint(1) NOT NULL default '0',
  `date_edition` datetime default NULL,
  `date_creation` datetime NOT NULL,
  PRIMARY KEY  (`id_objectif`),
  KEY `id_ressource` (`id_ressource`,`id_utilisateur`),
  KEY `id_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_openbadges`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_openbadges` (
  `id_openbadge` bigint(20) NOT NULL,
  `id_utilisateur` bigint(20) NOT NULL,
  `date_gagne` datetime NOT NULL,
  UNIQUE KEY `id_openbadge` (`id_openbadge`,`id_utilisateur`),
  KEY `id_openbadge_2` (`id_openbadge`),
  KEY `id_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_profils_apprentissage`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_profils_apprentissage` (
  `id_question` bigint(20) NOT NULL,
  `id_reponse_valide` bigint(20) default NULL,
  `id_utilisateur` bigint(20) NOT NULL,
  `date_edition` datetime default NULL,
  `date_creation` datetime NOT NULL,
  PRIMARY KEY  (`id_question`,`id_utilisateur`),
  KEY `id_reponse_valide` (`id_reponse_valide`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='reponses des utilisateurs dans chaque profils d''apprentissag';

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_succes`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_succes` (
  `id_utilisateur` bigint(20) NOT NULL,
  `id_succes` bigint(20) NOT NULL,
  `date_gagne` datetime default NULL,
  KEY `id_utilisateur` (`id_utilisateur`),
  KEY `id_badge` (`id_succes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_utilisateurs_v2`
--

CREATE TABLE IF NOT EXISTS `cape_utilisateurs_v2` (
  `id_utilisateur` bigint(20) NOT NULL auto_increment,
  `etablissement` varchar(250) NOT NULL,
  `email` varchar(200) NOT NULL,
  `nom_prenom` varchar(200) NOT NULL,
  `pseudo` varchar(128) NOT NULL,
  `password` varchar(200) NOT NULL,
  `salt` varchar(200) NOT NULL,
  `avatar_url` varchar(400) NOT NULL default 'public/img/avatar/0.png',
  `token_validation` varchar(200) NOT NULL,
  `token_reset` varchar(200) default NULL,
  `token_reset_expiration` int(11) NOT NULL,
  `token_oauth` varchar(200) NOT NULL,
  `token_oauth_expiration` int(11) NOT NULL,
  `email_est_valide` tinyint(1) NOT NULL default '0',
  `est_banni` tinyint(1) NOT NULL default '0',
  `est_admin` tinyint(1) NOT NULL default '0',
  `date_creation` int(11) NOT NULL default '0',
  `date_actif` int(11) NOT NULL default '0',
  `os_mobile` varchar(20) NOT NULL,
  `token_mobile` varchar(250) NOT NULL,
  `notifications_envoyees_par_mail` tinyint(1) NOT NULL DEFAULT '1',
  `id_provider` int(20) NULL DEFAULT NULL COMMENT 'Id interne OAuth', 
  `provider` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Nom provider OAuth',
  PRIMARY KEY  (`id_utilisateur`),
  KEY `pseudo` (`pseudo`),
  KEY `password` (`password`),
  KEY `code` (`token_reset`),
  KEY `date_creation` (`date_creation`),
  KEY `date_actif` (`date_actif`),
  KEY `est_admin` (`est_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cape_visibilites`
--

CREATE TABLE IF NOT EXISTS `cape_visibilites` (
  `id_visibilite` bigint(20) NOT NULL auto_increment,
  `nom` varchar(250) default NULL,
  PRIMARY KEY  (`id_visibilite`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Visibilités possible d''une capsule';

-- --------------------------------------------------------

--
-- Structure de la table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(255) collate utf8_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` MEDIUMTEXT collate utf8_bin,
  PRIMARY KEY  (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `cape_sessions` ( 
 `id_session` BIGINT(20) NOT NULL AUTO_INCREMENT ,
 `id_ressource` BIGINT(20) NOT NULL ,
 `id_groupe` BIGINT(20) NULL,
 `nom` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `id_utilisateur` BIGINT(20) NOT NULL ,
 `date_creation` BIGINT(20) NOT NULL ,
 PRIMARY KEY (`id_session`)) 
 ENGINE = InnoDB CHARACTER 
 SET utf8 COLLATE utf8_general_ci COMMENT = 'Sessions de formations / timeline';

 CREATE TABLE `cape_sessions_items` ( 
 `id_item` BIGINT(20) NOT NULL AUTO_INCREMENT ,
 `id_session` BIGINT(20) NOT NULL ,
 `id_utilisateur` BIGINT(20) NOT NULL ,
 `id_capsule` BIGINT(20) NULL ,
 `date_creation` BIGINT(20) NOT NULL ,
 `date_diffusion` BIGINT(20) NOT NULL ,
 `description`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
 `icone`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `sub_type`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `titre`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `type`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `type_icone`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `url`  VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
 `diffusion_automatique` INT(1) NOT NULL DEFAULT '0', 
 PRIMARY KEY (`id_item`)) 
 ENGINE = InnoDB CHARACTER 
 SET utf8 COLLATE utf8_general_ci COMMENT = 'Items des sessions de formations / timeline';

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `cape_capsules`
--
ALTER TABLE `cape_capsules`
  ADD CONSTRAINT `cape_capsules_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_10` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_11` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_12` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_13` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_14` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_15` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_16` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_17` FOREIGN KEY (`id_format`) REFERENCES `cape_formats` (`id_format`),
  ADD CONSTRAINT `cape_capsules_ibfk_2` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_3` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_4` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_5` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_6` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_7` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_8` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_capsules_ibfk_9` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`);

--
-- Contraintes pour la table `cape_capsules_selecteurs`
--
ALTER TABLE `cape_capsules_selecteurs`
  ADD CONSTRAINT `cape_capsules_selecteurs_ibfk_1` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`);

--
-- Contraintes pour la table `cape_groupes`
--
ALTER TABLE `cape_groupes`
  ADD CONSTRAINT `cape_groupes_ibfk_1` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_10` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_11` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_12` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_13` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_14` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_15` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_16` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_2` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_3` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_4` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_5` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_6` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_7` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_8` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_groupes_ibfk_9` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`);

--
-- Contraintes pour la table `cape_groupe_visibilite_capsule`
--
ALTER TABLE `cape_groupe_visibilite_capsule`
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_1` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_10` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_11` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_12` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_13` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_14` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_15` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_16` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_17` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_18` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_19` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_2` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_20` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_21` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_22` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_23` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_24` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_25` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_26` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_27` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_28` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_29` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_3` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_30` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_31` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_32` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_33` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_34` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_35` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_36` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_37` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_38` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_39` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_4` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_40` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_41` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_42` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_43` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_44` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_45` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_46` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_47` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_48` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_5` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_6` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_7` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_8` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_groupe_visibilite_capsule_ibfk_9` FOREIGN KEY (`id_visibilite`) REFERENCES `cape_visibilites` (`id_visibilite`);

--
-- Contraintes pour la table `cape_membres`
--
ALTER TABLE `cape_membres`
  ADD CONSTRAINT `cape_membres_ibfk_1` FOREIGN KEY (`id_role_espace`) REFERENCES `cape_roles_espace` (`id_role_espace`);

--
-- Contraintes pour la table `cape_membres_groupes`
--
ALTER TABLE `cape_membres_groupes`
  ADD CONSTRAINT `cape_membres_groupes_ibfk_1` FOREIGN KEY (`id_groupe`) REFERENCES `cape_groupes` (`id_groupe`),
  ADD CONSTRAINT `cape_membres_groupes_ibfk_2` FOREIGN KEY (`id_membre`) REFERENCES `cape_membres` (`id_membre`);

--
-- Contraintes pour la table `cape_membres_roles_ressources`
--
ALTER TABLE `cape_membres_roles_ressources`
  ADD CONSTRAINT `cape_membres_roles_ressources_ibfk_2` FOREIGN KEY (`id_role_ressource`) REFERENCES `cape_roles_ressource` (`id_role_ressource`),
  ADD CONSTRAINT `cape_membres_roles_ressources_ibfk_3` FOREIGN KEY (`id_membre`) REFERENCES `cape_membres` (`id_membre`);

--
-- Contraintes pour la table `cape_messages`
--
ALTER TABLE `cape_messages`
  ADD CONSTRAINT `cape_messages_ibfk_1` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_10` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_11` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_12` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_13` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_14` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_15` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_16` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_17` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_18` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_19` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_2` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_20` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_21` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_22` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_23` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_24` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_25` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_26` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_27` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_28` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_29` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_3` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_30` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_31` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_32` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_4` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_5` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_6` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_7` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_messages_ibfk_8` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_messages_ibfk_9` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`);

--
-- Contraintes pour la table `cape_messages_tags`
--
ALTER TABLE `cape_messages_tags`
  ADD CONSTRAINT `cape_messages_tags_ibfk_10` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_11` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_12` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_13` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_14` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_15` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_16` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_17` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_18` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_19` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_2` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_20` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_21` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_22` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_23` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_24` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_25` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_26` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_27` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_28` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_29` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_30` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_31` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_32` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_33` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_34` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_4` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_5` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_6` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_7` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_8` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_tags_ibfk_9` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cape_messages_votes`
--
ALTER TABLE `cape_messages_votes`
  ADD CONSTRAINT `cape_messages_votes_ibfk_10` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_11` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_12` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_13` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_14` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_15` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_16` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_17` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_18` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_19` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_2` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_20` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_21` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_22` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_23` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_24` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_25` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_26` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_27` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_28` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_29` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_30` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_31` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_32` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_33` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_34` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_4` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_5` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_6` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_7` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_8` FOREIGN KEY (`id_message`) REFERENCES `cape_messages` (`id_message`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_messages_votes_ibfk_9` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cape_openbadges`
--
ALTER TABLE `cape_openbadges`
  ADD CONSTRAINT `cape_openbadges_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`);

--
-- Contraintes pour la table `cape_questions_profils_apprentissage`
--
ALTER TABLE `cape_questions_profils_apprentissage`
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_10` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_11` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_12` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_13` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_14` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_15` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_16` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_2` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_3` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_4` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_5` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_6` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_7` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_8` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_ibfk_9` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`);

--
-- Contraintes pour la table `cape_questions_profils_apprentissage_langues`
--
ALTER TABLE `cape_questions_profils_apprentissage_langues`
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_10` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_11` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_12` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_13` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_14` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_15` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_16` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_17` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_18` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_19` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_2` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_20` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_21` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_22` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_23` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_24` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_25` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_26` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_27` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_28` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_29` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_3` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_30` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_31` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_32` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_4` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_5` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_6` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_7` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_8` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_questions_profils_apprentissage_langues_ibfk_9` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`);

--
-- Contraintes pour la table `cape_reponses_profils_apprentissage`
--
ALTER TABLE `cape_reponses_profils_apprentissage`
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_10` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_11` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_12` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_13` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_14` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_15` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_16` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_2` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_3` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_4` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_5` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_6` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_7` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_8` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_ibfk_9` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`);

--
-- Contraintes pour la table `cape_reponses_profils_apprentissage_langues`
--
ALTER TABLE `cape_reponses_profils_apprentissage_langues`
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_1` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_10` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_11` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_12` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_13` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_14` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_15` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_16` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_17` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_18` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_19` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_2` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_20` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_21` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_22` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_23` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_24` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_25` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_26` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_27` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_28` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_29` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_3` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_30` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_31` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_32` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_4` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_5` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_6` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_7` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_8` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_reponses_profils_apprentissage_langues_ibfk_9` FOREIGN KEY (`id_reponse`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`);

--
-- Contraintes pour la table `cape_reseaux`
--
ALTER TABLE `cape_reseaux`
  ADD CONSTRAINT `cape_reseaux_ibfk_2` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_ibfk_3` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_ibfk_4` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_ibfk_5` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_ibfk_6` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_ibfk_7` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_ibfk_8` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_ibfk_9` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cape_reseaux_utilisateurs`
--
ALTER TABLE `cape_reseaux_utilisateurs`
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_1` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_10` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_11` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_12` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_13` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_14` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_15` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_16` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_2` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_3` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_4` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_5` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_6` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_7` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_8` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_reseaux_utilisateurs_ibfk_9` FOREIGN KEY (`id_reseau`) REFERENCES `cape_reseaux` (`id_reseau`) ON DELETE CASCADE;

--
-- Contraintes pour la table `cape_ressources`
--
ALTER TABLE `cape_ressources`
  ADD CONSTRAINT `cape_ressources_ibfk_1` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_10` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_11` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_12` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_13` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_14` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_15` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_16` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_17` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_18` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_19` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_2` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_20` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_21` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_22` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_23` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_24` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_25` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_26` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_27` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_28` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_29` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_3` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_30` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_31` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_32` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_33` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_34` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_35` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_36` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_37` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_38` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_39` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_4` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_40` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_41` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_42` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_43` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_44` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_45` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_46` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_47` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_48` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_49` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_5` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_50` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_51` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_52` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_53` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_54` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_55` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_56` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_57` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_58` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_59` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_6` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_60` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_61` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_ressources_ibfk_62` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_63` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_64` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`),
  ADD CONSTRAINT `cape_ressources_ibfk_7` FOREIGN KEY (`id_usage`) REFERENCES `cape_usages` (`id_usage`),
  ADD CONSTRAINT `cape_ressources_ibfk_8` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_ressources_ibfk_9` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`);

--
-- Contraintes pour la table `cape_roles_espace`
--
ALTER TABLE `cape_roles_espace`
  ADD CONSTRAINT `cape_roles_espace_ibfk_1` FOREIGN KEY (`id_espace`) REFERENCES `cape_espaces` (`id_espace`);

--
-- Contraintes pour la table `cape_roles_ressource`
--
ALTER TABLE `cape_roles_ressource`
  ADD CONSTRAINT `cape_roles_ressource_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`);

--
-- Contraintes pour la table `cape_succes_langues`
--
ALTER TABLE `cape_succes_langues`
  ADD CONSTRAINT `cape_succes_langues_ibfk_1` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_10` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_11` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_12` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_13` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_14` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_15` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_16` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_2` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_3` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_4` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_5` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_6` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_7` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_8` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_succes_langues_ibfk_9` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`);

--
-- Contraintes pour la table `cape_themes_langues`
--
ALTER TABLE `cape_themes_langues`
  ADD CONSTRAINT `cape_themes_langues_ibfk_1` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_10` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_11` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_12` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_13` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_14` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_15` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_16` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_17` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_18` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_19` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_2` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_20` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_21` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_22` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_23` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_24` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_3` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_4` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_5` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_6` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_7` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_8` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_themes_langues_ibfk_9` FOREIGN KEY (`id_theme`) REFERENCES `cape_themes` (`id_theme`);

--
-- Contraintes pour la table `cape_traduction_langues`
--
ALTER TABLE `cape_traduction_langues`
  ADD CONSTRAINT `cape_traduction_langues_ibfk_1` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_10` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_11` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_12` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_13` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_14` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_15` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_16` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_2` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_3` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_4` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_5` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_6` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_7` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_8` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_traduction_langues_ibfk_9` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`);

--
-- Contraintes pour la table `cape_utilisateurs_capsules_parametres`
--
ALTER TABLE `cape_utilisateurs_capsules_parametres`
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_1` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_10` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_11` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_12` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_13` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_14` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_15` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_16` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_17` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_18` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_19` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_2` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_20` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_21` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_22` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_23` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_24` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_3` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_4` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_5` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_6` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_7` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_8` FOREIGN KEY (`id_capsule`) REFERENCES `cape_capsules` (`id_capsule`),
  ADD CONSTRAINT `cape_utilisateurs_capsules_parametres_ibfk_9` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cape_utilisateurs_categorie`
--
ALTER TABLE `cape_utilisateurs_categorie`
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_10` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_11` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_12` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_13` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_14` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_15` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_16` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_17` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_18` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_19` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_2` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_20` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_21` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_22` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_23` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_24` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_25` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_26` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_27` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_28` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_29` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_3` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_30` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_31` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_32` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_33` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_34` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_35` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_36` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_37` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_38` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_39` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_4` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_40` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_41` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_42` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_43` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_44` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_45` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_46` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_47` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_48` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_49` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_5` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_50` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_51` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_52` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_53` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_54` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_55` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_56` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_6` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_7` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_8` FOREIGN KEY (`id_categorie`) REFERENCES `cape_categories` (`id_categorie`),
  ADD CONSTRAINT `cape_utilisateurs_categorie_ibfk_9` FOREIGN KEY (`id_categorie_temp`) REFERENCES `cape_categories` (`id_categorie`);

--
-- Contraintes pour la table `cape_utilisateurs_classes`
--
ALTER TABLE `cape_utilisateurs_classes`
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_1` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_10` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_11` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_12` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_13` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_14` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_15` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_16` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_17` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_18` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_19` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_2` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_20` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_21` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_22` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_23` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_24` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_3` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_4` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_5` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_6` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_7` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_8` FOREIGN KEY (`id_classe`) REFERENCES `cape_classes` (`id_classe`),
  ADD CONSTRAINT `cape_utilisateurs_classes_ibfk_9` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `cape_utilisateurs_langues`
--
ALTER TABLE `cape_utilisateurs_langues`
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_1` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_10` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_11` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_12` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_13` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_14` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_15` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_16` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_17` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_18` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_19` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_2` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_20` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_21` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_22` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_23` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_24` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_3` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_4` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_5` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_6` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_7` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_8` FOREIGN KEY (`id_langue`) REFERENCES `cape_langues` (`id_langue`),
  ADD CONSTRAINT `cape_utilisateurs_langues_ibfk_9` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `cape_utilisateurs_objectifs_apprentissage`
--
ALTER TABLE `cape_utilisateurs_objectifs_apprentissage`
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_10` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_11` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_12` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_13` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_14` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_15` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_16` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_17` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_18` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_19` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_2` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_20` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_21` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_22` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_23` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_24` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_3` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_4` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_5` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_6` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_7` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_8` FOREIGN KEY (`id_ressource`) REFERENCES `cape_ressources` (`id_ressource`),
  ADD CONSTRAINT `cape_utilisateurs_objectifs_apprentissage_ibfk_9` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `cape_utilisateurs_openbadges`
--
ALTER TABLE `cape_utilisateurs_openbadges`
  ADD CONSTRAINT `cape_utilisateurs_openbadges_ibfk_1` FOREIGN KEY (`id_openbadge`) REFERENCES `cape_openbadges` (`id_openbadge`),
  ADD CONSTRAINT `cape_utilisateurs_openbadges_ibfk_2` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`);

--
-- Contraintes pour la table `cape_utilisateurs_profils_apprentissage`
--
ALTER TABLE `cape_utilisateurs_profils_apprentissage`
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_10` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_11` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_12` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_13` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_14` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_15` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_16` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_17` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_18` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_19` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_2` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_20` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_21` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_22` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_23` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_24` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_25` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_26` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_27` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_28` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_29` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_3` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_30` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_31` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_32` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_4` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_5` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_6` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_7` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_8` FOREIGN KEY (`id_reponse_valide`) REFERENCES `cape_reponses_profils_apprentissage` (`id_reponse`),
  ADD CONSTRAINT `cape_utilisateurs_profils_apprentissage_ibfk_9` FOREIGN KEY (`id_question`) REFERENCES `cape_questions_profils_apprentissage` (`id_question`);

--
-- Contraintes pour la table `cape_utilisateurs_succes`
--
ALTER TABLE `cape_utilisateurs_succes`
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_1` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_10` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_11` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_12` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_13` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_14` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_15` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_16` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_17` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_18` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_19` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_2` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_20` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_21` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_22` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_23` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_24` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_25` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_4` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_5` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_6` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_7` FOREIGN KEY (`id_utilisateur`) REFERENCES `cape_utilisateurs_v2` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_8` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`),
  ADD CONSTRAINT `cape_utilisateurs_succes_ibfk_9` FOREIGN KEY (`id_succes`) REFERENCES `cape_succes` (`id_succes`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
