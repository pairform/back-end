var mysql = require('mysql'),
   CONFIG = require('config'),
	async = require('async'),
 	  log = require("metalogger")(),
   bCrypt = require('bcrypt-nodejs'),
  cluster = require("cluster"),
	   fs = require('fs');

exports.initialiser = function (callback) {
	//On n'execute l'initialisation que sur le master
	if(!cluster.isMaster){
		return callback();
	}
	log.debug("Lancement du script d'initialisation PairForm.");

	var pairform_dao = require("../pairform_modules/webservices/node_modules/pairform_dao");

	var web_services = require("../pairform_modules/webservices/lib/webServices");

	//Shallow copy pour 
	var configuration_bdd = require('util')._extend({}, CONFIG.app.bdd_mysql),
			environnement = process.env.NODE_ENV || "production";

	// Pour utilisation plus tard
	var utilisateur = {};

	// Pour n'autoriser les multiples commandes que dans cet import
	configuration_bdd.multipleStatements = true;
	//On teste l'existence de la base de données plus tard, pour pouvoir traiter les erreurs de connexion en premier
	delete configuration_bdd.database;

	var connection = mysql.createConnection(configuration_bdd);

	log.debug("Verification de la base de données en cours...");

	async.series([
		function checkConnection(next_step) {
			connection.query("SELECT 1", function (err, rows, fields) {
				if (err) {
					log.error("Connection à la base de donnée impossible : vérifiez les informations de connexion");
					next_step(err);
				}
				else{
					next_step();
				}
			});
		},
		function checkDB(next_step) {
			//CREATE DATABASE IF NOT EXISTS buggé : http://stackoverflow.com/a/3034381/1437016
			//Séparation en deux étapes
			var query = connection.query("SHOW DATABASES LIKE ?",
				[CONFIG.app.bdd_mysql.database], function (err, rows, fields) {
				//Si la BDD n'existe pas, on la créé
				if (rows.length === 0) {				
					//Création de la base
					log.notice("Base de donnée inexistante, création de " + CONFIG.app.bdd_mysql.database);
					var query = connection.query("CREATE DATABASE ?? DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci",
						[CONFIG.app.bdd_mysql.database], function (err, rows, fields) {
						if (err) {
							log.error("Impossible de créer la base de données : vérifiez les permissions de l'utilisateur de mysql déclaré dans le fichier de config.");
							log.error(query.sql);
							next_step(err);
						}
						else{
							next_step();
						}
					});
				}
				else{
					next_step();
				}
			});
		},

		function useDB(next_step) {
			connection.query("USE ??",
				[CONFIG.app.bdd_mysql.database], function (err, rows, fields) {
				if (err) {
					log.error("Impossible d'utiliser la base de données : vérifiez qu'elle a bien été créée.");

					next_step(err);
				}
				else{
					next_step();
				}
			});
		},
		function createTablesAndDefaults(next_step) {
			connection.query("SHOW TABLES ", function (err, rows, fields) {
				if (err) {
					log.error("Impossible d'utiliser la base de données : vérifiez les permissions de l'utilisateur de mysql déclaré dans le fichier de config.");
					//TODO : possibilité de créer la db à ce moment
					throw err;
				}
				else{
					//Besoin de faire l'import de structure ?
					// if (1) {
					if (rows.length === 0) {
						//Ouep
						async.series([
						    function importSchema(callback){
								log.notice("Import de la base de données dans init/config/schema.sql...");
						        fs.readFile("init/mysql/schema.sql", "utf8", function (err, data) {
									if (err){
										log.debug("Pas de fichier init/config/schema.sql trouvé");
										callback(err);
									}
									connection.query(data, function (err, rows, fields) {
										if(!err)
											log.notice("Import init/config/schema.sql OK");
										callback(err);
									});
								});
						    },
						    function importStatic(callback){
								log.notice("Import des données statiques dans init/config/static_tables.sql...");
						        fs.readFile("init/mysql/static_tables.sql", "utf8", function (err, data) {
									if (err){
										log.debug("Pas de fichier init/config/static_tables.sql trouvé");
										callback(err);
									}
									connection.query(data, function (err, rows, fields) {
										if(!err)
											log.notice("Import init/config/static_tables.sql OK");
										callback(err);
									});
								});
						    },
						    function importAdmin(callback){
								log.notice("Insertion utilisateur 'Administrateur' avec les données fournies dans docker-compose.yml : ");
								var admin_email = CONFIG.app.compte_admin.email, //Par défaut : contact@pairform.fr
									admin_password = CONFIG.app.compte_admin.password; //Par défaut : pairform
								log.notice("Email : " + admin_email);
								log.notice("Password : " + admin_password);
								//on créé un salt et on hash le mot de passe avec
								var salt = bCrypt.genSaltSync(),
									password_hash = bCrypt.hashSync(admin_password, salt);

								//Récup de la DAO :

								async.waterfall([
									function insertionAdmin (next) {
										pairform_dao.utilisateur_dao.insertUtilisateurAdminValide(admin_email, password_hash, 
											function callback_success (retour_sql) {
												//Récuperation du lastInsertId, qui vaut l'id utilisateur
												//Global
												utilisateur = {
													id_utilisateur: retour_sql.insertId,
													est_admin_pairform: 1
												}
												log.debug("utilisateur.id_utilisateur : "+ utilisateur.id_utilisateur);
												next(null);
										}, next);
									},
									function insertionAdmin (next) {
										pairform_dao.utilisateur_dao.insertLangueUtilisateur(utilisateur.id_utilisateur, 3, 1, function (retour_sql) {
											next();
										}, next);
									},
								],
								function callback_final (err, results) {
									if (err) 
										throw err;
									else{
										callback();
									}
								})
						    },
						    function importDefaut(callback){
								log.notice("Import des données par défaut dans init/config/default_objects.json...");
								fs.readFile("init/mysql/default_objects.json", "utf8", function (err, data) {
									if (err){
										log.error("Pas de fichier init/config/default_objects.json trouvé");
										callback(err);
									}
									var defaults = JSON.parse(data);

									async.series([
										function insertEspaces (sub_callback) {
											async.each(
												defaults.espaces, 
												function  (espace, sub_each_callback) {
													log.debug("Ajout de l'espace : " + espace.nom_court);
													web_services.espace_backoffice.putEspace(espace, function (retour) {
														sub_each_callback();
													});
												},
												sub_callback
											);
										},
										function insertRessources (sub_callback) {

											async.each(
												defaults.ressources, 
												function (ressource, sub_each_callback) {
													log.debug("Ajout de la ressource : " + ressource.nom_court);
													web_services.ressource_backoffice.putRessource(ressource, utilisateur, function (retour) {
														sub_each_callback();
													});
												},
												sub_callback
											);
										},
										function insertCapsules (sub_callback) {

											async.each(
												defaults.capsules, 
												function (capsule, sub_each_callback) {
													//Ajout de l'URL du serveur pour les capsules par défaut
													if (capsule.url_mobile) {
														capsule.url_mobile = CONFIG.app.urls.serveur_node + capsule.url_mobile;
													}
													if (capsule.url_web) {
														capsule.url_web = CONFIG.app.urls.serveur_node + capsule.url_web;
													}
													log.debug("Ajout de la capsule : " + capsule.nom_court);
													web_services.capsule_backoffice.putCapsule(capsule, utilisateur.id_utilisateur, function (retour) {
														//On met à jour la clé ensuite, pour forcer la clé voulue
														pairform_dao.capsule_dao.updateClefPairFormCapsule(retour.id_capsule, capsule.clef_pairform, function () {
															sub_each_callback();
														}, sub_each_callback);
													});
												},
												sub_callback
											);
										},
									], function callback_final (sub_callback) {
										if(!err)
											log.notice("Import init/config/default_objects.json OK");
										callback(err);
									});

								});
						    }
						],
						// Callback final
						function(err, results){
							next_step(err);
						});
					}
					else{
						next_step();
					}

				}
			});
		},
		function createPairFormData (next_step) {
			fs.access(CONFIG.app.urls.repertoire_upload, fs.R_OK | fs.W_OK, function (err) {
				if (err) {	
					//errno: -13, code: 'EACCES'
					//Si les permissions sont mauvaises
					if (err.errno == -13) {
						log.error("Vérifiez les droits de lecture / écriture sur " + CONFIG.app.urls.repertoire_upload);
						next_step(err);
					}
					//errno: -2, code: 'ENOENT'
					//Si le dossier n'existe pas, on le créé
					else if (err.errno == -2){
						fs.mkdir(CONFIG.app.urls.repertoire_upload, 0o740, function (err, dir) {
							if (err) {
								log.error("Impossible de créer le répertoire de données.");
								next_step(err);
							}
							else{
								//Extraction du zip de fichiers par défaut
								var adm_zip = require("adm-zip");
								var pairform_data = new adm_zip(__dirname + "/pairform_data.zip");
								pairform_data.extractAllTo(CONFIG.app.urls.repertoire_upload);
								next_step();
							}
						});
					}
					else {
						next_step(err);
					}
				}
				else{
					
					next_step();	
				}
			});
		}
		],

		// Callback final
		function(err, results){
			connection.end();
			if (err) {
				throw err;
			}
			else{
				log.debug("Verification de la base de données terminée.");
				callback();
			}
		}
	);
}