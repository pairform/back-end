module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      dist_app_js: {
        files: ['./pairform_modules/app/private/js/**/*.js'],
        tasks: ['concat:dist_app']
      },
      dist_backoffice_js: {
        files: ['./pairform_modules/backoffice/private/js/**/*.js'],
        tasks: ['concat:dist_backoffice']
      },
      dist_app_css: {
        files: ['./pairform_modules/app/private/css/*.css'],
        tasks: ['concat:dist_css_app']
      },
      dist_vitrine_css: {
        files: ['./pairform_modules/vitrine/private/css/*.css'],
        tasks: ['concat:dist_css_vitrine']
      },
      dist_home_js: {
        files: ['./pairform_modules/home/private/js/*.js'],
        tasks: ['concat:dist_home']
      },
      dist_home_css: {
        files: ['./pairform_modules/home/private/css/*.css'],
        tasks: ['concat:dist_css_home']
      },
      dist_espace_js: {
        files: ['./pairform_modules/serveur/private/js/**/*.js'],
        tasks: ['concat:dist_espace']
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      // build_import: {
      //   src: "pairform_modules/app/private/js/import.js",
      //   dest: 'dist/pf_import.min.js'
      // },
      build_app: {
        src: "dist/pf_app.js",
        dest: 'dist/pf_app.min.js'
      },
      build_home: {
        src: "dist/pf_home.js",
        dest: 'dist/pf_home.min.js'
      },
      build_home_lib: {
        src: "dist/lib_home.js",
        dest: 'dist/lib_home.min.js'
      },
      build_vitrine: {
        src: "pairform_modules/vitrine/private/js/pf_vitrine.js",
        dest: 'dist/pf_vitrine.min.js'
      },
      build_backoffice: {
        src: "dist/pf_backoffice.js",
        dest: 'dist/pf_backoffice.min.js'
      },
      build_backoffice_lib: {
        src: "dist/lib_backoffice.js",
        dest: 'dist/lib_backoffice.min.js'
      },
      build_injecteur: {
        src: "dist/pf_injecteur.js",
        dest: 'dist/pf_injecteur.min.js'
      }
    },
    concat : {
      options: {
        separator : ';\n'
      },
      dist_app : {
        src : [
          "pairform_modules/app/private/js/layer_wrap_pre.js",
          
          "pairform_modules/app/private/js/app.js",
          "pairform_modules/app/private/js/config.js",

          "pairform_modules/app/private/js/classes/message.js",
          "pairform_modules/app/private/js/classes/notification.js",
          "pairform_modules/app/private/js/classes/panneau.js",
          "pairform_modules/app/private/js/classes/reseau.js",
          "pairform_modules/app/private/js/classes/utilisateur.js",

          "pairform_modules/app/private/js/helpers/general.js",
          "pairform_modules/app/private/js/helpers/http.js",
          "pairform_modules/app/private/js/helpers/langue.js",
          "pairform_modules/app/private/js/helpers/addons.js",

          // "pairform_modules/app/private/js/controllers/controller_accroche.js",
          "pairform_modules/app/private/js/controllers/controller_avatar_upload.js",
          "pairform_modules/app/private/js/controllers/controller_carrousel.js",
          "pairform_modules/app/private/js/controllers/controller_connexion.js",
          "pairform_modules/app/private/js/controllers/controller_cookie_consent.js",
          "pairform_modules/app/private/js/controllers/controller_fiche_ressource.js",
          "pairform_modules/app/private/js/controllers/controller_messages.js",
          "pairform_modules/app/private/js/controllers/controller_nav_bar.js",
          "pairform_modules/app/private/js/controllers/controller_preferences.js",
          "pairform_modules/app/private/js/controllers/controller_presentation.js",
          "pairform_modules/app/private/js/controllers/controller_profil.js",
          "pairform_modules/app/private/js/controllers/controller_profil_config.js",
          "pairform_modules/app/private/js/controllers/controller_profil_focus.js",
          "pairform_modules/app/private/js/controllers/controller_recherche_profil.js",
          "pairform_modules/app/private/js/controllers/controller_reseau.js",
          "pairform_modules/app/private/js/controllers/controller_ressources.js",
          "pairform_modules/app/private/js/controllers/controller_succes.js",
          "pairform_modules/app/private/js/controllers/main_controller.js",

          "pairform_modules/app/private/js/directives/draggable.js",
          "pairform_modules/app/private/js/directives/pfCarte.js",
          "pairform_modules/app/private/js/directives/pfCarteMessage.js",
          "pairform_modules/app/private/js/directives/pfCommentable.js",
          "pairform_modules/app/private/js/directives/pfImageThumbnail.js",
          "pairform_modules/app/private/js/directives/pfMenuChangerRole.js",
          "pairform_modules/app/private/js/directives/pfMenuLangue.js",
          "pairform_modules/app/private/js/directives/pfMenuMedaille.js",
          "pairform_modules/app/private/js/directives/pfMenuOptionsMessage.js",
          "pairform_modules/app/private/js/directives/pfMenuOptionsGeolocalisation.js",
          "pairform_modules/app/private/js/directives/pfMenuPartager.js",
          "pairform_modules/app/private/js/directives/pfMenuTags.js",
          "pairform_modules/app/private/js/directives/pfMessage.js",
          "pairform_modules/app/private/js/directives/pfMessageList.js",
          "pairform_modules/app/private/js/directives/pfObjectifApprentissage.js",
          "pairform_modules/app/private/js/directives/pfQuestionProfilApprentissage.js",
          "pairform_modules/app/private/js/directives/pfUploadPj.js",
          "pairform_modules/app/private/js/directives/pfUserMenu.js",
          "pairform_modules/app/private/js/directives/stopEvent.js",

          "pairform_modules/app/private/js/filters/date_timestamp.js",
          "pairform_modules/app/private/js/filters/orderMessages.js",
          "pairform_modules/app/private/js/filters/search.js",

          "pairform_modules/app/private/js/services/CarrouselLazyLoad.js",
          "pairform_modules/app/private/js/services/MessageService.js",
          "pairform_modules/app/private/js/services/OpenLayerLazyLoad.js",
          "pairform_modules/app/private/js/services/PJService.js",

          "pairform_modules/app/private/js/run.js",
          
          "pairform_modules/app/private/js/layer_wrap_post.js"
        ],
        dest : "dist/pf_app.js"
      },
      dist_css_app : {

        options: {
          separator : '\n'
        },
        src : [
          "pairform_modules/app/private/css/general.css",
          "pairform_modules/app/private/css/web_app.css",
          "pairform_modules/app/private/css/tooltip.css",
          "pairform_modules/app/private/css/accroche.css",
          "pairform_modules/app/private/css/carrousel.css",
          "pairform_modules/app/private/css/cookie_consent.css",
          // "pairform_modules/app/private/css/commentaires.css",
          // "pairform_modules/app/private/css/edition_profil.css",
          // "pairform_modules/app/private/css/forms.css",
          "pairform_modules/app/private/css/kiosque.css",
          "pairform_modules/app/private/css/injecteur.css",
          "pairform_modules/app/private/css/navbar.css",
          "pairform_modules/app/private/css/new_commentaires.css",
          "pairform_modules/app/private/css/new_design.css",
          "pairform_modules/app/private/css/new_forms.css",
          "pairform_modules/app/private/css/new_reseaux.css",
          "pairform_modules/app/private/css/new_ressource.css",
          "pairform_modules/app/private/css/ng_animations.css",
          "pairform_modules/app/private/css/panels.css",
          "pairform_modules/app/private/css/presentation.css",
          "pairform_modules/app/private/css/profil_focus.css",
          "pairform_modules/app/private/css/raleway.css",
          // "pairform_modules/app/private/css/recherche.css",
          // "pairform_modules/app/private/css/reseaux.css",
          // "pairform_modules/app/private/css/succes.css",
          "pairform_modules/app/private/css/addons.css",
          "public/lib/fonts/font-awesome-4.2.0/css/font-awesome.min.css"
        ],
        dest : "dist/pf_app.css"
      },
      dist_home : {
        src : [
          "pairform_modules/home/private/js/helpers/http.js",
          "pairform_modules/home/private/js/app.js",
          "pairform_modules/home/private/js/routes.js",
          "pairform_modules/home/private/js/classes/message.js",
          "pairform_modules/home/private/js/classes/notification.js",
          "pairform_modules/home/private/js/classes/panneau.js",
          "pairform_modules/home/private/js/classes/reseau.js",
          "pairform_modules/home/private/js/classes/utilisateur.js",
          "pairform_modules/home/private/js/controllers/controller_profil.js",
          "pairform_modules/home/private/js/controllers/controller_reseau.js",
        ],
        dest : "dist/pf_home.js"
      },
      dist_css_home : {
        options: {
          separator : '\n'
        },
        src : [
          "pairform_modules/home/private/css/accroche.css",
          "pairform_modules/home/private/css/carrousel.css",
          "pairform_modules/home/private/css/commentaires.css",
          "pairform_modules/home/private/css/edition_profil.css",
          "pairform_modules/home/private/css/forms.css",
          "pairform_modules/home/private/css/general.css",
          "pairform_modules/home/private/css/injecteur.css",
          "pairform_modules/home/private/css/kiosque.css",
          "pairform_modules/home/private/css/navbar.css",
          "pairform_modules/home/private/css/ng_animations.css",
          "pairform_modules/home/private/css/panels.css",
          "pairform_modules/home/private/css/presentation.css",
          "pairform_modules/home/private/css/profil.css",
          "pairform_modules/home/private/css/raleway.css",
          "pairform_modules/home/private/css/recherche.css",
          "pairform_modules/home/private/css/reseaux.css",
          "pairform_modules/home/private/css/ressource.css",
          "pairform_modules/home/private/css/succes.css",
          "pairform_modules/home/private/css/web_app.css",
          "pairform_modules/app/private/css/new_forms.css",
          "pairform_modules/backoffice/private/css/timeline.css",
          "public/lib/fonts/font-awesome-4.2.0/css/font-awesome.min.css"
        ],
        dest : "dist/pf_home.css"
      },
      lib_home : {
        src : [
          "public/lib/js/jquery-1.9.0.js",
          // "public/lib/js/angular-1.5.8.js",
          // "public/lib/js/angular-animate-1.5.8.js",
          "public/lib/js/angular-1.4.4.js",
          "public/lib/js/angular-animate-1.4.4.min.js",
          "public/lib/js/angular-carousel.min.js",
          "public/lib/js/angular-cookies.js",
          "public/lib/js/angular-file-upload.min.js",
          "public/lib/js/angular-img-crop.min.js",
          "public/lib/js/angular-loading-bar.js",
          "public/lib/js/angular-route.min.js",
          "public/lib/js/angular-sanitize.min.js",
          "public/lib/js/angular-thumbnail.min.js",
          "public/lib/js/angular-timeline.js",
          "public/lib/js/angular-touch.min.js",
          "public/lib/js/angular-translate-2.7.2.min.js",
          "public/lib/js/angular-translate-loader-static-files.js",
          "public/lib/js/angular-ui-router.min.js",
          "public/lib/js/angular_toaster.js",
          "public/lib/js/jquery.cookie.js",
          "public/lib/js/jquery.ui.position.js",
          "public/lib/js/ng-file-upload.js",
          "public/lib/js/ngStorage.min.js",
          "public/lib/js/angular-datepicker.min.js"
        ],
        dest : "dist/lib_home.js"
      },
      lib_app : {
        src : [
          "public/lib/js/jquery-1.9.0.js",
          "public/lib/js/jquery.cookie.js",
          "public/lib/js/jquery.ui.position.js",
          "public/lib/js/angular-1.4.5.js",
          "public/lib/js/angular-animate-1.4.5.min.js",
          "public/lib/js/angular-loading-bar.js",
          "public/lib/js/angular-cookies.js",
          "public/lib/js/angular-touch.min.js",
          "public/lib/js/angular-carousel.min.js",
          "public/lib/js/angular-img-crop.min.js",
          "public/lib/js/angular_toaster.js",
          "public/lib/js/angular-route.min.js",
          "public/lib/js/angular-sanitize.min.js",
          "public/lib/js/angular-translate-2.7.2.js",
          "public/lib/js/angular-translate-loader-static-files.js",
          "public/lib/js/angular-ui-router.min.js",
          'public/lib/js/ng-file-upload.min.js',
          'public/lib/js/ng-file-upload-shim.min.js',
          'public/lib/js/angular-thumbnail.js',
          "public/lib/js/ngStorage.min.js",
          "public/lib/js/languageManager.js",
          "public/lib/js/exif.js"
        ],
        dest : "dist/lib_app.js"
      },

      lib_app_css : {
        src : [
          "public/lib/fonts/font-awesome-4.2.0/css/font-awesome.min.css",
          "public/lib/css/outdatedbrowser.min.css",
          "public/lib/css/angular-carousel.min.css",
          "pairform_modules/app/private/css/toaster.css",
          "pairform_modules/app/private/css/angular-loading-bar.css"
        ],
        dest : "dist/lib_app.css"
      },
      dist_espace : {
        src : [
          "pairform_modules/serveur/private/js/pre.js",

          "pairform_modules/app/private/js/helpers/general.js",
          "pairform_modules/app/private/js/helpers/http.js",
          "pairform_modules/app/private/js/helpers/langue.js",
          "pairform_modules/app/private/js/helpers/addons.js",

          "pairform_modules/serveur/private/js/app.js",
          "pairform_modules/serveur/private/js/config.js",

          "pairform_modules/app/private/js/classes/message.js",
          "pairform_modules/app/private/js/classes/notification.js",
          "pairform_modules/app/private/js/classes/panneau.js",
          "pairform_modules/app/private/js/classes/reseau.js",
          "pairform_modules/app/private/js/classes/utilisateur.js",

          "pairform_modules/serveur/private/js/directives/pfMessage.js",
          "pairform_modules/serveur/private/js/directives/pfMessageList.js",


          "pairform_modules/app/private/js/directives/pfCarte.js",
          "pairform_modules/app/private/js/directives/pfCarteMessage.js",
          "pairform_modules/app/private/js/directives/pfImageThumbnail.js",
          "pairform_modules/app/private/js/directives/pfMenuChangerRole.js",
          "pairform_modules/app/private/js/directives/pfMenuLangue.js",
          "pairform_modules/app/private/js/directives/pfMenuMedaille.js",
          "pairform_modules/app/private/js/directives/pfMenuOptionsMessage.js",
          "pairform_modules/app/private/js/directives/pfMenuOptionsGeolocalisation.js",
          "pairform_modules/app/private/js/directives/pfMenuPartager.js",
          "pairform_modules/app/private/js/directives/pfMenuTags.js",
          "pairform_modules/app/private/js/directives/pfUploadPj.js",
          "pairform_modules/app/private/js/directives/pfUserMenu.js",


          "pairform_modules/app/private/js/services/OpenLayerLazyLoad.js",
          "pairform_modules/app/private/js/services/MessageService.js",          
          "pairform_modules/app/private/js/services/PJService.js",

          "pairform_modules/app/private/js/filters/date_timestamp.js",
          "pairform_modules/app/private/js/filters/search.js",
          "pairform_modules/app/private/js/filters/linky.js",

          "pairform_modules/serveur/private/js/filters/orderMessages.js",
          
          "pairform_modules/serveur/private/js/root.js",
          "pairform_modules/serveur/private/js/controllers/reseaux.js",
          "pairform_modules/serveur/private/js/controllers/community.js",
          "pairform_modules/serveur/private/js/controllers/objectifs.js",
          "pairform_modules/serveur/private/js/controllers/messages.js",
          "pairform_modules/serveur/private/js/controllers/timeline.js",

          "pairform_modules/app/private/js/controllers/controller_profil_focus.js",
          "pairform_modules/app/private/js/controllers/controller_nav_bar.js",
          "pairform_modules/app/private/js/controllers/controller_connexion.js",






          "pairform_modules/serveur/private/js/post.js"
        ],
        dest : "dist/pf_espace.js"
      },

      lib_espace : {
        src : [
          "public/lib/js/jquery-1.9.0.js",
          "public/lib/js/angular-1.4.4.js",
          "public/lib/js/angular-animate-1.4.4.min.js",
          "public/lib/js/angular-cookies.js",
          "public/lib/js/angular-loading-bar.js",
          "public/lib/js/angular-thumbnail.js",
          "public/lib/js/angular-sanitize.min.js",
          "public/lib/js/angular_toaster.js",
          "public/lib/js/angular-translate-2.7.2.min.js",
          "public/lib/js/angular-translate-loader-static-files.js",
          "public/lib/js/exif.js",
          "public/lib/js/ng-file-upload.js",
          "public/lib/js/ngStorage.min.js",
          "public/lib/js/moment-with-locales.min.js"
        ],
        dest : "dist/lib_espace.js"
      },
      dist_css_vitrine : {
        
        options: {
          separator : '\n'
        },
        src: [
          "pairform_modules/vitrine/private/css/accueil.css",
          "pairform_modules/vitrine/private/css/angular.css",
          "pairform_modules/vitrine/private/css/application.css",
          "pairform_modules/vitrine/private/css/circle.css",
          "pairform_modules/vitrine/private/css/conseil.css",
          "pairform_modules/vitrine/private/css/demo.css",
          "pairform_modules/vitrine/private/css/equipe.css",
          "pairform_modules/vitrine/private/css/fonts.css",
          "pairform_modules/vitrine/private/css/forms.css",
          "pairform_modules/vitrine/private/css/formation.css",
          "pairform_modules/vitrine/private/css/general.css",
          "pairform_modules/vitrine/private/css/keyframes.css",
          "pairform_modules/vitrine/private/css/kiosque.css",
          "pairform_modules/vitrine/private/css/livreblanc.css",
          "pairform_modules/vitrine/private/css/sur_mesure.css",
          "pairform_modules/vitrine/private/css/tarif.css"
        ],
        dest: "dist/pf_vitrine.css"
      },
      dist_backoffice : {
        src : [
          "pairform_modules/backoffice/private/js/initialisationBackoffice.js",
          "pairform_modules/backoffice/private/js/filtres.js",
          "pairform_modules/backoffice/private/js/services.js",
          "pairform_modules/backoffice/private/js/directives/draggable_page.js",
          "pairform_modules/backoffice/private/js/directives/drag_trash.js",
          "pairform_modules/backoffice/private/js/directives/timeline.js",
          "pairform_modules/backoffice/private/js/controllers/controleursTimeline.js",
          "pairform_modules/backoffice/private/js/controllers/controleursMenu.js",
          "pairform_modules/backoffice/private/js/controllers/controleursOpenBadges.js",
          "pairform_modules/backoffice/private/js/controllers/controleursTableauDeBord.js",
          "pairform_modules/backoffice/private/js/controllers/controleursConnexion.js",
          "pairform_modules/backoffice/private/js/controllers/controleursEspaces.js",
          "pairform_modules/backoffice/private/js/controllers/controleursRessources.js",
          "pairform_modules/backoffice/private/js/controllers/controleursCapsules.js",
          "pairform_modules/backoffice/private/js/controllers/controleursVisibiliteCapsules.js",
          "pairform_modules/backoffice/private/js/controllers/controleursGroupes.js",
          "pairform_modules/backoffice/private/js/controllers/controleursRolesEspace.js",
          "pairform_modules/backoffice/private/js/controllers/controleursRolesRessource.js",
          "pairform_modules/backoffice/private/js/controllers/controleursStatistiques.js"
        ],
        dest: "dist/pf_backoffice.js"
      },
      lib_backoffice : {
        src : [
          "public/lib/js/bootstrap.min.js"
        ],
        dest: "dist/lib_backoffice.js"
      },
      dist_injecteur : {
        src : [
          "pairform_modules/injecteur/private/js/layer_wrap_pre.js",

          "pairform_modules/app/private/js/classes/message.js",
          "pairform_modules/app/private/js/classes/notification.js",
          "pairform_modules/app/private/js/classes/panneau.js",
          "pairform_modules/app/private/js/classes/reseau.js",
          "pairform_modules/app/private/js/classes/utilisateur.js",

          "pairform_modules/app/private/js/helpers/general.js",
          "pairform_modules/app/private/js/helpers/http.js",
          "pairform_modules/app/private/js/helpers/langue.js",
          
          "pairform_modules/injecteur/private/js/config.js",
          "pairform_modules/injecteur/private/js/controllers/connexion.js",
          "pairform_modules/injecteur/private/js/controllers/creation_capsule.js",
          "pairform_modules/injecteur/private/js/controllers/injecteur.js",
          "pairform_modules/injecteur/private/js/controllers/nav_bar.js",
          "pairform_modules/injecteur/private/js/controllers/tutoriel.js",
          "pairform_modules/injecteur/private/js/filters/id_espace.js",
          "pairform_modules/injecteur/private/js/run.js",
          "pairform_modules/injecteur/private/js/layer_wrap_post.js"
        ],
        dest: "dist/pf_injecteur.js"
      }
    }
  });

  grunt.event.on('watch', function(action, filepath, target) {
    grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Load the plugin that provides the "concat" task.
  grunt.loadNpmTasks('grunt-contrib-concat');

  // Load the plugin that provides the "watch" task.
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['concat', 'uglify', 'watch']);
  // grunt.registerTask('default', ['concat']);

};
