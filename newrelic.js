'use strict'
/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */

exports.config = {
  /**
   * Array of application names.
   */
  app_name: (process.env.NODE_ENV == "production" ? ['PairForm'] : ['PairForm local']),
  /**
   * Your New Relic license key.
   */
  license_key: 'f178e0c4282c964282244b25645bd8f05b1db323',
  logging: {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level: 'info'
  },
  /**
   * Options regarding collecting system information. Used for system
   * utilization based pricing scheme.
   */
  utilization: {
    /**
     * This flag dictates whether the agent attempts to reach out to AWS
     * to get info about the vm the process is running on.
     *
     * @env NEW_RELIC_UTILIZATION_DETECT_AWS
     */
    detect_aws: false,
    /**
     * This flag dictates whether the agent attempts to detect if the
     * the process is running on Pivotal Cloud Foundary.
     *
     * @env NEW_RELIC_UTILIZATION_DETECT_PCF
     */
    detect_pcf: false,
    /**
     * This flag dictates whether the agent attempts to reach out to Azure to
     * get info about the vm the process is running on.
     *
     * @env NEW_RELIC_UTILIZATION_DETECT_AZURE
     */
    detect_azure: false,
    /**
     * This flag dictates whether the agent attempts to read files
     * to get info about the container the process is running in.
     *
     * @env NEW_RELIC_UTILIZATION_DETECT_DOCKER
     */
    detect_docker: false,

    /**
     * This flag dictates whether the agent attempts to reach out to GCP
     * to get info about the vm the process is running on.
     *
     * @env NEW_RELIC_UTILIZATION_DETECT_GCP
     */
    detect_gcp: false
  },
}